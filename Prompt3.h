#if !defined(AFX_PROMPT3_H__B0662009_171A_49C9_A79D_462E44B995AC__INCLUDED_)
#define AFX_PROMPT3_H__B0662009_171A_49C9_A79D_462E44B995AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Prompt3.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Prompt3 dialog

class Prompt3 : public CDialog
{
// Construction
public:
	Prompt3(CWnd* pParent = NULL);   // standard constructor

	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(Prompt3)
	enum { IDD = IDD_DIALOG2 };
	CString	m_prompt;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Prompt3)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Prompt3)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROMPT3_H__B0662009_171A_49C9_A79D_462E44B995AC__INCLUDED_)
