#if !defined(AFX_FILLERGRAPH_H__38B49728_8332_4884_920A_53105C9C6805__INCLUDED_)
#define AFX_FILLERGRAPH_H__38B49728_8332_4884_920A_53105C9C6805__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FillerGraph.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// FillerGraph dialog

class FillerGraph : public CDialog
{
friend class CMainFrame;
friend class CCapApp;
friend class XAxisView;
friend class Modify;
friend class SelectingFiller;
// Construction
public:

	void UpdateDisplay();
	FillerGraph(CWnd* pParent = NULL);   // standard constructor


	CMainFrame* pframe;
	CCapApp *theapp;
	XAxisView* m_pxaxis;
	float fill_graph_aftr_scale[600];
	int count1;float sum;

	int filler_valve_selection;

	float sum_tot_filler;int count_tot_fillr;
	float	sum_slctd_filler; int count_slctd_filler;

// Dialog Data
	//{{AFX_DATA(FillerGraph)
	enum { IDD = IDD_FillerGraph };
	CString	m_resultavg;
	CString	m_selected_filler_avg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FillerGraph)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FillerGraph)
	virtual void OnOK();
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnScalefactor10();
	afx_msg void OnSelectFillerValve();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLERGRAPH_H__38B49728_8332_4884_920A_53105C9C6805__INCLUDED_)
