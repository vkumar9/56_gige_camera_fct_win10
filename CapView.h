// CapView.h : interface of the CCapView class

/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAPVIEW_H__47013E9A_3D69_412F_B3BA_3B6905798A00__INCLUDED_)
#define AFX_CAPVIEW_H__47013E9A_3D69_412F_B3BA_3B6905798A00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCapView : public CFormView
{
friend class CCapDoc;
friend class CMainFrame;
friend class CCapApp;

protected: // create from serialization only
	CCapView();
	DECLARE_DYNCREATE(CCapView)

public:
	//{{AFX_DATA(CCapView)
	enum { IDD = IDD_CAP_FORM };
	CEdit	m_camser3;		//Control variable for interfacing with the camera 3 serial number edit box.
	CEdit	m_camser2;		//Control variable for interfacing with the camera 2 serial number edit box.
	CEdit	m_camser1;		//Control variable for interfacing with the camera 1 serial number edit box.
	CStatic	m_ul9;
	CStatic	m_uh9;
	CEdit	m_u2e;
	CEdit	m_u2d;
	CStatic	m_u2c;
	CStatic	m_u2b;
	CStatic	m_u2a;
	CString	m_total;		//Total number of inspections that have been performed.
	CString	m_rejects;		//Total number of bottles ejected.
	CString	m_status;		//Variable used to display the name of the selected job or the status of the system.
	CString	m_result1;
	CString	m_result2;
	CString	m_result3;
	CString	m_result4;
	CString	m_result5;
	CString	m_result6;
	CString	m_result7;
	CString	m_result8;
	CString	m_result9;
	CString	m_result10;
	CString	m_result11;
	CString	m_limhl;
	CString	m_limhm;
	CString	m_limhr;
	CString	m_limdiff;
	CString	m_limfill;
	CString	m_limtb;
	CString	m_limu;
	CString	m_limdiff2;
	CString	m_limfill2;
	CString	m_limhl2;
	CString	m_limhm2;
	CString	m_limhr2;
	CString	m_limtb2;
	CString	m_limu2;
	CString	m_limtb3;
	CString	m_limtb4;
	CString	m_limux;
	CString	m_limux2;
	CStatic	m_limtb4c;
	CStatic	m_tb3;
	CStatic	m_tbsurface;
	CEdit	m_result8s;
	CString	m_bwcount;				//NOT USED
	CString	m_colorcount;			//NOT USED
	CString	m_timedtrigger;			//Variable displays millisecond delay between timed triggers, in edit box.
	CString	m_time;			//NOT USED
	CString	m_time1;		//Used to display XAxisView.Display() function time.
	CString	m_time2;		//Used to display XAxisView.Display2() function time.
	CString	m_time3;		//Used to display XAxisView.Display3() function time.
	CString	m_time4;		//Used to display XAxisViewInspect() function time.
	CString	m_name3;
	CString	m_h1;
	CString	m_h2;
	CString	m_h3;
	CString	m_h4;
	CString	m_h5;
	CString	m_h6;
	CString	m_h7;
	CString	m_h8;
	CString	m_h9;
	CString	m_l1;
	CString	m_l2;
	CString	m_l3;
	CString	m_l4;
	CString	m_l5;
	CString	m_l6;
	CString	m_l7;
	CString	m_l8;
	CString	m_l9;
	CString	m_insp1;
	CString	m_insp2;
	CString	m_insp3;
	CString	m_insp4;
	CString	m_insp5;
	CString	m_insp6;
	CString	m_insp7;
	CString	m_insp8;
	CString	m_insp9;
	CString	m_timedfiller;		//GUI variable for filler timer simulator.
	CString	m_edit1;			//GUI variable for cocked cap camera 2 offset.
	CString	m_edit5;			//GUI variable for cocked cap camera 3 offset.
	CString	m_editcam1;			//Variable displays camera 1 serial number in edit box
	CString	m_editcam2;			//Variable displays camera 1 serial number in edit box
	CString	m_editcam3;			//Variable displays camera 1 serial number in edit box
	CString	m_inspdelay;		//Variable displays the inspection delay in edit box
	//}}AFX_DATA

// Attributes
public:
	CCapDoc* GetDocument();
	CMainFrame* pframe;
	CCapApp* theapp;
	CBitmapButton* pBit;	//NOT USED
	
	CString m_currentjob;	//String used to hold the name of the currently selected job.
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCapView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
void UpdateMinor();
	int BoxColor;			//NOT USED
	void OnlineBox(int BoxColor);
	bool ToggleColor;		//Flag used with a timer to toggle the ejector off-line color from red to gray.
	bool EjectAll;			//Flag indicating that all bottles are being ejected.
	void UpDateEject();
	bool toggle;			//Flag indicating if timed trigger is on (false) or off (true).
	void UpdateDisplay();
	void UpdateDisplay2();
void newImageSize();
	virtual ~CCapView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCapView)
	afx_msg void OnTrig1();
	afx_msg void OnTrig2();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRfault();
	afx_msg void OnRtot();
	afx_msg void OnFreeze();
	afx_msg void OnShowjob();
	afx_msg void OnRejectall();
	afx_msg void OnOnline();
	afx_msg void OnOffline();
	afx_msg void OnCont();
	afx_msg void OnFreeze2();
	afx_msg void OnFbutton();
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnTesteject();
	afx_msg void OnFmore();
	afx_msg void OnFless();
	afx_msg void OnC2Up();
	afx_msg void OnC2Dwn();
	afx_msg void OnC3Up();
	afx_msg void OnC3Dwn();
	afx_msg void OnButton2();
	afx_msg void OnSetfocusEdit2();
	afx_msg void OnChangeEdit2();
	afx_msg void OnChangeEdit7();
	afx_msg void OnChangeEdit8();
	afx_msg void OnResetTotCnt();
	afx_msg void OnTempPlc();
	afx_msg void OnTempUSB();
	afx_msg void OnLoadMultiple();
	afx_msg void OnRadio1();
	afx_msg void OnInspdelayup();
	afx_msg void OnInspdelaydw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CapView.cpp
inline CCapDoc* CCapView::GetDocument()
   { return (CCapDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAPVIEW_H__47013E9A_3D69_412F_B3BA_3B6905798A00__INCLUDED_)
