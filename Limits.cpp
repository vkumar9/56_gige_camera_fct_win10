// Limits.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Limits.h"
#include "Lim.h"

#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Limits dialog


Limits::Limits(CWnd* pParent /*=NULL*/)
	: CDialog(Limits::IDD, pParent)
{
	//{{AFX_DATA_INIT(Limits)
	//}}AFX_DATA_INIT
}


void Limits::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Limits)
	DDX_Control(pDX, IDC_SPIN9x, m_spin9x);
	DDX_Control(pDX, IDC_SPIN9, m_spin9);
	DDX_Control(pDX, IDC_SPIN8x, m_spin8x);
	DDX_Control(pDX, IDC_SPIN8, m_spin8);
	DDX_Control(pDX, IDC_SPIN7x, m_spin7x);
	DDX_Control(pDX, IDC_SPIN7, m_spin7);
	DDX_Control(pDX, IDC_SPIN6x, m_spin6x);
	DDX_Control(pDX, IDC_SPIN6, m_spin6);
	DDX_Control(pDX, IDC_SPIN5x, m_spin5x);
	DDX_Control(pDX, IDC_SPIN5, m_spin5);
	DDX_Control(pDX, IDC_SPIN4x, m_spin4x);
	DDX_Control(pDX, IDC_SPIN4, m_spin4);
	DDX_Control(pDX, IDC_SPIN3x, m_spin3x);
	DDX_Control(pDX, IDC_SPIN3, m_spin3);
	DDX_Control(pDX, IDC_SPIN2x, m_spin2x);
	DDX_Control(pDX, IDC_SPIN2, m_spin2);
	DDX_Control(pDX, IDC_SPIN1x, m_spin1x);
	DDX_Control(pDX, IDC_SPIN1, m_spin1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Limits, CDialog)
	//{{AFX_MSG_MAP(Limits)
	ON_BN_CLICKED(IDC_ENABLE1, OnEnable1)
	ON_BN_CLICKED(IDC_ENABLE2, OnEnable2)
	ON_BN_CLICKED(IDC_ENABLE3, OnEnable3)
	ON_BN_CLICKED(IDC_ENABLE4, OnEnable4)
	ON_BN_CLICKED(IDC_ENABLE5, OnEnable5)
	ON_BN_CLICKED(IDC_ENABLE6, OnEnable6)
	ON_BN_CLICKED(IDC_ENABLE7, OnEnable7)
	ON_BN_CLICKED(IDC_ENABLE8, OnEnable8)
	ON_BN_CLICKED(IDC_ENABLE9, OnEnable9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Limits message handlers

BOOL Limits::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_plim=this;
	theapp=(CCapApp*)AfxGetApp();

	m_spin1.SetRange(0, 500);
	m_spin1.SetPos(theapp->inspctn[1].lmin);
	m_spin1x.SetRange(0, 500);
	m_spin1x.SetPos(theapp->inspctn[1].lmax);

	m_spin2.SetRange(0, 500);
	m_spin2.SetPos(theapp->inspctn[2].lmin);
	m_spin2x.SetRange(0, 500);
	m_spin2x.SetPos(theapp->inspctn[2].lmax);

	m_spin3.SetRange(0, 500);
	m_spin3.SetPos(theapp->inspctn[3].lmin);
	m_spin3x.SetRange(0, 500);
	m_spin3x.SetPos(theapp->inspctn[3].lmax);

	m_spin4.SetRange(0, 500);
	m_spin4.SetPos(theapp->inspctn[4].lmin);
	m_spin4x.SetRange(0, 500);
	m_spin4x.SetPos(theapp->inspctn[4].lmax);

	m_spin5.SetRange(0, 500);
	m_spin5.SetPos(theapp->inspctn[5].lmin);
	m_spin5x.SetRange(0, 500);
	m_spin5x.SetPos(theapp->inspctn[5].lmax);

	m_spin6.SetRange(0, 2000);
	m_spin6.SetPos(theapp->inspctn[6].min);
	m_spin6x.SetRange(0, 2000);
	m_spin6x.SetPos(theapp->inspctn[6].max);

	m_spin7.SetRange(0, 500);
	m_spin7.SetPos(theapp->inspctn[7].min);
	m_spin7x.SetRange(0, 500);
	m_spin7x.SetPos(theapp->inspctn[7].max);

	m_spin8.SetRange(0, 1500);
	m_spin8.SetPos(theapp->inspctn[8].min);
	m_spin8x.SetRange(0, 1500);
	m_spin8x.SetPos(theapp->inspctn[8].max);

	m_spin9.SetRange(0, 1500);
	m_spin9.SetPos(theapp->inspctn[9].min);
	m_spin9x.SetRange(0, 1500);
	m_spin9x.SetPos(theapp->inspctn[9].max);

	tLHMin=theapp->inspctn[1].min;
	tLHMax=theapp->inspctn[1].max;
	tRHMin=theapp->inspctn[2].min;
	tRHMax=theapp->inspctn[2].max;
	tMHMin=theapp->inspctn[3].min;
	tMHMax=theapp->inspctn[3].max;
	tDMin=theapp->inspctn[4].min;
	tDMax=theapp->inspctn[4].max;
	tTBMin=theapp->inspctn[5].min;
	tTBMax=theapp->inspctn[5].max;
	tTB2Min=theapp->inspctn[6].min;
	tTB2Max=theapp->inspctn[6].max;
	tFillMin=theapp->inspctn[7].min;
	tFillMax=theapp->inspctn[7].max;
	tUMin=theapp->inspctn[8].min;
	tUMax=theapp->inspctn[8].max;
	tU2Min=theapp->inspctn[9].min;
	tU2Max=theapp->inspctn[9].max;

	CString temp;
	char buf[10];

	//pframe->ProfileCode=itoa(pframe->CurrentProfileInt,buf,10);
pframe->ProfileCode=itoa(theapp->jobinfo[pframe->CurrentJobNum].profile,buf,10);
	theapp->SavedEnable1=atoi(pframe->ProfileCode.Right(1));

	temp=pframe->ProfileCode.Right(2);
	theapp->SavedEnable2=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(3);
	theapp->SavedEnable3=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(4);
	theapp->SavedEnable4=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(5);
	theapp->SavedEnable5=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(6);
	theapp->SavedEnable6=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(7);
	theapp->SavedEnable7=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(8);
	theapp->SavedEnable8=atoi(temp.Left(1));

	temp=pframe->ProfileCode.Right(9);
	theapp->SavedEnable9=atoi(temp.Left(1));

	

	if( theapp->SavedEnable1==true ){CheckDlgButton(IDC_ENABLE1,1); }else{ CheckDlgButton(IDC_ENABLE1,0); }
	if( theapp->SavedEnable2==true ){CheckDlgButton(IDC_ENABLE2,1); }else{ CheckDlgButton(IDC_ENABLE2,0); }
	if( theapp->SavedEnable3==true ){CheckDlgButton(IDC_ENABLE3,1); }else{ CheckDlgButton(IDC_ENABLE3,0); }
	if( theapp->SavedEnable4==true ){CheckDlgButton(IDC_ENABLE4,1); }else{ CheckDlgButton(IDC_ENABLE4,0); }
	if( theapp->SavedEnable5==true ){CheckDlgButton(IDC_ENABLE5,1); }else{ CheckDlgButton(IDC_ENABLE5,0); }
	if( theapp->SavedEnable6==true ){CheckDlgButton(IDC_ENABLE6,1); }else{ CheckDlgButton(IDC_ENABLE6,0); }
	if( theapp->SavedEnable7==true ){CheckDlgButton(IDC_ENABLE7,1); }else{ CheckDlgButton(IDC_ENABLE7,0); }
	if( theapp->SavedEnable8==true ){CheckDlgButton(IDC_ENABLE8,1); }else{ CheckDlgButton(IDC_ENABLE8,0); }
	if( theapp->SavedEnable9==true ){CheckDlgButton(IDC_ENABLE9,1); }else{ CheckDlgButton(IDC_ENABLE9,0); }

	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void Limits::OnOK() 
{
	if(pframe->CurrentJobNum!=0)
	{
		theapp->inspctn[1].min=pframe->m_pxaxis->LHMin=m_spin1.GetPos();
		theapp->inspctn[1].max=pframe->m_pxaxis->LHMax=m_spin1x.GetPos();
		theapp->inspctn[2].min=pframe->m_pxaxis->RHMin=m_spin2.GetPos();
		theapp->inspctn[2].max=pframe->m_pxaxis->RHMax=m_spin2x.GetPos();
		theapp->inspctn[3].min=pframe->m_pxaxis->MHMin=m_spin3.GetPos();
		theapp->inspctn[3].max=pframe->m_pxaxis->MHMax=m_spin3x.GetPos();
		theapp->inspctn[4].min=pframe->m_pxaxis->DMin=m_spin4.GetPos();
		theapp->inspctn[4].max=pframe->m_pxaxis->DMax=m_spin4x.GetPos();
		theapp->inspctn[5].min=pframe->m_pxaxis->TBMin=m_spin5.GetPos();
		theapp->inspctn[5].max=pframe->m_pxaxis->TBMax=m_spin5x.GetPos();
		theapp->inspctn[6].min=pframe->m_pxaxis->TB2Min=m_spin6.GetPos();
		theapp->inspctn[6].max=pframe->m_pxaxis->TB2Max=m_spin6x.GetPos();
		theapp->inspctn[7].min=pframe->m_pxaxis->FillMin=m_spin7.GetPos();
		theapp->inspctn[7].max=pframe->m_pxaxis->FillMax=m_spin7x.GetPos();
		theapp->inspctn[8].min=pframe->m_pxaxis->UMin=m_spin8.GetPos();
		theapp->inspctn[8].max=pframe->m_pxaxis->UMax=m_spin8x.GetPos();
		theapp->inspctn[9].min=pframe->m_pxaxis->U2Min=m_spin9.GetPos();
		theapp->inspctn[9].max=pframe->m_pxaxis->U2Max=m_spin9x.GetPos();

	char buf[2];
		CString t1=itoa(theapp->SavedEnable1,buf,10);
		CString t2=itoa(theapp->SavedEnable2,buf,10);
		CString t3=itoa(theapp->SavedEnable3,buf,10);
		CString t4=itoa(theapp->SavedEnable4,buf,10);
		CString t5=itoa(theapp->SavedEnable5,buf,10);
		CString t6=itoa(theapp->SavedEnable6,buf,10);
		CString t7=itoa(theapp->SavedEnable7,buf,10);
		CString t8=itoa(theapp->SavedEnable8,buf,10);
		CString t9=itoa(theapp->SavedEnable9,buf,10);
		//pframe->ProfileCode=pframe->P1+pframe->P2*10+pframe->P3*100+pframe->P4*1000+pframe->P5*10000+pframe->P6*100000+1000000;
		pframe->ProfileCode="1"+t9+t8+t7+t6+t5+t4+t3+t2+t1;

		theapp->jobinfo[pframe->CurrentJobNum].profile=atoi(pframe->ProfileCode);

		pframe->SaveJob(pframe->CurrentJobNum);
	}
	pframe->m_plimit->UpdateDisplay();
	CDialog::OnOK();
}

void Limits::OnEnable1() 
{
	if( theapp->SavedEnable1==true ){CheckDlgButton(IDC_ENABLE1,0); theapp->SavedEnable1=false; }else{ CheckDlgButton(IDC_ENABLE1,1); theapp->SavedEnable1=true; }
	UpdateData(false);
	
}

void Limits::OnEnable2() 
{
	if( theapp->SavedEnable2==true ){CheckDlgButton(IDC_ENABLE2,0); theapp->SavedEnable2=false; }else{ CheckDlgButton(IDC_ENABLE2,1); theapp->SavedEnable2=true; }
	UpdateData(false);
	
}

void Limits::OnEnable3() 
{
	if( theapp->SavedEnable3==true ){CheckDlgButton(IDC_ENABLE3,0); theapp->SavedEnable3=false; }else{ CheckDlgButton(IDC_ENABLE3,1); theapp->SavedEnable3=true; }
	UpdateData(false);
	
}

void Limits::OnEnable4() 
{
	if( theapp->SavedEnable4==true ){CheckDlgButton(IDC_ENABLE4,0); theapp->SavedEnable4=false; }else{ CheckDlgButton(IDC_ENABLE4,1); theapp->SavedEnable4=true; }
	UpdateData(false);
	
}

void Limits::OnEnable5() 
{
	if( theapp->SavedEnable5==true ){CheckDlgButton(IDC_ENABLE5,0); theapp->SavedEnable5=false; }else{ CheckDlgButton(IDC_ENABLE5,1); theapp->SavedEnable5=true; }
	UpdateData(false);
	
}

void Limits::OnEnable6() 
{	
	if( theapp->SavedEnable6==true ){CheckDlgButton(IDC_ENABLE6,0); theapp->SavedEnable6=false; }else{ CheckDlgButton(IDC_ENABLE6,1); theapp->SavedEnable6=true; }
	UpdateData(false);
	
}

void Limits::OnEnable7() 
{
	if( theapp->SavedEnable7==true ){CheckDlgButton(IDC_ENABLE7,0); theapp->SavedEnable7=false; }else{ CheckDlgButton(IDC_ENABLE7,1); theapp->SavedEnable7=true; }
	UpdateData(false);
	
}

void Limits::OnEnable8() 
{
	if( theapp->SavedEnable8==true ){CheckDlgButton(IDC_ENABLE8,0); theapp->SavedEnable8=false; }else{ CheckDlgButton(IDC_ENABLE8,1); theapp->SavedEnable8=true; }
	//if(theapp->Single==true){CheckDlgButton(IDC_ENABLE8,0); theapp->SavedEnable8=false;}
	UpdateData(false);
	
}

void Limits::OnEnable9() 
{
	if( theapp->SavedEnable9==true ){CheckDlgButton(IDC_ENABLE9,0); theapp->SavedEnable9=false; }else{ CheckDlgButton(IDC_ENABLE9,1); theapp->SavedEnable9=true; }
	UpdateData(false);
	
}






