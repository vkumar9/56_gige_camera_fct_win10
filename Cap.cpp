// Cap.cpp : Defines the class behaviors for the application.
//
#include "stdafx.h"

#include "Cap.h"
#include "winreg.h"

#include "MainFrm.h"

#include "CapDoc.h"
#include "CapView.h"
#include "splitter.h"
#include "math.h"
#include <string>
#include <atlbase.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool StartDelay=false;
/////////////////////////////////////////////////////////////////////////////
// CCapApp

BEGIN_MESSAGE_MAP(CCapApp, CWinApp)
	//{{AFX_MSG_MAP(CCapApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCapApp construction

CCapApp::CCapApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCapApp object

CCapApp theApp;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {D1EFEB32-270C-47AA-813C-A2325AD5C9A3}
static const CLSID clsid =
{ 0xd1efeb32, 0x270c, 0x47aa, { 0x81, 0x3c, 0xa2, 0x32, 0x5a, 0xd5, 0xc9, 0xa3 } };

/////////////////////////////////////////////////////////////////////////////
// CCapApp initialization

CRegKey key1;

BOOL CCapApp::InitInstance()
{
	showFC=true;
	fillerCount=70;
	capperCount=20;
	showAlign=false;
	teachFrontBand=false;
	timedTrigger=150;
	commProblem=true;
	noCams=false;
	cam1XOffset=60;
	Motor=true;
	SecOff=false;
	StartDelay=true;
	ForceLightWJob=false;
	SavedDataFile=0;
	NoLip=false;
	bandwidth=65;//65
	lostCam=0;

	//Camera 1 AOI initialization
	cam1Left=132;
	//cam1Top=100;
	cam1Width=1024;
	cam1Height=668;
//	cam1Width = 512;
//	cam1Height = 334;

	//Camera 2 AOI initialization
	cam2Left=324;
	//cam2Top=300;//360
	cam2Width=640;
	cam2Height=224; //192   224
//	cam2Width = 320;
//	cam2Height = 112; //192   224

	//Camera 3 AOI initialization
	cam3Left=324;
	//cam3Top=300;
	cam3Width=640;
	cam3Height=224;
//	cam3Width = 320;
//	cam3Height = 112;

	cam2diff=0;
	cam3diff=0;

	//tempSelect=1;//Default is the PLC based temp sensor
	USB_Temperature = 0;
	PLC_Temperature = 0;

	DidTriggerOnce=false;
//	m_pMod0 = NULL;
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
	//	AfxMessageBox(IDP_OLE_INIT_FAILED);
/*			pframe->prompt_code=20;
			Prompt3 promtp3;
			promtp3.DoModal();
	*/
		return FALSE;
	}

	//####
	//The following variables are for running the app when No Serial and No camera is available
	//NoSerialComm=true;
	NoSerialComm=false;

#ifdef _DEBUG
	NoSerialComm = true;
#endif

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Silgan Closures"));

	SavedTotalJobs=GetProfileInt("Jobs", "Current", 50);
//SavedTotalJobs=50;
	char folderName[255];
	for(int i=1; i<=SavedTotalJobs; i++)
	{
		sprintf(folderName, "JobArray\\Job%03d", i);
		jobinfo[i].jobname=GetProfileString(folderName, "jobname", jobinfo[i].jobname);
	}

	//ReadRegistery();
	ReadRegistery(0);
	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_CAPTYPE,
		RUNTIME_CLASS(CCapDoc),
		RUNTIME_CLASS(C3WaySplitterFrame), // custom MDI child frame
		RUNTIME_CLASS(CCapView));
	AddDocTemplate(pDocTemplate);

	// Connect the COleTemplateServer to the document template.
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template.
	m_server.ConnectTemplate(clsid, pDocTemplate, FALSE);

	// Register all OLE server factories as running.  This enables the
	//  OLE libraries to create objects from other applications.////
	COleTemplateServer::RegisterAll();
		// Note: MDI applications register all server objects without regard
		//  to the /Embedding or /Automation on the command line.

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Check to see if launched as OLE server
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// Application was run with /Embedding or /Automation.  Don't show the
		//  main window in this case.
		return TRUE;
	}

//	IFCInit();
	// Initialize the IFC interrupts objects
	
//	if(m_pMod0 != NULL)
//	{
//		InitializeIfcIntr();
//	}
	// When a server application is launched stand-alone, it is a good idea
	//  to update the system registry in case it has been damaged.
	m_server.UpdateRegistry(OAT_DISPATCH_OBJECT);
	COleObjectFactory::UpdateRegistryAll();

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}



//////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CString	m_version;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnButton1();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	m_version = _T("");
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Text(pDX, IDC_Version, m_version);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCapApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCapApp message handlers


int CCapApp::ExitInstance() 
{
	
//	SaveDataToReg();
	
	return CWinApp::ExitInstance();
}

//Read job settings from the registry.
void CCapApp::ReadRegistery2()
{
	SavedDelay=GetProfileInt("Delay", "Current", 100);
	SavedThresh=GetProfileInt("Thresh", "Current", 150);
	SavedWidth=GetProfileInt("Width", "Current", 70);
	
	SavedSetup=GetProfileInt("Setup", "Current", 0);
	SavedLimits=GetProfileInt("Limits", "Current", 0);
	SavedEject=GetProfileInt("SEject", "Current", 1);
	SavedModify=GetProfileInt("Modify", "Current", 0);
	SavedJob=GetProfileInt("Job", "Current", 0);
	SavedFillH=GetProfileInt("FillH", "Current", 10);
	SavedFillW=GetProfileInt("FillW", "Current", 10);
	SavedTBH=GetProfileInt("TBH", "Current", 10);
	SavedTB2H=GetProfileInt("TB2H", "Current", 10);
	
	SavedBlack=GetProfileInt("black", "Current", 1);
	SavedBottleSensor=GetProfileInt("botdn", "Current", 0);
	SavedEncSim=GetProfileInt("EncSim", "Current", 0);
	SavedLeftSWin=GetProfileInt("LWin", "Current", 300);
	SavedRightSWin=GetProfileInt("RWin", "Current", 300);
	
	SavedPhotoeye=GetProfileInt("Photo", "Current", 0);	
	SavedLightLevel=GetProfileInt("Level", "Current", 50);

	SavedJ1Name=GetProfileString("Job1Name", "Current","Job1");
	SavedJ2Name=GetProfileString("Job2Name", "Current","Job2");
	SavedJ3Name=GetProfileString("Job3Name", "Current","Job3");
	SavedJ4Name=GetProfileString("Job4Name", "Current","Job4");
	SavedJ5Name=GetProfileString("Job5Name", "Current","Job5");
	SavedJ6Name=GetProfileString("Job6Name", "Current","Job6");
	SavedJ7Name=GetProfileString("Job7Name", "Current","Job7");
	SavedJ8Name=GetProfileString("Job8Name", "Current","Job8");
	SavedJ9Name=GetProfileString("Job9Name", "Current","Job9");
	SavedJ10Name=GetProfileString("Job10Name", "Current","Job10");

	SavedD1Name=GetProfileString("D1Name", "Current","DB1");
	SavedD2Name=GetProfileString("D2Name", "Current","DB2");
	SavedD3Name=GetProfileString("D3Name", "Current","DB3");
	SavedD4Name=GetProfileString("D4Name", "Current","DB4");
	SavedD5Name=GetProfileString("D5Name", "Current","DB5");
	SavedD6Name=GetProfileString("D6Name", "Current","DB6");
	SavedD7Name=GetProfileString("D7Name", "Current","DB7");
	SavedD8Name=GetProfileString("D8Name", "Current","DB8");
	SavedD9Name=GetProfileString("D9Name", "Current","DB9");
	SavedD10Name=GetProfileString("D10Name", "Current","DB10");	
}

void CCapApp::SaveDataToReg()
{
	

}

void CCapApp::DeleteKey(int JobNum)
{
	CString job;
	job.Format("Job%03d", JobNum);
	DelRegTree(GetSectionKey("JobArray"), job);
}

void CCapApp::ReadRegistery(int JobNum)
{
	CString min;
	CString max;
	
	char inspnum[10];
	char folderName[255];
	jobMutex.lock();

	//for (int i=1; i<=10; i++) 
	//{
		sprintf(folderName, "JobArray\\Job%03d", JobNum);
		jobinfo[JobNum].jobname=GetProfileString(folderName, "jobname", jobinfo[JobNum].jobname);
		
		jobinfo[JobNum].taught=GetProfileInt(folderName,"taught",jobinfo[JobNum].taught);

		if(jobinfo[JobNum].taught==1)
		{
			jobinfo[JobNum].lightLevel1=GetProfileInt(folderName,"lightLevel1",jobinfo[JobNum].lightLevel1);	
			jobinfo[JobNum].lightLevel2=GetProfileInt(folderName,"lightLevel2",jobinfo[JobNum].lightLevel2);	
			jobinfo[JobNum].lightLevel3=GetProfileInt(folderName,"lightLevel3",jobinfo[JobNum].lightLevel3);	

			jobinfo[JobNum].is_diet_coke=GetProfileInt(folderName,"is_diet_coke",jobinfo[JobNum].is_diet_coke); //specifically for diet coke
			
			jobinfo[JobNum].vertBarX=GetProfileInt(folderName, "vertbarx", jobinfo[JobNum].vertBarX);
			jobinfo[JobNum].midBarY=GetProfileInt(folderName, "midbary", jobinfo[JobNum].midBarY);
			jobinfo[JobNum].neckBarY=GetProfileInt(folderName, "neckbary", jobinfo[JobNum].neckBarY);

			jobinfo[JobNum].lDist=GetProfileInt(folderName, "lDist", jobinfo[JobNum].lDist);
			if(jobinfo[JobNum].lDist>=350) jobinfo[JobNum].lDist=350;//100
			if(jobinfo[JobNum].lDist<=0) jobinfo[JobNum].lDist=0;

			jobinfo[JobNum].tBandX=GetProfileInt(folderName, "tBandX", jobinfo[JobNum].tBandX);
			if(jobinfo[JobNum].tBandX>=500) jobinfo[JobNum].tBandX=500;
			if(jobinfo[JobNum].tBandX<=0) jobinfo[JobNum].tBandX=0;

			jobinfo[JobNum].tBandY=GetProfileInt(folderName, "tBandY", jobinfo[JobNum].tBandY);
			if(jobinfo[JobNum].tBandY>=500) jobinfo[JobNum].tBandY=500;
			if(jobinfo[JobNum].tBandY<=0) jobinfo[JobNum].tBandY=0;

			jobinfo[JobNum].tBandDx=GetProfileInt(folderName, "tBandDx", jobinfo[JobNum].tBandDx);
			if(jobinfo[JobNum].tBandDx>=450) jobinfo[JobNum].tBandDx=450;
			if(jobinfo[JobNum].tBandDx<=0) jobinfo[JobNum].tBandDx=0;
			
			jobinfo[JobNum].tBandDy=GetProfileInt(folderName, "tBandDy", jobinfo[JobNum].tBandDy);
			if(jobinfo[JobNum].tBandDy>=50) jobinfo[JobNum].tBandDy=50;
			if(jobinfo[JobNum].tBandDy<=0) jobinfo[JobNum].tBandDy=0;

			jobinfo[JobNum].tBandThresh=GetProfileInt(folderName, "tBandThresh", jobinfo[JobNum].tBandThresh);
			jobinfo[JobNum].tBandSurfThresh=GetProfileInt(folderName, "tBandSurfThresh", jobinfo[JobNum].tBandSurfThresh);
			jobinfo[JobNum].tBandSurfThresh2=GetProfileInt(folderName, "tBandSurfThresh2", jobinfo[JobNum].tBandSurfThresh2);

			jobinfo[JobNum].tBand2X=GetProfileInt(folderName, "tBand2X", jobinfo[JobNum].tBand2X);
			jobinfo[JobNum].tBand2Y=GetProfileInt(folderName, "tBand2Y", jobinfo[JobNum].tBand2Y);
			jobinfo[JobNum].tBand2Dx=GetProfileInt(folderName, "tBand2Dx", jobinfo[JobNum].tBand2Dx);

			jobinfo[JobNum].rearCamDy=GetProfileInt(folderName, "rearCamDy", jobinfo[JobNum].rearCamDy);
			if(jobinfo[JobNum].rearCamDy>=50) jobinfo[JobNum].rearCamDy=50;
			if(jobinfo[JobNum].rearCamDy<=0) jobinfo[JobNum].rearCamDy=0;

			jobinfo[JobNum].rearCamY=GetProfileInt(folderName, "rearCamY", jobinfo[JobNum].rearCamY);
			if(jobinfo[JobNum].rearCamY>=300) jobinfo[JobNum].rearCamY=300;
			if(jobinfo[JobNum].rearCamY<=0) jobinfo[JobNum].rearCamY=0;

			jobinfo[JobNum].lSWin=GetProfileInt(folderName, "lSWin", jobinfo[JobNum].lSWin);
			if(jobinfo[JobNum].lSWin>=150) jobinfo[JobNum].lSWin=150;
			if(jobinfo[JobNum].lSWin<=0) jobinfo[JobNum].lSWin=0;

			jobinfo[JobNum].is_NeckBarY_offset_negative=GetProfileInt(folderName, "is_NeckBarY_offset_negative", 0);
			
			jobinfo[JobNum].neckBarY_offset=GetProfileInt(folderName, "neckBarY_offset", 0);
			if(jobinfo[JobNum].is_NeckBarY_offset_negative == 1) {jobinfo[JobNum].neckBarY_offset =jobinfo[JobNum].neckBarY_offset*(-1);}
	
			//	if(jobinfo[JobNum].neckBarY_offset < 0) jobinfo[JobNum].neckBarY_offset=0;

			jobinfo[JobNum].taught_cap_width=GetProfileInt(folderName, "taught_cap_width", 300);
	
			///////////////
			jobinfo[JobNum].Rear_ht_Diff=GetProfileInt(folderName, "Rear_ht_Diff",12);
			////////////////////////
			jobinfo[JobNum].enable_foil_sensor=GetProfileInt(folderName, "enable_foil_sensor",0);

			
			jobinfo[JobNum].cam1_ul=GetProfileInt(folderName, "cam1_ul",220);
			jobinfo[JobNum].cam1_ll=GetProfileInt(folderName, "cam1_ll",100);

			jobinfo[JobNum].cam2_ul=GetProfileInt(folderName, "cam2_ul",220);
			jobinfo[JobNum].cam2_ll=GetProfileInt(folderName, "cam2_ll",100);


			jobinfo[JobNum].cam3_ul=GetProfileInt(folderName, "cam3_ul",220);
			jobinfo[JobNum].cam3_ll=GetProfileInt(folderName, "cam3_ll",100);

			jobinfo[JobNum].do_sobel=GetProfileInt(folderName, "do_sobel",0);
			jobinfo[JobNum].etb_cam1=GetProfileInt(folderName, "etb_cam1",0);
			jobinfo[JobNum].etb_cam2=GetProfileInt(folderName, "etb_cam2",0);
			jobinfo[JobNum].etb_cam3=GetProfileInt(folderName, "etb_cam3",0);

			jobinfo[JobNum].bottleReferenceBar=GetProfileInt(folderName, "bottlereferencebar", 50);

			jobinfo[JobNum].bottleReferenceBar_cam2=GetProfileInt(folderName, "bottleReferenceBar_cam2", 20);
			jobinfo[JobNum].bottleReferenceBar_cam3=GetProfileInt(folderName, "bottleReferenceBar_cam3", 20);
			///////////////
			jobinfo[JobNum].rSWin=GetProfileInt(folderName, "rSWin", jobinfo[JobNum].rSWin);
			if(jobinfo[JobNum].rSWin>=150) jobinfo[JobNum].rSWin=150;
			if(jobinfo[JobNum].rSWin<=0) jobinfo[JobNum].rSWin=0;

			jobinfo[JobNum].is_fillX_neg=GetProfileInt(folderName, "is_fillX_neg", 0);

		
 
			jobinfo[JobNum].fillX=GetProfileInt(folderName, "fillX",0 );//jobinfo[JobNum].fillX

			if(jobinfo[JobNum].is_fillX_neg == 1)
			{
				if(jobinfo[JobNum].fillX >0){	jobinfo[JobNum].fillX=jobinfo[JobNum].fillX* (-1);}
			
			}
			if(jobinfo[JobNum].fillX>=250) jobinfo[JobNum].fillX=250;
		//	if(jobinfo[JobNum].fillX<=0) jobinfo[JobNum].fillX=0;

		
			
			jobinfo[JobNum].fillY=GetProfileInt(folderName, "fillY", jobinfo[JobNum].fillY);
			if(jobinfo[JobNum].fillY>=400) jobinfo[JobNum].fillY=400;
			if(jobinfo[JobNum].fillY<=0) jobinfo[JobNum].fillY=0;

			jobinfo[JobNum].userStrength=GetProfileInt(folderName, "userStrength", jobinfo[JobNum].userStrength);
			
			jobinfo[JobNum].userX=GetProfileInt(folderName, "userX", jobinfo[JobNum].userX);
			if(jobinfo[JobNum].userX>=250) jobinfo[JobNum].userX=250;
			if(jobinfo[JobNum].userX<=-250) jobinfo[JobNum].userX=-250;

			jobinfo[JobNum].userY=GetProfileInt(folderName, "userY", jobinfo[JobNum].userY);
			if(jobinfo[JobNum].userY>=400) jobinfo[JobNum].userY=400;
			if(jobinfo[JobNum].userY<=0) jobinfo[JobNum].userY=0;

			jobinfo[JobNum].userDx=GetProfileInt(folderName, "userDx", jobinfo[JobNum].userDx);
			if(jobinfo[JobNum].userDx>=60) jobinfo[JobNum].userDx=60;
			if(jobinfo[JobNum].userDx<=0) jobinfo[JobNum].userDx=0;

			jobinfo[JobNum].active=GetProfileInt(folderName, "active", jobinfo[JobNum].active);
			jobinfo[JobNum].u2Pos=GetProfileInt(folderName, "u2Pos", jobinfo[JobNum].u2Pos);

			jobinfo[JobNum].u2Thresh2=GetProfileInt(folderName, "u2Thresh2", jobinfo[JobNum].u2Thresh2);
			jobinfo[JobNum].u2Thresh1=GetProfileInt(folderName, "u2Thresh1", jobinfo[JobNum].u2Thresh1);
			
			jobinfo[JobNum].u2XOffset=GetProfileInt(folderName, "u2XOffset", jobinfo[JobNum].u2XOffset);
			if(jobinfo[JobNum].u2XOffset<=0) jobinfo[JobNum].u2XOffset=0;
			//if(jobinfo[JobNum].u2XOffset>=100) jobinfo[JobNum].u2XOffset=100;

			jobinfo[JobNum].u2YOffset=GetProfileInt(folderName, "u2YOffset", jobinfo[JobNum].u2YOffset);
			if(jobinfo[JobNum].u2YOffset<=0) jobinfo[JobNum].u2YOffset=0;
			//if(jobinfo[JobNum].u2YOffset>=100) jobinfo[JobNum].u2YOffset=100;

			jobinfo[JobNum].u2XOffsetR=GetProfileInt(folderName, "u2XOffsetR", jobinfo[JobNum].u2XOffsetR);
			if(jobinfo[JobNum].u2XOffsetR<=0) jobinfo[JobNum].u2XOffsetR=0;
			//if(jobinfo[JobNum].u2XOffsetR>=100) jobinfo[JobNum].u2XOffsetR=100;

			jobinfo[JobNum].u2YOffsetR=GetProfileInt(folderName, "u2YOffsetR", jobinfo[JobNum].u2YOffsetR);
			if(jobinfo[JobNum].u2YOffsetR<=0) jobinfo[JobNum].u2YOffsetR=0;
			//if(jobinfo[JobNum].u2YOffsetR>=100) jobinfo[JobNum].u2YOffsetR=100;

			jobinfo[JobNum].profile=GetProfileInt(folderName, "profile", jobinfo[JobNum].profile);

			jobinfo[JobNum].tBandType=GetProfileInt(folderName, "tBandType", jobinfo[JobNum].tBandType);
			jobinfo[JobNum].sensitivity=GetProfileInt(folderName, "sensitivity", jobinfo[JobNum].sensitivity);

			jobinfo[JobNum].enable=GetProfileInt(folderName, "enable", jobinfo[JobNum].enable);
			jobinfo[JobNum].dark=GetProfileInt(folderName, "dark", jobinfo[JobNum].dark);
			//jobinfo[JobNum].type=GetProfileInt(folderName, "type", jobinfo[JobNum].type);

			jobinfo[JobNum].motPos=GetProfileInt(folderName, "motPos", jobinfo[JobNum].motPos);
			jobinfo[JobNum].lip=GetProfileInt(folderName, "lip", jobinfo[JobNum].lip);
			jobinfo[JobNum].lipx=GetProfileInt(folderName, "lipx", jobinfo[JobNum].lipx);
			jobinfo[JobNum].lipy=GetProfileInt(folderName, "lipy", jobinfo[JobNum].lipy);
			jobinfo[JobNum].lipYOffset=GetProfileInt(folderName, "lipyoffset", jobinfo[JobNum].lipYOffset);
			jobinfo[JobNum].lipSize=GetProfileInt(folderName, "lipSize", jobinfo[JobNum].lipSize);
			jobinfo[JobNum].totinsp=10;

			jobinfo[JobNum].rBandOffset[1]=GetProfileInt(folderName, "rBandOffset1",0 ); //jobinfo[JobNum].rBandOffset[1]   //<-- Replaced with 0 on 12/10/2014 
			jobinfo[JobNum].lBandOffset[1]=GetProfileInt(folderName, "lBandOffset1",0 );//jobinfo[JobNum].lBandOffset[1]
			jobinfo[JobNum].rBandOffset[2]=GetProfileInt(folderName, "rBandOffset2",0);//jobinfo[JobNum].rBandOffset[2]
			jobinfo[JobNum].lBandOffset[2]=GetProfileInt(folderName, "lBandOffset2",0);//jobinfo[JobNum].lBandOffset[2]
			jobinfo[JobNum].rBandOffset[3]=GetProfileInt(folderName, "rBandOffset3",0);//jobinfo[JobNum].rBandOffset[3]
			jobinfo[JobNum].lBandOffset[3]=GetProfileInt(folderName, "lBandOffset3",0);//jobinfo[JobNum].lBandOffset[3]
			jobinfo[JobNum].rBandOffset[4]=GetProfileInt(folderName, "rBandOffset4",0);//jobinfo[JobNum].rBandOffset[4]
			jobinfo[JobNum].lBandOffset[4]=GetProfileInt(folderName, "lBandOffset4",0);//jobinfo[JobNum].lBandOffset[4]
			jobinfo[JobNum].rBandOffset[5]=GetProfileInt(folderName, "rBandOffset5",0);//jobinfo[JobNum].rBandOffset[5]
			jobinfo[JobNum].lBandOffset[5]=GetProfileInt(folderName, "lBandOffset5",0);//jobinfo[JobNum].lBandOffset[5]

			jobinfo[JobNum].rearSurfThresh=GetProfileInt(folderName, "rearsurfthresh", jobinfo[JobNum].rearSurfThresh);
			jobinfo[JobNum].capWidthLimit=GetProfileInt(folderName, "capwidthlimit", jobinfo[JobNum].capWidthLimit);
			jobinfo[JobNum].colorTargR=GetProfileInt(folderName,"colortargr", 0);
			jobinfo[JobNum].colorTargG=GetProfileInt(folderName,"colortargg", 0);
			jobinfo[JobNum].colorTargB=GetProfileInt(folderName,"colortargb", 0);
			jobinfo[JobNum].rightBandShift=GetProfileInt(folderName,"rightbandshift", 10);
			jobinfo[JobNum].leftBandShift=GetProfileInt(folderName,"leftbandshift", 10);
			jobinfo[JobNum].band2Size=GetProfileInt(folderName,"band2size", 200);
			jobinfo[JobNum].bandColor=GetProfileInt(folderName,"bandcolor", 1);
			jobinfo[JobNum].sport=GetProfileInt(folderName,"sport", 0);
			jobinfo[JobNum].userType=GetProfileInt(folderName,"usertype", 1);
			jobinfo[JobNum].neckOffset=GetProfileInt(folderName,"neckoffset", 20);
			jobinfo[JobNum].ringLight=GetProfileInt(folderName,"ringLight", 20);
			jobinfo[JobNum].backLight=GetProfileInt(folderName,"backLight", 20);
			jobinfo[JobNum].sportPos=GetProfileInt(folderName,"sportcappos", 90);
			jobinfo[JobNum].sportSen=GetProfileInt(folderName,"sportcapsen", 50);
			jobinfo[JobNum].xtraLight=GetProfileInt(folderName,"xtralight", 0);
			jobinfo[JobNum].redCutOff=GetProfileInt(folderName,"redcutoff", 50);
			jobinfo[JobNum].fillArea=GetProfileInt(folderName,"fillarea", 200);
			jobinfo[JobNum].snapDelay=GetProfileInt(folderName,"snapdelay", 270);
			jobinfo[JobNum].slatPW=GetProfileInt(folderName,"slatpw", 10);
			jobinfo[JobNum].encDur=GetProfileInt(folderName,"encdur", 40);
			jobinfo[JobNum].encDist=GetProfileInt(folderName,"encdist", 987);

			jobinfo[JobNum].diff_for_no_cap=GetProfileInt(folderName,"diff_for_no_cap", 1);
			jobinfo[JobNum].limit_no_cap=GetProfileInt(folderName,"limit_no_cap", 40);

		

			jobinfo[JobNum].useLeft=GetProfileInt(folderName,"useleft", 0);
			jobinfo[JobNum].useRing=GetProfileInt(folderName,"usering", 1);

			jobinfo[JobNum].ScaleFactor=GetProfileInt(folderName,"scalefactor", 0);
			
			jobinfo[JobNum].totinsp=10;

			inspctn[1].name="Height Left";
			inspctn[2].name="Height Right";
			inspctn[3].name="Height Mid";
			inspctn[4].name="Cocked";
			inspctn[5].name="Band Edges";
			
			inspctn[6].name="Band Surface";
			inspctn[7].name="Low Fill Level";
			inspctn[8].name="Color";
			inspctn[9].name="User";
			inspctn[10].name="User";

			inspctn[1].type=2;
			inspctn[2].type=2;
			inspctn[3].type=2;
			inspctn[4].type=2;
			inspctn[5].type=2;
			inspctn[6].type=2;
			inspctn[7].type=2;
			inspctn[8].type=2;
			inspctn[9].type=2;
			inspctn[10].type=2;
			
			std::string abc(folderName);

			for (int CurInspctn=1; CurInspctn<=10; CurInspctn++)
			{			
				sprintf(inspnum, "\\Insp%03d", CurInspctn);
			//	strcat(folderName,inspnum);

				abc=abc+inspnum;

				inspctn[CurInspctn].lmin=GetProfileInt(abc.c_str(), "lmin", 0);
				if(inspctn[CurInspctn].lmin>=5000) inspctn[CurInspctn].lmin=100;

				inspctn[CurInspctn].lmax=GetProfileInt(abc.c_str(), "lmax", 100);
				if(inspctn[CurInspctn].lmax>=5000) inspctn[CurInspctn].lmax=100;

				inspctn[CurInspctn].enable=GetProfileInt(abc.c_str(), "enable", 0);

				int n=abc.length();
				abc=abc.substr(0,n-8);

				
			
			}
			inspctn[10].lmin=inspctn[9].lmin;
			inspctn[10].lmax=inspctn[9].lmax;
			if(inspctn[9].enable==true)inspctn[10].enable=true;
			if(inspctn[9].enable==false)inspctn[10].enable=false;

		}
		else
		{
			inspctn[1].name="Height Left";
			inspctn[2].name="Height Right";
			inspctn[3].name="Height Mid";
			inspctn[4].name="Cocked";
			inspctn[5].name="Band Edges";
			
			inspctn[6].name="Band Surface";
			inspctn[7].name="Low Fill Level";
			inspctn[8].name="Color";
			inspctn[9].name="User";
			inspctn[10].name="User";
			
			for(int g=1; g<=5; g++) {jobinfo[JobNum].rBandOffset[g]=0; jobinfo[JobNum].lBandOffset[g]=0;}

			jobinfo[JobNum].capWidthLimit=40;
			jobinfo[JobNum].rearSurfThresh=40;
			jobinfo[JobNum].lightLevel1=20;	
			jobinfo[JobNum].lightLevel2=20;	
			jobinfo[JobNum].lightLevel3=20;	
			
			jobinfo[JobNum].vertBarX=340;
			jobinfo[JobNum].midBarY=120;
			jobinfo[JobNum].neckBarY=240;

			///////////////
			jobinfo[JobNum].Rear_ht_Diff=12;

			jobinfo[JobNum].is_diet_coke=0;
			jobinfo[JobNum].diff_for_no_cap=1;
			jobinfo[JobNum].limit_no_cap=40;

			jobinfo[JobNum].snapDelay=290;
			jobinfo[JobNum].taught_cap_width=300;
			////////////////////////

			jobinfo[JobNum].do_sobel=0;
			jobinfo[JobNum].etb_cam1=0;
			jobinfo[JobNum].etb_cam2=0;
			jobinfo[JobNum].etb_cam3=0;
			jobinfo[JobNum].sportPos=90;

			jobinfo[JobNum].lDist=20;
			jobinfo[JobNum].tBandX=170;
			jobinfo[JobNum].tBandY=260;
			jobinfo[JobNum].tBandDx=100;
			jobinfo[JobNum].tBandDy=20;
			jobinfo[JobNum].tBandThresh=150;
			jobinfo[JobNum].tBandSurfThresh=150;
			jobinfo[JobNum].tBandSurfThresh2=150;

			jobinfo[JobNum].neckBarY_offset=0;
			jobinfo[JobNum].is_NeckBarY_offset_negative=0;//false

			jobinfo[JobNum].tBand2X=50;
			jobinfo[JobNum].tBand2Y=0;
			jobinfo[JobNum].tBand2Dx=10;
			jobinfo[JobNum].band2Size=200;

			jobinfo[JobNum].rearCamDy=10;
			jobinfo[JobNum].rearCamY=150;

			jobinfo[JobNum].lSWin=150;
			jobinfo[JobNum].rSWin=10;

			jobinfo[JobNum].fillX=0;
			jobinfo[JobNum].fillY=250;
			jobinfo[JobNum].is_fillX_neg=0;

			jobinfo[JobNum].userStrength=150;
			jobinfo[JobNum].userX=0;

			jobinfo[JobNum].userY=150;
			jobinfo[JobNum].userDx=20;

			jobinfo[JobNum].active=1;
			jobinfo[JobNum].u2Pos=100;

			jobinfo[JobNum].u2Thresh2=70;
			jobinfo[JobNum].u2Thresh1=80;

			jobinfo[JobNum].profile=257;
			jobinfo[JobNum].lip=0;
			jobinfo[JobNum].lipx=0;
			jobinfo[JobNum].lipy=100;
			jobinfo[JobNum].lipYOffset=0;
			jobinfo[JobNum].lipSize=0;
			jobinfo[JobNum].tBandType=2;
			jobinfo[JobNum].sensitivity=100;

			jobinfo[JobNum].enable=1;
			jobinfo[JobNum].dark=0;

			jobinfo[JobNum].motPos=200;

			jobinfo[JobNum].totinsp=10;
			jobinfo[JobNum].encDur=40;
			jobinfo[JobNum].encDist=987;

			jobinfo[JobNum].cam1_ul=220;
			jobinfo[JobNum].cam1_ll=100;

			jobinfo[JobNum].cam2_ul=220;
			jobinfo[JobNum].cam2_ll=100;

			jobinfo[JobNum].cam3_ul=220;
			jobinfo[JobNum].cam3_ll=100;

			jobinfo[JobNum].bottleReferenceBar=70;
			jobinfo[JobNum].bottleReferenceBar_cam2=20;
			jobinfo[JobNum].bottleReferenceBar_cam3=20;

			jobinfo[JobNum].useLeft=0;
			jobinfo[JobNum].useRing=1;

		
			////filler graph
			jobinfo[JobNum].ScaleFactor=GetProfileInt(folderName,"ScaleFactor", 0);
			/////
				std::string abc(folderName);
			for (int CurInspctn=1; CurInspctn<=jobinfo[JobNum].totinsp; CurInspctn++)
			{
				
				sprintf(inspnum, "\\Insp%03d", CurInspctn);
				strcat(folderName,inspnum);

				abc=abc+inspnum;

				inspctn[CurInspctn].lmin=0;
				inspctn[CurInspctn].lmax=0;
				if(CurInspctn==1 || CurInspctn==2 || CurInspctn==3) inspctn[CurInspctn].lmin=250;
				if(CurInspctn==1 || CurInspctn==2 || CurInspctn==3) inspctn[CurInspctn].lmax=300;
				if(CurInspctn==4) inspctn[CurInspctn].lmax=10;
				if(CurInspctn==5) inspctn[CurInspctn].lmax=10;
				if(CurInspctn==6) inspctn[CurInspctn].lmax=50;
				if(CurInspctn==7) inspctn[CurInspctn].lmax=150;
				if(CurInspctn==8) inspctn[CurInspctn].lmax=30;
				if(CurInspctn==9) inspctn[CurInspctn].lmax=10;
				
				inspctn[CurInspctn].name=GetProfileString(abc.c_str(), "name", inspctn[CurInspctn].name);
				int n=abc.length();
				abc=abc.substr(0,n-8);

			}

		
			SavedEjectDisable=1;
			SavedPassWord="114031";
			SavedIORejects=0;
			SavedConRejects=0;
			SavedKillSD=0;
			SavedKillSave=1;
			SavedTBl=0;

			SavedFillH=10;
			SavedFillW=10;

		}
	
		SavedMaster			=GetProfileString("PassMASTER", "Current", "114031");
		SavedOPPassWord		=GetProfileString("PassOP", "Current", "114031");
		SavedMANPassWord	=GetProfileString("PassMAN", "Current", "114031");
		SavedSUPPassWord	=GetProfileString("PassSUP", "Current", "114031");
		SavedEncSim			=GetProfileInt("EncSim", "Current", 0);
		SavedDelay			=GetProfileInt("SavedDelay", "Current", 50);

		cam2_ht_offset		=GetProfileInt("cam2_ht_offset", "Current", 0);
		cam3_ht_offset		=GetProfileInt("cam3_ht_offset", "Current", 0);

		CamSer1				=GetProfileInt("camser1","Current",11111111 );// //11235935
		CamSer2				=GetProfileInt("camser2","Current",10000000 );// //11030139
		CamSer3				=GetProfileInt("camser3","Current",10101010 );// //11030138

		isRed_block_North	=GetProfileInt("isRed_block_North","Current",1 );//default

		checkRearHeight		=GetProfileInt("checkRearHeight", "Current", 0);
		SavedEjectDisable	=GetProfileInt("Eject", "Current", 1);
		SavedPassWord		=GetProfileString("Pass", "Current", "1301");
		SavedIORejects		=GetProfileInt("IORejects", "Current", 0);
		SavedConRejects		=GetProfileInt("ConRejects", "Current", 0);
		SavedKillSD			=GetProfileInt("KillSD", "Current", 0);
		SavedKillSave		=GetProfileInt("KillSave", "Current", 0);
		SavedTBl			=GetProfileInt("TBl", "Current", 0);
		SavedEncSim			=GetProfileInt("EncSim", "Current", 0);
		SavedPhotoeye		=GetProfileInt("Photo", "Current", 0);
		SavedFillH			=GetProfileInt("FillHeight", "Current", 10);
		SavedFillW			=GetProfileInt("FillWidth", "Current", 10);
		SavedForcedEjects	=GetProfileInt("forceejects", "Current", 0);
		SavedDBOffset		=GetProfileInt("dboffset", "Current", 100);
		SavedBottleSensor	=GetProfileInt("botdn", "Current", 0);
		SavedDeadBand		=GetProfileInt("deadband", "Current", 70);
		fillerPulse			=GetProfileInt("fillerpulse", "Current", 100);

		fillerSize			=GetProfileInt("fillersize", "Current", 120);
		capperSize			=GetProfileInt("cappersize", "Current", 32);
		convInches			=GetProfileInt("pocketnum", "Current", 0);

		fillPocketOffset	=GetProfileInt("fillpocketoffset", "Current", 0);
		capPocketOffset		=GetProfileInt("cappocketoffset", "Current", 0);
		cam1Top				=GetProfileInt("cam1top", "Current", 100);
		cam2Top				=GetProfileInt("cam2top", "Current", 300);
		cam3Top				=GetProfileInt("cam3top", "Current", 300);
		tempLimit			=GetProfileInt("templimit", "Current", 130);
		surfRR				=GetProfileInt("surfrr", "Current", 1);
		surfLR				=GetProfileInt("surflr", "Current", 1);
		userRR				=GetProfileInt("userrr", "Current", 1);
		userLR				=GetProfileInt("userlr", "Current", 1);
		check_Rband			=GetProfileInt("check_Rband", "Current", 1); //var for Rear Band Inspection for both cam 2 and 3
		convRatio			=GetProfileInt("convratio", "Current", 1);
		xtraYOffset			=GetProfileInt("xtrayoffset", "Current", 1);

		tempSelect			=GetProfileInt("tempSelect","Current", 2);//1:PLC 2:USB
		
		savepics			=false;
	
		CString shut1;
		
		shut1				=GetProfileString("shuttercam1", "Current", "1.0");
		shutterSpeed		=atof(shut1);
	
		CString shut2;
		
		shut2				=GetProfileString("shuttercam2", "Current", "1.0");
		shutterSpeed2		=atof(shut2);
	
		CString shut3;
		
		shut3				=GetProfileString("shuttercam3", "Current", "1.0");
		shutterSpeed3		=atof(shut3);

////////////////
	//ML-
	//Inspection Delay
	inspDelay=GetProfileInt("inspdelay",	"Current", 30);
	
	//Cam Parameters
	bandwidth1=GetProfileInt("bandwidth1","Current", 62);
	bandwidth2=GetProfileInt("bandwidth2","Current", 62);

	whitebalance1Red=GetProfileInt("whitebalance1Red","Current", 630);
	whitebalance2Red=GetProfileInt("whitebalance2Red","Current", 630);
	whitebalance3Red=GetProfileInt("whitebalance3Red","Current", 630);
	whitebalance1Blue=GetProfileInt("whitebalance1Blue","Current", 500);
	whitebalance2Blue=GetProfileInt("whitebalance2Blue","Current", 500);
	whitebalance3Blue=GetProfileInt("whitebalance3Blue","Current", 500);
	////////////////
	jobMutex.unlock();

}


void CAboutDlg::OnButton1() 
{
	ApplicationHistory app1;
	app1.DoModal();
	
}

void CAboutDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CFont l_font;

	l_font.CreateFont(	16,					//int nHeight
						0,					//int nWidth,
						0,					//int nEscapement
						0,					//int nOrientation 
						FW_BOLD,			//int nWeight, FW_NORMAL
						FALSE,				//BYTE bItalic,
						FALSE,				//BYTE bUnderline,
						FALSE,				//BYTE cStrikeOut
						0,					//BYTE nCharSet
						OUT_DEFAULT_PRECIS,	//BYTE nOutPrecision, 
						CLIP_DEFAULT_PRECIS,//BYTE nClipPrecision,   
						DEFAULT_QUALITY,	//BYTE nQuality,
						DEFAULT_PITCH | FF_ROMAN, //BYTE nPitchAndFamily,
						"Arial"				//LPCTSTR lpszFacename
					);
	CDC *DC = GetDC();
	DC->SelectObject(&l_font);

	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(160, "Arial Bold");
	CStatic * m_Label;
	m_Label = (CStatic *)GetDlgItem(IDC_Version);
	m_Label->SetFont(m_Font1);
//	m_version.SetFont(m_Font1);


	m_version.Format("%s", "51R56G V 5.0"); //Specify the Version number here
	
	// TODO: Add extra initialization here
	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCapApp::DeleteRegKey(int index)
{
	LONG res;

res=	DelRegTree(HKEY_CURRENT_USER,"Software\\Silgan Closures\\Job Array\\Job050\\Insp001");

HKEY key;
RegOpenKeyEx(HKEY_CURRENT_USER, "SOFTWARE\blah", 0, 0, &key);


key1.m_hKey=HKEY_CURRENT_USER;
res=key1.Open(key1.m_hKey,_T("Software\\Silgan Closures\\Cap\\JobArray\\Job050"),KEY_ALL_ACCESS);

res=key1.RecurseDeleteKey(_T("Software\\Silgan Closures\\Cap\\JobArray\\Job050"));

res=key1.DeleteValue(_T("Software\\Silgan Closures\\Cap\\JobArray\\Job050\\Insp001"));
 
//res= RegDeleteTree(HKEY_CURRENT_USER,"Software\\Silgan Closures\\Job Array\\Job050\\Insp001");

/*
if(res!=ERROR_SUCCESS)
{
 AfxMessageBox("Failed to delete Key");
}
*/
key1.Close();

}
