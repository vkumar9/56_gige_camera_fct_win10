// Prompt3.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Prompt3.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Prompt3 dialog


Prompt3::Prompt3(CWnd* pParent /*=NULL*/)
	: CDialog(Prompt3::IDD, pParent)
{
	//{{AFX_DATA_INIT(Prompt3)
	m_prompt = _T("");
	//}}AFX_DATA_INIT
}


void Prompt3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Prompt3)
	DDX_Text(pDX, IDC_prompt, m_prompt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Prompt3, CDialog)
	//{{AFX_MSG_MAP(Prompt3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Prompt3 message handlers

/*
1--> Password Protected
2-->User not enabled from Limits menu!
3-->The file specified could not be opened.

4-->The file specified could not be opened. load image
5-->The file specified could not be opened. You will need to setup a new job
6-->The pattern file could not be saved.
7-->Please Turn ON Enhanced TB inspection from Limits Tab

8-->The file could not be saved.
9-->The file specified could not be opened. Do you have enough rejects yet?
10-->The serial port could not be opened.
11->The file could Not be saved. 
12-->Need to Select Something from the Drop Down Before you can Reset
13-->Please login with a minimum of the Filler Operator level password
14-->Please login with a minimum of the Label Operator level password
15-->Select a Job first
16-->This job has not been taught
17-->Please select a filler head first
18-->Please select a capper head first
19-->The Source file specified could not be opened. No saved Database?
20-->IDP_OLE_INIT_FAILED
21-->failed to create bus callback
22-->The grab thread has encountered a problem and needs to close
23-->failed to convert camera 3
24-->failed to convert camera 2
25-->failed to get trigger camera 1
26-->failed to set trigger camera 1
27-->REJECT ALL is enabled from External I\/O. \n\t Please disable that to continue
28-->Pictures have been saved to C:\Silgan Data \/Defective Pics 
29-->You need to choose a job before you click <Save As>!

  */
BOOL Prompt3::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	// TODO: Add extra initialization here

	switch(pframe->prompt_code)
	{

	case 1:
		m_prompt.Format("\t \t%s", " !!!  Password Protected !!!");
		UpdateData(false);
		break;
	
	case 2:
		m_prompt.Format("\t \t%s", " !!!  User not enabled from Limits menu!!!!");
		UpdateData(false);
		break;

	case 3:
		m_prompt.Format("\t \t%s", " !!!  The file specified could not be opened !!!");
		UpdateData(false);
		break;

	case 4:
		m_prompt.Format("\t \t%s", " !!! The file specified could not be opened. load image !!!");
		UpdateData(false);
		break;

	case 5:
		m_prompt.Format(" %s", " !!! The file specified could not be opened. You will need to setup a new job !!!");
		UpdateData(false);
		break;

	case 6:
		m_prompt.Format("\t \t%s", "!!! The file specified could not be opened. load image !!!");
		UpdateData(false);
		break;

	case 7:
		m_prompt.Format("%s", " !!!The file specified could not be opened. You will need to setup a new job!!!");
		UpdateData(false);
		break;

	case 8:
		m_prompt.Format("\t \t%s", " !!!The file could not be saved!!!");
		UpdateData(false);
		break;
	case 9:
		m_prompt.Format("%s", " !!!The file specified could not be opened. Do you have enough rejects yet?!!!");
		UpdateData(false);
		break;

	case 10:
		m_prompt.Format("\t \t%s", " !!! The serial port could not be opened.   !!!");
		UpdateData(false);
		break;
	
	case 11:
		m_prompt.Format("\t \t%s", " !!! The file could not be saved   !!!");
		UpdateData(false);
		break;

	case 12:
		m_prompt.Format("%s", " !!! Need to select something from the drop down list before you can Reset  !!!");
		UpdateData(false);
		break;


	case 13:
		m_prompt.Format("%s", " !!! Please login with a minimum of the Filler Operator level password !!!");
		UpdateData(false);
		break;
	
	case 14:
		m_prompt.Format("%s", " !!! Please login with a minimum of the Label Operator level password !!!");
		UpdateData(false);
		break;

		
	case 15:
		m_prompt.Format("\t \t%s", " !!! Select a Job first !!!");
		UpdateData(false);
		break;
	case 16:
		m_prompt.Format("\t \t%s", " !!! This job has not been taught !!!");
		UpdateData(false);
		break;
	case 17:
		m_prompt.Format("\t \t%s", " !!! Please select a filler head first !!!");
		UpdateData(false);
		break;
	case 18:
		m_prompt.Format("\t \t%s", " !!! Please select a capper head first !!!");
		UpdateData(false);
		break;
	
	case 19:
		m_prompt.Format("\t%s", " !!! The Source file specified could not be opened. No saved Database? !!!");
		UpdateData(false);
		break;

	case 20:
		m_prompt.Format("\t \t%s", " !!! IDP_OLE_INIT_FAILED   !!!");
		UpdateData(false);
		break;

	case 21:
		m_prompt.Format("\t \t%s", " !!! 	failed to create bus callback   !!!");
		UpdateData(false);
		break;

	case 22:
		m_prompt.Format("\t%s", " !!! The grab thread has encountered a problem and had to terminate  !!!");
		UpdateData(false);
		break;
	case 23:
		m_prompt.Format("\t \t%s", " !!! Failed to convert camera 3  !!!");
		UpdateData(false);
		break;
	case 24:
		m_prompt.Format("\t \t%s", " !!! Failed to convert camera 2  !!!");
		UpdateData(false);
		break;
	case 25:
		m_prompt.Format("\t \t%s", " !!! Failed to get trigger for camera 1  !!!");
		UpdateData(false);
		break;
	case 26:
		m_prompt.Format("\t \t%s", " !!! Failed to set trigger for camera 1  !!!");
		UpdateData(false);
		break;

	case 27:
		m_prompt.Format("%s", " !! REJECT ALL is enabled from External I\/O. \n\t Please disable that to continue  !!");
		UpdateData(false);
		break;

	case 28:
		m_prompt.Format("%s ", " !! Pictures have been saved to C:\/Silgan Data \/Defective Pics  !!");
		UpdateData(false);
		break;
	
	case 29:
		m_prompt.Format("%s ", " !! You need to choose a job before you click <Save As>!  !!");
		UpdateData(false);
		break;


	
	default:
		m_prompt.Format("\t \t%s", " !!!  Invalid Operation !!!");
		UpdateData(false);
		break;
	}

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Prompt3::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
