#if !defined(AFX_DATA_H__71112D45_C961_4286_9C19_AFFD9C633EC3__INCLUDED_)
#define AFX_DATA_H__71112D45_C961_4286_9C19_AFFD9C633EC3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Data.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Data dialog
class Data : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp; 
public:
	Data(CWnd* pParent = NULL);   // standard constructor
	void WriteData();
	void CreateNewFile();
	CCapApp *theapp;
	CMainFrame* pframe;

	CFile DataFile;		//Used for writing the logged data to a text file.
// Dialog Data
	//{{AFX_DATA(Data)
	enum { IDD = IDD_DATA };
	CString	m_r1;		//Number of bottles with missing caps
	CString	m_r10;		//Total number of bottles rejected when 
	CString	m_r2;		//Number of bottles with cocked caps
	CString	m_r3;		//Number of bottles with loose caps
	CString	m_r4;		//Not used
	CString	m_r5;		//Number of bottles with tamper band edge defects
	CString	m_r6;		//Number of bottles with tamper band surface defects
	CString	m_r7;		//Number of bottles with low fill
	CString	m_r8;		//Number of bottles with wrong color cap
	CString	m_r9;		//Total number of bottles scanned
	CString	m_r11;		//Number of bottles with a sport cap.
	CString	m_24hrtime;	//Time of day when data is to be saved.
	CString	m_foil_count;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Data)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Data)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrint();
	afx_msg void OnButtonearly();
	afx_msg void OnButtonlate();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class FillerData : public CDialog
{
// Construction
public:
	FillerData(CWnd* pParent = NULL);   // standard constructor
	CCapApp *theapp;
	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(FillerData)
	enum { IDD = IDD_FILLERDATA };
	//Variables for the filler heads. Each one shows how many defective bottles where found from the corresponding head.
	CString	m_1;
	CString	m_10;
	CString	m_11;
	CString	m_12;
	CString	m_13;
	CString	m_14;
	CString	m_15;
	CString	m_16;
	CString	m_17;
	CString	m_18;
	CString	m_19;
	CString	m_2;
	CString	m_22;
	CString	m_20;
	CString	m_21;
	CString	m_24;
	CString	m_23;
	CString	m_25;
	CString	m_3;
	CString	m_4;
	CString	m_5;
	CString	m_6;
	CString	m_7;
	CString	m_9;
	CString	m_8;
	CString	m_26;
	CString	m_27;
	CString	m_28;
	CString	m_29;
	CString	m_30;
	CString	m_31;
	CString	m_32;
	CString	m_33;
	CString	m_34;
	CString	m_35;
	CString	m_36;
	CString	m_38;
	CString	m_37;
	CString	m_39;
	CString	m_40;
	CString	m_41;
	CString	m_42;
	CString	m_43;
	CString	m_44;
	CString	m_45;
	CString	m_46;
	CString	m_47;
	CString	m_48;
	CString	m_49;
	CString	m_50;
	CString	m_51;
	CString	m_52;
	CString	m_53;
	CString	m_54;
	CString	m_55;
	CString	m_56;
	CString	m_57;
	CString	m_58;
	CString	m_59;
	CString	m_60;
	CString	m_61;
	CString	m_62;
	CString	m_63;
	CString	m_64;
	CString	m_65;
	CString	m_66;
	CString	m_67;
	CString	m_68;
	CString	m_69;
	CString	m_70;
	CString	m_71;
	CString	m_72;
	CString	m_73;
	CString	m_74;
	CString	m_75;
	CString	m_76;
	CString	m_77;
	CString	m_78;
	CString	m_79;
	CString	m_80;
	CString	m_81;
	CString	m_82;
	
	CString	m_83;
	CString	m_84;
	CString	m_85;
	CString	m_86;
	CString	m_87;
	CString	m_88;
	CString	m_89;
	CString	m_90;
	CString	m_91;
	CString	m_92;
	CString	m_93;
	CString	m_94;
	CString	m_95;
	CString	m_96;
	CString	m_97;
	CString	m_98;
	CString	m_99;
	CString	m_100;
	CString	m_101;
	CString	m_102;
	
	CString	m_103;
	CString	m_104;
	CString	m_105;
	CString	m_106;
	CString	m_107;
	CString	m_108;
	CString	m_109;
	CString	m_110;
	CString	m_111;
	CString	m_112;
	CString	m_113;
	CString	m_114;
	CString	m_115;
	CString	m_116;
	CString	m_117;
	CString	m_118;
	CString	m_119;
	CString	m_120;
	//Variables for the capper heads. Each one shows how many defective bottles where found from the corresponding head.
	CString	m_c1;
	CString	m_c10;
	CString	m_c11;
	CString	m_c12;
	CString	m_c13;
	CString	m_c14;
	CString	m_c15;
	CString	m_c16;
	CString	m_c17;
	CString	m_c18;
	CString	m_c19;
	CString	m_c2;
	CString	m_c20;
	CString	m_c22;
	CString	m_c23;
	CString	m_c24;
	CString	m_c25;
	CString	m_c26;
	CString	m_c27;
	CString	m_c28;
	CString	m_c29;
	CString	m_c30;
	CString	m_c31;
	CString	m_c32;
	CString	m_c3;
	CString	m_c4;
	CString	m_c5;
	CString	m_c6;
	CString	m_c7;
	CString	m_c8;
	CString	m_c9;
	CString	m_c21;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FillerData)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FillerData)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATA_H__71112D45_C961_4286_9C19_AFFD9C633EC3__INCLUDED_)
