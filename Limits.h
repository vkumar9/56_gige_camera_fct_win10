#if !defined(AFX_LIMITS_H__F6DAD754_9801_4A08_8989_82BD57DB37B5__INCLUDED_)
#define AFX_LIMITS_H__F6DAD754_9801_4A08_8989_82BD57DB37B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Limits.h : header file
//
friend class CMainFrame;
friend class CCapApp;
/////////////////////////////////////////////////////////////////////////////
// Limits dialog

class Limits : public CDialog
{
// Construction
public:
	Limits(CWnd* pParent = NULL);   // standard constructor

	int tLHMin;
	int tLHMax;
	int tRHMin;
	int tRHMax;
	int tMHMin;
	int tMHMax;
	int tDMin;
	int tDMax;
	int tTBMin;
	int tTB2Max;
	int tTB2Min;
	int tTBMax;
	int tFillMin;
	int tFillMax;
	int tUMin;
	int tUMax;
	int tU2Min;
	int tU2Max;
// Dialog Data
	//{{AFX_DATA(Limits)
	enum { IDD = IDD_LIMITS };
	CSpinButtonCtrl	m_spin9x;
	CSpinButtonCtrl	m_spin9;
	CSpinButtonCtrl	m_spin8x;
	CSpinButtonCtrl	m_spin8;
	CSpinButtonCtrl	m_spin7x;
	CSpinButtonCtrl	m_spin7;
	CSpinButtonCtrl	m_spin6x;
	CSpinButtonCtrl	m_spin6;
	CSpinButtonCtrl	m_spin5x;
	CSpinButtonCtrl	m_spin5;
	CSpinButtonCtrl	m_spin4x;
	CSpinButtonCtrl	m_spin4;
	CSpinButtonCtrl	m_spin3x;
	CSpinButtonCtrl	m_spin3;
	CSpinButtonCtrl	m_spin2x;
	CSpinButtonCtrl	m_spin2;
	CSpinButtonCtrl	m_spin1x;
	CSpinButtonCtrl	m_spin1;
	//}}AFX_DATA
	CMainFrame* pframe;
	CCapApp* theapp;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Limits)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Limits)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnEnable1();
	afx_msg void OnEnable2();
	afx_msg void OnEnable3();
	afx_msg void OnEnable4();
	afx_msg void OnEnable5();
	afx_msg void OnEnable6();
	afx_msg void OnEnable7();
	afx_msg void OnEnable8();
	afx_msg void OnEnable9();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIMITS_H__F6DAD754_9801_4A08_8989_82BD57DB37B5__INCLUDED_)
