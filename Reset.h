#if !defined(AFX_RESET_H__74EA8276_C8F1_4963_9210_CF98FDB39BB6__INCLUDED_)
#define AFX_RESET_H__74EA8276_C8F1_4963_9210_CF98FDB39BB6__INCLUDED_

#include "MainFrm.h"	// Added by ClassView
#include "Cap.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Reset.h : header file
//
friend class CMainFrame;
friend class CCapApp; 
/////////////////////////////////////////////////////////////////////////////
// Reset dialog

class Reset : public CDialog
{
// Construction
public:
	CCapApp *theapp;
	CMainFrame *pframe;
	Reset(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Reset)
	enum { IDD = IDD_RESETTOTALS };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Reset)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Reset)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESET_H__74EA8276_C8F1_4963_9210_CF98FDB39BB6__INCLUDED_)
