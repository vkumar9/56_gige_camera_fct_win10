// Pics.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Pics.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Pics dialog


Pics::Pics(CWnd* pParent /*=NULL*/)
	: CDialog(Pics::IDD, pParent)
{
	//{{AFX_DATA_INIT(Pics)
	m_editrecall = _T("");
	//}}AFX_DATA_INIT
}


void Pics::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Pics)
	DDX_Text(pDX, IDC_EDITRECALL, m_editrecall);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Pics, CDialog)
	//{{AFX_MSG_MAP(Pics)
	ON_BN_CLICKED(IDC_RECALL, OnRecall)
	ON_BN_CLICKED(IDC_LAST, OnLast)
	ON_BN_CLICKED(IDC_LAST2, OnLast2)
	ON_BN_CLICKED(IDC_LAST3, OnLast3)
	ON_BN_CLICKED(IDC_LAST4, OnLast4)
	ON_BN_CLICKED(IDC_SAVEONE1C, OnSaveone1c)
	ON_BN_CLICKED(IDC_SAVEONE1, OnSaveone1)
	ON_BN_CLICKED(IDC_SAVEONE2C, OnSaveone2c)
	ON_BN_CLICKED(IDC_SAVEONE2, OnSaveone2)
	ON_BN_CLICKED(IDC_SAVEONE3C, OnSaveone3c)
	ON_BN_CLICKED(IDC_SAVEONE3, OnSaveone3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Pics message handlers

BOOL Pics::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_ppics=this;
	theapp=(CCapApp*)AfxGetApp();

	pframe->PicsOpen=true;
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Pics::OnRecall() 
{
	char* m_name;
	char currentpath[60];

	CString m_name2;
	CFileDialog dlg(true,"bmp","c:\\silgandata\\initpic.bmp",OFN_NOCHANGEDIR,NULL,this);

	if(dlg.DoModal()==IDOK)
	{
		m_name2=dlg.GetPathName();
		m_editrecall.Format("%s",m_name2);
		UpdateData(false);
	
		
	    strcpy(currentpath,"");
	    strcat(currentpath,m_name2);
		pframe->m_pxaxis->OnRead(currentpath);
	}
	
}

void Pics::OnOK() 
{
	pframe->m_pxaxis->PreInsp();
	pframe->m_pxaxis->Inspect(pframe->m_pxaxis->im_dataC, pframe->m_pxaxis->im_dataC2, pframe->m_pxaxis->im_dataC3, pframe->m_pxaxis->im_data,
		pframe->m_pxaxis->im_data2,pframe->m_pxaxis->im_data3,pframe->m_pxaxis->sobel_cam2,pframe->m_pxaxis->sobel_cam3, pframe->m_pxaxis->im_red, 
		pframe->m_pxaxis->im_red2, pframe->m_pxaxis->im_red3, pframe->m_pxaxis->im_hue, pframe->m_pxaxis->im_hue2, pframe->m_pxaxis->im_hue3);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->PicsOpen=false;
	CDialog::OnOK();
}

void Pics::OnLast() 
{	
	pframe->m_pxaxis->OnReadLast();	
}

void Pics::OnLast2() 
{
	pframe->m_pxaxis->OnReadLast2();
	
}

void Pics::OnLast3() 
{
	pframe->m_pxaxis->OnReadLast3();
	
}

void Pics::OnLast4() 
{
	pframe->m_pxaxis->OnReadLast4();
	
}

void Pics::OnCancel() 
{
	pframe->PicsOpen=false;
	
	CDialog::OnCancel();
}


void Pics::OnSaveone1c() 
{
	theapp->saveOne1c=true;
	
}

void Pics::OnSaveone1() 
{
	theapp->saveOne1=true;
	
}

void Pics::OnSaveone2c() 
{
	theapp->saveOne2c=true;
	
}

void Pics::OnSaveone2() 
{
	theapp->saveOne2=true;
	
}

void Pics::OnSaveone3c() 
{
	theapp->saveOne3c=true;
	
}

void Pics::OnSaveone3() 
{
	theapp->saveOne3=true;
	
}
