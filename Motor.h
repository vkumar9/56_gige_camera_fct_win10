#if !defined(AFX_MOTOR_H__D6BBF889_4E7B_485C_8E2A_EE632F219EF6__INCLUDED_)
#define AFX_MOTOR_H__D6BBF889_4E7B_485C_8E2A_EE632F219EF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Motor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Motor dialog
class Motor : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;
public:
	int MotorPos;		//Used for storing current motor position.

	int CntrBits;//NOT USED
	int Function;//NOT USED
	int Value;//NOT USED

	Motor(CWnd* pParent = NULL);   // standard constructor
	CCapApp *theapp;
	CMainFrame* pframe;
	int MotPos;
// Dialog Data
	//{{AFX_DATA(Motor)
	enum { IDD = IDD_MOTOR };
	CString	m_mpot;		//Variable for displaying current motor position.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Motor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Motor)
	virtual BOOL OnInitDialog();
	afx_msg void OnHigh();
	afx_msg void OnLower();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnHigh2();
	afx_msg void OnLower2();
	virtual void OnOK();
	afx_msg void OnTrigger();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MOTOR_H__D6BBF889_4E7B_485C_8E2A_EE632F219EF6__INCLUDED_)
