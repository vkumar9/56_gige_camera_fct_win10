// Insp.cpp : implementation file
//

#include "stdafx.h"
#include "Insp.h"
#include "Cap.h"
#include "xaxisview.h"
#include "mainfrm.h"
#include "capview.h"
#include "tech.h"
#include "security.h"
#include "serial.h"
#include "tbcolor.h"
#include "Prompt3.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Insp dialog
int v=0;

Insp::Insp(CWnd* pParent /*=NULL*/)
	: CDialog(Insp::IDD, pParent)
{
	//{{AFX_DATA_INIT(Insp)
	m_capmiddle = _T("");
	m_capneck = _T("");
	m_topleft = _T("");
	m_tbl = _T("");
	m_tbr = _T("");
	m_fillh = _T("");
	m_fillv = _T("");
	m_edittb2s = _T("");
	m_edittb2h = _T("");
	m_edituh = _T("");
	m_editus = _T("");
	m_edituv = _T("");
	m_editsize = _T("");
	m_edittb2 = _T("");
	m_editb2v = _T("");
	m_edittbseam = _T("");
	m_tb2vert = _T("");
	//}}AFX_DATA_INIT
}


void Insp::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Insp)
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Control(pDX, IDC_LIPSIZE, m_lipsize);
	DDX_Control(pDX, IDC_LIPAREATALLER, m_lipareataller);
	DDX_Control(pDX, IDC_LIPAREASMALLER, m_lipareasmaller);
	DDX_Control(pDX, IDC_SQOFFSET, m_sqoffset);
	DDX_Control(pDX, IDC_LIPDN2, m_lipdn2);
	DDX_Control(pDX, IDC_LIPUP2, m_lipup2);
	DDX_Control(pDX, IDC_LIPAREA, m_liparea);
	DDX_Control(pDX, IDC_LIPUP, m_lipup);
	DDX_Control(pDX, IDC_LIPRIGHT, m_lipright);
	DDX_Control(pDX, IDC_LIPLEFT, m_lipleft);
	DDX_Control(pDX, IDC_LIPDN, m_lipdn);
	DDX_Control(pDX, IDC_TBUP, m_tbupsp);
	DDX_Control(pDX, IDC_TBSMALLER, m_tbsmallersp);
	DDX_Control(pDX, IDC_TB2UP, m_tb2upsp);
	DDX_Control(pDX, IDC_TB2MUP, m_tb2mupsp);
	DDX_Control(pDX, IDC_TB2MDN, m_tb2mdnsp);
	DDX_Control(pDX, IDC_TB2DN, m_tb2dnsp);
	DDX_Control(pDX, IDC_FDN, m_fdnsp);
	DDX_Control(pDX, IDC_FLEFT, m_fleftsp);
	DDX_Control(pDX, IDC_LRIGHT, m_lrightsp);
	DDX_Control(pDX, IDC_LLEFT, m_lleftsp);
	DDX_Control(pDX, IDC_FUP, m_fupsp);
	DDX_Control(pDX, IDC_FRIGHT, m_frightsp);
	DDX_Control(pDX, IDC_NUP, m_nupsp);
	DDX_Control(pDX, IDC_NDN, m_ndnsp);
	DDX_Control(pDX, IDC_MUP, m_mupsp);
	DDX_Control(pDX, IDC_MDN, m_mdnsp);
	DDX_Control(pDX, IDC_TBDN, m_tbdnsp);
	DDX_Control(pDX, IDC_TBLARGER, m_tblargersp);
	DDX_Control(pDX, IDC_TBLEFT, m_tbleftsp);
	DDX_Control(pDX, IDC_TBRIGHT, m_tbrightsp);
	DDX_Control(pDX, IDC_UDN, m_udnsp);
	DDX_Control(pDX, IDC_ULARGE, m_ulargesp);
	DDX_Control(pDX, IDC_ULEFT, m_uleftsp);
	DDX_Control(pDX, IDC_URIGHT, m_urightsp);
	DDX_Control(pDX, IDC_USMALL, m_usmallsp);
	DDX_Control(pDX, IDC_UUP, m_uupsp);
	DDX_Text(pDX, IDC_CAPMIDDLE, m_capmiddle);
	DDX_Text(pDX, IDC_CAPNECK, m_capneck);
	DDX_Text(pDX, IDC_TOPLEFT, m_topleft);
	DDX_Text(pDX, IDC_EDITTBL, m_tbl);
	DDX_Text(pDX, IDC_EDITFILLH, m_fillh);
	DDX_Text(pDX, IDC_EDITFILLV, m_fillv);
	DDX_Text(pDX, IDC_EDITTB2S, m_edittb2s);
	DDX_Text(pDX, IDC_EDITUH, m_edituh);
	DDX_Text(pDX, IDC_EDITUS, m_editus);
	DDX_Text(pDX, IDC_EDITUV, m_edituv);
	DDX_Text(pDX, IDC_EDITSIZE, m_editsize);
	DDX_Text(pDX, IDC_EDITTB2, m_edittb2);
	DDX_Text(pDX, IDC_EDITTBSEAM, m_edittbseam);
	DDX_Text(pDX, IDC_TB2VERT, m_tb2vert);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Insp, CDialog)
	//{{AFX_MSG_MAP(Insp)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_MUP, OnMup)
	ON_BN_CLICKED(IDC_MDN, OnMdn)
	ON_BN_CLICKED(IDC_NUP, OnNup)
	ON_BN_CLICKED(IDC_NDN, OnNdn)
	ON_BN_CLICKED(IDC_LLEFT, OnLleft)
	ON_BN_CLICKED(IDC_LRIGHT, OnLright)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_FINE, OnFine)
	ON_BN_CLICKED(IDC_TBUP, OnTbup)
	ON_BN_CLICKED(IDC_TBDN, OnTbdn)
	ON_BN_CLICKED(IDC_TBLEFT, OnTbleft)
	ON_BN_CLICKED(IDC_TBRIGHT, OnTbright)
	ON_BN_CLICKED(IDC_FUP, OnFup)
	ON_BN_CLICKED(IDC_FDN, OnFdn)
	ON_BN_CLICKED(IDC_FLEFT, OnFleft)
	ON_BN_CLICKED(IDC_FRIGHT, OnFright)
	ON_BN_CLICKED(ID_TRIGGER, OnTrigger)
	ON_BN_CLICKED(ID_TRIGGER2, OnTrigger2)
	ON_BN_CLICKED(ID_TRIGGER3, OnTrigger3)
	ON_BN_CLICKED(IDC_INSPECT, OnInspectPushed)
	ON_BN_CLICKED(IDC_TECH, OnTech)
	ON_BN_CLICKED(IDC_TB2UP, OnTb2up)
	ON_BN_CLICKED(IDC_TB2DN, OnTb2dn)
	ON_BN_CLICKED(IDC_TB2MUP, OnTb2mup)
	ON_BN_CLICKED(IDC_TB2MDN, OnTb2mdn)
	ON_BN_CLICKED(IDC_UUP, OnUup)
	ON_BN_CLICKED(IDC_UDN, OnUdn)
	ON_BN_CLICKED(IDC_ULEFT, OnUleft)
	ON_BN_CLICKED(IDC_URIGHT, OnUright)
	ON_BN_CLICKED(IDC_USUP, OnUsup)
	ON_BN_CLICKED(IDC_USDN, OnUsdn)
	ON_BN_CLICKED(IDC_USMALL, OnUsmall)
	ON_BN_CLICKED(IDC_ULARGE, OnUlarge)
	ON_BN_CLICKED(IDC_TBSMALLER, OnTbsmaller)
	ON_BN_CLICKED(IDC_TBLARGER, OnTblarger)
	ON_BN_CLICKED(IDC_RADIO1X, OnRadio1x)
	ON_BN_CLICKED(IDC_RADIO2X, OnRadio2x)
	ON_BN_CLICKED(IDC_RADIO3X, OnRadio3x)
	ON_BN_CLICKED(IDC_RADIO4X, OnRadio4x)
	ON_BN_CLICKED(IDC_RADIO5X, OnRadio5x)
	ON_BN_CLICKED(IDC_MANUAL, OnManual)
	ON_BN_CLICKED(IDC_LIPUP, OnLipup)
	ON_BN_CLICKED(IDC_LIPDN, OnLipdn)
	ON_BN_CLICKED(IDC_LIPLEFT, OnLipleft)
	ON_BN_CLICKED(IDC_LIPRIGHT, OnLipright)
	ON_BN_CLICKED(IDC_LIPUP2, OnLipup2)
	ON_BN_CLICKED(IDC_LIPDN2, OnLipdn2)
	ON_BN_CLICKED(IDC_LIPAREATALLER, OnLipareataller)
	ON_BN_CLICKED(IDC_LIPAREASMALLER, OnLipareasmaller)
	ON_BN_CLICKED(IDC_RADIO6X, OnRadio6x)
	ON_BN_CLICKED(IDC_SUNNY, OnSunny)
	ON_BN_CLICKED(IDC_SETUPCOLOR, OnSetupcolor)
	ON_BN_CLICKED(IDC_XTRALIGHT, OnXtralight)
	ON_BN_CLICKED(IDC_CHECK1, OnUseleft)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Insp message handlers

BOOL Insp::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pinsp=this;
	theapp=(CCapApp*)AfxGetApp();
	once=true;
	res=10;					//Set adjustments resolution (step size) large.
	GotLive=true;			//Image is loaded.
	manual=false;	//not used
	pframe->Stop=true;
	pframe->OnLine=false;	//Ejector turned off.
	pframe->m_pview->SetTimer(2,500,NULL);	//Toggle ejector off-line background from gray to red.
	manual=true;

	//Update GUI for extra light on/off.
	if(theapp->jobinfo[pframe->CurrentJobNum].xtraLight==true)
	{
		CheckDlgButton(IDC_XTRALIGHT,1);	
	}
	else
	{
		CheckDlgButton(IDC_XTRALIGHT,0);
	}

	//Update GUI for no lip controls using only the left search box.
	if(theapp->jobinfo[pframe->CurrentJobNum].useLeft==true)
	{
		CheckDlgButton(IDC_CHECK1,1);	
	}
	else
	{
		CheckDlgButton(IDC_CHECK1,0);
	}

	im_insp1 = (BYTE*)GlobalAlloc(GMEM_FIXED,1024 * 768* 4  ); //ptr for holding color
	im_insp2 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );
	im_insp3 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );

	im_ptr_C1 = (BYTE*)GlobalAlloc(GMEM_FIXED,1024 * 768* 4  ); //ptr for holding full size color img
	im_ptr_C2 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );
	im_ptr_C3 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );

	im_ptr_C1_1 = (BYTE*)GlobalAlloc(GMEM_FIXED,1024 * 768* 4  ); //ptr for holding full size color img
	im_ptr_C2_2 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );
	im_ptr_C3_3 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );

	im_INSP1 = (BYTE*)GlobalAlloc(GMEM_FIXED,1024 * 768* 4  );
	im_INSP2 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );
	im_INSP3 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );

	im_data1 = (BYTE*)GlobalAlloc(GMEM_FIXED,1024 * 768   ); //ptr for holding Grayscale
	im_data2 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );
	im_data3 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 * 4 );

	im_INSP1=pframe->m_pxaxis->im_reduce;

	im_INSP2=pframe->m_pxaxis->im_reduce2;

	im_INSP3=pframe->m_pxaxis->im_reduce3;

	//$$
	im_ptr_C1_1 =pframe->m_pxaxis->im_cam1Color;			//ptr for color image

	im_ptr_C2_2 =pframe->m_pxaxis->im_cam2Color;

	im_ptr_C3_3 =pframe->m_pxaxis->im_cam3Color;

	//$$
	
	pframe->PhotoEyeEnable(false);						//Disable the photoeye
	pframe->SetTimer(26,500,NULL);						//set red light

	//CAM 1												//half sized color imag

	CVisRGBAByteImage	imgco1(theapp->cam1Width/2,theapp->cam1Height/2, 1, -1,im_INSP1);
	imgco1.SetRect(0, 0, theapp->cam1Width/2,theapp->cam1Height/2);
	imageI = imgco1;

/*	CVisRGBAByteImage image1(512,367,1,-1,im_insp1);	//imgBuf1im_data
	image1.SetRect(0,0,512,367);//cam 1
	image1.CopyPixelsTo(imageI);
*/
	//im_INSP1=(unsigned char*)(imageI.PixelAddress(0,0,1));

//	pframe->m_pxaxis->OnSaveC(im_insp1,6);

	if(theapp->noCams == true)
	{
		if(pframe->m_pxaxis->load_1_pic==true)
		{
		im_data1=pframe->m_pxaxis->CtoBW(pframe->m_pxaxis->im_dataC);
		}
		else
		{
			im_data1=pframe->m_pxaxis->im_cam1;
		}
	
	}
	else
	{
		im_data1=pframe->m_pxaxis->CtoBW(pframe->m_pxaxis->m_imageProcessed[0]); 
	}
	//@@												//for full size color image




	//CAM 2
	CVisRGBAByteImage	imgco2(theapp->cam2Width/2,theapp->cam2Height/2, 1, -1,im_INSP2);
	imgco2.SetRect(0, 0, theapp->cam2Width/2,theapp->cam2Height/2);
	imageI2 = imgco2;




	//CAM 3

	CVisRGBAByteImage	imgco3(theapp->cam3Width/2,theapp->cam3Height/2, 1, -1,im_INSP3);
	imgco3.SetRect(0, 0, theapp->cam3Width/2, theapp->cam3Height/2);
	imageI3 = imgco3;


	//SetWindowPos(&wndTop,10,400,780,250,SWP_SHOWWINDOW | SWP_NOSIZE );

	//Make sure center cap line is in the correct range.
	if(theapp->jobinfo[pframe->CurrentJobNum].midBarY>480 || theapp->jobinfo[pframe->CurrentJobNum].midBarY <0 ) theapp->jobinfo[pframe->CurrentJobNum].midBarY=0;

	//Make sure neck line is in the correct range.
	if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY>480 || theapp->jobinfo[pframe->CurrentJobNum].neckBarY <0 ) theapp->jobinfo[pframe->CurrentJobNum].neckBarY=0;
	
	//RTopBar does not appear to be used.	
	if(pframe->m_pxaxis->RTopBar.x>480 || pframe->m_pxaxis->RTopBar.x <50 ) pframe->m_pxaxis->RTopBar.x=50;
	//if(theapp->jobinfo[pframe->CurrentJobNum].vertBarX>480 || theapp->jobinfo[pframe->CurrentJobNum].vertBarX <50 ) theapp->jobinfo[pframe->CurrentJobNum].vertBarX=50;

	//if(pframe->m_pxaxis->ShowBars==true){CheckDlgButton(IDC_BARS,1);}else{CheckDlgButton(IDC_BARS,0);}
	pframe->InspOpen=true;		//Set flag indicating that the inspection dialog box is open.

	//Update gui edit boxes from saved job.
	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);

	m_edittbseam.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandY);
	m_tbl.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandX);
	m_tbr.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandDx);	
	m_fillv.Format("%3i",pframe->m_pxaxis->Fill.y);
	m_fillh.Format("%3i",pframe->m_pxaxis->Fill.x);
	m_tbr.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandThresh);
	m_edittb2h.Format("%3i",pframe->m_pxaxis->TB2Offset.x);
	m_tb2vert.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamY);
	m_edittb2s.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh);
	m_editsize.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userDx);
	m_editus.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userStrength);
	m_edituh.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userX);
	m_edituv.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userY);
	//pframe->WasSetup=false;
	m_edittb2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamDy);
	m_topleft.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);
	pframe->CountEnable=false;

	InitialXLocations();
	if(pframe->m_pxaxis->MidTop.x <=50) pframe->m_pxaxis->MidTop.x=320;

//Update gui controls based on saved tamperband surface algorithm used.
//Update Tamperband surface algorithm
	switch(theapp->jobinfo[pframe->CurrentJobNum].tBandType)
	{
	case 1:
		CheckDlgButton(IDC_RADIO1X,1);
		CheckDlgButton(IDC_RADIO2X,0);
		CheckDlgButton(IDC_RADIO3X,0);
		CheckDlgButton(IDC_RADIO4X,0);
		CheckDlgButton(IDC_RADIO5X,0);
		CheckDlgButton(IDC_RADIO6X,0);
		CheckDlgButton(IDC_SUNNY,0);
		break;

	case 2:
		CheckDlgButton(IDC_RADIO1X,0);
		CheckDlgButton(IDC_RADIO2X,1);
		CheckDlgButton(IDC_RADIO5X,0);
		CheckDlgButton(IDC_RADIO6X,0);
		CheckDlgButton(IDC_SUNNY,0);
		if(theapp->jobinfo[pframe->CurrentJobNum].dark==1)
		{
			CheckDlgButton(IDC_RADIO3X,1);
			CheckDlgButton(IDC_RADIO4X,0);
		}
		else
		{
			CheckDlgButton(IDC_RADIO3X,0);
			CheckDlgButton(IDC_RADIO4X,1);
		}
		break;
	
	case 3:
		CheckDlgButton(IDC_RADIO1X,0);
		CheckDlgButton(IDC_RADIO2X,0);
		CheckDlgButton(IDC_RADIO3X,0);
		CheckDlgButton(IDC_RADIO4X,0);
		CheckDlgButton(IDC_RADIO5X,1);
		CheckDlgButton(IDC_RADIO6X,0);
		CheckDlgButton(IDC_SUNNY,0);
		break;

	case 4:
		CheckDlgButton(IDC_RADIO1X,0);
		CheckDlgButton(IDC_RADIO2X,0);
		CheckDlgButton(IDC_RADIO3X,0);
		CheckDlgButton(IDC_RADIO4X,0);
		CheckDlgButton(IDC_RADIO5X,0);
		CheckDlgButton(IDC_RADIO6X,1);
		CheckDlgButton(IDC_SUNNY,0);
		break;
	case 5:
		CheckDlgButton(IDC_RADIO1X,0);
		CheckDlgButton(IDC_RADIO2X,0);
		CheckDlgButton(IDC_RADIO3X,0);
		CheckDlgButton(IDC_RADIO4X,0);
		CheckDlgButton(IDC_RADIO5X,0);
		CheckDlgButton(IDC_RADIO6X,0);
		CheckDlgButton(IDC_SUNNY,1);
		break;
	}


	//If the bottle has no lip show no lip configuration controls on gui.
	if(theapp->jobinfo[pframe->CurrentJobNum].lip==1)
	{
		m_lipright.ShowWindow(SW_SHOW);
		m_lipleft.ShowWindow(SW_SHOW);
		m_lipdn.ShowWindow(SW_SHOW);
		m_lipup.ShowWindow(SW_SHOW);
//		m_editlipposc.ShowWindow(SW_SHOW);
		m_liparea.ShowWindow(SW_SHOW);
		m_lipdn2.ShowWindow(SW_SHOW);
		m_lipup2.ShowWindow(SW_SHOW);
		m_sqoffset.ShowWindow(SW_SHOW);
		m_lipareasmaller.ShowWindow(SW_SHOW);
		m_lipareataller.ShowWindow(SW_SHOW);
		m_lipsize.ShowWindow(SW_SHOW);



	}
	else // Otherwise hide the no lip configuration controls
	{
		m_lipright.ShowWindow(SW_HIDE);
		m_lipleft.ShowWindow(SW_HIDE);
		m_lipdn.ShowWindow(SW_HIDE);
		m_lipup.ShowWindow(SW_HIDE);
		m_lipdn2.ShowWindow(SW_HIDE);
		m_lipup2.ShowWindow(SW_HIDE);
//		m_editlipposc.ShowWindow(SW_HIDE);
		m_liparea.ShowWindow(SW_HIDE);
		m_sqoffset.ShowWindow(SW_HIDE);
		m_lipareasmaller.ShowWindow(SW_HIDE);
		m_lipareataller.ShowWindow(SW_HIDE);
		m_lipsize.ShowWindow(SW_HIDE);
	}
if(manual==false)//Not used
{
	m_lleftsp.ShowWindow(SW_HIDE);
	m_lrightsp.ShowWindow(SW_HIDE);
	m_tb2upsp.ShowWindow(SW_HIDE);
	m_tb2dnsp.ShowWindow(SW_HIDE);
	m_mupsp.ShowWindow(SW_HIDE);
	m_mdnsp.ShowWindow(SW_HIDE);
	m_nupsp.ShowWindow(SW_HIDE);
	m_ndnsp.ShowWindow(SW_HIDE);
	m_tblargersp.ShowWindow(SW_HIDE);
	m_tbsmallersp.ShowWindow(SW_HIDE);
	m_tbupsp.ShowWindow(SW_HIDE);
	m_tbdnsp.ShowWindow(SW_HIDE);
	m_tbrightsp.ShowWindow(SW_HIDE);
	m_tbleftsp.ShowWindow(SW_HIDE);
	m_fupsp.ShowWindow(SW_HIDE);
	m_fdnsp.ShowWindow(SW_HIDE);
	m_frightsp.ShowWindow(SW_HIDE);
	m_fleftsp.ShowWindow(SW_HIDE);
	m_uupsp.ShowWindow(SW_HIDE);
	m_udnsp.ShowWindow(SW_HIDE);
	m_urightsp.ShowWindow(SW_HIDE);
	m_uleftsp.ShowWindow(SW_HIDE);
	m_ulargesp.ShowWindow(SW_HIDE);
	m_usmallsp.ShowWindow(SW_HIDE);
}
//	if(theapp->jobinfo[pframe->CurrentJobNum].taught==true) {pframe->m_pxaxis->OnLoadPattern(pframe->CurrentJobNum);}
	SetTimer(5,1000,NULL);//refresh
	UpdateData(false);
	InvalidateRect(NULL,false);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Find the default x coordinate of the vertical left edge of cap line. Called when this window is first opened.
void Insp::InitialXLocations()
{
	if(theapp->noCams==false)
	{
	
		pframe->m_pxaxis->PreInsp();
	}
	//Get left cap side coordinates.
	SinglePoint CapLSide=pframe->m_pxaxis->FindCapSide(pframe->m_pxaxis->im_data,theapp->jobinfo[pframe->CurrentJobNum].midBarY*2,true);  //orgnl
	//SinglePoint CapLSide=pframe->m_pxaxis->FindCapSide(im_data1,theapp->jobinfo[pframe->CurrentJobNum].midBarY*2,true);
	
	//Set default position of left vertical bar to left side of cap plus a default offset.
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX=CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].lDist;
	
}

void Insp::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		//pframe->m_pxaxis->MidBar.y+=20;
	}

	if (nIDEvent==2)  //Timer set in OnTrigger
	{
		KillTimer(2);
		InvalidateRect(NULL,false);
		pframe->Stop=true;				//Not used.
	}

	if (nIDEvent==4)  //Timer set in OnTrigger
	{
		KillTimer(4);
		InvalidateRect(NULL,false);
		
	}

	if (nIDEvent==5) //Timer set in OnInitDialog
	{
		KillTimer(5);
		//OnFine();
		//UpdateData(false);
		//InvalidateRect(NULL,false);
		
	}
	CDialog::OnTimer(nIDEvent);
}


//Moves up the middle of cap line for pullup inspection, used to find middle of cap.
void Insp::OnMup() 
{
	//pframe->m_pxaxis->MidBar.y-=res;
	theapp->jobinfo[pframe->CurrentJobNum].midBarY-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].midBarY<=0)theapp->jobinfo[pframe->CurrentJobNum].midBarY=0;
	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves down the middle of cap line for pullup inspection, used to find middle of cap.
void Insp::OnMdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].midBarY+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].midBarY>=480)theapp->jobinfo[pframe->CurrentJobNum].midBarY=480;
	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);
	UpdateData(false);
	InvalidateRect(NULL,false);
}


//Called when exit is clicked.
void Insp::OnOK() 
{
	pframe->SaveJob(pframe->CurrentJobNum);
	pframe->PhotoEyeEnable(true);
	pframe->InspOpen=false;
	pframe->Stop=false;
	//	pframe->m_pxaxis->OnSnap();

	//pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	CDialog::OnOK();
}

//NOT USED
void Insp::OnClose() 
{
	//dont think this function is being called
	pframe->CountEnable=true;
	pframe->Stop=false;
	pframe->PhotoEyeEnable(true);
	//OnInspect();
		
	CDialog::OnClose();
}

//Moves up the middle of neck line for pullup inspection, used to find lip of bottle.
void Insp::OnNup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY-=res;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves down the middle of neck line for pullup inspection, used to find lip of bottle.
void Insp::OnNdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY+=res;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Finds the default inspection parameters.
void Insp::OnInspect() 
{
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	
	//Find the left mid cap coordinates.
	SinglePoint CapLSide=pframe->m_pxaxis->FindCapSide(pframe->m_pxaxis->im_data,theapp->jobinfo[pframe->CurrentJobNum].midBarY*2,true);
	
	//Find the right mid cap coordinates.
	SinglePoint CapRSide=pframe->m_pxaxis->FindCapSide(pframe->m_pxaxis->im_data,theapp->jobinfo[pframe->CurrentJobNum].midBarY*2,false);
	
	//Compute the distance between the left vertical bar and the left edge of the cap.
	theapp->jobinfo[pframe->CurrentJobNum].lDist=theapp->jobinfo[pframe->CurrentJobNum].vertBarX-CapLSide.x;
	
	//If vertical bar is too far right from left edge of cap.
	if(theapp->jobinfo[pframe->CurrentJobNum].lDist >=350) theapp->jobinfo[pframe->CurrentJobNum].lDist=350; //150 
	//If vertical bar is beyond the left edge of cap.
	if(theapp->jobinfo[pframe->CurrentJobNum].lDist <=0) theapp->jobinfo[pframe->CurrentJobNum].lDist=0;

	//Compute the distance between the right vertical bar and the right edge of the cap.
	//This is only a delta so both the left and right deltas are equivalent.
	theapp->jobinfo[pframe->CurrentJobNum].rDist=theapp->jobinfo[pframe->CurrentJobNum].lDist;
				
	pframe->m_pxaxis->LearnDone=false;	//Set flag indicating that inspection parameters need to be taught.
	
	//Teach inspection parameters.
	pframe->m_pxaxis->Inspect(pframe->m_pxaxis->im_dataC, pframe->m_pxaxis->im_dataC2, 
								pframe->m_pxaxis->im_dataC3, pframe->m_pxaxis->im_data,pframe->m_pxaxis->im_data2,pframe->m_pxaxis->im_data3,
								pframe->m_pxaxis->sobel_cam2,pframe->m_pxaxis->sobel_cam3, pframe->m_pxaxis->im_red, pframe->m_pxaxis->im_red2, 
								pframe->m_pxaxis->im_red3, pframe->m_pxaxis->im_hue, pframe->m_pxaxis->im_hue2, pframe->m_pxaxis->im_hue3);
	
	//Find distance between left lip and neck line.
	pframe->m_pxaxis->LipOffset=pframe->m_pxaxis->FindNeckLip(pframe->m_pxaxis->im_data, pframe->m_pxaxis->NeckLSide, true);
	
	//Compute coordinates of left lip bottom.
	pframe->m_pxaxis->LipDiffl=pframe->m_pxaxis->NeckLSide.y-pframe->m_pxaxis->LipOffset.y;

	//Find distance between right lip and neck line.
	pframe->m_pxaxis->LipOffset=pframe->m_pxaxis->FindNeckLip(pframe->m_pxaxis->im_data, pframe->m_pxaxis->NeckRSide, false);	
	
	//Compute coordinates of right lip bottom.
	pframe->m_pxaxis->LipDiffr=pframe->m_pxaxis->NeckRSide.y-pframe->m_pxaxis->LipOffset.y;
	
	//Compute delta between the cam1 tamper band edge search box coordinates and the cap edge.
	pframe->m_pxaxis->TBOffset.x=(theapp->jobinfo[pframe->CurrentJobNum].tBandX)-pframe->m_pxaxis->CapLSide.x;
	
	//Compute delta between the cam1 tamper band edge search box coordinates and the cap edge.	
	pframe->m_pxaxis->TBOffset.y=(theapp->jobinfo[pframe->CurrentJobNum].tBandY * 2)-(pframe->m_pxaxis->CapLTop.y+pframe->m_pxaxis->CapRTop.y)/2;
	
	//Not sure what this checks.
	if(GotLive==true)
	{	
		
		switch(theapp->jobinfo[pframe->CurrentJobNum].lip)
		{
		case 0:  //Save an image of the taught lip pattern for a normal cap type.
				pframe->m_pxaxis->LearnPattern(pframe->m_pxaxis->im_cam1, true);
				pframe->m_pxaxis->LearnPattern(pframe->m_pxaxis->im_cam1, false);
				break;
	
		case 1:	//Save an image of the taught lip pattern for a no lip bottle cap.
				pframe->m_pxaxis->LearnPatternUser(pframe->m_pxaxis->im_cam1, true);
				pframe->m_pxaxis->LearnPatternUser(pframe->m_pxaxis->im_cam1, false);
				break;
		
				  
		}
	
		
	}
	else
	{
		switch(theapp->jobinfo[pframe->CurrentJobNum].lip)
		{
		case 0:
				pframe->m_pxaxis->LearnPattern(pframe->m_pxaxis->im_data, true);
				pframe->m_pxaxis->LearnPattern(pframe->m_pxaxis->im_data, false);
				break;
	
		case 1:
				pframe->m_pxaxis->LearnPatternUser(pframe->m_pxaxis->im_data, true);
				pframe->m_pxaxis->LearnPatternUser(pframe->m_pxaxis->im_data, false);
				break;
				
		
		}
	}
	
	
	//pframe->m_pxaxis->FindEndLight(2);
	//Mark job as taught.
	pframe->CurrentJoba1=theapp->jobinfo[pframe->CurrentJobNum].taught=true;		//pframe->CurrentJoba1 not used, 
	pframe->m_pview->m_status="Taught Success!";	//Update text in status box to indicate that job has been taught.
	pframe->m_pview->UpdateDisplay();	//Update left hand pan to indicate that job has been taught.
	pframe->WasSetup=true;		//Update a job configured flag which is used in main frame Inspect() to update the left hand pan.
	pframe->CountEnable=true;	//NOT USED.
	
	InvalidateRect(NULL,false);			//Repaint display.
}

//Moves left the edge line for pullup inspection, used to find left edge of cap.
void Insp::OnLleft() 
{
	//pframe->m_pxaxis->LTopBar.x-=res;
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX-=res;	//Saves the new edge position in the jobs info.
	m_topleft.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);	//Update gui variable used to display left edge bar position.
	UpdateData(false);		//Update gui.
	InvalidateRect(NULL,false);
	
}

//Moves right the edge line for pullup inspection, used to find left edge of cap.
void Insp::OnLright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX+=res;
	m_topleft.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}
/*
void Insp::OnRleft() 
{
	pframe->m_pxaxis->RTopBar.x-=10;
	m_topright.Format("%3i",pframe->m_pxaxis->RTopBar.x);
	UpdateData(false);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	
}

void Insp::OnRright() 
{
	pframe->m_pxaxis->RTopBar.x+=10;
	m_topright.Format("%3i",pframe->m_pxaxis->RTopBar.x);
	UpdateData(false);
	pframe->m_pxaxis->InvalidateRect(NULL,false);
	
}

*/


void Insp::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	//Create colors
	COLORREF greencolor = RGB(0,255,0);
	COLORREF redcolor = RGB(255,0,0);
	COLORREF bluecolor = RGB(0,0,150);
	COLORREF lbluecolor = RGB(100,190,250);
	COLORREF graycolor = RGB(190,190,190);
	COLORREF blackcolor = RGB(50,50,50);


	CString m1,m2, m3, m4, m5, m6;

	//Create pens
	CPen BluePen(PS_SOLID, 1, RGB(0,0,250));
	CPen LGrayPen(PS_SOLID, 1, RGB(200,200,200));
	CPen LRedPen(PS_SOLID, 1, RGB(230,30,80));
	CPen FRedPen(PS_SOLID, 3, RGB(255,0,0));
	CPen RedDash(PS_DASH, 1, RGB(255,0,0));
	CPen BlackPen(PS_SOLID, 2, RGB(0,0,0));
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
	CPen LGreenPen(PS_SOLID, 1, RGB(0,255,0));
	CPen YellowPen(PS_DASH, 1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID, 2, RGB(255,255,255));
	CPen AquaPen(PS_SOLID, 4, RGB(0,255,255));
	int yOffset=-15;

	

	//prevent vertical teach bar from going beyond 0 
	if(theapp->jobinfo[pframe->CurrentJobNum].vertBarX<=0) theapp->jobinfo[pframe->CurrentJobNum].vertBarX=0;

	if(once==true)
	{
		bitmapl.CreateCompatibleBitmap(&dc, 60,40);
		bitmapr.CreateCompatibleBitmap(&dc, 60,40);
		once=false;
	}


	//if job is taught
   if(theapp->jobinfo[pframe->CurrentJobNum].taught==1)
   {
		bitmapr.SetBitmapBits(60 * 40 *4 , pframe->m_pxaxis->im_patr);	//Load right lip bit image.
		bitmapl.SetBitmapBits(60 * 40 *4 , pframe->m_pxaxis->im_patl);	//Load left lip bit image.
   }
   else
   {
	   memset(pframe->m_pxaxis->im_patr,0,60 * 40 *4);	//set left lip bit image to all 0s.
	   memset(pframe->m_pxaxis->im_patl,0,60 * 40 *4);	//set right lip bit image to all 0s.

	bitmapr.SetBitmapBits(60 * 40 *4 , pframe->m_pxaxis->im_patr);	//Load right lip bit image.
	bitmapl.SetBitmapBits(60 * 40 *4 , pframe->m_pxaxis->im_patl);	//Load left lip bit image.

   }
	//& if job is not taught..Patterns should be black boxes.

	CDC templ; templ.CreateCompatibleDC(0);
	CDC tempr; tempr.CreateCompatibleDC(0);

	BITMAP bmpl;
	BITMAP bmpr;

	CBitmap *old =templ.SelectObject(&bitmapl);
	::GetObject(bitmapl, sizeof(BITMAP), &bmpl);
	dc.BitBlt(645,330,60,40, &templ,0,0,SRCCOPY);

	CBitmap *oldr =tempr.SelectObject(&bitmapr);
	::GetObject(bitmapr, sizeof(BITMAP), &bmpr);
	dc.BitBlt(710,330,60,40, &tempr,0,0,SRCCOPY);

	dc.SetWindowOrg(-(theapp->cam1XOffset),-5);
	
	CVisImageBase& refimage=imageI;// = pDoc->Image();
	//refimage.SetRect(0,0,600,360);//crop image to fit in window better
	assert(refimage.IsValid());
	refimage.DisplayInHdc(dc); 



	dc.SetWindowOrg(-5,-280);
	CVisImageBase& refimage2=imageI2;// = pDoc->Image();
	assert(refimage2.IsValid());
	refimage2.DisplayInHdc(dc);

	dc.SetWindowOrg(-318,-280);
	CVisImageBase& refimage3=imageI3;// = pDoc->Image();
	assert(refimage3.IsValid());
	refimage3.DisplayInHdc(dc);
	
	dc.SetWindowOrg(-5,-5);

	//dc.SetWindowOrg(20,40);//compensate for crop
	//dc.SelectObject(&GreenPen);
	dc.SelectObject(&BluePen);
	dc.MoveTo(0+theapp->cam1XOffset-5, theapp->jobinfo[pframe->CurrentJobNum].midBarY);  //draw cap width line
	dc.LineTo(550, theapp->jobinfo[pframe->CurrentJobNum].midBarY);

			//pDC->MoveTo(RTopBar.x, MidBar.y);  //cap width lines left
			//pDC->LineTo(640, MidBar.y);

	dc.MoveTo(0+theapp->cam1XOffset-5, theapp->jobinfo[pframe->CurrentJobNum].neckBarY);  //draw neck width line
	dc.LineTo(550, theapp->jobinfo[pframe->CurrentJobNum].neckBarY);	
			
	dc.MoveTo((theapp->jobinfo[pframe->CurrentJobNum].vertBarX)/2+(theapp->cam1XOffset)-5, 5);   //draw cap edge line
	dc.LineTo((theapp->jobinfo[pframe->CurrentJobNum].vertBarX)/2+(theapp->cam1XOffset)-5, 300);	

			//pDC->SelectObject(&GreenPen);
			//pDC->MoveTo(TBand.x, TBand.y);  //tamper band
			//pDC->LineTo(TBand.x1, TBand.y);
	//***********tamperBand***************
	dc.Draw3dRect(
		(theapp->jobinfo[pframe->CurrentJobNum].tBandX)/2+theapp->cam1XOffset-5, 
		theapp->jobinfo[pframe->CurrentJobNum].tBandY,
		6,
		theapp->jobinfo[pframe->CurrentJobNum].tBandDy,
		lbluecolor,
		lbluecolor);

	dc.Draw3dRect(((int)(pframe->m_pxaxis->MidTop.x)/2)+(theapp->jobinfo[pframe->CurrentJobNum].fillX)+theapp->cam1XOffset-5, 
	   theapp->jobinfo[pframe->CurrentJobNum].fillY,theapp->SavedFillW/2,theapp->SavedFillH/2,bluecolor,bluecolor);//left side

	//************tBand2**************************
	int loffset=135;
	int roffset=10;
	
		dc.Draw3dRect(theapp->jobinfo[pframe->CurrentJobNum].lSWin, 305,160,75,redcolor,redcolor);
		dc.Draw3dRect(theapp->jobinfo[pframe->CurrentJobNum].rSWin+310, 305,160,75,redcolor,redcolor);
		dc.Draw3dRect(0, theapp->jobinfo[pframe->CurrentJobNum].rearCamY/2+285,600,theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,lbluecolor,lbluecolor);
	
	//************Color***************************		
//	if(theapp->inspctn[8].enable==true)
//	{
		dc.Draw3dRect((pframe->m_pxaxis->MidTop.x)/2+(theapp->jobinfo[pframe->CurrentJobNum].userX)/2+theapp->cam1XOffset-5, 
			theapp->jobinfo[pframe->CurrentJobNum].userY/2+(pframe->m_pxaxis->MidTop.y)/2,
			theapp->jobinfo[pframe->CurrentJobNum].userDx/2,
			theapp->jobinfo[pframe->CurrentJobNum].userDx/2,
			bluecolor,bluecolor);
//	}
	//dc.Draw3dRect(pframe->m_pxaxis->TB2Rect.x+20, pframe->m_pxaxis->TB2Rect.y+320,60,10,lbluecolor,lbluecolor);		
	
	dc.SelectObject(&LGrayPen);
	
	dc.MoveTo(0+theapp->cam1XOffset, 150+yOffset);
	dc.LineTo(16+theapp->cam1XOffset, 150+yOffset);  //cap width lines left
	dc.LineTo(10+theapp->cam1XOffset, 145+yOffset);
	dc.LineTo(10+theapp->cam1XOffset, 155+yOffset);
	dc.LineTo(16+theapp->cam1XOffset, 150+yOffset);

	dc.MoveTo(11+theapp->cam1XOffset, 146+yOffset);
	dc.LineTo(11+theapp->cam1XOffset, 154+yOffset);
	dc.MoveTo(12+theapp->cam1XOffset, 147+yOffset);
	dc.LineTo(12+theapp->cam1XOffset, 153+yOffset);
	dc.MoveTo(13+theapp->cam1XOffset, 148+yOffset);
	dc.LineTo(13+theapp->cam1XOffset, 152+yOffset);
	dc.MoveTo(14+theapp->cam1XOffset, 149+yOffset);
	dc.LineTo(14+theapp->cam1XOffset, 151+yOffset);

	dc.SetTextColor(graycolor);
	dc.TextOut(5+theapp->cam1XOffset,130+yOffset,"Photoeye");
	
	if(theapp->jobinfo[pframe->CurrentJobNum].lip==1)
	{
		dc.Draw3dRect
		(
			theapp->jobinfo[pframe->CurrentJobNum].lipx+theapp->jobinfo[pframe->CurrentJobNum].vertBarX/2+theapp->cam1XOffset-20,
			theapp->jobinfo[pframe->CurrentJobNum].lipy+theapp->jobinfo[pframe->CurrentJobNum].midBarY-5,
			30,
			20+theapp->jobinfo[pframe->CurrentJobNum].lipSize/2,
			greencolor,greencolor
		);

		dc.Draw3dRect
		(
			pframe->m_pxaxis->rdist/2+theapp->cam1XOffset-theapp->jobinfo[pframe->CurrentJobNum].lipx-20,
			theapp->jobinfo[pframe->CurrentJobNum].lipy+theapp->jobinfo[pframe->CurrentJobNum].midBarY-5+theapp->jobinfo[pframe->CurrentJobNum].lipYOffset,
			30,
			20+theapp->jobinfo[pframe->CurrentJobNum].lipSize/2,
			greencolor,greencolor
		);
			
		m_lipright.ShowWindow(SW_SHOW);
		m_lipleft.ShowWindow(SW_SHOW);
		m_lipdn.ShowWindow(SW_SHOW);
		m_lipup.ShowWindow(SW_SHOW);
//		m_editlipposc.ShowWindow(SW_SHOW);
		m_liparea.ShowWindow(SW_SHOW);
		m_lipdn2.ShowWindow(SW_SHOW);
		m_lipup2.ShowWindow(SW_SHOW);
		m_sqoffset.ShowWindow(SW_SHOW);
		m_lipareasmaller.ShowWindow(SW_SHOW);
		m_lipareataller.ShowWindow(SW_SHOW);
		m_lipsize.ShowWindow(SW_SHOW);
		m_check1.ShowWindow(SW_SHOW);
	}
	else
	{
		m_lipright.ShowWindow(SW_HIDE);
		m_lipleft.ShowWindow(SW_HIDE);
		m_lipdn.ShowWindow(SW_HIDE);
		m_lipup.ShowWindow(SW_HIDE);
		m_lipdn2.ShowWindow(SW_HIDE);
		m_lipup2.ShowWindow(SW_HIDE);
	//	m_editlipposc.ShowWindow(SW_HIDE);
		m_liparea.ShowWindow(SW_HIDE);
		m_sqoffset.ShowWindow(SW_HIDE);
		m_lipareasmaller.ShowWindow(SW_HIDE);
		m_lipareataller.ShowWindow(SW_HIDE);
		m_lipsize.ShowWindow(SW_HIDE);
		m_check1.ShowWindow(SW_HIDE);
	}
	// Do not call CDialog::OnPaint() for painting messages

		if(theapp->jobinfo[pframe->CurrentJobNum].xtraLight==true)
		{
		/*	dc.Draw3dRect(
				(pframe->m_pxaxis->MidTop.x)/2+(theapp->jobinfo[pframe->CurrentJobNum].fillX)/2+theapp->cam1XOffset-15,
				//(MidTop.x)/2+theapp->jobinfo[pframe->CurrentJobNum].fillX+xOffset-10,
				theapp->jobinfo[pframe->CurrentJobNum].fillArea,
				theapp->SavedFillW+20,
				120,
				greencolor,
				greencolor);//left side
				*/
	
			dc.Draw3dRect(
				(pframe->m_pxaxis->MidTop.x)/2+(theapp->jobinfo[pframe->CurrentJobNum].fillX)+theapp->cam1XOffset-15,
				//(MidTop.x)/2+theapp->jobinfo[pframe->CurrentJobNum].fillX+xOffset-10,
				theapp->jobinfo[pframe->CurrentJobNum].fillArea,
				(theapp->SavedFillW/2)+20,
				120,
				greencolor,
				greencolor);//left side
	
		}
}

//Handler for "Fine Adjust" Button Press. Toggles all configurations controls between small and large increments.
void Insp::OnFine() 
{
	if(res==10)							//If increment is 10
	{
		res=1;							//Reduce it to 1.
		CheckDlgButton(IDC_FINE,1);		//Toggle Button state.
	}
	else
	{
		res=10;
		CheckDlgButton(IDC_FINE,0);
	}
	
}

//Used to position the camera1 edge tamperband rectangle. Moves rectangle up.
void Insp::OnTbup() 
{
//	pframe->CurrentJobi=pframe->m_pxaxis->TBand.y-=res;
	theapp->jobinfo[pframe->CurrentJobNum].tBandY-=res;
	m_edittbseam.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Used to position the camera1 edge tamperband rectangle. Moves rectangle down.
void Insp::OnTbdn() 
{
	//pframe->CurrentJobi=pframe->m_pxaxis->TBand.y+=res;
	theapp->jobinfo[pframe->CurrentJobNum].tBandY+=res;
	m_edittbseam.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Used to position the camera1 edge tamperband rectangle. Moves rectangle left.
void Insp::OnTbleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandX-=res;
	theapp->jobinfo[pframe->CurrentJobNum].tBandDx-=res;
	m_tbl.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandX);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Used to position the camera1 edge tamperband rectangle. Moves rectangle right.
void Insp::OnTbright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandX+=res;
	theapp->jobinfo[pframe->CurrentJobNum].tBandDx+=res;
	m_tbl.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandX);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves low fill search area up.
void Insp::OnFup() 
{
	//pframe->CurrentJobl=pframe->m_pxaxis->Fill.y-=res;
if(theapp->jobinfo[pframe->CurrentJobNum].fillY<=10) theapp->jobinfo[pframe->CurrentJobNum].fillY=10;	//Set minimum height for low fill search area. 10 pixels from top.
theapp->jobinfo[pframe->CurrentJobNum].fillY-=res;
	m_fillv.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].fillY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves low fill search area down.
void Insp::OnFdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].fillY+=res;

	//Set maximum height for low fill search area. 16 pixels below half camera area of interest height.
	if(theapp->jobinfo[pframe->CurrentJobNum].fillY>=(theapp->cam1Height/2)-16)
	{
		theapp->jobinfo[pframe->CurrentJobNum].fillY=(theapp->cam1Height/2)-16;
	}
	
	m_fillv.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].fillY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves low fill search area left.
void Insp::OnFleft() 
{
	//pframe->CurrentJobk=pframe->m_pxaxis->Fill.x-=res;
	theapp->jobinfo[pframe->CurrentJobNum].fillX-=res;
	m_fillv.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].fillX);

	//Track if the low fill area search is to the left or right of center of the neck.
	if(theapp->jobinfo[pframe->CurrentJobNum].fillX < 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=1;}
	else if(theapp->jobinfo[pframe->CurrentJobNum].fillX > 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=0;}

	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves low fill search area right.
void Insp::OnFright() 
{
	//pframe->CurrentJobk=pframe->m_pxaxis->Fill.x+=res;
	theapp->jobinfo[pframe->CurrentJobNum].fillX+=res;
	m_fillv.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].fillX);

	if(theapp->jobinfo[pframe->CurrentJobNum].fillX < 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=1;}
	else if(theapp->jobinfo[pframe->CurrentJobNum].fillX > 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=0;}

	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Handler for "Acquire Single Image" Button Press
void Insp::OnTrigger() 
{
	
	pframe->Stop=false;
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	SetTimer(2,100,NULL);		//Repaints screen.
	GotLive=true;
	pframe->m_pxaxis->InvalidateRect(NULL,false);

	im_INSP1=pframe->m_pxaxis->im_reduce;

//	pframe->m_pxaxis->OnSaveC(pframe->m_pxaxis->im_reduce,6);

	im_INSP2=pframe->m_pxaxis->im_reduce2;
//		pframe->m_pxaxis->OnSaveC(pframe->m_pxaxis->im_reduce2,7);

	im_INSP3=pframe->m_pxaxis->im_reduce3;
//		pframe->m_pxaxis->OnSaveC(pframe->m_pxaxis->im_reduce3,8);

	
	SetTimer(4,200,NULL);
}

//NOT USED
void Insp::OnTrigger2() 
{
	
	char currentpath[60];

	CString m_name2;
	CFileDialog dlg(true,"bmp","c:\\silgandata\\initpic.bmp",OFN_NOCHANGEDIR,NULL,this);

	if(dlg.DoModal()==IDOK)
	{
		m_name2=dlg.GetPathName();
				
		strcpy(currentpath,"");
		strcat(currentpath,m_name2);
		
	CVisRGBAByteImage image;

	try
	{
		image.ReadFile(currentpath);
		imageI.Allocate(image.Rect());

			image.CopyPixelsTo(imageI);
			//pframe->m_pxaxis->im_data=imageI.PixelAddress(0,0,1);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Give up trying to read an image from this file.
			// Warn the user.
		//	AfxMessageBox("The file specified could not be opened.insp");

			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();

			//return FALSE;
		}
		
		
	}
	GotLive=false;
	InvalidateRect(NULL,false);
}

//NOT USED
void Insp::OnTrigger3() 
{
	pframe->PhotoEyeEnable(true);
	pframe->WaitNext=true;
	GotLive=true;
	InitialXLocations();
	InvalidateRect(NULL,false);
	SetTimer(4,1000,NULL);
	UpdateData(false);
	//InvalidateRect(NULL,false);
}

//Teach button pressed handler
void Insp::OnInspectPushed() 
{
	if(pframe->CurrentJobNum>=1 && pframe->CurrentJobNum <=50) //Check if job number is within valid range.
	{
		//Not used.
		if(manual==false) //always goes through this loop.
			pframe->m_pxaxis->ChooseBestPlacement();

		theapp->teachFrontBand=true;	//does not appear to be used.
		OnInspect();

		//Compute camera 2 and camera 3 surface tamper band sizes.
		float b=( ((pframe->m_pxaxis->CapRSide.x-pframe->m_pxaxis->CapLSide.x)+( (pframe->m_pxaxis->CapRSide.x-pframe->m_pxaxis->CapLSide.x) ) *0.70)-120+(pframe->m_pxaxis->TBl)*2 );//45
		pframe->m_pxaxis->Band2Size=((float(pframe->m_pxaxis->CapRSide.x-pframe->m_pxaxis->CapLSide.x))-b-10)/-0.929;
		
	//	pframe->m_pxaxis->SavePattern(pframe->m_pxaxis->im_data);
		pframe->m_pxaxis->SavePattern(im_data1);	//Save lip pattern image to hard drive.
		pframe->m_pxaxis->OnSaveBW(im_data1,1);		//Make another saved copy of the lip pattern as a black and white image.
		

		pframe->SaveJob(pframe->CurrentJobNum);	//Save changes made to job.

		pframe->m_pxaxis->reteachColor=true;	//Set reteach color flag.

	}
	else //popup dialog box which says selected job is out of range.
	{
	//	AfxMessageBox("You must load a job first!");
			pframe->prompt_code=15;
			Prompt3 promtp3;
			promtp3.DoModal();
	}	
}

//Opens Tech window
void Insp::OnTech() 
{
	
	if(pframe->STech==true) //Check security level
	{
		Tech tech;
		tech.DoModal();
	}
	else  //Pop up dlg to say security level is insufficient.
	{
	
		Security sec;
		sec.DoModal();

		if(pframe->STech==true)
		{
			Tech tech;
			tech.DoModal();
		}
	}
	
	
}

//NOT USED
void Insp::OnTb2up() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamY-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].rearCamY <=0) theapp->jobinfo[pframe->CurrentJobNum].rearCamY=0;
	m_tb2vert.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTb2dn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamY+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].rearCamY >=300) theapp->jobinfo[pframe->CurrentJobNum].rearCamY=300;
	m_tb2vert.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTb2left() 
{
	pframe->CurrentJobb1=pframe->m_pxaxis->TB2Offset.x-=res;
	m_edittb2h.Format("%3i",pframe->m_pxaxis->TB2Offset.x);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTb2right() 
{
	pframe->CurrentJobb1=pframe->m_pxaxis->TB2Offset.x+=res;
	m_edittb2h.Format("%3i",pframe->m_pxaxis->TB2Offset.x);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTb2mup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh+=res;
	m_edittb2s.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTb2mdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh-=res;
	m_edittb2s.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Move color search box up. Used to check if color of cap is correct.
void Insp::OnUup() 
{	
	//Limit vertical position of search box to be 10 from the top.
	if(theapp->jobinfo[pframe->CurrentJobNum].userY<=10) theapp->jobinfo[pframe->CurrentJobNum].userY=10;
	theapp->jobinfo[pframe->CurrentJobNum].userY-=res;
	m_edituv.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Move color search box down. Used to check if color of cap is correct.
void Insp::OnUdn() 
{
	//Limit horizontal position of search box to be -250 from the center.
	if(theapp->jobinfo[pframe->CurrentJobNum].userY>=400)theapp->jobinfo[pframe->CurrentJobNum].userY=400;

	theapp->jobinfo[pframe->CurrentJobNum].userY+=res;
	m_edituv.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userY);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
	
}

//Move color search box left. Used to check if color of cap is correct.
void Insp::OnUleft() 
{

	if(theapp->jobinfo[pframe->CurrentJobNum].userX<=-250)theapp->jobinfo[pframe->CurrentJobNum].userX=-250;

	theapp->jobinfo[pframe->CurrentJobNum].userX-=res;
	m_edituh.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userX);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
	
}

//Move color search box right. Used to check if color of cap is correct.
void Insp::OnUright() 
{

	if(theapp->jobinfo[pframe->CurrentJobNum].userX>=250)theapp->jobinfo[pframe->CurrentJobNum].userX=250;

	theapp->jobinfo[pframe->CurrentJobNum].userX+=res;
	m_edituh.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userX);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnUsup() 
{
	pframe->CurrentJobi1=pframe->m_pxaxis->UserStrength+=res;
	m_editus.Format("%3i",pframe->m_pxaxis->UserStrength);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnUsdn() 
{
	pframe->CurrentJobi1=pframe->m_pxaxis->UserStrength-=res;
	m_editus.Format("%3i",pframe->m_pxaxis->UserStrength);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Reduces the width of the cap color search box.
void Insp::OnUsmall() 
{
	theapp->jobinfo[pframe->CurrentJobNum].userDx-=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].userDx<=0)theapp->jobinfo[pframe->CurrentJobNum].userDx=0;

	m_editsize.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userDx);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Increase the width of the cap color search box.
void Insp::OnUlarge() 
{


	theapp->jobinfo[pframe->CurrentJobNum].userDx+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].userDx>=60)theapp->jobinfo[pframe->CurrentJobNum].userDx=60;

	m_editsize.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].userDx);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTbsmaller() 
{
	
	theapp->jobinfo[pframe->CurrentJobNum].rearCamDy-=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].rearCamDy<=0) theapp->jobinfo[pframe->CurrentJobNum].rearCamDy=0;
	m_edittb2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamDy);
	UpdateData(false);
	InvalidateRect(NULL,false);
	//pframe->m_pinsp->InvalidateRect(NULL,false);
	
}

//NOT USED
void Insp::OnTblarger() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamDy+=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].rearCamDy>=25) theapp->jobinfo[pframe->CurrentJobNum].rearCamDy=25;
	m_edittb2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamDy);
	UpdateData(false);
	InvalidateRect(NULL,false);
	//pframe->m_pinsp->InvalidateRect(NULL,false);
	
}

//Select TB surface edge detection algorithm.
void Insp::OnRadio1x() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=1; //Edge Detection 
	//Update radio buttons
	CheckDlgButton(IDC_RADIO1X,1);
	CheckDlgButton(IDC_RADIO2X,0);
	CheckDlgButton(IDC_RADIO3X,0);
	CheckDlgButton(IDC_RADIO4X,0);
	CheckDlgButton(IDC_RADIO5X,0);
	CheckDlgButton(IDC_RADIO6X,0);
	CheckDlgButton(IDC_SUNNY,0);
	UpdateData(false);
}

//Select TB surface threshold detection algorithm.
void Insp::OnRadio2x() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=2; //Threshold
	CheckDlgButton(IDC_RADIO1X,0);
	CheckDlgButton(IDC_RADIO2X,1);
	//CheckDlgButton(IDC_RADIO3X,0);
	//CheckDlgButton(IDC_RADIO4X,0);
	CheckDlgButton(IDC_RADIO5X,0);
	CheckDlgButton(IDC_RADIO6X,0);
	CheckDlgButton(IDC_SUNNY,0);
	UpdateData(false);
}

//Select TB surface threshold detection algo to look for black.
void Insp::OnRadio3x() 
{
	pframe->m_pxaxis->Black=1;
	theapp->jobinfo[pframe->CurrentJobNum].dark=1;		//Resets number of pixels below the threshold.  //Threshold and LOOK FOR BLACK
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=2; //Threshold and LOOK FOR BLACK
	CheckDlgButton(IDC_RADIO1X,0);
	CheckDlgButton(IDC_RADIO2X,1);
	CheckDlgButton(IDC_RADIO3X,1);
	CheckDlgButton(IDC_RADIO4X,0);
	CheckDlgButton(IDC_RADIO5X,0);
	CheckDlgButton(IDC_RADIO6X,0);
	CheckDlgButton(IDC_SUNNY,0);
	UpdateData(false);
	
}

//Select TB surface threshold detection algo to look for white.
void Insp::OnRadio4x() 
{
	pframe->m_pxaxis->Black=0;
	theapp->jobinfo[pframe->CurrentJobNum].dark=0;	   //Resets number of pixels below the threshold.		//Threshold and LOOK FOR WHITE
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=2;//Threshold and LOOK FOR WHITE
	CheckDlgButton(IDC_RADIO1X,0);
	CheckDlgButton(IDC_RADIO2X,1);
	CheckDlgButton(IDC_RADIO3X,0);
	CheckDlgButton(IDC_RADIO4X,1);
	CheckDlgButton(IDC_RADIO5X,0);
	CheckDlgButton(IDC_RADIO6X,0);
	CheckDlgButton(IDC_SUNNY,0);
	UpdateData(false);
	
}

//Select TB surface shaded average detection algorithm. Does not appear to be used.
void Insp::OnRadio5x() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=3;
	CheckDlgButton(IDC_RADIO1X,0);
	CheckDlgButton(IDC_RADIO2X,0);
	CheckDlgButton(IDC_RADIO3X,0);
	CheckDlgButton(IDC_RADIO4X,0);
	CheckDlgButton(IDC_RADIO5X,1);
	CheckDlgButton(IDC_RADIO6X,0);
	CheckDlgButton(IDC_SUNNY,0);
	UpdateData(false);
}

//Select TB surface Integrity check detection algorithm.
void Insp::OnRadio6x() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=4;
	CheckDlgButton(IDC_RADIO1X,0);
	CheckDlgButton(IDC_RADIO2X,0);
	CheckDlgButton(IDC_RADIO3X,0);
	CheckDlgButton(IDC_RADIO4X,0);
	CheckDlgButton(IDC_RADIO5X,0);
	CheckDlgButton(IDC_RADIO6X,1);
	CheckDlgButton(IDC_SUNNY,0);
	UpdateData(false);
	
}

//Select TB surface color compare detection algorithm.
void Insp::OnSunny() 
{

	theapp->jobinfo[pframe->CurrentJobNum].tBandType=5; //Compare Color
	CheckDlgButton(IDC_RADIO1X,0);
	CheckDlgButton(IDC_RADIO2X,0);
	CheckDlgButton(IDC_RADIO3X,0); 
	CheckDlgButton(IDC_RADIO4X,0);
	CheckDlgButton(IDC_RADIO5X,0);
	CheckDlgButton(IDC_RADIO6X,0);
	CheckDlgButton(IDC_SUNNY,1);
	UpdateData(false);
	
}

//NOT USED
void Insp::OnManual() 
{
	m_lleftsp.ShowWindow(SW_RESTORE);//SW_MINIMIZESW_SHOWNA
	m_lrightsp.ShowWindow(SW_RESTORE);

	m_tb2upsp.ShowWindow(SW_RESTORE);
	m_tb2dnsp.ShowWindow(SW_RESTORE);
	m_mupsp.ShowWindow(SW_RESTORE);
	m_mdnsp.ShowWindow(SW_RESTORE);
	m_nupsp.ShowWindow(SW_RESTORE);
	m_ndnsp.ShowWindow(SW_RESTORE);
	m_tblargersp.ShowWindow(SW_RESTORE);
	m_tbsmallersp.ShowWindow(SW_RESTORE);
	m_tbupsp.ShowWindow(SW_RESTORE);
	m_tbdnsp.ShowWindow(SW_RESTORE);
	m_tbrightsp.ShowWindow(SW_RESTORE);
	m_tbleftsp.ShowWindow(SW_RESTORE);
	m_fupsp.ShowWindow(SW_RESTORE);
	m_fdnsp.ShowWindow(SW_RESTORE);
	m_frightsp.ShowWindow(SW_RESTORE);
	m_fleftsp.ShowWindow(SW_RESTORE);

	m_uupsp.ShowWindow(SW_RESTORE);
	m_udnsp.ShowWindow(SW_RESTORE);
	m_urightsp.ShowWindow(SW_RESTORE);
	m_uleftsp.ShowWindow(SW_RESTORE);
	m_ulargesp.ShowWindow(SW_RESTORE);
	m_usmallsp.ShowWindow(SW_RESTORE);
	manual=true;
	
}

//Moves the phantom lip search area boxes up.
void Insp::OnLipup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipy-=1;
//	m_editlippos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].lipy);
	InvalidateRect(NULL,false);
	UpdateData(false);
}

//Moves the phantom lip search area boxes down.
void Insp::OnLipdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipy+=1;
//	m_editlippos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].lipy);
	InvalidateRect(NULL,false);
	UpdateData(false);
}

//Moves the phantom lip search area boxes left.
void Insp::OnLipleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipx-=1;
//	m_editlippos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].lipx);
	InvalidateRect(NULL,false);
	UpdateData(false);

}

//Moves the phantom lip search area boxes right.
void Insp::OnLipright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipx+=1;
//	m_editlippos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].lipx);
	InvalidateRect(NULL,false);
	UpdateData(false);
	
}

void Insp::OnLipup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipYOffset-=1;
	//m_editlippos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].lipy);
	InvalidateRect(NULL,false);
	UpdateData(false);
	
}

//Moves the right phantom lip search area box down.
void Insp::OnLipdn2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipYOffset+=1;
	//m_editlippos.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].lipy);
	InvalidateRect(NULL,false);
	UpdateData(false);
	
}

//Makes phantom lip search area box taller.
void Insp::OnLipareataller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipSize+=2;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].lipSize>=0)theapp->jobinfo[pframe->CurrentJobNum].lipSize=0;
	
	InvalidateRect(NULL,false);
	UpdateData(false);
}

//Makes phantom lip search area box shorter.
void Insp::OnLipareasmaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lipSize-=2;

	if(theapp->jobinfo[pframe->CurrentJobNum].lipSize<=-30)theapp->jobinfo[pframe->CurrentJobNum].lipSize=-30;
	
	InvalidateRect(NULL,false);
	UpdateData(false);
}

//Select color for TB surface color compare detection algorithm.
void Insp::OnSetupcolor() 
{
	TBColor tbcolor;
	tbcolor.DoModal();
	
}

//Toggles extra lighting on/off. Used to help see fill level in opaque bottles.
void Insp::OnXtralight() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].xtraLight==true)	//If extra light is on
	{
		theapp->jobinfo[pframe->CurrentJobNum].xtraLight=false;	//Turn extra light off.
		if(theapp->NoSerialComm==false) 
		{
	
	
			//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
			//pframe->m_pserial->SendMessage(WM_QUE,0,440); 
		}
		CheckDlgButton(IDC_XTRALIGHT,0);	

	}
	else//Opposite of above.
	{
		theapp->jobinfo[pframe->CurrentJobNum].xtraLight=true;

		if(theapp->NoSerialComm==false) 
		{
	

			//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
			//pframe->m_pserial->SendMessage(WM_QUE,0,430);
		}
		CheckDlgButton(IDC_XTRALIGHT,1);
	}
	UpdateData(false);	//Update gui.
}

//Toggle the use of both left and right or only the left phantom lip search box.
void Insp::OnUseleft() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].useLeft==true)
	{
		CheckDlgButton(IDC_CHECK1,0);
		theapp->jobinfo[pframe->CurrentJobNum].useLeft=false;
	}
	else
	{
		CheckDlgButton(IDC_CHECK1,1);
		theapp->jobinfo[pframe->CurrentJobNum].useLeft=true;
	}
	UpdateData(false);	
}
