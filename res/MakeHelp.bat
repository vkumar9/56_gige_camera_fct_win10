@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by IAMS.HPJ. >"hlp\iams.hm"
echo. >>"hlp\iams.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\iams.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\iams.hm"
echo. >>"hlp\iams.hm"
echo // Prompts (IDP_*) >>"hlp\iams.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\iams.hm"
echo. >>"hlp\iams.hm"
echo // Resources (IDR_*) >>"hlp\iams.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\iams.hm"
echo. >>"hlp\iams.hm"
echo // Dialogs (IDD_*) >>"hlp\iams.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\iams.hm"
echo. >>"hlp\iams.hm"
echo // Frame Controls (IDW_*) >>"hlp\iams.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\iams.hm"
REM -- Make help for Project IAMS


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\iams.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\iams.hlp" goto :Error
if not exist "hlp\iams.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\iams.hlp" Debug
if exist Debug\nul copy "hlp\iams.cnt" Debug
if exist Release\nul copy "hlp\iams.hlp" Release
if exist Release\nul copy "hlp\iams.cnt" Release
echo.
goto :done

:Error
echo hlp\iams.hpj(1) : error: Problem encountered creating help file

:done
echo.
