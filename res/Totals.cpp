// Totals.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Totals.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Totals dialog


Totals::Totals(CWnd* pParent /*=NULL*/)
	: CDialog(Totals::IDD, pParent)
{
	//{{AFX_DATA_INIT(Totals)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Totals::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Totals)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Totals, CDialog)
	//{{AFX_MSG_MAP(Totals)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Totals message handlers

BOOL Totals::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_ptotals=this;
	theapp=(CCapApp*)AfxGetApp();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Totals::OnOK() 
{
	pframe->ResetTotals();
	
	CDialog::OnOK();
}
