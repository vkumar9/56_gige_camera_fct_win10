# Microsoft Developer Studio Project File - Name="Cap" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Cap - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Cap.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Cap.mak" CFG="Cap - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Cap - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Cap - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Cap - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /w /W0 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "NO_FOSDKLIB_FILE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib quartz.lib pgrflycapture.lib pgrflycapturegui.lib vfw32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Cap - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /w /W0 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fr /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 pgrflycapture.lib pgrflycapturegui.lib vfw32.lib /nologo /subsystem:windows /incremental:no /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Cap - Win32 Release"
# Name "Cap - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Advanced.cpp
# End Source File
# Begin Source File

SOURCE=.\Band.cpp
# End Source File
# Begin Source File

SOURCE=.\Blowoff.cpp
# End Source File
# Begin Source File

SOURCE=.\BottleDown.cpp
# End Source File
# Begin Source File

SOURCE=.\Camera.cpp
# End Source File
# Begin Source File

SOURCE=.\Cap.cpp
# End Source File
# Begin Source File

SOURCE=.\Cap.odl
# End Source File
# Begin Source File

SOURCE=.\Cap.rc
# End Source File
# Begin Source File

SOURCE=.\CapDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CapView.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\Data.cpp
# End Source File
# Begin Source File

SOURCE=.\Database.cpp
# End Source File
# Begin Source File

SOURCE=.\DigIO.cpp
# End Source File
# Begin Source File

SOURCE=.\Filler.cpp
# End Source File
# Begin Source File

SOURCE=.\Insp.cpp
# End Source File
# Begin Source File

SOURCE=.\Job.cpp
# End Source File
# Begin Source File

SOURCE=.\Light.cpp
# End Source File
# Begin Source File

SOURCE=.\Lim.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\Modify.cpp
# End Source File
# Begin Source File

SOURCE=.\Motor.cpp
# End Source File
# Begin Source File

SOURCE=.\MoveHead.cpp
# End Source File
# Begin Source File

SOURCE=.\mscomm.cpp
# End Source File
# Begin Source File

SOURCE=.\Photoeye.cpp
# End Source File
# Begin Source File

SOURCE=.\Pics.cpp
# End Source File
# Begin Source File

SOURCE=.\Prompt.cpp
# End Source File
# Begin Source File

SOURCE=.\SaveAs.cpp
# End Source File
# Begin Source File

SOURCE=.\Security.cpp
# End Source File
# Begin Source File

SOURCE=.\Serial.cpp
# End Source File
# Begin Source File

SOURCE=.\Setup.cpp
# End Source File
# Begin Source File

SOURCE=.\SPLITTER.CPP
# End Source File
# Begin Source File

SOURCE=.\Status.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TBColor.cpp
# End Source File
# Begin Source File

SOURCE=.\Tech.cpp
# End Source File
# Begin Source File

SOURCE=.\View.cpp
# End Source File
# Begin Source File

SOURCE=.\XAxisView.cpp
# End Source File
# Begin Source File

SOURCE=.\Xtra.cpp
# End Source File
# Begin Source File

SOURCE=.\xview.cpp
# End Source File
# Begin Source File

SOURCE=.\YAxisView.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Advanced.h
# End Source File
# Begin Source File

SOURCE=.\Align.h
# End Source File
# Begin Source File

SOURCE=.\Band.h
# End Source File
# Begin Source File

SOURCE=.\Blowoff.h
# End Source File
# Begin Source File

SOURCE=.\BottleDown.h
# End Source File
# Begin Source File

SOURCE=.\Camera.h
# End Source File
# Begin Source File

SOURCE=.\Cap.h
# End Source File
# Begin Source File

SOURCE=.\CapDoc.h
# End Source File
# Begin Source File

SOURCE=.\CapView.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\Data.h
# End Source File
# Begin Source File

SOURCE=.\Database.h
# End Source File
# Begin Source File

SOURCE=.\DigIO.h
# End Source File
# Begin Source File

SOURCE=.\Filler.h
# End Source File
# Begin Source File

SOURCE=.\FrameRate.h
# End Source File
# Begin Source File

SOURCE=.\FSTREAM.H
# End Source File
# Begin Source File

SOURCE=.\Histo.h
# End Source File
# Begin Source File

SOURCE=.\Insp.h
# End Source File
# Begin Source File

SOURCE=.\Job.h
# End Source File
# Begin Source File

SOURCE=.\Light.h
# End Source File
# Begin Source File

SOURCE=.\Lim.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Modify.h
# End Source File
# Begin Source File

SOURCE=.\Motor.h
# End Source File
# Begin Source File

SOURCE=.\MoveHead.h
# End Source File
# Begin Source File

SOURCE=.\mscomm.h
# End Source File
# Begin Source File

SOURCE=.\Photoeye.h
# End Source File
# Begin Source File

SOURCE=.\Pics.h
# End Source File
# Begin Source File

SOURCE=.\Prompt.h
# End Source File
# Begin Source File

SOURCE=.\Prompt3.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SaveAs.h
# End Source File
# Begin Source File

SOURCE=.\Security.h
# End Source File
# Begin Source File

SOURCE=.\Serial.h
# End Source File
# Begin Source File

SOURCE=.\Setup.h
# End Source File
# Begin Source File

SOURCE=.\SideView.h
# End Source File
# Begin Source File

SOURCE=.\SPLITTER.H
# End Source File
# Begin Source File

SOURCE=.\Status.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TBColor.h
# End Source File
# Begin Source File

SOURCE=.\Tech.h
# End Source File
# Begin Source File

SOURCE=.\Thread.h
# End Source File
# Begin Source File

SOURCE=.\Totals.h
# End Source File
# Begin Source File

SOURCE=.\View.h
# End Source File
# Begin Source File

SOURCE=.\XAxisView.h
# End Source File
# Begin Source File

SOURCE=.\Xtra.h
# End Source File
# Begin Source File

SOURCE=.\xview.h
# End Source File
# Begin Source File

SOURCE=.\YAxisView.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Cap.ico
# End Source File
# Begin Source File

SOURCE=.\res\Cap.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CapDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ej.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MCS_LOGO.BMP
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\Cap.reg
# End Source File
# Begin Source File

SOURCE=.\codeex.txt
# End Source File
# Begin Source File

SOURCE=.\datab.txt
# End Source File
# Begin Source File

SOURCE=.\job.txt
# End Source File
# Begin Source File

SOURCE=.\m.txt
# End Source File
# Begin Source File

SOURCE=.\neuralpattern.txt
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\saved.txt
# End Source File
# Begin Source File

SOURCE=.\xtra.txt
# End Source File
# End Target
# End Project
# Section Cap : {AACB416F-A171-11D3-AD09-00104B107B86}
# 	2:5:Class:C_Office97Spin
# 	2:10:HeaderFile:_office97spin.h
# 	2:8:ImplFile:_office97spin.cpp
# End Section
# Section Cap : {648A5600-2C6E-101B-82B6-000000000014}
# 	2:21:DefaultSinkHeaderFile:mscomm.h
# 	2:16:DefaultSinkClass:CMSComm
# End Section
# Section Cap : {E6E17E90-DF38-11CF-8E74-00A0C90F26F8}
# 	2:5:Class:CMSComm
# 	2:10:HeaderFile:mscomm.h
# 	2:8:ImplFile:mscomm.cpp
# End Section
# Section Cap : {083039C2-13F4-11D1-8B7E-0000F8754DA1}
# 	2:5:Class:CCommonDialog1
# 	2:10:HeaderFile:commondialog.h
# 	2:8:ImplFile:commondialog.cpp
# End Section
# Section Cap : {AACB4170-A171-11D3-AD09-00104B107B86}
# 	2:21:DefaultSinkHeaderFile:_office97spin.h
# 	2:16:DefaultSinkClass:C_Office97Spin
# End Section
# Section Cap : {BEF6E003-A874-101A-8BBA-00AA00300CAB}
# 	2:5:Class:COleFont
# 	2:10:HeaderFile:font.h
# 	2:8:ImplFile:font.cpp
# End Section
# Section Cap : {F9043C85-F6F2-101A-A3C9-08002B2F49FB}
# 	2:21:DefaultSinkHeaderFile:commondialog.h
# 	2:16:DefaultSinkClass:CCommonDialog1
# End Section
