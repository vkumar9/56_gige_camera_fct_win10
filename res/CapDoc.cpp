// CapDoc.cpp : implementation of the CCapDoc class
//

#include "stdafx.h"
#include "Cap.h"


#include "CapDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCapDoc

IMPLEMENT_DYNCREATE(CCapDoc, CDocument)

BEGIN_MESSAGE_MAP(CCapDoc, CDocument)
	//{{AFX_MSG_MAP(CCapDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CCapDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CCapDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ICap to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {FD9A4023-EFBD-4EDB-A2BF-5CD0D2D51261}
static const IID IID_ICap =
{ 0xfd9a4023, 0xefbd, 0x4edb, { 0xa2, 0xbf, 0x5c, 0xd0, 0xd2, 0xd5, 0x12, 0x61 } };

BEGIN_INTERFACE_MAP(CCapDoc, CDocument)
	INTERFACE_PART(CCapDoc, IID_ICap, Dispatch)
END_INTERFACE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCapDoc construction/destruction

CCapDoc::CCapDoc()
{
	// TODO: add one-time construction code here

	EnableAutomation();

	AfxOleLockApp();
}

CCapDoc::~CCapDoc()
{
	AfxOleUnlockApp();
}

BOOL CCapDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCapDoc serialization

void CCapDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCapDoc diagnostics

#ifdef _DEBUG
void CCapDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCapDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCapDoc commands
