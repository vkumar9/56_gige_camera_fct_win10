#if !defined(AFX_TOTALS_H__2F571FD4_B369_4AEC_8C71_BB7B86D747FB__INCLUDED_)
#define AFX_TOTALS_H__2F571FD4_B369_4AEC_8C71_BB7B86D747FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Totals.h : header file
//
friend class CMainFrame;
friend class CCapApp; 
/////////////////////////////////////////////////////////////////////////////
// Totals dialog

class Totals : public CDialog
{
// Construction
public:
	Totals(CWnd* pParent = NULL);   // standard constructor

	CCapApp *theapp;
	CMainFrame* pframe;

// Dialog Data
	//{{AFX_DATA(Totals)
	enum { IDD = IDD_TOTALS };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Totals)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Totals)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOTALS_H__2F571FD4_B369_4AEC_8C71_BB7B86D747FB__INCLUDED_)
