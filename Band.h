#if !defined(AFX_BAND_H__78C9C62F_DA3F_4D6D_AE21_77AB7F878E61__INCLUDED_)
#define AFX_BAND_H__78C9C62F_DA3F_4D6D_AE21_77AB7F878E61__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Band.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Band dialog

class Band : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:
	void refresh();
	Band(CWnd* pParent = NULL);   // standard constructor
CMainFrame* pframe;
CCapApp* theapp;
// Dialog Data
	//{{AFX_DATA(Band)
	enum { IDD = IDD_REARBAND };
	CString	m_edittaller;		//Stores the height of all rear (cameras 2, 3) tamper band search boxes.
	CString	m_editsen;			//NOT USED
	CString	m_editsen2;			//Stores threshold for rear (cameras 2, 3) tamper band search boxes.
	CString	m_edit2;			//Stores lowest vertical position of all rear (cameras 2,3) tamper band search boxes.
	CString	m_editband2size;	//Stores the width of all rear (cameras 2, 3) tamper band search boxes.
	CString	m_choice;			//Stores the value selected in inspection configuration for tamper band surface algorithm.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Band)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Band)
	afx_msg void OnUp5();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnDn5();
	afx_msg void OnUp4();
	afx_msg void OnDn4();
	afx_msg void OnUp3();
	afx_msg void OnDn3();
	afx_msg void OnUp2();
	afx_msg void OnDn2();
	afx_msg void OnUp1();
	afx_msg void OnDn1();
	afx_msg void OnUpr5();
	afx_msg void OnDnr5();
	afx_msg void OnUpr4();
	afx_msg void OnDnr4();
	afx_msg void OnUpr3();
	afx_msg void OnDnr3();
	afx_msg void OnUpr2();
	afx_msg void OnDnr2();
	afx_msg void OnUpr1();
	afx_msg void OnDnr1();
	afx_msg void OnTaller();
	afx_msg void OnShorter();
	afx_msg void OnUpall();
	afx_msg void OnDnall2();
	afx_msg void OnSenmore();
	afx_msg void OnSenmore2();
	afx_msg void OnDnall();
	afx_msg void OnLesssen();
	afx_msg void OnMoresen();
	afx_msg void OnLeftleft();
	afx_msg void OnLeftright();
	afx_msg void OnRightleft();
	afx_msg void OnRightright();
	afx_msg void OnWider();
	afx_msg void OnShort();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAND_H__78C9C62F_DA3F_4D6D_AE21_77AB7F878E61__INCLUDED_)
