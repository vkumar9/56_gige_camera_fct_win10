#if !defined(AFX_BOTTLEDOWN_H__A02291E2_51FA_4F87_87D3_6D0BAC15BCCB__INCLUDED_)
#define AFX_BOTTLEDOWN_H__A02291E2_51FA_4F87_87D3_6D0BAC15BCCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BottleDown.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// BottleDown dialog

class BottleDown : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:
	CString m_enable;	//String which stores bottle down sensor enabled text.
	BottleDown(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(BottleDown)
	enum { IDD = IDD_BOTTLEDOWN };
	CString	m_dbedit;
	//}}AFX_DATA

	CMainFrame* pframe;
	CCapApp* theapp;

	long Value;			//NOT USED
	int Function;		//NOT USED
	int CntrBits;		//NOT USED
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BottleDown)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BottleDown)
	virtual BOOL OnInitDialog();
	afx_msg void OnBotdn();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDbmore();
	afx_msg void OnDbless();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOTTLEDOWN_H__A02291E2_51FA_4F87_87D3_6D0BAC15BCCB__INCLUDED_)
