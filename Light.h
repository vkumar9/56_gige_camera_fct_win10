#if !defined(AFX_LIGHT_H__B6520F71_0F20_417D_8AFF_FC416739095B__INCLUDED_)
#define AFX_LIGHT_H__B6520F71_0F20_417D_8AFF_FC416739095B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Light.h : header file
//
//#include "pcrcam.h"
/////////////////////////////////////////////////////////////////////////////
// Light dialog
class Light : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;

public:
	int Cams;				//Used to track for which cameras the gain is being adjusted.
	void UpdatePLC();	//NOT USED
	int OldLevel;			//Not used
	int LightLevel1;		//gain lvl for cam1
	int LightLevel2;		//gain lvl for cam2
	int LightLevel3;		//gain lvl for cam3
	Light(CWnd* pParent = NULL);   // standard constructor

	CCapApp *theapp;
	CMainFrame* pframe;
//CICamera *cam0;
// Dialog Data
	//{{AFX_DATA(Light)
	enum { IDD = IDD_LIGHTS };
	CSliderCtrl	m_slide3;		//Slider control interface for camera3.
	CSliderCtrl	m_slide2;		//Slider control interface for camera2.
	CSliderCtrl	m_slide;		//Slider control interface for camera1.
	CString	m_lightval;			//GUI variable which stores the display variable for cameras 1, 2 and 3.
	CString	m_lightval2;		//GUI variable which stores the display variable for ring light.
	CString	m_lightval3;		//GUI variable which stores the intensity of the back lights.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Light)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Light)
	virtual BOOL OnInitDialog();
	afx_msg void OnLight();
	afx_msg void OnDark();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnAll();
	afx_msg void OnFront();
	afx_msg void OnLeft();
	afx_msg void OnRight();
	afx_msg void OnReleasedcaptureSlider1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSlider2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSlider3(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLight2();
	afx_msg void OnDark2();
	afx_msg void OnLight3();
	afx_msg void OnDark3();
	afx_msg void OnRing();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIGHT_H__B6520F71_0F20_417D_8AFF_FC416739095B__INCLUDED_)
