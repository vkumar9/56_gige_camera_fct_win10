// Prompt.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Prompt.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "capview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Prompt dialog


Prompt::Prompt(CWnd* pParent /*=NULL*/)
	: CDialog(Prompt::IDD, pParent)
{
	//{{AFX_DATA_INIT(Prompt)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Prompt::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Prompt)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Prompt, CDialog)
	//{{AFX_MSG_MAP(Prompt)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOFF, OnOff)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Prompt message handlers

//Start ejecting all bottles.
void Prompt::OnOK() 
{
	pframe->m_pxaxis->RejectAll=true;					//Set the eject all flag.
	pframe->m_pview->EjectAll=true;						//Set the eject all flag.
	pframe->m_pview->m_status="Reject All is on!!!";	//Update status to show that all bottles are being ejected.
	SetTimer(1,500,NULL);								//Start timer which toggles the blinking red color of this dialog box.
	//CDialog::OnOK();
}

BOOL Prompt::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pprompt=this;	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Stop ejecting all bottles.
void Prompt::OnCancel() 
{
	pframe->m_pview->EjectAll=false;				//Clear the eject all flag.
	pframe->m_pxaxis->RejectAll=false;				//Clear the eject all flag.
	pframe->m_pview->m_status="Reject All is off";	//Update status to show that all bottles are no longer being ejected.
	KillTimer(1);									//Stop timer which toggles the blinking red color of this dialog box.
	CDialog::OnCancel();
}

//Toggles the background of this dialog box between red and gray. Used when all bottles are being rejected.
void Prompt::RePaint() 
{

	//Set coordinates of red rectangle.	
	int x=10;
	int y=10;
	int x2=x+30;
	int y2=y+130;
	CRect a, b, c, d;
	//CRgn s;

	//Set colors and brushes
	COLORREF sqrred, sqrgreen, sqrgray;
	CBrush Colorr, Colorg, Colorb;
	CClientDC ColorRect(this);

	//aa.SetRect(83,90,125,125);

	//Position Rectangles.	
	a.SetRect(x,y,x2,y2);
	b.SetRect(x+230,y,x2+230,y2);

	c.SetRect(x,y,x2+230,y+3);
	d.SetRect(x,y2-3,x2+230,y2);
	
	sqrred=RGB(200,0,0);
	sqrgreen=RGB(0,200,0);
	sqrgray=RGB(200,200,200);
	Colorr.CreateSolidBrush(sqrred);
	Colorg.CreateSolidBrush(sqrgreen);
	Colorb.CreateSolidBrush(sqrgray);

	//Toggle colors.
	switch (BoxColor)
	{
		case 1: 
		
			ColorRect.SelectObject(&Colorr);	
			ColorRect.FillRect(a,&Colorr);
			ColorRect.FillRect(b,&Colorr);
			ColorRect.FillRect(c,&Colorr);
			ColorRect.FillRect(d,&Colorr);
			break;

		case 2: 
		
			ColorRect.SelectObject(&Colorb);	
			ColorRect.FillRect(a,&Colorb);
			ColorRect.FillRect(b,&Colorb);
			ColorRect.FillRect(c,&Colorb);
			ColorRect.FillRect(d,&Colorb);
			break;
	}
}

void Prompt::OnTimer(UINT nIDEvent) 
{
	//Timer used to toggle the background color of this dialog box, between read and gray. 
	if (nIDEvent==1) 
	{
		
		if(BoxColor==1) {BoxColor=2;}else{BoxColor=1;}
		RePaint();
		InvalidateRect(NULL,false);
	}	
	CDialog::OnTimer(nIDEvent);
}


//Stop ejecting all bottles.
void Prompt::OnOff() 
{
	pframe->m_pview->EjectAll=false;				//Clear the eject all flag.
	pframe->m_pxaxis->RejectAll=false;				//Clear the eject all flag.
	pframe->m_pview->m_status="Reject All is off";	//Update status to show that all bottles are no longer being ejected.
	KillTimer(1);									//Stop timer which toggles the blinking red color of this dialog box.
}
