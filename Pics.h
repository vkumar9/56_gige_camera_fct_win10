#if !defined(AFX_PICS_H__799493F3_E137_49ED_AF36_F2C1F190E33C__INCLUDED_)
#define AFX_PICS_H__799493F3_E137_49ED_AF36_F2C1F190E33C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Pics.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Pics dialog

class Pics : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;
public:
	Pics(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Pics)
	enum { IDD = IDD_PICS };
	CString	m_editrecall;
	//}}AFX_DATA
	CCapApp *theapp;
	CMainFrame* pframe;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Pics)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Pics)
	virtual BOOL OnInitDialog();
	afx_msg void OnRecall();
	virtual void OnOK();
	afx_msg void OnLast();
	virtual void OnCancel();
	afx_msg void OnLast2();
	afx_msg void OnLast3();
	afx_msg void OnLast4();
	afx_msg void OnSaveone1c();
	afx_msg void OnSaveone1();
	afx_msg void OnSaveone2c();
	afx_msg void OnSaveone2();
	afx_msg void OnSaveone3c();
	afx_msg void OnSaveone3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICS_H__799493F3_E137_49ED_AF36_F2C1F190E33C__INCLUDED_)
