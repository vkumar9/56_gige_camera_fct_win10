#if !defined(AFX_PROMPT1_H__172AFE3D_E307_4228_9110_47A1728B4D25__INCLUDED_)
#define AFX_PROMPT1_H__172AFE3D_E307_4228_9110_47A1728B4D25__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Prompt1.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// Prompt1 dialog

class Prompt1 : public CDialog
{
// Construction
friend class PicRecal;
public:
	Prompt1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Prompt1)
	enum { IDD = IDD_DIALOG1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Prompt1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Prompt1)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROMPT1_H__172AFE3D_E307_4228_9110_47A1728B4D25__INCLUDED_)
