#if !defined(AFX_ZAXISVIEW_H__99EE49BD_5A40_11D7_BD22_00024405BDD6__INCLUDED_)
#define AFX_ZAXISVIEW_H__99EE49BD_5A40_11D7_BD22_00024405BDD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ZAxisView.h : header file
//
#include <pcrcam.h>
#include "mainfrm.h"
/////////////////////////////////////////////////////////////////////////////
// CZAxisView view

typedef struct  {
	int x; 
	int y; 
	int dx; 
	int dy;} HVLINE;

class CZAxisView : public CView
{
protected:
	CZAxisView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CZAxisView)

// Attributes
public:
	int ztot;
	BYTE* CropIt(BYTE* Image, int x, int y, int dx, int dy);
	void Inspect(BYTE *im_src);
	HVLINE TopEdge(BYTE *im_srcHV, int x, int y, int dx, int dy);
	void OnSnap();
	void LoadCam();
	
	CMartechApp *theapp;
	CMainFrame* pframe;
	
//	BYTE *imgBuf2;

	
	CICamera *cam2;
	DWORD areaDxBW;	// DX of the image area being processed
	DWORD areaDyBW;  // DY of the image area being processed
	
	
	CImgConn *imgConn2;

	BYTE* im_src2;
	BYTE *im_srcHV;
	BYTE* Image;

	int Thresh;
	int Defects;
	int Size[100];
	int LocY[100];
	int LocX[100];
// Operations
public:
	int BumpLimit;
	int BumpDefect;
	bool CurrentDefect;
	int gotone;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CZAxisView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CZAxisView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CZAxisView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ZAXISVIEW_H__99EE49BD_5A40_11D7_BD22_00024405BDD6__INCLUDED_)
