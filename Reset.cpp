// Reset.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Reset.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Reset dialog


Reset::Reset(CWnd* pParent /*=NULL*/)
	: CDialog(Reset::IDD, pParent)
{
	//{{AFX_DATA_INIT(Reset)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Reset::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Reset)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Reset, CDialog)
	//{{AFX_MSG_MAP(Reset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Reset message handlers

void Reset::OnOK() 
{
	pframe->ResetTotals();	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL Reset::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_preset=this;
	theapp=(CCapApp*)AfxGetApp();	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
