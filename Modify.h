#if !defined(AFX_MODIFY_H__AB86F5B7_6566_4789_BB90_091B16BBB445__INCLUDED_)
#define AFX_MODIFY_H__AB86F5B7_6566_4789_BB90_091B16BBB445__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Modify.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Modify dialog

class Modify : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;
friend class FillerGraph;
public:
	bool newTarget;		//Flag indicating if a new cap color should be taught.
	bool toggle;		//NOT USED
	void UpdateDisplay();
	int res;			//Resolution (step size) of adjustments made by the tab controls
	Modify(CWnd* pParent = NULL);   // standard constructor

	CCapApp *theapp;
	CMainFrame* pframe;

	CString m_enable;
// Dialog Data
	//{{AFX_DATA(Modify)
	enum { IDD = IDD_MODIFY };
	CButton	m_cfilter;				//NOT USED
	CButton	m_cfiltermore;			//NOT USED
	CButton	m_cfilterless;			//NOT USED
	CEdit	m_ceditcutoff;			//NOT USED
	CStatic	m_colorinst;			//Controls for interfacing with color compare label under tamper band surface threshold.
	CStatic	m_sca;					//Controls for interfacing with sports cap search area label
	CStatic	m_scs;					//Controls for interfacing with sports cap sensitivity label
	CEdit	m_sportcapeditc;		//Controls for interfacing with sports cap search area edit box.
	CButton	m_sportcapdn;			//Controls for interfacing with sports cap search area down button.
	CButton	m_sportcapless;			//Control for interfacing with sports cap sensitivity search area up button.
	CButton	m_sportcapmore;			//Control for interfacing with sports cap sensitivity search area down button.
	CButton	m_sportcapup;			//Controls for interfacing with sports cap search area up button.
	CButton	m_sportcapframe;		//Control for interfacing with sports cap search parameters group box.
	CButton	m_tbcolor;				//NOT USED
	CEdit	m_editec;				//NOT USED
	CEdit	m_editsc;				//NOT USED
	CButton	m_sdn;					//NOT USED
	CButton	m_sup;					//NOT USED
	CString	m_capneck;				//Stores position of middle of neck selection line.
	CString	m_capmiddle;			//Stores position of middle of cap selection line.
	CString	m_captop;				//Stores position of left edge selection line.
	CString	m_edits;				//Stores tamper band front (camera1) surface threshold
	CString	m_edite;				//Stores tamper band edge threshold
	CString	m_edits2;				//Stores tamper band rear (cameras 2 and 3) surface threshold
	CString	m_choice;				//Used to display the type of surface detection algorithm selected.
	CString	m_sportcapedit;			//Stores sports cap search sensitivity.
	CString	m_editcutoff;			//Stores extra light filter value
	CString	m_diff_var;				//Stores no cap sensitivity value
	CString	m_limit;				//Stores no cap differential limit value
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Modify)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Modify)
	afx_msg void OnLleft();
	afx_msg void OnLright();
	afx_msg void OnMup();
	afx_msg void OnMdn();
	afx_msg void OnNup();
	afx_msg void OnNdn();
	afx_msg void OnTbleft();
	afx_msg void OnTbup();
	afx_msg void OnTbdn();
	afx_msg void OnTbright();
	afx_msg void OnSup();
	afx_msg void OnSdn();
	afx_msg void OnFup();
	afx_msg void OnFdn();
	virtual BOOL OnInitDialog();
	afx_msg void OnTbup2();
	afx_msg void OnTbdn2();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnFillleft();
	afx_msg void OnFillright();
	afx_msg void OnSup2();
	afx_msg void OnSdn2();
	afx_msg void OnFast();
	afx_msg void OnColor();
	afx_msg void OnPaint();
	afx_msg void OnTbcolor();
	afx_msg void OnSup3();
	afx_msg void OnSdn3();
	afx_msg void OnSportcapUp();
	afx_msg void OnSportcapDn();
	afx_msg void OnSportcapMore();
	afx_msg void OnSportcapLess();
	afx_msg void OnFiltermore();
	afx_msg void OnFilterless();
	afx_msg void OnViewGraph();
	afx_msg void OnCheckRearHeight();
	afx_msg void OnSavePics();
	afx_msg void OnIncrDiff();
	afx_msg void OnDcrDiff();
	afx_msg void OnIncrLimit();
	afx_msg void OnDcrlimit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODIFY_H__AB86F5B7_6566_4789_BB90_091B16BBB445__INCLUDED_)
