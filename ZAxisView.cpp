// ZAxisView.cpp : implementation file
//

#include "stdafx.h"
#include "Martech.h"
#include "ZAxisView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//CAM_ATTR attrBW2;
//HIFCGRAB GrabID2;
HVLINE TubTop;
/////////////////////////////////////////////////////////////////////////////
// CZAxisView

IMPLEMENT_DYNCREATE(CZAxisView, CView)

CZAxisView::CZAxisView()
{
}

CZAxisView::~CZAxisView()
{
}

void CZAxisView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->pzaxis=this;
	theapp=(CMartechApp*)AfxGetApp();
	BumpLimit=theapp->SavedBLim;
	BumpDefect=0;
	Thresh=150;
	
}

void CZAxisView::LoadCam()
{

	theapp=(CMartechApp*)AfxGetApp();
	cam2=(CICamera*)theapp->m_cam2;
	cam2->GetAttr(&attrBW2);
	
	areaDyBW=attrBW2.dwHeight;
	areaDxBW=attrBW2.dwWidth;

	//imgBuf0 = (BYTE*)GlobalAlloc(GMEM_FIXED,areaDxBW * areaDyBW  );//
	//imgBuf2 = (BYTE*)GlobalAlloc(GMEM_FIXED,areaDxBW * areaDyBW  * 3);

//	imgConn2 = new CImgConn(imgBuf2,640,480,8, m_hWnd,IFC_MONO,IFC_DIB_SINK );
	//imgConn = new CImgConn(imgBuf1,640,480,8, m_hWnd,IFC_MONO,IFC_DIB_SINK );
	//im_sr=im_create(IM_BYTE,areaDxBW ,areaDyBW );
	//im_ov=im_create(IM_BYTE,areaDxBW ,areaDyBW );

	im_src2=(BYTE*)GlobalAlloc(GMEM_FIXED,areaDxBW * areaDyBW  );
	Image=(BYTE*)GlobalAlloc(GMEM_FIXED,areaDxBW * areaDyBW  );
}

void CZAxisView::OnSnap()
{

	cam2=(CICamera*)theapp->m_pMod1->GetCam(2);	
	cam2->Freeze(GrabID2);
	//cam0->Snap(imgBuf0,0,0,640,480);
	//GrabID2=cam2->Grab(0,imgBuf2,1);
	//imgBuf1=CropIt(imgBuf0,0,100,640,280);
	//im_sr->pix=imI;
	ztot+=1;
	//Inspect(imgBuf2);100,40,370,170
//	TubTop=TopEdge(imgBuf2, 100, 50, 550, 400);
//	imgConn2->GetSrc()->SetBufferAddr(imgBuf2);//
	imgConn2->Display();
	//QueryPerformanceCounter(&timeEnd);
	//spanElapsed = float(timeEnd.LowPart -  timeStart.LowPart)/float(Frequency.LowPart);
	//pframe->m_pview->m_time.Format(" %.5f",spanElapsed);
	InvalidateRect(NULL,false);
}

HVLINE CZAxisView::TopEdge(BYTE *im_srcHV, int x, int y, int dx, int dy)
{
	int pix1=0;	
	int result=0;
	int LineLength=640;
	int pointa=0;
	int pointb=0;
	bool FirstTime=true;
	int points=0;
	int ydif=0;
	
	int tubecolor=140;

	HVLINE Result;
	Result.x=Result.y=Result.dx=Result.dy=0;
//*****************find y distance*******************************
	

	for(int j=50; j<550;)//
	{
		for(int p=10;p<220;p++)
		{	
			pix1=*(im_srcHV + j +(LineLength * p));
			if(pix1 > tubecolor )
			{ 
				pointa=p;
				if(FirstTime==false)
				{
					ydif=abs(pointb-pointa); 
					if(ydif>=BumpLimit)
					{
						Result.x=j; 
						Result.y=p;  
						j=550; 
					}
				}
				FirstTime=false;
				p=220;
			}
			
		}
		pointb=pointa;
		j+=32;
	}
//	int posx=0;
	//int best=0;
//	int t1, t2, t3;

//	t2=ydif[0];
		
//	for(int c=1; c<=16;c++)
	//{
	//	t1=ydif[c];
	//	t3=max(t1,t2);
	//	if(t3==t1){best=t1; posx=c;}
	//	t2=t3;
//	}
	//if(best>=BumpLimit)
	//{
	//	Result.x=(posx*32); 
		//Result.y=50;
	//}

	return Result;
}

BEGIN_MESSAGE_MAP(CZAxisView, CView)
	//{{AFX_MSG_MAP(CZAxisView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZAxisView drawing

void CZAxisView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	CClientDC dc( this );
	int i=0;
	CBrush Red, Blue, RedLine;
	COLORREF greencolor = RGB(0,255,0);
	COLORREF redcolor = RGB(255,0,0);
	COLORREF bluecolor = RGB(0,0,255);
	
	Red.CreateSolidBrush(RGB(255,0,0)); 
	Blue.CreateSolidBrush(RGB(0,0,255));
	CPen BluePen(PS_SOLID, 3, RGB(0,0,250));
	CPen LRedPen(PS_SOLID, 1, RGB(230,30,80));
	CPen FRedPen(PS_SOLID, 3, RGB(255,0,0));
	CPen RedDash(PS_DASH, 1, RGB(255,0,0));
	CPen BlackPen(PS_SOLID, 2, RGB(0,0,0));
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
	CPen YellowPen(PS_DASH, 1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID, 2, RGB(255,255,255));
	CPen AquaPen(PS_SOLID, 4, RGB(0,255,255));
	CRect rect;
	GetClientRect(rect);
	CString m1;

	pDC->SetBkMode(TRANSPARENT);

	pDC->Draw3dRect(50,40,500,150,bluecolor,bluecolor);

	pDC->SelectObject(&WhitePen);
	pDC->SetTextColor(RGB(255,255,255));
	///////////********BigBag**********///////////
	CurrentDefect=false;
	if(TubTop.x>0)
	{
		CurrentDefect=true;
		BumpDefect+=1;

		m1="Bump";
		pDC->TextOut(TubTop.x,TubTop.y,m1);
		pDC->SelectObject(&LRedPen);
		pDC->MoveTo(TubTop.x,TubTop.y);
		pDC->LineTo(TubTop.x+32,TubTop.y);
	}

}

/////////////////////////////////////////////////////////////////////////////
// CZAxisView diagnostics

#ifdef _DEBUG
void CZAxisView::AssertValid() const
{
	CView::AssertValid();
}

void CZAxisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CZAxisView message handlers

