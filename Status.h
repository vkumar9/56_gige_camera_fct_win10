#if !defined(AFX_STATUS_H__383B54EA_8C42_4AEB_ADE4_D121E234361E__INCLUDED_)
#define AFX_STATUS_H__383B54EA_8C42_4AEB_ADE4_D121E234361E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Status.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Status dialog

class Status : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp; 
public:
	Status(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Status)
	enum { IDD = IDD_CAMSTART };
	CString	m_inittry;				//GUI variable for displaying how many attempts were made to connect to this camera.
	CString	m_cam;					//GUI variable for displaying which camera is being connected to.
	//}}AFX_DATA

	CMainFrame* pframe;
	CCapApp* theapp;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Status)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Status)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATUS_H__383B54EA_8C42_4AEB_ADE4_D121E234361E__INCLUDED_)
