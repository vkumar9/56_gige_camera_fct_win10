// Prompt2.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Prompt2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Prompt2 dialog


Prompt2::Prompt2(CWnd* pParent /*=NULL*/)
	: CDialog(Prompt2::IDD, pParent)
{
	//{{AFX_DATA_INIT(Prompt2)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Prompt2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Prompt2)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Prompt2, CDialog)
	//{{AFX_MSG_MAP(Prompt2)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Prompt2 message handlers
