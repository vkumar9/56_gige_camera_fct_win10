#if !defined(AFX_ADVANCED_H__9A6AC3B5_5207_4E1B_8B6D_C74AEB6C3A21__INCLUDED_)
#define AFX_ADVANCED_H__9A6AC3B5_5207_4E1B_8B6D_C74AEB6C3A21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Advanced.h : header file
//
//#include "GoIO_DLL_interface.h"

/////////////////////////////////////////////////////////////////////////////
// Advanced dialog

class Advanced : public CDialog
{
// Construction
public:
	int motorHeight;		//Variable which stores the camera enclosure motor height.
	Advanced(CWnd* pParent = NULL);   // standard constructor

	CMainFrame* pframe;
	CCapApp* theapp;

	int localTemp;			//Variable which stores the temperature obtained from the sensor.

//	void Initialise_temp_probe();
//	bool GetAvailableDeviceName(char *deviceName, gtype_int32 nameLength, gtype_int32 *pVendorId, gtype_int32 *pProductId);
//	void OSSleep(unsigned long msToSleep);
// Dialog Data
	//{{AFX_DATA(Advanced)
	enum { IDD = IDD_ADVANCED };
	CString	m_editheight;	//Variable which stores the camera enclosure motor height.
	CString	m_edittemp;		//Control cabinet temperature limit.
	CString	m_editcurrent;	//Current temperature for control cabinet.
	CString	m_editconv;		//Conveyor speed offset.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Advanced)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CString m_enable;
	// Generated message map functions
	//{{AFX_MSG(Advanced)
	afx_msg void OnEncsim();
	virtual BOOL OnInitDialog();
	afx_msg void OnPlcpos();
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnMore2();
	afx_msg void OnLess2();
	afx_msg void OnThigher();
	afx_msg void OnTlower();
	afx_msg void OnOhigher();
	afx_msg void OnOlower();
	afx_msg void OnButton2();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnOk();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADVANCED_H__9A6AC3B5_5207_4E1B_8B6D_C74AEB6C3A21__INCLUDED_)
