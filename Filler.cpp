// Filler.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Filler.h"
#include "mainfrm.h"
#include "serial.h"
#include "Prompt3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Filler dialog


Filler::Filler(CWnd* pParent /*=NULL*/)
	: CDialog(Filler::IDD, pParent)
{
	//{{AFX_DATA_INIT(Filler)
	m_position = _T("");
	m_duration = _T("");
	m_cappos = _T("");
	m_capnum = _T("");
	m_filpos = _T("");
	m_filnum = _T("");
	m_s10 = _T("");
	m_s2 = _T("");
	m_s3 = _T("");
	m_s4 = _T("");
	m_s5 = _T("");
	m_s6 = _T("");
	m_s7 = _T("");
	m_s8 = _T("");
	m_s9 = _T("");
	m_c1 = _T("");
	m_c10 = _T("");
	m_c2 = _T("");
	m_c3 = _T("");
	m_c4 = _T("");
	m_c5 = _T("");
	m_c6 = _T("");
	m_c7 = _T("");
	m_c8 = _T("");
	m_c9 = _T("");
	m_s1 = _T("");
	m_c11 = _T("");
	m_c12 = _T("");
	m_c13 = _T("");
	m_c14 = _T("");
	m_c15 = _T("");
	m_c16 = _T("");
	m_c17 = _T("");
	m_c18 = _T("");
	m_c19 = _T("");
	m_c20 = _T("");
	m_c21 = _T("");
	m_c22 = _T("");
	m_c23 = _T("");
	m_c24 = _T("");
	m_c25 = _T("");
	m_c26 = _T("");
	m_c27 = _T("");
	m_c28 = _T("");
	m_c29 = _T("");
	m_c30 = _T("");
	m_c31 = _T("");
	m_c32 = _T("");
	m_c33 = _T("");
	m_s11 = _T("");
	m_s12 = _T("");
	m_s13 = _T("");
	m_s14 = _T("");
	m_s15 = _T("");
	m_s16 = _T("");
	m_s17 = _T("");
	m_s18 = _T("");
	m_s19 = _T("");
	m_s20 = _T("");
	m_s21 = _T("");
	m_s22 = _T("");
	m_s24 = _T("");
	m_s23 = _T("");
	m_s25 = _T("");
	m_s26 = _T("");
	m_s27 = _T("");
	m_s28 = _T("");
	m_s29 = _T("");
	m_s30 = _T("");
	m_s31 = _T("");
	m_s32 = _T("");
	m_s33 = _T("");
	//}}AFX_DATA_INIT
}


void Filler::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Filler)
	DDX_Control(pDX, IDC_FILDN, m_filldnc);
	DDX_Control(pDX, IDC_FILDN2, m_filldn2c);
	DDX_Control(pDX, IDC_FILUP2, m_fillup2c);
	DDX_Control(pDX, IDC_FILUP, m_fillupc);
	DDX_Text(pDX, IDC_EDIT1, m_cappos);
	DDX_Text(pDX, IDC_EDIT2, m_capnum);
	DDX_Text(pDX, IDC_EDIT3, m_filpos);
	DDX_Text(pDX, IDC_EDIT4, m_filnum);
	DDX_Text(pDX, IDC_S10, m_s10);
	DDX_Text(pDX, IDC_S2, m_s2);
	DDX_Text(pDX, IDC_S3, m_s3);
	DDX_Text(pDX, IDC_S4, m_s4);
	DDX_Text(pDX, IDC_S5, m_s5);
	DDX_Text(pDX, IDC_S6, m_s6);
	DDX_Text(pDX, IDC_S7, m_s7);
	DDX_Text(pDX, IDC_S8, m_s8);
	DDX_Text(pDX, IDC_S9, m_s9);
	DDX_Text(pDX, IDC_C1, m_c1);
	DDX_Text(pDX, IDC_C10, m_c10);
	DDX_Text(pDX, IDC_C2, m_c2);
	DDX_Text(pDX, IDC_C3, m_c3);
	DDX_Text(pDX, IDC_C4, m_c4);
	DDX_Text(pDX, IDC_C5, m_c5);
	DDX_Text(pDX, IDC_C6, m_c6);
	DDX_Text(pDX, IDC_C7, m_c7);
	DDX_Text(pDX, IDC_C8, m_c8);
	DDX_Text(pDX, IDC_C9, m_c9);
	DDX_Text(pDX, IDC_S1, m_s1);
	DDX_Text(pDX, IDC_C11, m_c11);
	DDX_Text(pDX, IDC_C12, m_c12);
	DDX_Text(pDX, IDC_C13, m_c13);
	DDX_Text(pDX, IDC_C14, m_c14);
	DDX_Text(pDX, IDC_C15, m_c15);
	DDX_Text(pDX, IDC_C16, m_c16);
	DDX_Text(pDX, IDC_C17, m_c17);
	DDX_Text(pDX, IDC_C18, m_c18);
	DDX_Text(pDX, IDC_C19, m_c19);
	DDX_Text(pDX, IDC_C20, m_c20);
	DDX_Text(pDX, IDC_C21, m_c21);
	DDX_Text(pDX, IDC_C22, m_c22);
	DDX_Text(pDX, IDC_C23, m_c23);
	DDX_Text(pDX, IDC_C24, m_c24);
	DDX_Text(pDX, IDC_C25, m_c25);
	DDX_Text(pDX, IDC_C26, m_c26);
	DDX_Text(pDX, IDC_C27, m_c27);
	DDX_Text(pDX, IDC_C28, m_c28);
	DDX_Text(pDX, IDC_C29, m_c29);
	DDX_Text(pDX, IDC_C30, m_c30);
	DDX_Text(pDX, IDC_C31, m_c31);
	DDX_Text(pDX, IDC_C32, m_c32);
	DDX_Text(pDX, IDC_C33, m_c33);
	DDX_Text(pDX, IDC_S11, m_s11);
	DDX_Text(pDX, IDC_S12, m_s12);
	DDX_Text(pDX, IDC_S13, m_s13);
	DDX_Text(pDX, IDC_S14, m_s14);
	DDX_Text(pDX, IDC_S15, m_s15);
	DDX_Text(pDX, IDC_S16, m_s16);
	DDX_Text(pDX, IDC_S17, m_s17);
	DDX_Text(pDX, IDC_S18, m_s18);
	DDX_Text(pDX, IDC_S19, m_s19);
	DDX_Text(pDX, IDC_S20, m_s20);
	DDX_Text(pDX, IDC_S21, m_s21);
	DDX_Text(pDX, IDC_S22, m_s22);
	DDX_Text(pDX, IDC_S24, m_s24);
	DDX_Text(pDX, IDC_S23, m_s23);
	DDX_Text(pDX, IDC_S25, m_s25);
	DDX_Text(pDX, IDC_S26, m_s26);
	DDX_Text(pDX, IDC_S27, m_s27);
	DDX_Text(pDX, IDC_S28, m_s28);
	DDX_Text(pDX, IDC_S29, m_s29);
	DDX_Text(pDX, IDC_S30, m_s30);
	DDX_Text(pDX, IDC_S31, m_s31);
	DDX_Text(pDX, IDC_S32, m_s32);
	DDX_Text(pDX, IDC_S33, m_s33);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Filler, CDialog)
	//{{AFX_MSG_MAP(Filler)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_COMMAND(IDC_SETUP, OnSetup)
	ON_BN_CLICKED(IDC_CAPUP, OnCapup)
	ON_BN_CLICKED(IDC_CAPDN, OnCapdn)
	ON_BN_CLICKED(IDC_CAPUP2, OnCapup2)
	ON_BN_CLICKED(IDC_CAPDN2, OnCapdn2)
	ON_BN_CLICKED(IDC_FILUP, OnFilup)
	ON_BN_CLICKED(IDC_FILDN, OnFildn)
	ON_BN_CLICKED(IDC_FILUP2, OnFilup2)
	ON_BN_CLICKED(IDC_FILDN2, OnFildn2)
	ON_BN_CLICKED(IDC_ALLHEADS, OnAllheads)
	ON_BN_CLICKED(ID_START, OnStart)
	ON_BN_CLICKED(ID_CLEAR, OnClear)
	ON_BN_CLICKED(IDC_CAPUPFAST, OnCapupfast)
	ON_BN_CLICKED(IDC_CAPDNFAST, OnCapdnfast)
	ON_BN_CLICKED(IDC_FILUPFAST, OnFilupfast)
	ON_BN_CLICKED(IDC_FILDNFAST, OnFildnfast)
	ON_BN_CLICKED(IDC_ALLHEADS2, OnAllheads2)
	ON_BN_CLICKED(IDC_1THRU6, On1thru6)
	ON_BN_CLICKED(IDC_7THRU12, On7thru12)
	ON_BN_CLICKED(IDC_13THRU18, On13thru18)
	ON_BN_CLICKED(IDC_19THRU24, On19thru24)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Filler message handlers

BOOL Filler::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pfiller=this;
	theapp=(CCapApp*)AfxGetApp();
	theapp->fillerOpen=true;		//Specify that filler/capper dialog box is open.

	pParent2=GetParent(); 			//Get reference to parent window.
	
	//Create a filler/capper setup dialog box.
	m_pfsetup=new FSetup(); 
	m_pfsetup->Create(IDD_FILLERSET,pParent2);

	//Clear flags indicating that bottles from all fillers/cappers are to be ejected.
	allCap=false;
	allFill=false;

	oneSix=false;					//NOT USED
	sevenTwelve=false;				//NOT USED
	thirteenEighteen=false;			//NOT USED
	nineteenTwentyfour=false;		//NOT USED
		
	//Reset the labels of which filler/capper heads was used for which bottles.
	for(int d=0; d<=33; d++)
	{
		bottles[d].y=0;
		bottles[d].x=0;
	}	

	SetTimer(1,200,NULL);		//Some kind of communication to the PLC
	SetTimer(2,50,NULL); 		//Set timer to update pictorial representation of which filler/capper head was used on which bottle.
	lockOut=false;				//NOT USED
	
	oldPos=0;					//NOT USED
	z=0;						//NOT USED
	shiftOne=true;				//NOT USED
//SetTimer(3,500,NULL);
	
	//Send info to PLC indicating that the current filler/capper heads position is 0.
	if(theapp->NoSerialComm==false) 
	{
		pframe->m_pserial->fillerPos=0;
	}

	//Reset the index of the heads and the number of bottles to be ejected.
	theapp->fillerNum=0;
	theapp->fillerValve=0;
	theapp->capperNum=0;
	theapp->capperHead=0;

	//Update GUI variables.
	m_filnum.Format("%i",theapp->fillerNum);
	m_filpos.Format("%i",theapp->fillerValve);
	m_capnum.Format("%i",theapp->capperNum);
	m_cappos.Format("%i",theapp->capperHead);

	

	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Filler::OnOK() 
{
	//Delete pointer to filler/capper setup dialog box.
	if(m_pfsetup) delete m_pfsetup;
	theapp->fillerOpen=false;	//Clear flag indicating that the filler/capper dialog box is open.
	
	//pframe->SetTimer(30,1,NULL);	
	//pframe->SetTimer(31,250,NULL);
	
	KillTimer(2);		//Stop filler/capper display from updating.

	CDialog::OnOK();

}

void Filler::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	CPen YellowPen(PS_DASH, 1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID, 2, RGB(255,255,255));
	CPen AquaPen(PS_SOLID, 4, RGB(0,255,255));
	COLORREF redcolor = RGB(255,0,0);
	COLORREF bluecolor = RGB(50,50,220);
	
	CBrush brushBlue(RGB(0, 0, 255));
	CBrush brushWhite(RGB(255, 255, 255));
	CBrush brushRed(RGB(255, 0, 0));
	CBrush brushGreen(RGB(0, 255, 0));
	dc.SetBkMode(OPAQUE);								//Set background 
	dc.SelectObject(&WhitePen);							//Set pen

   CBrush* pOldBrush = dc.SelectObject(&brushWhite);	//Set brush

   // create and select a thick, black pen
   CPen penBlack;
   penBlack.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
   CPen* pOldPen = dc.SelectObject(&penBlack);

   // get our client rectangle
   CRect rect;
   CPoint p1,p2;
   p2=5,5;
   p1=110,110;

   int x=0;
   int y=0;
   int x2=0;
	int y2=0;
	int i;
   //GetClientRect(rect);
			if(bottles[1].x==121)	//value of 121 is used as a flag for bottle to eject.
			{
				m_s1.Format("%s","ej");		//label "ej" on a bottle to be ejected based on filler head.
			}
			else  //label the bottle with the filler head which was used to fill it.
			{
				m_s1.Format("%2i",bottles[1].x);
			}
			if(bottles[1].y==121)
			{
				m_c1.Format("%s","ej");		//label "ej" on a bottle to be ejected based on capper head.
			}
			else
			{
				m_c1.Format("%2i",bottles[1].y);	//label the bottle with the capper head which was used to cap it.
			}

			if(bottles[2].x==121){m_s2.Format("%s","ej");}else{m_s2.Format("%2i",bottles[2].x);}
			if(bottles[2].y==121){m_c2.Format("%s","ej");}else{m_c2.Format("%2i",bottles[2].y);}

			if(bottles[3].x==121){m_s3.Format("%s","ej");}else{m_s3.Format("%2i",bottles[3].x);}
			if(bottles[3].y==121){m_c3.Format("%s","ej");}else{m_c3.Format("%2i",bottles[3].y);}

			if(bottles[4].x==121){m_s4.Format("%s","ej");}else{m_s4.Format("%2i",bottles[4].x);}
			if(bottles[4].y==121){m_c4.Format("%s","ej");}else{m_c4.Format("%2i",bottles[4].y);}
	
			if(bottles[5].x==121){m_s5.Format("%s","ej");}else{m_s5.Format("%2i",bottles[5].x);}
			if(bottles[5].y==121){m_c5.Format("%s","ej");}else{m_c5.Format("%2i",bottles[5].y);}

			if(bottles[6].x==121){m_s6.Format("%s","ej");}else{m_s6.Format("%2i",bottles[6].x);}
			if(bottles[6].y==121){m_c6.Format("%s","ej");}else{m_c6.Format("%2i",bottles[6].y);}

			if(bottles[7].x==121){m_s7.Format("%s","ej");}else{m_s7.Format("%2i",bottles[7].x);}
			if(bottles[7].y==121){m_c7.Format("%s","ej");}else{m_c7.Format("%2i",bottles[7].y);}

			if(bottles[8].x==121){m_s8.Format("%s","ej");}else{m_s8.Format("%2i",bottles[8].x);}
			if(bottles[8].y==121){m_c8.Format("%s","ej");}else{m_c8.Format("%2i",bottles[8].y);}

			if(bottles[9].x==121){m_s9.Format("%s","ej");}else{m_s9.Format("%2i",bottles[9].x);}
			if(bottles[9].y==121){m_c9.Format("%s","ej");}else{m_c9.Format("%2i",bottles[9].y);}

			if(bottles[10].x==121){m_s10.Format("%s","ej");}else{m_s10.Format("%2i",bottles[10].x);}
			if(bottles[10].y==121){m_c10.Format("%s","ej");}else{m_c10.Format("%2i",bottles[10].y);}

			if(bottles[11].x==121){m_s11.Format("%s","ej");}else{m_s11.Format("%2i",bottles[11].x);}
			if(bottles[11].y==121){m_c11.Format("%s","ej");}else{m_c11.Format("%2i",bottles[11].y);}

			if(bottles[12].x==121){m_s12.Format("%s","ej");}else{m_s12.Format("%2i",bottles[12].x);}
			if(bottles[12].y==121){m_c12.Format("%s","ej");}else{m_c12.Format("%2i",bottles[12].y);}

			if(bottles[13].x==121){m_s13.Format("%s","ej");}else{m_s13.Format("%2i",bottles[13].x);}
			if(bottles[13].y==121){m_c13.Format("%s","ej");}else{m_c13.Format("%2i",bottles[13].y);}

			if(bottles[14].x==121){m_s14.Format("%s","ej");}else{m_s14.Format("%2i",bottles[14].x);}
			if(bottles[14].y==121){m_c14.Format("%s","ej");}else{m_c14.Format("%2i",bottles[14].y);}
				
			if(bottles[15].x==121){m_s15.Format("%s","ej");}else{m_s15.Format("%2i",bottles[15].x);}
			if(bottles[15].y==121){m_c15.Format("%s","ej");}else{m_c15.Format("%2i",bottles[15].y);}

			if(bottles[16].x==121){m_s16.Format("%s","ej");}else{m_s16.Format("%2i",bottles[16].x);}
			if(bottles[16].y==121){m_c16.Format("%s","ej");}else{m_c16.Format("%2i",bottles[16].y);}

			if(bottles[17].x==121){m_s17.Format("%s","ej");}else{m_s17.Format("%2i",bottles[17].x);}
			if(bottles[17].y==121){m_c17.Format("%s","ej");}else{m_c17.Format("%2i",bottles[17].y);}

			if(bottles[18].x==121){m_s18.Format("%s","ej");}else{m_s18.Format("%2i",bottles[18].x);}
			if(bottles[18].y==121){m_c18.Format("%s","ej");}else{m_c18.Format("%2i",bottles[18].y);}

			if(bottles[19].x==121){m_s19.Format("%s","ej");}else{m_s19.Format("%2i",bottles[19].x);}
			if(bottles[19].y==121){m_c19.Format("%s","ej");}else{m_c19.Format("%2i",bottles[19].y);}

			if(bottles[20].x==121){m_s20.Format("%s","ej");}else{m_s20.Format("%2i",bottles[20].x);}
			if(bottles[20].y==121){m_c20.Format("%s","ej");}else{m_c20.Format("%2i",bottles[20].y);}

			if(bottles[21].x==121){m_s21.Format("%s","ej");}else{m_s21.Format("%2i",bottles[21].x);}
			if(bottles[21].y==121){m_c21.Format("%s","ej");}else{m_c21.Format("%2i",bottles[21].y);}

			if(bottles[22].x==121){m_s22.Format("%s","ej");}else{m_s22.Format("%2i",bottles[22].x);}
			if(bottles[22].y==121){m_c22.Format("%s","ej");}else{m_c22.Format("%2i",bottles[22].y);}

			if(bottles[23].x==121){m_s23.Format("%s","ej");}else{m_s23.Format("%2i",bottles[23].x);}
			if(bottles[23].y==121){m_c23.Format("%s","ej");}else{m_c23.Format("%2i",bottles[23].y);}

			if(bottles[24].x==121){m_s24.Format("%s","ej");}else{m_s24.Format("%2i",bottles[24].x);}
			if(bottles[24].y==121){m_c24.Format("%s","ej");}else{m_c24.Format("%2i",bottles[24].y);}

		
				
			if(bottles[25].x==121){m_s25.Format("%s","ej");}else{m_s25.Format("%2i",bottles[25].x);}
			if(bottles[25].y==121){m_c25.Format("%s","ej");}else{m_c25.Format("%2i",bottles[25].y);}

			if(bottles[26].x==121){m_s26.Format("%s","ej");}else{m_s26.Format("%2i",bottles[26].x);}
			if(bottles[26].y==121){m_c26.Format("%s","ej");}else{m_c26.Format("%2i",bottles[26].y);}

			if(bottles[27].x==121){m_s27.Format("%s","ej");}else{m_s27.Format("%2i",bottles[27].x);}
			if(bottles[27].y==121){m_c27.Format("%s","ej");}else{m_c27.Format("%2i",bottles[27].y);}

			if(bottles[28].x==121){m_s28.Format("%s","ej");}else{m_s28.Format("%2i",bottles[28].x);}
			if(bottles[28].y==121){m_c28.Format("%s","ej");}else{m_c28.Format("%2i",bottles[28].y);}

			if(bottles[29].x==121){m_s29.Format("%s","ej");}else{m_s29.Format("%2i",bottles[29].x);}
			if(bottles[29].y==121){m_c29.Format("%s","ej");}else{m_c29.Format("%2i",bottles[29].y);}

			if(bottles[30].x==121){m_s30.Format("%s","ej");}else{m_s30.Format("%2i",bottles[30].x);}
			if(bottles[30].y==121){m_c30.Format("%s","ej");}else{m_c30.Format("%2i",bottles[30].y);}

			if(bottles[31].x==121){m_s31.Format("%s","ej");}else{m_s31.Format("%2i",bottles[31].x);}
			if(bottles[31].y==121){m_c31.Format("%s","ej");}else{m_c31.Format("%2i",bottles[31].y);}

			if(bottles[32].x==121){m_s32.Format("%s","ej");}else{m_s32.Format("%2i",bottles[32].x);}
			if(bottles[32].y==121){m_c32.Format("%s","ej");}else{m_c32.Format("%2i",bottles[32].y);}

			if(bottles[33].x==121){m_s33.Format("%s","ej");}else{m_s33.Format("%2i",bottles[33].x);}
			if(bottles[33].y==121){m_c33.Format("%s","ej");}else{m_c33.Format("%2i",bottles[33].y);}

   dc.SelectObject(pOldBrush);
   dc.SelectObject(pOldPen);
}

//NOT USED
void Filler::OnTimer(UINT nIDEvent) 
{

	if(nIDEvent==1)
	{
		KillTimer(1);
		if(theapp->NoSerialComm==false) 
		{
		pframe->m_pserial->ControlChars=0;
		}
	
	}

	if(nIDEvent==2)
	{
	
		UpdateDisplay();
	
	}

	//NOT USED
	if(nIDEvent==4)
	{
	
		KillTimer(4);
		lockOut=false;
	
	}

	CDialog::OnTimer(nIDEvent);
}



//Updates the pictorial representation of which filler/capper head was used on which bottle on the conveyor.
void Filler::UpdateDisplay()
{
	if(theapp->NoSerialComm==false) 
	{

		int totalCovPulses=(theapp->SavedDelay/7)*theapp->convInches;		//Compute the number of conveyor pulses between
																			//the exit of the filler/capper and the camera tunnel.
		int onePocketofPulses=theapp->SavedDelay/3.5;						//Compute the number of pulses between two consecutive bottles 
																			//leaving the filler/capper machine.
		theapp->numConvPockets=totalCovPulses/onePocketofPulses;			//Compute the number of bottles between the exit of the		
																			//filler and capper and the vision system.
		if(theapp->numConvPockets>=33) theapp->numConvPockets=33;//Cannot display more then 33 bottles.
	//	m_pocketquant.Format("%2i",numConvPockets);
		if(theapp->numConvPockets<=0) theapp->numConvPockets=0;

		//In the following loop, the bottles[i].x values represent the number of the filler head used on a bottle and
		//bottles[i].y represent the number of the capper head used on a bottle.
		for(int i=0; i<=theapp->numConvPockets; i++)	//For number of bottles between the filler/capper and camera tunnel.
		{
			//Filler head computations positioning
			if(pframe->m_pserial->currentFillerPosition+i <= theapp->fillerSize && pframe->m_pserial->currentFillerPosition+i >=1)
			{
						//For this filler head, if eject counter is 1 or greater 
						if(theapp->fillEject[pframe->m_pserial->currentFillerPosition+i] >=1 )
						{
							bottles[i+1].x=121;
						}
						else  //Otherwise assign the appropriate filler head used on this bottle.
						{
							bottles[i+1].x=pframe->m_pserial->currentFillerPosition+i;
						}
						
					}
					else if(pframe->m_pserial->currentFillerPosition+i > theapp->fillerSize)
					{
						if(theapp->fillEject[(pframe->m_pserial->currentFillerPosition+i)-theapp->fillerSize] >=1 )
						{
							bottles[i+1].x=121;
						}
						else
						{
							bottles[i+1].x=(pframe->m_pserial->currentFillerPosition+i)-theapp->fillerSize;
						}
						
					}
			

					//Capper head computations positioning
					if(pframe->m_pserial->currentCapperPosition+i <= theapp->capperSize && pframe->m_pserial->currentCapperPosition+i >=1)
					{
						if(theapp->capEject[pframe->m_pserial->currentCapperPosition+i] >=1 )
						{
							bottles[i+1].y=121;
						}
						else
						{
							bottles[i+1].y=pframe->m_pserial->currentCapperPosition+i;
						}
						
					}
					else if(pframe->m_pserial->currentCapperPosition+i > theapp->capperSize && pframe->m_pserial->currentCapperPosition+i <= theapp->capperSize*2)
					{
						if(theapp->capEject[(pframe->m_pserial->currentCapperPosition+i)-theapp->capperSize] >=1 )
						{
							bottles[i+1].y=121;
						}
						else
						{
							bottles[i+1].y=(pframe->m_pserial->currentCapperPosition+i)-theapp->capperSize;
						}
						
					}
					else if(pframe->m_pserial->currentCapperPosition+i >= theapp->capperSize * 2)
					{
						if(theapp->capEject[(pframe->m_pserial->currentCapperPosition+i)-theapp->capperSize *2] >=1 )
						{
							bottles[i+1].y=121;
						}
						else
						{
							
							bottles[i+1].y=(pframe->m_pserial->currentCapperPosition+i)-theapp->capperSize*2;
						}
					}
			
		InvalidateRect(NULL,false);
		UpdateData(false);

		}
		
		
		oldPos=pframe->m_pserial->currentFillerPosition;
	}	
}


//NOT USED
void Filler::OnSetup() 
{
	m_pfsetup->ShowWindow(SW_SHOW);	
}
/////////////////////////////////////////////////////////////////////////////
// FSetup dialog


FSetup::FSetup(CWnd* pParent /*=NULL*/)
	: CDialog(FSetup::IDD, pParent)
{
	//{{AFX_DATA_INIT(FSetup)
	m_pocketnum = _T("");
	m_cappersize = _T("");
	m_fillersize = _T("");
	m_editcapoffset = _T("");
	m_editfilloffset = _T("");
	m_pocketquant = _T("");
	//}}AFX_DATA_INIT
}


void FSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FSetup)
	DDX_Text(pDX, IDC_POCKETNUM, m_pocketnum);
	DDX_Text(pDX, IDC_CSIZE, m_cappersize);
	DDX_Text(pDX, IDC_FSIZE, m_fillersize);
	DDX_Text(pDX, IDC_EDITCAPOFFSET, m_editcapoffset);
	DDX_Text(pDX, IDC_EDITFILLOFFSET, m_editfilloffset);
	DDX_Text(pDX, IDC_POCKETQUANT, m_pocketquant);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FSetup, CDialog)
	//{{AFX_MSG_MAP(FSetup)
	
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BMORE, OnBmore)
	ON_BN_CLICKED(IDC_BLESS, OnBless)
	ON_BN_CLICKED(IDC_FILLNUMMORE, OnFillnummore)
	ON_BN_CLICKED(IDC_FILLNUMLESS, OnFillnumless)
	ON_BN_CLICKED(IDC_CAPPERMORE, OnCappermore)
	ON_BN_CLICKED(IDC_CAPPERLESS, OnCapperless)
	ON_BN_CLICKED(IDC_FOFFSETMORE, OnFoffsetmore)
	ON_BN_CLICKED(IDC_FOFFSETLESS, OnFoffsetless)
	ON_BN_CLICKED(IDC_CAPOFFSETMORE, OnCapoffsetmore)
	ON_BN_CLICKED(IDC_CAPOFFSETLESS, OnCapoffsetless)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FSetup message handlers

BOOL FSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pfsetup2=this;
	theapp=(CCapApp*)AfxGetApp();

	//Update gui from saved data.
	m_pocketnum.Format("%i",theapp->convInches);
	m_cappersize.Format("%i",theapp->capperSize);
	m_fillersize.Format("%i",theapp->fillerSize);
	m_editfilloffset.Format("%i",theapp->fillPocketOffset);
	m_editcapoffset.Format("%i",theapp->capPocketOffset);



	UpdateData(false);
	
	 //filler enable

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FSetup::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);
		if(theapp->NoSerialComm==false) 
		{
		pframe->m_pserial->ControlChars=0;
		}
	
	}
	
	if(nIDEvent==2)
	{
		pframe->m_pfiller->UpdateDisplay();
	}

	if(nIDEvent==3)
	{
		if(theapp->NoSerialComm==false) 
		{
		pframe->m_pserial->fillerPos+=1;
		if(pframe->m_pserial->fillerPos>=150) pframe->m_pserial->fillerPos=0;
		}
	
	}
	
	CDialog::OnTimer(nIDEvent);
}


//Increment the number of the capper head for which bottles are to be ejected.
void Filler::OnCapup() 
{
	theapp->fillDataLoaded=false;		//Filler/capper data is being modified so do not eject any bottles.
	theapp->capperHead+=1;				//increment the index of the capper head from which to eject bottles.
	
	//Index of capper head cannot be greater then the number of heads on the machine.
	if(theapp->capperHead>=theapp->capperSize) theapp->capperHead=theapp->capperSize;
	m_cappos.Format("%i",theapp->capperHead);
	//theapp->capEject[]+=1;
	UpdateData(false);
}

//Decrement the number of the capper head for which bottles are to be ejected.
void Filler::OnCapdn() 
{
	theapp->fillDataLoaded=false;
	theapp->capperHead-=1;
	//Index of capper head cannot be less then 0.
	if(theapp->capperHead<=0) theapp->capperHead=0;
	m_cappos.Format("%i",theapp->capperHead);

	UpdateData(false);
}

//Increment the number of bottles which are to be ejected from this head.
void Filler::OnCapup2() 
{
	
	theapp->fillDataLoaded=false;
	if(theapp->capperHead>0)
	{
		theapp->capperNum+=1;
		theapp->capEject[theapp->capperHead]=theapp->capperNum;
		m_capnum.Format("%i",theapp->capperNum);

	
	}
	else  //If no capper head was selected prompt the user to selected a capper head.
	{
	//	AfxMessageBox("Pick a Capper Head first!!!!!!!!");
			pframe->prompt_code=18;
			Prompt3 promtp3;
			promtp3.DoModal();
	}

	UpdateData(false);
}

//Decrement the number of bottles which are to be ejected from this head.
void Filler::OnCapdn2() 
{
	theapp->fillDataLoaded=false;
	if(theapp->capperHead>0)
	{
		theapp->capperNum-=1;
		if(theapp->capperNum<=0) theapp->capperNum=0;
		theapp->capEject[theapp->capperHead]=theapp->capperNum;
		m_capnum.Format("%i",theapp->capperNum);

	
	}
	else
	{
		//AfxMessageBox("Pick a Capper Head first!!!!!!!!");
			pframe->prompt_code=18;
			Prompt3 promtp3;
			promtp3.DoModal();
	}

	UpdateData(false);
	
}

void Filler::OnFilup() 
{
	theapp->fillDataLoaded=false;
	theapp->fillerValve+=1;
	if(theapp->fillerValve>=theapp->fillerSize) theapp->fillerValve=theapp->fillerSize;
	m_filpos.Format("%i",theapp->fillerValve);
	//theapp->capEject[]+=1;
	UpdateData(false);
	
}

void Filler::OnFildn() 
{
	theapp->fillDataLoaded=false;
	theapp->fillerValve-=1;
	if(theapp->fillerValve<=0) theapp->fillerValve=0;
	m_filpos.Format("%i",theapp->fillerValve);
	//theapp->capEject[]+=1;
	UpdateData(false);
}

void Filler::OnFilup2() 
{
	theapp->fillDataLoaded=false;
	if(theapp->fillerValve>0)
	{
		theapp->fillerNum+=1;
		theapp->fillEject[theapp->fillerValve]=theapp->fillerNum;
		m_filnum.Format("%i",theapp->fillerNum);

	
	}
	else
	{
		//AfxMessageBox("Pick a Filler Head first!!!!!!!!");

			pframe->prompt_code=17;
			Prompt3 promtp3;
			promtp3.DoModal();
	}

	UpdateData(false);
	
}

void Filler::OnFildn2() 
{
	theapp->fillDataLoaded=false;
	if(theapp->fillerValve>0)
	{
		theapp->fillerNum-=1;
		if(theapp->fillerNum<=0) theapp->fillerNum=0;
		theapp->fillEject[theapp->fillerValve]=theapp->fillerNum;
		m_filnum.Format("%i",theapp->fillerNum);

	
	}
	else
	{
	//	AfxMessageBox("Pick a Filler Head first!!!!!!!!");

			pframe->prompt_code=17;
			Prompt3 promtp3;
			promtp3.DoModal();
	}

	UpdateData(false);
}

void Filler::OnAllheads() 
{
	allFill=true;
	CheckDlgButton(IDC_ALLHEADS,1);
	UpdateData(false);
	
}

//Start ejecting bottles from the configured filler and capper heads.
void Filler::OnStart() 
{

	theapp->fillDataLoaded=true;		//Filler/capper data is done being modified so start ejecting bottles per the configuration.
	
	//Update Check boxes and toggle buttons on the GUI.
	CheckDlgButton(IDC_ALLHEADS,0);
	CheckDlgButton(IDC_ALLHEADS2,0);
	CheckDlgButton(IDC_1THRU6,0);
	CheckDlgButton(IDC_7THRU12,0);
	CheckDlgButton(IDC_13THRU18,0);
	CheckDlgButton(IDC_19THRU24,0);
	

	//NOT USED//
	if(oneSix==true)
	{
		for(int i=1; i<=6; i++)
		{	
			theapp->capEject[i]=1;	
		}
	}
	oneSix=false;

	if(sevenTwelve==true)
	{
		for(int i=7; i<=12; i++)
		{	
			theapp->capEject[i]=1;	
		}
	}
	sevenTwelve=false;

	if(thirteenEighteen==true)
	{
		for(int i=13; i<=18; i++)
		{	
			theapp->capEject[i]=1;	
		}
	}
	thirteenEighteen=false;

	if(nineteenTwentyfour==true)
	{
		for(int i=19; i<=24; i++)
		{	
			theapp->capEject[i]=1;	
		}
	}
	nineteenTwentyfour=false;
	////


	if(allCap==true)	//If bottles from all capper heads are to be ejected
	{
		//Set bottles from all capper heads to be ejected 1 time.
		for(int i=1; i<=theapp->capperSize; i++)
		{	
			theapp->capEject[i]=1;	
		}
	}
	allCap=false;	//Toggle capper head ejection flag off.

	if(allFill==true)	//If bottles from all filler heads are to be ejected
	{
		//Set bottles from all filler heads to be ejected 1 time.
		for(int i=1; i<=theapp->fillerSize; i++)
		{	
			theapp->fillEject[i]=1;	
		}
	}
	allFill=false;	//Toggle filler head ejection flag off.
	
	//Update GUI to show that the number of bottles to be ejected from the filler and capper is set by the 
	//corresponding check boxes.
	theapp->fillerNum=0;	
	m_filnum.Format("%i",theapp->fillerNum);

	theapp->capperNum=0;
	m_capnum.Format("%i",theapp->capperNum);
	
	theapp->fillerValve=0;
	m_filpos.Format("%i",theapp->fillerValve);

	theapp->capperHead=0;
	m_cappos.Format("%i",theapp->capperHead);

	theapp->waitForOne=false;
	UpdateData(false);
	
}

//Clears all the filler and capper settings.
void Filler::OnClear() 
{
	//Clear the saved number of bottles which should be ejected from each filler head.
	for(int i=1; i<=theapp->fillerSize; i++)
	{	
		theapp->fillEject[i]=0;	
	}

	//Clear the saved number of bottles which should be ejected from each capper head.
	for(int j=1; j<=theapp->capperSize; j++)
	{	
		theapp->capEject[j]=0;	
	}
		//Update Check boxes on the GUI.
	CheckDlgButton(IDC_ALLHEADS,0);
	CheckDlgButton(IDC_ALLHEADS2,0);

	theapp->fillDataLoaded=false;	//Specify that the filler and capper heads ejection settings are not configured.
	
	//Reset the index of the heads and the number of bottles to be ejected.
	theapp->fillerNum=0;
	theapp->fillerValve=0;
	theapp->capperNum=0;
	theapp->capperHead=0;

	

	UpdateData(false);
	
}

//Increase conveyor length. The length here means the measured length, in inches, between the filler/capper and the camera tunnel.
void FSetup::OnBmore() 
{
	theapp->convInches+=1;			//Increase the length of the conveyor from the exit of the capper to the vision system.
	m_pocketnum.Format("%i",theapp->convInches);	//Update the conveyor length variable on the gui.

	int totalCovPulses=(theapp->SavedDelay/9)*theapp->convInches;			//Compute the number of conveyor pulses between
																			//the exit of the filler/capper and the camera tunnel.
	int onePocketofPulses=theapp->SavedDelay/5.25;							//Compute the number of pulses between two consecutive bottles 
																			//leaving the filler/capper machine.
	theapp->numConvPockets=theapp->convInches/5.25;							//Compute the number of bottles between the exit of the		
																			//filler and capper and the vision system.
	//Cannot display more then 50 bottles.
	if(theapp->numConvPockets>=50) theapp->numConvPockets=50;

	m_pocketquant.Format("%2i",theapp->numConvPockets);						//Update gui variable holding the number of bottles on the conveyor.

	//Update PLC with new conveyor length.
	Value=theapp->convInches;			//Set value to send to PLC.

	//Value=Value*1000;
	Function=56;						//Set value to send to PLC.

	Function=Function *10;				//Set value to send to PLC.
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
	
	UpdateData(false);
	
}

//Decrease conveyor length. The length here means the measured length, in inches, between the filler/capper and the camera tunnel.
void FSetup::OnBless() 
{
	theapp->convInches-=1;
	m_pocketnum.Format("%i",theapp->convInches);

	int totalCovPulses=(theapp->SavedDelay/9)*theapp->convInches;
	int onePocketofPulses=theapp->SavedDelay/5.25;
	theapp->numConvPockets=theapp->convInches/5.25;
	if(theapp->numConvPockets>=50) theapp->numConvPockets=50;

	
	m_pocketquant.Format("%2i",theapp->numConvPockets);

	Value=theapp->convInches;

	//Value=Value*1000;
	Function=56;

	Function=Function *10;
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
	//pframe->m_pserial->QueMessage(Value,Function);
	UpdateData(false);
	
}

void FSetup::OnOK() 
{
	//if(theapp->pocketSize>=1 && theapp->convInches>=1)
	//{
		//theapp->boolSync=true;
	//}
	//else
	//{
	//	theapp->boolSync=false;
	//	AfxMessageBox("All settings must have a value larger than 0 for the filler tracking to work.");
	//}
		pframe->SaveJob(pframe->CurrentJobNum);
	CDialog::OnOK();
}

//Increase the recorded number of heads on the filler.
void FSetup::OnFillnummore() 
{
	theapp->fillerSize+=1;
	if(theapp->fillerSize>=120) theapp->fillerSize=120;		//Cannot handle a filler with more then 120 heads.
	m_fillersize.Format("%i",theapp->fillerSize);
	
	//Update PLC with new number of filler heads.
	Value=theapp->fillerSize;

	//Value=Value*1000;
	Function=51;

	Function=Function *10;
	
//	pframe->m_pserial->QueMessage(Value,Function);
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
	UpdateData(false);
	
}

//Decrease the recorded number of heads on the filler.
void FSetup::OnFillnumless() 
{
	theapp->fillerSize-=1;
	if(theapp->fillerSize<=1) theapp->fillerSize=1;
	m_fillersize.Format("%i",theapp->fillerSize);

	Value=theapp->fillerSize;

	//Value=Value*1000;
	Function=51;

	Function=Function *10;
	
	if(theapp->NoSerialComm==false) 
	{
	
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
	
}

//Increase the recorded number of heads on the capper.
void FSetup::OnCappermore() 
{
	theapp->capperSize+=1;
	if(theapp->capperSize>=32) theapp->capperSize=32;
	m_cappersize.Format("%i",theapp->capperSize);

	Value=theapp->capperSize;

	//Value=Value*1000;
	Function=52;

	Function=Function *10;
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
	
}

//Decrease the recorded number of heads on the capper.
void FSetup::OnCapperless() 
{
	theapp->capperSize-=1;
	if(theapp->capperSize<=1) theapp->capperSize=1;
	m_cappersize.Format("%i",theapp->capperSize);

	Value=theapp->capperSize;

	//Value=Value*1000;
	Function=52;

	Function=Function *10;
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
	
}

//Increase filler head offset. Offset between the head number at which the system starts and the actual 0 head.
void FSetup::OnFoffsetmore() 
{
	
	theapp->fillPocketOffset+=1;
	m_editfilloffset.Format("%i",theapp->fillPocketOffset);
	if(theapp->fillPocketOffset >=theapp->fillerSize) theapp->fillPocketOffset=theapp->fillerSize;
	Value=theapp->fillPocketOffset;

	//Value=Value*1000;
	Function=53;

	Function=Function *10;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	
	UpdateData(false);
	InvalidateRect(NULL,true);
	
}

//Decrease filler head offset. Offset between the head number at which the system starts and the actual 0 head.
void FSetup::OnFoffsetless() 
{
	theapp->fillPocketOffset-=1;
	if(theapp->fillPocketOffset<=0)theapp->fillPocketOffset=0;
	m_editfilloffset.Format("%i",theapp->fillPocketOffset);

	Value=theapp->fillPocketOffset;

	//Value=Value*1000;
	Function=53;

	Function=Function *10;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	
	UpdateData(false);
	InvalidateRect(NULL,true);
	
}

//Increase capper head offset. Offset between the head number at which the system starts and the actual 0 head.
void FSetup::OnCapoffsetmore() 
{
	theapp->capPocketOffset+=1;
	if(theapp->capPocketOffset>=theapp->capperSize) theapp->capPocketOffset=theapp->capperSize;
	m_editcapoffset.Format("%i",theapp->capPocketOffset);

	Value=theapp->capPocketOffset;

	//Value=Value*1000;
	Function=54;

	Function=Function *10;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
	
	UpdateData(false);
	InvalidateRect(NULL,true);
	
}

//Decrease capper head offset. Offset between the head number at which the system starts and the actual 0 head.
void FSetup::OnCapoffsetless() 
{
	theapp->capPocketOffset-=1;
	if(theapp->capPocketOffset<=0)theapp->capPocketOffset=0;
	m_editcapoffset.Format("%i",theapp->capPocketOffset);

	Value=theapp->capPocketOffset;

	//Value=Value*1000;
	Function=54;

	Function=Function *10;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
	
	UpdateData(false);
	InvalidateRect(NULL,true);
	
}

//Increment the number of the capper head for which bottles are to be ejected. Large step size.
void Filler::OnCapupfast() 
{
	theapp->fillDataLoaded=false;
	theapp->capperHead+=2;
	if(theapp->capperHead>=theapp->capperSize) theapp->capperHead=theapp->capperSize;
	m_cappos.Format("%i",theapp->capperHead);
	//theapp->capEject[]+=1;
	UpdateData(false);
	
}

//Decrement the number of the capper head for which bottles are to be ejected. Large step size.
void Filler::OnCapdnfast() 
{
	theapp->fillDataLoaded=false;
	theapp->capperHead-=2;
	if(theapp->capperHead<=0) theapp->capperHead=0;
	m_cappos.Format("%i",theapp->capperHead);

	UpdateData(false);
	
}

void Filler::OnFilupfast() 
{
	theapp->fillDataLoaded=false;
	theapp->fillerValve+=5;
	if(theapp->fillerValve>=theapp->fillerSize) theapp->fillerValve=theapp->fillerSize;
	m_filpos.Format("%i",theapp->fillerValve);
	//theapp->capEject[]+=1;
	UpdateData(false);
	
}

void Filler::OnFildnfast() 
{
	theapp->fillDataLoaded=false;
	theapp->fillerValve-=5;
	if(theapp->fillerValve<=0) theapp->fillerValve=0;
	m_filpos.Format("%i",theapp->fillerValve);
	//theapp->capEject[]+=1;
	UpdateData(false);
	
}

//Specify that bottles from all capper heads should be ejected 1 time.
void Filler::OnAllheads2() 
{
	allCap=true;
	CheckDlgButton(IDC_ALLHEADS2,1);
	UpdateData(false);
	
}

//NOT USED
void Filler::On1thru6() 
{
	oneSix=true;
	CheckDlgButton(IDC_1THRU6,1);
	UpdateData(false);
}

//NOT USED
void Filler::On7thru12() 
{
	sevenTwelve=true;
	CheckDlgButton(IDC_7THRU12,1);
	UpdateData(false);
	
}

//NOT USED
void Filler::On13thru18() 
{
	thirteenEighteen=true;
	CheckDlgButton(IDC_13THRU18,1);
	UpdateData(false);
}

//NOT USED
void Filler::On19thru24() 
{
	nineteenTwentyfour=true;
	CheckDlgButton(IDC_19THRU24,1);
	UpdateData(false);
	
}
