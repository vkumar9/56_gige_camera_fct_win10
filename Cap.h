// Cap.h : main header file for the CAP application
//
#include <mutex>

#if !defined(AFX_CAP_H__5A73635B_3FD7_49CB_8DB2_2B1311614E89__INCLUDED_)
#define AFX_CAP_H__5A73635B_3FD7_49CB_8DB2_2B1311614E89__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "ApplicationHistory.h"
#include <atomic>

#define WM_INSPECT (WM_USER + 101)
#define WM_SAVE (WM_USER + 102)
#define WM_TRIGGER (WM_USER + 103)
#define WM_NEWJOB (WM_USER + 104)
#define WM_RELOAD (WM_USER + 105)
#define WM_SETLIGHTLEVEL (WM_USER + 106)
#define WM_UPDATEVIEW (WM_USER + 107)
#define WM_UPDATEVIEW2 (WM_USER + 108)
#define WM_UPDATEVIEW3 (WM_USER + 109)
#define WM_QUE (WM_USER + 110)
#define CELEMS(rgFoo) (sizeof(rgFoo) / sizeof(rgFoo[0]))
/////////////////////////////////////////////////////////////////////////////
// CCapApp:
// See Cap.cpp for the implementation of this class
//
struct Inspctn
{

	
	CString name;	//Inspection type name. e.g. left height or low fill.
	std::atomic<int> jobNum;
	int lmin;		//Inspection lower limit
	int lmax;		//Inspection upper limit
	bool enable;	//Flag indicating if this inspection is being performed or not.
	int type;
};

struct JobInfo
{
	int vertBarX;							//Pullup inspection vertical bar coordinate, left edge of screen is 0.
	int midBarY;							//Pullup inspection mid cap horizontal bar coordinate. Top of screen is 0.
	int neckBarY;			//Pullup inspection mid neck bar coordinate. Top of screen is 0. This is set in the inspection configuration window.
	int neckBarY_offset;					//Pullup inspection mid neck bar offset from neckBarY this is set in the modify tab.
	bool is_NeckBarY_offset_negative;		//Flag indicating if the offset is below (positive) or above (negative) the neckBarY.
	

	int totinsp;							//Number of inspection types being performed.
	bool taught;							//Flag indicating if job is taught or not.
	double lightLevel1;						//Camera1 gain level
	double lightLevel2;						//Camera2 gain level
	double lightLevel3;						//Camera3 gain level
	int offX;//NOT USED
	int offY;//NOT USED
	CString jobname;						//String which holds the job's text name.
	int motPos;								//Motor position.
	bool black;//NOT USED

	bool is_diet_coke;
	int diff_for_no_cap;
	int limit_no_cap;
	int neckLlip_x;//NOT USED
	int neckRlip_x;//NOT USED


	int enable_foil_sensor; // only holds whether to enable or disable foil sensor
	////
	int cam2_ll;
	int cam3_ll;

	int cam2_ul;
	int cam3_ul;

	int cam1_ll;
	int cam1_ul;

	int bottleReferenceBar;					//Camera1 top search limit bar
	int bottleReferenceBar_cam2;			//Camera2 top search limit bar
	int bottleReferenceBar_cam3;			//Camera3 top search limit bar

	bool is_fillX_neg;						//low fill search box x coordinate is negative.


/////

	
	int profile;//NOT USED
	int u2Thresh1;//NOT USED
	int u2Thresh2;//NOT USED
	int u2XOffset;//NOT USED
	int u2YOffset;//NOT USED
	int u2XOffsetR;//NOT USED
	int u2YOffsetR;//NOT USED
	int u2Pos;//NOT USED

	int lipX;

	int rDist;								//Distance between the right cap edge and the right vertical line. 
	int lDist;								//Distance between the left cap edge and the left vertical line.

	int tBandX;								//Front camera left edge tamper band x position.
	int tBandY;								//Front camera left edge tamper band y position.
	int tBandDx;							//Front camera left edge tamper band width.
	int tBandDy;							//Front camera left edge tamper band height.

	int tBandThresh;						//Tamper band edge intensity threshold. Used in tamper band edge search boxes.
											//If a pixel has a value above the threshold it is considered to be a broken band area.
	int tBandSurfThresh;					//Tamper band front (camera1) surface intensity threshold
	int tBandSurfThresh2;					//Tamper band surface cameras 2 and 3 intensity threshold
	int tBandType;							//Hold variable which indicates which type of TB surface algo is used. 1 = Edge Detection, 2 = Threshold, 3 = Shaded average, 4 = Integrity check, 5 = Compare Color

	int rSWin;								//Camera3 cap edge search box.
	int lSWin;								//Camera2 cap edge search box.
	int rSWin2;//NOT USED
	int lSWin2;//NOT USED
	int rearCamDy;						//Cameras 2 and 3 tamper band search boxes height. Each box has the same height, which is equal to this value.
	int rearCamY;						//Cameras 2 and 3 vertical tamper band search boxes position. Used for moving all the boxes together.

	int Rear_ht_Diff;
	bool do_sobel;
	bool etb_cam1,etb_cam2,etb_cam3;

	int tBand2X;
	int tBand2Y;
	int tBand2Dx;
	signed	int fillX;					//low fill search box horizontal position, relative to the center of the bottle.
	int fillY;							//low fill search box vertical position. Top of screen is 0.
	
	//Cap color search variables.
	int userStrength;
	int userX;							//Cap color search box x coordinate
	int userY;							//Cap color search box y coordinate
	int userDx;							//Cap color search box width.
	bool active;
	int taught_cap_width;

	int lipy;							//Used for no lip bottles, phantom lip area search box y coordinate
	int lipx;							//Used for no lip bottles, phantom lip area search box x coordinate
	int lip;							//Used to track bottle lip type. 0 = normal, 1 = no lip.
	int lipYOffset;						//Used for no lip bottles, offset of right area search box with respect to left.
	int lipSize;						//Used for no lip bottles, phantom lip area search box height.
	int sensitivity;					//Used for no lip bottles, indicates that only left bottle shoulder search box will be used.
	int enable;
	int dark;							//dark threshold.
	int rearSurfThresh;
	int lBandOffset[6];					//Stores rear camera2 tamper band break search boxes vertical position offsets. Top of image is 0.
	int rBandOffset[6];					//Stores rear camera3 tamper band break search boxes vertical position offsets. Top of image is 0.
	int capWidthLimit;					//Used for detecting missing cap.
	int colorTargR;						//Cap color taught R value.	
	int colorTargG;						//Cap color taught G value.
	int colorTargB;						//Cap color taught B value.	
	int rightBandShift;					//Horizontal offset of rear (camera3) tamper band search boxes. Left edge of cap is 0.
	int leftBandShift;					//Horizontal offset of rear (camera2) tamper band search boxes. Right edge of cap is 0.
	int band2Size;						//Cameras 2 and 3 tamper band search boxes width. This value is the sum of the width of the search boxes.
	int bandColor;						//The color of the color based tamper band inspection.
	int sport;							//Bottle cap type, 0 = normal, 1 = sports cap, 2 = foil
	int userType;
	int neckOffset;						//X Delta between neck left side and cap left side.
	int ringLight;						//Ring light on duration (brightness).
	int backLight;
	int sportPos;
	int sportSen;
	bool xtraLight;
	int redCutOff;						//Extra light filter intensity.
	int fillArea;						//Low fill extra light search area.
	int snapDelay;						//Encoder count between the photoeye triggering and image acquisition.
	int slatPW;							//Stores ejector pulse width modulation.
	int encDur;							//Stores encoder count for how long the ejector should remain on.
	int encDist;						//Stores encoder count for how far the ejector is from the photo eye.
	int ScaleFactor;
	bool useLeft;
	bool useRing;						//Flag indicating if ring light is on or off.
};

class CCapApp : public CWinApp
{
friend class CMainFrame;

public:
	//ML- 
	float	bandwidth1;		//Camera bus 1 band width, for camera 1
	float	bandwidth2;		//Camera bus 2 band width, for cameras 2 and 3 
	std::atomic<int>	whitebalance1Red;	//Camera 1 White Balance Red Component
	std::atomic<int>	whitebalance2Red;	//Camera 2 White Balance Red Component
	std::atomic<int>	whitebalance3Red;	//Camera 3 White Balance Red Component
	std::atomic<int>	whitebalance1Blue;	//Camera 1 White Balance Blue Component
	std::atomic<int>	whitebalance2Blue;	//Camera 2 White Balance Blue Component
	std::atomic<int>	whitebalance3Blue;	//Camera 3 hite Balance Blue Component

	int		inspDelay;		//Inspection delay is a time window used to synchronize the transfer of images from the 3 cameras.
							//It is  used in the first cameras' display() function in the XAxisView. The idea is that after an image from 
							//camera 1 is transfered it will take a maximum of inspDelay to tranfer images from the other 2 cameras.
							//After inspDelay the 

	////////////////////////

	bool StartDelay;			//NOT USED

	bool savepics;		//Flag indicating if all pictures will be saved to the hard drive.

	//////////////////

	//Camera serial numbers
	int CamSer1, CamSer2, CamSer3, CamSer4;
	//The below values are subtracted from the calculated cocked cap values for cameras 1 and 2.
	//This is used to fine tune cocked cap limits when there is a large difference between the computed
	//cocked cap values of cameras 1 and 2 for a good cap.

	int cam3_ht_offset;			//Cocked cap camera 2 offset.
	int cam2_ht_offset;			//Cocked cap camera 3 offset.
////////////////////

	//CICapMod    *m_pBrdMod;   // Ptr array 2 PCVision2 board capture modules
    //CICamera    *m_pCam;
	bool rejectFromUser;		//Flag indicating that eject all bottles signal from user was received.
	bool SavedForcedEjects;		//Flag indicating that bottles should be rejected as long as PLC input remains high.
	CString SavedMaster;		//Saved master password.
	CString SavedOPPassWord;	//Saved operator password.
	CString	SavedSUPPassWord;	//Saved supervisor password.
	CString	SavedMANPassWord;	//Saved manager password.
	int isRed_block_North;		//NOT USED
void ReadRegistery(int JobNum);
void ReadRegistery2();
	struct Inspctn inspctn[11];		//Array of inspection limits and on/off status. 1= Height left, 2= Height right, 3= Height mid, 4 = cocked, 5 = band edge, 6= band surface, 7= low fill, 8 = color, 9 and 10 = user
	struct JobInfo jobinfo[51];		//Array of job settings.

	std::mutex jobMutex;

public:
	void DeleteRegKey(int index);

	int WhichCam;				//NOT USED
	int tempSelect;				//Show temperature from PLC: tempSelect = 1, show temperature from USB sensor: tempSelect = 2.
	int USB_Temperature;		//Current capper number
	int PLC_Temperature;		//Temperature from the PLC temperature sensor.

	bool NoSerialComm;				//Flag indicating the computer is connected to a PLC.

	int inspRatio;
	int capWidth;					//Computed cap width in pixels
	float shutterSpeed;				//Camera 1 shutter speed.
	float shutterSpeed2;			//Camera 2 shutter speed.
	float shutterSpeed3;			//Camera 3 shutter speed.
	int xtraYOffset;				//Rear Cameras Tamper Band Break Search Boxes Vertical Offset
	int capWidth2;					//Difference between learned cap width and current cap width.
	int convRatio;					//conveyor speed offset.
	bool slatBroken;
	bool surfRR;					//Flag indicating if rear (camera 3) tamper band surface inspection is enabled.
	bool surfLR;					//Flag indicating if rear (camera 2) tamper band surface inspection is enabled.
	bool userRR;					//NOT USED
	bool userLR;					//NOT USED
	bool check_Rband;				//Flag indicating if rear (cameras 2, 3) tamper band surface inspection is enabled.
	int tempLimit;				//Control cabinet temperature limit
	bool showFC;				//Flag indicating that the filler/capper statistics should be shown in the statistics tab.
	int TotalNet;				//Variable used to track the total number of bottles scanned, this variable is used
								//for checking if new information needs to be sent to user PLC. If the oldtotal is
								//less than TotalNet then send new data to user PLC.
	bool resetFromPLC;			//Flag indicating that a reset signal was sent from the PLC. Used in serial dialog box.
	int statusInt;				//NOT USED
	bool fillerEject;				//Flag indicating if the current bottle should be ejected.
	bool enableWidth;			//NOT USED, Check for missing cap using cap width limit, on/off
	int hourClock;					//Hour of day at which to save collected defect data. Used in data dialog box.
	int lostCam;				//NOT USED
	int fillerPulse;
	int capperCount;		//Current capper number
	int fillerCount;		//Current filler number
	int capperCount2;		//Last ejected capper number
	int fillerCount2;		//Last ejected filler number
	bool waitForOne;		//Variable used to start capper ejection from head number 1.
	bool fillDataLoaded;	//Flag indicating that filler/capper ejector settings are being modified and thus should not be used.
	int capPocketOffset;	//Filler head offset, offset between the head number at which the system starts and the actual 0 head.
	int fillPocketOffset;	//Filler head offset, offset between the head number at which the system starts and the actual 0 head.
	int fillerSize;			//Number of filler heads on the filler machine.
	int capperSize;			//Number of capper heads on the capper machine.
	int numConvPockets;		//Number of bottles between the exit of the capper/filler system and the camera tunnel.
	int convInches;			//Length of the conveyor from the exit of the capper to the vision system.
	int fillerValve;		//Index specifying from which filler head bottles should be rejected.
	int capperHead;			//Index specifying from which capper head bottles should be rejected.
	int capperNum;			//Variable which holds the number of bottles which should be ejected from a specific capper head.
	int fillerNum;			//Variable which holds the number of bottles which should be ejected from a specific filler head.
	bool fillerOpen;		//Flag indicating the the fill/capper dialog box is open.
	int fillEject[100];		//Array which holds the number of bottles which should be ejected from each filler head.
	int capEject[100];		//Array which holds the number of bottles which should be ejected from each capper head.
	bool showAlign;			//Flag indicating if the camera alignment overlay is displayed.
	int SavedDeadBand;		//The deadband delay, "cap width", prevents one bottle from triggering the photoeye multiple times. Similar to debouncing. Used in delay trigger dialog box.
	
	int SavedDBOffset;		//Bottle down sensor encoder count to ejector. Used in the bottle down dialog box.
	bool teachFrontBand;	//Flag indicating that some tamper band algorithm related to shaded average should be re-taught. Does not appear to be used.
	float rcorr;				//NOT USED
	float lcorr;				//NOT USED
	
	bool saveOne1;				//NOT USED
	bool saveOne1c;				//NOT USED
	bool saveOne2;				//NOT USED
	bool saveOne2c;				//NOT USED
	bool saveOne3;				//NOT USED
	bool saveOne3c;				//NOT USED
	CMainFrame* pframeSub;
	int timedTrigger;			//Number of milliseconds between timed triggers.
	bool commProblem;			//Flag indicating that there was a problem opening the COM port.
	bool UserLip;				//NOT USED
	bool DidTriggerOnce;		//NOT USED
	int cam1XOffset;			//Used for drawing cap inspection overlays in the inspection dialog box.
	int SavedTotalJobs;			//Indicates the total number of job slots, used for generating the jobs list box in the jobs dialog box.
	float bandwidth;			//NOT USED
	int cam1Height;				//Variable for holding camera 1 height.
	int cam1Width;				//Variable for holding camera 1 width.
	int cam1Top;				//Variable for holding camera 1 offset from top edge.
	int cam1Left;				//Variable for holding camera 1 offset from left edge.

	int cam2Height;				//Variable for holding camera 2 height.
	int cam2Width;				//Variable for holding camera 2 width.
	int cam2Top;				//Variable for holding camera 2 offset from top edge.
	int cam2Left;				//Variable for holding camera 2 offset from left edge

	int cam3Height;				//Variable for holding camera 3 height.
	int cam3Width;				//Variable for holding camera 3 width.
	int cam3Top;				//Variable for holding camera 3 offset from top edge.
	int cam3Left;				//Variable for holding camera 3 offset from left edge.

	/////////
	bool checkRearHeight;		//Flag indicating if the cocked cap inspection should be performed.
	int cam2diff;				//Height difference between the lowest and highest point of cap in camera 2. Used for checking cocked cap.
	int cam3diff;				//Height difference between the lowest and highest point of cap in camera 3. Used for checking cocked cap.

	//////
	bool noCams;
	bool Color;							//NOT USED
	bool NoCard;						//NOT USED	
	bool NoLip;							//NOT USED
	int SavedJ1Profile;					//NOT USED
	int SavedJ2Profile;					//NOT USED
	int SavedJ3Profile;					//NOT USED
	int SavedJ4Profile;					//NOT USED
	int SavedJ5Profile;					//NOT USED
	int SavedJ6Profile;					//NOT USED
	int SavedJ7Profile;					//NOT USED
	int SavedJ8Profile;					//NOT USED
	int SavedJ9Profile;					//NOT USED
	int SavedJ10Profile;				//NOT USED
	bool ForceLightWJob;				//NOT USED
	void SaveDataToReg();
	void DeleteKey(int JobNum);
	bool SavedKillSave;				//NOT USED
	bool SavedKillSD;				//NOT USED
	
	int SavedConRejects;	//Number of bottles consecutively rejected before the customer PLC output is set to high.
	int SavedIORejects;		//Number of bottles to be rejected after the customer PLC input is set to high.
	bool SecOff;			//Used in main frame to disable enable security settings.	
	bool SavedPhotoeye;		//Flag indicating direction of product flow and correspondingly which photoeye is used.
	int SavedTBl;			//Used in X axis.
	int SavedRightSWin;			//NOT USED
	int SavedLeftSWin;			//NOT USED
 
	int SavedEjectDisable;		//Flag indicating if the ejector is enabled or disabled. Used in the blowoff dialog box.
	bool SavedEncSim;			//Flag indicating if the conveyor encoder or timer is used.
	bool Motor;
	int SavedJob;				//Flag indicating if user can use the jobs dialog box.
	int SavedBottleSensor;		//Flag indicating if bottle down sensor is enabled. Used in the bottle down dialog box.
	bool SavedTech;				//Flag indicating if user can use the rejector dialog box.
	int SavedBlack;				//NOT USED
	int SavedInspColor;			//NOT USED
	
	int SavedTB2H;				//NOT USED
	int SavedTBH;				//NOT USED
	int SavedFillH;					//Low fill search box Height.
	int SavedFillW;					//Low fill search box width.
	
	int SavedTB2YOffset;		//NOT USED
	int SavedTB2XOffset;		//NOT USED

	int SavedLightLevel;		//NOT USED
	
	bool SavedModify;		//Flag indicating if user can use the modify tab.
	bool SavedLimits;		//Flag indicating if user can use the limits tab.
	bool SavedSetup;		//Flag indicating if user can use the configure inspection dialog box.
	CString SavedPassWord;
	int SavedWidth;				//NOT USED
	int SavedThresh;			//NOT USED
//	bool Single;
	int SavedDelay;			//Number of encoder pulses per inch. This is used to compute the total number of encoder
							//pulses between the exit of the filler/capper machine and the camera tunnel. This is also 
							//used to compute the number of encoder pulses between 2 successive
							//bottles exiting the filler/capper machine. 
	bool SavedEject;		//Flag indicating if user can use the rejector dialog box.
	int SavedDataFile;		//Number for save data file. This is used once a day to save scan and defect statistics. Used in data dialog box. Also used when manual print to file is used.
	
	int slx;					//NOT USED
	int sldx;					//NOT USED
	int sly;					//NOT USED
	int sldy;					//NOT USED
	int srx;					//NOT USED
	int srdx;					//NOT USED
	int sry;					//NOT USED
	int srdy;					//NOT USED
	
	//int SavedEncDur;
	//int SavedEncDist;

//NOT USED
	CString SavedJ1Name;
	CString SavedJ2Name;
	CString SavedJ3Name;
	CString SavedJ4Name;
	CString SavedJ5Name;
	CString SavedJ6Name;
	CString SavedJ7Name;
	CString SavedJ8Name;
	CString SavedJ9Name;
	CString SavedJ10Name;

	CString SavedD1Name;
	CString SavedD2Name;
	CString SavedD3Name;
	CString SavedD4Name;
	CString SavedD5Name;
	CString SavedD6Name;
	CString SavedD7Name;
	CString SavedD8Name;
	CString SavedD9Name;
	CString SavedD10Name;
//

	CCapApp();
	
	DWORD m_childCount;			//NOT USED
	
//	unsigned long m_ulCounter[THREAD_LAST];
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCapApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CCapApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAP_H__5A73635B_3FD7_49CB_8DB2_2B1311614E89__INCLUDED_)
