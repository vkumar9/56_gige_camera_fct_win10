// TBColor.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "TBColor.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TBColor dialog


TBColor::TBColor(CWnd* pParent /*=NULL*/)
	: CDialog(TBColor::IDD, pParent)
{
	//{{AFX_DATA_INIT(TBColor)
	m_result = _T("");
	//}}AFX_DATA_INIT
}


void TBColor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TBColor)
	DDX_Text(pDX, IDC_RESULT, m_result);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TBColor, CDialog)
	//{{AFX_MSG_MAP(TBColor)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TBColor message handlers

BOOL TBColor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_ptbcolor=this;
	theapp=(CCapApp*)AfxGetApp();
		//Initialize the dialog box to show saved data.
		switch(theapp->jobinfo[pframe->CurrentJobNum].bandColor)
		{
			case 1:
				CheckDlgButton(IDC_RADIO1,1);
				CheckDlgButton(IDC_RADIO2,0);
				CheckDlgButton(IDC_RADIO3,0);
				CheckDlgButton(IDC_RADIO4,0);
				color=1;
				prod=1;
				
			break;

			case 2:
				CheckDlgButton(IDC_RADIO1,0);
				CheckDlgButton(IDC_RADIO2,1);
				CheckDlgButton(IDC_RADIO3,0);
				CheckDlgButton(IDC_RADIO4,0);
				color=2;
				prod=1;
				
			break;
			
			case 3:
				CheckDlgButton(IDC_RADIO1,0);
				CheckDlgButton(IDC_RADIO2,0);
				CheckDlgButton(IDC_RADIO3,1);
				CheckDlgButton(IDC_RADIO4,0);
				color=3;
				prod=1;
				
			break;

			case 4:
				CheckDlgButton(IDC_RADIO1,0);
				CheckDlgButton(IDC_RADIO2,0);
				CheckDlgButton(IDC_RADIO3,0);
				CheckDlgButton(IDC_RADIO4,1);
				color=4;
				prod=1;
				
			break;
			case 5:
				CheckDlgButton(IDC_RADIO1,0);
				CheckDlgButton(IDC_RADIO2,0);
				CheckDlgButton(IDC_RADIO3,0);
				CheckDlgButton(IDC_RADIO4,0);
				color=5;
				prod=1;
				
			break;
		}
		m_result.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].bandColor);
		UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Select blue.
void TBColor::OnRadio1() 
{
	color=1;
	prod=1;
	show();
}

//Select red.
void TBColor::OnRadio2() 
{
	color=2;
	prod=1;
	show();
}

//Select green.
void TBColor::OnRadio3() 
{
	color=3;
	prod=1;
	show();
}

//Select orange
void TBColor::OnRadio4() 
{
	color=4;
prod=1;
	show();
	
}

//Updates current job with selected color.
void TBColor::show()
{
switch(color)
	{
	case 1:
		switch(prod)
		{
			case 1:
				theapp->jobinfo[pframe->CurrentJobNum].bandColor=1;
			break;

			case 2:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=2;
			break;
			
			case 3:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=3;
			break;

			case 4:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=4;
			break;
		}

		break;
	case 2:
		switch(prod)
		{
			case 1:
				theapp->jobinfo[pframe->CurrentJobNum].bandColor=2;
			break;

			case 2:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=6;
			break;
			
			case 3:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=7;
			break;

			case 4:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=8;
			break;
		}
		break;

	case 3:

		switch(prod)
		{
			case 1:
				theapp->jobinfo[pframe->CurrentJobNum].bandColor=3;
			break;

			case 2:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=10;
			break;
			
			case 3:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=11;
			break;

			case 4:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=12;
			break;
		}

		break;
	case 4:

		switch(prod)
		{
			case 1:
				theapp->jobinfo[pframe->CurrentJobNum].bandColor=4;
			break;

			case 2:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=10;
			break;
			
			case 3:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=11;
			break;

			case 4:
				//theapp->jobinfo[pframe->CurrentJobNum].bandColor=12;
			break;
		}

		break;


	}
m_result.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].bandColor);
UpdateData(false);
}



