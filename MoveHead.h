#if !defined(AFX_MOVEHEAD_H__86E95A1D_8E2B_4C87_92AE_C22B101E1A04__INCLUDED_)
#define AFX_MOVEHEAD_H__86E95A1D_8E2B_4C87_92AE_C22B101E1A04__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MoveHead.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// MoveHead dialog

class MoveHead : public CDialog
{
friend class CCapApp;
friend class CMainFrame;
// Construction
public:
	int count;	//Used to track the number of times the motor has tried to reach the saved position for the job.
	bool go;	//Not used.
	MoveHead(CWnd* pParent = NULL);   // standard constructor
void OnHigh();
void OnLow();
void OnHighSlow();
void OnLowSlow();
void CheckMotorPos();

	int MotorPos;	//Stores current motor position

	int CntrBits;//Not used
	int Function;//Not used
	int Value;//Not used

	CCapApp *theapp;
	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(MoveHead)
	enum { IDD = IDD_MOVEHEAD };
	CString	m_mpos;		//Current motor position
	CString	m_mpos2;	//Target motor position
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MoveHead)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MoveHead)
	virtual BOOL OnInitDialog();
	afx_msg void OnStart();
	afx_msg void OnStop();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnMotorup();
	afx_msg void OnMotordn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MOVEHEAD_H__86E95A1D_8E2B_4C87_92AE_C22B101E1A04__INCLUDED_)
