#if !defined(AFX_TBCOLOR_H__02A1FC60_95EA_4E6A_BB13_E2BFFE430D4D__INCLUDED_)
#define AFX_TBCOLOR_H__02A1FC60_95EA_4E6A_BB13_E2BFFE430D4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TBColor.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// TBColor dialog

class TBColor : public CDialog
{
friend class CMainFrame;
friend class CCapApp;
// Construction
public:
	TBColor(CWnd* pParent = NULL);   // standard constructor
void show();
	int prod;
	int color;						//Number of the color radio button selected.

CMainFrame* pframe;
CCapApp* theapp;
// Dialog Data
	//{{AFX_DATA(TBColor)
	enum { IDD = IDD_TBCOLOR };
	CString	m_result;				//Debug output showing the number of the radio button selected.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TBColor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(TBColor)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TBCOLOR_H__02A1FC60_95EA_4E6A_BB13_E2BFFE430D4D__INCLUDED_)
