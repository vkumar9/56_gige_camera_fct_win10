// Stats.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Stats.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Stats dialog


Stats::Stats(CWnd* pParent /*=NULL*/)
	: CDialog(Stats::IDD, pParent)
{
	//{{AFX_DATA_INIT(Stats)
	m_st1 = _T("");
	m_st2 = _T("");
	m_st3 = _T("");
	m_st4 = _T("");
	m_st5 = _T("");
	m_st6 = _T("");
	m_st7 = _T("");
	m_st8 = _T("");
	m_st9 = _T("");
	//}}AFX_DATA_INIT
}


void Stats::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Stats)
	DDX_Text(pDX, IDC_1, m_st1);
	DDX_Text(pDX, IDC_2, m_st2);
	DDX_Text(pDX, IDC_3, m_st3);
	DDX_Text(pDX, IDC_4, m_st4);
	DDX_Text(pDX, IDC_5, m_st5);
	DDX_Text(pDX, IDC_6, m_st6);
	DDX_Text(pDX, IDC_7, m_st7);
	DDX_Text(pDX, IDC_8, m_st8);
	DDX_Text(pDX, IDC_9, m_st9);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Stats, CDialog)
	//{{AFX_MSG_MAP(Stats)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Stats message handlers

BOOL Stats::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pstats=this;
	theapp=(CCapApp*)AfxGetApp();	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Stats::UpdateDisplay()
{
	if(pframe->Total<=0 )
	{
		m_st1.Format("%3.2f",0);
		m_st2.Format("%3.2f",0);
 		m_st3.Format(" %3i",0);
		m_st4.Format(" %3i",0);
		m_st5.Format(" %3i",0);
		m_st6.Format(" %3i",0);
		m_st7.Format(" %3i",0);
		m_st8.Format(" %3i",0);
		m_st9.Format(" %3i",0);

	}
	else
	{
		m_st1.Format("%3.2f",pframe->BottleRate);
		m_st2.Format("%3.2f",pframe->EjectRate);
	
		m_st3.Format(" %3i",pframe->m_pxaxis->MHCnt);
		m_st4.Format(" %3i",pframe->m_pxaxis->LHCnt);
		m_st5.Format(" %3i",pframe->m_pxaxis->RHCnt);
		m_st6.Format(" %3i",pframe->m_pxaxis->DCnt);
		m_st7.Format(" %3i",pframe->m_pxaxis->FCnt);
		m_st8.Format(" %3i",pframe->m_pxaxis->TBCnt);
		m_st9.Format(" %3i",pframe->m_pxaxis->UCnt);
	
	}
	
	UpdateData(false);
	InvalidateRect(NULL,false);

}
