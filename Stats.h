#if !defined(AFX_STATS_H__90252499_F3CF_484B_835D_D8EFCE83C80B__INCLUDED_)
#define AFX_STATS_H__90252499_F3CF_484B_835D_D8EFCE83C80B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Stats.h : header file
//
friend class CCapApp;
friend class CMainFrame;

#define IDD_STATS 162
/////////////////////////////////////////////////////////////////////////////
// Stats dialog

class Stats : public CDialog
{
// Construction
public:
	void UpdateDisplay();
	Stats(CWnd* pParent = NULL);   // standard constructor

	CCapApp *theapp;
	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(Stats)
	enum { IDD = IDD_STATS };
	CString	m_st1;
	CString	m_st2;
	CString	m_st3;
	CString	m_st4;
	CString	m_st5;
	CString	m_st6;
	CString	m_st7;
	CString	m_st8;
	CString	m_st9;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Stats)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Stats)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATS_H__90252499_F3CF_484B_835D_D8EFCE83C80B__INCLUDED_)
