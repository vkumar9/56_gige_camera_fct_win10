#if !defined(AFX_BLOWOFF_H__A36BA62E_8876_4EAF_AF38_DFC3A086319C__INCLUDED_)
#define AFX_BLOWOFF_H__A36BA62E_8876_4EAF_AF38_DFC3A086319C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Blowoff.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Blowoff dialog

class Blowoff : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;

public:
	void Enable(bool off);
	Blowoff(CWnd* pParent = NULL);   // standard constructor

	long Value;		//NOT USED
	int Function;	//NOT USED
	int CntrBits;	//NOT USED
// Dialog Data
	//{{AFX_DATA(Blowoff)
	enum { IDD = IDD_BLOWOFF };
	CString	m_dist;					//Displays blowoff distance.
	CString	m_dur;					//Displays blowoff duration.
	CString	m_enable;				//String which used to display ejector enabled/disabled status.
	CString	m_slatpw;				//String which used to display ejector pulse width modulation status.
	//}}AFX_DATA
	CMainFrame* pframe;
	CCapApp* theapp;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Blowoff)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Blowoff)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnM2();
	afx_msg void OnL2();
	afx_msg void OnMore2();
	afx_msg void OnLess2();
	afx_msg void OnEnable();
	virtual void OnOK();
	afx_msg void OnPwmore();
	afx_msg void OnPwless();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLOWOFF_H__A36BA62E_8876_4EAF_AF38_DFC3A086319C__INCLUDED_)
