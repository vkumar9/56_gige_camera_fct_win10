// Modify.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Modify.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "FillerGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Modify dialog


Modify::Modify(CWnd* pParent /*=NULL*/)
	: CDialog(Modify::IDD, pParent)
{
	//{{AFX_DATA_INIT(Modify)
	m_capneck = _T("");
	m_capmiddle = _T("");
	m_captop = _T("");
	m_edits = _T("");
	m_edite = _T("");
	m_edits2 = _T("");
	m_choice = _T("");
	m_sportcapedit = _T("");
	m_editcutoff = _T("");
	m_diff_var = _T("");
	m_limit = _T("");
	//}}AFX_DATA_INIT
}


void Modify::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Modify)
	DDX_Control(pDX, IDC_FILTER, m_cfilter);
	DDX_Control(pDX, IDC_FILTERMORE, m_cfiltermore);
	DDX_Control(pDX, IDC_FILTERLESS, m_cfilterless);
	DDX_Control(pDX, IDC_EDITCUTOFF, m_ceditcutoff);
	DDX_Control(pDX, IDC_COLORINST, m_colorinst);
	DDX_Control(pDX, IDC_SCA, m_sca);
	DDX_Control(pDX, IDC_SCS, m_scs);
	DDX_Control(pDX, IDC_SC_EDIT, m_sportcapeditc);
	DDX_Control(pDX, IDC_SPORTCAP_DN, m_sportcapdn);
	DDX_Control(pDX, IDC_SPORTCAP_LESS, m_sportcapless);
	DDX_Control(pDX, IDC_SPORTCAP_MORE, m_sportcapmore);
	DDX_Control(pDX, IDC_SPORTCAP_UP, m_sportcapup);
	DDX_Control(pDX, IDC_SPORTCAPFRAME, m_sportcapframe);
	DDX_Control(pDX, IDC_TBCOLOR, m_tbcolor);
	DDX_Control(pDX, IDC_EDITE, m_editec);
	DDX_Control(pDX, IDC_EDITS, m_editsc);
	DDX_Control(pDX, IDC_SDN, m_sdn);
	DDX_Control(pDX, IDC_SUP, m_sup);
	DDX_Text(pDX, IDC_CAPNECK, m_capneck);
	DDX_Text(pDX, IDC_CAPMIDDLE, m_capmiddle);
	DDX_Text(pDX, IDC_TOPLEFT, m_captop);
	DDX_Text(pDX, IDC_EDITS, m_edits);
	DDX_Text(pDX, IDC_EDITE, m_edite);
	DDX_Text(pDX, IDC_EDITS2, m_edits2);
	DDX_Text(pDX, IDC_CHOICE, m_choice);
	DDX_Text(pDX, IDC_SC_EDIT, m_sportcapedit);
	DDX_Text(pDX, IDC_EDITCUTOFF, m_editcutoff);
	DDX_Text(pDX, IDC_diff, m_diff_var);
	DDX_Text(pDX, IDC_FAST, m_enable);
	DDX_Text(pDX, IDC_limit, m_limit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Modify, CDialog)
	//{{AFX_MSG_MAP(Modify)
	ON_BN_CLICKED(IDC_LLEFT, OnLleft)
	ON_BN_CLICKED(IDC_LRIGHT, OnLright)
	ON_BN_CLICKED(IDC_MUP, OnMup)
	ON_BN_CLICKED(IDC_MDN, OnMdn)
	ON_BN_CLICKED(IDC_NUP, OnNup)
	ON_BN_CLICKED(IDC_NDN, OnNdn)
	ON_BN_CLICKED(IDC_TBLEFT, OnTbleft)
	ON_BN_CLICKED(IDC_TBUP, OnTbup)
	ON_BN_CLICKED(IDC_TBDN, OnTbdn)
	ON_BN_CLICKED(IDC_TBRIGHT, OnTbright)
	ON_BN_CLICKED(IDC_SUP, OnSup)
	ON_BN_CLICKED(IDC_SDN, OnSdn)
	ON_BN_CLICKED(IDC_FUP, OnFup)
	ON_BN_CLICKED(IDC_FDN, OnFdn)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_FILLLEFT, OnFillleft)
	ON_BN_CLICKED(IDC_FILLRIGHT, OnFillright)
	ON_BN_CLICKED(IDC_SUP2, OnSup2)
	ON_BN_CLICKED(IDC_SDN2, OnSdn2)
	ON_BN_CLICKED(IDC_FAST, OnFast)
	ON_BN_CLICKED(IDC_COLOR, OnColor)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_TBCOLOR, OnTbcolor)
	ON_BN_CLICKED(IDC_SUP3, OnSup3)
	ON_BN_CLICKED(IDC_SDN3, OnSdn3)
	ON_BN_CLICKED(IDC_SPORTCAP_UP, OnSportcapUp)
	ON_BN_CLICKED(IDC_SPORTCAP_DN, OnSportcapDn)
	ON_BN_CLICKED(IDC_SPORTCAP_MORE, OnSportcapMore)
	ON_BN_CLICKED(IDC_SPORTCAP_LESS, OnSportcapLess)
	ON_BN_CLICKED(IDC_FILTERMORE, OnFiltermore)
	ON_BN_CLICKED(IDC_FILTERLESS, OnFilterless)
	ON_BN_CLICKED(IDC_ViewGraph, OnViewGraph)
	ON_BN_CLICKED(IDC_CHECK1, OnCheckRearHeight)
	ON_BN_CLICKED(IDC_CHECK2, OnSavePics)
	ON_BN_CLICKED(IDC_BUTTON10, OnIncrDiff)
	ON_BN_CLICKED(IDC_BUTTON12, OnDcrDiff)
	ON_BN_CLICKED(IDC_BUTTON2, OnIncrLimit)
	ON_BN_CLICKED(IDC_BUTTON3, OnDcrlimit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Modify message handlers

BOOL Modify::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pmod=this;
	theapp=(CCapApp*)AfxGetApp();
	res=1;
	toggle=true;CheckDlgButton(IDC_FAST,0); m_enable.Format("%s","Fast");
	newTarget=false;

/*	if(theapp->checkRearHeight ==true){CheckDlgButton(IDC_CHECK1,1);}
	else{CheckDlgButton(IDC_CHECK1,0);}
	*/

	if(theapp->savepics ==true){CheckDlgButton(IDC_CHECK2,1);}
	else{CheckDlgButton(IDC_CHECK2,0);}


	//m_spin2.SetRange(0, 500);
	//m_edite.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandThresh);

	//UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//Move left edge line left.
void Modify::OnLleft() 
{

	theapp->jobinfo[pframe->CurrentJobNum].vertBarX-=res;
	theapp->jobinfo[pframe->CurrentJobNum].rDist+=1;
	theapp->jobinfo[pframe->CurrentJobNum].lDist-=1;
	m_captop.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);	//Update displayed position of left edge line.

	SetTimer(1,3000,NULL);	//Used to update the job with modified parameters. Why not just call a function instead of a timer?
	UpdateData(false);
}

//Move left edge line right.
void Modify::OnLright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX+=res;
	theapp->jobinfo[pframe->CurrentJobNum].lDist+=1;
	theapp->jobinfo[pframe->CurrentJobNum].rDist-=1;
	m_captop.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move middle of cap line up.
void Modify::OnMup() 
{
	//if(theapp->jobinfo[pframe->CurrentJobNum].lip!=1)
	//{
		theapp->jobinfo[pframe->CurrentJobNum].midBarY-=res;
		if(theapp->jobinfo[pframe->CurrentJobNum].midBarY<=0) 	//line cannot go below vertical 0.
			pframe->CurrentJobb=theapp->jobinfo[pframe->CurrentJobNum].midBarY=0;
		m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);

		SetTimer(1,3000,NULL);
	//}
	//else
	//{
	//	AfxMessageBox("This setting can only be adjusted in the Configure Inspections window");
	//}

	UpdateData(false);
	
}

//Move middle of cap line down.
void Modify::OnMdn() 
{
	//if(theapp->jobinfo[pframe->CurrentJobNum].lip!=1)
	//{
		theapp->jobinfo[pframe->CurrentJobNum].midBarY+=res;
		if(pframe->m_pxaxis->MidBar.y>=480) pframe->CurrentJobb=theapp->jobinfo[pframe->CurrentJobNum].midBarY=480;
		m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);

		SetTimer(1,3000,NULL);
	//}
	//else
	//{
		//AfxMessageBox("This setting can only be adjusted in the Configure Inspections window");
	//}

	UpdateData(false);
}

//Move middle of neck line (lip locator line) up.
void Modify::OnNup() 
{
/*
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY-=res;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);

*/	
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset-=1;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset);

	if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset <0 ) theapp->jobinfo[pframe->CurrentJobNum].is_NeckBarY_offset_negative=true;
	else if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset >=0) theapp->jobinfo[pframe->CurrentJobNum].is_NeckBarY_offset_negative=false;


	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move middle of neck line (lip locator line) down.
void Modify::OnNdn() 
{
/*	
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY+=res;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY);
*/
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset+=1;
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset);

	if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset <0 ) theapp->jobinfo[pframe->CurrentJobNum].is_NeckBarY_offset_negative=true;
	else if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset >=0) theapp->jobinfo[pframe->CurrentJobNum].is_NeckBarY_offset_negative=false;



	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//All of the below adjustments will also be applied to the right edge tamper band search box.
//The right search box is generated automatically. The width of the surface tamper band search box 
//is also generated automatically based on the position of the left tamper band search box.


//Move the left edge tamper band search box up.
void Modify::OnTbup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandY-=1;
	pframe->m_pxaxis->TBOffset.y-=2;

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move the left edge tamper band search box down.
void Modify::OnTbdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandY+=1;
	pframe->m_pxaxis->TBOffset.y+=2;


	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move the left edge tamper band search box left.
void Modify::OnTbright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandX+=res;
	pframe->m_pxaxis->TBOffset.x+=1;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move the left edge tamper band search box right.
void Modify::OnTbleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandX-=res;
	pframe->m_pxaxis->TBOffset.x-=1;

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

//Increase the threshold for tamper band surface front (camera1) search box.
//Pixels in the tamper band search box with intensities above/below (depending on algorithm selection) this threshold are considered a broken band.
void Modify::OnSup() 
{
	//Threshold has to be between 0 and 255, since we are using 8 bits per pixel intensity.
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh < 0 || theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh > 255) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh=0;
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh-=res;
	
	m_edits.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the threshold for tamper band surface front (camera1) search box.
void Modify::OnSdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh < 0 || theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh > 255) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh=0;
	//m_edits.Format("%3i",pframe->m_pxaxis->TB2Thresh);
	
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandType==5)	//If color compare
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh>=100)	//Threshold must be 100 or less.
			theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh=100;
	}
	m_edits.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the threshold for tamper band surface rear (cameras 2 and 3) search box.
void Modify::OnSup3() 
{
	//Threshold has to be between 0 and 255, since we are using 8 bits per pixel intensity.
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 < 0 || theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 > 255) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2=0;
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2-=res;
	
	m_edits2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the threshold for tamper band surface rear (cameras 2 and 3) search box.
void Modify::OnSdn3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2+=res;
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 < 0 || theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 > 255) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2=0;
	//m_edits.Format("%3i",pframe->m_pxaxis->TB2Thresh);
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandType==5)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2>=100) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2=100;
	}
	m_edits2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move low fill search box up.
void Modify::OnFup() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].fillY>=30)	//low fill search box must be equal to or more then 30 pixels from the top
		theapp->jobinfo[pframe->CurrentJobNum].fillY-=res;
	pframe->m_pxaxis->TurnOff23=true; //to disable Cam2 and Cam3's picture from being drawn
	pframe->m_pxaxis->SetTimer(1,5000,NULL);   //timer to Enable Cam2 and Cam3's pciture to be drawn

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move low fill search box down.
void Modify::OnFdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].fillY+=res;
	//Low fill search box has to be 16 or more pixels from the bottom of the image.
	if(theapp->jobinfo[pframe->CurrentJobNum].fillY>=(theapp->cam1Height/2)-16)
	{
		theapp->jobinfo[pframe->CurrentJobNum].fillY=(theapp->cam1Height/2)-16;
	}
	pframe->m_pxaxis->TurnOff23=true; //to disable Cam2 and Cam3's picture from being drawn
	pframe->m_pxaxis->SetTimer(1,5000,NULL);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Updates modify tab gui.
void Modify::UpdateDisplay()
{
	//update surface type sensitivity algorithm selected.
	switch(theapp->jobinfo[pframe->CurrentJobNum].tBandType)
	{
		
		case 0:	//Nothing selected
			m_choice.Format("%s","");
		break;
		case 1:	//Edge selected
			m_choice.Format("%s","edge");
		break;

		case 2:	//Threshold selected
			if(theapp->jobinfo[pframe->CurrentJobNum].dark==1)
			{
				m_choice.Format("%s","find dark");	//Dark threshold
			}
			else
			{
				m_choice.Format("%s","find light");	//Light threshold
				m_colorinst.ShowWindow(SW_HIDE);
			}
		break;

		case 3:
			
		break;
	
		case 4://Integrity selected
			m_choice.Format("%s","integrity");
		break;
	
		case 5://Color compare selected
			m_choice.Format("%s","color");
			m_colorinst.ShowWindow(SW_SHOW);
		break;

		
	}

	//Shaded average is not used.
	if (theapp->jobinfo[pframe->CurrentJobNum].tBandType==3) {m_tbcolor.ShowWindow(SW_SHOW);}else{m_tbcolor.ShowWindow(SW_HIDE);}
	
	//Update display boxes.	
	m_capmiddle.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].midBarY);
	m_capneck.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset);// changed
	m_captop.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].vertBarX);
	m_edits.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh);
	m_edits2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2);
	m_edite.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandThresh);
//	m_tboffsety.Format("%3i",pframe->m_pxaxis->TBOffset.y);
	m_sportcapedit.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);
	m_editcutoff.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].redCutOff);

	m_diff_var.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap);
	m_limit.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap);


	//Show or hide controls for sports cap depending on type of job configuration.
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
	{
		m_sportcapup.ShowWindow(SW_SHOW);
		m_sportcapdn.ShowWindow(SW_SHOW);
		m_sportcapmore.ShowWindow(SW_SHOW);
		m_sportcapless.ShowWindow(SW_SHOW);
		m_sportcapframe.ShowWindow(SW_SHOW);
		m_sportcapeditc.ShowWindow(SW_SHOW);
		m_sca.ShowWindow(SW_SHOW);
		m_scs.ShowWindow(SW_SHOW);
	}
	else
	{
		m_sportcapup.ShowWindow(SW_HIDE);
		m_sportcapdn.ShowWindow(SW_HIDE);
		m_sportcapmore.ShowWindow(SW_HIDE);
		m_sportcapless.ShowWindow(SW_HIDE);
		m_sportcapframe.ShowWindow(SW_HIDE);
		m_sportcapeditc.ShowWindow(SW_HIDE);
		m_sca.ShowWindow(SW_HIDE);
		m_scs.ShowWindow(SW_HIDE);
	}
	
	UpdateData(false);

	//Update reTeach color controls
	if(newTarget==true)
	{ 
		newTarget=false; 
		UpdateData(false);
		InvalidateRect(NULL,false);
	}
}

//NOT USED
void Modify::OnTbup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamY-=res;

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

//NOT USED
void Modify::OnTbdn2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamY+=res;

	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Modify::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1)  //Updates job with new parameters.
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}

	CDialog::OnTimer(nIDEvent);
}

//Move low fill search box left.
void Modify::OnFillleft() 
{

	theapp->jobinfo[pframe->CurrentJobNum].fillX-=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].fillX < 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=1;}
	else if(theapp->jobinfo[pframe->CurrentJobNum].fillX > 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=0;}

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Move low fill search box right.
void Modify::OnFillright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].fillX+=res;

	if(theapp->jobinfo[pframe->CurrentJobNum].fillX < 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=1;}
	else if(theapp->jobinfo[pframe->CurrentJobNum].fillX > 0) {theapp->jobinfo[pframe->CurrentJobNum].is_fillX_neg=0;}

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
	
}

//Increase the threshold for tamper band edge search boxes.
//Pixels in the tamper band search boxes with intensities above this threshold are considered a broken band.
void Modify::OnSup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandThresh-=1;
	m_edite.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandThresh);
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the threshold for tamper band edge search boxes.
void Modify::OnSdn2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandThresh+=1;
	m_edite.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandThresh);

	SetTimer(1,3000,NULL);	
	UpdateData(false);
	
	
}

//NOT USED
void Modify::OnFast() 
{
	if(toggle==true){res=10;toggle=false;CheckDlgButton(IDC_FAST,1); m_enable.Format("%s","Fast");}else{toggle=true;CheckDlgButton(IDC_FAST,0); m_enable.Format("%s","Fine");res=1;}	
	UpdateData(false);
}

//Re-teach cap color
void Modify::OnColor() 
{
	pframe->m_pxaxis->reteachColor=true;
	newTarget=true;
	UpdateDisplay();
}

void Modify::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	COLORREF scolor;
	
	CBrush Saved;
	
	scolor= pframe->m_pxaxis->ColorTarget;
	
	
	Saved.CreateSolidBrush(scolor);

	CRect resrect;
	resrect.top=110;
	resrect.left=480;//97
	resrect.bottom=resrect.top+20;
	resrect.right=resrect.left+40;

	dc.FillRect(resrect,&Saved);

}

//NOT USED
void Modify::OnTbcolor() 
{
	theapp->teachFrontBand=true;
	
}


//The sports cap is found by performing a horizontal scan through the search box.
//The left and right edges are then detected and the width of the cap is calculated.

//Move sports cap search box up.
void Modify::OnSportcapUp() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportPos-=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sportPos<=10)theapp->jobinfo[pframe->CurrentJobNum].sportPos=10;
}

//Move sports cap search box down.
void Modify::OnSportcapDn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportPos+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sportPos>=200)theapp->jobinfo[pframe->CurrentJobNum].sportPos=200;
	
}

//Increase sports cap search sensitivity, this is used to detect an edge due to intensity transition.
void Modify::OnSportcapMore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportSen-=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sportSen<=0)theapp->jobinfo[pframe->CurrentJobNum].sportSen=0;
	m_sportcapedit.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);

	UpdateData(false);
	
}

//Decrease sports cap search sensitivity, this is used to detect an edge due to intensity transition.
void Modify::OnSportcapLess() 
{
	theapp->jobinfo[pframe->CurrentJobNum].sportSen+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].sportSen>=100)theapp->jobinfo[pframe->CurrentJobNum].sportSen=100;
	m_sportcapedit.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].sportSen);

	UpdateData(false);
	
}

//Low fill filter works by converting an intensity image into a binary image.
//The filter value specifies the point of separation, any pixel with an intensity
//below the threshold will be black and any pixel with an intensity above will be white.


//Increase extra light filter threshold.
void Modify::OnFiltermore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].redCutOff+=1;
	m_editcutoff.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].redCutOff);
	
	UpdateData(false);
}

//Decrease extra light filter threshold.
void Modify::OnFilterless() 
{
	theapp->jobinfo[pframe->CurrentJobNum].redCutOff-=1;
	
	m_editcutoff.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].redCutOff);
	UpdateData(false);
	
}

//NOT USED
void Modify::OnViewGraph() 
{
	FillerGraph fillergraph;
	fillergraph.DoModal();		
}

//NOT USED
void Modify::OnCheckRearHeight() 
{
	if(	theapp->checkRearHeight == true)
	{
		theapp->checkRearHeight=false;
		CheckDlgButton(IDC_CHECK1,0);

	}

	else if(theapp->checkRearHeight==false)
	{
	
	theapp->checkRearHeight=true;
	CheckDlgButton(IDC_CHECK1,1);
	
	}

		UpdateData(false);
		UpdateDisplay();	
}

//Toggles between saving or not saving all pictures to disk. 
//This is all pictures not just pictures of bottles which failed an inspection.
//The the images from cameras 1, 2, and 3 are saved to the hard drive.
void Modify::OnSavePics() 
{
	if(theapp->savepics == true)
	{
		theapp->savepics =false;
		CheckDlgButton(IDC_CHECK2,0); 


	}
	else
	{
		theapp->savepics =true;

		CheckDlgButton(IDC_CHECK2,1); 

	}
		UpdateData(false);	
}

//These controls deal with a missing cap inspection for a silver bottle which is painted in the area where the cap is.
//Thus when there is a missing cap this is not detected through the normal algorithm, since the bottle itself has a 
//non transparent edge.
//
void Modify::OnIncrDiff() 
{
	theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap+=1;
	m_diff_var.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap);
	
			UpdateData(false);
	
}

void Modify::OnDcrDiff() 
{
	theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap -=1;
	m_diff_var.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap);
	UpdateData(false);
}

void Modify::OnIncrLimit() 
{
	
	theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap+=1;

	m_limit.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap);
	UpdateData(false);
}

void Modify::OnDcrlimit() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap >=1)
	{
	theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap-=1;
	}
	m_limit.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap);
	UpdateData(false);
	
}

//