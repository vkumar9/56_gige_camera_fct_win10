// Light.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Light.h"
#include "mainfrm.h"
#include "serial.h"
#include "camera.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Light dialog


Light::Light(CWnd* pParent /*=NULL*/)
	: CDialog(Light::IDD, pParent)
{
	//{{AFX_DATA_INIT(Light)
	m_lightval = _T("");
	m_lightval2 = _T("");
	m_lightval3 = _T("");
	//}}AFX_DATA_INIT
}


void Light::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Light)
	DDX_Control(pDX, IDC_SLIDER3, m_slide3);
	DDX_Control(pDX, IDC_SLIDER2, m_slide2);
	DDX_Control(pDX, IDC_SLIDER1, m_slide);
	DDX_Text(pDX, IDC_LIGHTVAL, m_lightval);
	DDX_Text(pDX, IDC_LIGHTVAL2, m_lightval2);
	DDX_Text(pDX, IDC_LIGHTVAL3, m_lightval3);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Light, CDialog)
	//{{AFX_MSG_MAP(Light)
	ON_BN_CLICKED(IDC_LIGHT, OnLight)
	ON_BN_CLICKED(IDC_DARK, OnDark)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_FRONT, OnFront)
	ON_BN_CLICKED(IDC_LEFT, OnLeft)
	ON_BN_CLICKED(IDC_RIGHT, OnRight)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER1, OnReleasedcaptureSlider1)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER2, OnReleasedcaptureSlider2)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER3, OnReleasedcaptureSlider3)
	ON_BN_CLICKED(IDC_LIGHT2, OnLight2)
	ON_BN_CLICKED(IDC_DARK2, OnDark2)
	ON_BN_CLICKED(IDC_LIGHT3, OnLight3)
	ON_BN_CLICKED(IDC_DARK3, OnDark3)
	ON_BN_CLICKED(IDC_RING, OnRing)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Light message handlers

//Initialize lights dlg 
BOOL Light::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*) AfxGetMainWnd();		//Get pointer to main window.
	pframe->m_plight=this;						//Set main window's pointer ref for this dlg.
	theapp=(CCapApp*)AfxGetApp();				//Get pointer to app object.

	//Set light lvls variables from saved job.
	LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
	LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
	LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
	
	m_lightval.Format("%3i",LightLevel1);		//Set light level variable for GUI display to light lvl of cam 1.

	//Initialize slider for camera1.
	m_slide.SetRange(0,100);
	m_slide.SetTicFreq(10);
	m_slide.SetPos(100-LightLevel1);

	//Initialize slider for camera2.
	m_slide2.SetRange(0,100);
	m_slide2.SetTicFreq(10);
	m_slide2.SetPos(100-LightLevel2);

	//Initialize slider for camera3.
	m_slide3.SetRange(0,100);
	m_slide3.SetTicFreq(10);
	m_slide3.SetPos(100-LightLevel3);

	//Default all camera gain's are adjusted at the same time.
	CheckDlgButton(IDC_ALL,1);
	CheckDlgButton(IDC_FRONT,0);
	CheckDlgButton(IDC_LEFT,0);
	CheckDlgButton(IDC_RIGHT,0);
	Cams=4;

	//If saved job says ring light is used.
	if(theapp->jobinfo[pframe->CurrentJobNum].useRing==false)
	{
		if(theapp->NoSerialComm==false)  //If plc is connected turn on.
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,260);
			pframe->m_pserial->SendChar(0,260);	//Turn on right light
		}
		CheckDlgButton(IDC_RING,1);	//Check box which says ring light is used.
	}
	else	//If ring light is not used.
	{
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,270);
			pframe->m_pserial->SendChar(0,270);
		}

		CheckDlgButton(IDC_RING,0);
	}

	m_lightval2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ringLight);

	UpdateData(false);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Increase camera gain.
void Light::OnLight() 
{
	//LightLevel+=1;
	
	

	switch(Cams)//Check for which camera(s) the gain is being adjusted.
	{
	case 4://If all three are selected.
		 pframe->m_pcamera->OnGmore();
		 pframe->m_pcamera->OnGmore2();
		 pframe->m_pcamera->OnGmore3();

		LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;	//Set gain lvl for this dlgBx's cam1 variable
		m_lightval.Format("%3i",LightLevel1);	//Set gain lvl for this dlgBx's cam1 variable shown in edit box.
		m_slide.SetPos(100-LightLevel1);	//Set gain lvl for this dlgBx's cam1 slider 

		LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
		m_slide2.SetPos(100-LightLevel2);

		LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
		m_slide3.SetPos(100-LightLevel3);

		break;
	case 1://If camera1 is selected.
		pframe->m_pcamera->OnGmore();
		LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
		m_lightval.Format("%3i",LightLevel1);
		m_slide.SetPos(100-LightLevel1);

		break;
	case 2://If camera2 is selected.
		pframe->m_pcamera->OnGmore2();
		LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
		m_lightval.Format("%3i",LightLevel2);
		m_slide2.SetPos(100-LightLevel2);

		break;
	case 3://If camera3 is selected.
		 pframe->m_pcamera->OnGmore3();
		LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
		m_lightval.Format("%3i",LightLevel3);
		m_slide3.SetPos(100-LightLevel3);
		break;
	}

	

	pframe->SendMessage(WM_TRIGGER,NULL,NULL);	//Trigger camera.

	UpdateData(false);
	
}

//Decrease camera gain.
void Light::OnDark() 
{
	LightLevel1-=1;

	
	switch(Cams)
	{
	case 4:
		 pframe->m_pcamera->OnGless();
		 pframe->m_pcamera->OnGless2();
		 pframe->m_pcamera->OnGless3();
		LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
		m_lightval.Format("%3i",LightLevel1);
		m_slide.SetPos(100-LightLevel1);

		LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
		m_slide2.SetPos(100-LightLevel2);

		LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
		m_slide3.SetPos(100-LightLevel3);

		break;
	case 1:
		pframe->m_pcamera->OnGless();
		LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
		m_lightval.Format("%3i",LightLevel1);
		m_slide.SetPos(100-LightLevel1);
		break;
	case 2:
		pframe->m_pcamera->OnGless2();
		LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
		m_lightval.Format("%3i",LightLevel2);
		m_slide2.SetPos(100-LightLevel2);
		break;
	case 3:
		 pframe->m_pcamera->OnGless3();
		LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
		m_lightval.Format("%3i",LightLevel3);
		m_slide3.SetPos(100-LightLevel3);

		break;
	}

	


	pframe->SendMessage(WM_TRIGGER,NULL,NULL);

	UpdateData(false);
	
}

//Close Dlg
void Light::OnOK() 
{
	
	pframe->SaveJob(pframe->CurrentJobNum);		//Save light lvls for current job.
	pframe->SetTimer(43,100,NULL); //To Refresh the pic in XAxisView
	CDialog::OnOK();
}

//Cancel out of Dlg.
void Light::OnCancel() 
{
	
	
	CDialog::OnCancel();
}

//NOT USED
void Light::UpdatePLC()
{

	int Value=LightLevel1;
	int Function=0;
	int CntrBits=0;
	int ControlChars;
		
	Value=Value*1000;
	Function=1;
	Function=Function *10;
	CntrBits=0;

	ControlChars=Value+Function+CntrBits;
	//pframe->m_pserial->QueMessage(Value,Function);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
}

//Gain adjustments will be for all cameras.
void Light::OnAll() 
{
	Cams=4;
	
}

//Gain will be adjusted for front camera.
void Light::OnFront() 
{
	Cams=1;
	
}

//Gain will be adjusted for camera 2.
void Light::OnLeft() 
{
	Cams=2;
	
}

//Gain will be adjusted for camera 3.
void Light::OnRight() 
{
	Cams=3;
	
}

//Handler for camera1 slider release
void Light::OnReleasedcaptureSlider1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100-double(m_slide.GetPos());		//Get thumb position.

	//Calculate difference between new and old thumb position.
	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4-res;

	if(dif<0) //If thumb moved up
	{
		//float absolute value
		for(double dist=fabs(dif); dist>0; dist-=1)	//increment gain several times.
		{
			pframe->m_pcamera->OnGmore();
		}
		
	}
	else//If thumb moved down
	{
		for(double dist=fabs(dif); dist>0; dist-=1)//decrement gain several times.
		{
			pframe->m_pcamera->OnGless();
		}
	}

	//Load camera1 light lvl
	LightLevel1=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1)*4;
	m_lightval.Format("%3i",LightLevel1);	//update variable which displays light lvl on GUI
		//m_slide.SetPos(100-LightLevel1);

	pframe->SendMessage(WM_TRIGGER,NULL,NULL);		//Trigger camera.
	UpdateData(false);		//Update GUI
	*pResult = 0;
}

//Handler for camera2 slider release
void Light::OnReleasedcaptureSlider2(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100-double(m_slide2.GetPos());

	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{
			pframe->m_pcamera->OnGmore2();
		}
		
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{
			pframe->m_pcamera->OnGless2();
		}
	}

	LightLevel2=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel2)*4;
	m_lightval.Format("%3i",LightLevel2);
		//m_slide.SetPos(100-LightLevel1);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	
	*pResult = 0;
}

//Handler for camera3 slider release
void Light::OnReleasedcaptureSlider3(NMHDR* pNMHDR, LRESULT* pResult) 
{
	double res=100-double(m_slide3.GetPos());

	double dif=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4-res;

	if(dif<0) 
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{
			pframe->m_pcamera->OnGmore3();
		}
		
	}
	else
	{
		for(double dist=fabs(dif); dist>0; dist-=1)
		{
			pframe->m_pcamera->OnGless3();
		}
	}

	LightLevel3=(theapp->jobinfo[pframe->CurrentJobNum].lightLevel3)*4;
	m_lightval.Format("%3i",LightLevel3);
		//m_slide.SetPos(100-LightLevel1);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	UpdateData(false);
	
	*pResult = 0;
}

//Make ring light intensity lighter by increasing duration.
void Light::OnLight2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ringLight+=1;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->jobinfo[pframe->CurrentJobNum].ringLight,370);
		pframe->m_pserial->SendChar(theapp->jobinfo[pframe->CurrentJobNum].ringLight,370);
	}
	m_lightval2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ringLight);

	pframe->SaveJob(pframe->CurrentJobNum);

	UpdateData(false);
}

//Make ring light intensity darker by decreasing duration.
void Light::OnDark2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].ringLight-=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].ringLight <0 ) {theapp->jobinfo[pframe->CurrentJobNum].ringLight =0;}

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->jobinfo[pframe->CurrentJobNum].ringLight,370);
		pframe->m_pserial->SendChar(theapp->jobinfo[pframe->CurrentJobNum].ringLight,370);
	}
	m_lightval2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].ringLight);

	pframe->SaveJob(pframe->CurrentJobNum);

	UpdateData(false);
	
}


//NOT USED
void Light::OnLight3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].backLight+=1;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->jobinfo[pframe->CurrentJobNum].backLight,380);
		pframe->m_pserial->SendChar(theapp->jobinfo[pframe->CurrentJobNum].backLight,380);
	}

	m_lightval3.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].backLight);

	UpdateData(false);
	
}

//NOT USED
void Light::OnDark3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].backLight-=1;
//	pframe->m_pserial->QueMessage(theapp->jobinfo[pframe->CurrentJobNum].backLight,380);
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->jobinfo[pframe->CurrentJobNum].backLight,380);
		pframe->m_pserial->SendChar(theapp->jobinfo[pframe->CurrentJobNum].backLight,380);
	}
	m_lightval3.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].backLight);

	UpdateData(false);
	
}

//Toggles ring light on or off.
void Light::OnRing() 
{
	//turn on
	if(theapp->jobinfo[pframe->CurrentJobNum].useRing==false)
	{
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,270);
			pframe->m_pserial->SendChar(0,270);				//Tell plc to turn ring light on.
		}

		theapp->jobinfo[pframe->CurrentJobNum].useRing=true;	//Save info to job.
		CheckDlgButton(IDC_RING,0);							//Update GUI checkbox.
	}
	else//turn off
	{
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,260);
			pframe->m_pserial->SendChar(0,260);
		}

		theapp->jobinfo[pframe->CurrentJobNum].useRing=false;
		CheckDlgButton(IDC_RING,1);
	}
	UpdateData(false);	
}
