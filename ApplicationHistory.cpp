// ApplicationHistory.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "ApplicationHistory.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ApplicationHistory dialog


ApplicationHistory::ApplicationHistory(CWnd* pParent /*=NULL*/)
	: CDialog(ApplicationHistory::IDD, pParent)
{
	//{{AFX_DATA_INIT(ApplicationHistory)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void ApplicationHistory::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ApplicationHistory)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ApplicationHistory, CDialog)
	//{{AFX_MSG_MAP(ApplicationHistory)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ApplicationHistory message handlers

void ApplicationHistory::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
