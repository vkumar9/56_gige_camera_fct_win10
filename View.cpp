// View.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "View.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// View dialog


View::View(CWnd* pParent /*=NULL*/)
	: CDialog(View::IDD, pParent)
{
	//{{AFX_DATA_INIT(View)
	m_id1 = _T("");
	m_id2 = _T("");
	m_id3 = _T("");
	m_id4 = _T("");
	m_id5 = _T("");
	m_id6 = _T("");
	m_id7 = _T("");
	m_id8 = _T("");
	m_id9 = _T("");
	m_id10 = _T("");
	m_id11 = _T("");
	m_id12 = _T("");
	m_id13 = _T("");
	m_id14 = _T("");
	m_t3 = _T("");
	m_t4 = _T("");
	m_t5 = _T("");
	m_t6 = _T("");
	m_t7 = _T("");
	m_t8 = _T("");
	m_t9 = _T("");
	m_t10 = _T("");
	m_sd1 = _T("");
	m_sd2 = _T("");
	m_sd3 = _T("");
	m_sd4 = _T("");
	m_sd5 = _T("");
	m_sd6 = _T("");
	m_sd7 = _T("");
	m_sd8 = _T("");
	m_sport = _T("");
	m_cappernumber = _T("");
	m_fillernumber = _T("");
	m_cappernumber2 = _T("");
	m_fillernumber2 = _T("");
	m_capwidth = _T("");
	m_imgRead1 = _T("");
	m_imgRead2 = _T("");
	m_imgRead3 = _T("");
	m_finished1 = _T("");
	m_finished2 = _T("");
	m_finished3 = _T("");
	m_FoilSensor = _T("");
	//}}AFX_DATA_INIT
}


void View::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(View)
	DDX_Control(pDX, IDC_FILLERNUMBER2, m_a9);
	DDX_Control(pDX, IDC_FILLERNUMBER, m_a8);
	DDX_Control(pDX, IDC_CAPPERNUMBER2, m_a6);
	DDX_Control(pDX, IDC_CAPPERNUMBER, m_a5);
	DDX_Control(pDX, IDC_A4, m_a4);
	DDX_Control(pDX, IDC_A3, m_a3);
	DDX_Control(pDX, IDC_A2, m_a2);
	DDX_Control(pDX, IDC_A1, m_a1);
	DDX_Control(pDX, IDC_TBSNAME, m_tbsname);
	DDX_Text(pDX, IDC_1, m_id1);
	DDX_Text(pDX, IDC_2, m_id2);
	DDX_Text(pDX, IDC_3, m_id3);
	DDX_Text(pDX, IDC_4, m_id4);
	DDX_Text(pDX, IDC_5, m_id5);
	DDX_Text(pDX, IDC_6, m_id6);
	DDX_Text(pDX, IDC_7, m_id7);
	DDX_Text(pDX, IDC_8, m_id8);
	DDX_Text(pDX, IDC_9, m_id9);
	DDX_Text(pDX, IDC_10, m_id10);
	DDX_Text(pDX, IDC_11, m_id11);
	DDX_Text(pDX, IDC_12, m_id12);
	DDX_Text(pDX, IDC_13, m_id13);
	DDX_Text(pDX, IDC_14, m_id14);
	DDX_Text(pDX, IDC_T3, m_t3);
	DDX_Text(pDX, IDC_T4, m_t4);
	DDX_Text(pDX, IDC_T5, m_t5);
	DDX_Text(pDX, IDC_T6, m_t6);
	DDX_Text(pDX, IDC_T7, m_t7);
	DDX_Text(pDX, IDC_T8, m_t8);
	DDX_Text(pDX, IDC_T9, m_t9);
	DDX_Text(pDX, IDC_T10, m_t10);
	DDX_Text(pDX, IDC_SD1, m_sd1);
	DDX_Text(pDX, IDC_SD2, m_sd2);
	DDX_Text(pDX, IDC_SD3, m_sd3);
	DDX_Text(pDX, IDC_SD4, m_sd4);
	DDX_Text(pDX, IDC_SD5, m_sd5);
	DDX_Text(pDX, IDC_SD6, m_sd6);
	DDX_Text(pDX, IDC_SD7, m_sd7);
	DDX_Text(pDX, IDC_SD8, m_sd8);
	DDX_Text(pDX, IDC_SPORT, m_sport);
	DDX_Text(pDX, IDC_CAPPERNUMBER, m_cappernumber);
	DDX_Text(pDX, IDC_FILLERNUMBER, m_fillernumber);
	DDX_Text(pDX, IDC_CAPPERNUMBER2, m_cappernumber2);
	DDX_Text(pDX, IDC_FILLERNUMBER2, m_fillernumber2);
	DDX_Text(pDX, IDC_CAPWIDTH, m_capwidth);
	DDX_Text(pDX, IDC_READY1, m_imgRead1);
	DDX_Text(pDX, IDC_READY2, m_imgRead2);
	DDX_Text(pDX, IDC_READY3, m_imgRead3);
	DDX_Text(pDX, IDC_STATUS1, m_finished1);
	DDX_Text(pDX, IDC_STATUS2, m_finished2);
	DDX_Text(pDX, IDC_STATUS3, m_finished3);
	DDX_Text(pDX, IDC_FoilSensor, m_FoilSensor);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(View, CDialog)
	//{{AFX_MSG_MAP(View)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// View message handlers

BOOL View::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pview2=this;
	theapp=(CCapApp*)AfxGetApp();

	//Hide or show statistics on filler and capper.
	if(theapp->showFC==false)
	{
		m_a1.ShowWindow(SW_HIDE);
		m_a2.ShowWindow(SW_HIDE);
		m_a3.ShowWindow(SW_HIDE);
		m_a4.ShowWindow(SW_HIDE);
		m_a5.ShowWindow(SW_HIDE);
		m_a6.ShowWindow(SW_HIDE);
		m_a8.ShowWindow(SW_HIDE);
		m_a9.ShowWindow(SW_HIDE);

	}
	else
	{
		m_a1.ShowWindow(SW_SHOW);
		m_a2.ShowWindow(SW_SHOW);
		m_a3.ShowWindow(SW_SHOW);
		m_a4.ShowWindow(SW_SHOW);
		m_a5.ShowWindow(SW_SHOW);
		m_a6.ShowWindow(SW_SHOW);
		m_a8.ShowWindow(SW_SHOW);
		m_a9.ShowWindow(SW_SHOW);
	}
	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//Update displayed statistics
void View::UpdateDisplay()
{
	
	if(pframe->Total<=0 )	//If no bottles have been scanned set all statistics to 0.
	{
		m_id1.Format("%3.2f",0);
		m_id2.Format("%3.2f",0);
 		m_id3.Format(" %3i",0);
		m_id4.Format(" %3i",0);
		m_id5.Format(" %3i",0);
		m_id6.Format(" %3i",0);
		m_id7.Format(" %3i",0);
		m_id8.Format(" %3i",0);
		m_id9.Format(" %3i",0);
		m_id10.Format(" %3i",0);
		m_id11.Format(" %3i",0);
		m_id12.Format(" %3i",0);
		m_id13.Format(" %3i",0);
		m_id14.Format(" %3i",0);
		
 		m_t3.Format(" %3i",0);
		m_t4.Format(" %3i",0);
		m_t5.Format(" %3i",0);
		m_t6.Format(" %3i",0);
		m_t7.Format(" %3i",0);
		m_t8.Format(" %3i",0);
		m_t9.Format(" %3i",0);
		m_t10.Format(" %3i",0);

		m_sd1.Format("%3.2f",0);
		m_sd2.Format("%3.2f",0);
 		m_sd3.Format("%3.2f",0);
		m_sd4.Format("%3.2f",0);
		m_sd5.Format("%3.2f",0);
		m_sd6.Format("%3.2f",0);
		m_sd7.Format("%3.2f",0);
		m_sd8.Format("%3.2f",0);
		m_fillernumber.Format(" %2i",0);
		m_cappernumber.Format(" %2i",0);

		m_fillernumber2.Format(" %2i",0);
		m_cappernumber2.Format(" %2i",0);
	}
	else
	{
		m_id1.Format("%3.2f",pframe->BottleRate);
		m_id2.Format("%3.2f",pframe->EjectRate);
	
		m_id3.Format(" %3i",pframe->m_pxaxis->E1Cnt);
		m_id4.Format(" %3i",pframe->m_pxaxis->E2Cnt);
		m_id5.Format(" %3i",pframe->m_pxaxis->E3Cnt);
		if(theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor==0)
		{m_id6.Format(" %3i",pframe->m_pxaxis->E4Cnt);}
		else
		{
		m_id6.Format(" %3i",pframe->m_pxaxis->Missing_Foil_Count);

		}
		m_id7.Format(" %3i",pframe->m_pxaxis->E5Cnt);
		m_id8.Format(" %3i",pframe->m_pxaxis->E6Cnt);
		m_id9.Format(" %3i",pframe->m_pxaxis->E7Cnt);
		m_id10.Format(" %3i",pframe->m_pxaxis->E8Cnt);
		m_id11.Format(" %3i",pframe->m_pxaxis->NoCapCnt);
		m_id12.Format(" %3i",pframe->m_pxaxis->CockedCnt);
		m_id13.Format(" %3i",pframe->m_pxaxis->LooseCnt);
		m_id14.Format(" %3i",pframe->m_pxaxis->E9Cnt);

		m_t3.Format("%3.2f",pframe->m_pxaxis->LHt);
		m_t4.Format("%3.2f",pframe->m_pxaxis->RHt);
		m_t5.Format("%3.2f",pframe->m_pxaxis->MHt);
		m_t6.Format("%3.2f",pframe->m_pxaxis->Dt);
		m_t7.Format("%3.2f",pframe->m_pxaxis->TBt);
		m_t8.Format("%3.2f",pframe->m_pxaxis->TB2t);
		m_t9.Format("%3.2f",pframe->m_pxaxis->Ft);
		m_t10.Format("%3.2f",pframe->m_pxaxis->Ut);

		m_sd1.Format("%3.2f",pframe->m_pxaxis->LHsd);
		m_sd2.Format("%3.2f",pframe->m_pxaxis->LHsd);
 		m_sd3.Format("%3.2f",pframe->m_pxaxis->LHsd);
		m_sd4.Format("%3.2f",pframe->m_pxaxis->Dsd);
		m_sd5.Format("%3.2f",pframe->m_pxaxis->TBsd);
		m_sd6.Format("%3.2f",pframe->m_pxaxis->TB2sd);
		m_sd7.Format("%3.2f",pframe->m_pxaxis->Fsd);
		m_sd8.Format("%3.2f",pframe->m_pxaxis->Usd);
		m_fillernumber.Format(" %2i",theapp->fillerCount);
		m_cappernumber.Format(" %2i",theapp->capperCount);

		m_fillernumber2.Format(" %2i",theapp->fillerCount2);
		m_cappernumber2.Format(" %2i",theapp->capperCount2);
		m_capwidth.Format(" %3i",pframe->m_pxaxis->currentCapWidth);
	}

		//Set label text for normal cap vs sports cap vs foil.
		if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)
		{
			m_sport.Format("%s","Height Middle");
		}
		else if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
		{
			m_sport.Format("%s","Sport Band");
		}
		else if(theapp->jobinfo[pframe->CurrentJobNum].sport==2)
		{
			m_sport.Format("%s","Foil");
		}


		//For the foil sensor
		if(theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor==1)
		{
			m_FoilSensor.Format("%s","Missing Foil");
		}


	m_imgRead1.Format("%i", pframe->countC1.load());
	m_imgRead2.Format("%i", pframe->countC2.load());
	m_imgRead3.Format("%i", pframe->countC3.load());


	if(pframe->countC0>0)
	{
		m_finished1.Format("%i", pframe->m_pxaxis->cameraDone1[pframe->countC0.load()]);
		m_finished2.Format("%i", pframe->m_pxaxis->cameraDone2[pframe->countC0.load()]);
		m_finished3.Format("%i", pframe->m_pxaxis->cameraDone3[pframe->countC0.load()]);
	}


	UpdateData(false);
	InvalidateRect(NULL,false);

}

void View::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	//DOES NOT APPEAR TO DO ANYTHING?
	COLORREF bluecolor = RGB(0,0,150);
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
	dc.SetBkMode(TRANSPARENT);
	dc.SelectObject(&GreenPen);

}
