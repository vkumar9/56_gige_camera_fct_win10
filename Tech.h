#if !defined(AFX_TECH_H__E7CD203B_496C_4D45_80F3_91D68E1C964A__INCLUDED_)
#define AFX_TECH_H__E7CD203B_496C_4D45_80F3_91D68E1C964A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Tech.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Tech dialog

class Tech : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;
public:
	void UpdateDisplay();
	Tech(CWnd* pParent = NULL);   // standard constructor

	CMainFrame* pframe;
	CCapApp* theapp;
	
	//CString m_forcelights;
	//CString m_nolip;
	long Value;			//Not used
	int Function;		//Not used
	int CntrBits;		//Not used
// Dialog Data
	//{{AFX_DATA(Tech)
	enum { IDD = IDD_HIDDEN };
	CButton	m_rtb2right;
	CButton	m_rtb2left;
	CButton	m_ltb2right;
	CButton	m_ltb2left;
	CButton	m_tblarger;
	CButton	m_tbllonger;
	CButton	m_tbsmaller;
	CButton	m_tblshorter;
	CString	m_fsizeh;		//Low fill search box height.
	CString	m_fsizew;		//Low fill search box width.
	CString	m_edittb;		//Left tamperband height display variable.
	CString	m_editlswin;	//camera2 cap edge search box x coordinate. 
	CString	m_editrswin;	//camera3 cap edge search box x coordinate. 
	CString	m_editwidth;	//Used for detecting missing cap.
	CString	m_editol;		//Low fill extra light search area.
	CString	m_edityoff;		//Rear cameras tamperband break search boxes vertical offset
	CString	m_target_ht_bar;		//Camera1 top search limit bar
	CString	m_target_height_bar2;	//Camera2 top search limit bar
	CString	m_target_ht_bar3;		//Camera3 top search limit bar
	CString	m_diff_var;				//Not used
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Tech)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Tech)
	virtual BOOL OnInitDialog();
	afx_msg void OnWlarger();
	afx_msg void OnWsmaller();
	afx_msg void OnHlarger();
	afx_msg void OnHsmaller();
	afx_msg void OnTblarger();
	afx_msg void OnTbsmaller();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio5();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLtb2left();
	afx_msg void OnLtb2right();
	afx_msg void OnRtb2right();
	afx_msg void OnRtb2left();
	virtual void OnOK();
	afx_msg void OnCheck1();
	afx_msg void OnCheck2();
	afx_msg void OnCheck3();
	afx_msg void OnWidthmore();
	afx_msg void OnWidthless();
	afx_msg void OnCheck4();
	afx_msg void OnEnablewidth();
	afx_msg void OnCheck5();
	afx_msg void OnOlup();
	afx_msg void OnOldn();
	afx_msg void OnYoffup();
	afx_msg void OnYoffdn();
	afx_msg void OnButtonup();
	afx_msg void OnButtondn();
	afx_msg void OnButtonup2();
	afx_msg void OnButtonDown2();
	afx_msg void OnButtonup3();
	afx_msg void OnButtonDown3();
	afx_msg void OnCheck6();
	afx_msg void OnIncrDiff();
	afx_msg void OnDcrDiff();
	afx_msg void OnCheck7();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TECH_H__E7CD203B_496C_4D45_80F3_91D68E1C964A__INCLUDED_)
