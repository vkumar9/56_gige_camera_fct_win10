// SelectingFiller.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "SelectingFiller.h"
#include "FillerGraph.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SelectingFiller dialog

SelectingFiller::SelectingFiller(CWnd* pParent /*=NULL*/)
	: CDialog(SelectingFiller::IDD, pParent)
{
	//{{AFX_DATA_INIT(SelectingFiller)
	m_edit1 = _T("");
	//}}AFX_DATA_INIT
}


void SelectingFiller::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SelectingFiller)
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SelectingFiller, CDialog)
	//{{AFX_MSG_MAP(SelectingFiller)
	ON_BN_CLICKED(IDC_BUTTON1, OnIncreby1)
	ON_BN_CLICKED(IDC_BUTTON2, OnDecrby1)
	ON_BN_CLICKED(IDC_BUTTON3, OnIncreby5)
	ON_BN_CLICKED(IDC_BUTTON4, OnDecrby5)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SelectingFiller message handlers

void SelectingFiller::OnOK() 
{

	
	CDialog::OnOK();
	pframe->m_pfillergraph->InvalidateRect(false);
}

void SelectingFiller::OnIncreby1() 
{

	if(!(pframe->m_pfillergraph->filler_valve_selection >120 ))
	{
		pframe->m_pfillergraph->filler_valve_selection++;
	}
	else{pframe->m_pfillergraph->filler_valve_selection=120;}

//	UpdateData(false);
UpdateDisplay();

}

void SelectingFiller::OnDecrby1() 
{
		if(!(pframe->m_pfillergraph->filler_valve_selection < 0 ))
	{
		pframe->m_pfillergraph->filler_valve_selection--;
	}
	else{pframe->m_pfillergraph->filler_valve_selection=0;}

//	UpdateData(false);
UpdateDisplay();


	
}

void SelectingFiller::OnIncreby5() 
{
		if(!(pframe->m_pfillergraph->filler_valve_selection >115 ))
	{
		pframe->m_pfillergraph->filler_valve_selection+=5;
	}
	else{pframe->m_pfillergraph->filler_valve_selection=120;}

//	UpdateData(false);
UpdateDisplay();
	
}

void SelectingFiller::OnDecrby5() 
{
		if(!(pframe->m_pfillergraph->filler_valve_selection < 5 ))
	{
		pframe->m_pfillergraph->filler_valve_selection-=5;
	}
	else{pframe->m_pfillergraph->filler_valve_selection=0;}

//UpdateData(false);
UpdateDisplay();

	
}

BOOL SelectingFiller::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pselectingfiller=this;
	theapp=(CCapApp*)AfxGetApp();


	pframe->m_pfillergraph->filler_valve_selection=0;
//	UpdateData(false);
	UpdateDisplay();
	

	return TRUE;  // 
}

void SelectingFiller::UpdateDisplay()
{
	
	m_edit1.Format("%i",pframe->m_pfillergraph->filler_valve_selection);
	UpdateData(false);
	
}

void SelectingFiller::OnDestroy() 
{
	CDialog::OnDestroy();
	
pframe->m_pfillergraph->InvalidateRect(false);
}
