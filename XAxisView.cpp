// XAxisView.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "CapDoc.h"
#include "Camera.h"
#include <vector>
#include <algorithm>

#include "XAxisView.h"
#include "capview.h"
#include <windowsx.h>
#include <math.h>
#include "mainfrm.h"
#include "modify.h"
#include "PicRecal.h"
#include "Prompt3.h"
#include "serial.h"

#include <stdlib.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



using namespace std;


/////////////////////////////////////////////////////////////////////////////
// CXAxisView
char* pFileName = "c:\\SilganData\\capdata.bmp";//drive




IMPLEMENT_DYNCREATE(CXAxisView, CView)

CXAxisView::CXAxisView()
{
	for(int i=0; i<EXPECTED_CAMS; i++)
		m_imageProcessed[i] = NULL;
}

CXAxisView::~CXAxisView()
{
	GlobalFree(imgBuf0);
	GlobalFree(im_src0);
//	GlobalFree(im_src);
	GlobalFree(imgBuf1);
	for(int i=0; i<EXPECTED_CAMS; i++)
		free(m_imageProcessed[i]);
}

void CXAxisView::AllocateAndCopyImage(int index)
{
	if(m_imageProcessed[index] == NULL)
	{
		imageSize[index] = pframe->m_pcamera->m_imageProcessed[index][pframe->m_pcamera->readNext[index]]->GetDataSize()*sizeof(BYTE);
		m_imageProcessed[index] = (BYTE*)malloc(imageSize[index]);
	}

	memcpy(m_imageProcessed[index], pframe->m_pcamera->m_imageProcessed[index][pframe->m_pcamera->readNext[index]]->GetData(), imageSize[index]);	//Get color image data for camera 1 from camera dialog box.
}


BEGIN_MESSAGE_MAP(CXAxisView, CView)
	//{{AFX_MSG_MAP(CXAxisView)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// XAxisView drawing


/////////////////////////////////////////////////////////////////////////////
// CXAxisView diagnostics

#ifdef _DEBUG
void CXAxisView::AssertValid() const
{
	CView::AssertValid();
}

void CXAxisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// XAxisView message handlers
void CXAxisView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pxaxis=this;
	theapp=(CCapApp*)AfxGetApp();

	cam1Total=cam2Total=cam3Total=0;
	clearLipStats(0);



	TBl=theapp->SavedTBl;
	RejectAll=false;
	KillSave=theapp->SavedKillSave;
	nScreenWidth  = 1024;//610;//::GetSystemMetrics( SM_CXSCREEN );
    nScreenHeight = 768;//380;//::GetSystemMetrics( SM_CYSCREEN );

	lockQuery=false;
	gotColor=false;	
	U2Thresh2=0;
	U2Thresh=0;
	U2Height=20;
	U2Offset=0;
	Skip=false;
	Skip2=false;
	Skip3=false;
	Skip4=false;
	Skip5=false;

	pic_number=1;
	count_pic=1;
	freeze_current_pic=false;
	
	theapp->inspctn[1].lmax=theapp->inspctn[1].lmin=theapp->inspctn[2].lmax=theapp->inspctn[2].lmin=theapp->inspctn[3].lmax=theapp->inspctn[3].lmin=theapp->inspctn[4].lmax=theapp->inspctn[4].lmin=theapp->inspctn[5].lmax=theapp->inspctn[5].lmin=theapp->inspctn[8].lmax=theapp->inspctn[8].lmin=theapp->inspctn[6].lmax=theapp->inspctn[6].lmin=theapp->inspctn[7].lmax=theapp->inspctn[7].lmin=0;
	badcount=0;
	KillSD=theapp->SavedKillSD;
	
	sam.x=512;
	sam.y=520;
	saml.x=320;
	saml.y=180;
	samr.x=320;
	samr.y=180;
	TurnOff23=false;
	ShowBars=false;	
	CapThresh=200;
	StartLockOut=true;
	Result1=Result2=Result3=Result4=Result5=Result6=Result7=Result8=Result9=Result10=0;
	pframe->InspOpen=false;
	Modify=false;
	piccount=0;
	MidBar.x=0;
	MidBar.y=110;
	MidTop.x=320;
	NeckBar.x=0;
	NeckBar.y=200;
	TBand2.x=TBand2.y=theapp->jobinfo[pframe->CurrentJobNum].tBandX=theapp->jobinfo[pframe->CurrentJobNum].tBandY=theapp->jobinfo[pframe->CurrentJobNum].tBandDx=MTBand.x=MTBand.y=RTBand.x=RTBand.y=RTBand2.x=RTBand2.y=0;
	RightBand.x=RightBand.y=LeftBand.x=LeftBand.y=FrontBand.y=FrontBand.x=0;
	MTBLength=0;
	capPointCount=0;
	tot_bottle_cnt=0;

	load_1_pic=true;

	LHsd=Fsd=TBsd=TB2sd=Dsd=Usd=0;

	Fill.x=0;
	Fill.y=250;
	Fill.x1=theapp->SavedFillW;
	Fill.y1=theapp->SavedFillH;

	theapp->jobinfo[pframe->CurrentJobNum].tBandType=2;
	Black=theapp->SavedBlack=0;

	MidCap=200;
	RTopBar.x=450;
	no_cap_detected =false;
	
	newMidCap=60;
	LearnDone=false;
	HeightDiff=1;
	NeckOffsetX=0;
	XStart=50;
	XEnd=975;
	cam1y=0;
	cam1dy=420;
	TimesThru=0;
	ReloadOnce=false;
	LipOffset.x=LipOffset.y=0;
	LHCnt=0;
	RHCnt=0;
	MHCnt=0;
	UCnt=0;
	U2Cnt=0;
	FCnt=0;
	DCnt=0;
	TBCnt=0;
	TB2Cnt=0;
	CockedCnt=0;
	LooseCnt=0;
	NoCapCnt=0;
	Band2Size=300;
	LHt=0;
	RHt=0;
	MHt=0;
	Ut=0;
	Ft=0;
	Dt=0;
	TBt=0;
	TB2t=0;

	TB2Rect.x=TB2Rect.y=0;
	TB2Offset.x=0;
	TBOffset.x=0;
	//TB2Thresh=theapp->SavedTB2Thresh;
	b=400;
	LHInc=0;
	RHInc=0;
	MHInc=0;
	DInc=0;
	TBInc=0;
	TB2Inc=0;
	FInc=0;
	UInc=0;
	E1Cntx=E2Cntx=E3Cntx=E4Cntx=E5Cntx=E6Cntx=E7Cntx=E8Cntx=E9Cntx=E10Cntx=0;
	E1CntNet=E2CntNet=E3CntNet=E4CntNet=E7CntNet=0;
	GotOneBad=false;
//	if(theapp->Single==true) { }else{BlackOK=true;}
	Once=false;
	GotImage1=false;
	GotImage2=false;
	GotImage3=false;

	std::fill_n(inspn_failed,20,0); //initialise the array with 0's
	count_inspn_failed=1;//ini with 1
	foil_missing=false;// Variable holding result of Foil Detection

	im_data		=(BYTE*)GlobalAlloc(GMEM_FIXED |GMEM_ZEROINIT , 1024 * 768 * 4);
	im_datax	=(BYTE*)GlobalAlloc(GMEM_FIXED |GMEM_ZEROINIT, 640 * 480 );
	im_data2	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 1024 * 768 );
	im_data3	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 1024 * 768 );
	im_result	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 640 * 480  );
	im_resultc	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 640 * 480 * 4  );
	im_pat		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 1024 * 768  );
	im_patl		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 70 * 50 *4 );
	im_patr		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 70 * 50 *4 );
	im_match	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 60 * 50 );
	im_matchl	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 60 * 50 );
	im_matchr	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 60 * 50 );
	im_match2	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 60 * 50 );
	imgBuf0		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 * 4 );
	im_reduceX 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,512 * 384 * 4  );
	im_reduce 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768  );
	im_reduce1 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 * 2 );
	im_reduce2 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 * 4 );
	im_reduce3 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 * 4 );
	im_bw 		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	im_bw2 		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	im_bw3 		=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	im_cam1 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	im_cam2 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	im_cam3 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	im_cam1Color=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768*4);
	im_cam2Color=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 250*4);
	im_cam3Color=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 250*4);
	im_flip1 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768*4);
	im_flip2 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 250*4);
	im_flip3 	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 250*4);

	im_dataC	=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, 1024 * 768 * 4);

	//		memset(im_matchl,0x0, 60 * 50);//initialise with all 0



	///// for the picture Log
	imgBuf3RGB_cam2=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,(theapp->cam2Width/2)*(theapp->cam2Height/2) * 4 * 500 );
	imgBuf3RGB_cam1=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,(theapp->cam1Width/2)*(theapp->cam1Height/2) *4 * 500 );
	
	imgBuf3RGB_cam3=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,(theapp->cam3Width/2)*(theapp->cam3Height/2) * 4 * 500 );

	arr1[0]=imgBuf3RGB_cam1;
	arr1[1]=imgBuf3RGB_cam2;
	arr1[2]=imgBuf3RGB_cam3;
	//arr1[3]=imgBuf3RGB_cam4;

	typofault=0;showostore=0;imge_no=0;

	for(int i1=0;i1<500;i1++)
	{
		for(int j=0;j<=4;j++)
		{
			tim_stamp[i1][j]=0;
		}
	}
	
		//////////////
		CVisRGBAByteImage image(1024,768,1,-1,imgBuf0);
		image.SetRect(0,0,1024,768);//cam 1
		//image.SetRect(0,0,640,480);//cam 1
		imageC=image;



//		imageFile2.SetRect(0,0,512,367);//cam 2
		
//		im_src=(BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );
		
		//CVisByteImage image(640,480,1,-1,imgBuf0);
		CVisRGBAByteImage image2(512,367,1,-1,imgBuf0);
		CVisRGBAByteImage image3(512,367,1,-1,imgBuf0);

		
		image2.SetRect(0,0,512,367);//cam 2
		image3.SetRect(0,0,512,367); //cam 3

		//imageBW=image;
		imageC2=image2;
		imageC3=image3;
	
		
	
//	imgBuf1 = (BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );
//	imgBuf2 = (BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );
//	imgBuf3 = (BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );
	

	im_src0=(BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );
	

	

	im_srcam2=(BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );
	im_srcam3=(BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );
	im_srcam1=(BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );
	


	for(int a1=0;a1<2000;a1++)
	{
	 rearBandLwSobel[a1].x= rearBandLwSobel[a1].y=0;
	 rearBandRwSobel[a1].x= rearBandRwSobel[a1].y=0;
	 FrontBandWSobel[a1].x=FrontBandWSobel[a1].y=0;
	}

	//////////// For the SObel Enhanced TB inspection


	grayscale_cam1=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	grayscale_cam2=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );
	grayscale_cam3=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );

	sobel_cam1=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,1024 * 768 );
	sobel_cam1_reduced=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,(1024 * 768)/2 );
	sobel_cam2=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );
	sobel_cam3=(BYTE*)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT,640 * 480 );

	/////////////
	im_red = (BYTE*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 1024 * 768);
	im_red2 = (BYTE*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 640 * 480);
	im_red3 = (BYTE*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 640 * 480);
	im_hue = (BYTE*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 1024 * 768);
	im_hue2 = (BYTE*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 640 * 480);
	im_hue3 = (BYTE*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 640 * 480);


	

	///////////////for the FIller Level Graph
	int i=0;
	for(i=0;i<600;i++){fill_level[i]=0.0;}
	count_fill=0;graph_open=0;

	for(int i2=0;i2<6000;i2++){fill_level_scale10[i2]=0.0;}
	count_fill_scale10=0;

	for(int i3=0;i3<120;i3++)
	{
		for(int i4=0;i4<6000;i4++)
		{filler_valves[i3][i4]=0;}
	}
	filler_no=0;fillr_no_value=0;
	/////////////

	////////////////////////////////////////////
	//ML- THIS TO KEEP TRACK OF CAMERA PERFORMANCE

	for(i=0; i<190; i++)
	{
		cameraDone1[i] = false;
		cameraDone2[i] = false;
		cameraDone3[i] = false;
	}
	
	first=true;

	////////////////////////////////////////////

 
}

//NOT USED
void CXAxisView::ReEstablishCameras()
{

}

///Called when configure inspection window is first opened. Loads image data from camera dialog box to this class.
///If camera is not connected the images are loaded from the hard drive.
void CXAxisView::PreInsp()
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	theapp=(CCapApp*)AfxGetApp();
	if(theapp->noCams==false)		//If cameras are connected.	
	{
		for(int i=0; i<EXPECTED_CAMS; i++)
		{
			//Reset camera processed image data pointers.
			AllocateAndCopyImage(i);
		}
		//camera1.

		//If image data is present.
		if(pframe->m_pcamera->m_imageProcessed[0][pframe->m_pcamera->readNext[0]]->GetData()!=NULL)
		{                                                    
			im_cam1=CtoBW(m_imageProcessed[0]);	//Convert camera 1 image from color to grayscale.
		}
		
		im_dataC=pframe->m_pcamera->m_imageProcessed[0][pframe->m_pcamera->readNext[0]]->GetData();		//Get color image data for camera 1 from camera dialog box.

		CVisByteImage image(theapp->cam1Width,theapp->cam1Height,1,-1,im_cam1);	//Create an instance of MSVision SDK image.
		image.SetRect(0,0,theapp->cam1Width,theapp->cam1Height);				//Position MSVision SDK image.
		imageBW=image;															//Save image to class variable.

		im_data=imageBW.PixelAddress(0,0,1);									//Make copy of the pointer to camera1 grayscale image.


		//--------------Same as above for camera2.
		im_dataC2=m_imageProcessed[1];
		if(pframe->m_pcamera->m_imageProcessed[1][pframe->m_pcamera->readNext[0]] !=NULL)
		{
			im_cam2=CtoBW2(m_imageProcessed[1]);
		}

		CVisByteImage image2(theapp->cam2Width,theapp->cam2Height,1,-1,im_cam2);
		image2.SetRect(0,0,theapp->cam2Width,theapp->cam2Height);
		imageBW2=image2;

		im_data2=imageBW2.PixelAddress(0,0,1);

		//----------------Same as above for camera3.
		im_dataC3=m_imageProcessed[2];
		if(pframe->m_pcamera->m_imageProcessed[2][pframe->m_pcamera->readNext[0]]->GetData()!=NULL)
		{
			im_cam3=CtoBW2(m_imageProcessed[2]);
		}

		CVisByteImage image3(theapp->cam3Width,theapp->cam3Height,1,-1,im_cam3);
		image3.SetRect(0,0,theapp->cam3Width,theapp->cam3Height);
		imageBW3=image3;

		im_data3=imageBW3.PixelAddress(0,0,1);



	}
	else  //If cameras are not connected.
	{
		//OnReadC("c:\\silgandata\\save\\colorImage.bmp");
		LoadImage();
		
	
	}

	
}

///When cameras are not connected loads images from hard drive.
void CXAxisView::LoadImage()
{
	CVisRGBAByteImage image;
	CVisRGBAByteImage imageL;
	CVisRGBAByteImage imageR;
	char* pFileName;char* pFileNameL;	char* pFileNameR;




	//	CVisRGBAByteImage image;
	//	char* pFileName;
		pFileName="c:\\silgandata\\cam1sd.bmp";//cam1sd.bmp";
	//	pFileName="c:\\SilganData\\51R56\\1saved.bmp";

	//	CVisRGBAByteImage imageL;
	//	char* pFileNameL;
		pFileNameL="c:\\silgandata\\cam2sd.bmp";//cam3sd.bmp";//v856L.bmp";

	//	CVisRGBAByteImage imageR;
	//	char* pFileNameR;
		pFileNameR="c:\\silgandata\\cam3sd.bmp";//cam2sd.bmp";//v856R.bmp";


	bool messedup=false;

	

		
	try
		{
			image.ReadFile(pFileName);
			imageFile.Allocate(image.Rect());

			image.CopyPixelsTo(imageFile);
		//	im_data=imageFile.PixelAddress(0,0,1);
			im_dataC=imageFile.PbFirstPixelInRow(0);




			//ReloadOnce=true;
		}
		catch (...) // "..." is bad - we could leak an exception object.
		{
			//pFileName="c:\\silgandata\\initpic.bmp";
			//image.ReadFile(pFileName);
			//imageFile.Allocate(image.Rect());

			//image.CopyPixelsTo(imageFile);
		//	im_data=imageFile.PixelAddress(0,0,1);
			//im_dataC=imageFile.PbFirstPixelInRow(0);
		
			//AfxMessageBox("The file specified could not be opened. load image");

			pframe->prompt_code=3;
			Prompt3 promtp3;
+			promtp3.DoModal();

			messedup=true;
			//return FALSE;
		}
	
		try
		{
			imageL.ReadFile(pFileNameL);
			imageFileL.Allocate(imageL.Rect());

			imageL.CopyPixelsTo(imageFileL);
		//	im_data=imageFile.PixelAddress(0,0,1);
			im_dataCL=imageFileL.PbFirstPixelInRow(0);
			//ReloadOnce=true;
		}
		catch (...) // "..." is bad - we could leak an exception object.
		{
			//pFileName="c:\\silgandata\\initpic.bmp";
			//image.ReadFile(pFileName);
			//imageFile.Allocate(image.Rect());

			//image.CopyPixelsTo(imageFile);
		//	im_data=imageFile.PixelAddress(0,0,1);
			//im_dataC=imageFile.PbFirstPixelInRow(0);
		
			//	AfxMessageBox("The file specified could not be opened. load image2");

			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();

			messedup=true;
			//return FALSE;
		}

		try
		{
			imageR.ReadFile(pFileNameR);
			imageFileR.Allocate(imageR.Rect());

			imageR.CopyPixelsTo(imageFileR);
		//	im_data=imageFile.PixelAddress(0,0,1);
			im_dataCR=imageFileR.PbFirstPixelInRow(0);
			//ReloadOnce=true;
		}
		catch (...) // "..." is bad - we could leak an exception object.
		{
			//pFileName="c:\\silgandata\\initpic.bmp";
			//image.ReadFile(pFileName);
			//imageFile.Allocate(image.Rect());

			//image.CopyPixelsTo(imageFile);
		//	im_data=imageFile.PixelAddress(0,0,1);
			//im_dataC=imageFile.PbFirstPixelInRow(0);


			//AfxMessageBox("The file specified could not be opened.");
			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();

			messedup=true;
			//return FALSE;
		}

		  // "..." is bad - we could leak an exception object.
		
if(messedup==false)
{	
    //Merely Flips a 180 Deg
	im_cam1Color=FlipImage1(im_dataC);
	im_cam2Color=FlipImage2(im_dataCL);
	im_cam3Color=FlipImage3(im_dataCR);

	//Flips 180 Deg and Converts to BW
	im_data=im_cam1=CtoBW(im_dataC);
	im_data2=im_cam2=CtoBW2(im_dataCL);
	im_data3=im_cam3=CtoBW3(im_dataCR);
	//if(KillSave==false) OnSaveC(m_imageProcessed.pData,1);

	GetRedChannel(im_cam1Color, theapp->cam1Width, theapp->cam1Height, im_red);
	GetRedChannel(im_cam2Color, theapp->cam2Width, theapp->cam2Height, im_red2);
	GetRedChannel(im_cam3Color, theapp->cam3Width, theapp->cam3Height, im_red3);
	GetHueChannel(im_cam1Color, theapp->cam1Width, theapp->cam1Height, im_hue);
	GetHueChannel(im_cam2Color, theapp->cam2Width, theapp->cam2Height, im_hue2);
	GetHueChannel(im_cam3Color, theapp->cam3Width, theapp->cam3Height, im_hue3);
	
	
	//Flips 180 Deg and Reduces to Half size
	im_reduce1=Reduce(im_dataC);
	im_reduce2=Reduce2(im_dataCL);
	im_reduce3=Reduce3(im_dataCR);
	CVisRGBAByteImage image1(theapp->cam1Width/2,theapp->cam1Height/2,1,-1,im_reduce1);
	CVisRGBAByteImage image2(theapp->cam2Width/2,theapp->cam2Height/2,1,-1,im_reduce2);
	CVisRGBAByteImage image3(theapp->cam3Width/2,theapp->cam3Height/2,1,-1,im_reduce3);
	
	if(theapp->saveOne2c==true){OnSaveC(im_cam2Color,2);theapp->saveOne2c=false;}
	if(theapp->saveOne2==true){OnSaveBW(im_cam2,2);theapp->saveOne2=false;}

	////	sobel_cam1=CalcSobel(im_cam1,2,1020,2,666);
		
		//OnSaveBW(sobel_cam1,1);

	
		//////paint image to screen
	imageC=image1;
	imageC2=image2;
	imageC3=image3;

}
		InvalidateRect(false);

}


//NOT USED
BYTE* CXAxisView::NoCamConvert(BYTE* Image)
{

		int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam1Width*2*4;

	int startx=0;
	int endx=theapp->cam1Width*2*4;
	int starty=0;
	int endy=theapp->cam1Height*2;
	
	int l=0;
	int j=0;
	int i, k;

	for(int i=starty; i<endy-20;)
	{
		for(int k=startx; k<endx-8;)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
			pix4=*((Image+k +4 +LineLength * (i)));
			pix5=*((Image+k +5 +LineLength * (i)));
			pix6=*((Image+k +6 +LineLength * (i)));

			pix1x=*((Image+k +LineLength * (i+1)));
			pix2x=*((Image+k+1+LineLength * (i+1)));
			pix3x=*((Image+k +2+LineLength * (i+1)));
			pix4x=*((Image+k +4 +LineLength * (i+1)));
			pix5x=*((Image+k +5 +LineLength * (i+1)));
			pix6x=*((Image+k +6 +LineLength * (i+1)));

			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
			*(im_reduceX +j +(LineLength/2) *l )=resultr;
			*(im_reduceX +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduceX +j+2 +(LineLength/2) *l )=resultb;
			k+=8;
			j+=4;
			
		}
		i+=2;
		l+=1;
		j=0;
		k=0;
	}
	return im_reduceX;
}


///Updates camera 1 image format (color/grayscale, spatial subsampling, vertical flipping). Starts inspection. Optionally saves images.
///Called from cameras grab thread.
///image: NOT USED, image to display on screen.
void CXAxisView::Display(BYTE *image)
{
	char str[100];
	/////////////////////////////////////
	//ML- THIS WILL KEEP TRACK OF THE CAMERA PERFORMANCE
		//signal
	WaitForSingleObject(pframe->m_pcamera->dispSignal[0], INFINITE);
	memset(str, 0, 100);
	sprintf(str, "start display1 %d\n", pframe->countC1.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));
	//Measure how long CXAxisView::Display(BYTE *image) takes.
	QueryPerformanceFrequency(&m_lnC1FreqI0); 
	QueryPerformanceCounter(&m_lnC1StartI0);	//start measurement timer.

	if(first==true)
	{
		first=false;
		pframe->countC0 = pframe->countC1.load();
	}

	//ML- ONLY Display starts this timer
	//Timer used to delay between camera 1 getting an image  and completing the display function 
	//and waiting for cameras 2 and 3 to get an image and complete their display functions. 
	//After this delay XAxisInspect() will be called.
	SetTimer(2,theapp->inspDelay,NULL);		


	////////////////////////////
		LARGE_INTEGER li;double PCFreq = 0.0;
	__int64 CounterStart = 0;

	if(!QueryPerformanceFrequency(&li))	cout << "QueryPerformanceFrequency failed!\n";


   
    PCFreq = double(li.QuadPart)/1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;

//	if(pframe->InspOpen == false)
//	{
		//////////////////////////////		
		//Transfer image data from camera to XAxisView class.
		AllocateAndCopyImage(0);

		im_cam1Color=FlipImage1(m_imageProcessed[0]);									//Just flips vertically
		
		im_cam1=CtoBW(m_imageProcessed[0]);												//Converts to grayscale
		

	
		im_reduce1=Reduce(m_imageProcessed[0]);											//Spatially subsample
		
		if(theapp->saveOne1c==true){OnSaveC(im_reduce1,6);theapp->saveOne1c=false;}		//NOT USED
		if(theapp->saveOne1==true){OnSaveBW(im_cam1,1);theapp->saveOne1=false;}			//NOT USED

	//	if(!theapp->jobinfo[pframe->CurrentJobNum].do_sobel ) 
	//	{
				//Create MSVisSDK image from reduced image of camera 1.


	/*	}
		else
		{
				sobel_cam1=CalcSobel(im_cam1,1,1020,2,666);
				CalcSobel_Reduced(im_reduce1);
				CVisByteImage imagebwSobel(theapp->cam1Width/2,theapp->cam1Height/2,1,-1,sobel_cam1_reduced);
				imageC1Sobel= imagebwSobel;

		}
*/

		//if (theapp->jobinfo[pframe->CurrentJobNum].dark == 2) //red
		//{
		//	GetRedChannel(im_cam1Color, theapp->cam1Width, theapp->cam1Height, im_red);
		//}

		//if (theapp->jobinfo[pframe->CurrentJobNum].dark = 3) //hue
		//{
		//	GetHueChannel(im_cam1Color, theapp->cam1Width, theapp->cam1Height, im_hue);
		//}


		if(theapp->savepics==true)		//saving all the color images
        {
            
            if( OnSaveC(im_cam1Color,1))	//Save camera 1 image.
            {
				if( OnSaveC(im_cam2Color,2))	//Save camera 1 image.
				{
					if( OnSaveC(im_cam3Color,3))	//Save camera 1 image.
					{
						
						//AfxMessageBox("Successfully Executed all 3 Image Saving functions");
					}		
				}	
            }
           // theapp->savepics=false; 
        }

	
		//ML- THIS IS CALLED FROM TIMER2 NOW
		//Inspect(im_cam1Color, im_cam2Color, im_cam3Color, im_cam1,im_cam2,im_cam3,sobel_cam2,sobel_cam3);

		//////////////////////////////////////////
		QueryPerformanceCounter(&li);
		spanElapsed4=double(li.QuadPart-CounterStart)/PCFreq;
		////////////////////////////////
	
		//ML- THIS ONLY INSIDE Inspect(...),ONCE DONE
		/*
		if(pframe->PicsOpen==false)
		{
			if(pframe->Freeze==false || BlackOK==false)
			{InvalidateRect(NULL,false);}
			else
			{
				if(pframe->FreezeReset==true) 
				{InvalidateRect(NULL,false);}
				
				if(BadCap==true)
				{InvalidateRect(NULL,false); pframe->FreezeReset=false;}else{ pframe->m_pview->UpdateDisplay2();}
			}
		}*/
//	}		
	
	

	/////////////////////////////////////

	cameraDone1[pframe->countC1]	= true;	//Tracking cam performance

	QueryPerformanceCounter(&m_lnC1EndI0);  	//end measurement timer.
	nDiff1=0; 
	timeMicroSecondC1I0= 0;

	//Convert to seconds.
	if( m_lnC1StartI0.QuadPart !=0 && m_lnC1EndI0.QuadPart != 0)
	{
		timeMicroSecondC1I0	= (double)(m_lnC1FreqI0.QuadPart/1000); 
		nDiff1			= m_lnC1EndI0.QuadPart-m_lnC1StartI0.QuadPart; 
		timeMicroSecondC1I0	= nDiff1/timeMicroSecondC1I0;
	}

	/////////////////////////////////////
	memset(str, 0, 100);
	sprintf(str, "end display1 %d\n", pframe->countC1.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));

	if (cameraDone2[pframe->countC1.load()] && cameraDone3[pframe->countC1.load()])
	{
		memset(str, 0, 100);
		sprintf(str, "Go next %d\n", pframe->countC1.load());
		pframe->m_pcamera->filestream.write(str, strlen(str));
		//signal
		DrawImages();
		for(int i=0; i<EXPECTED_CAMS; i++)
			ReleaseSemaphore(pframe->m_pcamera->dispSignal[i], 1, 0);
	}
	pframe->m_pcamera->filestream.flush();
}

///Updates camera 2 image format (color/grayscale, spatial subsampling, vertical flipping). Starts inspection. Optionally saves images.
///Called from cameras grab thread.
///image: NOT USED, image to display on screen.
void CXAxisView::Display2(BYTE *image)
{
	char str[100];
	WaitForSingleObject(pframe->m_pcamera->dispSignal[1], INFINITE);
	memset(str, 0, 100);
	sprintf(str, "start display2 %d\n", pframe->countC2.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));
	////////////////////////////////////////
	//ML- MEASURING TIME FOR DISPLAY2
	QueryPerformanceFrequency(&m_lnC2FreqI0);
	QueryPerformanceCounter(&m_lnC2StartI0);	//start measurement timer.

	////////////////
	if(first==true)
	{
		first=false;
		pframe->countC0 = pframe->countC2.load();
	}

	////////////////////////////////////////

	AllocateAndCopyImage(1);
	im_cam2=CtoBW2(m_imageProcessed[1]);
	im_cam2Color=FlipImage2(m_imageProcessed[1]);

	if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam2==true))
	{
		sobel_cam2=CalcSobel2(im_cam2Color,2);//im_reduce2_sobel=Reduce2_sobel(sobel_cam2); //OnSaveBW(sobel_cam2,2);

	}

	
	if(theapp->saveOne2c==true){OnSaveC(im_cam2Color,2);theapp->saveOne2c=false;}
	if(theapp->saveOne2==true){OnSaveBW(im_cam2,2);theapp->saveOne2=false;}

	im_reduce2=Reduce2(m_imageProcessed[1]);	
		


	////////////////////////////////////////

	cameraDone2[pframe->countC2]	= true;	//Tracking cam performance

	QueryPerformanceCounter(&m_lnC2EndI0);  	//end measurement timer.
	nDiff2=0; 
	timeMicroSecondC2I0= 0;

	if( m_lnC2StartI0.QuadPart !=0 && m_lnC2EndI0.QuadPart != 0)
	{
		timeMicroSecondC2I0	= (double)(m_lnC2FreqI0.QuadPart/1000); 
		nDiff2			= m_lnC2EndI0.QuadPart-m_lnC2StartI0.QuadPart; 
		timeMicroSecondC2I0	= nDiff2/timeMicroSecondC2I0;
	}

	memset(str, 0, 100);
	sprintf(str, "end display2 %d\n", pframe->countC2.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));

	if (cameraDone1[pframe->countC2.load()] && cameraDone3[pframe->countC2.load()])
	{
		memset(str, 0, 100);
		sprintf(str, "Go next %d\n", pframe->countC2.load());
		pframe->m_pcamera->filestream.write(str, strlen(str));
		//signal
		DrawImages();
		for (int i = 0; i < EXPECTED_CAMS; i++)
			ReleaseSemaphore(pframe->m_pcamera->dispSignal[i], 1, 0);
	}
	pframe->m_pcamera->filestream.flush();

}

void CXAxisView::DrawImages()
{
	write = true;
	CVisRGBAByteImage image1(theapp->cam1Width / 2, theapp->cam1Height / 2, 1, -1, im_reduce);
	imageC = image1;
	CVisRGBAByteImage image2(theapp->cam2Width / 2, theapp->cam2Height / 2, 1, -1, im_reduce2);
	imageC2 = image2;
	CVisRGBAByteImage image3(theapp->cam3Width / 2, theapp->cam3Height / 2, 1, -1, im_reduce3);
	imageC3 = image3;
	char str[100];
	memset(str, 0, 100);
	sprintf(str, "Update Images %d, %d, %d\n", pframe->countC1.load(), pframe->countC2.load(), pframe->countC3.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));

}


///Updates camera 3 image format (color/grayscale, spatial subsampling, vertical flipping). Starts inspection. Optionally saves images.
///Called from cameras grab thread.
///image: NOT USED, image to display on screen.
void CXAxisView::Display3(BYTE *image)
{
	char str[100];
	////////////////////////////////////////
	//ML- MEASURING TIME FOR DISPLAY2
	WaitForSingleObject(pframe->m_pcamera->dispSignal[2], INFINITE);
	memset(str, 0, 100);
	sprintf(str, "start display3 %d\n", pframe->countC3.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));

	QueryPerformanceFrequency(&m_lnC3FreqI0);
	QueryPerformanceCounter(&m_lnC3StartI0); 	//start measurement timer.

	if(first==true)
	{
		first=false;
		pframe->countC0 = pframe->countC3.load();
	}

	////////////////////////////////////////
	AllocateAndCopyImage(2);
	im_cam3=CtoBW3(m_imageProcessed[2]);
	im_cam3Color=FlipImage3(m_imageProcessed[2]);

	if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 ==true))
	{	sobel_cam3=CalcSobel3(im_cam3Color,3);//im_reduce3_sobel=Reduce3_sobel(sobel_cam3); //OnSaveBW(sobel_cam3,3);
	
	
	}
	
	if(theapp->saveOne3c==true){OnSaveC(im_cam3Color,3);theapp->saveOne3c=false;}
	if(theapp->saveOne3==true){OnSaveBW(im_cam3,3);theapp->saveOne3=false;}

	im_reduce3=Reduce3(m_imageProcessed[2]);	

	////////////////////////////////////////
	cameraDone3[pframe->countC3]	= true;	//Tracking cam performance

	QueryPerformanceCounter(&m_lnC3EndI0);  	//end measurement timer.
	nDiff3=0; 
	timeMicroSecondC3I0= 0;

	if( m_lnC3StartI0.QuadPart !=0 && m_lnC3EndI0.QuadPart != 0)
	{
		timeMicroSecondC3I0	= (double)(m_lnC3FreqI0.QuadPart/1000); 
		nDiff3			= m_lnC3EndI0.QuadPart-m_lnC3StartI0.QuadPart; 
		timeMicroSecondC3I0	= nDiff3/timeMicroSecondC3I0;
	}

	memset(str, 0, 100);
	sprintf(str, "End display3 %d\n", pframe->countC3.load());
	pframe->m_pcamera->filestream.write(str, strlen(str));

	if (cameraDone1[pframe->countC3.load()] && cameraDone2[pframe->countC3.load()])
	{
		memset(str, 0, 100);
		sprintf(str, "Go next %d\n", pframe->countC3.load());
		pframe->m_pcamera->filestream.write(str, strlen(str));
		//signal
		DrawImages();
		for (int i = 0; i < EXPECTED_CAMS; i++)
			ReleaseSemaphore(pframe->m_pcamera->dispSignal[i], 1, 0);
	}
	pframe->m_pcamera->filestream.flush();

	////////////////////////////////////////
}

//Used to convert color to gray scale images for camera 1.
//image: image to convert from color to grayscale.
BYTE* CXAxisView::CtoBW(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;

	int resultavg=0;

	int LineLength=theapp->cam1Width*4;
	int endx=theapp->cam1Width*4;
	int startx=0;
	
	int starty=0;
	int endy=theapp->cam1Height;

	int l=theapp->cam1Height;
	int j=theapp->cam1Width;
	
	for(int i=starty; i<endy; i++)
	{
		for(int k=startx; k<endx-4; k+=4)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
		
			resultavg=(pix1+pix2+pix3)/3;
		
			*(im_bw +j +(LineLength/4) *l )=resultavg;
			
			if(j>0) j-=1;
			
		}
	
		j=theapp->cam1Width;
		if(l>0) l-=1;
	}
	return im_bw;
}

//Used to convert color to gray scale images for camera 2.
//image: image to convert from color to grayscale.
BYTE* CXAxisView::CtoBW2(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;

	int resultavg=0;

	int LineLength=theapp->cam2Width*4;
	int startx=0;//theapp->cam2Left*4;
	int endx=theapp->cam2Width*4;// +theapp->cam3Left*4;
	int starty=0;//theapp->cam3Top;
	int endy=theapp->cam2Height;//+theapp->cam3Top;

	int l=theapp->cam2Height;
	int j=theapp->cam2Width;
	
	for(int i=starty; i<endy; i++)
	{
		for(int k=startx; k<endx; k+=4)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
		
			resultavg=(pix1+pix2+pix3)/3;
		
			*(im_bw2 +j +(LineLength/4) *l )=resultavg;
			
			if(j>0) j-=1;
			
		}
	
		j=theapp->cam2Width;
		if(l>0) l-=1;
	}
	return im_bw2;
}

//Used to convert color to gray scale images for camera 3.
//image: image to convert from color to grayscale.
BYTE* CXAxisView::CtoBW3(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;

	int resultavg=0;

	int LineLength=theapp->cam3Width*4;
	int startx=0;//theapp->cam3Left*4;
	int endx=theapp->cam3Width*4;// +theapp->cam3Left*4;
	int starty=0;//theapp->cam3Top;
	int endy=theapp->cam3Height;//+theapp->cam3Top;

	int l=theapp->cam3Height;
	int j=theapp->cam3Width;
	
	for(int i=starty; i<endy; i++)
	{
		for(int k=startx; k<endx; k+=4)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
		
			resultavg=(pix1+pix2+pix3)/3;
		
			*(im_bw3 +j +(LineLength/4) *l )=resultavg;
			
			if(j>0) j-=1;
			
		}
	
		j=theapp->cam3Width;
		if(l>0) l-=1;
	}
	return im_bw3;
}

//Used to vertically and horizontally flip camera1 images.
//image: image to flip.
BYTE* CXAxisView::FlipImage1(BYTE* Image)
{
	int pix1=0;	//Holds R byte
	int pix2=0;	//Holds G byte
	int pix3=0;	//Holds B byte
		
	int LineLength=theapp->cam1Width*4;		//image stride. i.e. image width in bytes.

	int startx=0;
	int endx=theapp->cam1Width*4;
	int starty=0;
	int endy=theapp->cam1Height;
	
	int l=theapp->cam1Height;
	int j=theapp->cam1Width*4;
	
	//Flip image.
	for(int i=starty; i<endy; i++)
	{
		for(int k=startx; k<endx-4; k+=4)
		{
			*(im_flip1 +j + (LineLength) *l )=*((Image+k + LineLength * (i)));
			*(im_flip1 +j+1+(LineLength) *l )=*((Image+k+1+LineLength * (i)));
			*(im_flip1 +j+2+(LineLength) *l )=*((Image+k+2+LineLength * (i)));
		
			if(j>=4)j-=4;	//Check bound and decrement horizontal index of flipped image.	
		}
	
		if(l>0)l-=1;	//Check bound and decrement vertical index of averaged image.
		j=theapp->cam1Width*4;
	}
	return im_flip1;
}

//Used to vertically and horizontally flip camera2 images.
//image: image to flip.
BYTE* CXAxisView::FlipImage2(BYTE* Image)
{
	int pix1=0;
	int pix2=0;
	int pix3=0;
		
	int LineLength=theapp->cam2Width*4;

	int startx=0;
	int endx=theapp->cam2Width*4;
	int starty=0;
	int endy=theapp->cam2Height;
	
	int l=theapp->cam2Height;
	int j=theapp->cam2Width*4;
	
	for(int i=starty; i<endy; i++)
	{
		for(int k=startx; k<endx-4; k+=4)
		{
			*(im_flip2 +j + (LineLength) *l )=*((Image+k + LineLength * (i)));
			*(im_flip2 +j+1+(LineLength) *l )=*((Image+k+1+LineLength * (i)));
			*(im_flip2 +j+2+(LineLength) *l )=*((Image+k+2+LineLength * (i)));
		
			if(j>=4)j-=4;	
		}
	
		if(l>0)l-=1;
		j=theapp->cam2Width*4;
	}
	return im_flip2;
}

//Used to vertically and horizontally flip camera3 images.
//image: image to flip.
BYTE* CXAxisView::FlipImage3(BYTE* Image)
{
	int pix1=0;
	int pix2=0;
	int pix3=0;
		
	int LineLength=theapp->cam3Width*4;

	int startx=0;
	int endx=theapp->cam3Width*4;
	int starty=0;
	int endy=theapp->cam3Height;
	
	int l=theapp->cam3Height;
	int j=theapp->cam3Width*4;
	
	for(int i=starty; i<endy; i++)
	{
		for(int k=startx; k<endx-4; k+=4)
		{
			*(im_flip3 +j + (LineLength) *l )=*((Image+k + LineLength * (i)));
			*(im_flip3 +j+1+(LineLength) *l )=*((Image+k+1+LineLength * (i)));
			*(im_flip3 +j+2+(LineLength) *l )=*((Image+k+2+LineLength * (i)));
		
			if(j>=4)j-=4;	
		}
	
		if(l>0)l-=1;
		j=theapp->cam3Width*4;
	}
	return im_flip3;
}

//Spatially averages 2x2 pixels for camera 1 image.
//Vertically reduce size by 2.
//Horizontally reduce size by 2.
//Throw out transparency byte of every pixel.
//Horizontally and vertically flipped?
//If extra light option is on converts required are of image to binary.
//image: image to spatially average.
BYTE* CXAxisView::Reduce(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam1Width*4;

	int startx=0;
	int endx=theapp->cam1Width*4;
	int starty=0;
	int endy=theapp->cam1Height;
	
	int l=theapp->cam1Height/2;		//height index of resized image. Size of image is halved. Starts from bottom of image.
	int j=theapp->cam1Width*4/2;	//width index of resized image. Size of image is halved. Starts from right side of image.

	//int fillx=endx-( (MidTop.x*4)+ (theapp->jobinfo[pframe->CurrentJobNum].fillX*4)  );
	//int fillx=( (1024-(theapp->jobinfo[pframe->CurrentJobNum].fillX*2)*4)+((1020-MidTop.x)*4) )-300*4;
	
	//variables used when extra light option is enabled
	//int fillx=( (1024*4)/2 )-(350*4/2);		//Left x coordinate from which binary area of the image is suppose to start.  //512 /2 - (
	//int fillx2=fillx+350*4;				//Right x coordinate at which binary area of the image is suppose to end.
	int fillx=( (1024*4)/2 )-225*2;	//Left x coordinate from which binary area of the image is suppose to start.
	int fillx2=fillx+225*4;			//Right x coordinate at which binary area of the image is suppose to end.
	
	int filly=endy-(theapp->jobinfo[pframe->CurrentJobNum].fillArea*2)-(120*2);//Lower y coordinate from which binary area of the image is suppose to start.
	int filly2=filly+(120*2);	//Upper y coordinate at which binary area of the image is suppose to end.


	//int z=(theapp->cam1Width*4)-8;
	//int o=theapp->cam1Height;
	for(int i=starty; i<endy-2;)
	{
		for(int k=startx; k<endx-4;)
		{
			//Take 2 pixels from horizontal line. 0 offset = R byte, 1 offset = G byte, 2 offset = B byte, 3 offset transparency value.
			pix1=*((Image+k +LineLength * (i)));		//R byte, pixel 1, line a
			pix2=*((Image+k+1+LineLength * (i)));		//G byte, pixel 1, line a
			pix3=*((Image+k +2+LineLength * (i)));		//B byte, pixel 1, line a
			pix4=*((Image+k +4 +LineLength * (i)));		//R byte, pixel 2, line a
			pix5=*((Image+k +5 +LineLength * (i)));		//G byte, pixel 2, line a
			pix6=*((Image+k +6 +LineLength * (i)));		//B byte, pixel 2, line a

			//Take 2 pixels from the next horizontal line
			pix1x=*((Image+k +LineLength * (i+1)));		//R byte, pixel 1, line b
			pix2x=*((Image+k+1+LineLength * (i+1)));	//G byte, pixel 1, line b
			pix3x=*((Image+k +2+LineLength * (i+1)));	//B byte, pixel 1, line b
			pix4x=*((Image+k +4 +LineLength * (i+1)));	//R byte, pixel 2, line b
			pix5x=*((Image+k +5 +LineLength * (i+1)));	//G byte, pixel 2, line b
			pix6x=*((Image+k +6 +LineLength * (i+1)));	//B byte, pixel 2, line b

			//Average 2x2 pixels
			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
		//creating a binary image when Xtra Light option is enabled
		if(theapp->jobinfo[pframe->CurrentJobNum].xtraLight==true)
		{
			//If in area which is suppose to be converted to binary
			if(i>filly && i< filly2 && k>fillx && k<fillx2)
			{

				//If below binary threshold
				if(resultb <=theapp->jobinfo[pframe->CurrentJobNum].redCutOff)
				{
					//Mark as black
					*(im_reduce +j +(LineLength/2) *l )  =0;
					*(im_reduce +j+1 +(LineLength/2) *l )=0;
					*(im_reduce +j+2 +(LineLength/2) *l )=0;

				}
				else //Otherwise mark as white.
				{
					*(im_reduce +j +(LineLength/2) *l )=255;
					*(im_reduce +j+1 +(LineLength/2) *l )=255;
					*(im_reduce +j+2 +(LineLength/2) *l )=255;
				}
			}
			else
			{
				*(im_reduce +j +(LineLength/2) *l )=resultr;
				*(im_reduce +j+1 +(LineLength/2) *l )=resultg;
				*(im_reduce +j+2 +(LineLength/2) *l )=resultb;
			}
		}
		else
		{
			*(im_reduce +j +(LineLength/2) *l )=resultr;
			*(im_reduce +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduce +j+2 +(LineLength/2) *l )=resultb;
		}
			
			k+=8;	//Increment horizontal index for original image by 2 pixels, i.e. 8 bytes.
			//if(z>=8)z-=8;
			if(j>=4)j-=4;	//Check bound and decrement horizontal index of averaged image.
			
		}
		i+=2;			//Increment vertical index for original image by 2 lines.
		if(l>0)l-=1;	//Check bound and decrement vertical index of averaged image.
		//if(o>1)o-=2;
		//z=(theapp->cam1Width*4)-8;
		j=theapp->cam1Width*4/2;
	}
	return im_reduce;
}

//Spatially averages 2x2 pixels for camera 2 image.
//Vertically reduce size by 2.
//Horizontally reduce size by 2.
//Throw out transparency byte of every pixel.
//Horizontally and vertically flipped?
//image: image to spatially average.
BYTE* CXAxisView::Reduce2(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam2Width*4;

	int startx=0;
	int endx=theapp->cam2Width*4;
	int starty=1;
	int endy=theapp->cam2Height;
	
	int l=theapp->cam2Height/2;
	int j=(theapp->cam2Width*4)/2;
	
	for(int i=starty; i<endy;)
	{
		for(int k=startx; k<endx-4;)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
			pix4=*((Image+k +4 +LineLength * (i)));
			pix5=*((Image+k +5 +LineLength * (i)));
			pix6=*((Image+k +6 +LineLength * (i)));

			pix1x=*((Image+k +LineLength * (i+1)));
			pix2x=*((Image+k+1+LineLength * (i+1)));
			pix3x=*((Image+k +2+LineLength * (i+1)));
			pix4x=*((Image+k +4 +LineLength * (i+1)));
			pix5x=*((Image+k +5 +LineLength * (i+1)));
			pix6x=*((Image+k +6 +LineLength * (i+1)));

			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
			*(im_reduce2 +j +(LineLength/2) *l )=resultr;
			*(im_reduce2 +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduce2 +j+2 +(LineLength/2) *l )=resultb;
			k+=8;
			if(j>=4)j-=4;
			
		}
		i+=2;
		if(l>0)l-=1;
		j=(theapp->cam2Width*4)/2;
	}
	return im_reduce2;
}

//Spatially averages 2x2 pixels for camera 3 image.
//Vertically reduce size by 2.
//Horizontally reduce size by 2.
//Throw out transparency byte of every pixel.
//Horizontally and vertically flipped?
//image: image to spatially average.
BYTE* CXAxisView::Reduce3(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam3Width*4;

	int startx=0;
	int endx=theapp->cam3Width*4;
	int starty=0;
	int endy=theapp->cam3Height;
	
	int l=theapp->cam2Height/2;
	int j=(theapp->cam2Width*4)/2;
	
	for(int i=starty; i<endy;)
	{
		for(int k=startx; k<endx-4;)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
			pix4=*((Image+k +4 +LineLength * (i)));
			pix5=*((Image+k +5 +LineLength * (i)));
			pix6=*((Image+k +6 +LineLength * (i)));

			pix1x=*((Image+k +LineLength * (i+1)));
			pix2x=*((Image+k+1+LineLength * (i+1)));
			pix3x=*((Image+k +2+LineLength * (i+1)));
			pix4x=*((Image+k +4 +LineLength * (i+1)));
			pix5x=*((Image+k +5 +LineLength * (i+1)));
			pix6x=*((Image+k +6 +LineLength * (i+1)));

			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
			*(im_reduce3 +j +(LineLength/2) *l )=resultr;
			*(im_reduce3 +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduce3 +j+2 +(LineLength/2) *l )=resultb;
			k+=8;
			if(j>=4)j-=4;
			
		}
		i+=2;
		if(l>0)l-=1;
		j=(theapp->cam2Width*4)/2;
	}
	return im_reduce3;
}


//NOT USED, Spatially down sample image of camera 2. 
BYTE* CXAxisView::Reduce2x(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam1Width*4;

	int startx=272*4;
	int endx=theapp->cam1Width*4;
	int starty=220;
	int endy=theapp->cam1Height;
	
	int l=0;
	int j=0;
	
	for(int i=starty; i<endy;)
	{
		for(int k=startx; k<endx-4;)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
			pix4=*((Image+k +4 +LineLength * (i)));
			pix5=*((Image+k +5 +LineLength * (i)));
			pix6=*((Image+k +6 +LineLength * (i)));

			pix1x=*((Image+k +LineLength * (i+1)));
			pix2x=*((Image+k+1+LineLength * (i+1)));
			pix3x=*((Image+k +2+LineLength * (i+1)));
			pix4x=*((Image+k +4 +LineLength * (i+1)));
			pix5x=*((Image+k +5 +LineLength * (i+1)));
			pix6x=*((Image+k +6 +LineLength * (i+1)));

			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
			*(im_reduce2 +j +(LineLength/2) *l )=resultr;
			*(im_reduce2 +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduce2 +j+2 +(LineLength/2) *l )=resultb;
			k+=8;
			j+=4;
			
		}
		i+=2;
		l+=1;
		j=0;
	}
	return im_reduce2;
}

//Spatially down sample image of camera 3. NOT USED
BYTE* CXAxisView::Reduce3x(BYTE* Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam1Width*4;

	int startx=224*4;
	int endx=theapp->cam1Width*4;
	int starty=220;
	int endy=theapp->cam1Height;
	
	int l=0;
	int j=0;
	
	for(int i=starty; i<endy;)
	{
		for(int k=startx; k<endx-4;)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
			pix3=*((Image+k +2+LineLength * (i)));
			pix4=*((Image+k +4 +LineLength * (i)));
			pix5=*((Image+k +5 +LineLength * (i)));
			pix6=*((Image+k +6 +LineLength * (i)));

			pix1x=*((Image+k +LineLength * (i+1)));
			pix2x=*((Image+k+1+LineLength * (i+1)));
			pix3x=*((Image+k +2+LineLength * (i+1)));
			pix4x=*((Image+k +4 +LineLength * (i+1)));
			pix5x=*((Image+k +5 +LineLength * (i+1)));
			pix6x=*((Image+k +6 +LineLength * (i+1)));

			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
			*(im_reduce3 +j +(LineLength/2) *l )=resultr;
			*(im_reduce3 +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduce3 +j+2 +(LineLength/2) *l )=resultb;
			k+=8;
			j+=4;
			
		}
		i+=2;
		l+=1;
		j=0;
	}
	return im_reduce3;
}


void CXAxisView::OnDraw(CDC* pDC)
{
	if (pframe->countC1.load() != pframe->countC2.load() ||
		pframe->countC2.load() != pframe->countC3.load() ||
		pframe->countC1.load() != pframe->countC3.load())
		return;
	//CDocument* pDoc = GetDocument();
	CClientDC dc( this );	
	CBrush Red, Blue, RedLine, Colorw, Gray;
	COLORREF greencolor = RGB(0,255,0);
	COLORREF redcolor = RGB(255,0,0);
	COLORREF bluecolor = RGB(0,0,150);
	COLORREF lbluecolor = RGB(168,218,242);
	COLORREF graycolor = RGB(190,190,190);
	COLORREF blackcolor = RGB(50,50,50);
	COLORREF gray = RGB(100,100,100);
	COLORREF wcolor = RGB(255,255,255);

	Colorw.CreateSolidBrush(wcolor);
//	CString m1,m2, m3, m4, m5, m6;
	Gray.CreateSolidBrush(RGB(192,192,192)); 
	Red.CreateSolidBrush(RGB(255,0,0)); 
	Blue.CreateSolidBrush(RGB(0,0,255));
	CPen BluePen(PS_SOLID, 1, RGB(0,0,250));
	CPen LGrayPen(PS_SOLID, 1, RGB(200,200,200));
	CPen LRedPen(PS_SOLID, 1, RGB(230,30,80));
	CPen FRedPen(PS_SOLID, 3, RGB(255,0,0));
	CPen RedDash(PS_DASH, 1, RGB(255,0,0));
	CPen BlackPen(PS_SOLID, 2, RGB(0,0,0));
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
	CPen LGreenPen(PS_SOLID, 1, RGB(0,255,0));
	CPen YellowPen(PS_SOLID, 1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID, 2, RGB(255,255,255));
	CPen AquaPen(PS_SOLID, 4, RGB(0,255,255));
	CPen GrayPen(PS_SOLID, 1, RGB(100,100,100));
	CPen GrayPenDashed(PS_DASH, 1, RGB(100,100,100));

	char buf[4];
	buf[3] = '\0';

	int xOffset=60;
	int xOffset3=67;
	int yOffset=-15;
	CRect rect,a;
	a.SetRect(0,0,640,60);
	GetClientRect(rect);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SelectObject(&GreenPen);

	pDC->SetWindowOrg(-xOffset,0);

	if (write)
	{
	char str1[100];
	memset(str1, 0, 100);
	sprintf(str1, "OnDraw Images %d, %d, %d\n", pframe->countC1.load(), pframe->countC2.load(), pframe->countC3.load());
	pframe->m_pcamera->filestream.write(str1, strlen(str1));
	pframe->m_pcamera->filestream.flush();
	}
//	if(!theapp->jobinfo[pframe->CurrentJobNum].do_sobel	)
//	{

		//Display images for camera 1.
		CVisImageBase& refimage=imageC;
		assert(refimage.IsValid());	
		refimage.DisplayInHdc(*pDC);
/*	}
	else
	{
		CVisImageBase& refimage=imageC1Sobel;
		assert(refimage.IsValid());	
		refimage.DisplayInHdc(*pDC);
	}
	*/
		CString str;
		str.Format("%d", pframe->countC1.load());
		pDC->TextOut(30, 50, str);

		//outlining the frame of the image for Camera1
		pDC->Draw3dRect( 1,1,theapp->cam1Width/2-10,theapp->cam1Height/2,blackcolor,blackcolor);//outlining the frame of the image in Camera1
		
		//Draw bottle offset value (distance from cameras) on camera 1 image.
		itoa(theapp->capWidth2,buf,10);
		pDC->TextOut(xOffset,10,buf);
	
	//If low fill enabled.
	if(theapp->inspctn[7].enable==true)
	{
		// Draw low fill box in image for camera 1.
		pDC->Draw3dRect( (MidTop.x)/2+theapp->jobinfo[pframe->CurrentJobNum].fillX, theapp->jobinfo[pframe->CurrentJobNum].fillY,theapp->SavedFillW/2,theapp->SavedFillH/2,redcolor,redcolor);//left side			
	}

	//Camera 2
	pDC->SetWindowOrg(0,-260);

	CVisImageBase& refimage2=imageC2;// = pDoc->Image();
		//refimage.SetRect(0,0,648,480);
	assert(refimage2.IsValid());
	
	//If not adjusting the low fill box in image of camera 1.
	if(TurnOff23==false) 
		refimage2.DisplayInHdc(*pDC); 	//Draw image for camera 2.

	CString str2;
	str2.Format("%d", pframe->countC2.load());
	pDC->TextOut(80, 5, str2);

	//Draw camera 2 top limit bar, in image of camera 2.
	pDC->SelectObject(&GrayPenDashed);
	pDC->MoveTo(0, theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2/2);
	pDC->LineTo(300,theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2/2);

	if(theapp->checkRearHeight== true)
	{	 
	//	pDC->Draw3dRect(Cam2_neckpts.x/2,Cam2_neckpts.y/2,3,3,bluecolor,bluecolor);
		pDC->Draw3dRect(CaptopRearL.x/2,CaptopRearL.y/2,3,3,redcolor,redcolor);	//Draw red rectangle for bottom most point of cap in left camera, cam2.
	
		pDC->SetTextColor(blackcolor);

		if(cam2min.x>0)
		{
			pDC->Draw3dRect(cam2min.x/2,cam2min.y/2,3,3,greencolor,greencolor); //Draw green rectangle for top most point of cap in left camera (cam2).
		}//marking the min pts on cap Top}
		if(cam2max.x>0)
		{
			pDC->Draw3dRect(cam2max.x/2,cam2max.y/2,3,3,bluecolor,bluecolor);	//Draw blue rectangle for bottom most point of cap in left camera, (cam2).
		}//marking the max pts on cap top
	}

	
	pDC->SetWindowOrg(-320,-260);//Camera 3

	CVisImageBase& refimage3=imageC3;// = pDoc->Image();
		//refimage.SetRect(0,0,648,480);
	assert(refimage3.IsValid());
	
	
	//If not adjusting the low fill box in image of camera 1.
	if(TurnOff23==false) 
		refimage3.DisplayInHdc(*pDC);	//Draw image for camera 3. 

	CString str3;
	str3.Format("%d", pframe->countC3.load());
	pDC->TextOut(220, 5, str3);



	//Draw camera 3 top limit bar, in image of camera 3.
	pDC->SelectObject(&GrayPenDashed);
	pDC->MoveTo(0, theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3/2);
	pDC->LineTo(300,theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3/2);

	if(theapp->checkRearHeight== true)
	{
	//	pDC->Draw3dRect(Cam3_neckpts.x/2,Cam3_neckpts.y/2,3,3,bluecolor,bluecolor);
		pDC->Draw3dRect(CaptopRearR.x/2,CaptopRearR.y/2,3,3,redcolor,redcolor);			//Draw red rectangle for bottom most point of cap in right camera, cam3.

		pDC->SetTextColor(blackcolor);

		if(cam3min.x>0)
		{
			pDC->Draw3dRect(cam3min.x/2,cam3min.y/2,3,3,greencolor,greencolor); 		//Draw green rectangle for top most point of cap in right camera (cam3).
		}//marking the min pts on cap Top}
		if(cam3max.x>0)
		{
			pDC->Draw3dRect(cam3max.x/2,cam3max.y/2,3,3,bluecolor,bluecolor); 			//Draw blue rectangle for bottom most point of cap in right camera, (cam3).
		}//marking the max pts on cap top

	}
			
	//Draw camera 1 top limit bar, in image of camera 1.	
	pDC->SetWindowOrg(0,0);

	//Red dividing line between cameras 2 and 3.
	pDC->SelectObject(&LRedPen);
	pDC->MoveTo(320, 260);
	pDC->LineTo(320,500);

	//Draw camera1 top limit bar, in image of camera 1.
	pDC->SelectObject(&GrayPenDashed);
	pDC->MoveTo(0, theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar/2);
	pDC->LineTo(900,theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar/2);

	//If modify tab is selected.
	if(Modify==true )
	{
		if(posToShow<=40)posToShow=40;
		if(posToShow>=220)posToShow=220;

		//Show left camera cap edge search box.
		pDC->Draw3dRect(theapp->jobinfo[pframe->CurrentJobNum].lSWin, 260+(posToShow/2)-5,160,10,bluecolor,bluecolor);

		//Show right camera cap edge search box.
		pDC->Draw3dRect(theapp->jobinfo[pframe->CurrentJobNum].rSWin+320, 260+(posToShow/2)-5,160,10,bluecolor,bluecolor);
		
		pDC->SetTextColor(graycolor);
		if(theapp->SavedPhotoeye==false)				//if right photoeye is chosen
		{
			pDC->TextOut(440,50,"Bottle Flow --->");	//Show "Bottle Flow --->" text
		}
		else											////if left photoeye is chosen
		{
			pDC->TextOut(440,50,"Bottle Flow <---");	//Show "Bottle Flow <---" text
		}
	}

	//If configure Inspection is open or if the modify tab is selected.
	if(pframe->InspOpen==true || Modify==true)
	{
		pDC->SelectObject(&BluePen);
			
		//Draw horizontal middle of cap line.
		pDC->MoveTo((MidBar.x/2)+xOffset, theapp->jobinfo[pframe->CurrentJobNum].midBarY);  //cap width lines left
		pDC->LineTo(640, theapp->jobinfo[pframe->CurrentJobNum].midBarY);
						
		//Draw horizontal neck line
		pDC->MoveTo(NeckBar.x+xOffset, theapp->jobinfo[pframe->CurrentJobNum].neckBarY+theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset);  //cap width lines left
		pDC->LineTo(640, theapp->jobinfo[pframe->CurrentJobNum].neckBarY+theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset);	
				
		//Draw vertical left cap edge line.	
		pDC->MoveTo(((CapLTop.x)/2)+xOffset, 0);  //cap width lines left
		pDC->LineTo(((CapLTop.x)/2)+xOffset, 280);	

		
		int xOffset2=10;	
		pDC->SelectObject(&GrayPen);
	/*
		pDC->MoveTo(0+xOffset3, 150+yOffset);
		pDC->LineTo(16+xOffset3, 150+yOffset);  //cap width lines left
		pDC->LineTo(10+xOffset3, 145+yOffset);
		pDC->LineTo(10+xOffset3, 155+yOffset);
		pDC->LineTo(16+xOffset3, 150+yOffset);

		pDC->MoveTo(11+xOffset3, 146+yOffset);
		pDC->LineTo(11+xOffset3, 154+yOffset);
		pDC->MoveTo(12+xOffset3, 147+yOffset);
		pDC->LineTo(12+xOffset3, 153+yOffset);
		pDC->MoveTo(13+xOffset3, 148+yOffset);
		pDC->LineTo(13+xOffset3, 152+yOffset);
		pDC->MoveTo(14+xOffset3, 149+yOffset);
		pDC->LineTo(14+xOffset3, 151+yOffset);
	*/
		pDC->SetTextColor(gray);
		pDC->TextOut(0+xOffset3,150+yOffset,"-------------->");

		pDC->SetTextColor(graycolor);
		pDC->TextOut(xOffset3,135+yOffset,"Photoeye");
	
		pDC->SelectObject(&BluePen);		
	}

	pDC->SelectObject(&LRedPen);
	
	pDC->Draw3dRect(((CapLSide.x)/2)+xOffset, (CapLSide.y)/2,4,4,redcolor,redcolor);	//Draw camera 1 left cap edge red rectangle
	pDC->Draw3dRect(((CapRSide.x)/2)+xOffset, (CapRSide.y)/2,4,4,redcolor,redcolor);	//Draw camera 1 right cap edge red rectangle

	if(theapp->jobinfo[pframe->CurrentJobNum].lip != 1)
	{
		pDC->Draw3dRect(((NeckLSide.x)/2)+xOffset, (NeckLSide.y)/2,4,4,redcolor,redcolor);	//Draw camera 1 left neck edge red rectangle
		pDC->Draw3dRect(((NeckRSide.x)/2)+xOffset, (NeckRSide.y)/2,4,4,redcolor,redcolor);	//Draw camera 1 right neck edge red rectangle	
	}
	if(theapp->inspctn[1].enable==true)		//If left height inspection is enabled.
	{
		pDC->Draw3dRect(((CapLTop.x)/2)+xOffset, (CapLTop.y)/2,4,4,redcolor,redcolor);	//Draw left top cap rectangle.
		if(theapp->jobinfo[pframe->CurrentJobNum].lip != 1)					//If bottle has normal lip 
		{ 
			pDC->Draw3dRect(((NeckLLip.x)/2)+xOffset-2,(NeckLLip.y)/2,4,4,redcolor,redcolor);	//Draw left lip rectangle.
		}
		else
		{
			pDC->Draw3dRect(((NeckLLip.x)/2)+xOffset-5,(NeckLLip.y)/2-5,10,10,redcolor,redcolor);	//Draw left lip rectangle.
		}

	/*	if(no_cap_detected ==true )
		{		pDC->SetTextColor(graycolor);
				pDC->TextOut(100,100,"No CAP DET");
				pDC->Draw3dRect(100,200,20,10,redcolor,redcolor);//No Cap
		}
		else if(no_cap_detected ==false)
		{
				pDC->SetTextColor(graycolor);
				pDC->TextOut(100,100," CAP DETECTED");
				pDC->Draw3dRect(100,200,20,10,greencolor,greencolor);//No Cap

		}
		*/
	}
	
	if(theapp->inspctn[2].enable==true)		//If right height inspection is enabled.
	{
		pDC->Draw3dRect(((CapRTop.x)/2)+xOffset, (CapRTop.y)/2,4,4,redcolor,redcolor);	//Draw right top cap rectangle.
		if(theapp->jobinfo[pframe->CurrentJobNum].lip != 1)		//If bottle has normal lip 
		{ 
			pDC->Draw3dRect(((NeckRLip.x)/2)+xOffset-2,(NeckRLip.y)/2,4,4,redcolor,redcolor);	//Draw right lip rectangle.
		}
		else
		{
			pDC->Draw3dRect(((NeckRLip.x)/2)+xOffset-5,(NeckRLip.y)/2-5,10,10,redcolor,redcolor);		//Draw right lip rectangle.
		}
	}

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==2)	//If bottle has sports cap.
	{
		pDC->Draw3dRect(100/2+xOffset,50/2,8,8,greencolor,greencolor);//left side

	}
	//*************Front Band^^^^^^^^^^^^^^
	if(theapp->inspctn[5].enable==true )	//If band edge inspection is enabled.
	{

		pDC->SelectObject(&GreenPen);
		//midBand=( (CapRSide.x-CapLSide.x)/2+CapLSide.x)/2;
		
		
		//Draw tamper band left edge inspection box, in camera 1 image.
		pDC->Draw3dRect(((CapLSide.x)/2)+(TBOffset.x)/2+xOffset, (CapLTop.y)/2+TBOffset.y/2,6,theapp->jobinfo[pframe->CurrentJobNum].tBandDy,greencolor,greencolor);
		


		//Draw camera 1 surface tamper band inspection search box.
		pDC->MoveTo( ((CapLSide.x)/2)+(TBOffset.x)/2+xOffset+7, (CapLTop.y)/2+TBOffset.y/2);
		pDC->LineTo( ((RTBand2.x-5)/2)+xOffset-5, RTBand2.y/2);
		pDC->LineTo( ((RTBand2.x-5)/2)+xOffset-5, RTBand2.y/2+theapp->jobinfo[pframe->CurrentJobNum].tBandDy);
		pDC->LineTo( ((CapLSide.x)/2)+(TBOffset.x)/2+xOffset+7, TBand2.y/2+theapp->jobinfo[pframe->CurrentJobNum].tBandDy);
		pDC->LineTo(((CapLSide.x)/2)+(TBOffset.x)/2+xOffset+7, (CapLTop.y)/2+TBOffset.y/2);
		
		//Draw tamper band right edge inspection box, in camera 1 image.
		pDC->Draw3dRect(((CapRSide.x)/2)-(TBOffset.x)/2+xOffset-5, (CapRTop.y)/2+TBOffset.y/2,6,theapp->jobinfo[pframe->CurrentJobNum].tBandDy,greencolor,greencolor);
	
		segSize=( (CapRSide.x-TBOffset.x)-(CapLSide.x+TBOffset.x) )/48;
		if(showPos==1)
		{
			//Draw black and white rectangles in camera 1 image to indicate that camera 1 surface tamper band inspection had the most values outside of bounds.
			pDC->Draw3dRect(((CapLSide.x)/2)+(TBOffset.x)/2+xOffset-5 , (CapLTop.y)/2+TBOffset.y/2,8,8,blackcolor,blackcolor);//+showPos1*segSize
			pDC->Draw3dRect(((CapLSide.x)/2)+(TBOffset.x)/2+xOffset-5 , (CapLTop.y)/2+TBOffset.y/2+2,4,4,wcolor,wcolor);//+showPos1*segSize+2
		}
	}

	//****************Color*************************
	if(theapp->inspctn[8].enable==true)  //If cap color inspection is enabled
		
		//Draw cap color search box
		pDC->Draw3dRect(
		(theapp->jobinfo[pframe->CurrentJobNum].userX)/2+xOffset+(MidTop.x)/2,
		theapp->jobinfo[pframe->CurrentJobNum].userY/2+(MidTop.y)/2,
		theapp->jobinfo[pframe->CurrentJobNum].userDx/2,
		theapp->jobinfo[pframe->CurrentJobNum].userDx/2,
		redcolor,
		redcolor);


	//********rear cameras
	if(theapp->inspctn[6].enable==true )	//If band surface inspection is enabled.
	{
	
		segSize=(Band2Size)/48;
		if(showPos==3)
		{
			//Draw black and white rectangles in camera 3 image to indicate that camera 1 surface tamper band inspection had the most values outside of bounds.
			pDC->Draw3dRect( ((RightBand.x)/2)+320 , (TBYOffsetR)/2+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260,8,8,blackcolor,blackcolor);//+showPos3*segSize
			pDC->Draw3dRect( ((RightBand.x)/2)+320+2, (TBYOffsetR)/2+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+2,4,4,wcolor,wcolor);// +showPos3*segSize
		}

		if(showPos==2)
		{
			//Draw black and white rectangles in camera 2 image to indicate that camera 1 surface tamper band inspection had the most values outside of bounds.
			pDC->Draw3dRect( ((LeftBand.x)/2), (TBYOffsetR)/2+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260,8,8,blackcolor,blackcolor);//-showPos2*segSize
			pDC->Draw3dRect( ((LeftBand.x)/2)+2, (TBYOffsetR)/2+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+2,4,4,wcolor,wcolor);//-showPos2*segSize
		
		}

		//pDC->SelectObject(&YellowPen)
		pDC->SelectObject(&LRedPen);
		//for(int count=0; count<=200; count++)
		//{

		for(int count=0; count<=1600; count+=8)
		{
			//Draw rear left (cam2) red rectangles indicating where the surface tamper band is broken.
			pDC->Draw3dRect( ((rearBandL[count].x)/2) , (rearBandL[count].y)/2+260,2,2,redcolor,redcolor);
			rearBandL[count].x=rearBandL[count].y=0;
		
			//Draw rear right (cam3) red rectangles indicating where the surface tamper band is broken.
			pDC->Draw3dRect( ((rearBandR[count].x)/2+320) , (rearBandR[count].y)/2+260,2,2,redcolor,redcolor);
			rearBandR[count].x=rearBandR[count].y=0;

			//Draw rear front (cam1) red rectangles indicating where the surface tamper band is broken.
			pDC->Draw3dRect( ((frontBand[count].x)/2+xOffset) , (frontBand[count].y)/2,2,2,redcolor,redcolor);
			frontBand[count].x=frontBand[count].y=0;
		}

		//If sobel based tamper band inspection is enabled.
		if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true)
		{
			if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 == true)
			{
				for(int count=0; count<=1600; count+=1)
				{
					if(rearBandLwSobel[count].x!=0 && rearBandLwSobel[count].y!=0){pDC->Draw3dRect( ((rearBandLwSobel[count].x)/2) , (rearBandLwSobel[count].y)/2+260,2,2,bluecolor,bluecolor);
					rearBandLwSobel[count].x=rearBandLwSobel[count].y=0;}
				}
			}

			if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 == true)
			{
				for(int count1=0; count1<=1600; count1+=1)
				{
					if(rearBandRwSobel[count1].x!=0 && rearBandRwSobel[count1].y!=0){pDC->Draw3dRect( ((rearBandRwSobel[count1].x)/2+320) , (rearBandRwSobel[count1].y)/2+260,2,2,bluecolor,bluecolor);
					rearBandRwSobel[count1].x=rearBandRwSobel[count1].y=0;}
				}
			}

			if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 == true)
			{
				for(int count2=0; count2<=1600; count2+=1)
				{
					if(FrontBandWSobel[count2].x!=0 && FrontBandWSobel[count2].y!=0){pDC->Draw3dRect( ((FrontBandWSobel[count2].x)/2+xOffset) , (FrontBandWSobel[count2].y)/2,2,2,bluecolor,bluecolor);
					FrontBandWSobel[count2].x=FrontBandWSobel[count2].y=0;}
				}
			}


		}

		int shift=-5; //what is this? why -5?

		//Draw rectangles to the left and right of the camera 1 image.
		if(lBand[1].x !=0) pDC->Rectangle(lBand[1].x,lBand[1].y+260,lBand[1].x+5,lBand[1].y+260+5);
		if(rBand[1].x !=0) pDC->Rectangle(rBand[1].x+320, rBand[1].y+260,rBand[1].x+320+5,rBand[1].y+260+5);

		
		if(theapp->surfLR==true)	//If camera 2 surface tamper band inspection is enabled.
		{
			//Draw rear camera surface tamper band inspection rectangles
			pDC->Draw3dRect( 
				((LeftBand.x)/2)-(Band2Size)/2, 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( 
				((LeftBand.x)/2)-(Band2Size)/2+sgSize/2, 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( 
				((LeftBand.x)/2)-(Band2Size)/2+2*(sgSize/2), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( 
				((LeftBand.x)/2)-(Band2Size)/2+3*(sgSize/2), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);
			pDC->Draw3dRect( 
				((LeftBand.x)/2)-(Band2Size)/2+4*(sgSize/2), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);
		}

		if(theapp->surfRR==true)	//If camera 3 surface tamper band inspection is enabled.
		{
			//Draw rear camera surface tamper band inspection rectangles
			pDC->Draw3dRect( 
				((RightBand.x)/2+320), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( 
				((RightBand.x)/2)+320+sgSize/2, 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( 
				((RightBand.x)/2)+320+2*(sgSize/2), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( 
				((RightBand.x)/2)+320+3*(sgSize/2), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);
			pDC->Draw3dRect( 
				((RightBand.x)/2)+320+4*(sgSize/2), 
				((TBYOffsetR)/2+theapp->jobinfo[pframe->CurrentJobNum].rearCamY)/2+260+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5]/2+shift,
				Band2Size/10,
				theapp->jobinfo[pframe->CurrentJobNum].rearCamDy/2,
				greencolor,
				greencolor);
		
		}
	}	

	
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1 && theapp->inspctn[3].enable==true)		//If the bottle has a sports cap.
	{
		
		pDC->MoveTo( ((CapLTop.x)/2)+xOffset, theapp->jobinfo[pframe->CurrentJobNum].sportPos/2-5);
		pDC->LineTo( ((CapRTop.x)/2)+xOffset, theapp->jobinfo[pframe->CurrentJobNum].sportPos/2-5);
		pDC->LineTo( ((CapRTop.x)/2)+xOffset, theapp->jobinfo[pframe->CurrentJobNum].sportPos/2+5);
		pDC->LineTo( ((CapLTop.x)/2)+xOffset, theapp->jobinfo[pframe->CurrentJobNum].sportPos/2+5);
		pDC->LineTo( ((CapLTop.x)/2)+xOffset, theapp->jobinfo[pframe->CurrentJobNum].sportPos/2-5);
		
		pDC->Draw3dRect( sportBand.x/2+xOffset, theapp->jobinfo[pframe->CurrentJobNum].sportPos/2,3,6,greencolor,greencolor);
		pDC->Draw3dRect(sportBand.x1/2+xOffset,theapp->jobinfo[pframe->CurrentJobNum].sportPos/2,3,6,greencolor,greencolor);

	}
	else
	{	
		//Draw mid cap rectangle.
		if(theapp->inspctn[3].enable==true) pDC->Draw3dRect(((MidTop.x)/2)+xOffset, (MidTop.y)/2,4,4,redcolor,redcolor);//left side
		
		//itoa(midYResult,buf,10);
		//pDC->TextOut(50+xOffset,10,buf);
	}

	//User Inspection, NOT USED.
	if(theapp->inspctn[9].enable==true)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].userType==1 )
		{
			for(int i=0; i<capPointCount; i++)
			{
				pDC->Draw3dRect( (capPoint[i].x)/2+xOffset, capPoint[i].y/2,3,3,greencolor,greencolor);
			}
		
			for(int r=0; r<23; r++)
			{
				pDC->Draw3dRect( (capPointR[r].x)/2+320, capPointR[r].y/2+260,3,3,greencolor,greencolor);
			}
			for(int k=0; k<23; k++)
			{
				pDC->Draw3dRect( (capPointL[k].x)/2, capPointL[k].y/2+260,3,3,greencolor,greencolor);
			}

		}
		else
		{
		
			pDC->Draw3dRect((CapLSide.x)/2+xOffset+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset/2,
				MidTop.y/2+theapp->jobinfo[pframe->CurrentJobNum].u2YOffset/2,
				(CapRSide.x-CapLSide.x)/2-theapp->jobinfo[pframe->CurrentJobNum].u2XOffset,
				U2Height/2,
				greencolor,
				greencolor);
			
			pDC->Draw3dRect( (RightU2Band.x/2)+320,
				theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR/2+TBYOffsetR/2+260,
				theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR/2,
				U2Height/2,
				greencolor,
				greencolor);

			pDC->Draw3dRect( (LeftU2Band.x/2)-theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR/2,
				theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR/2+TBYOffsetR/2+260,
				theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR/2,
				U2Height/2,
				greencolor,
				greencolor);

		}
	}

	//If camera alignment is to be done.
	if(theapp->showAlign==true)
	{
		//Draw camera alignment lines.
		pDC->SelectObject(&LRedPen);
		
		pDC->MoveTo( xOffset+256-250, 40);
		pDC->LineTo( xOffset+256+250, 40);

		pDC->MoveTo( xOffset+256-190, 40 );
		pDC->LineTo( xOffset+256-190, 150);
		pDC->MoveTo( xOffset+256+190, 40 );
		pDC->LineTo( xOffset+256+190, 150);

		pDC->SelectObject(&GrayPen);
		pDC->MoveTo( xOffset+256-220, 40 );
		pDC->LineTo( xOffset+256-220, 150);
		pDC->MoveTo( xOffset+256+220, 40 );
		pDC->LineTo( xOffset+256+220, 150);

		pDC->SelectObject(&BluePen);
		pDC->MoveTo( xOffset+256-130, 40 );
		pDC->LineTo( xOffset+256-130, 150);
		pDC->MoveTo( xOffset+256+130, 40 );
		pDC->LineTo( xOffset+256+130, 150);

		pDC->SelectObject(&BlackPen);
		pDC->MoveTo( xOffset+256-160, 40 );
		pDC->LineTo( xOffset+256-160, 150);
		pDC->MoveTo( xOffset+256+160, 40 );
		pDC->LineTo( xOffset+256+160, 150);

		pDC->SelectObject(&GreenPen);
		pDC->MoveTo( xOffset+256-250, 40 );
		pDC->LineTo( xOffset+256-250, 150);
		pDC->MoveTo( xOffset+256+250, 40 );
		pDC->LineTo( xOffset+256+250, 150);

		pDC->SetWindowOrg(0,-260);

		pDC->SelectObject(&LRedPen);
		
		pDC->MoveTo( 160-140, 18);
		pDC->LineTo( 160+140, 18);

		pDC->MoveTo( 160-60, 18 );
		pDC->LineTo( 160-60, 150);
		pDC->MoveTo( 160+60, 18 );
		pDC->LineTo( 160+60, 150);

		pDC->SelectObject(&GrayPen);
		pDC->MoveTo( 160-80, 18 );
		pDC->LineTo( 160-80, 150);
		pDC->MoveTo( 160+80, 18 );
		pDC->LineTo( 160+80, 150);


		pDC->SelectObject(&BluePen);
		pDC->MoveTo( 160-100, 18 );
		pDC->LineTo( 160-100, 150);
		pDC->MoveTo( 160+100, 18 );
		pDC->LineTo( 160+100, 150);

		pDC->SelectObject(&BlackPen);
		pDC->MoveTo( 160-120, 18 );
		pDC->LineTo( 160-120, 150);
		pDC->MoveTo( 160+120, 18 );
		pDC->LineTo( 160+120, 150);


		pDC->SelectObject(&GreenPen);
		pDC->MoveTo( 160-140, 18 );
		pDC->LineTo( 160-140, 150);
		pDC->MoveTo( 160+140, 18 );
		pDC->LineTo( 160+140, 150);

		pDC->SetWindowOrg(-320,-260);

		pDC->SelectObject(&LRedPen);
		
		pDC->MoveTo( 160-140, 18);
		pDC->LineTo( 160+140, 18);

		pDC->MoveTo( 160-60, 18 );
		pDC->LineTo( 160-60, 150);
		pDC->MoveTo( 160+60, 18 );
		pDC->LineTo( 160+60, 150);

		pDC->SelectObject(&GrayPen);
		pDC->MoveTo( 160-80, 18 );
		pDC->LineTo( 160-80, 150);
		pDC->MoveTo( 160+80, 18 );
		pDC->LineTo( 160+80, 150);


		pDC->SelectObject(&BluePen);
		pDC->MoveTo( 160-100, 18 );
		pDC->LineTo( 160-100, 150);
		pDC->MoveTo( 160+100, 18 );
		pDC->LineTo( 160+100, 150);

		pDC->SelectObject(&BlackPen);
		pDC->MoveTo( 160-120, 18 );
		pDC->LineTo( 160-120, 150);
		pDC->MoveTo( 160+120, 18 );
		pDC->LineTo( 160+120, 150);


		pDC->SelectObject(&GreenPen);
		pDC->MoveTo( 160-140, 18 );
		pDC->LineTo( 160-140, 150);
		pDC->MoveTo( 160+140, 18 );
		pDC->LineTo( 160+140, 150);

		pDC->SetWindowOrg(0,0);		
	}

	//Special overlays for diet coke.
	if(theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke == 1)
	{
		if(no_cap_detected==true)
		{
			pDC->Draw3dRect((no_cap_x/2)+xOffset, no_cap_y/2,6,6,greencolor,greencolor);
		}
		else if(no_cap_detected==false)
		{
			pDC->TextOut(xOffset+5, 25,"Cap Present");
			
		}
	}

	//Draw a white rectangle to the left and right of the camera 1 image.
	CRect lRect,rRect;
	lRect.SetRect(500+xOffset,0,500+xOffset+100,260);
	rRect.SetRect(0,0,xOffset,260);
	pDC->Rectangle(lRect);
	pDC->FillRect(lRect,&Colorw);
	pDC->Rectangle(rRect);
	pDC->FillRect(rRect,&Colorw);

	pframe->m_pview->UpdateDisplay();
}

//Teaches the inspection parameters and performs inspection.
//im_srcInC: color image of camera1
//im_srcInC2: color image of camera2
//im_srcInC3: color image of camera3
//im_srcIn: grayscale image of camera1.
//im_srcIn: grayscale image of camera2.
//im_srcIn: grayscale image of camera3.
void CXAxisView::Inspect(BYTE *im_srcInC, BYTE *im_srcInC2, BYTE *im_srcInC3, BYTE *im_srcIn,BYTE *im_srcIn2,BYTE *im_srcIn3,BYTE *im_srcInSob2,BYTE *im_srcInSob3,
		BYTE *im_srcInRed, BYTE *im_srcInRed2, BYTE *im_srcInRed3, BYTE* im_srcInHue, BYTE *im_srcInHue2, BYTE *im_srcInHue3)
{
	////////////////////////////////////////
	//ML- MEASURING THE TIME OF INSPECT ALONE
	QueryPerformanceFrequency(&Frequency0);
	QueryPerformanceCounter(&timeStart0);	//start measurement timer.

	////////////////////////////////////////

	int left=0;
	int right=0;
	
	CapLSide	=FindCapSide(im_srcIn,newMidCap,true);						//Find cap left side coordinate.
	CapRSide	=FindCapSide(im_srcIn,newMidCap,false);						//Find the right coordinates of the cap.
	
	ldist		=CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].lDist;		//Find x coordinate of left vertical cap line
	rdist		=CapRSide.x-theapp->jobinfo[pframe->CurrentJobNum].lDist;		//Find x coordinate of right vertical cap line


	CapLTop		=FindCapTop(im_srcIn,ldist,true);							//Find the left top coordinates of the cap.
	CapRTop		=FindCapTop(im_srcIn,rdist,false);							//Find the right top coordinates of the cap.
/*
	//##########logic for detecting broken band in aluminium bottles
	if(theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke == 1)
	{
		Check_Cap_presence(im_srcIn,CapLTop);
	}
	//
*/

	//*********logic to compensate for slanted cap //doesnt work so commenting it out Jun-2013
	int shiftTB=0;

/*	if(CapLTop.y>CapRTop.y+4) {CapLTop.y+=3;shiftTB=3;}
	else if(CapLTop.y>CapRTop.y+3){CapLTop.y+=2;shiftTB=2;}
		
		
	if(CapRTop.y>CapLTop.y+4)CapRTop.y+=3;
	else if(CapRTop.y>CapLTop.y+3)CapRTop.y+=2;
*/
	CapLSide	=FindCapSide(im_srcIn,CapLTop.y+60,true);		//Find the left cap side coordinates with an offset of 60 from top of cap.
	CapRSide	=FindCapSide(im_srcIn,CapRTop.y+60,false);		//Find the right cap side coordinates with an offset of 60 from top of cap.

	theapp->capWidth=CapRSide.x-CapLSide.x; //Compute cap width, Actual
	capWidthDiff	=learnedCapWidth-theapp->capWidth; // Learned - Actual

	//*************0.62 calculated from trial and error
	theapp->capWidth2=(learnedCapWidth-theapp->capWidth)*0.62; //compensating factor for lateral movement


	MidTop.x= float((CapRSide.x-CapLSide.x)/2)+CapLSide.x;	//Compute mid top cap coordinates.

	
	if(LearnDone==false)	//If inspection is being taught.
	{
		MidBar1024.y=theapp->jobinfo[pframe->CurrentJobNum].midBarY*2;	//Set the mid cap bar y coordinate

		CapLSide=FindCapSide(im_srcIn,MidBar1024.y,true);			//Find cap left side coordinate.
		CapRSide=FindCapSide(im_srcIn,MidBar1024.y,false);			//Find cap right side coordinate.

		ldist=CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].lDist;	//Find x coordinate of left vertical cap line
		rdist=CapRSide.x-theapp->jobinfo[pframe->CurrentJobNum].lDist;	//Find x coordinate of right vertical cap line

		CapLTop=FindCapTop(im_srcIn,ldist,true); 			//Find the left top coordinates of the cap.
		CapRTop=FindCapTop(im_srcIn,rdist,false);			//Find the right top coordinates of the cap.

		newMidCap=CapLTop.y+60;								//Set new mid cap y coordinate to be the y coordinate of the left top cap point plus an offset of 60.

		CapLSide=FindCapSide(im_srcIn,newMidCap,true);			//Find the left coordinates of the cap.
		CapRSide=FindCapSide(im_srcIn,newMidCap,false);			//Find the right coordinates of the cap.
		
		learnedCapWidth=CapRSide.x-CapLSide.x;					//Set the learned cap width to the difference between the left and 
																	//right cap side coordinates.
		//changed JP
		theapp->capWidth2=(learnedCapWidth-theapp->capWidth)*0.62; //compensating factor for lateral movement
		//NOT USED
		if(learnedCapWidth>0)
			theapp->inspRatio=30.0/learnedCapWidth; //dont think  this is being used anywhere.
		
		NeckLSide=FindNeckSide(im_srcIn,theapp->jobinfo[pframe->CurrentJobNum].neckBarY*2,CapLSide.x,true);			//Find left neck coordinates.
		NeckRSide=FindNeckSide(im_srcIn,theapp->jobinfo[pframe->CurrentJobNum].neckBarY*2,CapRSide.x,false);		//Find right neck coordinates.

		NeckOffsetX=NeckLSide.x-CapLSide.x;																		//X Delta between neck left side and cap left side.
		theapp->jobinfo[pframe->CurrentJobNum].neckOffset=NeckOffsetX;
		OriginalTBYOffset=(CapLTop.y+CapRTop.y)/2; //basically this is just OriginalTBYOffset=CapMtop.y		//Average of cap left top and cap right top y coordinates.
		pframe->SaveJob(pframe->CurrentJobNum); 															//Save learned parameters.
	}
	else  //If inspection is being performed.
	{
			
		currentCapWidth=CapRSide.x-CapLSide.x;							//Compute cap width.
		NeckOffsetX=theapp->jobinfo[pframe->CurrentJobNum].neckOffset;	//Get delta between neck left side and cap left side.
		TBYOffset=OriginalTBYOffset-(CapLTop.y+CapRTop.y)/2;  //   (OrigCapMTop.y - currCapMidTop.y)
		TBYOffset=-TBYOffset;
		
		TBYOffsetR=(float(TBYOffset-5)/0.7); //not sure about the 5..0.7 is conversion from Cam 1 co ordintes to Cam 2 and 3.
		if(TBYOffset<=0)TBYOffset=0;

		NeckLSide.y=NeckRSide.y=theapp->jobinfo[pframe->CurrentJobNum].neckBarY*2;
		NeckLSide.x=CapLSide.x+NeckOffsetX;
		NeckRSide.x=CapRSide.x-NeckOffsetX;
			
		switch(theapp->jobinfo[pframe->CurrentJobNum].lip)
		{
		case 0:  //Normal lip
			//Find left lip pattern coordinates
			NeckLLip   =FindNeckLipPat(im_srcIn, NeckLSide, true);
			NeckLLip.y-=LipDiffl;
			//Find right lip pattern coordinates
			NeckRLip   =FindNeckLipPat(im_srcIn, NeckRSide, false);
			NeckRLip.y-=LipDiffr;
			break;

		case 1:    //No lip
			//Find left lip pattern
			NeckLLip	=FindNeckLipPatUser(im_srcIn,
		    	theapp->jobinfo[pframe->CurrentJobNum].lipx*2+ldist,
			      theapp->jobinfo[pframe->CurrentJobNum].lipy*2+MidBar1024.y,
			     	true);
			//NeckLLip.y-=LipDiffl;	

			//If both left and right lip search boxes are to be used.
			if(theapp->jobinfo[pframe->CurrentJobNum].useLeft==false)
			{
				//Find right lip pattern
				NeckRLip=FindNeckLipPatUser(im_srcIn,
					rdist-theapp->jobinfo[pframe->CurrentJobNum].lipx*2,
					  theapp->jobinfo[pframe->CurrentJobNum].lipy*2+theapp->jobinfo[pframe->CurrentJobNum].lipYOffset*2+MidBar1024.y,
			     		false);
			}
			else //If only the left search box is to be used.
			{
				//Estimate coordinates of right lip pattern match.
					NeckRLip.x=NeckLLip.x+470;
				if(NeckRLip.x>=1000) NeckRLip.x=1000;
				NeckRLip.y=NeckLLip.y;
			}
			break;
	
		}

		//Distance between left lip and left top point of cap.
		Result1=sqrtf( (NeckLLip.y-CapLTop.y)*(NeckLLip.y-CapLTop.y) + (NeckLLip.x-CapLTop.x)*(NeckLLip.x-CapLTop.x) );	
		
		//Distance between right lip and right top point of cap.
		Result2=sqrtf( (NeckRLip.y-CapRTop.y)*(NeckRLip.y-CapRTop.y) + (NeckRLip.x-CapRTop.x)*(NeckRLip.x-CapRTop.x) );

		float NeckLipYMin=min(NeckLLip.y, NeckRLip.y);	//Find minimum y coordinate of left and right lip coordinates.
		
		Result1+=theapp->capWidth2;	//Add delta of learned and measured cap width to the distance between lip and top of cap.
		//Result1=Result1*theapp->inspRatio;
		
		Result2+=theapp->capWidth2;	//Add delta of learned and measured cap width to the distance between lip and top of cap.
		//Result2=Result2*theapp->inspRatio;
		
		//Normal Cap
		if(theapp->jobinfo[pframe->CurrentJobNum].sport==0) //Normal Cap
		{
			MidBottom.y= (abs(NeckRLip.y-NeckLLip.y)/2)+NeckLipYMin;	//Find y coordinate of highest lip
			MidTop.y =FindCapMid(im_srcIn, MidTop.x);					//Find y coordinate of the middle of the cap.

			Result3=MidBottom.y-MidTop.y;								//Delta between the cap top y coordinate and lip y coordinate.
			Result3+=theapp->capWidth2;
			//Result3=Result3*theapp->inspRatio;
		
		}
		//Sports cap
		else if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
		{
			sportBand=CheckSportBand2(im_srcIn, CapLTop, CapRTop);
			Result3=sportBand.x1-sportBand.x;

			Result3+=theapp->capWidth2;
			
			MidTop.y =CapLTop.y;
		}
		//Foil cap
		else if(theapp->jobinfo[pframe->CurrentJobNum].sport==2)
		{
			MidBottom.y= (abs(NeckRLip.y-NeckLLip.y)/2)+NeckLipYMin;
			MidTop.y =FindCapMid(im_srcIn, MidTop.x);

			midYResult=(MidBottom.y-MidTop.y)-20;
			if(Result1<=midYResult) Result1=midYResult;

			Result3=FoilWindow(im_srcIn);
		
		}

		//NOT USED
		//compare shade/color segments
		if(theapp->jobinfo[pframe->CurrentJobNum].tBandType==3 && theapp->teachFrontBand==true)
		{
			TeachBandFront(im_srcInC, CapLSide.x, CapLTop.y, CapRSide.x, CapRTop.y, TBOffset.x, TBOffset.y);
			TeachBandRear(im_srcIn2, im_srcInC2, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY+theapp->xtraYOffset), Band2Size, true);
		
			TeachBandRear(im_srcIn3, im_srcInC3, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY+theapp->xtraYOffset), Band2Size, false);
		

			theapp->teachFrontBand=false;
			//Result8=(RightBand.y+LeftBand.y + FrontBand.y)/3;
		}

		//cam1 surface
		FrontBand=CheckBand(im_srcIn,im_srcInC,  im_red, im_hue, CapLSide.x, CapLTop.y-theapp->capWidth2, CapRSide.x, CapRTop.y-theapp->capWidth2, TBOffset.x, TBOffset.y, theapp->jobinfo[pframe->CurrentJobNum].tBandType, theapp->jobinfo[pframe->CurrentJobNum].dark);
		Band2Size=theapp->jobinfo[pframe->CurrentJobNum].band2Size; //Set rear cameras tamper band search box sizes
	
		//cam2 surface tamper band

		//NOT USED
		posToShow=TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY+theapp->xtraYOffset);
		posToShow-=20; //This var is not being used anywhere..

		if(theapp->surfLR==true) //If camera 2 tamper band surface search is enabled.
		{
			//Get count of how many pixels are below/above threshold for cam 2.
			LeftBand=CheckBand2(im_srcIn2, im_srcInC2,im_srcInSob2, im_srcInRed2, im_srcInHue2, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY+theapp->xtraYOffset), Band2Size, theapp->jobinfo[pframe->CurrentJobNum].tBandType, theapp->jobinfo[pframe->CurrentJobNum].dark, true);
		
			if(theapp->checkRearHeight ==  true) //If cocked cap inspection is to be performed.
			{
					
					CaptopRearL.x=CaptopRearL.y=0;//theapp->h3=0;
			
					CaptopRearL=FindCapTopRear(im_srcIn2,RearL.x-25,true);	//Find top most point of cap on cam2.		//Use Neck Coord, find CapTop

					bool cocked=dist2top_cam2(im_srcIn2,CaptopRearL.x,CaptopRearL.y);      //Find if cap is cocked in cam 2.
		
				
			} //0.35
		}
		else
		{
			LeftBand.x=LeftBand.y=0; //Set count of how many pixels are below/above threshold to 0.
		}
			
		//cam3 surface tamper band
		
		if(theapp->surfRR==true)//If camera 3 tamper band surface search is enabled.
		{
			//Get count of how many pixels are below/above threshold for cam 3.
			RightBand=CheckBand2(im_srcIn3, im_srcInC3,im_srcInSob3, im_srcInRed3, im_srcInHue3, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY+theapp->xtraYOffset), Band2Size, theapp->jobinfo[pframe->CurrentJobNum].tBandType, theapp->jobinfo[pframe->CurrentJobNum].dark, false);
			//RightBand=CheckBand2(im_srcIn3, im_srcInC3,im_srcInSob3, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].rearCamY), Band2Size, theapp->jobinfo[pframe->CurrentJobNum].tBandType, theapp->jobinfo[pframe->CurrentJobNum].dark, false);

			if(theapp->checkRearHeight ==  true)	//If cocked cap inspection is to be performed.
			{
				CaptopRearR.x=CaptopRearR.y=0;//theapp->h4=0;
				//Cam3_neckpts=FindNeckLipPat_Rear(im_srcIn3, RearR, false);

				CaptopRearR=FindCapTopRear(im_srcIn3,RearR.x+25,false);					//Find top most point of cap on cam3.
				bool cocked_c3=dist2top_cam3(im_srcIn3,CaptopRearR.x,CaptopRearR.y);   //Find if cap is cocked in cam 2.	
					
			}

		}
		else
		{
			RightBand.x=RightBand.y=0;	 //Set count of how many pixels are below/above threshold to 0.
		}
		
		Result5=FrontBand.x;			//Transfer number of pixels beyond threshold for cam1 surface tamper band to result5.
		
		//Transfer number of pixels beyond threshold for cam2 or cam3, which ever is greatest, surface tamper band to result6.
		if(RightBand.y>=LeftBand.y)
		{
			Result6=RightBand.y;
			showPos=3;
		}
		else
		{
			Result6=LeftBand.y;showPos=2;
		}
		if(FrontBand.y>Result6){ Result6=FrontBand.y;showPos=1;}
	
		
		 //GetMax(3, RightBand.y,LeftBand.y ,FrontBand.y,0);

		//If extra light option for low fill is enabled.
		if(theapp->jobinfo[pframe->CurrentJobNum].xtraLight==true)
		{
			//ApplyFilter(im_srcInC, theapp->jobinfo[pframe->CurrentJobNum].fillArea);
	
			Result7=CheckFill2(im_reduce1,MidTop.x/2+(theapp->jobinfo[pframe->CurrentJobNum].fillX), (theapp->jobinfo[pframe->CurrentJobNum].fillY));
		
		}
		else  //If extra light option for low fill is disabled.
		{
			Result7=CheckFill(im_srcIn,MidTop.x+(theapp->jobinfo[pframe->CurrentJobNum].fillX)*2, (theapp->jobinfo[pframe->CurrentJobNum].fillY)*2);
		
		}
		//*************Color********************
		//If cap color inspection is enabled.
		if(theapp->inspctn[8].enable==true && pframe->reloading==false)
		{ 
			if(reteachColor==true)	//If cap color is being taught.
			{
				ColorTarget=DoUser(im_srcInC, MidTop.x+(theapp->jobinfo[pframe->CurrentJobNum].userX),MidTop.y+(theapp->jobinfo[pframe->CurrentJobNum].userY), (theapp->jobinfo[pframe->CurrentJobNum].userDx), theapp->jobinfo[pframe->CurrentJobNum].userStrength);
				reteachColor=false;		//Clear re-teach flag

				//Get taught RGB values.
				targetR=theapp->jobinfo[pframe->CurrentJobNum].colorTargR=GetRValue(ColorTarget);
				targetG=theapp->jobinfo[pframe->CurrentJobNum].colorTargG=GetGValue(ColorTarget);
				targetB=theapp->jobinfo[pframe->CurrentJobNum].colorTargB=GetBValue(ColorTarget);
				pframe->SaveJob(pframe->CurrentJobNum);
			//	if(Modify==true) pframe->m_pmod->UpdateDisplay();
			}
			else //If cap color is being looked for.
			{

				ColorResult=DoUser(im_srcInC, MidTop.x+(theapp->jobinfo[pframe->CurrentJobNum].userX),MidTop.y+(theapp->jobinfo[pframe->CurrentJobNum].userY), (theapp->jobinfo[pframe->CurrentJobNum].userDx), theapp->jobinfo[pframe->CurrentJobNum].userStrength);
				//Result7=UserResult.x;
				//Load taught RGB values.
				targetR=theapp->jobinfo[pframe->CurrentJobNum].colorTargR;
				targetG=theapp->jobinfo[pframe->CurrentJobNum].colorTargG;
				targetB=theapp->jobinfo[pframe->CurrentJobNum].colorTargB;
				
				//Get Found RGB values.
				resR=GetRValue(ColorResult);
				resG=GetGValue(ColorResult);
				resB=GetBValue(ColorResult);

				//Compare taught and found RGB values.
				Result8=sqrtf( fabs((double)((resB-targetB)*(resB-targetB))) + fabs((double)((resG-targetG)*(resG-targetG))) + fabs((double)((resR-targetR)*(resR-targetR))));
			}
		//if(Modify==true) pframe->m_pmod->UpdateDisplay();
		}
/*
		//###############FOIL SENSOR ##############

		foil_missing=false;//Variable to be used in PicRecall 
	
		//##If Foil Sensor is enabled
		if(theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor == 1)
		{
			//paramters to be passed to the routine
			int y_offset=theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar;

			int startx=850,endx=950; //over 100 pixels wide search are
			int starty=y_offset-15,endy=y_offset;
			int sum_total=0,count=0,pix1=0;
			bool result=false;
			
			if(starty>0 && starty<theapp->cam1Height && endy< theapp->cam1Height)
			{
				Foil_Sensor(im_srcIn, startx,endx,starty,endy,result);

			}

	
			if(result == true) 
			{
				Missing_Foil_Count++;
				foil_missing=true; 
			}
		}
		//##############################################
		*/
//????????????????????????????????????????????????????????????????????????????????????????

		//NOT USED
		if(theapp->jobinfo[pframe->CurrentJobNum].userType==0)//look for dark in overhead box
		{
			//user cam1
			FrontU2Band=CheckU2Band(im_srcIn, CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapLTop.y, CapRSide.x-theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapRTop.y, TBOffset.x, (theapp->jobinfo[pframe->CurrentJobNum].u2YOffset));
			//user cam2
			if(theapp->userLR==true)
			{
				LeftU2Band=CheckU2Band2(im_srcIn2, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR+theapp->xtraYOffset), theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR,0,true);
				//LeftU2Band=CheckU2Band2(im_srcIn2, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR), theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR,0,true);
			}
			else
			{
				LeftU2Band.x=LeftU2Band.y=0;
			}
				//user cam3
			
			if(theapp->userRR==true)
			{
				RightU2Band=CheckU2Band2(im_srcIn3, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR+theapp->xtraYOffset), theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR,0,false);
				//RightU2Band=CheckU2Band2(im_srcIn3, TBYOffsetR+(theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR), theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR,0,false);
			}
			else
			{
				RightU2Band.x=RightU2Band.y=0;
			}
			Result9=FrontU2Band.y;
			Result10=LeftU2Band.y+RightU2Band.y;
		
		}
		else//look for bump
		{
			if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)//sport cap
			{
				Result9=0;
				if(theapp->userLR==true)
				{
					left=CheckForBumpSport2(im_srcIn2,LeftBand.x, LeftBand.x,theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2,true);
				}
				else
				{
					left=0;
				}
				
				if(theapp->userRR==true)
				{
					right=CheckForBumpSport2(im_srcIn3,RightBand.x, RightBand.x,theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2,false);	
				}
				else
				{
					right=0;
				}
				if(right>=left) {Result10=right;}else{Result10=left;}
			}
			else
			{
				Result9=CheckForBump(im_srcIn,CapLTop.x, CapRTop.x,theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1);
				
				
				if(theapp->userLR==true)
				{
					left=CheckForBump2(im_srcIn2,LeftBand.x, LeftBand.x,theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2,true);
				}
				else
				{
					left=0;
				}
				
				if(theapp->userRR==true)
				{
					right=CheckForBump2(im_srcIn3,RightBand.x, RightBand.x,theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2,false);	
				}
				else
				{
					right=0;
				}

				if(right>=left) {Result10=right;}else{Result10=left;}
			}
			
		
		}


//????????????????????????????????????????????????????????????????????????????????????????


	}//learn Done

	//NOT USED, USER INSPECTION
	if( theapp->inspctn[10].enable==true){if(  Result10>= theapp->inspctn[10].lmin && Result10<= theapp->inspctn[10].lmax){inspResultOK[10]=true;}else{inspResultOK[10]=false;typofault=10;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[10]=true;fault10=false;}
	
	//NOT USED, USER INSPECTION
	if( theapp->inspctn[9 ].enable==true){if(  Result9>= theapp->inspctn[9].lmin && Result9<= theapp->inspctn[9].lmax){inspResultOK[9]=true;}else{inspResultOK[9]=false;typofault=9;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[9]=true;fault9=false;}
	
	//Color inspection
	if( theapp->inspctn[8 ].enable==true)	//If enabled.
	{
		if(  Result8>= theapp->inspctn[8].lmin && Result8<= theapp->inspctn[8].lmax)	//If inspection is within limits.
		{
			inspResultOK[8]=true; //record inspection as passed.
		}
		else // otherwise 
		{
			inspResultOK[8]=false;							//Record inspection as failed.
			typofault=8;									//Record type of inspection failed.	
			inspn_failed[count_inspn_failed]=typofault;		//Record type of inspection failed.	
			count_inspn_failed+=1;							//Increment index of failed inspection
		} 
	}
	else //if inspection is not enabled
	{
		inspResultOK[8]=true;	//record inspection as passed.
		fault8=false;			//record inspection as passed.
	}

	//Low fill.
	if( theapp->inspctn[7 ].enable==true){if(  Result7>= theapp->inspctn[7].lmin && Result7<= theapp->inspctn[7].lmax){inspResultOK[7]=true;}else{inspResultOK[7]=false;typofault=7;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[7]=true;fault7=false;}

	//Band surface.
	if( theapp->inspctn[6 ].enable==true){if(  Result6>= theapp->inspctn[6].lmin && Result6<= theapp->inspctn[6].lmax){inspResultOK[6]=true;}else{inspResultOK[6]=false;typofault=6;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[6]=true;fault6=false;}

	//Band edges.
	if( theapp->inspctn[5 ].enable==true){if(  Result5>= theapp->inspctn[5].lmin && Result5<= theapp->inspctn[5].lmax){inspResultOK[5]=true;}else{inspResultOK[5]=false;typofault=5;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[5]=true;fault5=false;}	

	//Cocked cap.
	if( theapp->inspctn[4 ].enable==true){if( (theapp->cam2diff >= theapp->inspctn[4].lmin) && (theapp->cam2diff <= theapp->inspctn[4].lmax) && (theapp->cam3diff >= theapp->inspctn[4].lmin) && (theapp->cam3diff <= theapp->inspctn[4].lmax)){inspResultOK[4]=true;}else{ inspResultOK[4]=false;typofault=4;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[4]=true;fault4=false;}

	//Left cap height.
	if( theapp->inspctn[1 ].enable==true)  
	{
		if(  Result1>= theapp->inspctn[1].lmin && Result1<= theapp->inspctn[1].lmax)
		{
			inspResultOK[1]=true;
		}
		else
		{
			inspResultOK[1]=false;
			typofault=1;
			
			inspn_failed[count_inspn_failed]=typofault; 
			count_inspn_failed+=1;

		}
	}
	else
	{
		inspResultOK[1]=true;
		fault1=false;
	}

	//Right cap height.
	if( theapp->inspctn[2].enable==true){if(  Result2>= theapp->inspctn[2].lmin && Result2<= theapp->inspctn[2].lmax){inspResultOK[2]=true;}else{ inspResultOK[2]=false;typofault=2;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[2]=true;fault2=false;}
	
	
	//Middle cap height
	if( theapp->inspctn[3].enable==true){if(  Result3>= theapp->inspctn[3].lmin && Result3<= theapp->inspctn[3].lmax){inspResultOK[3]=true;}else{inspResultOK[3]=false;typofault=3;inspn_failed[count_inspn_failed]=typofault;count_inspn_failed+=1;} }else{inspResultOK[3]=true;fault3=false;}	
	
	skipNext=false;		//NOT USED

	//If mid cap inspection is enabled.
	if( theapp->inspctn[3].enable==true)
	{
		if( Result3>= theapp->inspctn[3].lmin && Result3<= theapp->inspctn[3].lmax) //If inspection is within limits.
		{
			fault1=false; //record as not faulted
		}
		else
		{
			E3Cnt+=1; 												//Increment counter of mid cap height inspections failed.
			if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)		//If sports cap.
			{
				E4CntNet+=1;	//Increment total counter of of sports caps height inspections failed.
				E4Cntx+=1;		//Increment counter of of sports caps height inspections failed.
			}
			E3Cnt24+=1;			//Increment 24 hour counter of mid cap height inspections failed.
			fault1=true;
		} 
	}
	else
	{ 
		E3Cnt24=0;
	}

	//If mid cap height inspection did not fail.	
	if(fault1==false)
	{
		//If right cap height inspection is enabled.
		if( theapp->inspctn[2].enable==true)//cap r and l
		{
			if( Result2>= theapp->inspctn[2].lmin && Result2<= theapp->inspctn[2].lmax)	//If right cap height inspection is within limits.
			{
				fault2=false; //record inspection as not faulted
			}
			else //Otherwise
			{
				E2Cnt+=1;			//Increment counter of left cap height inspections failed.
				E2Cnt24+=1;			//Increment counter of left cap height inspections failed in 24 hours.
				fault2=true;		//Record inspection as not faulted
			} 
		}
		else	//If inspection is disabled clear its counter of number of inspections failed.
		{ 
			E2Cnt24=0;
		}
		
		//If left cap height inspection did not fail.
		if(fault2==false)
		{
			//If right cap height inspection is enabled.
			if( theapp->inspctn[1].enable==true)//cap r and l
			{if( Result1>= theapp->inspctn[1].lmin && Result1<= theapp->inspctn[1].lmax){fault3=false;}else{E1Cnt+=1;  E1Cnt24+=1;fault3=true;} }else{ E1Cnt24=0;}
		
			if(fault3==false)
			{
				//Band edges inspection
				if( theapp->inspctn[5].enable==true)
				{if(  Result5>= theapp->inspctn[5].lmin && Result5<= theapp->inspctn[5].lmax) {fault5=false;}else{E5Cnt+=1;E5Cntx+=1;E5Cnt24+=1;fault5=true;} }else{E5Cnt24=0;}
								
					
					if(fault5==false)
					{	
						//Band surface inspection
						if( theapp->inspctn[6].enable==true)
						{if(  Result6>= theapp->inspctn[6].lmin && Result6<= theapp->inspctn[6].lmax) {fault6=false;}else{E6Cnt+=1;E6Cntx+=1;E6Cnt24+=1;fault6=true;} }else{E6Cnt24=0;}
									
						
						if(fault6==false)
						{
							//Low fill inspection
							if( theapp->inspctn[7].enable==true) 
							{if(  Result7>= theapp->inspctn[7].lmin && Result7<= theapp->inspctn[7].lmax){fault7=false;}else{E7CntNet+=1;E7Cnt+=1;E7Cntx+=1;E7Cnt24+=1;fault7=true;} }else{E7Cnt24=0;}	
					
							if(fault7==false)
							{
								//Cap color inspection.
								if( theapp->inspctn[8].enable==true) 
								{if(  Result8>= theapp->inspctn[8].lmin && Result8<= theapp->inspctn[8].lmax){fault8=false;}else{E8Cnt+=1;E8Cntx+=1;E8Cnt24+=1;fault8=true;} }else{E8Cnt24=0;}
						
								
								if(fault8==false)
								{
									//User inspection
									if( theapp->inspctn[9].enable==true) 
									{if(  Result9>= theapp->inspctn[9].lmin && Result9<= theapp->inspctn[9].lmax){fault9=false;}else{E9Cnt+=1;E9Cntx+=1;E9Cnt24+=1;fault9=true;} }else{E9Cnt24=0;}	
			
										
									if(fault9==false)
									{	
										//User inspection
										if( theapp->inspctn[10].enable==true)
										{if(  Result10>= theapp->inspctn[10].lmin && Result10<= theapp->inspctn[10].lmax) {fault10=false;}else{E10Cnt+=1;E10Cntx+=1;E10Cnt24+=1;fault10=true;} }else{E10Cnt24=0;}
										if(fault10==false)
										{
										}

									}
								}	
							}
						}
					}//
			//	}
			}
		}
	}

	//If a cap height inspection failed.
	if(inspResultOK[1]==false || 	inspResultOK[2]==false || 	inspResultOK[3]==false  || inspResultOK[4]==false )
		
	{
			//If cap is missing.
			if(currentCapWidth<=(learnedCapWidth-theapp->jobinfo[pframe->CurrentJobNum].capWidthLimit) )
			{
				NoCapCnt+=1;	//If cap is missing.
				E1Cntx+=1;
				E1CntNet+=1;
			}
			else //If cap is cocked. 
				if( 
				(Result1>= theapp->inspctn[1].lmax && Result2<= theapp->inspctn[2].lmax)||
				(Result2>= theapp->inspctn[2].lmax && Result1<= theapp->inspctn[1].lmax) ||
				(theapp->cam2diff > theapp->inspctn[4].lmax) || (theapp->cam3diff >= theapp->inspctn[4].lmax)
				)
			{
				CockedCnt+=1; //Increment cocked cap counter.
				E2Cntx+=1;
				E2CntNet+=1;
			}
			else  //If cap is not cocked and not missing and the heights are faulty than it is a loose cap.
			{
				LooseCnt+=1;
				E3Cntx+=1;
				E3CntNet+=1;
			}
	
	}

/*

	///Only For Diet Coke ///Commenting it out ..doesn't look like it works very well and very often
	if(theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke == 1)
	{

		if(	no_cap_detected==true)
			{

			NoCapCnt+=1;
			inspResultOK[1]=false;
			inspResultOK[2]=false;
			inspResultOK[3]=false;
			typofault=11; // only for Diet Coke NO CAP scenario

			}

	}
*/
	//If all inspections passed.
	if(inspResultOK[1]==true && inspResultOK[2]==true && inspResultOK[3]==true && inspResultOK[4]==true  && inspResultOK[5]==true && inspResultOK[6]==true && inspResultOK[7]==true && inspResultOK[8]==true && inspResultOK[9]==true && inspResultOK[10]==true ) //&& (theapp->cam2diff<theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff) && (theapp->cam3diff < theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff)
	{
		BadCap=false;		//Clear BadCap flag.
		Fault="Good";
//		tot_bottle_cnt+=1;if(tot_bottle_cnt >=1000){tot_bottle_cnt=0;} //Only for testing in the plant
		
	}
	else //Otherwise
	{
		BadCap=true;		//Set BadCap flag.
		//tot_bottle_cnt+=1;if(tot_bottle_cnt >=1000){tot_bottle_cnt=0;} //only for testing in the plant
		if(inspResultOK[7]==true) // If low fill inspection passed
		{
			theapp->fillerEject=false; //don't eject because of filler.
		}
		else //If low fill failed
		{
			theapp->fillerEject=true;	//eject because of filler.
		}


////for the PICTURE RECALL option

		if((pframe->CurrentJobNum >=1)  )
		{
		
			if(showostore== 0)														//FLag which is set to 1 when the Dialog for teh pictures is Opened  ////// 0 otherwise
			{
				CVisRGBAByteImage im_FileBW3_cam1(theapp->cam1Width/2,theapp->cam1Height/2, 1, -1, imgBuf3RGB_cam1);
				CVisRGBAByteImage im_FileBW3_cam2(theapp->cam2Width/2,theapp->cam2Height/2, 1, -1, imgBuf3RGB_cam2);
				CVisRGBAByteImage im_FileBW3_cam3(theapp->cam3Width/2,theapp->cam3Height/2, 1, -1, imgBuf3RGB_cam3);

				imageC.CopyPixelsTo(im_FileBW3_cam1);
				imageC2.CopyPixelsTo(im_FileBW3_cam2);
				imageC3.CopyPixelsTo(im_FileBW3_cam3);

	
				/////introducing the time stamp 
				
				SYSTEMTIME sysTime;
				GetLocalTime(&sysTime);//char timeStr[255];

				//If Foil is found missing, only store Foil missing in Pic Recall
				//sport==2 implies cap type== Foil
				if(theapp->jobinfo[pframe->CurrentJobNum].sport==2 &&(inspResultOK[3]==false) )
				{
					
						inspn_failed[12]=typofault=12; //66 implies Foil missing
						*(imgBuf3RGB_cam1 + (4+(2*1)))=inspn_failed[12]; //12 is for Missing Foil
						*(imgBuf3RGB_cam1 +2)=(1);
					
				}

				else
				{
		
					//new way of storing all possible inspections failed for the current image
					for(int i=1;i<count_inspn_failed;i++)   // < coz count_inspn_failed is incre by 1 extra 
					{
						*(imgBuf3RGB_cam1 + (4+(2*i)))=inspn_failed[i]; //start storing from the 6th Byte..No specific reason..

																	 //count_inspn_failed-1 because var is incremented by 1in the end.
					}

		
					*(imgBuf3RGB_cam1 +2)=(count_inspn_failed-1); //storing the count for total no. of inspections failed
				}


				//Reset the count and the array contents
				count_inspn_failed=1;
				std::fill_n(inspn_failed,20,0); // resets array to 0
	
			
				
				//	*(imgBuf3RGB_cam1 )=typofault;//Inspection which failed
	
		
				tim_stamp[imge_no][0]=1;
				tim_stamp[imge_no][1]=sysTime.wHour; 
				tim_stamp[imge_no][2]=sysTime.wMinute; 
				tim_stamp[imge_no][3]=sysTime.wSecond; 
				tim_stamp[imge_no][4]=sysTime.wMilliseconds; //new addition

				imge_no++;
				////////////////////////////////////////////////

				// Camera 1 Pointer Increment
				if(imgBuf3RGB_cam1 != arr1[0]+(499*((theapp->cam1Width/2)*(theapp->cam1Height/2)*4)))  //1000 images Limit
				{	imgBuf3RGB_cam1+=((theapp->cam1Width/2)*(theapp->cam1Height/2)*4);}
				else
				{	imgBuf3RGB_cam1 = arr1[0];imge_no=0;
					for(int a=0;a<500;a++){tim_stamp[a][0]=0;}
									
				}			///Else Default to the 1st address. &&&& Default imge_no to 0

			
				// CAmera 2 Pointer Increment
				if(imgBuf3RGB_cam2 != arr1[1]+(499*((theapp->cam2Width/2)*(theapp->cam2Height/2)*4)))  //1000 images Limit
				{	imgBuf3RGB_cam2+=((theapp->cam2Width/2)*(theapp->cam2Height/2)*4);}
				
				else
				{	imgBuf3RGB_cam2 = arr1[1];		}



				// CAmera 3 Pointer Increment
				if(imgBuf3RGB_cam3 != arr1[2]+(499*((theapp->cam3Width/2)*(theapp->cam3Height/2)*4)))  //1000 images Limit
				{	imgBuf3RGB_cam3+=((theapp->cam3Width/2)*(theapp->cam3Height/2)*4);}
				else
				{	imgBuf3RGB_cam3=arr1[2];	}
			}
		}

		
	}

	//If reject all bottles is on, mark all bottles as having bad caps.
	if(RejectAll==true) BadCap=true;

	pframe->SendMessage(WM_INSPECT,NULL,NULL);		//See CMainFrame::Inspect(UINT, LONG)
	
	//Skip 1-5 is NOT USED.
	Skip=false;
	Skip2=false;
	Skip3=false;
	Skip4=false;
	Skip5=false;
	TimesThru+=1;
	if(TimesThru >=5) {StartLockOut=false; TimesThru=6;}

	if(StartLockOut==false)
	{
		//If left height inspection passed and is enabled. Compute the average and standard deviation for the left height values.
		if(inspResultOK[1]==true && theapp->inspctn[1].enable==true)
		{
			LHInc+=1;						//Increment left height array index.
			LHArray[LHInc]=Result1;			//Add the current left height value to the array.
			if(LHInc>=10)
			{ 
				LHInc=0; 					//Reset array index
				LHtf=(LHArray[1]+LHArray[2]+LHArray[3]+LHArray[4]+LHArray[5]+LHArray[6]+LHArray[7]+LHArray[8]+LHArray[9]+LHArray[10]);
				LHt=LHtf/10;		//Calculate the average of the last 10 left heights.
				
				LHsd=sqrt(   (
								( 
								 (LHArray[1] * LHArray[1])+
								 (LHArray[2] * LHArray[2])+
								 (LHArray[3] * LHArray[3])+
								 (LHArray[4] * LHArray[4])+
								 (LHArray[5] * LHArray[5])+
								 (LHArray[6] * LHArray[6])+
								 (LHArray[7] * LHArray[7])+
								 (LHArray[8] * LHArray[8])+
								 (LHArray[9] * LHArray[9])+
								 (LHArray[10] * LHArray[10])
															)/10
																)-(LHt*LHt) );//-LHti
			if(LHsd<=0) LHsd=0;	 //standard deviation cannot be less than 0.												
			}
		}

		//Compute right cap height statistics.
		if(inspResultOK[2]==true && theapp->inspctn[2].enable==true)
		{
			RHInc+=1;
			RHArray[RHInc]=Result2;
			if(RHInc>=10){ RHInc=0; RHt=(RHArray[1]+RHArray[2]+RHArray[3]+RHArray[4]+RHArray[5]+RHArray[6]+RHArray[7]+RHArray[8]+RHArray[9]+RHArray[10])/10;}
		}

		//Compute middle cap height statistics.
		if(inspResultOK[3]==true && theapp->inspctn[3].enable==true)
		{
			MHInc+=1;
			MHArray[MHInc]=Result3;
			if(MHInc>=10){ MHInc=0; MHt=(MHArray[1]+MHArray[2]+MHArray[3]+MHArray[4]+MHArray[5]+MHArray[6]+MHArray[7]+MHArray[8]+MHArray[9]+MHArray[10])/10;}
		}

		//Compute cocked cap statistics
		if(inspResultOK[4]==true && theapp->inspctn[4].enable==true)
		{
			DInc+=1;
			DArray[DInc]=Result4;
			if(DInc>=10)
			{ 
				DInc=0; 
				Dtf=(DArray[1]+DArray[2]+DArray[3]+DArray[4]+DArray[5]+DArray[6]+DArray[7]+DArray[8]+DArray[9]+DArray[10]);
				Dt=Dtf/10;
				
				Dsd=sqrt(   (
								( 
								 (DArray[1] * DArray[1])+
								 (DArray[2] * DArray[2])+
								 (DArray[3] * DArray[3])+
								 (DArray[4] * DArray[4])+
								 (DArray[5] * DArray[5])+
								 (DArray[6] * DArray[6])+
								 (DArray[7] * DArray[7])+
								 (DArray[8] * DArray[8])+
								 (DArray[9] * DArray[9])+
								 (DArray[10] * DArray[10])
															)/10
																)-(Dt*Dt) );//-LHti
				if(Dsd<=0) Dsd=0;													
			}
		}

		//Camera 1 tamper band surface statistics.
		if(inspResultOK[5]==true && theapp->inspctn[5].enable==true)
		{
			TBInc+=1;
			TBArray[TBInc]=Result5;
			if(TBInc>=10)
			{ 
				TBInc=0; 
				TBtf=(TBArray[1]+TBArray[2]+TBArray[3]+TBArray[4]+TBArray[5]+TBArray[6]+TBArray[7]+TBArray[8]+TBArray[9]+TBArray[10]);
				TBt=TBtf/10;
				TBsd=sqrt(   (
								( 
								 (TBArray[1] * TBArray[1])+
								 (TBArray[2] * TBArray[2])+
								 (TBArray[3] * TBArray[3])+
								 (TBArray[4] * TBArray[4])+
								 (TBArray[5] * TBArray[5])+
								 (TBArray[6] * TBArray[6])+
								 (TBArray[7] * TBArray[7])+
								 (TBArray[8] * TBArray[8])+
								 (TBArray[9] * TBArray[9])+
								 (TBArray[10] * TBArray[10])
															)/10
																)-(TBt*TBt) );//-LHti
				if(TBsd<=0) TBsd=0;													
			}
		}

		//Cameras 2 and 3 tamper band surface statistics.
		if(inspResultOK[6]==true && theapp->inspctn[6].enable==true)
		{
			TB2Inc+=1;
			TB2Array[TB2Inc]=Result6;
			if(TB2Inc>=10)
			{ 
				TB2Inc=0; 
				TB2tf=(TB2Array[1]+TB2Array[2]+TB2Array[3]+TB2Array[4]+TB2Array[5]+TB2Array[6]+TB2Array[7]+TB2Array[8]+TB2Array[9]+TB2Array[10]);
				TB2t=TB2tf/10;
				TB2sd=sqrt(   (
								( 
								 (TB2Array[1] * TB2Array[1])+
								 (TB2Array[2] * TB2Array[2])+
								 (TB2Array[3] * TB2Array[3])+
								 (TB2Array[4] * TB2Array[4])+
								 (TB2Array[5] * TB2Array[5])+
								 (TB2Array[6] * TB2Array[6])+
								 (TB2Array[7] * TB2Array[7])+
								 (TB2Array[8] * TB2Array[8])+
								 (TB2Array[9] * TB2Array[9])+
								 (TB2Array[10] * TB2Array[10])
															)/10
																)-(TB2t*TB2t) );//-LHti
				if(TB2sd<=0) TB2sd=0;													
			}
		
		
		}

		//Low fill statistics.
		if(inspResultOK[7]==true && theapp->inspctn[7].enable==true)
		{
			FInc+=1;
			FArray[FInc]=Result7;
			if(FInc>=10)
			{ 
				FInc=0; 
				Ftf=(FArray[1]+FArray[2]+FArray[3]+FArray[4]+FArray[5]+FArray[6]+FArray[7]+FArray[8]+FArray[9]+FArray[10]);
				Ft=Ftf/10;
				Fsd=sqrt(   (
								( 
								 (FArray[1] * FArray[1])+
								 (FArray[2] * FArray[2])+
								 (FArray[3] * FArray[3])+
								 (FArray[4] * FArray[4])+
								 (FArray[5] * FArray[5])+
								 (FArray[6] * FArray[6])+
								 (FArray[7] * FArray[7])+
								 (FArray[8] * FArray[8])+
								 (FArray[9] * FArray[9])+
								 (FArray[10] * FArray[10])
															)/10
																)-(Ft*Ft) );//-LHti
				if(Fsd<=0) Fsd=0;
			}
		}

		//Cap color statistics.
		if(inspResultOK[8]==true && theapp->inspctn[8].enable==true)
		{
			UInc+=1;
			UArray[UInc]=Result8;
			if(UInc>=10)
			{ 
				UInc=0; 
				Utf=(UArray[1]+UArray[2]+UArray[3]+UArray[4]+UArray[5]+UArray[6]+UArray[7]+UArray[8]+UArray[9]+UArray[10]);
				Ut=Utf/10;
				Usd=sqrt(   (
								( 
								 (UArray[1] * UArray[1])+
								 (UArray[2] * UArray[2])+
								 (UArray[3] * UArray[3])+
								 (UArray[4] * UArray[4])+
								 (UArray[5] * UArray[5])+
								 (UArray[6] * UArray[6])+
								 (UArray[7] * UArray[7])+
								 (UArray[8] * UArray[8])+
								 (UArray[9] * UArray[9])+
								 (UArray[10] *UArray[10])
															)/10
																)-(Ut*Ut) );//-LHti
				if(Usd<=0) Usd=0;
			}
		}
	}

	//////////////////////////////////////
	//ML- THIS ONLY INSIDE Inspect(...),ONCE DONE
	
	//If pictures dialog box is not open. Always executes this code.
	if(pframe->PicsOpen==false)
	{
		if(pframe->Freeze==false )	// If post new picture for every bottle. //|| BlackOK==false
		{
			InvalidateRect(NULL,false);
		}
		else  //Otherwise
		{
			if(pframe->FreezeReset==true) 
			{
				InvalidateRect(NULL,false);
			}
			
			if(BadCap==true) //if bottle cap is bad
			{
				InvalidateRect(NULL,false);  //post new picture and update display.
				pframe->FreezeReset=false;	 //Toggle flag to only post failed cap bottles.
			}
			else
			{ 
				pframe->m_pview->UpdateDisplay2();	//Update totals and rejected totals.
			}
		}
	}
	
	//////////////////////////////////////
	//ML- MEASURING THE TIME FOR INSPECT ALONE
	QueryPerformanceCounter(&timeEnd0);	//end measurement timer.
	
	//Convert to seconds
	if(timeStart0.QuadPart!=0 && timeEnd0.QuadPart!=0)
	{
		spanElapsed0	= (double)(Frequency0.QuadPart/1000); 
		nDiff0 = timeEnd0.QuadPart-timeStart0.QuadPart;	
		spanElapsed0 = nDiff0/spanElapsed0;
	}

	//////////////////////////////////////

	first=true;	
	
	//////////////////////////////////////
		
}

//User Inspection, NOT USED
SinglePoint CXAxisView::CheckU2Band(BYTE *im_srcHV, int x,int y,int x1,int y1, int TBx, int TBy)
{								//CapLSide.x,CapLTop.y,CapRSide.x,CapRTop.y,TBOffset.x,TBOffset.y
	//CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapLTop.y, CapRSide.x-+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapRTop.y, TBOffset.x, (theapp->jobinfo[pframe->CurrentJobNum].u2YOffset
	
	SinglePoint result;
	result.x=result.y=0;
	int pixa=0;
	
	int LineLength=1024;
	int lx=0;
	int ly=0;
	int ypos=0;
	int xpos=0;
	int i=0;
	int j=0;
	int d1=0;
	int count=0;
	int row=0;
	int dark=70;
	int avgShade=0;
	int lowLim, highLim;
	int pixb,pixc,pixd,pixe,pixf,pixg,pixh,pixi,pixj,pixk,pixl,pixm,pixn,pixo,pixp,pixq,pixr;
	lowLim=2*(100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1);
	if (lowLim>=200) lowLim=1000;

	lx=(x+10);
	rx=(x1-10);

	ly=TBy+abs((y+y1)/2);
	if(ly<=1) ly=1;


		if(lx>=0 && lx < 1000 && rx <=1000 && (ly+U2Height+cam1y) <= theapp->cam1Height-10 && ly >=1)
		{		
			for(i=ly; i<= ly+U2Height; i++)
			{
				for(j=lx; j<= rx; j++)
				{
						//pixm=*(im_srcHV+j-2+(LineLength * (i-1))); pixa=*(im_srcHV+j+(LineLength * (i-1)));  pixg=*(im_srcHV+j+2+(LineLength * (i-1)));	
						//pixn=*(im_srcHV+j-2+(LineLength * (i))); pixb=*(im_srcHV+j+(LineLength * (i)));    pixh=*(im_srcHV+j+2+(LineLength * (i)));
						//pixo=*(im_srcHV+j-2+(LineLength * (i+1))); pixc=*(im_srcHV+j+(LineLength * (i+1)));  pixi=*(im_srcHV+j+2+(LineLength * (i+1)));
						//pixp=*(im_srcHV+j-2+(LineLength * (i+2))); pixd=*(im_srcHV+j+(LineLength * (i+2)));  pixj=*(im_srcHV+j+2+(LineLength * (i+2)));	
						//pixq=*(im_srcHV+j-2+(LineLength * (i+3))); pixe=*(im_srcHV+j+(LineLength * (i+3)));  pixk=*(im_srcHV+j+2+(LineLength * (i+3)));
						//pixr=*(im_srcHV+j-2+(LineLength * (i+4))); pixf=*(im_srcHV+j+(LineLength * (i+4)));  pixl=*(im_srcHV+j+2+(LineLength * (i+4)));
						pixa=*(im_srcHV+j-1+(LineLength * (i-1)));  pixd=*(im_srcHV+j+2+(LineLength * (i-1)));
						pixb=*(im_srcHV+j-1+(LineLength * (i)));    pixe=*(im_srcHV+j+2+(LineLength * (i))); 
						pixc=*(im_srcHV+j-1+(LineLength * (i+1)));  pixf=*(im_srcHV+j+2+(LineLength * (i+1)));
						d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
														
						if(d1>=lowLim)
							 //(pixa<=lowLim && pixb<=lowLim && pixc<=lowLim) && (pixd<=lowLim && pixe<=lowLim && pixf<=lowLim) &&
							 //( (pixg>=lowLim && pixh>=lowLim && pixi>=lowLim) && (pixj>=lowLim && pixk>=lowLim && pixl>=lowLim) || (pixm>=lowLim && pixn>=lowLim && pixo>=lowLim) && (pixp>=lowLim && pixq>=lowLim && pixr>=lowLim))
							 // ||
							 //(pixa>=highLim && pixb>=highLim && pixc>=highLim) || (pixd>=highLim && pixe>=highLim && pixf>=highLim)   
							 
						  
						{			
							count+=1;		
							result.y=count;					
						}
					
				}
			
			}
		}
	
	
		
	
	return result;
}

//User inspection, NOT USED
int CXAxisView::CheckForBump(BYTE *im_srcHV, int x,int x2, int limit)
{								//CapLSide.x,CapLTop.y,CapRSide.x,CapRTop.y,TBOffset.x,TBOffset.y
	//CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapLTop.y, CapRSide.x-+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapRTop.y, TBOffset.x, (theapp->jobinfo[pframe->CurrentJobNum].u2YOffset
	
	int result;
	result=0;
	int pixa=0;
	
	int LineLength=1024;
	int lx=0;
	int ly=0;
	int xstart=x;
	int xend=x2;
	int i=0;
	int j=0;
	int d1=0;
	capPointCount=0;
	//int count=0;
	int avgY=0;
	int smallAvg=0;
	limit=limit*2;
	
	int pixb,pixc,pixd,pixe,pixf,pixg,pixh,pixi,pixj,pixk,pixl,pixm,pixn,pixo,pixp,pixq,pixr;
	
	
		if(xstart>=10 && xend < 1000)
		{
			for(j=xstart; j<= xend; j+=16)
			{
				for(i=30; i<= 300; i++)	
				{
						
					pixa=*(im_srcHV+j-1+(LineLength * (i-2)));  
					pixb=*(im_srcHV+j+(LineLength * (i-2)));     
					pixc=*(im_srcHV+j+1+(LineLength * (i-2)));  

					pixd=*(im_srcHV+j-1+(LineLength * (i+2)));
					pixe=*(im_srcHV+j+(LineLength * (i+2)));
					pixf=*(im_srcHV+j+1+(LineLength * (i+2)));
					
					d1=abs( (pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc) );
															
					if(d1>=limit)
					{			
						capPoint[capPointCount].x=j;		
						capPoint[capPointCount].y=i;
						avgY+=capPoint[capPointCount].y;
						i= 300;
						capPointCount+=1;
						if(capPointCount>50){ capPointCount=0; j= rx;}
					}
				}
			}
		}
		else
		{
			for(int x=0; x<=30; x++)
			{
			
				capPoint[x].y=0;
				capPoint[x].x=0;
			}

		}

		if(capPointCount>0)avgY=avgY/capPointCount;
		int largest=0;
		int cnt=0;

		for(int e=0; e<capPointCount; e++)
		{
			
			//if( largest<=abs(capPoint[e].y-avgY) ) largest=abs(capPoint[e].y-avgY);
			if( capPoint[e].y>=avgY)
			{
				smallAvg+=capPoint[e].y;
				cnt+=1;
			}
			
		}
		if(cnt>0) smallAvg=smallAvg/cnt;

		for(int g=0; g<capPointCount; g++)
		{
			
			if( largest<=(smallAvg-capPoint[g].y) ) largest=(smallAvg-capPoint[g].y);
			
		}
		
		result=largest;
	if(limit==0) result=0;
	return result;
}

//User inspection, NOT USED
int CXAxisView::CheckForBump2(BYTE *im_srcHV3, int x,int x2, int limit,int left)
{								//CapLSide.x,CapLTop.y,CapRSide.x,CapRTop.y,TBOffset.x,TBOffset.y
	//CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapLTop.y, CapRSide.x-+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapRTop.y, TBOffset.x, (theapp->jobinfo[pframe->CurrentJobNum].u2YOffset
	
	int result;
	result=0;
	int pixa=0;
	
	int LineLength=640;
	int lx=0;
	int ly=0;
	int xstart=x;
	int xend=x;
	int i=0;
	int j=0;
	int d1=0;
	capPointCountR=0;
	capPointCountL=0;
	//int count=0;
	int avgY=0;
	int smallAvg=0;
	limit=limit*2;
	int largest=0;
	int pixb,pixc,pixd,pixe,pixf;
	
	if(left==false)
	{
		for(int m=100; m<540;m++)
		{
			pixa=*(im_srcHV3+m-1+(LineLength * (3)));  
			pixb=*(im_srcHV3+m-1+(LineLength * (4)));     
			pixc=*(im_srcHV3+m-1+(LineLength * (5)));  

			pixd=*(im_srcHV3+m+2+(LineLength * (3)));
			pixe=*(im_srcHV3+m+2+(LineLength * (4)));
			pixf=*(im_srcHV3+m+2+(LineLength * (5)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

			if(d1>=80)
			{
				xend=m;
				m=640;
			}
		}

			for(int x=0; x<=30; x++)
			{
			
				capPointR[x].y=0;
				capPointR[x].x=0;
			}

		if(xstart>=50 && xend <= 540)
		{
			for(j=xstart+10; j<= xend-6; j+=12)
			{
				for(i=10; i<= 150; i++)	
				{
						
					pixa=*(im_srcHV3+j-1+(LineLength * (i-2)));  
					pixb=*(im_srcHV3+j+(LineLength * (i-2)));     
					pixc=*(im_srcHV3+j+1+(LineLength * (i-2)));  

					pixd=*(im_srcHV3+j-1+(LineLength * (i+2)));
					pixe=*(im_srcHV3+j+(LineLength * (i+2)));
					pixf=*(im_srcHV3+j+1+(LineLength * (i+2)));
					
					d1=abs( (pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc) );
															
					if(d1>=limit)
					{			
						capPointR[capPointCountR].x=j;		
						capPointR[capPointCountR].y=i;
						avgY+=capPointR[capPointCountR].y;
						i= 150;
						capPointCountR+=1;
						if(capPointCountR>22){ j= xend;}
					}
				}
			}
		}
		

		if(capPointCountR>0)avgY=avgY/capPointCountR;
		
		int cnt=0;

		for(int e=0; e<capPointCountR; e++)
		{
			
			//if( largest<=abs(capPoint[e].y-avgY) ) largest=abs(capPoint[e].y-avgY);
			if( capPointR[e].y>=avgY)
			{
				smallAvg+=capPointR[e].y;
				cnt+=1;
			}
			
		}
		if(cnt>0) smallAvg=smallAvg/cnt;

		for(int g=0; g<capPointCountR; g++)
		{
			
			if( largest<=(smallAvg-capPointR[g].y) ) largest=(smallAvg-capPointR[g].y);
			
		}
	}
	else
	{

		for(int m=540; m>100;m--)
		{
			pixa=*(im_srcHV3+m-1+(LineLength * (3)));  
			pixb=*(im_srcHV3+m-1+(LineLength * (4)));     
			pixc=*(im_srcHV3+m-1+(LineLength * (5)));  

			pixd=*(im_srcHV3+m+2+(LineLength * (3)));
			pixe=*(im_srcHV3+m+2+(LineLength * (4)));
			pixf=*(im_srcHV3+m+2+(LineLength * (5)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

			if(d1>=80)
			{
				xend=m;
				//if(xend<=
				m=100;
			}
		}

			for(int x=0; x<=30; x++)
			{
			
				capPointL[x].y=0;
				capPointL[x].x=0;
			}

		

		if(xstart<=600 && xend > 100)
		{
			for(j=xstart-25; j> xend+6; j-=12)//
			{
				for(i=10; i<= 150; i++)	
				{
						
					pixa=*(im_srcHV3+j-1+(LineLength * (i-2)));  
					pixb=*(im_srcHV3+j+(LineLength * (i-2)));     
					pixc=*(im_srcHV3+j+1+(LineLength * (i-2)));  

					pixd=*(im_srcHV3+j-1+(LineLength * (i+2)));
					pixe=*(im_srcHV3+j+(LineLength * (i+2)));
					pixf=*(im_srcHV3+j+1+(LineLength * (i+2)));
					
					d1=abs( (pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc) );
															
					if(d1>=limit)
					{			
						capPointL[capPointCountL].x=j;		
						capPointL[capPointCountL].y=i;
						avgY+=capPointL[capPointCountL].y;
						i= 150;
						capPointCountL+=1;
						if(capPointCountL>22){  j= xend;}
					}
				}
			}
		}
		

		if(capPointCountL>0)avgY=avgY/capPointCountL;
		
		int cnt=0;

		for(int e=0; e<capPointCountL; e++)
		{
			
			//if( largest<=abs(capPoint[e].y-avgY) ) largest=abs(capPoint[e].y-avgY);
			if( capPointL[e].y>=avgY)
			{
				smallAvg+=capPointL[e].y;
				cnt+=1;
			}
			
		}
		if(cnt>0) smallAvg=smallAvg/cnt;

		for(int g=0; g<capPointCountL; g++)
		{
			
			if( largest<=(smallAvg-capPointL[g].y) ) largest=(smallAvg-capPointL[g].y);
			
		}
	}
		
		result=largest;
	if(limit==0) result=0;
	return result;
}

//User inspection, NOT USED
int CXAxisView::CheckForBumpSport2(BYTE *im_srcHV3, int x,int x2, int limit,int left)
{								//CapLSide.x,CapLTop.y,CapRSide.x,CapRTop.y,TBOffset.x,TBOffset.y
	//CapLSide.x+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapLTop.y, CapRSide.x-+theapp->jobinfo[pframe->CurrentJobNum].u2XOffset, CapRTop.y, TBOffset.x, (theapp->jobinfo[pframe->CurrentJobNum].u2YOffset
	
	int result;
	result=0;
	int pixa=0;
	
	int LineLength=640;
	int lx=0;
	int ly=0;
	int xstart=x;
	int xend=x;
	int i=0;
	int j=0;
	int d1=0;
	capPointCountR=0;
	capPointCountL=0;
	//int count=0;
	int avgY=0;
	int smallAvg=0;
	limit=limit*2;
	int largest=0;
	int pixb,pixc,pixd,pixe,pixf;
	
	if(left==false)
	{
		for(int m=100; m<400;m++)
		{
			pixa=*(im_srcHV3+m-1+(LineLength * (3)));  
			pixb=*(im_srcHV3+m-1+(LineLength * (4)));     
			pixc=*(im_srcHV3+m-1+(LineLength * (5)));  

			pixd=*(im_srcHV3+m+2+(LineLength * (3)));
			pixe=*(im_srcHV3+m+2+(LineLength * (4)));
			pixf=*(im_srcHV3+m+2+(LineLength * (5)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

			if(d1>=80)
			{
				xend=m;
				m=640;
			}
		}

			for(int x=0; x<=30; x++)
			{
			
				capPointR[x].y=0;
				capPointR[x].x=0;
			}

		if(xstart>=50 && xend < 400)
		{
			for(j=xstart+4; j<= xstart+8; j++)
			{
				for(i=10; i<= 150; i++)	
				{
						
					pixa=*(im_srcHV3+j-1+(LineLength * (i-2)));  
					pixb=*(im_srcHV3+j+(LineLength * (i-2)));     
					pixc=*(im_srcHV3+j+1+(LineLength * (i-2)));  

					pixd=*(im_srcHV3+j-1+(LineLength * (i+2)));
					pixe=*(im_srcHV3+j+(LineLength * (i+2)));
					pixf=*(im_srcHV3+j+1+(LineLength * (i+2)));
					
					d1=abs( (pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc) );
															
					if(d1>=limit)
					{			
						capPointR[capPointCountR].x=j;		
						capPointR[capPointCountR].y=i;
						avgY+=capPointR[capPointCountR].y;
						i= 150;
						capPointCountR+=1;
						if(capPointCountR>50){ capPointCountR=0; j= xstart+8;}
					}
				}
			}
		}
		

		if(capPointCountR>0)avgY=avgY/capPointCountR;
		
		int cnt=0;

		for(int e=0; e<capPointCountR; e++)
		{
			
			//if( largest<=abs(capPoint[e].y-avgY) ) largest=abs(capPoint[e].y-avgY);
			if( capPointR[e].y>=avgY)
			{
				smallAvg+=capPointR[e].y;
				cnt+=1;
			}
			
		}
		if(cnt>0) smallAvg=smallAvg/cnt;

		for(int g=0; g<capPointCountR; g++)
		{
			
			if( largest<=(smallAvg-capPointR[g].y) ) largest=(smallAvg-capPointR[g].y);
			
		}
	}
	else
	{

		for(int m=540; m>200;m--)
		{
			pixa=*(im_srcHV3+m-1+(LineLength * (3)));  
			pixb=*(im_srcHV3+m-1+(LineLength * (4)));     
			pixc=*(im_srcHV3+m-1+(LineLength * (5)));  

			pixd=*(im_srcHV3+m+2+(LineLength * (3)));
			pixe=*(im_srcHV3+m+2+(LineLength * (4)));
			pixf=*(im_srcHV3+m+2+(LineLength * (5)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

			if(d1>=80)
			{
				xend=m;
				m=200;
			}
		}

			for(int x=0; x<=30; x++)
			{
			
				capPointL[x].y=0;
				capPointL[x].x=0;
			}

		

		if(xstart<=600 && xend > 200)
		{
			for(j=xstart-4; j> xstart-8; j--)
			{
				for(i=10; i<= 150; i++)	
				{
						
					pixa=*(im_srcHV3+j-1+(LineLength * (i-2)));  
					pixb=*(im_srcHV3+j+(LineLength * (i-2)));     
					pixc=*(im_srcHV3+j+1+(LineLength * (i-2)));  

					pixd=*(im_srcHV3+j-1+(LineLength * (i+2)));
					pixe=*(im_srcHV3+j+(LineLength * (i+2)));
					pixf=*(im_srcHV3+j+1+(LineLength * (i+2)));
					
					d1=abs( (pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc) );
															
					if(d1>=limit)
					{			
						capPointL[capPointCountL].x=j;		
						capPointL[capPointCountL].y=i;
						avgY+=capPointL[capPointCountL].y;
						i= 150;
						capPointCountL+=1;
						if(capPointCountL>50){ capPointCountL=0; j= xstart-8;}
					}
				}
			}
		}
		

		if(capPointCountL>0)avgY=avgY/capPointCountL;
		
		int cnt=0;

		for(int e=0; e<capPointCountL; e++)
		{
			
			//if( largest<=abs(capPoint[e].y-avgY) ) largest=abs(capPoint[e].y-avgY);
			if( capPointL[e].y>=avgY)
			{
				smallAvg+=capPointL[e].y;
				cnt+=1;
			}
			
		}
		if(cnt>0) smallAvg=smallAvg/cnt;

		for(int g=0; g<capPointCountL; g++)
		{
			
			if( largest<=(smallAvg-capPointL[g].y) ) largest=(smallAvg-capPointL[g].y);
			
		}
	}
		
		result=largest;
	
	return result;
}

//NOT USED
bool CXAxisView::CheckForBlack(BYTE *im_srcIn, int filly)
{
	bool result;
	result=true;
	
	int pixa=0;
	int LineLength=1024;
	
	int ypos=0;
	int xpos=0;
	int count=0;
	int avg=0;
	int store=0;

	if(filly<=theapp->cam1Height-30 && filly>=0)
	{
		for (ypos=filly; ypos < filly+10; ypos++)
		{
			for (xpos=30; xpos < 40; xpos++)
			{
				pixa=*(im_srcIn + xpos + (LineLength * (ypos)));
				
				store+=pixa;
				count+=1;
			}		
		}
	}
	
 
	if(count>=1) avg=store/count;
	if(avg<=70){ result=false; }else{ result=true;}


	return result;
}


//Used to get cap color.
//im_srcIn: source color image.
//x: x coordinate from which to start cap color inspection.
//y: y coordinate from which to start cap color inspection.
//width: cap color search box width
//UserStrength: NOT USED
COLORREF CXAxisView::DoUser(BYTE *im_srcIn, int x, int y, int width, int UserStrength)
{
	COLORREF resultColor;
	
	int pixB, pixR, pixG;
	int	totR=0;
	int	totB=0;
	int	totG=0;
	int	count=0;
	int LineLength=1024*4;
	
	int ypos=0;
	int xpos=0;
	int xwidth=width*4;
	int yheight=width;

/*	if(x % 4 !=0)  
	{
		x+=1;
		if(x % 4 !=0)
		{
			x+=1;
			if(x % 4 !=0)
			{
				x+=1;
			}
		}
	}
*/

	if(x>=0 && x<=theapp->cam1Width-30 && y>=0 && y <=theapp->cam1Height-30)
	{
		for(ypos=y; ypos<=yheight+y; ypos++)
		{
			for(xpos=x*4; xpos<=xwidth+(x*4); xpos+=4)
			{	
				pixB=*(im_srcIn+xpos+(LineLength*ypos));
				pixG=*(im_srcIn+xpos+1+(LineLength*ypos));
				pixR=*(im_srcIn+xpos+2+(LineLength*ypos));
						
				count+=1;
				totR+=pixR;
				totG+=pixG;
				totB+=pixB;
			}
		}
					if(count>0)
					{
						totB=(totB/count);
						totR=(totR/count);
						totG=(totG/count);
						resultColor = RGB(totR,totG,totB);
					}

					
		
	}

	return resultColor;
}

//Save an image of the taught lip pattern for a normal cap type.
//im_srcIn: Source image, grayscale.
//left: left = true save lip pattern for left lip, left = false save lip pattern for right lip
BYTE* CXAxisView::LearnPattern(BYTE *im_srcIn, bool left)
{
	int ypos=0;
	int ypos2=0;
	int xpos=0;
	int xpos2=0;
	int xpos3=0;
		
	int LineLength=1024;
	int LineLength2=60*4;
	int LineLength3=60;
	int cnt=0;

	//im_patl=(BYTE*)GlobalAlloc(GMEM_FIXED, 40 * 40 *4 );
	//im_patr=(BYTE*)GlobalAlloc(GMEM_FIXED, 40 * 40 *4 );

	if(NeckLSide.x > 30 && NeckLSide.x <1000 && (theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2 > 40 )
	{
		if(left==true)
		{
			for(ypos=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2-39; ypos<=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2; ypos++)
			{
				for(xpos=NeckLSide.x-49;xpos<=NeckLSide.x+10;xpos++)
				{	
					//for comparison later
					*(im_matchl + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					//only for show
					*(im_patl + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			SaveLipPattern(im_matchl,true);
		
		}
		else
		{
			for(ypos=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2-39; ypos<=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2; ypos++)
			{
				for(xpos=NeckRSide.x-10;xpos<=NeckRSide.x+49;xpos++)
				{	
					//for comparison later
					*(im_matchr + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					//only for show
					*(im_patr + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			SaveLipPattern(im_matchr,false);
		}

	}
	LearnDone=true;
	return im_pat;
}

//NOT USED
BYTE* CXAxisView::LearnPatternNoLip(BYTE *im_srcIn, bool left)
{
	int ypos=0;
	int ypos2=0;
	int xpos=0;
	int xpos2=0;
	int xpos3=0;
		
	int LineLength=1024;
	int LineLength2=60*4;
	int LineLength3=60;
	int cnt=0;

	//im_patl=(BYTE*)GlobalAlloc(GMEM_FIXED, 40 * 40 *4 );
	//im_patr=(BYTE*)GlobalAlloc(GMEM_FIXED, 40 * 40 *4 );

	if(NeckLSide.x > 30 && NeckLSide.x <1000 && (theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2 > 40 )
	{
		if(left==true)
		{
			for(ypos=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2-39+10; ypos<=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2+10; ypos++)
			{
				for(xpos=NeckLSide.x-29;xpos<=NeckLSide.x+30;xpos++)
				{	
					//for comparison later
					*(im_matchl + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					//only for show
					*(im_patl + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			//SavePattern(im_matchl,true);
		
		}
		else
		{
			for(ypos=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2-39+10; ypos<=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2+10; ypos++)
			{
				for(xpos=NeckRSide.x-30;xpos<=NeckRSide.x+29;xpos++)
				{	
					//for comparison later
					*(im_matchr + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					//only for show
					*(im_patr + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			//SavePattern(im_matchr,false);
		}

	}
	LearnDone=true;
	return im_pat;
}

//Save an image of the taught lip pattern for a no lip bottle cap.
//im_srcIn: Source image, grayscale.
//left: left = true save lip pattern for left lip, left = false save lip pattern for right lip
BYTE* CXAxisView::LearnPatternUser(BYTE *im_srcIn, bool left)
{
	int ypos=0;
	int ypos2=0;
	int xpos=0;
	int xpos2=0;
	int xpos3=0;
	int ySize=theapp->jobinfo[pframe->CurrentJobNum].lipSize;
	
	int xposition=(theapp->jobinfo[pframe->CurrentJobNum].lipx*2+ldist);
	int yposition=(theapp->jobinfo[pframe->CurrentJobNum].lipy*2+theapp->jobinfo[pframe->CurrentJobNum].midBarY*2);
	if(left==false)
	{
		xposition=(rdist-theapp->jobinfo[pframe->CurrentJobNum].lipx*2);
		yposition=(theapp->jobinfo[pframe->CurrentJobNum].lipy*2+theapp->jobinfo[pframe->CurrentJobNum].lipYOffset*2+theapp->jobinfo[pframe->CurrentJobNum].midBarY*2);
	}
	
	int LineLength=1024;
	int LineLength2=60*4;
	int LineLength3=60;
	int cnt=0;

	//im_patl=(BYTE*)GlobalAlloc(GMEM_FIXED, 40 * 40 *4 );
	//im_patr=(BYTE*)GlobalAlloc(GMEM_FIXED, 40 * 40 *4 );

	if( xposition > 30 && xposition <1000 && yposition > 0 )
	{
		if(left==true)
		{
			//cleanup
			for(ypos=yposition; ypos<=yposition+39; ypos++)
			{
				for(xpos=xposition-30;xpos<=xposition+29;xpos++)
				{	
					
					*(im_patl + xpos2 + LineLength2 *(ypos2))= 255;
					*(im_patl + xpos2 +1 + LineLength2 *(ypos2))=255;
					*(im_patl + xpos2 +2 + LineLength2 *(ypos2))=255;
					xpos2+=4;
					xpos3+=1;
					
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			xpos3=0;
			xpos2=0;
			ypos2=0;

			for(ypos=yposition; ypos<=yposition+39+ySize; ypos++)
			{
				for(xpos=xposition-30;xpos<=xposition+29;xpos++)
				{	
					//for comparison later
					*(im_matchl + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					//only for show
					*(im_patl + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			//SavePattern(im_matchl,true);
		
		}
		else
		{

			for(ypos=yposition; ypos<=yposition+39; ypos++)
			{
				for(xpos=xposition-29;xpos<=xposition+30;xpos++)
				{	
					
					*(im_patr + xpos2 + LineLength2 *(ypos2))=255;
					*(im_patr + xpos2 +1 + LineLength2 *(ypos2))=255;
					*(im_patr + xpos2 +2 + LineLength2 *(ypos2))=255;
					xpos2+=4;
					xpos3+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			xpos3=0;
			xpos2=0;
			ypos2=0;

			for(ypos=yposition; ypos<=yposition+39+ySize; ypos++)
			{
				for(xpos=xposition-29;xpos<=xposition+30;xpos++)
				{	
					//for comparison later
					*(im_matchr + xpos3 + LineLength3 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					//only for show
					*(im_patr + xpos2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +1 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +2 + LineLength2 *(ypos2))= *(im_srcIn + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			//SavePattern(im_matchr,false);
		}

	}

	clearLipStats(0);
	initLip=false;
	LearnDone=true;
	return im_pat;
}

//Checks cam1 surface tamper band. Returns count of how many points are beyond the threshold.
//im_srcHV: Grayscale image
//im_srcC: Color image
//x:   x coordinate of left cap point.
//y:   y coordinate of left cap point.
//x1:  x coordinate of right cap point.
//y1:  y coordinate of right cap point.
//TBx: x delta between the cam1 tamper band edge search box coordinates and the cap edge
//TBy: y delta between the cam1 tamper band edge search box coordinates and the cap edge
//type: Type of tamper band algorithm to use.
//color: color threshold.
SinglePoint CXAxisView::CheckBand(BYTE *im_srcHV, BYTE *im_srcC, BYTE *im_red, BYTE *im_hue, int x,int y,int x1,int y1, int TBx, int TBy, int type, int color)
{							                         	   //CapLSide.x,CapLTop.y,CapRSide.x,CapRTop.y,TBOffset.x,TBOffset.y
	
	//TBx is the dist between CapLSide.x and starting c ord of the Left TB box.
	SinglePoint result;
	result.x=result.y=0;
	int pixa=0;
	float all=0;
	int count_sob=0;
	for(int xyz=0;xyz<2000;xyz++){FrontBandWSobel[xyz].x=FrontBandWSobel[xyz].y=0;} ///initialising the array holding sobel points
	
	int passes=0;
	int tempTotal=0;
	int segNum;
	int k;
	
	int LineLength=theapp->cam1Width; //1024
	int lx=0;
	int ly=0;
	int ypos=0;
	int xpos=0;
	int i=0;
	int j=0;
	int d1=0;
	int count=0;
	int largest=0;
	int Thresh=theapp->jobinfo[pframe->CurrentJobNum].tBandThresh; //Edge threshold
	int TB2Thresh=theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh; //surface threshold
	lx=TBand2.x=(x+TBx);//starting X co ordinate of the Left TB edge box
	ly=TBand2.y=(y+TBy);//starting Y co ordinate of the Left TB edge box 
	if(ly<=0)ly=1;

	rx=RTBand2.x=(x1-TBx);//ending X co ordinate of the Right TB edge box
	ry=RTBand2.y=(y1+TBy);

	//RTBand2.x-=5;
	int subResult=0;
	int lx2=lx+7; //lx2=lx+7 since lx=starting X Co ord of the left TB box 
	int rx2=rx-7; //rx2 is starting X co ord of the Right TB edge box

	int segmentSize=(rx2-lx2)/24;//this should be TB Surface area for Cam 1
	int yDiff=y1-y;
	if (yDiff >=25) yDiff=25;
	int yPosRight=0;
	int resultB=0;
	int resultG=0;
	int resultR=0;
	int subResultB=0;
	int subResultG=0;
	int subResultR=0;
	int pixB;
	int pixG;
	int pixR;
	int tempTotalB=0;
	int tempTotalG=0;
	int tempTotalR=0;
	

	//following is TB EDGE inspection for Camera 1
	if(lx>=0 && 
		lx2>=0 && 
		lx2 < theapp->cam1Height-30 && 
		lx < theapp->cam1Height-30 && 
		rx <=1000 && 
		rx2 <=1000 && 
		rx >=10 && 
		rx2 >=10 && 
		(ly+theapp->jobinfo[pframe->CurrentJobNum].tBandDy) <=theapp->cam1Height-30 && 
		(ly) >=1)
	{
		for (ypos=ly; ypos < ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; ypos++)
		{
			for (xpos=lx+1; xpos <= lx+6; xpos++)  //strictly within the Left TB Edge box 
			{
				pixa=*(im_srcHV + xpos + (LineLength * (ypos)));
				all+=1;
				if( pixa > Thresh )
				{				 
					result.x+=1; //total count of all pixels greater than threshold
				}
			}
		
			yPosRight=ypos+yDiff;
			if(yPosRight<=0) yPosRight=0;
			for (xpos=rx-1; xpos >= rx-6; xpos--)    //strictly within the Right TB Edge box
			{
				pixa=*(im_srcHV + xpos + (LineLength * (yPosRight)));
				all+=1;
				if( pixa > Thresh )
				{				 
					result.x+=1;  //total count of all pixels greater than threshold
				}
			}

		}


		//End of TB EDGE


		//Beginning of TB SURFACE inspection for Camera 1
		switch(type)
		{
	
		case 1:      //Edge detection
					//NOt a true edge detection

				int pixb,pixc,pixd,pixe,pixf;
			
				for(i=ly; i<= ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
				{
					for(j=lx2; j<= rx2; j++)
					{	
						pixa=*(im_srcHV+j-1+(LineLength * (i-1)));  pixd=*(im_srcHV+j+1+(LineLength * (i-1)));	
						pixb=*(im_srcHV+j-1+(LineLength * (i)));    pixe=*(im_srcHV+j+1+(LineLength * (i)));
						pixc=*(im_srcHV+j-1+(LineLength * (i+1)));  pixf=*(im_srcHV+j+1+(LineLength * (i+1)));
						
						d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
												
						if( (d1 >= TB2Thresh)  )
						{			
							count+=1;
							
							result.y=count;	//total count of defective pixels				
						}
					}
				}
			
			break;

		case 2:  //Threshold
	
				if(color==0)//search for white
				{
					if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true ) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 ==true))
					{
					//CalcSobel(im_cam1,lx2-10,rx2+10,ly-10,(ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2)+10);
					}
					
				
					for(i=ly; i<= ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
					{
						for(j=lx2; j<= rx2; j++)  //TB Surface 
						{	
							pixa=*(im_srcHV+j+(LineLength * (i)));  

							if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 ==true)
							{
							  pixb=*(sobel_cam1+j+(LineLength * (i)));  
							  if(pixb !=0 && count_sob <1600) {FrontBandWSobel[count_sob].x=j;FrontBandWSobel[count_sob].y=i;count_sob+=1;}
							}
							
													
							if( (pixa >= TB2Thresh)  )
							{			
								count+=1;

								if(count<=1600 && (count % 8) ==0)
								{
									frontBand[count].x=j;
									frontBand[count].y=i;
								}
								
								result.y=count;	//total count of defective pixels				
							}
						}
					}
						if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true){ result.y=count_sob + count;}
				
				}
				else if(color == 1)//search for black
				{
					for(i=ly; i<= ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
					{
						for(j=lx2; j<= rx2; j++)
						{	
							pixa=*(im_srcHV+j+(LineLength * (i)));  
													
							if( (pixa <= TB2Thresh)  )
							{			
								count+=1;

								if(count<=1600 && (count % 8) ==0)
								{
									frontBand[count].x=j;
									frontBand[count].y=i;
								}
								
								result.y=count;		 //total count of defective pixels			
							}
						}
					}
				
				}
				else if (color == 2)//search red channel
				{
					for (i = ly; i <= ly + (theapp->jobinfo[pframe->CurrentJobNum].tBandDy) * 2; i++)
					{
						for (j = lx2; j <= rx2; j++)
						{
							pixa = *(im_red + j + (LineLength * (i)));

							if ((pixa <= TB2Thresh))
							{
								count += 1;

								if (count <= 1600 && (count % 8) == 0)
								{
									frontBand[count].x = j;
									frontBand[count].y = i;
								}

								result.y = count;		 //total count of defective pixels			
							}
						}
					}
					

				}
				else if (color == 3)//search hue channel
				{
					for (i = ly; i <= ly + (theapp->jobinfo[pframe->CurrentJobNum].tBandDy) * 2; i++)
					{
						for (j = lx2; j <= rx2; j++)
						{
							pixa = *(im_hue + j + (LineLength * (i)));
							 
							if ((pixa <= TB2Thresh))
							{
								count += 1;

								if (count <= 1600 && (count % 8) == 0)
								{
									frontBand[count].x = j;
									frontBand[count].y = i;
								}

								result.y = count;		 //total count of defective pixels			
							}
						}
					}
				}
				break;

		case 3:

			if(lx2 % 4 !=0)
			{
				lx2+=1;
				if(lx2 % 4 !=0)
				{
					lx2+=1;
					if(lx2 % 4 !=0)
					{
						lx2+=1;
					}
				}
			}

			segmentSize=segmentSize*4;
			lx2=lx2*4;
			LineLength=1024*4;
			resultB=0;
			resultG=0;
			resultR=0;
			subResultB=0;
			subResultG=0;
			subResultR=0;
			
			tempTotalB=0;
			tempTotalG=0;
			tempTotalR=0;

			for(segNum=0; segNum<24; segNum++)
			{
				int lyOffset=ly;
				if(segNum>=6 && segNum < 9) lyOffset+=4;
				if(segNum>=9 && segNum <=15) lyOffset+=7;
				if(segNum>15 && segNum <=18) lyOffset+=4;

				for(k=lx2+segNum*segmentSize; k<=lx2+segNum*segmentSize+segmentSize; k+=4)
				{
					for(i=lyOffset; i<= lyOffset+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
					{
						
						pixB=*(im_srcC+k+(LineLength * (i)));
						pixG=*(im_srcC+k+1+(LineLength * (i)));
						pixR=*(im_srcC+k+2+(LineLength * (i)));
																	
						tempTotalB+=pixB;
						tempTotalG+=pixG;
						tempTotalR+=pixR;
						passes+=1;
																
					}		
				}
				if(passes!=0)
				{

					currentSegValueFront[segNum]=GetHue(tempTotalR/passes,tempTotalG/passes,tempTotalB/passes);
				}
				else
				{
					currentSegValueFront[segNum]=0;
				
				}

				subResult=abs(currentSegValueFront[segNum]-taughtSegValueFront[segNum]);
				
				if(subResult>=result.y){ result.y=subResult; showPos1=segNum;} 
				
				tempTotalB=0;
				tempTotalG=0;
				tempTotalR=0;
				passes=0;
			}

				
			
		break;

		case 4:  //Integrity check
				//looks like it isnt doing a true pattern match
			for(i=ly; i<= ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
			{
				for(j=lx2; j<= rx2; j++)
				{	
					pixa=*(im_srcHV+j+(LineLength * (i)));  
													
					if( (pixa <= TB2Thresh)  )
					{			
						count+=1;
								
						result.y=count;					
					}
				}
			}

			break;

		case 5:   //compare color 
			
		passes=0;
		int	targetB=0;
		int	targetG=0;
		int	targetR=0;
		int tolBand=100-theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh;
	
			if(lx2 % 4 !=0)
			{
				lx2+=1;
				if(lx2 % 4 !=0)
				{
					lx2+=1;
					if(lx2 % 4 !=0)
					{
						lx2+=1;
					}
				}
			}
	
			//lx2=lx2*4;
			LineLength=1024*4;
			//*********get target***********
			
			for(k=sam.x*4; k<=sam.x*4+32; k+=4)
			{
				for(i=sam.y-8; i<= sam.y; i++)
				{
						
					pixB=*(im_srcC+k+(LineLength * (i)));
					pixG=*(im_srcC+k+1+(LineLength * (i)));
					pixR=*(im_srcC+k+2+(LineLength * (i)));
																	
					targetB+=pixB;
					targetG+=pixG;
					targetR+=pixR;
					passes+=1;
																
				}		
			}
			
			if(passes>=1)targetB=targetB/passes;
			if(passes>=1)targetG=targetG/passes;
			if(passes>=1)targetR=targetR/passes;

			int highR=targetR+tolBand;
			int lowR=targetR-tolBand;
			int highG=targetG+tolBand;
			int lowG=targetG-tolBand;
			int highB=targetB+tolBand;
			int lowB=targetB-tolBand;
		
			for(k=lx2*4; k<=rx2*4; k+=4)
			{
				for(i=ly; i<= ly+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
				{
						
					pixB=*(im_srcC+k+(LineLength * (i)));
					pixG=*(im_srcC+k+1+(LineLength * (i)));
					pixR=*(im_srcC+k+2+(LineLength * (i)));
																	
					
							switch(theapp->jobinfo[pframe->CurrentJobNum].bandColor)
							{
								case 1:
									if((pixB >= pixR+tolBand))result.y+=1;
								
								break;

								case 2:
									if((pixR >= pixB+tolBand))result.y+=1;
								
								break;

								case 3:
									if((pixG >= pixR+tolBand))result.y+=1;
								
								break;

								case 4:
									if( (pixR >= pixB+tolBand) &&
									(pixR >= pixG) )result.y+=1;
								
								break;
							}
																
				}		
			}
			

		break;

		}//end of switch

	
	}
	return result;
}

//Checks cam2 or cam3 surface tamper band. Returns count of how many points are beyond the threshold.
//im_srcHV: Grayscale image
//im_srcC: Color image
//im_srcInSob: sobel filter image
//TBYDiff: rear camera vertical offset. (where to start  search for tamper band break).
//XWidth: rear camera width of search area.
//type: Type of tamper band algorithm to use.
//color: color threshold.
//Left: left or right camera to search.
SinglePoint CXAxisView::CheckBand2(BYTE *im_srcIn, BYTE *im_srcC, BYTE *im_srcInSob, BYTE *im_srcR, BYTE *im_srcInHue, int TBYDiff, int XWidth, int type, int color, bool Left)
{								
	SinglePoint result;
	result.x=result.y=0;RearL.x=RearL.y=0;RearR.x=RearR.y=0;
	int xpos=0;
	int count=0;int count_sob=0;


	int tempTotal=0;
	int segNum;
	int k, j;

	if(TBYDiff>=160 ) TBYDiff=160; //Commented out by Vivek 8/13/2014 ..Dont see why this should get limited to 160 when image height is 
	int ypos=TBYDiff;
	if(ypos<=0) ypos=10;
	if(ypos>=theapp->cam2Height-30) ypos=theapp->cam2Height-30;
	int xoffset=10;
	int yposwidth=theapp->jobinfo[pframe->CurrentJobNum].rearCamDy;
	int LineLengthL=theapp->cam2Width;
	int LineLengthR=theapp->cam3Width;
	int avgShade=0;
	int d1=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	int Thresh=40;
	int passes=0;
	int TB2Thresh=theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2;
	int RBand2X=(theapp->jobinfo[pframe->CurrentJobNum].rSWin)*2;
	int LBand2X=(theapp->jobinfo[pframe->CurrentJobNum].lSWin)*2+320;
	int subResult=0;
	int segmentSize=XWidth/24;
	int largest=0;
	int yposD=ypos-10;
	if(yposD<=2) yposD=2;
	int gotw=0;

	int tempTotalB=0;
	int tempTotalG=0;
	int tempTotalR=0;
	int subResultB=0;
	int subResultG=0;
	int subResultR=0;
	int resultB=0;
	int resultG=0;
	int resultR=0;
	int numOfSegs=5;
	sgSize=XWidth/numOfSegs;
	count=0;

	int gotSome=0;
	int avg=0;
	int darkest=255;
	int lightest=0;


	if(Left==true)
	{
		//********Look for the side  435
		for(int x=LBand2X; x>10; x--)
		{	
			//if(x <=1) x=1;// added 08/26/13

			pixa=*(im_srcIn+x-1+(LineLengthL * (yposD-1)));  pixd=*(im_srcIn+x+1+(LineLengthL * (yposD-1)));	
			pixb=*(im_srcIn+x-1+(LineLengthL * (yposD)));    pixe=*(im_srcIn+x+1+(LineLengthL * (yposD)));
			pixc=*(im_srcIn+x-1+(LineLengthL * (yposD+1)));  pixf=*(im_srcIn+x+1+(LineLengthL * (yposD+1)));
				
			d1=(pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc);
			if(pixb>240) gotw+=1;
										
			if( (d1 >= Thresh && gotw>=5)  )
			{			
				result.x=xpos=x-theapp->jobinfo[pframe->CurrentJobNum].leftBandShift;
				RearL.x=result.x;RearL.y=yposD;
						
				x=1;				
			}
		}
		if(xpos<=120) xpos=120;
		if(xpos>=639) xpos=639;
		//*********look missing serations

		if(gotw>=5)
		{
			
		switch(type)
		{
	
		case 1:


			if(xpos-XWidth <=20 )
			{
				XWidth=xpos-20;
			}

			if(xpos-XWidth >=1  && xpos <640)
			{
				for(int i=xpos; i>xpos-XWidth; i--)
				{
					for(int j=ypos; j<ypos+yposwidth; j++)
					{	
						pixa=*(im_srcIn+i-1+(LineLengthL * (j-1)));  pixd=*(im_srcIn+i+1+(LineLengthL * (j-1)));	
						pixb=*(im_srcIn+i-1+(LineLengthL * (j)));    pixe=*(im_srcIn+i+1+(LineLengthL * (j)));
						pixc=*(im_srcIn+i-1+(LineLengthL * (j+1)));  pixf=*(im_srcIn+i+1+(LineLengthL * (j+1)));
						
						d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
												
						if( (d1 >= TB2Thresh)  )
						{			
							count+=1;
							
							result.y=count;					
						}
					}
				}
			}
			break;

		case 2:///left

			{	count=0;count_sob=0;for(int xyz=0;xyz<2000;xyz++){rearBandLwSobel[xyz].x=rearBandLwSobel[xyz].y=0;}
			numOfSegs=5;
			if(xpos-XWidth <=20 )
			{
				XWidth=xpos-20;
			}
		if((ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1] >2) && (ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1] <theapp->cam2Height) &&
			(ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2] >2) && (ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2] <theapp->cam2Height) &&
			(ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3] >2) && (ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3] <theapp->cam2Height) &&
			(ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4] >2) && (ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4] <theapp->cam2Height) &&
			(ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5] >2) && (ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5] <theapp->cam2Height) ) 
		{
			if(color==0)//looking for white
			{
			

				for(int g=0; g<numOfSegs; g++)
				{
					for(int i=xpos-(g*sgSize); i>xpos-((g+1)*sgSize); i--)
					{
						for(int j=ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; j<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; j++)
						{	
							pixa=*(im_srcIn+i+(LineLengthL * (j)));  
							if(g==0) pixa-=6;
							if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 ==true) ) 
							{
							  pixb=*(im_srcInSob+i+(LineLengthL * (j)));  
							  if(pixb !=0 && count_sob <1600) {rearBandLwSobel[count_sob].x=i;rearBandLwSobel[count_sob].y=j;count_sob+=1;}
							}
							
							if( (pixa >= TB2Thresh)  ) //if greater than sensitivity
							{			
								count+=1;
									
								if(count<=1600 && (count % 8) ==0)
								{
									rearBandL[count].x=i;
									rearBandL[count].y=j;
								}
								result.y=count;					
							}
						}
					}
				}

				if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 ==true)){ result.y=count_sob + count;}
			}
			else if(color == 1)   //looking for black
			{
				for(int g=0; g<numOfSegs; g++)
				{
					for(int i=xpos-(g*sgSize); i>xpos-((g+1)*sgSize); i--)
					{
						for(int j=ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; j<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; j++)
						{	
							pixa=*(im_srcIn+i+(LineLengthL * (j)));  
													
							if( (pixa <= TB2Thresh)  )  //If less than sensitivity
							{			
								count+=1;
								
								if(count<=1600 && (count % 8) ==0)
								{
									rearBandL[count].x=i;
									rearBandL[count].y=j;
								}
								
								result.y=count;					
							}
						}
					}
				}
			}
			else if (color == 2) //red
			{
				for (int g = 0; g < numOfSegs; g++)
				{
					for (int i = xpos - (g*sgSize); i > xpos - ((g + 1)*sgSize); i--)
					{
						for (int j = ypos + theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g + 1]; j < ypos + yposwidth + theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g + 1]; j++)
						{
							pixa = *(im_srcR + i + (LineLengthL * (j)));

							if ((pixa <= TB2Thresh))  //If less than sensitivity
							{
								count += 1;

								if (count <= 1600 && (count % 8) == 0)
								{
									rearBandL[count].x = i;
									rearBandL[count].y = j;
								}

								result.y = count;
							}
						}
					}
				}

			}
			else if (color == 3) //hue
			{
				for (int g = 0; g < numOfSegs; g++)
				{
					for (int i = xpos - (g*sgSize); i > xpos - ((g + 1)*sgSize); i--)
					{
						for (int j = ypos + theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g + 1]; j < ypos + yposwidth + theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g + 1]; j++)
						{
							pixa = *(im_srcInHue + i + (LineLengthL * (j)));

							if ((pixa <= TB2Thresh))  //If less than sensitivity
							{
								count += 1;

								if (count <= 1600 && (count % 8) == 0)
								{
									rearBandL[count].x = i;
									rearBandL[count].y = j;
								}

								result.y = count;
							}
						}
					}
				}

			}
		}
			break;
			}
		case 3:

			if(xpos % 4 !=0)
			{
				xpos+=1;
				if(xpos % 4 !=0)
				{
					xpos+=1;
					if(xpos % 4 !=0)
					{
						xpos+=1;
					}
				}
			}

			segmentSize=segmentSize*4;
			xpos=xpos*4;
			LineLengthL=theapp->cam2Width*4;
			resultB=0;
			resultG=0;
			resultR=0;
			subResultB=0;
			subResultG=0;
			subResultR=0;
			int pixB;
			int pixG;
			int pixR;
			tempTotalB=0;
			tempTotalG=0;
			tempTotalR=0;
			

			for(segNum=0; segNum<24; segNum++)
			{
				int lyOffset=ypos;
			
				for(k=xpos-segNum*segmentSize; k>=xpos-segNum*segmentSize-segmentSize; k-=4)
				{
					for(int i=lyOffset; i<= lyOffset+yposwidth; i++)
					{	
						pixB=*(im_srcC+k+(LineLengthL * (i)));
						pixG=*(im_srcC+k+1+(LineLengthL * (i)));
						pixR=*(im_srcC+k+2+(LineLengthL * (i)));
																	
						tempTotalB+=pixB;
						tempTotalG+=pixG;
						tempTotalR+=pixR;
						passes+=1;										
					}		
				}
				if(passes!=0)
				{
					currentSegValueLeft[segNum]=GetHue(tempTotalR/passes,tempTotalG/passes,tempTotalB/passes);
				}
				else
				{
					currentSegValueLeft[segNum]=0;
				}
				subResult=abs(currentSegValueLeft[segNum]-taughtSegValueLeft[segNum]);
				
				if(subResult>=result.y){ result.y=subResult;showPos2=segNum;} 
				
				tempTotalB=0;
				tempTotalG=0;
				tempTotalR=0;
				passes=0;
			}
		

		break;

		case 4:

			if(xpos-XWidth <=20 )
			{
				XWidth=xpos-20;
			}
			
		
		
			numOfSegs=5;
			sgSize=XWidth/numOfSegs;
			count=0;
			result.y=0;
		
			lBand[1].x=0;
			lBand[1].y=0;
			
			
			if(ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1] >2 &&ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5] >2)
			{
			
			for(int g=0; g<numOfSegs; g++)
			{
				for(int i=xpos-(g*sgSize); i>xpos-((g+1)*sgSize); i-=3)
				{
					for(int j=ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; j<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; j++)
					{	
						pixa=*(im_srcIn+i+(LineLengthL * (j)));  
						count+=1;								
						if(pixa<=darkest) darkest=pixa;
						if(pixa>=lightest) lightest=pixa;
						avg+=pixa;
					}

					if(count!=0) avg=avg/count;

					if(lightest-darkest >= theapp->jobinfo[pframe->CurrentJobNum].rearSurfThresh)//theapp->jobinfo[pframe->CurrentJobNum].rearSurfThresh
					{
						result.y=(lightest-darkest)*3;
					if(theapp->jobinfo[pframe->CurrentJobNum].rearSurfThresh>=100)result.y=0;
						lBand[1].x=i/2;
						lBand[1].y=j/2;
						gotSome+=1;
					}
					else
					{
						gotSome=0;
					}

					if(gotSome>=2)
					{
						g=numOfSegs;
						j=ypos+yposwidth+10;
						i=-640;
					}
					
				
					count=0;
					darkest=255;
					lightest=0;
					gotSome=0;
				}
			
			}
			}
			

		break;

		case 5:

		targetB=0;
		targetG=0;
		targetR=0;
		int tolBand=100-theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2;
			if(xpos % 4 !=0)
			{
				xpos+=1;
				if(xpos % 4 !=0)
				{
					xpos+=1;
					if(xpos % 4 !=0)
					{
						xpos+=1;
					}
				}
			}

			
			xpos=xpos*4;
			LineLengthL=theapp->cam2Width*4;
		
		
			if(ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1] >2 &&
				ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5] >2)
			{
				for(int g=0; g<numOfSegs; g++)
				{
					for(int k=xpos-(g*sgSize*4); k>xpos-((g+1)*sgSize*4); k-=4)
					{
						for(int i=ypos+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; i<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g+1]; i++)
						{	
							pixB=*(im_srcC+k+(LineLengthL * (i)));
							pixG=*(im_srcC+k+1+(LineLengthL * (i)));
							pixR=*(im_srcC+k+2+(LineLengthL * (i)));  
							
							switch(theapp->jobinfo[pframe->CurrentJobNum].bandColor)
							{
							case 1:
									if((pixB >= pixR+tolBand))result.y+=1;
								
								break;

								case 2:
									if((pixR >= pixB+tolBand))result.y+=1;
								
								break;

								case 3:
									if((pixG >= pixR+tolBand))result.y+=1;
								
								break;

								case 4:
									if( (pixR >= pixB+tolBand)  )result.y+=1;//&&(pixR >= pixG)
								
								break;
							}
							
						}
					}
				}
			}

			break;
		
		}//end of case
		}//gotw>=5
	}
	else  // right
	{
		for(int x=RBand2X; x<639; x++)
		{	
			if(x<=1 ) x=1;// added 08/26/13 
			pixa=*(im_srcIn+x-1+(LineLengthR * (yposD-1)));  pixd=*(im_srcIn+x+1+(LineLengthR * (yposD-1)));	
			pixb=*(im_srcIn+x-1+(LineLengthR * (yposD)));    pixe=*(im_srcIn+x+1+(LineLengthR * (yposD)));
			pixc=*(im_srcIn+x-1+(LineLengthR * (yposD+1)));  pixf=*(im_srcIn+x+1+(LineLengthR * (yposD+1)));
				
			d1=(pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf);

			if(pixb>240) gotw+=1;
		
										
			if( (d1 >= Thresh && gotw>=5)  )
			{			
				result.x=xpos=x+theapp->jobinfo[pframe->CurrentJobNum].rightBandShift;
				RearR.x=result.x;RearR.y=yposD;
						
				x=639;				
			}
		}

		if(xpos<=10) xpos=10;
		if(xpos>=520) xpos=520;
	
		if(gotw>=5)
		{
		switch(type)
		{
		case 1:

			if(xpos+XWidth <640  && xpos >=0 && ypos+yposwidth<=theapp->cam1Height-30)
			{
				for(int i=xpos; i<xpos+XWidth; i++)
				{
					for(int j=ypos; j<ypos+yposwidth; j++)
					{	
						pixa=*(im_srcIn+i-1+(LineLengthR * (j-1)));  pixd=*(im_srcIn+i+1+(LineLengthR * (j-1)));	
						pixb=*(im_srcIn+i-1+(LineLengthR * (j)));    pixe=*(im_srcIn+i+1+(LineLengthR * (j)));
						pixc=*(im_srcIn+i-1+(LineLengthR * (j+1)));  pixf=*(im_srcIn+i+1+(LineLengthR * (j+1)));
						
						d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
												
						if( (d1 >= TB2Thresh)  )
						{			
							count+=1;
							
							result.y=count;					
						}
					}
				}
			}
			break;

		case 2:
		{	count=0;count_sob=0;for(int xyz=0;xyz<2000;xyz++){rearBandRwSobel[xyz].x=rearBandRwSobel[xyz].y=0;}
			numOfSegs=5;
		if(xpos+XWidth >=638 )
		{
				XWidth=500-xpos;
		}
		if( (ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1] >2) &&(ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1] < theapp->cam2Height)  
			&& (ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2] >2) &&(ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2]< theapp->cam2Height) 
			&& (ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3] >2) &&(ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3]< theapp->cam2Height)
			&& (ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4] >2) &&(ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4]< theapp->cam2Height)
			&& (ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5] >2)&& (ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5]< theapp->cam2Height))
		{
			if(color==0)
			{
		
				for(int g=0; g<5; g++)
				{
					for(int i=xpos+(g*sgSize); i< xpos+((g+1)*sgSize); i++)
					{
						for(int j=ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; j<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; j++)
						{	
							pixa=*(im_srcIn+i+(LineLengthR * (j)));
							if(g==0) pixa-=6;
					
							if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 ==true))
							{
							  pixb=*(im_srcInSob+i+(LineLengthR * (j)));  
							  if(pixb !=0 && count_sob <1600) {rearBandRwSobel[count_sob].x=i;rearBandRwSobel[count_sob].y=j;count_sob+=1;}
							}
							if( (pixa >= TB2Thresh)  )
							{			
								count+=1;
								if(count<=1600 && (count % 8) ==0)
								{
									rearBandR[count].x=i;
									rearBandR[count].y=j;
								}
								
								result.y=count;					
							}
						}
					}
				}
				if((theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true) && (theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 ==true)){ result.y=count_sob + count;}
			}
			else if(color == 1)
			{
				
				for(int g=0; g<5; g++)
				{
					for(int i=xpos+(g*sgSize); i< xpos+((g+1)*sgSize); i++)
					{
						for(int j=ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; j<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; j++)
						{	
							pixa=*(im_srcIn+i-1+(LineLengthR * (j-1)));  
						
							if( (pixa <= TB2Thresh)  )
							{			
								count+=1;
								
								if(count<=1600 && (count % 8) ==0)
								{
									rearBandR[count].x=i;
									rearBandR[count].y=j;
								}
								
								result.y=count;					
							}
						}
					}
				}
				
			}
			else if (color == 2) //red
			{
				for (int g = 0; g < numOfSegs; g++)
				{
					for (int i = xpos - (g*sgSize); i > xpos - ((g + 1)*sgSize); i--)
					{
						for (int j = ypos + theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g + 1]; j < ypos + yposwidth + theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g + 1]; j++)
						{
							pixa = *(im_srcR + i + (LineLengthL * (j)));

							if ((pixa <= TB2Thresh))  //If less than sensitivity
							{
								count += 1;

								if (count <= 1600 && (count % 8) == 0)
								{
									rearBandR[count].x = i;
									rearBandR[count].y = j;
								}

								result.y = count;
							}
						}
					}
				}

			}
			else if (color == 3) //hue
			{
				for (int g = 0; g < numOfSegs; g++)
				{
					for (int i = xpos - (g*sgSize); i > xpos - ((g + 1)*sgSize); i--)
					{
						for (int j = ypos + theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g + 1]; j < ypos + yposwidth + theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[g + 1]; j++)
						{
							pixa = *(im_srcInHue + i + (LineLengthL * (j)));

							if ((pixa <= TB2Thresh))  //If less than sensitivity
							{
								count += 1;

								if (count <= 1600 && (count % 8) == 0)
								{
									rearBandL[count].x = i;
									rearBandL[count].y = j;
								}

								result.y = count;
							}
						}
					}
				}

			}
		}
			break;
		}
		case 3:
			
			if(xpos % 4 !=0)
			{
				xpos+=1;
				if(xpos % 4 !=0)
				{
					xpos+=1;
					if(xpos % 4 !=0)
					{
						xpos+=1;
					}
				}
			}

			segmentSize=segmentSize*4;
			xpos=xpos*4;
			LineLengthR=theapp->cam3Width*4;
			resultB=0;
			resultG=0;
			resultR=0;
			subResultB=0;
			subResultG=0;
			subResultR=0;
			int pixB;
			int pixG;
			int pixR;
			tempTotalB=0;
			tempTotalG=0;
			tempTotalR=0;

			for(segNum=0; segNum<24; segNum++)
			{
				int lyOffset=ypos;
			
				for(k=xpos+segNum*segmentSize; k<=xpos+segNum*segmentSize+segmentSize; k+=4)
				{
					for(int i=lyOffset; i<= lyOffset+yposwidth; i++)
					{	
						pixB=*(im_srcC+k+(LineLengthR * (i)));
						pixG=*(im_srcC+k+1+(LineLengthR * (i)));
						pixR=*(im_srcC+k+2+(LineLengthR * (i)));
																	
						tempTotalB+=pixB;
						tempTotalG+=pixG;
						tempTotalR+=pixR;
						passes+=1;										
					}		
				}
				if(passes!=0)
				{
					currentSegValueRight[segNum]=GetHue(tempTotalR/passes,tempTotalG/passes,tempTotalB/passes);
				}
				else
				{
					currentSegValueRight[segNum]=0;
				}
				subResult=abs(currentSegValueRight[segNum]-taughtSegValueRight[segNum]);
				
				if(subResult>=result.y){ result.y=subResult;showPos3=segNum;} 
				
				tempTotalB=0;
				tempTotalG=0;
				tempTotalR=0;
				passes=0;
			}
		
			
		break;

		case 4:

			
			sgSize=XWidth/numOfSegs;
			count=0;
			result.y=0;
			rBand[1].x=0;
			rBand[1].y=0;
			

			gotSome=0;
			
			if(ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4] >2 && ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5] >2)
			{
			for(int g=0; g<numOfSegs; g++)
			{
				for(int i=xpos+(g*sgSize); i< xpos+((g+1)*sgSize); i+=2)
				{
					for(int j=ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; j<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; j++)
					{	
						pixa=*(im_srcIn+i+(LineLengthR * (j)));  
						count+=1;								
						if(pixa<=darkest) darkest=pixa;
						if(pixa>=lightest) lightest=pixa;
						avg+=pixa;
					}

					if(count!=0) avg=avg/count;

					if(lightest-darkest >=theapp->jobinfo[pframe->CurrentJobNum].rearSurfThresh )//
					{
						result.y=(lightest-darkest)*4;
						if(theapp->jobinfo[pframe->CurrentJobNum].rearSurfThresh>=100)result.y=0;
						rBand[1].x=i/2;
						rBand[1].y=j/2;
						gotSome+=1;
					}
					else
					{
						gotSome=0;
					}

					if(gotSome>=2)
					{
						g=numOfSegs;
						j=ypos+yposwidth;
						i=640;
					}
					
					count=0;
					darkest=255;
					lightest=0;
					gotSome=0;
				}
			
			}
			}
		break;

	case 5:


		targetB=0;
		targetG=0;
		targetR=0;
		int tolBand=100-theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2;
			if(xpos % 4 !=0)
			{
				xpos+=1;
				if(xpos % 4 !=0)
				{
					xpos+=1;
					if(xpos % 4 !=0)
					{
						xpos+=1;
					}
				}
			}

			segmentSize=segmentSize*4;
			xpos=xpos*4;
			LineLengthL=theapp->cam2Width*4;
		
						
			if(ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4] >2 && 
				ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5] >2)
			{
				for(int g=0; g<5; g++)
				{
					for(int k=xpos+(g*sgSize*4); k< xpos+((g+1)*sgSize*4); k+=4)
					{
						for(int i=ypos+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; i<ypos+yposwidth+theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[g+1]; i++)
						{	
							pixB=*(im_srcC+k+(LineLengthL * (i)));
							pixG=*(im_srcC+k+1+(LineLengthL * (i)));
							pixR=*(im_srcC+k+2+(LineLengthL * (i)));
							switch(theapp->jobinfo[pframe->CurrentJobNum].bandColor)
							{
								case 1:
									if((pixB >= pixR+tolBand))result.y+=1;
								
								break;

								case 2:
									if((pixR >= pixB+tolBand))result.y+=1;
								
								break;

								case 3:
									if((pixG >= pixR+tolBand))result.y+=1;
								
								break;

								case 4:
									if( (pixR >= pixB+tolBand)  )result.y+=1;//&&(pixR >= pixG)
								
								break;
							}
						}
					}
				}
			}

			break;

		}//end of case
		}//gotw>=5
	}
	
	return result;
}


//Shaded average tamper band inspection, NOT USED.
void CXAxisView::TeachBandFront(BYTE *im_srcHV, int x,int y,int x1,int y1, int TBx, int TBy )
{
	int pixR=0;
	int pixG=0;
	int pixB=0;

	float all=0;
	
	int passes=0;
	int tempTotalB=0;
	int tempTotalG=0;
	int tempTotalR=0;
	int segNum;
	int k;
	
	int LineLength=1024*4;
	int lx=0;
	int ly=0;
	int ypos=0;
	int xpos=0;
	int i=0;
	int j=0;
	int d1=0;
	int count=0;
	int Thresh=theapp->jobinfo[pframe->CurrentJobNum].tBandThresh;
	
	lx=(x+TBx);
	ly=(y+TBy);
	if(ly<=0)ly=1;

	rx=RTBand2.x=(x1-TBx);
	ry=RTBand2.y=(y1+TBy);

	//RTBand2.x-=5;

	int lx2=lx+7;
	int rx2=rx-7;
	int segmentSize=(rx2-lx2)/24;
	segmentSize=segmentSize*4;

	if(lx2 % 4 !=0)
	{
		lx2+=1;
		if(lx2 % 4 !=0)
		{
			lx2+=1;
			if(lx2 % 4 !=0)
			{
				lx2+=1;
			}
		}
	}

	lx2=lx2*4;

	for(segNum=0; segNum<24; segNum++)
	{
		int lyOffset=ly;
		if(segNum>=6 && segNum < 9) lyOffset+=4;
		if(segNum>=9 && segNum <=15) lyOffset+=7;
		if(segNum>15 && segNum <=18) lyOffset+=4;

		
		for(k=lx2+segNum*segmentSize; k<=lx2+segNum*segmentSize+segmentSize; k+=4)
		{
			for(i=lyOffset; i<= lyOffset+(theapp->jobinfo[pframe->CurrentJobNum].tBandDy)*2; i++)
			{
				
				pixB=*(im_srcHV+k+(LineLength * (i)));
				pixG=*(im_srcHV+k+1+(LineLength * (i)));
				pixR=*(im_srcHV+k+2+(LineLength * (i)));
															
				tempTotalB+=pixB;
				tempTotalG+=pixG;
				tempTotalR+=pixR;
				passes+=1;
			}		
		}
			if(passes!=0)
			{

				taughtSegValueFront[segNum]=GetHue(tempTotalR/passes,tempTotalG/passes,tempTotalB/passes);

			}
				
			tempTotalB=0;
			tempTotalG=0;
			tempTotalR=0;
			passes=0;
	}
}

//Shaded average tamper band inspection, NOT USED.
void CXAxisView::TeachBandRear(BYTE *im_srcIn, BYTE *im_srcC, int TBYDiff, int XWidth, bool Left)
{

	int xpos=0;
	int count=0;

	int passes=0;
	int tempTotal=0;
	int segNum;
	int k;
	int ypos=TBYDiff;
	if(ypos<=0) ypos=1;
	if(ypos>=theapp->cam1Height-30) ypos=theapp->cam1Height-30;
	int xoffset=10;
	int yposwidth=theapp->jobinfo[pframe->CurrentJobNum].rearCamDy;
	int LineLengthL=theapp->cam2Width;
	int LineLengthR=theapp->cam3Width;
	int avgShade=0;
	int d1=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	int Thresh=40;
	int TB2Thresh=theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh;
	int RBand2X=(theapp->jobinfo[pframe->CurrentJobNum].rSWin)*2;
	int LBand2X=(theapp->jobinfo[pframe->CurrentJobNum].lSWin)*2+320;

	int segmentSize=XWidth/24;
	
	if(Left==true)
	{
		//********Look for the side  435
		for(int x=LBand2X; x>10; x--)
		{	
			pixa=*(im_srcIn+x-1+(LineLengthL * (ypos-1)));  pixd=*(im_srcIn+x+1+(LineLengthL * (ypos-1)));	
			pixb=*(im_srcIn+x-1+(LineLengthL * (ypos)));    pixe=*(im_srcIn+x+1+(LineLengthL * (ypos)));
			pixc=*(im_srcIn+x-1+(LineLengthL * (ypos+1)));  pixf=*(im_srcIn+x+1+(LineLengthL * (ypos+1)));
				
			d1=(pixd+(2*pixe)+pixf)-(pixa+(2*pixb)+pixc);
										
			if( (d1 >= Thresh)  )
			{			
				xpos=x-10;
						
				x=1;				
			}
		}
	
		if(xpos % 4 !=0)
		{
			xpos+=1;
			if(xpos % 4 !=0)
			{
				xpos+=1;
				if(xpos % 4 !=0)
				{
					xpos+=1;
				}
			}
		}

		LineLengthL=theapp->cam2Width*4;
		XWidth=XWidth*4;
		segmentSize=segmentSize*4;
		xpos=xpos*4;

		int resultB=0;
			int resultG=0;
			int resultR=0;
			int subResultB=0;
			int subResultG=0;
			int subResultR=0;
			int pixB;
			int pixG;
			int pixR;
			int tempTotalB=0;
			int tempTotalG=0;
			int tempTotalR=0;

		if(xpos-23*segmentSize-segmentSize >=0  && xpos-XWidth <=640*4 && xpos <640*4 && xpos>=0 && XWidth * yposwidth > 0 && ypos+yposwidth<=theapp->cam2Height-10)
		{
			for(segNum=0; segNum<24; segNum++)
			{
				for(int i=xpos-segNum*segmentSize; i>xpos-segNum*segmentSize-segmentSize; i-=4)
				{
					for(int j=ypos; j<ypos+yposwidth; j++)
					{	
						pixB=*(im_srcC+i+(LineLengthL * (j)));
						pixG=*(im_srcC+i+1+(LineLengthL * (j)));
						pixR=*(im_srcC+i+2+(LineLengthL * (j)));
																	
						tempTotalB+=pixB;
						tempTotalG+=pixG;
						tempTotalR+=pixR;
						passes+=1;
															
					}
				}
				if(passes!=0)
				{

					taughtSegValueLeft[segNum]=GetHue(tempTotalR/passes,tempTotalG/passes,tempTotalB/passes);

				}
				
			tempTotalB=0;
			tempTotalG=0;
			tempTotalR=0;
			passes=0;
			}
			
		
		}
	}
	else  // right
	{
		for(int x=RBand2X; x<639; x++)
		{	
			pixa=*(im_srcIn+x-1+(LineLengthR * (ypos-1)));  pixd=*(im_srcIn+x+1+(LineLengthR * (ypos-1)));	
			pixb=*(im_srcIn+x-1+(LineLengthR * (ypos)));    pixe=*(im_srcIn+x+1+(LineLengthR * (ypos)));
			pixc=*(im_srcIn+x-1+(LineLengthR * (ypos+1)));  pixf=*(im_srcIn+x+1+(LineLengthR * (ypos+1)));
				
			d1=(pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf);
										
			if( (d1 >= Thresh)  )
			{			
				xpos=x+10;
						
				x=639;				
			}
		}

		LineLengthR=theapp->cam3Width*4;
		XWidth=XWidth*4;
		segmentSize=segmentSize*4;
		xpos=xpos*4;

		int resultB=0;
			int resultG=0;
			int resultR=0;
			int subResultB=0;
			int subResultG=0;
			int subResultR=0;
			int pixB;
			int pixG;
			int pixR;
			int tempTotalB=0;
			int tempTotalG=0;
			int tempTotalR=0;

		if(xpos+XWidth >=0  && xpos+XWidth <=640*4 && xpos <640*4 && xpos>=0 && XWidth * yposwidth > 0 && ypos+yposwidth<=theapp->cam3Height-10)
		{
			for(segNum=0; segNum<24; segNum++)
			{
				for(int i=xpos+segNum*segmentSize; i<xpos+segNum*segmentSize+segmentSize; i+=4)
				{
					for(int j=ypos; j<ypos+yposwidth; j++)
					{	
						pixB=*(im_srcC+i+(LineLengthR * (j)));
						pixG=*(im_srcC+i+1+(LineLengthR * (j)));
						pixR=*(im_srcC+i+2+(LineLengthR * (j)));
																	
						tempTotalB+=pixB;
						tempTotalG+=pixG;
						tempTotalR+=pixR;
						passes+=1;
															
					}
				}
				if(passes!=0)
				{

					taughtSegValueRight[segNum]=GetHue(tempTotalR/passes,tempTotalG/passes,tempTotalB/passes);

				}
				
			tempTotalB=0;
			tempTotalG=0;
			tempTotalR=0;
			passes=0;
			}
			
		
		}
	
	}
}

//Used to find the mid top y coordinates of the cap.
//im_srcIn: source image, grayscale.
//MidTop: x coordinate of the middle of the cap.
float CXAxisView::FindCapMid(BYTE* im_srcIn, float MidTop)
{
	float result;

	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;

	int pixax=0;
	int pixbx=0;
	int pixcx=0;
	int pixdx=0;
	int pixex=0;
	int pixfx=0;

	int pixam=0;
	int pixbm=0;
	int pixcm=0;
	int pixdm=0;
	int pixem=0;
	int pixfm=0;

	int pixal=0;
	int pixbl=0;
	int pixcl=0;
	int pixdl=0;
	int pixel=0;
	int pixfl=0;

	int pixar=0;
	int pixbr=0;
	int pixcr=0;
	int pixdr=0;
	int pixer=0;
	int pixfr=0;
	
	int Limit=60;
	int dl,dr,dm, dmx, dx, dz;
	int resultm=0;
	int resultr=0;
	int resultl=0;
	
	int LineLength=1024;
	int xpos=MidTop;
	int ypos=1;
	int xshift1=17;
	int xshift2=8;
	int xshift3=-8;
	int xshift4=-17;
	int ystart=theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar;

	
	
	if(xpos<=900 && xpos >=1 )
	{
				
		for(ypos=cam1y+ystart;ypos<cam1dy;ypos++)// was 40
		{	
			pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
			pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
			pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

			pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
			pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
			pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

		
			pixal=*(im_srcIn+xpos+xshift1-1+(LineLength * (ypos)));  	
			pixbl=*(im_srcIn+xpos+xshift1+(LineLength * (ypos)));    
			pixcl=*(im_srcIn+xpos+xshift1+1+(LineLength * (ypos)));  

			pixdl=*(im_srcIn+xpos+xshift1-1+(LineLength * (ypos+1)));
			pixel=*(im_srcIn+xpos+xshift1+(LineLength * (ypos+1)));
			pixfl=*(im_srcIn+xpos+xshift1+1+(LineLength * (ypos+1)));


			pixar=*(im_srcIn+xpos+xshift2-1+(LineLength * (ypos)));  	
			pixbr=*(im_srcIn+xpos+xshift2+(LineLength * (ypos)));    
			pixcr=*(im_srcIn+xpos+xshift2+1+(LineLength * (ypos)));  

			pixdr=*(im_srcIn+xpos+xshift2-1+(LineLength * (ypos+1)));
			pixer=*(im_srcIn+xpos+xshift2+(LineLength * (ypos+1)));
			pixfr=*(im_srcIn+xpos+xshift2+1+(LineLength * (ypos+1)));

			pixam=*(im_srcIn+xpos+xshift3-1+(LineLength * (ypos)));  	
			pixbm=*(im_srcIn+xpos+xshift3+(LineLength * (ypos)));    
			pixcm=*(im_srcIn+xpos+xshift3+1+(LineLength * (ypos)));  

			pixdm=*(im_srcIn+xpos+xshift3-1+(LineLength * (ypos+1)));
			pixem=*(im_srcIn+xpos+xshift3+(LineLength * (ypos+1)));
			pixfm=*(im_srcIn+xpos+xshift3+1+(LineLength * (ypos+1)));

			pixax=*(im_srcIn+xpos+xshift4-1+(LineLength * (ypos)));  	
			pixbx=*(im_srcIn+xpos+xshift4+(LineLength * (ypos)));    
			pixcx=*(im_srcIn+xpos+xshift4+1+(LineLength * (ypos)));  

			pixdx=*(im_srcIn+xpos+xshift4-1+(LineLength * (ypos+1)));
			pixex=*(im_srcIn+xpos+xshift4+(LineLength * (ypos+1)));
			pixfx=*(im_srcIn+xpos+xshift4+1+(LineLength * (ypos+1)));

			dm=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
			dz=abs((pixam+(2*pixbm)+pixcm)-(pixdm+(2*pixem)+pixfm));
			dl=abs((pixal+(2*pixbl)+pixcl)-(pixdl+(2*pixel)+pixfl));
			dr=abs((pixar+(2*pixbr)+pixcr)-(pixdr+(2*pixer)+pixfr));
			dx=abs((pixax+(2*pixbx)+pixcx)-(pixdx+(2*pixex)+pixfx));
							
			if( (dm> Limit || dl> Limit || dr > Limit || dz > Limit || dx > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
			{			
				result=ypos;
			
				ypos=cam1dy;				
			}
			
		

		}
	}

	return result;
}

//Finds coordinates at which the neck lip patterns is found.
//im_srcIn: source image, grayscale.
//NeckSide: Coordinates of left or right neck point.
//left: true = find left lip pattern, false = find right lip pattern.
SinglePoint CXAxisView::FindNeckLipPat(BYTE* im_srcIn, SinglePoint NeckSide, bool left)
{
	SinglePoint result;
	
	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	int r,q;
	
	int LineLength=1024;
	int LineLength2=60;
	int xpos=1;
	int xpos2=0;
	int ypos=1;
	int ypos2=0;
	int xpos3=0;

	float sumxy=0;
	float sumx=0;
	float sumy=0;
	float sumxsq=0;
	float sumysq=0;
	float n=2400;
	float f0=0;
	float f1=0;
	float f2=0;
	float corr=0;
	float savedcorr=0.1;
	int savedpos=-50;
	int cnt=0;
	int neckBarY=theapp->jobinfo[pframe->CurrentJobNum].neckBarY*2;
	if(neckBarY <=80) neckBarY=80;
	int neck_offset=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY_offset*2); //newly introduced

	if(NeckSide.x >=50 && NeckSide.x <=1000 && NeckSide.y >=10 && NeckSide.y <=theapp->cam1Height-30 && theapp->jobinfo[pframe->CurrentJobNum].neckBarY >=40)
	{
		if(left==true)
		{
			xpos=NeckSide.x;
				
					for(q=-30+neck_offset; q<=25+neck_offset; q++)
					{
						for(ypos=neckBarY-39; ypos<=neckBarY; ypos++)
						{
							for(xpos3=xpos-49;xpos3<=xpos+10;xpos3++)
							{					
								*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
								xpos2+=1;
							}
							xpos2=0;
							ypos2+=1;
						}
						ypos2=0;
						//calculate correlation

						sumxy=0.0;
						sumx=0.0;
						sumy=0.0;
						sumxsq=0.0;
						sumysq=0.0;
						for(r=0; r < 2400; r++)
						{
							sumxy+= (*(im_matchl+r)) * (*(im_match2+r));
							sumx+= *(im_matchl+r);
							sumy+= *(im_match2+r);
							sumxsq+= (*(im_matchl+r)) * (*(im_matchl+r));
							sumysq+= (*(im_match2+r)) * (*(im_match2+r));
						}
						f0=sumxy-((sumx * sumy)/n);
						f1=sumxsq-((sumx * sumx)/n);
						f2=sumysq-((sumy * sumy)/n);
						if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}else{corr=0;}
						
						if(corr>=savedcorr){savedcorr=corr; savedpos=q;}
						result.y=savedpos+NeckSide.y;
						
						result.x=NeckSide.x-5;
						
					}			
				
			
		}
		else
		{
			xpos=NeckSide.x;
			
				for(q=-30+neck_offset; q<=25+neck_offset; q++)
				{
					for(ypos=neckBarY-39; ypos<=neckBarY; ypos++)
					{
						for(xpos3=xpos-10;xpos3<=xpos+49;xpos3++)
						{					
							*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
							xpos2+=1;
						}
						xpos2=0;
						ypos2+=1;
					}
					ypos2=0;
					//calculate correlation

					sumxy=0.0;
					sumx=0.0;
					sumy=0.0;
					sumxsq=0.0;
					sumysq=0.0;
					for(r=0; r < 2400; r++)
					{
						sumxy+= (*(im_matchr+r)) * (*(im_match2+r));
						sumx+= *(im_matchr+r);
						sumy+= *(im_match2+r);
						sumxsq+= (*(im_matchr+r)) * (*(im_matchr+r));
						sumysq+= (*(im_match2+r)) * (*(im_match2+r));
					}
					f0=sumxy-((sumx * sumy)/n);
					f1=sumxsq-((sumx * sumx)/n);
					f2=sumysq-((sumy * sumy)/n);
					if( sqrtf(f1*f2)!=0.0 )
					{
						corr=f0/sqrtf(f1*f2);
					}
					else
					{corr=0;}
						
					if(corr>=savedcorr){savedcorr=corr; savedpos=q;}
					result.y=savedpos+NeckSide.y;
					result.x=NeckSide.x+5;
						
				}			
			
		}
	
	}


	
	return result;
}

//Finds coordinates at which the neck lip patterns is found for a bottle with no lip.
//x: x coordinate from which to start the search.
//y: y coordinate from which to start the search.
////left: true = find left lip pattern, false = find right lip pattern.
SinglePoint CXAxisView::FindNeckLipPatUser(BYTE* im_srcIn,int x, int y, bool left)
{
	SinglePoint result;
	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	int r,q;
	
	int LineLength=1024;
	int LineLength2=60;
	int xpos=1;
	int xpos2=0;
	int ypos=1;
	int ypos2=0;
	int xpos3=0;

	float sumxy=0;
	float sumx=0;
	float sumy=0;
	float sumxsq=0;
	float sumysq=0;

	float f0=0;
	float f1=0;
	float f2=0;
	float corr=0;
	float savedcorr=0.3;
	int savedpos=-50;
	int cnt=0;
	int cnt2=0;
	int cnt3=0;
	int f=0;
	int ySize=theapp->jobinfo[pframe->CurrentJobNum].lipSize;
	float n=60 *(40+ySize);
	
	
	if(x > 30 && x <=1000 && y <=theapp->cam1Height-30 && y >=40)
	{
		if(left==true)
		{
			xpos=x;
									
					for(q=-30; q<=25; q++)
					{
						for(ypos=y; ypos<=y+39+ySize; ypos++)
						{
							for(xpos3=xpos-30;xpos3<=xpos+29;xpos3++)
							{					
								*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
								xpos2+=1;
								cnt+=1;
							}
							xpos2=0;
							ypos2+=1;
						}
						ypos2=0;
						cnt2+=1;
											
						//calculate correlation

						sumxy=0.0;
						sumx=0.0;
						sumy=0.0;
						sumxsq=0.0;
						sumysq=0.0;
						for(r=0; r < cnt; r++)
						{
							sumxy+= (*(im_matchl+r)) * (*(im_match2+r));
							sumx+= *(im_matchl+r);
							sumy+= *(im_match2+r);
							sumxsq+= (*(im_matchl+r)) * (*(im_matchl+r));
							sumysq+= (*(im_match2+r)) * (*(im_match2+r));
						}
						cnt=0;
						f0=sumxy-((sumx * sumy)/n);
						f1=sumxsq-((sumx * sumx)/n);
						f2=sumysq-((sumy * sumy)/n);
						if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}else{corr=0;}
					
						
						if(q <= largestQ+3 && q >=largestQ-3) corr+=0.1;
						if(corr>=savedcorr)
						{
							theapp->lcorr=savedcorr=corr; 
							savedpos=q;

						}
						result.y=savedpos+y;
					
						result.x=x-5;
						
			}
			if(initLip==false) {statSaveLeft[savedpos+30].accum=200;largestQ=savedpos;}
		
			updateStats(savedpos,true);
		
		}
		else
		{
			xpos=x;
			
				for(q=-30; q<=25; q++)
				{
					for(ypos=y; ypos<=y+39+ySize; ypos++)
					{
						for(xpos3=xpos-29;xpos3<=xpos+30;xpos3++)
						{					
							*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
							xpos2+=1;
							cnt+=1;
						}
						xpos2=0;
						ypos2+=1;
					}
					ypos2=0;
					//calculate correlation

					sumxy=0.0;
					sumx=0.0;
					sumy=0.0;
					sumxsq=0.0;
					sumysq=0.0;
					for(r=0; r < cnt; r++)
					{
						sumxy+= (*(im_matchr+r)) * (*(im_match2+r));
						sumx+= *(im_matchr+r);
						sumy+= *(im_match2+r);
						sumxsq+= (*(im_matchr+r)) * (*(im_matchr+r));
						sumysq+= (*(im_match2+r)) * (*(im_match2+r));
					}
					cnt=0;
					f0=sumxy-((sumx * sumy)/n);
					f1=sumxsq-((sumx * sumx)/n);
					f2=sumysq-((sumy * sumy)/n);
					if( sqrtf(f1*f2)!=0.0 )
					{
						corr=f0/sqrtf(f1*f2);
					}
					else
					{corr=0;}
					
					if(q <= largestQR+3 && q >=largestQR-3)  corr+=0.1;
					
					if(corr>=savedcorr)
					{
						theapp->rcorr=savedcorr=corr; 
						savedpos=q;
					}
					result.y=savedpos+y;
					result.x=x+5;
						
				}
				if(initLip==false) {statSaveRight[savedpos+30].accum=200; initLip=true;largestQR=savedpos;}
			
				updateStats(savedpos,false);
		
		}
	//	result.y-=LipDiffr;
	
	}


	
	return result;
}

//NOT USED, Find lip pattern location for no lip bottles.
SinglePoint CXAxisView::FindNeckLipPatUser2(BYTE* im_srcIn,int x, int y, bool left)    //Not being Used
{ 

	SinglePoint result;
	//short PointsX[32];
	//short PointsY[32];
	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	int r,q;
	
	int LineLength=1024;
	int LineLength2=60;
	int xpos=1;
	int xpos2=0;
	int ypos=1;
	int ypos2=0;
	int xpos3=0;

	float sumxy=0;
	float sumx=0;
	float sumy=0;
	float sumxsq=0;
	float sumysq=0;

	float f0=0;
	float f1=0;
	float f2=0;
	float corr=0;
	float savedcorr=0.3;
	int savedpos=-50;
	int cnt=0;
	int cnt2=0;
	int cnt3=0;
	int cnt4=1;
	int f=0;
	int ySize=theapp->jobinfo[pframe->CurrentJobNum].lipSize;
	float n=60 *(40+ySize);
	int targetX[100];
	int currentX[100];
	
	int baseValue=0;
	if(x > 30 && x <=1000 && y >=39 && y <=theapp->cam1Height-30)
	{
		if(left==true)
		{
			xpos=x;
			
			for(ypos=1; ypos<39+ySize; ypos++)
			{
				for(xpos3=1;xpos3<59;xpos3++)
				{					
					pixa= *(im_matchl+ xpos3-1 + LineLength2 * (ypos-1));  pixd= *(im_matchl+ xpos3+1 + LineLength2 * (ypos-1));
					pixb= *(im_matchl+ xpos3-1 + LineLength2 * (ypos));    pixe= *(im_matchl+ xpos3+1 + LineLength2 * (ypos));
					pixc= *(im_matchl+ xpos3-1 + LineLength2 * (ypos+1));  pixf= *(im_matchl+ xpos3+1 + LineLength2 * (ypos+1));
					
					d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				
					if( (d1 > 60)  )//&& (d2 > Limit) && (d3 > Limit )
					{
						if(cnt4==1) baseValue=xpos3;
						if(cnt4>1) targetX[cnt4]=xpos3-baseValue;

						xpos3=59;	
					}
					else
					{
						targetX[cnt4]=0;
					}

				}	
				cnt4+=1;
					
			}
				
			
					cnt4=0;		
					for(q=-30; q<=25; q++)
					{
						for(ypos=y; ypos<=y+39+ySize; ypos++)
						{
							for(xpos3=xpos-30;xpos3<=xpos+29;xpos3++)
							{					
								pixa= *(im_srcIn+ xpos3-1 + LineLength * (ypos-1+q));    pixd= *(im_srcIn+ xpos3+1 + LineLength * (ypos-1+q));
								pixb= *(im_srcIn+ xpos3-1 + LineLength * (ypos+q));      pixe= *(im_srcIn+ xpos3+1 + LineLength * (ypos+q));
								pixc= *(im_srcIn+ xpos3-1 + LineLength * (ypos+1+q));    pixf= *(im_srcIn+ xpos3+1 + LineLength * (ypos+1+q));
					
								d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

								if( (d1 > 60)  )//&& (d2 > Limit) && (d3 > Limit )
								{
										if(cnt4==1) baseValue=xpos3;
										if(cnt4>1) currentX[cnt4]=xpos3-baseValue;

									xpos3=xpos+30;	
								}
								else
								{
									currentX[cnt4]=0;
								}
							
							}
							cnt4+=1;
					
						}
				
						cnt2+=1;
										
						
						//calculate correlation

						sumxy=0.0;
						sumx=0.0;
						sumy=0.0;
						sumxsq=0.0;
						sumysq=0.0;
						for(r=1; r < cnt4-1; r++)
						{
							sumxy+= targetX[r] * currentX[r];
							sumx+= targetX[r];
							sumy+= currentX[r];
							sumxsq+= targetX[r] * targetX[r];
							sumysq+= currentX[r] * currentX[r];
						}
						
						f0=sumxy-((sumx * sumy)/cnt4);
						f1=sumxsq-((sumx * sumx)/cnt4);
						f2=sumysq-((sumy * sumy)/cnt4);
						if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}else{corr=0;}
						
						cnt4=0;
											
						if(q <= largestQ+3 && q >=largestQ-3) corr+=0.1;
						if(corr>=savedcorr)
						{
							theapp->lcorr=savedcorr=corr; 
							savedpos=q;

							cnt3+=1;
						
						}
						result.y=savedpos+y;
					
						result.x=x-5;
						
			}
			if(initLip==false) {statSaveLeft[savedpos+30].accum=200;largestQ=savedpos;}
		
			updateStats(savedpos,true);
		
		}
		else
		{
			xpos=x;
				for(q=-30; q<=25; q++)
				{
					for(ypos=y; ypos<=y+39+ySize; ypos++)
					{
						for(xpos3=xpos-29;xpos3<=xpos+30;xpos3++)
						{					
							*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
							xpos2+=1;
							cnt+=1;
						}
						xpos2=0;
						ypos2+=1;
					}
					ypos2=0;
					//calculate correlation

					sumxy=0.0;
					sumx=0.0;
					sumy=0.0;
					sumxsq=0.0;
					sumysq=0.0;
					for(r=0; r < cnt; r++)
					{
						sumxy+= (*(im_matchr+r)) * (*(im_match2+r));
						sumx+= *(im_matchr+r);
						sumy+= *(im_match2+r);
						sumxsq+= (*(im_matchr+r)) * (*(im_matchr+r));
						sumysq+= (*(im_match2+r)) * (*(im_match2+r));
					}
					cnt=0;
					f0=sumxy-((sumx * sumy)/n);
					f1=sumxsq-((sumx * sumx)/n);
					f2=sumysq-((sumy * sumy)/n);
					if( sqrtf(f1*f2)!=0.0 )
					{
						corr=f0/sqrtf(f1*f2);
					}
					else
					{corr=0;}
						//corr=f0/sqrtf(f1*f2);
					//corr=corr+statSaveRight[q+30].percent;
					if(q <= largestQR+3 && q >=largestQR-3)  corr+=0.1;
					//if(largestQR>=q-6 && largestQR<=q+6)  corr+=0.05;
					if(corr>=savedcorr)
					{
						theapp->rcorr=savedcorr=corr; 
						savedpos=q;
					}
					result.y=savedpos+y;
					result.x=x+5;
						
				}
				if(initLip==false) {statSaveRight[savedpos+30].accum=200; initLip=true;largestQR=savedpos;}
			
				updateStats(savedpos,false);
		
		}
	
	
	}


	
	return result;
}

void CXAxisView::clearLipStats(int q)
{
	for(int i=0; i<=59; i++)
	{
		statSaveLeft[i].accum=0;
		statSaveRight[i].accum=0;
		statSaveLeft[i].percent=0;
		statSaveRight[i].percent=0;
	}
	if(q!=0)
	{
	
	}
}

void CXAxisView::updateStats(int q,bool left)    //Being Called from Multiple places But I dont Understand this functions purpose..Jim?
{
	float total=0;
	q=q+30;
	int largestLeft=0;
	int largestRight=0;
	if(left==true)
	{
		//add new value
		statSaveLeft[q].accum+=1;
		//get totals
		for(int i=1; i<=56; i++)
		{
			total+=statSaveLeft[i].accum;
			if(statSaveLeft[i].accum>=largestLeft)
			{
				largestLeft=statSaveLeft[i].accum;
				largestQ=i-30;
			}
		}
		if(total!=0&& statSaveLeft[q].accum > 1) statSaveLeft[q].percent=statSaveLeft[q].accum/total;
		statSaveLeft[q].percent=statSaveLeft[q].percent/10;
		if(statSaveLeft[q].percent>=0.1) statSaveLeft[q].percent=0.1;
	}
	else
	{
		//add new value
		statSaveRight[q].accum+=1;
		//get totals
		for(int j=1; j<=56; j++)
		{
			total+=statSaveRight[j].accum;
			if(statSaveRight[j].accum>=largestRight)
			{
				largestRight=statSaveRight[j].accum;
				largestQR=j-30;
			}
		}
		if(total!=0 && statSaveRight[q].accum > 1) statSaveRight[q].percent=statSaveRight[q].accum/total;
		statSaveRight[q].percent=statSaveRight[q].percent/10;
		if(statSaveRight[q].percent>=0.1) statSaveRight[q].percent=0.1;
	}
}

//NOT USED, Find lip pattern location for no lip bottles.
SinglePoint CXAxisView::FindNeckLipPatNoLip(BYTE* im_srcIn, SinglePoint NeckSide, bool left)
{
	SinglePoint result;
	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	int r,q;
	
	int LineLength=1024;
	int LineLength2=60;
	int xpos=1;
	int xpos2=0;
	int ypos=1;
	int ypos2=0;
	int xpos3=0;

	float sumxy=0;
	float sumx=0;
	float sumy=0;
	float sumxsq=0;
	float sumysq=0;
	float n=2400;
	float f0=0;
	float f1=0;
	float f2=0;
	float corr=0;
	float savedcorr=0.3;
	int savedpos=-50;
	int cnt=0;int neckBarY=theapp->jobinfo[pframe->CurrentJobNum].neckBarY*2;
	
	if(NeckSide.x >=10 && NeckSide.x <=1000 && NeckSide.y >=10 && NeckSide.y <=theapp->cam1Height-30 && theapp->jobinfo[pframe->CurrentJobNum].neckBarY >=40)
	{
		if(left==true)
		{
			xpos=NeckSide.x;
				
					for(q=-30; q<=25; q++)
					{
						for(ypos=neckBarY-39+10; ypos<=neckBarY+10; ypos++)
						{
							for(xpos3=xpos-29;xpos3<=xpos+30;xpos3++)
							{					
								*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
								xpos2+=1;
							}
							xpos2=0;
							ypos2+=1;
						}
						ypos2=0;
						//calculate correlation

						sumxy=0.0;
						sumx=0.0;
						sumy=0.0;
						sumxsq=0.0;
						sumysq=0.0;
						for(r=0; r < 2400; r++)
						{
							sumxy+= (*(im_matchl+r)) * (*(im_match2+r));
							sumx+= *(im_matchl+r);
							sumy+= *(im_match2+r);
							sumxsq+= (*(im_matchl+r)) * (*(im_matchl+r));
							sumysq+= (*(im_match2+r)) * (*(im_match2+r));
						}
						f0=sumxy-((sumx * sumy)/n);
						f1=sumxsq-((sumx * sumx)/n);
						f2=sumysq-((sumy * sumy)/n);
						if( sqrtf(f1*f2)!=0.0 ) {corr=f0/sqrtf(f1*f2);}else{corr=0;}
						
						if(corr>=savedcorr){savedcorr=corr; savedpos=q;}
						result.y=savedpos+NeckSide.y;
						
						result.x=NeckSide.x-5;
						
					}			
				
			
		}
		else
		{
			xpos=NeckSide.x;
			
				for(q=-30; q<=25; q++)
				{
					for(ypos=neckBarY-39+10; ypos<=neckBarY+10; ypos++)
					{
						for(xpos3=xpos-30;xpos3<=xpos+29;xpos3++)
						{					
							*(im_match2+ xpos2+ LineLength2 *(ypos2))= *(im_srcIn+ xpos3 + LineLength * (ypos+q));
								
							xpos2+=1;
						}
						xpos2=0;
						ypos2+=1;
					}
					ypos2=0;
					//calculate correlation

					sumxy=0.0;
					sumx=0.0;
					sumy=0.0;
					sumxsq=0.0;
					sumysq=0.0;
					for(r=0; r < 2400; r++)
					{
						sumxy+= (*(im_matchr+r)) * (*(im_match2+r));
						sumx+= *(im_matchr+r);
						sumy+= *(im_match2+r);
						sumxsq+= (*(im_matchr+r)) * (*(im_matchr+r));
						sumysq+= (*(im_match2+r)) * (*(im_match2+r));
					}
					f0=sumxy-((sumx * sumy)/n);
					f1=sumxsq-((sumx * sumx)/n);
					f2=sumysq-((sumy * sumy)/n);
					if( sqrtf(f1*f2)!=0.0 )
					{
						corr=f0/sqrtf(f1*f2);
					}
					else
					{corr=0;}
						
					if(corr>=savedcorr){savedcorr=corr; savedpos=q;}
					result.y=savedpos+NeckSide.y;
					result.x=NeckSide.x+5;
						
				}			
			
		}
	
	}

	return result;
}

//Find coordinate of where the lip intersects the neck.
//im_srcIn: source image, grayscale.
//NeckSide: coordinates of neck side point.
//left: true = find left coordinates, false = find right coordinates.
SinglePoint CXAxisView::FindNeckLip(BYTE* im_srcIn, SinglePoint NeckSide, bool left)
{
	SinglePoint result;
	result.x=result.y=480;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	
	int LineLength=1024;
	int xpos=1;
	int ypos=1;


	
	if(NeckSide.x >50 && NeckSide.x <=1000 && NeckSide.y >50 && NeckSide.y <=theapp->cam1Height-30)
	{
		if(left==true)
		{
			for(xpos=NeckSide.x-16; xpos>=NeckSide.x-26; xpos--)
			{
				for(ypos=NeckSide.y;ypos>NeckSide.y-50;ypos--)
				{	
					pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
					pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
					pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

					pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
					pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
					pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

					d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
									
					if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
					{			
						//result.y=ypos;
						//result.x=xpos;
						//if(ypos<=result.y){ result.y=ypos; result.x=xpos;}
						result.y=ypos;
						result.x=xpos;
						
						ypos=0;
						xpos=0;
					
								
					}
				}
			}

		}
		else
		{
			for(xpos=NeckSide.x+16; xpos<=NeckSide.x+26; xpos++)
			{
				for(ypos=NeckSide.y;ypos>NeckSide.y-50;ypos--)
				{	
					pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
					pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
					pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

					pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
					pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
					pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

					d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
								
					if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
					{			
						//if(ypos<=result.y){ result.y=ypos; result.x=xpos;}
						
						
						result.y=ypos;
						result.x=xpos;
						
						ypos=0;
						xpos=1000;
								
					}
				}
			}
		}
	}



	return result;
}

//Used to find the left and right coordinates of the bottle neck sides.
//im_srcIn: source image, grayscale.
//NeckPos: y coordinate of middle of neck line.
//CapPos: x coordinate of left or right cap side.
//left: true = find left neck coordinate, false = find right neck coordinate.
SinglePoint CXAxisView::FindNeckSide(BYTE* im_srcIn, int NeckPos, int CapPos, bool left)
{
	SinglePoint result;

	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	int d2=0;
	int d3=0;
	int LineLength=1024;
	int xpos=1;
	int ypos=NeckPos;

	
	if(ypos<=theapp->cam1Height-30 && ypos >=1 && CapPos+60 <=1024 && CapPos-60 >=0)
	{
		if(left==true)
		{

			
			for(xpos=CapPos-60;xpos<XEnd;xpos++)
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos-1)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos-1)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos-4)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos-4)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos-3)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos-3)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos-2)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos-2)));

				d2=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));	
				
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos+4)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos+4)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos+3)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos+3)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos+2)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+2)));

				d3=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if( (d1 > Limit)  || (d2 > Limit) || (d3 > Limit )  )
				{			
					result.y=ypos;
					result.x=xpos;
					xpos=XEnd;
						
				}
			}
		}
		else
		{
			for(xpos=CapPos+60;xpos>XStart;xpos--)
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos-1)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos-1)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos-4)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos-4)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos-3)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos-3)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos-2)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos-2)));

				d2=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));	
				
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos+4)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos+4)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos+3)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos+3)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos+2)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+2)));

				d3=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));

				if( (d1 > Limit)  || (d2 > Limit) || (d3 > Limit )  )
				{
					result.y=ypos;
					result.x=xpos;
					xpos=XStart;
						
				}
			}
		}
	}

	return result;
}

//Used to find the left top and right top coordinates of the cap.
//im_srcIn: source image, grayscale.
//SideDist: x coordinate of the left or right vertical cap line.
//left: true = find left top cap coordinate, false = find right top cap coordinate.
SinglePoint CXAxisView::FindCapTop(BYTE* im_srcIn, int SideDist, bool left)
{
	SinglePoint result;

	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	
	int LineLength=1024;
	int xpos=SideDist;
	int ypos=1;

	int ystart=theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar;
	
	
	if(xpos<=XEnd && xpos >=XStart )
	{
		if(left==true)
		{		
			for(ypos=ystart;ypos<theapp->cam1Height-30;ypos++) //ystart was 40 initially
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
				pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

				pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
				pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
				pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					result.y=ypos;
					result.x=xpos;
					ypos=theapp->cam1Height-30;
							
				}
			}
		}
		else
		{
			for(ypos=ystart;ypos<theapp->cam1Height-30;ypos++) //40
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
				pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

				pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
				pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
				pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					result.y=ypos;
					result.x=xpos;
					ypos=theapp->cam1Height-30;
							
				}
			}
		}
	}

	return result;
}

//Used to find the left and right coordinates of the cap sides.
//im_srcIn: source image, grayscale.
//MidCap: y coordinate of the middle of the cap.
//left: true = find left cap coordinate, false = find right cap coordinate.
SinglePoint CXAxisView::FindCapSide(BYTE* im_srcIn, int MidCap, bool left)
{
	SinglePoint result;

	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	int pixg=0;
	int pixh=0;

	int Limit=40;
	int d1=0;
	
	int LineLength=1024;
	int xpos=10;
	int ypos=MidCap;
	int startx=0;

	
	if(ypos<=theapp->cam1Height-30 && ypos >=1)
	{
		if(left==true)
		{
			for(xpos=512;xpos>XStart;xpos--)
			{
				pixa=*(im_srcIn+xpos+1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+xpos+1+(LineLength * (ypos-1)));    
				pixc=*(im_srcIn+xpos+(LineLength * (ypos-1)));  
				pixd=*(im_srcIn+xpos+(LineLength * (ypos)));
				pixe=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
				pixf=*(im_srcIn+xpos-2+(LineLength * (ypos)));    
				pixg=*(im_srcIn+xpos-3+(LineLength * (ypos)));  
				pixh=*(im_srcIn+xpos-4+(LineLength * (ypos)));
				if(pixa >= 240 && pixb >= 240 && pixc >= 240 && pixd >= 240 && pixe >= 240 && pixf >= 240 && pixg >= 240 && pixh >= 240) {startx=xpos-20; xpos=XStart;}else{startx=XStart;}
			}
			
			if(startx<=20) startx=XStart;

			for(xpos=startx;xpos<XEnd;xpos++)
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos-1)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos-1)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					result.y=ypos;
					result.x=xpos;
					xpos=XEnd;
						
				}
			}
		}
		else
		{

			for(xpos=512;xpos<XEnd;xpos++)
			{
				pixa=*(im_srcIn+xpos+1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+xpos+1+(LineLength * (ypos-1)));    
				pixc=*(im_srcIn+xpos+(LineLength * (ypos-1)));  
				pixd=*(im_srcIn+xpos+(LineLength * (ypos)));
				pixe=*(im_srcIn+xpos+2+(LineLength * (ypos)));  	
				pixf=*(im_srcIn+xpos+3+(LineLength * (ypos)));    
				pixg=*(im_srcIn+xpos+4+(LineLength * (ypos)));  
				pixh=*(im_srcIn+xpos+5+(LineLength * (ypos)));
				if(pixa >= 240 && pixb >= 240 && pixc >= 240 && pixd >= 240&& pixe >= 240 && pixf >= 240 && pixg >= 240 && pixh >= 240) {startx=xpos+20; xpos=XEnd;}else{startx=XEnd;}
			}

			if(startx>=XEnd) startx=XEnd;

			for(xpos=startx;xpos>XStart;xpos--)
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos-1)));  pixd=*(im_srcIn+xpos+1+(LineLength * (ypos-1)));	
				pixb=*(im_srcIn+xpos-1+(LineLength * (ypos)));    pixe=*(im_srcIn+xpos+1+(LineLength * (ypos)));
				pixc=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));  pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					result.y=ypos;
					result.x=xpos;
					xpos=XStart;
						
				}
			}
		}
	}

	return result;
}

//NOT USED
void CXAxisView::LearnBand(BYTE *im_srcIn)
{

}


//Find how many points in cam1 are beyond the low fill threshold.
//x: x coordinate from which to search for low fill.
//y: y coordinate from which to search for low fill.
int CXAxisView::CheckFill(BYTE *im_srcHV, int x,int y)
{								
	int result=0;
	int pixa=0;
	
	int LineLength=theapp->cam1Width;
	int lx=0;
	int ly=0;
	int ypos=0;
	int xpos=0;
	for (ypos=y; ypos <y+theapp->SavedFillH; ypos++)
	{
			for (xpos=x; xpos < x+theapp->SavedFillW; xpos++)
			{
				if((xpos >0) &&(xpos<1000) &&(ypos>0) &&(ypos<(theapp->cam1Height-2)))
				{
			
					pixa=*(im_srcHV + xpos + (LineLength * (ypos)));
			
					result+=pixa;
				}
			}
	}

	result=result/((theapp->SavedFillH)*(theapp->SavedFillW));

	return result;
}

//With extra light option enabled, check how many points in cam1 are beyond the low fill threshold.
//im_srcIn: source image, grayscale.
//x: x coordinate from which to search for low fill.
//y: y coordinate from which to search for low fill.
int CXAxisView::CheckFill2(BYTE *im_srcHV, int x,int y)   //gets reduced image
{								
	int result=0;
	int pixa=0;
	
	int LineLength=(theapp->cam1Width/2)*4;
	int lx=0;
	int ly=0;
	int ypos=0;
	int xpos=0;
	//x=x/2;
	
		for (ypos=y; ypos < y+2; ypos++)
		{
			for (xpos=x*4; xpos < x*4+(30*4); xpos+=4)
			{
				if(xpos>=0 && (xpos<=LineLength) && ypos >=0 && ypos <=(theapp->cam1Height)/2-30) // checking for bounds
				{
				pixa=*(im_srcHV + xpos + (LineLength * (ypos)));
			
				if(pixa==0)result+=1;
				}
			}
		}

	return result;
}

/////////////////////////////////////////////////////////////////////////////
// CXAxisView drawing


//NOT USED
bool CXAxisView::BackLight(BYTE *im_srcHV)
{
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	bool result1, result2, result;
	
	int xpos=20;
	int xpos2=XEnd;
	int p=5;
	int LineLength=1024;
	int thresh=250;
	
	pixa=*(im_srcHV + xpos +(LineLength * p));
	pixb=*(im_srcHV + xpos+1 + (LineLength * p));
	pixc=*(im_srcHV + xpos+2 + (LineLength * p));
	pixd=*(im_srcHV + xpos +(LineLength * p+1));
	pixe=*(im_srcHV + xpos+1 + (LineLength * p+1));
	pixf=*(im_srcHV + xpos+2 + (LineLength * p+1));
	if(pixa < thresh || pixb < thresh || pixc < thresh || pixd < thresh || pixe < thresh || pixf < thresh){ result1=false;}else{ result1=true;}
	
	pixa=*(im_srcHV + xpos2 +(LineLength * p));
	pixb=*(im_srcHV + xpos2+1 + (LineLength * p));
	pixc=*(im_srcHV + xpos2+2 + (LineLength * p));
	pixd=*(im_srcHV + xpos2 +(LineLength * p+1));
	pixe=*(im_srcHV + xpos2+1 + (LineLength * p+1));
	pixf=*(im_srcHV + xpos2+2 + (LineLength * p+1));
	if(pixa < thresh || pixb < thresh || pixc < thresh || pixd < thresh || pixe < thresh || pixf < thresh){ result2=false;}else{ result2=true;}
	
	if(result1==false && result2==false) {result=false;}else {result=true;}
	return result;

}

/*
BYTE* CXAxisView::Kernel(BYTE *im_srcHV,int x, int y, int dx, int dy)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix7=0;
	int pix8=0;
	int pix9=0;
	int result=0;
		
	int LineLength=640;
	int column=480;
	int startx=x;
	int endx=dx;
	int starty=max(y,dy);
	int endy=dy+100;
	int lengthx=endx-startx;
	if(startx==0)startx=1;
	
	for(int i=starty+1; i<endy-1; i++)
	{
		for(int k=startx+1; k<endx-1; k++)
		{
			pix1=*((im_srcHV+k -1 +LineLength * (i-1)));
			pix2=*((im_srcHV+k +LineLength * (i-1)));
			pix3=*((im_srcHV+k +1 +LineLength * (i-1)));

			pix4=*((im_srcHV+k -1 +LineLength * (i)));
			pix5=*((im_srcHV+k +LineLength * (i)));
			pix6=*((im_srcHV+k +1 +LineLength * (i)));

			pix7=*((im_srcHV+k -1 +LineLength * (i+1)));
			pix8=*((im_srcHV+k +LineLength * (i+1)));
			pix9=*((im_srcHV+k +1 +LineLength * (i+1)));

			result=(pix7+2*pix8+pix9)-(pix1+2*pix2+pix3);//1st x deriv
			//result=4*pix5-(pix2 + pix4 + pix6 + pix8);//2nd xy laplacian
			//result=pix1-pix3+(2*pix4)-(2*pix6)+pix7-pix9;  //sobel 1st x deriv
			//result=pix1-(2*pix2)+pix3+(2*pix4)-(4*pix5)+(2*pix6)+pix7-(2*pix8)+pix9;//2nd order sobel deriv in x
			//result=-pix1+pix3+pix7-pix9;//2nd sobel xy
			*(im_result +k +LineLength *i )=result;
		}
	}
	return im_result;
}
*/

//NOT USED
double CXAxisView::GetMin(int numvals, double a, double b, double c, double d)
{
	double result=0;
	double t1=0;
	double t2=0;
	double t3=0;
	double t4=0;


	switch(numvals)
	{
	case 1:
		result=a;
		break;

	case 2:
		result=__min(a,b);
		break;

	case 3:
		t1=__min(a,b);
		result=__min(t1, c);
		break;

	case 4:
		t1=__min(a,b);
		t2=__min(t1, c);
		result=__min(t2,d);
		break;

	}

	return result;
}

//NOT USED
double CXAxisView::GetMax(int numvals, double a, double b, double c, double d)
{
	double result=0;
	double t1=0;
	double t2=0;
	double t3=0;
	double t4=0;
	double t5=0;


	switch(numvals)
	{
	case 1:
		result=a;
		break;

	case 2:
		result=__max(a,b);
		break;

	case 3:
		t1=__max(a,b);
		result=__max(t1, c);
		break;

	case 4:
		t1=__max(a,b);
		t2=__max(t1, c);
		result=__max(t2,d);
		break;

	}

	return result;
}


//NOT USED
int CXAxisView::GetBestAvg(int p1, int p2, int p3)
{
	int resultp=0;
	int av=(p1+p2+p3)/3;
	int rr1=abs(av-p1);
	int rr2=abs(av-p2);
	int rr3=abs(av-p3);

	if(rr1>=rr2&&rr1>=rr3)resultp=(p2+p3)/2;
	if(rr2>=rr1&&rr2>=rr3)resultp=(p1+p3)/2;
	if(rr3>=rr1&&rr3>=rr2)resultp=(p1+p2)/2;

	return resultp;
}

//NOT USED
void CXAxisView::OnRead(char* pFileName)
{
	CVisByteImage image;

		try
		{
			image.ReadFile(pFileName);

			imageBW.Allocate(image.Rect());

			image.CopyPixelsTo(imageBW);
			im_data=imageBW.PixelAddress(0,0,1);
			ReloadOnce=true;
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Give up trying to read an image from this file.
			// Warn the user.
		//	AfxMessageBox("The file specified could not be opened.");

			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();

			//return FALSE;
		}
		
		//imI=(unsigned char *)image2.PixelAddress(0,0,1);
		//imI=(unsigned char *)image2.PtrToFirstPixelInRow(0);
		//im_read=Convert(imI);
		//Inspect(im_read,imgBuf0);
		InvalidateRect(false);
}


void CXAxisView::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		TurnOff23=false;
		KillTimer(1);
	}

	//Timer used to start XAxisInspect(). Called from Display().
	if (nIDEvent==2) //	ML- THIS STARTS THE INSPECTION
	{
		KillTimer(2);
		Inspect(im_cam1Color, im_cam2Color, im_cam3Color, im_cam1,im_cam2,im_cam3,sobel_cam2,sobel_cam3, im_red,im_red2, im_red3, im_hue, im_hue2, im_hue3);
	}

	CView::OnTimer(nIDEvent);
}

//User Inspection, NOT USED
SinglePoint CXAxisView::CheckU2Band2(BYTE *im_srcIn, int TBYDiff, int XWidth, int offset, bool Left)
{
	SinglePoint result;

	result.x=result.y=0;
	int xpos=0;
	int count=0;
	if(TBYDiff>=100 || TBYDiff<=0) TBYDiff=0;
	int ypos=offset+TBYDiff;
	if(ypos<=0) ypos=1;
	if(ypos>=theapp->cam1Height-30) ypos=theapp->cam1Height-30;
	int xoffset=10;
	int yposwidth=20;
	int LineLength=1024;
	int avgShade=0;
	int d1=0;
	int pixa=0;
	int pixb,pixc,pixd,pixe,pixf,pixg,pixh,pixi,pixj,pixk,pixl,pixm,pixn,pixo,pixp,pixq,pixr;
	int Thresh=40;
	int dark=50;
	int lowLim=2*(100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2);
	if (lowLim>=200) lowLim=1000;

	int LineLengthL=theapp->cam2Width;
	int LineLengthR=theapp->cam3Width;

	int RBand2X=(theapp->jobinfo[pframe->CurrentJobNum].rSWin)*2;
	int LBand2X=(theapp->jobinfo[pframe->CurrentJobNum].lSWin)*2+320;
	int whiteCount=0;
	int countx=0;

	if(Left==true)
	{
		//********Look for the side  435
		for(int x=LBand2X; x>10; x--)
		{	
			pixa=*(im_srcIn+x-1+(LineLengthL * (ypos-1)));  pixd=*(im_srcIn+x+1+(LineLengthL * (ypos-1)));	
			pixb=*(im_srcIn+x-1+(LineLengthL * (ypos)));    pixe=*(im_srcIn+x+1+(LineLengthL * (ypos)));
			pixc=*(im_srcIn+x-1+(LineLengthL * (ypos+1)));  pixf=*(im_srcIn+x+1+(LineLengthL * (ypos+1)));
			
			if(countx< 3){if(pixa>200 && pixb > 200){ whiteCount+=1;}}
			countx+=1;
			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
										
			if( d1 >= Thresh )
			{			
				result.x=xpos=x-12;
						
				x=1;				
			}
		}

		if(xpos-XWidth >=1  && xpos <640 && whiteCount>=2)
		{
			for(int j=xpos; j>xpos-XWidth; j--)
			{
				for(int i=ypos; i<ypos+yposwidth; i++)
				{	
					pixa=*(im_srcIn+j+(LineLengthL * (i-1)));  pixd=*(im_srcIn+j+3+(LineLengthL * (i-1)));	
					pixb=*(im_srcIn+j+(LineLengthL * (i)));    pixe=*(im_srcIn+j+3+(LineLengthL * (i)));
					pixc=*(im_srcIn+j+(LineLengthL * (i+1)));  pixf=*(im_srcIn+j+3+(LineLengthL * (i+1)));
					
					d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
													
					if( d1>=lowLim)		 
					{			
						count+=1;		
						result.y=count;					
					}
				}
			}
		}		
	}
	else  // right
	{
		for(int x=RBand2X; x<639; x++)
		{	
			pixa=*(im_srcIn+x-1+(LineLengthR * (ypos-1)));  pixd=*(im_srcIn+x+1+(LineLengthR * (ypos-1)));	
			pixb=*(im_srcIn+x-1+(LineLengthR * (ypos)));    pixe=*(im_srcIn+x+1+(LineLengthR * (ypos)));
			pixc=*(im_srcIn+x-1+(LineLengthR * (ypos+1)));  pixf=*(im_srcIn+x+1+(LineLengthR * (ypos+1)));

			if(countx< 3) {if(pixa>200 && pixb > 200){ whiteCount+=1;}}
			countx+=1;
				
			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
										
			if( (d1 >= Thresh)  )
			{			
				result.x=xpos=x+12;
						
				x=639;				
			}
		}

		if(xpos+XWidth <640  && xpos >=0 && ypos+yposwidth<=theapp->cam1Height-30 && whiteCount>=2)
		{
			for(int j=xpos; j<xpos+XWidth; j++)
			{
				for(int i=ypos; i<ypos+yposwidth; i++)
				{	
					pixa=*(im_srcIn+j+(LineLengthR * (i-1)));  pixd=*(im_srcIn+j+3+(LineLengthR * (i-1)));	
					pixb=*(im_srcIn+j+(LineLengthR * (i)));    pixe=*(im_srcIn+j+3+(LineLengthR * (i)));
					pixc=*(im_srcIn+j+(LineLengthR * (i+1)));  pixf=*(im_srcIn+j+3+(LineLengthR * (i+1)));
					
					d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
													
					if( d1>=lowLim)		 
					{			
						count+=1;		
						result.y=count;					
					}
				}
			}
		}
	}

	return result;

}

//NOT USED
SinglePoint CXAxisView::FindShoulder(BYTE *im_srcIn, int TopLeft, int TopRight, SinglePoint CapSide, int NeckBar, bool Left)
{
	SinglePoint result;
	result.x=result.y=0;

	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=60;
	int d1=0;
	
	int LineLength=1024;
	int xpos=0;
	int ypos=0;
	int offset=TopLeft-TopRight;
	ypos=NeckBar;
	
	if(Left==true)
	{
		xpos=(CapSide.x+(offset*0.5)-10)+15; 
	}
	else
	{
		xpos=(CapSide.x+(offset*0.5)+10)-15;
	}
	
	
	
	if(xpos>=1 && xpos<=theapp->cam1Width-30  && ypos<=theapp->cam1Height-100 && ypos>=1 )
	{
				
			for(int ypos1=ypos;ypos1<ypos+100;ypos1++)
			{	
				for(int xpos1=xpos-4;xpos1<xpos+4;xpos1++)
				{
					pixa=*(im_srcIn+xpos1-1+(LineLength * (ypos1)));  	
					pixb=*(im_srcIn+xpos1+(LineLength * (ypos1)));    
					pixc=*(im_srcIn+xpos1+1+(LineLength * (ypos1)));  

					pixd=*(im_srcIn+xpos1-1+(LineLength * (ypos1+1)));
					pixe=*(im_srcIn+xpos1+(LineLength * (ypos1+1)));
					pixf=*(im_srcIn+xpos1+1+(LineLength * (ypos1+1)));

					d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
					
					if(pixa <=70 && pixb <=70 && pixc <=70)
					{
						result.y=ypos1;
						result.x=xpos1;
						ypos1=ypos+100;
						xpos1=xpos+4;
								
					}
				}
			}
	
	}

	return result;

}

//NOT USED
void CXAxisView::OnReadLast()
{
	CVisByteImage image;
	try
	{
		image.ReadFile(SavedLast);
		imageBW.Allocate(image.Rect());
		image.CopyPixelsTo(imageBW);
		im_data=imageBW.PixelAddress(0,0,1);

		ReloadOnce=true;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
//		AfxMessageBox("The file specified could not be opened. Do you have enough rejects yet?");

			pframe->prompt_code=9;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
		InvalidateRect(false);
}

//NOT USED
void CXAxisView::OnReadLast2()
{
	CVisByteImage image;
	try
	{
		image.ReadFile(SavedLast1);
		imageBW.Allocate(image.Rect());
		image.CopyPixelsTo(imageBW);
		im_data=imageBW.PixelAddress(0,0,1);

		ReloadOnce=true;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		//AfxMessageBox("The file specified could not be opened. Do you have enough rejects yet?");

			pframe->prompt_code=9;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
		InvalidateRect(false);
}

//NOT USED
void CXAxisView::OnReadLast3()
{
	CVisByteImage image;
	try
	{
		image.ReadFile(SavedLast2);
		imageBW.Allocate(image.Rect());
		image.CopyPixelsTo(imageBW);
		im_data=imageBW.PixelAddress(0,0,1);

		ReloadOnce=true;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
	//	AfxMessageBox("The file specified could not be opened. Do you have enough rejects yet?");
			pframe->prompt_code=9;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
		InvalidateRect(false);
}

//NOT USED
void CXAxisView::OnReadLast4()
{
	CVisByteImage image;
	try
	{
		image.ReadFile(SavedLast3);
		imageBW.Allocate(image.Rect());
		image.CopyPixelsTo(imageBW);
		im_data=imageBW.PixelAddress(0,0,1);

		ReloadOnce=true;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		//AfxMessageBox("The file specified could not be opened. Do you have enough rejects yet?");

			pframe->prompt_code=9;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
		InvalidateRect(false);
}

//NOT USED
void CXAxisView::OnReadC(char* pFileName)
{
	CVisByteImage image;

		try
		{
			image.ReadFile(pFileName);

			imageBW.Allocate(image.Rect());

			image.CopyPixelsTo(imageBW);
			im_data=imageBW.PixelAddress(0,0,1);
			//ReloadOnce=true;
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Give up trying to read an image from this file.
			// Warn the user.
		//	AfxMessageBox("The file specified could not be opened.");
			
			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();


			//return FALSE;
		}
		
		//imI=(unsigned char *)image2.PixelAddress(0,0,1);
		//imI=(unsigned char *)image2.PtrToFirstPixelInRow(0);
		//im_read=Convert(imI);
		//Inspect(im_read,imgBuf0);
		InvalidateRect(false);
}

void CXAxisView::OnLoad(int Job)
{
	CVisByteImage image;
	
	char buf[10];
	char currentpath[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	
	c=itoa(Job,buf,10);
		
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	try
	{
		image.ReadFile(currentpath);
		
		imageBW.Allocate(image.Rect());
//image.SetRect(0,40,640,480);//cam 1
		image.CopyPixelsTo(imageBW);
		im_data=imageBW.PixelAddress(0,0,1);
//im_datax=imageBW.PixelAddress(0,0,1);
		pframe->JobNoGood=false;
		ReloadOnce=true;
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
			// Give up trying to read an image from this file.
			// Warn the user.
	//	AfxMessageBox("The file specified could not be opened. You will need to setup a new job");

			pframe->prompt_code=5;
			Prompt3 promtp3;
			promtp3.DoModal();

		pframe->JobNoGood=true;
			//return FALSE;
	}
	im_dataC = im_dataC2 = im_dataC3 = pframe->m_pcamera->m_imageProcessed1.GetData();
}

//Save image of lip pattern to hard drive.
//im_sr: Holds pointer to image of lip pattern to save.
void CXAxisView::SavePattern(BYTE *im_sr)
{
	char buf[10];
	char currentpath[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive

	c =itoa(pframe->CurrentJobNum,buf,10);
	
		

	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	//$$ Added to store the NEck Lip X co ordinates..
//	*(im_sr+15+(1024*10))=NeckLSide.x; 
//	*(im_sr+18+(1024*10))=NeckRSide.x;

/*	for(int j=0;j<10;j++)
	{
		for( int i=0;i <300;i++)
		{

		//	*(im_sr+i)=0;
			*(im_sr+i+(1024 * (j)))=0;
		}
	}
*/
	//$$
	
	CVisByteImage image(theapp->cam1Width,theapp->cam1Height,1,-1,im_sr);
	//image.SetRect(0,40,640,480);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = currentpath;

		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
	//	AfxMessageBox("The pat file could not be saved.");

			pframe->prompt_code=6;
			Prompt3 promtp3;
			promtp3.DoModal();


		
	}

}

//Save color image to hard drive.
//im_sr: Holds pointer to image to save.
//Who: camera number of which camera the image came from.
bool CXAxisView::OnSaveC(BYTE *im_sr, int who)
{
	char buf[10];
	
	if(piccount>=1000) {RolledOver=true; piccount=0;}
	piccount+=1;

	CString c =itoa(piccount,buf,10);
	CString d = c + "saved";
	
	char* pFileName = "c:\\silgandata\\";//drive	
	char currentpath[60];
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);

	strcat(currentpath,".bmp");

	int xdist=1024;
	int ydist=768;

	switch(who)
	{
	case 1:
		xdist=theapp->cam1Width;
		ydist=theapp->cam1Height;
		break;
	case 2:
		xdist=theapp->cam2Width;
		ydist=theapp->cam2Height;
		break;
	case 3:
		xdist=theapp->cam3Width;
		ydist=theapp->cam3Height;
		break;

	case 6:
		xdist=theapp->cam1Width/2;
		ydist=theapp->cam1Height/2;
		break;
	case 7:
		xdist=theapp->cam2Width/2;
		ydist=theapp->cam2Height/2;
		break;
	case 8:
		xdist=theapp->cam3Width/2;
		ydist=theapp->cam3Height/2;
		break;
	}
	
	CVisRGBAByteImage image(xdist,ydist,1,-1,im_sr);
	imageCS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = currentpath;

		imageCS.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
		//AfxMessageBox("The file could not be saved. ");

			pframe->prompt_code=8;
			Prompt3 promtp3;
			promtp3.DoModal();


		
	}

	return true;

}

//Save grayscale image to hard drive.
//im_sr: Holds pointer to image to save.
//Who: camera number of which camera the image came from.
void CXAxisView::OnSaveBW(BYTE *im_sr, int who)
{
	char buf[10];

	
	if(piccount>=10) {RolledOver=true; piccount=0;}
	piccount+=1;

	CString c =itoa(piccount,buf,10);
	CString d = itoa(who,buf,10);
	
	
	char* pFileName = "c:\\silgandata\\";//drive	
	char currentpath[60];
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);
	strcat(currentpath,c);

	strcat(currentpath,".bmp");
	
	int xdist=1024;
	int ydist=768;

	switch(who)
	{
	case 1:
		xdist=theapp->cam1Width;
		ydist=theapp->cam1Height;
		break;
	case 2:
		xdist=theapp->cam2Width;
		ydist=theapp->cam2Height;
		break;
	case 3:
		xdist=theapp->cam3Width;
		ydist=theapp->cam3Height;
		break;
	}
	
	CVisByteImage image(xdist,ydist,1,-1,im_sr);
	imageS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = currentpath;

		imageS.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
//		AfxMessageBox("The file could not be saved. ");
			pframe->prompt_code=8;
			Prompt3 promtp3;
			promtp3.DoModal();


		
	}

}

//NOT USED
void CXAxisView::OnSave(BYTE *im_sr)
{
	char buf[10];
	
	if(piccount>=1000) {RolledOver=true; piccount=0;}
	piccount+=1;

	CString c =itoa(piccount,buf,10);
	CString d = c + Fault;
	
	char* pFileName = "c:\\silgandata\\";//drive	
	char currentpath[60];
	strcpy(currentpath,pFileName);
	strcat(currentpath,d);

	strcat(currentpath,".bmp");
	if(Fault!="Good") 
	{
		badcount+=1;
		if(badcount>=4) SavedLast3=SavedLast2;
		if(badcount>=3) SavedLast2=SavedLast1;
		if(badcount>=2) SavedLast1=SavedLast;
		SavedLast=currentpath;

	}
	
	CVisByteImage image(1024,768,1,-1,im_sr);
	imageS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = currentpath;

		imageS.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
	//	AfxMessageBox("The file could not be saved. ");

		pframe->prompt_code=8;
			Prompt3 promtp3;
			promtp3.DoModal();

		
	}

}

//NOT USED
void CXAxisView::ChooseBestPlacement()
{
	SinglePoint result;

	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	int pixg=0;
	int pixh=0;
	
	int Limit=40;
	int d1=0;
	int startx=0;
	int LineLength=1024;
	int xpos=512;
	int ypos=1;
	int halfACap=50;
	int bestYPosIn=0;
	int bestYPosOut=0;
	int lipX=100;
	int lipY=100;

	if(xpos<=XEnd && xpos >=XStart )
	{	
		//look for the top of the cap in the middle of the picture
		for(ypos=40;ypos<theapp->cam1Height-30;ypos++)
		{	
			pixa=*(im_cam1+xpos-1+(LineLength * (ypos)));  	
			pixb=*(im_cam1+xpos+(LineLength * (ypos)));    
			pixc=*(im_cam1+xpos+1+(LineLength * (ypos)));  

			pixd=*(im_cam1+xpos-1+(LineLength * (ypos+1)));
			pixe=*(im_cam1+xpos+(LineLength * (ypos+1)));
			pixf=*(im_cam1+xpos+1+(LineLength * (ypos+1)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
			if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
			{			
				result.y=ypos;
				result.x=xpos;
				ypos=theapp->cam1Height-30;
							
			}
		}			
	}

	//place the midBarY in the middle of the cap
	ypos=(theapp->jobinfo[pframe->CurrentJobNum].midBarY=(result.y/2)+ halfACap)*2;

	for(xpos=512;xpos>XStart;xpos--)
	{
		pixa=*(im_cam1+xpos+1+(LineLength * (ypos)));  	
		pixb=*(im_cam1+xpos+1+(LineLength * (ypos-1)));    
		pixc=*(im_cam1+xpos+(LineLength * (ypos-1)));  
		pixd=*(im_cam1+xpos+(LineLength * (ypos)));
		pixe=*(im_cam1+xpos-1+(LineLength * (ypos)));  	
		pixf=*(im_cam1+xpos-2+(LineLength * (ypos)));    
		pixg=*(im_cam1+xpos-3+(LineLength * (ypos)));  
		pixh=*(im_cam1+xpos-4+(LineLength * (ypos)));
		if(pixa >= 220 && pixb >= 220 && pixc >= 220 && pixd >= 220 && pixe >= 220 && pixf >= 220 && pixg >= 220 && pixh >= 220) {startx=xpos-60; xpos=XStart;}else{startx=XStart;}
	}

	//place the vertBarX 60 pixels inside the left edge of the cap
	theapp->jobinfo[pframe->CurrentJobNum].vertBarX=(startx+60) + 60 ;

	int savedSideIn=10;	
	int savedSideOut=512;


	if(startx<=20) startx=XStart;
	if(startx>=900) startx=900;
	for(int ypos2=ypos+100;ypos2<=550; ypos2++)
	{
		for(xpos=startx;xpos<XEnd;xpos++)
		{	
			pixa=*(im_cam1+xpos-2+(LineLength * (ypos2-1)));  pixd=*(im_cam1+xpos+1+(LineLength * (ypos2-1)));	
			pixb=*(im_cam1+xpos-2+(LineLength * (ypos2)));    pixe=*(im_cam1+xpos+1+(LineLength * (ypos2)));
	 		pixc=*(im_cam1+xpos-2+(LineLength * (ypos2+1)));  pixf=*(im_cam1+xpos+1+(LineLength * (ypos2+1)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
			if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
			{			
				if(xpos > savedSideIn){ savedSideIn=xpos; bestYPosIn=ypos2;}
				if(xpos < savedSideOut){ savedSideOut=xpos; bestYPosOut=ypos2;}
				xpos=XEnd;
							
			}
		
		}
	}

	xpos=savedSideIn-7;
	bestYPosIn+=6;
	if(bestYPosIn<=101) bestYPosIn=101;
	for(ypos=bestYPosIn;ypos>bestYPosIn-100;ypos--)
	{	
		pixa=*(im_cam1+xpos-1+(LineLength * (ypos)));  	
		pixb=*(im_cam1+xpos+(LineLength * (ypos)));    
		pixc=*(im_cam1+xpos+1+(LineLength * (ypos)));  

		pixd=*(im_cam1+xpos-1+(LineLength * (ypos+1)));
		pixe=*(im_cam1+xpos+(LineLength * (ypos+1)));
		pixf=*(im_cam1+xpos+1+(LineLength * (ypos+1)));

		d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
									
		if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
		{						
			lipY=ypos;
			lipX=xpos;
						
			ypos=bestYPosIn-100;							
		}
	}
	lipY+=16;	
	bestYPosOut+=35;
	theapp->jobinfo[pframe->CurrentJobNum].neckBarY=lipY/2;
	if(theapp->jobinfo[pframe->CurrentJobNum].neckBarY<=50)theapp->jobinfo[pframe->CurrentJobNum].neckBarY=50;
	theapp->jobinfo[pframe->CurrentJobNum].tBandY=theapp->jobinfo[pframe->CurrentJobNum].neckBarY-56;
	theapp->jobinfo[pframe->CurrentJobNum].tBandX=lipX+0;
	theapp->jobinfo[pframe->CurrentJobNum].userX=0;
	theapp->jobinfo[pframe->CurrentJobNum].userY=theapp->jobinfo[pframe->CurrentJobNum].midBarY;
	if(theapp->jobinfo[pframe->CurrentJobNum].vertBarX>=1000)theapp->jobinfo[pframe->CurrentJobNum].vertBarX=1000;
	if(theapp->jobinfo[pframe->CurrentJobNum].vertBarX<=100)theapp->jobinfo[pframe->CurrentJobNum].vertBarX=100;

}

//NOT USED
SinglePoint CXAxisView::FindCapTopSport(BYTE* im_srcIn, SinglePoint SideDist, bool left)
{
	SinglePoint result;

	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	int pixg=0;
	int pixh=0;
	int pixi=0;
	int pixj=0;
	int pixk=0;
	int pixl=0;
	int pixm=0;
	int pixn=0;
	int pixo=0;
	int pixp=0;
	int pixq=0;
	int pixr=0;
	int Limit=40;
	int d1=0;
	int d2=0;
	int d3=0;
	int LineLength=1024;
	int xpos=SideDist.x;
	int ypos2=SideDist.y;
	int xpos4=0;
	int ypos4;
	int ypos5;
	int ypos=0;
	int xpos2;
	int xpos3;

	if(xpos<=XEnd && xpos >=XStart )
	{
		if(left==true)
		{
			for(xpos2=xpos-25;xpos2<512;xpos2++)
			{	
				pixa=*(im_srcIn+xpos2-1+(LineLength * (ypos2-1)));  pixd=*(im_srcIn+xpos2+1+(LineLength * (ypos2-1)));	
				pixb=*(im_srcIn+xpos2-1+(LineLength * (ypos2)));    pixe=*(im_srcIn+xpos2+1+(LineLength * (ypos2)));
				pixc=*(im_srcIn+xpos2-1+(LineLength * (ypos2+1)));  pixf=*(im_srcIn+xpos2+1+(LineLength * (ypos2+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				if( (d1 > Limit)  )//got edge of cap
				{
					
					for(xpos3=xpos2-25;xpos3<512;xpos3++)
					{	
						pixa=*(im_srcIn+xpos3-1+(LineLength * (ypos2-1-20)));  pixd=*(im_srcIn+xpos3+1+(LineLength * (ypos2-1-20)));	
						pixb=*(im_srcIn+xpos3-1+(LineLength * (ypos2-20)));    pixe=*(im_srcIn+xpos3+1+(LineLength * (ypos2-20)));
						pixc=*(im_srcIn+xpos3-1+(LineLength * (ypos2+1-20)));  pixf=*(im_srcIn+xpos3+1+(LineLength * (ypos2+1-20)));

						//pixg=*(im_srcIn+xpos3-1+(LineLength * (ypos2-1-22)));  pixj=*(im_srcIn+xpos3+1+(LineLength * (ypos2-1-22)));	
						//pixh=*(im_srcIn+xpos3-1+(LineLength * (ypos2-22)));    pixk=*(im_srcIn+xpos3+1+(LineLength * (ypos2-22)));
						//pixi=*(im_srcIn+xpos3-1+(LineLength * (ypos2+1-22)));  pixl=*(im_srcIn+xpos3+1+(LineLength * (ypos2+1-22)));

						//pixm=*(im_srcIn+xpos3-1+(LineLength * (ypos2-1-24)));  pixp=*(im_srcIn+xpos3+1+(LineLength * (ypos2-1-24)));	
						//pixn=*(im_srcIn+xpos3-1+(LineLength * (ypos2-24)));    pixq=*(im_srcIn+xpos3+1+(LineLength * (ypos2-24)));
						//pixo=*(im_srcIn+xpos3-1+(LineLength * (ypos2+1-24)));  pixr=*(im_srcIn+xpos3+1+(LineLength * (ypos2+1-24)));

						d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						//d2=abs((pixg+(2*pixh)+pixi)-(pixj+(2*pixk)+pixl));
						//d3=abs((pixm+(2*pixn)+pixo)-(pixp+(2*pixq)+pixr));		
						
						if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit ) )
						{
							if(xpos3-xpos2>=25)//found top of cap 
							{
								ypos5=ypos2-45;
								if(ypos5<=1)ypos5=1;
								for(int xpos5=xpos3-25;xpos5<512;xpos5++)
								{
									pixa=*(im_srcIn+xpos5-1+(LineLength * (ypos5-1)));  pixd=*(im_srcIn+xpos5+1+(LineLength * (ypos5-1)));	
									pixb=*(im_srcIn+xpos5-1+(LineLength * (ypos5)));    pixe=*(im_srcIn+xpos5+1+(LineLength * (ypos5)));
									pixc=*(im_srcIn+xpos5-1+(LineLength * (ypos5+1)));  pixf=*(im_srcIn+xpos5+1+(LineLength * (ypos5+1)));
									d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
									if( (d1 > Limit)  ){xpos4=xpos5-6; xpos5=512;}
								}
								
						
								
								xpos3=512; //end second loop
								xpos2=512;  //end first loop
								for(int ypos4=ypos2-40;ypos4<400;ypos4++)
								{	
									pixa=*(im_srcIn+xpos4-1+(LineLength * (ypos4-1)));  	
									pixb=*(im_srcIn+xpos4+(LineLength * (ypos4-1)));    
									pixc=*(im_srcIn+xpos4+1+(LineLength * (ypos4-1)));  

									pixd=*(im_srcIn+xpos4-1+(LineLength * (ypos4+1)));
									pixe=*(im_srcIn+xpos4+(LineLength * (ypos4+1)));
									pixf=*(im_srcIn+xpos4+1+(LineLength * (ypos4+1)));

									d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
													
									if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
									{	
										//int ypos4=ypos2-25;
										result.y=ypos4;
										result.x=xpos4;
										ypos4=400;
													
									}
								}
							}
							else
							{
								xpos3=512; //end second loop
								xpos2=xpos-20;
								if(ypos2>=26) {ypos2-=1;}else{xpos2=512;}//exit
							}
						}
					}					
				}
			}	
		}
		else
		{
			for(xpos2=xpos+25;xpos2>512;xpos2--)
			{	
				pixa=*(im_srcIn+xpos2-1+(LineLength * (ypos2-1)));  pixd=*(im_srcIn+xpos2+1+(LineLength * (ypos2-1)));	
				pixb=*(im_srcIn+xpos2-1+(LineLength * (ypos2)));    pixe=*(im_srcIn+xpos2+1+(LineLength * (ypos2)));
				pixc=*(im_srcIn+xpos2-1+(LineLength * (ypos2+1)));  pixf=*(im_srcIn+xpos2+1+(LineLength * (ypos2+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
				if( (d1 > Limit)  )//got edge of cap
				{
					
					for(xpos3=xpos2+25;xpos3>512;xpos3--)
					{	
						pixa=*(im_srcIn+xpos3-1+(LineLength * (ypos2-1-20)));  pixd=*(im_srcIn+xpos3+1+(LineLength * (ypos2-1-20)));	
						pixb=*(im_srcIn+xpos3-1+(LineLength * (ypos2-20)));    pixe=*(im_srcIn+xpos3+1+(LineLength * (ypos2-20)));
						pixc=*(im_srcIn+xpos3-1+(LineLength * (ypos2+1-20)));  pixf=*(im_srcIn+xpos3+1+(LineLength * (ypos2+1-20)));

						//pixg=*(im_srcIn+xpos3-1+(LineLength * (ypos2-1-22)));  pixj=*(im_srcIn+xpos3+1+(LineLength * (ypos2-1-22)));	
						//pixh=*(im_srcIn+xpos3-1+(LineLength * (ypos2-22)));    pixk=*(im_srcIn+xpos3+1+(LineLength * (ypos2-22)));
						//pixi=*(im_srcIn+xpos3-1+(LineLength * (ypos2+1-22)));  pixl=*(im_srcIn+xpos3+1+(LineLength * (ypos2+1-22)));

						//pixm=*(im_srcIn+xpos3-1+(LineLength * (ypos2-1-24)));  pixp=*(im_srcIn+xpos3+1+(LineLength * (ypos2-1-24)));	
						//pixn=*(im_srcIn+xpos3-1+(LineLength * (ypos2-24)));    pixq=*(im_srcIn+xpos3+1+(LineLength * (ypos2-24)));
						//pixo=*(im_srcIn+xpos3-1+(LineLength * (ypos2+1-24)));  pixr=*(im_srcIn+xpos3+1+(LineLength * (ypos2+1-24)));

						d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						//d2=abs((pixg+(2*pixh)+pixi)-(pixj+(2*pixk)+pixl));
						//d3=abs((pixm+(2*pixn)+pixo)-(pixp+(2*pixq)+pixr));		
						
						if( (d1 > Limit) )// && (d2 > Limit) && (d3 > Limit ) )
						{
							if(xpos2-xpos3>=25)//found top of cap 
							{
								ypos5=ypos2-45;
								if(ypos5<=1)ypos5=1;
								for(int xpos5=xpos3+25;xpos5>512;xpos5--)
								{
									pixa=*(im_srcIn+xpos5-1+(LineLength * (ypos5-1)));  pixd=*(im_srcIn+xpos5+1+(LineLength * (ypos5-1)));	
									pixb=*(im_srcIn+xpos5-1+(LineLength * (ypos5)));    pixe=*(im_srcIn+xpos5+1+(LineLength * (ypos5)));
									pixc=*(im_srcIn+xpos5-1+(LineLength * (ypos5+1)));  pixf=*(im_srcIn+xpos5+1+(LineLength * (ypos5+1)));
									d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
									if( (d1 > Limit)  ){xpos4=xpos5+6; xpos5=512;}
								}
							
								xpos3=512; //end second loop
								xpos2=512;  //end first loop
								for(ypos4=ypos2-40;ypos4<400;ypos4++)
								{	
									pixa=*(im_srcIn+xpos4-1+(LineLength * (ypos4-1)));  	
									pixb=*(im_srcIn+xpos4+(LineLength * (ypos4-1)));    
									pixc=*(im_srcIn+xpos4+1+(LineLength * (ypos4-1)));  

									pixd=*(im_srcIn+xpos4-1+(LineLength * (ypos4+1)));
									pixe=*(im_srcIn+xpos4+(LineLength * (ypos4+1)));
									pixf=*(im_srcIn+xpos4+1+(LineLength * (ypos4+1)));

									d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
												
									if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
									{			
										result.y=ypos4;
										result.x=xpos4;
										ypos4=400;
												
									}
								}
							}
							else
							{
								xpos3=512; //end second loop
								xpos2=xpos+20;
								if(ypos2>=26) {ypos2-=1;}else{xpos2=512;}//exit
							}
						}
					}					
				}
			}	
		}
	}

	return result;
}

//NOT USED
float CXAxisView::CheckSportBand(BYTE* im_srcIn, SinglePoint CapLTop, SinglePoint CapRTop)
{
	float result;
	result=0;
	int x;
	int y;
	int count=0;
	int LineLength=1024;
	int pixa;

	if(CapLTop.x>0 && CapLTop.x<1000 && CapRTop.x>0 && CapRTop.x<1000 && CapLTop.y-70>0 && CapLTop.y<600 && CapRTop.y-70>0 && CapRTop.y<600)
	{
		for(x=CapLTop.x+24; x < CapRTop.x-24; x++)
		{
			for(y=CapLTop.y; y>CapLTop.y-70; y--)
			{	
				pixa=*(im_srcIn+x+(LineLength * (y)));  
														
				if (pixa >= theapp->jobinfo[pframe->CurrentJobNum].tBandThresh)  
				{			
					count+=1;
									
					result=count;					
				}
			}
		}
	}
				

	return result;
}

//With extra light option enabled, check how many points in cam1 are beyond the low fill threshold.
//im_srcIn: source image, grayscale.
//CapLTop: coordinates of cap left top point.
//CapRTop: coordinates of cap right top point.
TwoPoint CXAxisView::CheckSportBand2(BYTE* im_srcIn, SinglePoint CapLTop, SinglePoint CapRTop)
{
	TwoPoint result;
	result.x=result.y=result.x1=result.y1=0;
	int xpos=0;
	int ypos=theapp->jobinfo[pframe->CurrentJobNum].sportPos;//CapLTop.y-90;
	if(ypos<=20)ypos=20;
	int count=0;
	int LineLength=1024;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	int pixg=0;
	int pixh=0;

	int Limit=theapp->jobinfo[pframe->CurrentJobNum].sportSen;
	int d1=0;

	if(CapLTop.x>1 && CapLTop.x<1000 && CapRTop.x>1 && CapRTop.x<1000 && ypos-20>0 && ypos+20<400)
	{
		for(xpos=CapLTop.x;xpos<650;xpos++)
		{	
			pixa=*(im_srcIn+xpos-2+(LineLength * (ypos-1)));  pixd=*(im_srcIn+xpos+2+(LineLength * (ypos-1)));	
			pixb=*(im_srcIn+xpos-2+(LineLength * (ypos)));    pixe=*(im_srcIn+xpos+2+(LineLength * (ypos)));
			pixc=*(im_srcIn+xpos-2+(LineLength * (ypos+1)));  pixf=*(im_srcIn+xpos+2+(LineLength * (ypos+1)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
			if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
			{			
				result.y=ypos;
				result.x=xpos;
				xpos=650;
						
			}
		}

		for(xpos=CapRTop.x;xpos>350;xpos--)
		{	
			pixa=*(im_srcIn+xpos-2+(LineLength * (ypos-1)));  pixd=*(im_srcIn+xpos+2+(LineLength * (ypos-1)));	
			pixb=*(im_srcIn+xpos-2+(LineLength * (ypos)));    pixe=*(im_srcIn+xpos+2+(LineLength * (ypos)));
			pixc=*(im_srcIn+xpos-2+(LineLength * (ypos+1)));  pixf=*(im_srcIn+xpos+2+(LineLength * (ypos+1)));

			d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
						
			if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
			{			
				result.y1=ypos;
				result.x1=xpos;
				xpos=350;
						
			}
		}
	}
				

	return result;
}


union hexNumber {unsigned char digits[4];unsigned int value;};

//NOT USED
void CXAxisView::writeOutGrayArrayFile(const char * filename,
					 const int width,
					 const int height,
					 const unsigned char * bmpGrayArray)
{
	if (filename == NULL)
	{
		AfxMessageBox("File name missing.");
		exit(0);
	}

	FILE *fptr = NULL;
	fptr = fopen(filename,"wb");

	if (fptr == NULL)
	{
	//	AfxMessageBox("Unable to open output file");
			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();
		exit(0);
	}

	// BMP files require 4-byte allignment
	int offset = width%4;
	// Data area may include zero-byte padding at end of each row
	int dataSize = 3*width*height + offset*height;

	hexNumber totalFileSize, hexWidth, hexHeight, hexImageSize;
	totalFileSize.value = int (54 + dataSize);
	hexWidth.value = width;
	hexHeight.value = height;
	hexImageSize.value = dataSize;


	unsigned char header[54];
	memset(header,0,54);
	header[0]  = 'B';
	header[1]  = 'M';
	header[2]  = totalFileSize.digits[0];
	header[3]  = totalFileSize.digits[1];
	header[4]  = totalFileSize.digits[2];
	header[5]  = totalFileSize.digits[3];
	header[10] = 54;
	header[14] = 40;
	header[18] = hexWidth.digits[0];
	header[19] = hexWidth.digits[1];
	header[20] = hexWidth.digits[2];
	header[21] = hexWidth.digits[3];
	header[22] = hexHeight.digits[0];
	header[23] = hexHeight.digits[1];
	header[24] = hexHeight.digits[2];
	header[25] = hexHeight.digits[3];
	header[26] = 1;
	header[28] = 24;
	header[34] = hexImageSize.digits[0];
	header[35] = hexImageSize.digits[1];
	header[36] = hexImageSize.digits[2];
	header[37] = hexImageSize.digits[3];
	header[38] = 120;
	header[39] = 30;
	header[42] = 120;
	header[43] = 30;

	fwrite(header,1,54,fptr);

	const unsigned char * cPtr = bmpGrayArray;
	unsigned char zeroByte = 0;

	for (int i=0; i<height; i++)
	{
		for(int j=0; j<width; j++)
		{
			fwrite(cPtr,1,1,fptr);
			fwrite(cPtr,1,1,fptr);
			fwrite(cPtr,1,1,fptr);
			cPtr++;
		}
		// Write offset at end of each horizontal row
		if (offset>0)
			fwrite(&zeroByte,1,1,fptr);
		if (offset>1)
			fwrite(&zeroByte,1,1,fptr);
		if (offset>2)
			fwrite(&zeroByte,1,1,fptr);
	}
	fclose(fptr);
}

//Compute Hue from RGB. Used in CheckBand and CheckBand2.
int CXAxisView::GetHue(int r, int g, int b)
{
	int result=0;
	float maxColor, minColor, highest, lowest, diffColor, resultf;

	float R=r*0.003921;
	float G=g*0.003921;
	float B=b*0.003921;
	 
	if(R>G) {highest=R;}else{highest=G;}
	if(highest>B) {maxColor=highest;}else{maxColor=B;}

	if(R<G) {lowest=R;}else{lowest=G;}
	if(lowest<B) {minColor=lowest;}else{minColor=B;}

	diffColor=maxColor-minColor;
	if (diffColor==0) diffColor=1;


	if((R==maxColor) && (diffColor!=0) ) resultf=0+43*(G-B)/diffColor;
	if((G==maxColor) && (diffColor!=0) ) resultf=85+(43*(B-R)/diffColor);
	if((B==maxColor) && (diffColor!=0) ) resultf=171+(43*(R-G)/diffColor);

	result=resultf;

	return result;
}

//

int CXAxisView::FoilWindow(BYTE *im_srcIn)    //verified within Bounds
{
	int result=0;

	int x;
	int y;
	int count=0;
	int LineLength=1024;
	int pixa=0;


		for(x=150; x < 160; x++)
		{
			for(y=50; y<60; y++)
			{	
				pixa+=*(im_srcIn+x+(LineLength * (y)));  													
				count+=1;
										
			}
		}

		if(count>0) result=pixa/count;


	return result;

}

//Used to find the top most coordinate of the cap on one of the two rear cameras.
//im_srcIn: source image, grayscale.
//SideDist: x distance between edge of cap and edge of image.
//left: true = left camera (cam2), false = right camera (cam3).
SinglePointF CXAxisView::FindCapTopRear(BYTE *im_srcIn, int SideDist, bool left)      //verified within Bounds
{

	//This function uses Sub Pixelation ..
	//Good Stuff.. 

	SinglePointF result;

	result.x=result.y=0;
	double pixa=0;
	double pixb=0;
	double pixc=0;
	double pixd=0;
	double pixe=0;
	double pixf=0;
	double pixup=0;
	double pixdn=0;

	int num1, num2, num3, num4, num5, closest, lightest, darkest;
	
	int Limit=40;
	int d1=0;
	
	int LineLength=640;
	int xpos=SideDist;
	int ypos=1;
	double m, s, ypos2, midVal;
	
	
	if(xpos<=630 && xpos >=20 )
	{
		if(left==true)
		{		
			for(ypos=5;ypos<theapp->cam2Height-20;ypos++)
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
				pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

				pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
				pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
				pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					//result.y=ypos;
					result.x=xpos;
					//se{result.x=0;}
				
					pixup=*(im_srcIn+xpos+(LineLength * (ypos-3)));  
					pixa=*(im_srcIn+xpos+(LineLength * (ypos-2)));  	
					pixb=*(im_srcIn+xpos+(LineLength * (ypos-1)));    
					pixc=*(im_srcIn+xpos+(LineLength * (ypos)));  

					pixd=*(im_srcIn+xpos+(LineLength * (ypos+1)));
					pixe=*(im_srcIn+xpos+(LineLength * (ypos+2)));
					pixdn=*(im_srcIn+xpos+(LineLength * (ypos+3)));

					
					if(pixa>=pixb) {lightest=pixa;} else {lightest=pixb;}
					if(pixc>lightest)lightest=pixc;
					if(pixd>lightest)lightest=pixd;
					if(pixe>lightest)lightest=pixe;
					
					if(pixa<=pixb) {darkest=pixa;} else {darkest=pixb;}
					if(pixc<darkest)darkest=pixc;
					if(pixd<darkest)darkest=pixd;
					if(pixe<darkest)darkest=pixe;

					midVal=(lightest-darkest)/2;
					midVal=darkest+midVal;
					
					num1=abs(midVal-pixa);
					num2=abs(midVal-pixb);
					num3=abs(midVal-pixc);
					num4=abs(midVal-pixd);
					num5=abs(midVal-pixe);

					if(num1<=num2){ closest=num1;}else{closest=num2;}
					if(num3<=closest) closest=num3;
					if(num4<=closest) closest=num4;
					if(num5<=closest) closest=num5;

					if(ypos<=2) ypos=2;
					ypos2=ypos;

					if(closest==num1)
					{
						m=(pixup-pixb)/((ypos2-3)-(ypos2-1));
						b=pixup-((ypos2-3)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num2)
					{
						m=(pixa-pixc)/((ypos2-2)-(ypos2));
						b=pixa-((ypos2-2)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num3)
					{
						m=(pixb-pixd)/((ypos2-1)-(ypos2+1));
						b=pixb-((ypos2-1)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num4)
					{
						m=(pixc-pixe)/((ypos2)-(ypos2+2));
						b=pixc-(ypos2*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num5)
					{
						m=(pixd-pixdn)/((ypos2+1)-(ypos2+3));
						b=pixd-((ypos2+1)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}

					if(result.y>=ypos+2 || result.y<ypos-2) result.y=ypos;
					ypos=theapp->cam2Height-20;
					
							
				}
			}
		}
		else
		{
			for(ypos=5;ypos<theapp->cam3Height-20;ypos++)
			{	
				pixa=*(im_srcIn+xpos-1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+xpos+(LineLength * (ypos)));    
				pixc=*(im_srcIn+xpos+1+(LineLength * (ypos)));  

				pixd=*(im_srcIn+xpos-1+(LineLength * (ypos+1)));
				pixe=*(im_srcIn+xpos+(LineLength * (ypos+1)));
				pixf=*(im_srcIn+xpos+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					
					result.x=xpos;

					pixup=*(im_srcIn+xpos+(LineLength * (ypos-3)));  
					pixa=*(im_srcIn+xpos+(LineLength * (ypos-2)));  	
					pixb=*(im_srcIn+xpos+(LineLength * (ypos-1)));    
					pixc=*(im_srcIn+xpos+(LineLength * (ypos)));  

					pixd=*(im_srcIn+xpos+(LineLength * (ypos+1)));
					pixe=*(im_srcIn+xpos+(LineLength * (ypos+2)));
					pixdn=*(im_srcIn+xpos+(LineLength * (ypos+3)));

					if(pixa>=pixb) {lightest=pixa;} else {lightest=pixb;}
					if(pixc>lightest)lightest=pixc;
					if(pixd>lightest)lightest=pixd;
					if(pixe>lightest)lightest=pixe;
					
					if(pixa<=pixb) {darkest=pixa;} else {darkest=pixb;}
					if(pixc<darkest)darkest=pixc;
					if(pixd<darkest)darkest=pixd;
					if(pixe<darkest)darkest=pixe;

					midVal=(lightest-darkest)/2;
					midVal=darkest+midVal;
					
					num1=abs(midVal-pixa);
					num2=abs(midVal-pixb);
					num3=abs(midVal-pixc);
					num4=abs(midVal-pixd);
					num5=abs(midVal-pixe);

					if(num1<=num2){ closest=num1;}else{closest=num2;}
					if(num3<=closest) closest=num3;
					if(num4<=closest) closest=num4;
					if(num5<=closest) closest=num5;

					if(ypos<=2) ypos=2;
					ypos2=ypos;

					if(closest==num1)
					{
						m=(pixup-pixb)/((ypos2-3)-(ypos2-1));
						b=pixup-((ypos2-3)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num2)
					{
						m=(pixa-pixc)/((ypos2-2)-(ypos2));
						b=pixa-((ypos2-2)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num3)
					{
						m=(pixb-pixd)/((ypos2-1)-(ypos2+1));
						b=pixb-((ypos2-1)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num4)
					{
						m=(pixc-pixe)/((ypos2)-(ypos2+2));
						b=pixc-(ypos2*m);
						if(m!=0)result.y=(midVal-b)/m;
					}
					else if(closest==num5)
					{
						m=(pixd-pixdn)/((ypos2+1)-(ypos2+3));
						b=pixd-((ypos2+1)*m);
						if(m!=0)result.y=(midVal-b)/m;
					}

					if(result.y>=ypos+2 || result.y<ypos-2) result.y=ypos;
					ypos=theapp->cam3Height-20;
							
				}
			}
		}
	}

	return result;

}

//Determines if the cap is cocked based on camera 2 image.
//im_srcIn: source image, grayscale.
//x: x coordinate of top most point on cap in cam2.
//y: y coordinate of top most point on cap in cam2.
bool CXAxisView::dist2top_cam2(BYTE *im_srcIn, int x, int y)		 //verified within Bounds
{

	//I/p is img, X co ord of the TOp, Y Co ord of the top

	int pixa,pixb,pixc,pixd,pixe,pixf;int d1;
	pixa=pixb=pixc=pixd=pixe=pixf=0;
	int Limit=40;	vector<int> myvector;
	int LineLength=theapp->cam2Width;
		
	SinglePoint arr_cam2[70];for(int xyz=0;xyz<70;xyz++){arr_cam2[xyz].x=arr_cam2[xyz].y=0;}
	int count=0;cam2cocked = false;

 //test to check whether The capwidth 
	int capwidth;
	myvector.clear();
	if(x<=0) x=0; if(x>theapp->cam2Width ) x=theapp->cam2Width-5;
	if(y<=0) y=0; if(y+25>theapp->cam2Height) y=theapp->cam2Height-30;	

	int test_var=0;


	capwidth=currentCapWidth	* (0.63)   ;      //236/373 =0.63 (ratio to convert cam 1 measuremnt in cam 2

	capwidth+=(capWidthDiff * (-1)* 0.63);

//	for(int a=x+2;(a>x-capwidth-35) && (a>250);a-=5)     //150 in office
	for(int a=x+2;(a>x-(capwidth/2)) && (a>300);a-=5)     //150 in office
	{
		for(int ypos=2;ypos<y+25;ypos++)
		{	
	
				///$$$$$$

				pixa=*(im_srcIn+a-1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+a+(LineLength * (ypos)));    
				pixc=*(im_srcIn+a+1+(LineLength * (ypos)));  

				pixd=*(im_srcIn+a-1+(LineLength * (ypos+1)));
				pixe=*(im_srcIn+a+(LineLength * (ypos+1)));
				pixf=*(im_srcIn+a+1+(LineLength * (ypos+1)));

				
				///$$$$$$

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
				if( (d1 > Limit)  )//&& (d2 > Limit) && (d3 > Limit )
				{			
					if(count <70)
					{
						myvector.push_back(ypos);

						arr_cam2[count].x=a;arr_cam2[count].y=ypos;
					}
						ypos=y+25;
				}

			} 
		count++;
	}
	
	if((myvector.size() >0) && (myvector.size() <=70))
	{

		sort(myvector.begin(),myvector.end());

		theapp->cam2diff=abs(abs(myvector.at(0)-myvector.at(myvector.size()-1))-theapp->cam2_ht_offset);

		if(theapp->cam2diff<0 ){theapp->cam2diff =0;} //If it is less than 0
		if(theapp->cam2diff>100){theapp->cam2diff =100;}//If it is over 25
		//if(abs(myvector.at(0)-myvector.at(myvector.size()-1)) > 1){cam2cocked=true;} //taking decision for cap being cocked
		
		for(int abc=0;abc<70;abc++)
		{ 
			if(arr_cam2[abc].y == myvector.at(0)){cam2min.x=arr_cam2[abc].x;cam2min.y=arr_cam2[abc].y;} //minimum x&y pts for cam2
			if(arr_cam2[abc].y == myvector.at(myvector.size()-1)){cam2max.x=arr_cam2[abc].x;cam2max.y=arr_cam2[abc].y;} //maximum x&y pts for cam2
		}

	}

	
	myvector.clear();


	return cam2cocked;
}

//Determines if the cap is cocked based on camera 3 image.
//im_srcIn: source image, grayscale.
//x: x coordinate of top most point on cap in cam3.
//y: y coordinate of top most point on cap in cam3
bool CXAxisView::dist2top_cam3(BYTE *im_srcIn, int x, int y)
{
	int pixa,pixb,pixc,pixd,pixe,pixf;
	pixa=pixb=pixc=pixd=pixe=pixf=0;
	int d1;
	int Limit=40;	
	vector<int> myvector3;
	int LineLength=640;
	
	SinglePoint arr_cam3[70];for(int xyz=0;xyz<70;xyz++){arr_cam3[xyz].x=arr_cam3[xyz].y=0;}
	int count=0;
	cam3cocked = false;
	myvector3.clear();
	//////
	int capwidth;
	if(x<=0) x=0;
	if(y<=0) y=0;

	capwidth=currentCapWidth	* (0.63)    ;      //236/373 =0.63 (ratio to convert cam 1 measuremnt in cam 2)

	capwidth+=(capWidthDiff * (-1)* 0.63);
	/////////lines added to automate cap width

	//for(int a=x+2;(a<x+capwidth-35) && (a<400) ;a+=5)		//490 in office
	for(int a=x+2;(a< x+(capwidth/2)) && (a<450) ;a+=5)		//490 in office
	{
		for(int ypos=2;ypos<y+25;ypos++)
		{	
				pixa=*(im_srcIn+a-1+(LineLength * (ypos)));  	
				pixb=*(im_srcIn+a+(LineLength * (ypos)));    
				pixc=*(im_srcIn+a+1+(LineLength * (ypos)));  

				pixd=*(im_srcIn+a-1+(LineLength * (ypos+1)));
				pixe=*(im_srcIn+a+(LineLength * (ypos+1)));
				pixf=*(im_srcIn+a+1+(LineLength * (ypos+1)));

				d1=abs((pixa+(2*pixb)+pixc)-(pixd+(2*pixe)+pixf));
							
				if( (d1 > Limit) )//&& (d2 > Limit) && (d3 > Limit )
				{			
					if(count <70)
					{
						myvector3.push_back(ypos);

						arr_cam3[count].x=a;arr_cam3[count].y=ypos;
					}
					ypos=y+25;
				}

		}
		count++;
	}
	
	if((myvector3.size() >0) && (myvector3.size() <=70))
	{

		sort(myvector3.begin(),myvector3.end());

		theapp->cam3diff=abs(abs(myvector3.at(0)-myvector3.at(myvector3.size()-1))-theapp->cam3_ht_offset);
		
		if(theapp->cam3diff<0 ){theapp->cam3diff =0;} ////If it goes less than 0
		if(theapp->cam3diff>100){theapp->cam3diff =100;} ///If it goes over 30

	//	if(abs(myvector3.at(0)-myvector3.at(myvector3.size()-1)) > 1){cam3cocked=true;} //taking decision for cap being cocked
	//	for(int b1=0;b1<myvector3.size();b1++){TRACE("%d ",myvector3.at(b1));}TRACE("\n");
		for(int abc=0;abc<70;abc++)
		{ 
			if(arr_cam3[abc].y == myvector3.at(0)){cam3min.x=arr_cam3[abc].x;cam3min.y=arr_cam3[abc].y;} //minimum x&y pts for cam3
			if(arr_cam3[abc].y == myvector3.at(myvector3.size()-1)){cam3max.x=arr_cam3[abc].x;cam3max.y=arr_cam3[abc].y;} //maximum x&y pts for cam3
		}

	}

	
	
	myvector3.clear();

	return cam3cocked;
}



//NOT USED, Edge detection for camera 1
BYTE* CXAxisView::CalcSobel(BYTE *grayscale_cam1, int x1, int x2,int y1,int y2)
{


//	int pixB,pixG,pixR,pixY;


//*************1st calc the Grayscale of the RGB image

/*

	int j=0;

	
	for(int a=50;a<theapp->cam1Height-150;a++)
	{
		for(int b=50*4;b<=(theapp->cam1Width*4)-(50*4)-4;b+=4)
		{
			pixB=*((Image+b + LineLength * (a)));
			pixG=*((Image+b +1 + LineLength * (a)));
			pixR=*((Image+b +2 + LineLength * (a)));

			pixY=int(0.11*pixB+ 0.59*pixG + 0.3*pixR);

			*(grayscale_cam1+j+(theapp->cam1Width*a))=pixY;

			j++;
		}

		j=0;
	}

	*/

///***********Then use the Sobel mask over the entire image

	int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;

	int pix; 
	int Length=theapp->cam1Width;


	for(int a1=y1;a1<y2;a1++) ////for(int a1=1;a1<theapp->cam1Height-1;a1++)
	{
		for(int b1=x1;b1<x2;b1++) ///for(int b1=1;b1<theapp->cam1Width-1;b1++)
		{

				pix1 = *(grayscale_cam1 + b1 - 1  + Length*(a1 - 1));
				pix2 = *(grayscale_cam1 + b1 + 0  + Length*(a1 - 1));
				pix3 = *(grayscale_cam1 + b1 + 1  + Length*(a1 - 1));

				pix4 = *(grayscale_cam1 + b1 - 1  + Length*(a1 + 0));
				pix5 = *(grayscale_cam1 + b1 + 0  + Length*(a1 + 0));
				pix6 = *(grayscale_cam1 + b1 + 1  + Length*(a1 + 0));

				pix7 = *(grayscale_cam1 + b1 - 1  + Length*(a1 + 1));
				pix8 = *(grayscale_cam1 + b1 + 0  + Length*(a1 + 1));
				pix9 = *(grayscale_cam1 + b1 + 1  + Length*(a1 + 1));


				
			pix	 =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
							abs( (pix3+2*pix6+pix9) - (pix1+2*pix4+pix7) );

			if( (pix >theapp->jobinfo[pframe->CurrentJobNum].cam1_ll && pix <theapp->jobinfo[pframe->CurrentJobNum].cam1_ul))
			{
				*(sobel_cam1 + b1	+ Length * a1) = pix; //size of the image is 2 rows and 2 columns less
			}
			else{*(sobel_cam1 + b1	+ Length * a1)=0;}
			
			
		}
		
 
	}


	//OnSaveBW(sobel_cam1,1);
	return sobel_cam1;


}

void CXAxisView::GetRedChannel(BYTE *colorImage, int width, int height, BYTE* dest)
{
	int j = 0;
	for (int i = 0; i < height*width * 3; i += 3)
	{
		dest[j] = colorImage[i];
		j++;
	}
}

void CXAxisView::GetHueChannel(BYTE *colorImage, int width, int height, BYTE* dest)
{
	float r, g, b, h, min, max, delta;
	int j = 0;
	for (int i = 0; i < height*width * 3; i += 3)
	{
		r = (colorImage[i] / 255.0f);
		g = (colorImage[i + 1] / 255.0f);
		b = (colorImage[i + 2] / 255.0f);

		min = min(min(r, g), b);
		max = max(max(r, g), b);
		delta = max - min;

		if (std::abs(max - r) < std::abs(max - g)
			&& std::abs(max - r) < std::abs(max - b))
		{
			h = ((g - b) / 6) / delta;
		}
		else if (std::abs(max - g) < std::abs(max - r)
			&& std::abs(max - g) < std::abs(max - b))
		{
			h = (1.0f / 3) + ((b - r) / 6) / delta;
		}
		else
		{
			h = (2.0f / 3) + ((r - g) / 6) / delta;
		}

		if (h < 0)
			h += 1;
		if (h > 1)
			h -= 1;

		h = (int)(h * 360);
		dest[j] = h;
		j++;
	}
}

//Edge detection for camera 2
BYTE* CXAxisView::CalcSobel2(BYTE *Image, int camNo)
{


	int LineLength=theapp->cam2Width*4;

	int pixB,pixG,pixR,pixY;

//*************1st calc the Grayscale of the RGB image



	int j=0;

	
	for(int a=0;a<theapp->cam2Height;a++)
	{
		for(int b=0;b<=theapp->cam2Width*4-4;b+=4)
		{
			pixB=*((Image+b + LineLength * (a)));
			pixG=*((Image+b +1 + LineLength * (a)));
			pixR=*((Image+b +2 + LineLength * (a)));

			pixY=int(0.11*pixB+ 0.59*pixG + 0.3*pixR);

			*(grayscale_cam2+j+(theapp->cam2Width*a))=pixY;

			j++;
		}

		j=0;
	}

	

///***********Then use the Sobel mask over the entire image

int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;

int pix; 
int Length=theapp->cam2Width;


for(int a1=1;a1<theapp->cam2Height-1;a1++)
{
	for(int b1=1;b1<theapp->cam2Width-1;b1++)
	{

			pix1 = *(grayscale_cam2 + b1 - 1  + Length*(a1 - 1));
			pix2 = *(grayscale_cam2 + b1 + 0  + Length*(a1 - 1));
			pix3 = *(grayscale_cam2 + b1 + 1  + Length*(a1 - 1));

			pix4 = *(grayscale_cam2 + b1 - 1  + Length*(a1 + 0));
			pix5 = *(grayscale_cam2 + b1 + 0  + Length*(a1 + 0));
			pix6 = *(grayscale_cam2 + b1 + 1  + Length*(a1 + 0));

			pix7 = *(grayscale_cam2 + b1 - 1  + Length*(a1 + 1));
			pix8 = *(grayscale_cam2 + b1 + 0  + Length*(a1 + 1));
			pix9 = *(grayscale_cam2 + b1 + 1  + Length*(a1 + 1));


			
		pix	 =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
						abs( (pix3+2*pix6+pix9) - (pix1+2*pix4+pix7) );

		if( (pix >theapp->jobinfo[pframe->CurrentJobNum].cam2_ll && pix <theapp->jobinfo[pframe->CurrentJobNum].cam2_ul))
		{
			*(sobel_cam2 + b1	+ Length * a1) = pix; //size of the image is 2 rows and 2 columns less
		}
		else{*(sobel_cam2 + b1	+ Length * a1)=0;}
		
		
	}
	
 
}


return sobel_cam2;
}

//Edge detection for camera 3
BYTE* CXAxisView::CalcSobel3(BYTE *Image, int camNo)
{

	int LineLength=theapp->cam3Width*4;

	int pixB,pixG,pixR,pixY;

//*************1st calc the Grayscale of the RGB image


	int l=theapp->cam3Height/2;
	int j=0;

	
	for(int a=0;a<theapp->cam3Height;a++)
	{
		for(int b=0;b<=theapp->cam3Width*4-4;b+=4)
		{
			pixB=*((Image+b + LineLength * (a)));
			pixG=*((Image+b +1 + LineLength * (a)));
			pixR=*((Image+b +2 + LineLength * (a)));

			pixY=int(0.11*pixB+ 0.59*pixG + 0.3*pixR);

			*(grayscale_cam3+j+(theapp->cam3Width*a))=pixY;

			j++;
		}

		j=0;
	}

	

///***********Then use the Sobel mask over the entire image

int pix1,pix2,pix3,pix4,pix5,pix6,pix7,pix8,pix9;

int pix; int j1=0;
int Length=theapp->cam3Width;


for(int a1=1;a1<theapp->cam3Height-1;a1++)
{
	for(int b1=1;b1<theapp->cam3Width-1;b1++)
	{

			pix1 = *(grayscale_cam3 + b1 - 1  + Length*(a1 - 1));
			pix2 = *(grayscale_cam3 + b1 + 0  + Length*(a1 - 1));
			pix3 = *(grayscale_cam3 + b1 + 1  + Length*(a1 - 1));

			pix4 = *(grayscale_cam3 + b1 - 1  + Length*(a1 + 0));
			pix5 = *(grayscale_cam3 + b1 + 0  + Length*(a1 + 0));
			pix6 = *(grayscale_cam3+ b1 + 1  + Length*(a1 + 0));

			pix7 = *(grayscale_cam3+ b1 - 1  + Length*(a1 + 1));
			pix8 = *(grayscale_cam3+ b1 + 0  + Length*(a1 + 1));
			pix9 = *(grayscale_cam3+ b1 + 1  + Length*(a1 + 1));


			
		pix	 =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
						abs( (pix3+2*pix6+pix9) - (pix1+2*pix4+pix7) );

		if( (pix >theapp->jobinfo[pframe->CurrentJobNum].cam3_ll && pix <theapp->jobinfo[pframe->CurrentJobNum].cam3_ul))
		{
			*(sobel_cam3+ b1	+ Length * a1) = pix; //size of the image is 2 rows and 2 columns less
		}
		else{*(sobel_cam3+ b1	+ Length * a1)=0;}
		
		
	}
	
 
}


return sobel_cam3;


}

//NOT USED
void CXAxisView::FindEndLight(int CamNo)
{
int pix1,pix2,pix3,pix4,pix5,pix6;
int LineLength=theapp->cam2Width;

if(CamNo == 2)
	{
	
	//m_imageProcessed2.pData=pframe->m_pcamera->m_imageProcessed2.pData;
		im_cam2=CtoBW2(m_imageProcessed[1]);
		
		for(int i=theapp->cam2Width;i>150;i-=6)
		{
			pix1=*((im_cam2+i +LineLength * (6)));
			pix2=*((im_cam2+i+LineLength * (8)));
			pix3=*((im_cam2+i+LineLength * (10)));

			pix4=*((im_cam2+i-6 +LineLength * (6)));
			pix5=*((im_cam2+i-6 +LineLength * (8)));
			pix6=*((im_cam2+i-6 +LineLength * (10)));

			if((pix1 + (2* pix2) + pix3) - (pix4 + (2*pix5) + pix6)> 50)
			{
			
			
			}
		}	
	
	
	}
else{

		im_cam3=CtoBW3(m_imageProcessed[2]);

		for(int i=150;i<theapp->cam3Width;i+=6)
		{
			pix1=*((im_cam3+i+6 +LineLength * (6)));
			pix2=*((im_cam3+i+6+LineLength * (8)));
			pix3=*((im_cam3+i+6+LineLength * (10)));

			pix4=*((im_cam3+i-6 +LineLength * (6)));
			pix5=*((im_cam3+i-6 +LineLength * (8)));
			pix6=*((im_cam3+i-6 +LineLength * (10)));

			if((pix1 + (2* pix2) + pix3) - (pix4 + (2*pix5) + pix6)> 50)
			{
			
			
			}
		}	

	}

}

//NOT USED
void CXAxisView::Check_Cap_presence(BYTE *im_srcIn, SinglePoint CapLeftTop)
{ 
	
	SinglePoint result;

	result.x=result.y=0;
	int pixa=0;
	int pixb=0;
	int pixc=0;
	int pixd=0;
	int pixe=0;
	int pixf=0;
	
	int Limit=40;
	int d1=0;
	
	int LineLength=theapp->cam1Width;
	int xpos;
	int ypos=1;

	int ystart=CapLeftTop.y -10;

	no_cap_detected =false;
	no_cap_x=no_cap_y=0;
	


		
				for( int  ypos1=CapLeftTop.y+2;ypos1<CapLeftTop.y+30;ypos1+=5)
				{

					for(int xpos1 =CapLeftTop.x-25;xpos1 < CapLeftTop.x+25;xpos1 +=5)
					{
							pixa=*(im_srcIn+xpos1+(LineLength * (ypos1))); 
							pixb=*(im_srcIn+xpos1+5+(LineLength * (ypos1))); 
						
							
							if((pixa - pixb) > theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap )
							{

								for ( int xpos2 = CapLeftTop.x-25;xpos2 < CapLeftTop.x+25;xpos2 +=5)
								{
										pixa=*(im_srcIn+xpos2+(LineLength * (ypos1+5))); 
										pixb=*(im_srcIn+xpos2+5+(LineLength * (ypos1+5))); 

										if((pixa - pixb) > theapp->jobinfo[pframe->CurrentJobNum].limit_no_cap )
										{
											if(xpos2- xpos1 > theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap)
											{
													no_cap_detected=true;
													no_cap_x=xpos2;//for On Draw
													no_cap_y=ypos1+5;// for On Draw
													xpos2 = CapLeftTop.x+25;
													xpos1 = CapLeftTop.x+25;
													ypos1 = CapLeftTop.y+30;
											}
											else
											{
													xpos2 = CapLeftTop.x+25;
													xpos1 = CapLeftTop.x+25;
											}


										}
								}


							
							

							}



					}



				}
			
	


}

//NOT USED
//Loads lip pattern image for the specified job number. Loads last image from camera 1.
//Job: job number of image to load.
void CXAxisView::OnLoadPattern(int Job)
{

	bool read_successfull =true;
	CVisByteImage image;
	
	char buf[10];
	char currentpath[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	
	c=itoa(Job,buf,10);
		
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,c);
	strcat(currentpath,".bmp");

	try
	{
		image.ReadFile(currentpath);
		
		imageBW.Allocate(image.Rect());
//image.SetRect(0,40,640,480);//cam 1
		image.CopyPixelsTo(imageBW);
		im_data=imageBW.PixelAddress(0,0,1);

	
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
			// Give up trying to read an image from this file.
			// Warn the user.
	//	AfxMessageBox("The file specified could not be opened. You will need to setup a new job");
			pframe->prompt_code=5;
			Prompt3 promtp3;
			promtp3.DoModal();
		pframe->JobNoGood=true;
		read_successfull=false;
			//return FALSE;
	}


	//Now load the pattern from the above Image

	int ypos=0;
	int ypos2=0;
	int xpos=0;
	int xpos2=0;
	int xpos3=0;
		
	int LineLength=1024;
	int LineLength2=60*4;
	int LineLength3=60;
	int cnt=0;
	int X_Lside=NeckLSide.x;
	int X_Rside=NeckRSide.x;
//	if(left==true)
//		{
	if(read_successfull == true) 
	{
		X_Lside=*(im_data+ 15 +(1024*10)); //Neck L Side.x
		X_Rside=*(im_data+ 17 +(1024*10 )); // Neck R Side .x 
	}
			for(ypos=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2-39; ypos<=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2; ypos++)
			{
				for(xpos=X_Lside-49;xpos<=X_Lside+10;xpos++)
				{	
					
					//only for show
					*(im_patl + xpos2 + LineLength2 *(ypos2))= *(im_data + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +1 + LineLength2 *(ypos2))= *(im_data + xpos + LineLength * (ypos));
					*(im_patl + xpos2 +2 + LineLength2 *(ypos2))= *(im_data + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}
			//SavePattern(im_matchl,true);
		
	//	}


			for(ypos=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2-39; ypos<=(theapp->jobinfo[pframe->CurrentJobNum].neckBarY)*2; ypos++)
			{
				for(xpos=X_Rside-10;xpos<=X_Rside+49;xpos++)
				{	
					//only for show
					*(im_patr + xpos2 + LineLength2 *(ypos2))= *(im_data + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +1 + LineLength2 *(ypos2))= *(im_data + xpos + LineLength * (ypos));
					*(im_patr + xpos2 +2 + LineLength2 *(ypos2))= *(im_data + xpos + LineLength * (ypos));
					xpos2+=4;
					xpos3+=1;
					cnt+=1;
				}
				xpos3=0;
				xpos2=0;
				ypos2+=1;
			}


		
}

//Saves the lip pattern found during teaching.
//im_sr: Holds pointer to image of lip pattern to save.
//left: true = left left lip pattern, false = right lip pattern.
void CXAxisView::SaveLipPattern(BYTE *im_sr,bool left)
{

	char buf[10];
	char currentpath[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive

	c =itoa(pframe->CurrentJobNum,buf,10);
	
	if(left == true)
	{

	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,"Left");
	strcat(currentpath,".bmp");
	}
	else if( left == false)
	{
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,"Right");
	strcat(currentpath,".bmp");
	}

	CVisByteImage image(60,40,1,-1,im_sr);
	//image.SetRect(0,40,640,480);
	imagepat=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = currentpath;

		imagepat.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
	//	AfxMessageBox("The pattern file could not be saved.");

			pframe->prompt_code=6;
			Prompt3 promtp3;
			promtp3.DoModal();


		
	}

}

//NOT USED
void CXAxisView::OnLoadLipPattern(int Job)
{

	CVisByteImage image;
	
	char buf[10];
	char currentpath[60];
	char* pFileName = "c:\\silgandata\\pat\\";//drive
	
	c=itoa(Job,buf,10);
		
	strcpy(currentpath,pFileName);
	strcat(currentpath,c);
	strcat(currentpath,"\\");
	strcat(currentpath,"Left");
	strcat(currentpath,".bmp");

	try
	{
		image.ReadFile(currentpath);
		
		imageBW.Allocate(image.Rect());
//image.SetRect(0,40,640,480);//cam 1
		image.CopyPixelsTo(imageBW);
		im_matchl=imageBW.PixelAddress(0,0,1);

	
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
			// Give up trying to read an image from this file.
			// Warn the user.
	//	AfxMessageBox("The file specified could not be opened. You will need to setup a new job");
			pframe->prompt_code=5;
			Prompt3 promtp3;
			promtp3.DoModal();

		pframe->JobNoGood=true;
			//return FALSE;
	}
}

//Load 50 pictures for no hardware connected testing.
void CXAxisView::Load50Pics()
{
	//For now its load 30 pics :) 

	bool messedup =false;


	char* pFileName;
	char* pFileNameL;
	char* pFileNameR;

	
    //Camera 1


	pFileName="c:\\SilganData\\51R56\\";//cam1sd.bmp";
	if(count_pic > 50 ) count_pic=1;   //Reset After every 30 pics


	char buf1[10];
	char currentpath1[80];

	
	CString c =itoa(count_pic,buf1,10);
	CString d = c;

	strcpy(currentpath1,pFileName);
	strcat(currentpath1,d);
	strcat(currentpath1,"saved.bmp");

	bool success=0;
	CFile ImageFile1;


	CVisRGBAByteImage image;
	
//	success=ImageFile1.Open(currentpath1, CFile::modeRead);

//	if(success !=0)
//	{

		try
		{
			image.ReadFile(currentpath1);//currentpath1);//"C:\\SilganData\\51R56\\1saved.bmp"
			imageFile1.Allocate(image.Rect());

			image.CopyPixelsTo(imageFile1);
		
			im_dataC=imageFile1.PbFirstPixelInRow(0);

			
		}
		catch(...)
		{
		//	AfxMessageBox("The file specified could not be opened. load image");
				pframe->prompt_code=4;
			Prompt3 promtp3;
			promtp3.DoModal();
			messedup=true;
		}

//	}
//if(!freeze_current_pic)	
count_pic++;

	//Camera 2

	CVisRGBAByteImage imageL;

	pFileNameL="c:\\silgandata\\51R56\\";//cam3sd.bmp";
	char buf2[10];
	char currentpath2[60];

	CString c2 =itoa(count_pic,buf2,10);
	CString d2 = c2;

	strcpy(currentpath2,pFileName);
	strcat(currentpath2,d2);
	strcat(currentpath2,"saved.bmp");
//	bool success2=ImageFile.Open(currentpath2, CFile::modeRead);

//	if(success2 !=0)
//	{
		try
		{
		

			imageL.ReadFile(currentpath2);
			imageFileL.Allocate(imageL.Rect());

			imageL.CopyPixelsTo(imageFileL);
	
			im_dataCL=imageFileL.PbFirstPixelInRow(0);

			

		}

		catch(...)
		{
		//	AfxMessageBox("The file specified could not be opened. load image");

			pframe->prompt_code=4;
			Prompt3 promtp3;
			promtp3.DoModal();
			messedup=true;
		}

//	}

//if(!freeze_current_pic)	
	count_pic++;

	//Camera 3 

	CVisRGBAByteImage imageR;
	pFileNameR="c:\\silgandata\\51R56\\";

	char buf3[10];
	char currentpath3[60];

	CString c3 =itoa(count_pic,buf3,10);
	CString d3 = c3;

	strcpy(currentpath3,pFileName);
	strcat(currentpath3,d3);
	strcat(currentpath3,"saved.bmp");

//	bool success3=ImageFile.Open(currentpath3, CFile::modeRead);


//	if(success3 !=0)
//	{
	

		try
		{
			imageR.ReadFile(currentpath3);
			imageFileR.Allocate(imageR.Rect());

			imageR.CopyPixelsTo(imageFileR);
		
			im_dataCR=imageFileR.PbFirstPixelInRow(0);

			
		 
		}
		catch (...) // "..." is bad - we could leak an exception object.
		{
		
		//	AfxMessageBox("The file specified could not be opened.");

			pframe->prompt_code=3;
			Prompt3 promtp3;
			promtp3.DoModal();
			messedup=true;
			//return FALSE;
		}

//	}
//if(!freeze_current_pic) 
 count_pic++;

	if(messedup==false)
	{	

	im_cam1Color=FlipImage1(im_dataC);//im_cam1Color=im_dataC;//
	im_cam2Color=FlipImage2(im_dataCL);//	im_cam2Color=im_dataCL;//
    im_cam3Color=FlipImage3(im_dataCR);//im_cam3Color=im_dataCR;//	

	im_data=im_cam1=CtoBW(im_dataC);
	im_data2=im_cam2=CtoBW2(im_dataCL);
	im_data3=im_cam3=CtoBW3(im_dataCR);
	//if(KillSave==false) OnSaveC(m_imageProcessed.pData,1);
	
	
	im_reduce1=Reduce(im_dataC);
	im_reduce2=Reduce2(im_dataCL);
	im_reduce3=Reduce3(im_dataCR);

	CVisRGBAByteImage image1(theapp->cam1Width/2,theapp->cam1Height/2,1,-1,im_reduce1);
	CVisRGBAByteImage image2(theapp->cam2Width/2,theapp->cam2Height/2,1,-1,im_reduce2);
	CVisRGBAByteImage image3(theapp->cam3Width/2,theapp->cam3Height/2,1,-1,im_reduce3);
	
	if(theapp->saveOne2c==true){OnSaveC(im_cam2Color,2);theapp->saveOne2c=false;}
	if(theapp->saveOne2==true){OnSaveBW(im_cam2,2);theapp->saveOne2=false;}

	
		//////paint image to screen
	imageC=image1;
	imageC2=image2;
	imageC3=image3;

	}


}

//NOT USED, Edge detection for reduced size image.
void CXAxisView::CalcSobel_Reduced(BYTE *Image)
{
	

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	
	int resultr=0;

	int LineLength=theapp->cam1Width;

	int startx=0;
	int endx=theapp->cam1Width;
	int starty=0;
	int endy=theapp->cam1Height;
	
	int l=0;
	int j=0;

	
	for(int i=starty; i<endy-2;i+=2)
	{
		for(int k=startx; k<endx-2;k+=2)
		{
			pix1=*((Image+k +LineLength * (i)));
			pix2=*((Image+k+1+LineLength * (i)));
		
			pix3=*((Image+k +LineLength * (i+1)));
			pix4=*((Image+k +1 +LineLength * (i+1)));
		
			resultr=(pix1+pix2+pix3+pix4)/4;
		
	
			*(sobel_cam1_reduced +j +(LineLength/2) *l )=resultr;
		
		if((j<theapp->cam1Width/2)-2)j+=1;
	
		}
	
		if(l<=(theapp->cam1Height/2)-2)l+=1;
	
		j=0;
	}
	


			
}

void CXAxisView::BinarizeFillLevelArea(BYTE *Image)
{

	int pix1=0;
	int pix2=0;
	int pix3=0;
	int pix4=0;
	int pix5=0;
	int pix6=0;
	int pix1x=0;
	int pix2x=0;
	int pix3x=0;
	int pix4x=0;
	int pix5x=0;
	int pix6x=0;
	int resultr=0;
	int resultg=0;
	int resultb=0;
		
	int LineLength=theapp->cam1Width*4;

	int startx=0;
	int endx=theapp->cam1Width*4;
	int starty=0;
	int endy=theapp->cam1Height;
	
	int l=theapp->cam1Height/2;		//height index of resized image. Size of image is halved. Starts from bottom of image.
	int j=theapp->cam1Width*4/2;	//width index of resized image. Size of image is halved. Starts from right side of image.

	//int fillx=endx-( (MidTop.x*4)+ (theapp->jobinfo[pframe->CurrentJobNum].fillX*4)  );
	//int fillx=( (1024-(theapp->jobinfo[pframe->CurrentJobNum].fillX*2)*4)+((1020-MidTop.x)*4) )-300*4;
	
	//variables used when extra light option is enabled
	int fillx=( (1024*4)/2 )-(225*4/2);	//Left x coordinate from which binary area of the image is suppose to start.
	int fillx2=fillx+225*4;			//Right x coordinate at which binary area of the image is suppose to end.
	
	int filly=endy-(theapp->jobinfo[pframe->CurrentJobNum].fillArea*2)-(120*2);//Lower y coordinate from which binary area of the image is suppose to start.
	int filly2=filly+(120*2);	//Upper y coordinate at which binary area of the image is suppose to end.


	//int z=(theapp->cam1Width*4)-8;
	//int o=theapp->cam1Height;
	for(int i=starty; i<endy-2;)
	{
		for(int k=startx; k<endx-4;)
		{
			//Take 2 pixels from horizontal line. 0 offset = R byte, 1 offset = G byte, 2 offset = B byte, 3 offset transparency value.
			pix1=*((Image+k +LineLength * (i)));		//R byte, pixel 1, line a
			pix2=*((Image+k+1+LineLength * (i)));		//G byte, pixel 1, line a
			pix3=*((Image+k +2+LineLength * (i)));		//B byte, pixel 1, line a
			pix4=*((Image+k +4 +LineLength * (i)));		//R byte, pixel 2, line a
			pix5=*((Image+k +5 +LineLength * (i)));		//G byte, pixel 2, line a
			pix6=*((Image+k +6 +LineLength * (i)));		//B byte, pixel 2, line a

			//Take 2 pixels from the next horizontal line
			pix1x=*((Image+k +LineLength * (i+1)));		//R byte, pixel 1, line b
			pix2x=*((Image+k+1+LineLength * (i+1)));	//G byte, pixel 1, line b
			pix3x=*((Image+k +2+LineLength * (i+1)));	//B byte, pixel 1, line b
			pix4x=*((Image+k +4 +LineLength * (i+1)));	//R byte, pixel 2, line b
			pix5x=*((Image+k +5 +LineLength * (i+1)));	//G byte, pixel 2, line b
			pix6x=*((Image+k +6 +LineLength * (i+1)));	//B byte, pixel 2, line b

			//Average 2x2 pixels
			resultr=(pix1+pix4+pix1x+pix4x)/4;
			resultg=(pix2+pix5+pix2x+pix5x)/4;
			resultb=(pix3+pix6+pix3x+pix6x)/4;
		
		//creating a binary image when Xtra Light option is enabled
		if(theapp->jobinfo[pframe->CurrentJobNum].xtraLight==true)
		{
			//If in area which is suppose to be converted to binary
			if(i>filly && i< filly2 && k>fillx && k<fillx2)
			{

				//If below binary threshold
				if(resultb <=theapp->jobinfo[pframe->CurrentJobNum].redCutOff)
				{
					//Mark as black
					*(im_reduce +j +(LineLength/2) *l )  =0;
					*(im_reduce +j+1 +(LineLength/2) *l )=0;
					*(im_reduce +j+2 +(LineLength/2) *l )=0;

				}
				else //Otherwise mark as white.
				{
					*(im_reduce +j +(LineLength/2) *l )=255;
					*(im_reduce +j+1 +(LineLength/2) *l )=255;
					*(im_reduce +j+2 +(LineLength/2) *l )=255;
				}
			}
			else
			{
				*(im_reduce +j +(LineLength/2) *l )=resultr;
				*(im_reduce +j+1 +(LineLength/2) *l )=resultg;
				*(im_reduce +j+2 +(LineLength/2) *l )=resultb;
			}
		}
		else
		{
			*(im_reduce +j +(LineLength/2) *l )=resultr;
			*(im_reduce +j+1 +(LineLength/2) *l )=resultg;
			*(im_reduce +j+2 +(LineLength/2) *l )=resultb;
		}
			
			k+=8;	//Increment horizontal index for original image by 2 pixels, i.e. 8 bytes.
			//if(z>=8)z-=8;
			if(j>=4)j-=4;	//Check bound and decrement horizontal index of averaged image.
			
		}
		i+=2;			//Increment vertical index for original image by 2 lines.
		if(l>0)l-=1;	//Check bound and decrement vertical index of averaged image.
		//if(o>1)o-=2;
		//z=(theapp->cam1Width*4)-8;
		j=theapp->cam1Width*4/2;
	}
}

void CXAxisView::Foil_Sensor(BYTE *Image, int startx,int endx,int starty,int endy, bool& result)
{
//	bool result=false;
	int LineLength=theapp->cam1Width;


	int sum_total=0,count=0,pix1=0,average=0;


	for(int i=starty; i<endy;i++)
	{
		for(int k=startx; k<endx;k++)
		{
			
			pix1=*((Image+k +LineLength * (i)));

			sum_total+=pix1;

			count++;
		}
	}

	if(count >0) 
	{
		average=(int) sum_total/count;
		if(average <10) result=true; 
	
	}


//	return &result;

}
