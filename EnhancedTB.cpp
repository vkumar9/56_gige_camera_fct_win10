// EnhancedTB.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "EnhancedTB.h"
#include "MainFrm.h"
#include "XAxisView.h"
#include "Modify.h"
#include "Prompt3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EnhancedTB dialog


EnhancedTB::EnhancedTB(CWnd* pParent /*=NULL*/)
	: CDialog(EnhancedTB::IDD, pParent)
{
	//{{AFX_DATA_INIT(EnhancedTB)
	m_edit1 = _T("");
	m_edit2 = _T("");
	m_edit5 = _T("");
	m_edit6 = _T("");
	m_edit7 = _T("");
	m_edit8 = _T("");
	//}}AFX_DATA_INIT
}


void EnhancedTB::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EnhancedTB)
	DDX_Control(pDX, IDC_BUTTON13, m_button13);
	DDX_Control(pDX, IDC_CHECK8, m_check8);
	DDX_Control(pDX, IDC_CHECK7, m_check7);
	DDX_Control(pDX, IDC_CHECK6, m_check66);
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_EDIT2, m_edit2);
	DDX_Text(pDX, IDC_EDIT5, m_edit5);
	DDX_Text(pDX, IDC_EDIT6, m_edit6);
	DDX_Text(pDX, IDC_EDIT7, m_edit7);
	DDX_Text(pDX, IDC_EDIT8, m_edit8);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EnhancedTB, CDialog)
	//{{AFX_MSG_MAP(EnhancedTB)
	ON_BN_CLICKED(IDC_BUTTON1, OnCam1LLincre)
	ON_BN_CLICKED(IDC_BUTTON5, OnCam1ULincre)
	ON_BN_CLICKED(IDC_BUTTON6, OnCam2LLincre)
	ON_BN_CLICKED(IDC_BUTTON9, OnCam3LLincre)
	ON_BN_CLICKED(IDC_BUTTON7, OnCam2ULincre)
	ON_BN_CLICKED(IDC_BUTTON11, OnCam3ULincre)
	ON_BN_CLICKED(IDC_BUTTON2, OnCam1LLdcr)
	ON_BN_CLICKED(IDC_BUTTON4, OnCam2LLdcr)
	ON_BN_CLICKED(IDC_BUTTON10, OnCam3LLdcr)
	ON_BN_CLICKED(IDC_BUTTON3, OnCam1ULdcr)
	ON_BN_CLICKED(IDC_BUTTON8, OnCam2ULdcr)
	ON_BN_CLICKED(IDC_BUTTON12, OnCam3ULdcr)
	ON_BN_CLICKED(IDC_CHECK6, OnCheck6)
	ON_BN_CLICKED(IDC_CHECK7, OnCheck7)
	ON_BN_CLICKED(IDC_CHECK8, OnCheck8)
	ON_WM_CANCELMODE()
	ON_WM_CAPTURECHANGED()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_BUTTON13, OnButton13)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EnhancedTB message handlers

void EnhancedTB::OnOK() 
{
	pframe->SaveJob(pframe->CurrentJobNum);
	CDialog::OnOK();
}

void EnhancedTB::OnCam1LLincre() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam1_ll <240){theapp->jobinfo[pframe->CurrentJobNum].cam1_ll+=step_size;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam1ULincre() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam1_ul <240){theapp->jobinfo[pframe->CurrentJobNum].cam1_ul+=step_size;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam2LLincre() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam2_ll <240){theapp->jobinfo[pframe->CurrentJobNum].cam2_ll+=step_size;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam3LLincre() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam3_ll <240){theapp->jobinfo[pframe->CurrentJobNum].cam3_ll+=step_size;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam2ULincre() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam2_ul <240){theapp->jobinfo[pframe->CurrentJobNum].cam2_ul+=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam3ULincre() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam3_ul <240){theapp->jobinfo[pframe->CurrentJobNum].cam3_ul+=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam1LLdcr() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam1_ll >20){theapp->jobinfo[pframe->CurrentJobNum].cam1_ll-=step_size;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam2LLdcr() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam2_ll >20){theapp->jobinfo[pframe->CurrentJobNum].cam2_ll-=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam3LLdcr() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].cam3_ll >20){theapp->jobinfo[pframe->CurrentJobNum].cam3_ll-=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam1ULdcr() 
{
	
	if(theapp->jobinfo[pframe->CurrentJobNum].cam1_ul >20){theapp->jobinfo[pframe->CurrentJobNum].cam1_ul-=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam2ULdcr() 
{

	if(theapp->jobinfo[pframe->CurrentJobNum].cam2_ul >20){theapp->jobinfo[pframe->CurrentJobNum].cam2_ul-=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::OnCam3ULdcr() 
{
	
	if(theapp->jobinfo[pframe->CurrentJobNum].cam3_ul >20){theapp->jobinfo[pframe->CurrentJobNum].cam3_ul-=1;}
	UpdateData(false);
	UpdateDisplay();
}

void EnhancedTB::UpdateDisplay()
{
	m_edit1.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam1_ll);
	m_edit2.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam2_ul);
	m_edit5.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam1_ul);
	m_edit6.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam2_ll);
	m_edit7.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam3_ll);
	m_edit8.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam3_ul);

	pframe->SaveJob(pframe->CurrentJobNum);

	UpdateData(false);


}

BOOL EnhancedTB::OnInitDialog() 
{
	CDialog::OnInitDialog();
//	if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true)  //Should be enabled from the Limits tab
//	{
	
		pframe=(CMainFrame*)AfxGetMainWnd();
		pframe->m_penhancedtb=this;
		theapp=(CCapApp*)AfxGetApp();
		step_size=5;
			m_button13.SetWindowText("5");
		if(pframe->CurrentJobNum != 0)
		{
			m_edit1.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam1_ll);
			m_edit2.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam2_ul);
			m_edit5.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam1_ul);
			m_edit6.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam2_ll);
			m_edit7.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam3_ll);
			m_edit8.Format("%3i ",theapp->jobinfo[pframe->CurrentJobNum].cam3_ul);
		}
	if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true)  //Should be enabled from the Limits tab
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 == false){m_check66.SetWindowText("OFF");CheckDlgButton(IDC_CHECK6,0); }
			else{m_check66.SetWindowText("ON");CheckDlgButton(IDC_CHECK6,1); }

			if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 == false){m_check7.SetWindowText("OFF");CheckDlgButton(IDC_CHECK7,0); }
			else{m_check7.SetWindowText("ON");CheckDlgButton(IDC_CHECK7,1); }

			if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 == false){m_check8.SetWindowText("OFF");CheckDlgButton(IDC_CHECK8,0); }
			else{m_check8.SetWindowText("ON");CheckDlgButton(IDC_CHECK8,1); }
		
		UpdateData(false);

	}
	else
	{
	//	AfxMessageBox("Please Turn ON Enhanced TB inspection from Limits Tab",MB_OK);
			pframe->prompt_code=7;
			Prompt3 promtp3;
			promtp3.DoModal();
	
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void EnhancedTB::OnCheck6() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 == false)
		{
			theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 =true;
			CheckDlgButton(IDC_CHECK6,1); 
			m_check66.SetWindowText("ON");
		}
		else
		{ 
		
			theapp->jobinfo[pframe->CurrentJobNum].etb_cam1 =false;
			CheckDlgButton(IDC_CHECK6,0); 
			m_check66.SetWindowText("OFF");
		}
	}
	else{
	//	AfxMessageBox("Please Turn ON Enhanced TB inspection from Limits Tab",MB_OK);
			pframe->prompt_code=7;
			Prompt3 promtp3;
			promtp3.DoModal();
	
	}
	
	UpdateData(false);		
}

void EnhancedTB::OnCheck7() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true)  //Should be enabled from the Limits tab
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 == false)
		{
			theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 =true;
			CheckDlgButton(IDC_CHECK7,1); 
			m_check7.SetWindowText("ON");
		}
		else
		{ 
		
			theapp->jobinfo[pframe->CurrentJobNum].etb_cam2 =false;
			CheckDlgButton(IDC_CHECK7,0); 
			m_check7.SetWindowText("OFF");
		}
	}
		
	else{
	//	AfxMessageBox("Please Turn ON Enhanced TB inspection from Limits Tab",MB_OK);
		pframe->prompt_code=7;
			Prompt3 promtp3;
			promtp3.DoModal();
	}
	
	UpdateData(false);		
}

void EnhancedTB::OnCheck8() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel == true)  //Should be enabled from the Limits tab
	{
	
		if(theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 == false)
		{
			theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 =true;
			CheckDlgButton(IDC_CHECK8,1); 
			m_check8.SetWindowText("ON");
		}
		else
		{ 
		
			theapp->jobinfo[pframe->CurrentJobNum].etb_cam3 =false;
			CheckDlgButton(IDC_CHECK8,0); 
			m_check8.SetWindowText("OFF");
		}
	}
	else{
	//	AfxMessageBox("Please Turn ON Enhanced TB inspection from Limits Tab",MB_OK);
			pframe->prompt_code=7;
			Prompt3 promtp3;
			promtp3.DoModal();
	
	
	}
	
	UpdateData(false);		
	
}

void EnhancedTB::OnCancelMode() 
{
	CDialog::OnCancelMode();
	
//	AfxMessageBox("On Cancel");
}

void EnhancedTB::OnCaptureChanged(CWnd *pWnd) 
{
	// TODO: Add your message handler code here
	
	CDialog::OnCaptureChanged(pWnd);
}

void EnhancedTB::OnClose() 
{
	pframe->SaveJob(pframe->CurrentJobNum);
	
	CDialog::OnClose();
}

void EnhancedTB::OnRadio1() 
{
if(step_size==1)step_size=5;
else step_size=1;
}

void EnhancedTB::OnButton13() 
{
if(step_size==1)
{
	step_size=5;
	m_button13.SetWindowText("5");
}
else 
{
	step_size=1;
		m_button13.SetWindowText("1");
}
	UpdateData(false);
}
