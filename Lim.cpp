// Lim.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Lim.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "capview.h"
#include "EnhancedTB.h"
#include "Prompt3.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Lim dialog


Lim::Lim(CWnd* pParent /*=NULL*/)
	: CDialog(Lim::IDD, pParent)
{
	//{{AFX_DATA_INIT(Lim)
	m_1 = _T("");
	m_2 = _T("");
	m_5 = _T("");
	m_4 = _T("");
	m_3 = _T("");
	m_6 = _T("");
	m_8 = _T("");
	m_7 = _T("");
	m_m1 = _T("");
	m_m2 = _T("");
	m_m3 = _T("");
	m_m4 = _T("");
	m_m5 = _T("");
	m_m6 = _T("");
	m_m7 = _T("");
	m_m8 = _T("");
	m_l2 = _T("");
	m_l1 = _T("");
	m_l3 = _T("");
	m_l4 = _T("");
	m_l5 = _T("");
	m_l6 = _T("");
	m_l7 = _T("");
	m_l8 = _T("");
	m_1max = _T("");
	m_2max = _T("");
	m_3max = _T("");
	m_4max = _T("");
	m_5max = _T("");
	m_6max = _T("");
	m_7max = _T("");
	m_8max = _T("");
	m_1min = _T("");
	m_2min = _T("");
	m_3min = _T("");
	m_4min = _T("");
	m_5min = _T("");
	m_6min = _T("");
	m_7min = _T("");
	m_8min = _T("");
	m_10 = _T("");
	m_l10 = _T("");
	m_m10 = _T("");
	m_10max = _T("");
	m_10min = _T("");
	m_9 = _T("");
	m_l9 = _T("");
	m_m9 = _T("");
	m_9max = _T("");
	m_9min = _T("");
	m_edit1 = _T("");
	//}}AFX_DATA_INIT
}


void Lim::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Lim)
	DDX_Control(pDX, IDC_10UP, m_10up);
	DDX_Control(pDX, IDC_10DN, m_10dn);
	DDX_Control(pDX, IDC_6UP, m_6upc);
	DDX_Control(pDX, IDC_6DN, m_6dnc);
	DDX_Control(pDX, IDC_6, m_6c);
	DDX_Control(pDX, IDC_1UP, m_1up);
	DDX_Control(pDX, IDC_1DN, m_1dn);
	DDX_Text(pDX, IDC_1, m_1);
	DDX_Text(pDX, IDC_2, m_2);
	DDX_Text(pDX, IDC_5, m_5);
	DDX_Text(pDX, IDC_4, m_4);
	DDX_Text(pDX, IDC_3, m_3);
	DDX_Text(pDX, IDC_6, m_6);
	DDX_Text(pDX, IDC_8, m_8);
	DDX_Text(pDX, IDC_7, m_7);
	DDX_Text(pDX, IDC_M1, m_m1);
	DDX_Text(pDX, IDC_M2, m_m2);
	DDX_Text(pDX, IDC_M3, m_m3);
	DDX_Text(pDX, IDC_M4, m_m4);
	DDX_Text(pDX, IDC_M5, m_m5);
	DDX_Text(pDX, IDC_M6, m_m6);
	DDX_Text(pDX, IDC_M7, m_m7);
	DDX_Text(pDX, IDC_M8, m_m8);
	DDX_Text(pDX, IDC_L2, m_l2);
	DDX_Text(pDX, IDC_L1, m_l1);
	DDX_Text(pDX, IDC_L3, m_l3);
	DDX_Text(pDX, IDC_L4, m_l4);
	DDX_Text(pDX, IDC_L5, m_l5);
	DDX_Text(pDX, IDC_L6, m_l6);
	DDX_Text(pDX, IDC_L7, m_l7);
	DDX_Text(pDX, IDC_L8, m_l8);
	DDX_Text(pDX, IDC_MAX1, m_1max);
	DDX_Text(pDX, IDC_MAX2, m_2max);
	DDX_Text(pDX, IDC_MAX3, m_3max);
	DDX_Text(pDX, IDC_MAX4, m_4max);
	DDX_Text(pDX, IDC_MAX5, m_5max);
	DDX_Text(pDX, IDC_MAX6, m_6max);
	DDX_Text(pDX, IDC_MAX7, m_7max);
	DDX_Text(pDX, IDC_MAX8, m_8max);
	DDX_Text(pDX, IDC_MIN1, m_1min);
	DDX_Text(pDX, IDC_MIN2, m_2min);
	DDX_Text(pDX, IDC_MIN3, m_3min);
	DDX_Text(pDX, IDC_MIN4, m_4min);
	DDX_Text(pDX, IDC_MIN5, m_5min);
	DDX_Text(pDX, IDC_MIN6, m_6min);
	DDX_Text(pDX, IDC_MIN7, m_7min);
	DDX_Text(pDX, IDC_MIN8, m_8min);
	DDX_Text(pDX, IDC_10, m_10);
	DDX_Text(pDX, IDC_L10, m_l10);
	DDX_Text(pDX, IDC_M10, m_m10);
	DDX_Text(pDX, IDC_MAX10, m_10max);
	DDX_Text(pDX, IDC_MIN10, m_10min);
	DDX_Text(pDX, IDC_9, m_9);
	DDX_Text(pDX, IDC_L9, m_l9);
	DDX_Text(pDX, IDC_MAX9, m_9max);
	DDX_Text(pDX, IDC_MIN9, m_9min);
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_E1, m_enable1);
	DDX_Text(pDX, IDC_E2, m_enable2);
	DDX_Text(pDX, IDC_E3, m_enable3);
	DDX_Text(pDX, IDC_E4, m_enable4);
	DDX_Text(pDX, IDC_E5, m_enable5);
	DDX_Text(pDX, IDC_E6, m_enable6);
	DDX_Text(pDX, IDC_RADIO5, m_enableRear);
	DDX_Text(pDX, IDC_E6b, m_enable6b);
	DDX_Text(pDX, IDC_E6c, m_enable6c);
	DDX_Text(pDX, IDC_E9b, m_enable9b);
	DDX_Text(pDX, IDC_E9c, m_enable9c);
	DDX_Text(pDX, IDC_E7, m_enable7);
	DDX_Text(pDX, IDC_E8, m_enable8);
	DDX_Text(pDX, IDC_E9, m_enable9);
	DDX_Text(pDX, IDC_FINE, m_fine);
	DDX_Text(pDX,IDC_enableTB,m_enabletb);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Lim, CDialog)
	//{{AFX_MSG_MAP(Lim)
	ON_BN_CLICKED(IDC_1UP, On1up)
	ON_BN_CLICKED(IDC_1DN, On1dn)
	ON_BN_CLICKED(IDC_2UP, On2up)
	ON_BN_CLICKED(IDC_2DN, On2dn)
	ON_BN_CLICKED(IDC_3UP, On3up)
	ON_BN_CLICKED(IDC_3DN, On3dn)
	ON_BN_CLICKED(IDC_4UP, On4up)
	ON_BN_CLICKED(IDC_4DN, On4dn)
	ON_BN_CLICKED(IDC_5UP, On5up)
	ON_BN_CLICKED(IDC_5DN, On5dn)
	ON_BN_CLICKED(IDC_6UP, On6up)
	ON_BN_CLICKED(IDC_6DN, On6dn)
	ON_BN_CLICKED(IDC_7UP, On7up)
	ON_BN_CLICKED(IDC_7DN, On7dn)
	ON_BN_CLICKED(IDC_8UP, On8up)
	ON_BN_CLICKED(IDC_8DN, On8dn)
	ON_BN_CLICKED(IDC_MAX, OnMax)
	ON_BN_CLICKED(IDC_MIN, OnMin)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_E1, OnE1)
	ON_BN_CLICKED(IDC_E2, OnE2)
	ON_BN_CLICKED(IDC_E3, OnE3)
	ON_BN_CLICKED(IDC_E4, OnE4)
	ON_BN_CLICKED(IDC_E5, OnE5)
	ON_BN_CLICKED(IDC_E6, OnE6)
	ON_BN_CLICKED(IDC_E7, OnE7)
	ON_BN_CLICKED(IDC_FINE, OnFine)
	ON_BN_CLICKED(IDC_E10, OnE10)
	ON_BN_CLICKED(IDC_10UP, On10up)
	ON_BN_CLICKED(IDC_10DN, On10dn)
	ON_BN_CLICKED(IDC_E9, OnE9)
	ON_BN_CLICKED(IDC_9UP, On9up)
	ON_BN_CLICKED(IDC_9DN, On9dn)
	ON_BN_CLICKED(IDC_E8, OnE8)
	ON_BN_CLICKED(IDC_E6b, OnE6b)
	ON_BN_CLICKED(IDC_E6c, OnE6c)
	ON_BN_CLICKED(IDC_E9b, OnE9b)
	ON_BN_CLICKED(IDC_E9c, OnE9c)
	ON_BN_CLICKED(IDC_BUTTON1, OnhtUp)
	ON_BN_CLICKED(IDC_BUTTON2, OnhtDwn)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_BN_CLICKED(IDC_enableTB, OnenableTB)
	ON_BN_CLICKED(IDC_BUTTON3, OnAdjustingLimits)
	ON_BN_CLICKED(IDC_BUTTON4, OnAuto)
	ON_BN_CLICKED(IDC_BUTTON6, OnEnableRearSurface)
	ON_BN_CLICKED(IDC_RADIO5, OnRadio5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Lim message handlers

BOOL Lim::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_plimit=this;
	theapp=(CCapApp*)AfxGetApp();

	Max=true;
	CheckDlgButton(IDC_FINE,0);
	m_fine.Format("%s","slow");
	res=1;
	fine=true;
	OnMax();
	AbsoluteMax=500;

	m_1.Format("%3i",0);
	m_2.Format("%3i",0);
	m_3.Format("%3i",0);
	m_4.Format("%3i",0);
	m_5.Format("%3i",0);
	m_6.Format("%3i",0);
	m_7.Format("%3i",0);
	m_8.Format("%3i",0);
	m_9.Format("%3i",0);

	m_l1.Format("%s",theapp->inspctn[1].name);
	m_l2.Format("%s",theapp->inspctn[2].name);
	m_l3.Format("%s",theapp->inspctn[3].name);
	m_l4.Format("%s",theapp->inspctn[4].name);
	m_l5.Format("%s",theapp->inspctn[5].name);
	m_l6.Format("%s",theapp->inspctn[6].name);
	m_l7.Format("%s",theapp->inspctn[7].name);
	m_l8.Format("%s",theapp->inspctn[8].name);
	m_l9.Format("%s",theapp->inspctn[9].name);

/*	if(theapp->checkRearHeight ==true){CheckDlgButton(IDC_CHECK1,1);}
	else{CheckDlgButton(IDC_CHECK1,0);}
*/

//	m_edit1.Format("%d",theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff);
	

	UpdateData(false);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//--------------------------------------------------------------------
//Limits Operation:
//There is a counter for each inspection type indicating how many pixels
//fall outside the range of lower and upper limits, i.e. are above the upper limit or below the lower limit.
//Each inspection type can be turned on or off by using the corresponding toggle buttons.
//If an inspection is not on its limits are not applied and its counter is no incremented.

//--------------------------------------------------------------------


//Updates the limits tab to display all the values and controls based on what was previously saved.
void Lim::UpdateDisplay()
{
	if(Max==true)	//Load max limit values into the corresponding text boxes.
	{
		m_1.Format("%3i",theapp->inspctn[1].lmax);
		m_2.Format("%3i",theapp->inspctn[2].lmax);
		m_3.Format("%3i",theapp->inspctn[3].lmax);
		m_4.Format("%3i",theapp->inspctn[4].lmax);
		m_5.Format("%3i",theapp->inspctn[5].lmax);
		m_6.Format("%3i",theapp->inspctn[6].lmax);
		m_7.Format("%3i",theapp->inspctn[7].lmax);
		m_8.Format("%3i",theapp->inspctn[8].lmax);
		m_9.Format("%3i",theapp->inspctn[9].lmax);
	}
	else   //Load min limit values into the corresponding text boxes.
	{
		m_1.Format("%3i",theapp->inspctn[1].lmin);
		m_2.Format("%3i",theapp->inspctn[2].lmin);
		m_3.Format("%3i",theapp->inspctn[3].lmin);
		m_4.Format("%3i",theapp->inspctn[4].lmin);
		m_5.Format("%3i",theapp->inspctn[5].lmin);
		m_6.Format("%3i",theapp->inspctn[6].lmin);
		m_7.Format("%3i",theapp->inspctn[7].lmin);
		m_8.Format("%3i",theapp->inspctn[8].lmin);
		m_9.Format("%3i",theapp->inspctn[9].lmin);

	}
		//Display inspection names above limit adjustment controls, e.g. height left, cocked.
		m_l1.Format("%s",theapp->inspctn[1].name);
		m_l2.Format("%s",theapp->inspctn[2].name);
		m_l3.Format("%s",theapp->inspctn[3].name);
		m_l4.Format("%s",theapp->inspctn[4].name);
		m_l5.Format("%s",theapp->inspctn[5].name);
		m_l6.Format("%s",theapp->inspctn[6].name);
		m_l7.Format("%s",theapp->inspctn[7].name);
		m_l8.Format("%s",theapp->inspctn[8].name);
		m_l9.Format("%s",theapp->inspctn[9].name);

	//	m_edit1.Format("%d",theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff);

	if( theapp->inspctn[1].enable==true ){CheckDlgButton(IDC_E1,0); m_enable1.Format("%s","ON"); }else{ CheckDlgButton(IDC_E1,1); m_enable1.Format("%s","OFF");}
	if( theapp->inspctn[2].enable==true ){CheckDlgButton(IDC_E2,0); m_enable2.Format("%s","ON"); }else{ CheckDlgButton(IDC_E2,1); m_enable2.Format("%s","OFF");}
	if( theapp->inspctn[3].enable==true ){CheckDlgButton(IDC_E3,0); m_enable3.Format("%s","ON"); }else{ CheckDlgButton(IDC_E3,1); m_enable3.Format("%s","OFF");}
	if( theapp->inspctn[4].enable==true ){CheckDlgButton(IDC_E4,0); m_enable4.Format("%s","ON"); }else{ CheckDlgButton(IDC_E4,1); m_enable4.Format("%s","OFF");}
	if( theapp->inspctn[5].enable==true ){CheckDlgButton(IDC_E5,0); m_enable5.Format("%s","ON"); }else{ CheckDlgButton(IDC_E5,1); m_enable5.Format("%s","OFF");}
	if( theapp->inspctn[6].enable==true ){CheckDlgButton(IDC_E6,0); m_enable6.Format("%s","ON"); }else{ CheckDlgButton(IDC_E6,1); m_enable6.Format("%s","OFF");}
	if( theapp->inspctn[7].enable==true ){CheckDlgButton(IDC_E7,0); m_enable7.Format("%s","ON"); }else{ CheckDlgButton(IDC_E7,1); m_enable7.Format("%s","OFF");}
	if( theapp->inspctn[8].enable==true ){CheckDlgButton(IDC_E8,0); m_enable8.Format("%s","ON"); }else{ CheckDlgButton(IDC_E8,1); m_enable8.Format("%s","OFF");}
	if( theapp->inspctn[9].enable==true ){CheckDlgButton(IDC_E9,0); m_enable9.Format("%s","ON"); }else{ CheckDlgButton(IDC_E9,1); m_enable9.Format("%s","OFF");}
	if( theapp->inspctn[10].enable==true ){CheckDlgButton(IDC_E10,0); m_enable10.Format("%s","ON"); }else{ CheckDlgButton(IDC_E10,1); m_enable10.Format("%s","OFF");}
	if( theapp->surfLR==true ){CheckDlgButton(IDC_E6b,0); m_enable6b.Format("%s","ON"); }else{ CheckDlgButton(IDC_E6b,1); m_enable6b.Format("%s","OFF");}
	if( theapp->surfRR==true ){CheckDlgButton(IDC_E6c,0); m_enable6c.Format("%s","ON"); }else{ CheckDlgButton(IDC_E6c,1); m_enable6c.Format("%s","OFF");}
	if( theapp->userRR==true ){CheckDlgButton(IDC_E9c,0); m_enable9c.Format("%s","ON"); }else{ CheckDlgButton(IDC_E9c,1); m_enable9c.Format("%s","OFF");}
	if( theapp->userLR==true ){CheckDlgButton(IDC_E9b,0); m_enable9b.Format("%s","ON"); }else{ CheckDlgButton(IDC_E9b,1); m_enable9b.Format("%s","OFF");}

//f( theapp->check_Rband==true ){CheckDlgButton(IDC_RADIO5,0); m_enableRear.Format("%s","ON"); }else{ CheckDlgButton(IDC_RADIO5,1);m_enableRear.Format("%s","OFF"); }///

	if ((theapp->surfLR==true) || (theapp->surfRR==true)){CheckDlgButton(IDC_RADIO5,0); m_enableRear.Format("%s","ON"); }
	else{ CheckDlgButton(IDC_RADIO5,1);m_enableRear.Format("%s","OFF"); }///
	
	if( theapp->jobinfo[pframe->CurrentJobNum].do_sobel==true ){CheckDlgButton(IDC_enableTB,0); m_enabletb.Format("%s","ON"); }else{ CheckDlgButton(IDC_enableTB,1); m_enabletb.Format("%s","OFF"); }

	//NOT USED
	if(theapp->inspctn[1].type==1){m_1min.Format("%s", "0"); m_1max.Format("%s", "255");}
	if(theapp->inspctn[2].type==1){m_2min.Format("%s", "0"); m_2max.Format("%s", "255");}
	if(theapp->inspctn[3].type==1){m_3min.Format("%s", "0"); m_3max.Format("%s", "255");}
	if(theapp->inspctn[4].type==1){m_4min.Format("%s", "0"); m_4max.Format("%s", "255");}
	if(theapp->inspctn[5].type==1){m_5min.Format("%s", "0"); m_5max.Format("%s", "255");}
	if(theapp->inspctn[6].type==1){m_6min.Format("%s", "0"); m_6max.Format("%s", "255");}
	if(theapp->inspctn[7].type==1){m_7min.Format("%s", "0"); m_7max.Format("%s", "255");}
	if(theapp->inspctn[8].type==1){m_8min.Format("%s", "0"); m_8max.Format("%s", "255");}

	//Sets the units for the inspection limits, e.g. pixels, grySc, RGB
	if(theapp->inspctn[1].type==2 ){m_1min.Format("%s", "pixels"); m_1max.Format("%s", "");}
	if(theapp->inspctn[2].type==2 ){m_2min.Format("%s", "pixels"); m_2max.Format("%s", "");}
	if(theapp->inspctn[3].type==2 ){m_3min.Format("%s", "pixels"); m_3max.Format("%s", "");}
	if(theapp->inspctn[4].type==2 ){m_4min.Format("%s", "pixels"); m_4max.Format("%s", "");}
	if(theapp->inspctn[5].type==2 ){m_5min.Format("%s", "grySc"); m_5max.Format("%s", "");}
	if(theapp->inspctn[6].type==2 ){m_6min.Format("%s", "grySc"); m_6max.Format("%s", "");}
	if(theapp->inspctn[7].type==2 ){m_7min.Format("%s", "grySc"); m_7max.Format("%s", "");}
	if(theapp->inspctn[8].type==2 ){m_8min.Format("%s", "RGB"); m_8max.Format("%s", "");}
	if(theapp->inspctn[9].type==2 ){m_9min.Format("%s", "grySc"); m_9max.Format("%s", "");}

	//NOT USED
	if(theapp->inspctn[1].type==3){m_1min.Format("%s", "0"); m_1max.Format("%s", "2.00");}
	if(theapp->inspctn[2].type==3){m_2min.Format("%s", "0"); m_2max.Format("%s", "2.00");}
	if(theapp->inspctn[3].type==3){m_3min.Format("%s", "0"); m_3max.Format("%s", "2.00");}
	if(theapp->inspctn[4].type==3){m_4min.Format("%s", "0"); m_4max.Format("%s", "2.00");}
	if(theapp->inspctn[5].type==3){m_5min.Format("%s", "0"); m_5max.Format("%s", "2.00");}
	if(theapp->inspctn[6].type==3){m_6min.Format("%s", "0"); m_6max.Format("%s", "2.00");}
	if(theapp->inspctn[7].type==3){m_7min.Format("%s", "0"); m_7max.Format("%s", "2.00");}
	if(theapp->inspctn[8].type==3){m_8min.Format("%s", "0"); m_8max.Format("%s", "2.00");}

	//Display proper inspection type name for Height middle based on inspection configuration.
	if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)
	{
			//m_name3.Format("%s","Height Middle");
		m_l3.Format("%s","Height Mid");
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
	{
			//m_name3.Format("%s","Sport Band");
		m_l3.Format("%s","Sport Band");
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].sport==2)
	{
			//m_name3.Format("%s","Sport Band");
		m_l3.Format("%s","Foil");
	}

	UpdateData(false);
}

//Increase the Max/Min limit for the left cap height inspection.
void Lim::On1up() 
{
	//if(theapp->inspctn[1].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}
	//if(theapp->inspctn[1].type==2) {res=1;}else{res=10;}
	if(Max==true) //If maximum limit adjustments are selected
	{
		theapp->inspctn[1].lmax+=res;				//Increase maximum limit by res amount.
		if(theapp->inspctn[1].lmax >=AbsoluteMax) theapp->inspctn[1].lmax=AbsoluteMax;	//Cannot go beyond maximum height value.
		m_1.Format("%3i",theapp->inspctn[1].lmax);
	}
	else
	{
		theapp->inspctn[1].lmin+=res;
		if(theapp->inspctn[1].lmin >=AbsoluteMax) theapp->inspctn[1].lmin=AbsoluteMax;
		m_1.Format("%3i",theapp->inspctn[1].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

//Decrease the Max/Min limit for the left cap height inspection.
void Lim::On1dn() 
{

	//if(theapp->inspctn[1].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}
//if(theapp->inspctn[1].type==2) {res=1;}else{res=10;}
	if(Max==true)
	{
		theapp->inspctn[1].lmax-=res;
		if(theapp->inspctn[1].lmax <=0)  //Height cannot be less then 1.
			theapp->inspctn[1].lmax=0;
		m_1.Format("%3i",theapp->inspctn[1].lmax);
	}
	else
	{
		theapp->inspctn[1].lmin-=res;
		if(theapp->inspctn[1].lmin <=0) theapp->inspctn[1].lmin=0;
		m_1.Format("%3i",theapp->inspctn[1].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the Max/Min limit for the right cap height inspection.
void Lim::On2up() 
{
	//if(theapp->inspctn[2].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[2].lmax+=res;
		if(theapp->inspctn[2].lmax >=AbsoluteMax) theapp->inspctn[2].lmax=AbsoluteMax;
		m_2.Format("%3i",theapp->inspctn[2].lmax);
	}
	else
	{
		theapp->inspctn[2].lmin+=res;
		if(theapp->inspctn[2].lmin >=AbsoluteMax) theapp->inspctn[2].lmin=AbsoluteMax;
		m_2.Format("%3i",theapp->inspctn[2].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the Max/Min limit for the right cap height inspection.
void Lim::On2dn() 
{

//	if(theapp->inspctn[2].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[2].lmax-=res;
		if(theapp->inspctn[2].lmax <=0) theapp->inspctn[2].lmax=0;
		m_2.Format("%3i",theapp->inspctn[2].lmax);
	}
	else
	{
		theapp->inspctn[2].lmin-=res;
		if(theapp->inspctn[2].lmin <=0) theapp->inspctn[2].lmin=0;
		m_2.Format("%3i",theapp->inspctn[2].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the Max/Min limit for the middle cap height inspection.
void Lim::On3up() 
{

	//if(theapp->inspctn[3].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[3].lmax+=res;
		if(theapp->inspctn[3].lmax >=AbsoluteMax) theapp->inspctn[3].lmax=AbsoluteMax;
		m_3.Format("%3i",theapp->inspctn[3].lmax);
	}
	else
	{
		theapp->inspctn[3].lmin+=res;
		if(theapp->inspctn[3].lmin >=AbsoluteMax) theapp->inspctn[3].lmin=AbsoluteMax;
		m_3.Format("%3i",theapp->inspctn[3].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

//Decrease the Max/Min limit for the middle cap height inspection.
void Lim::On3dn() 
{
	//if(theapp->inspctn[3].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[3].lmax-=res;
		if(theapp->inspctn[3].lmax <=0) theapp->inspctn[3].lmax=0;
		m_3.Format("%3i",theapp->inspctn[3].lmax);
	}
	else
	{
		theapp->inspctn[3].lmin-=res;
		if(theapp->inspctn[3].lmin <=0) theapp->inspctn[3].lmin=0;
		m_3.Format("%3i",theapp->inspctn[3].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//NOT USED
void Lim::On4up() 
{
	//if(theapp->inspctn[4].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

/*	if(Max==true)
	{
		theapp->inspctn[4].lmax+=res;
		//if(theapp->inspctn[4].lmax >=100) theapp->inspctn[4].lmax=100;
		m_4.Format("%3i",theapp->inspctn[4].lmax);
	}
	else
	{
		theapp->inspctn[4].lmin+=res;
		if(theapp->inspctn[4].lmin >=AbsoluteMax) theapp->inspctn[4].lmin=AbsoluteMax;
		m_4.Format("%3i",theapp->inspctn[4].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	*/
}

//NOT USED
void Lim::On4dn() 
{
	//if(theapp->inspctn[4].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}
/*
		if(Max==true)
	{
		theapp->inspctn[4].lmax-=res;
		if(theapp->inspctn[4].lmax <=0) theapp->inspctn[4].lmax=0;
		m_4.Format("%3i",theapp->inspctn[4].lmax);
	}
	else
	{
		theapp->inspctn[4].lmin-=res;
		if(theapp->inspctn[4].lmin <=0) theapp->inspctn[4].lmin=0;
		m_4.Format("%3i",theapp->inspctn[4].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	*/
}

//Increase the Max/Min limit for the tamper band edge inspection.
void Lim::On5up() 
{
	//if(theapp->inspctn[5].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[5].lmax+=res;
		if(theapp->inspctn[5].lmax >=AbsoluteMax) theapp->inspctn[5].lmax=AbsoluteMax;
		m_5.Format("%3i",theapp->inspctn[5].lmax);
	}
	else
	{
		theapp->inspctn[5].lmin+=res;
		if(theapp->inspctn[5].lmin >=AbsoluteMax) theapp->inspctn[5].lmin=AbsoluteMax;
		m_5.Format("%3i",theapp->inspctn[5].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the Max/Min limit for the tamper band edge inspection.
void Lim::On5dn() 
{
	//if(theapp->inspctn[5].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}


	if(Max==true)
	{
		theapp->inspctn[5].lmax-=res;
		if(theapp->inspctn[5].lmax <=0) theapp->inspctn[5].lmax=0;
		m_5.Format("%3i",theapp->inspctn[5].lmax);
	}
	else
	{
		theapp->inspctn[5].lmin-=res;
		//if(theapp->inspctn[5].lmin <=0) theapp->inspctn[5].lmin=0;
		m_5.Format("%3i",theapp->inspctn[5].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the Max/Min limit for the tamper band surface inspections.
void Lim::On6up() 
{
	//if(theapp->inspctn[6].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[6].lmax+=res;
		if(theapp->inspctn[6].lmax >=AbsoluteMax) theapp->inspctn[6].lmax=AbsoluteMax;
		m_6.Format("%3i",theapp->inspctn[6].lmax);
	}
	else
	{
		theapp->inspctn[6].lmin+=res;
		if(theapp->inspctn[6].lmin >=AbsoluteMax) theapp->inspctn[6].lmin=AbsoluteMax;
		m_6.Format("%3i",theapp->inspctn[6].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the Max/Min limit for the tamper band surface inspections.
void Lim::On6dn() 
{
	//if(theapp->inspctn[6].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[6].lmax-=res;
		if(theapp->inspctn[6].lmax <=0) theapp->inspctn[6].lmax=0;
		m_6.Format("%3i",theapp->inspctn[6].lmax);
	}
	else
	{
		theapp->inspctn[6].lmin-=res;
		//if(theapp->inspctn[6].lmin <=0) theapp->inspctn[6].lmin=0;
		m_6.Format("%3i",theapp->inspctn[6].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the Max/Min limit for the low fill inspections.
void Lim::On7up() 
{
	//if(theapp->inspctn[7].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[7].lmax+=res;
		if(theapp->inspctn[7].lmax >=255) theapp->inspctn[7].lmax=255;
		m_7.Format("%3i", theapp->inspctn[7].lmax);
	}
	else
	{
		theapp->inspctn[7].lmin+=res;
	//	if(theapp->inspctn[7].lmin >=100) theapp->inspctn[7].lmin=100;
		m_7.Format("%3i",theapp->inspctn[7].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the Max/Min limit for the low fill inspections.
void Lim::On7dn() 
{
	//if(theapp->inspctn[7].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[7].lmax-=res;
		if(theapp->inspctn[7].lmax <=0) theapp->inspctn[7].lmax=0;
		m_7.Format("%3i",theapp->inspctn[7].lmax);
	}
	else
	{
		theapp->inspctn[7].lmin-=res;
		if(theapp->inspctn[7].lmin <=0) theapp->inspctn[7].lmin=0;
		m_7.Format("%3i",theapp->inspctn[7].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the Max/Min limit for the cap color inspections.
void Lim::On8up() 
{
	//if(theapp->inspctn[8].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[8].lmax+=res;
		//if(theapp->inspctn[8].lmax >=5000) theapp->inspctn[8].lmax=5000;
		m_8.Format("%3i",theapp->inspctn[8].lmax);
	}
	else
	{
		theapp->inspctn[8].lmin+=res;
	//	if(theapp->inspctn[8].lmin >=0) theapp->inspctn[8].lmin=0;
		m_8.Format("%3i",theapp->inspctn[8].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the Max/Min limit for the cap color inspections.
void Lim::On8dn() 
{
	//if(theapp->inspctn[8].type==7){if(fine==false){res=0.1;}else{res=0.01;}}else{if(fine==false){res=10;}else{res=1;}}

	if(Max==true)
	{
		theapp->inspctn[8].lmax-=res;
		if(theapp->inspctn[8].lmax <=0) theapp->inspctn[8].lmax=0;
		m_8.Format("%3i",theapp->inspctn[8].lmax);
	}
	else
	{
		theapp->inspctn[8].lmin-=res;
		if(theapp->inspctn[8].lmin <=0) theapp->inspctn[8].lmin=0;
		m_8.Format("%3i",theapp->inspctn[8].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Selects adjustment of maximum limits for inspections.
void Lim::OnMax() 
{
	Max=true;					//Update flag.
	m_m1.Format("%s","Max");	//Update text strings.
	m_m2.Format("%s","Max");
	m_m3.Format("%s","Max");
	m_m4.Format("%s","Max");
	m_m5.Format("%s","Max");
	m_m6.Format("%s","Max");
	m_m7.Format("%s","Max");
	m_m8.Format("%s","Max");
	m_m9.Format("%s","Max");
	UpdateDisplay();			//Reloads the values for the max limits.

	
}

//Selects adjustment of minimum limits for inspections.
void Lim::OnMin() 
{
	Max=false;					//Update flag.
	m_m1.Format("%s","Min");	//Update text strings.
	m_m2.Format("%s","Min");
	m_m3.Format("%s","Min");
	m_m4.Format("%s","Min");
	m_m5.Format("%s","Min");
	m_m6.Format("%s","Min");
	m_m7.Format("%s","Min");
	m_m8.Format("%s","Min");
	m_m9.Format("%s","Min");
	UpdateDisplay();			//Reloads the values for the min limits.
	
}

void Lim::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum); 	//Saves the modifications made to the limits. 
		pframe->m_pview->UpdateDisplay();			//Updates the display of the left hand pan.
		pframe->m_pview->UpdateMinor();				//Updates the display of the left hand pan.
	}
	
	CDialog::OnTimer(nIDEvent);
}

//Toggle between enabling and disabling Left Height inspection.
void Lim::OnE1() 
{
	if( theapp->inspctn[1].enable==true )	//If inspection enabled
	{
		CheckDlgButton(IDC_E1,1);			//Show inspection enabled button as not pressed.
		theapp->inspctn[1].enable=false;	//disable inspection
		m_enable1.Format("%s","OFF"); 		//Update text on button to say "OFF"
	}
	else//If inspection disabled
	{
		CheckDlgButton(IDC_E1,0);			//Show inspection enabled button as pressed.
		theapp->inspctn[1].enable=true;		//enable inspection
		m_enable1.Format("%s","ON");		//Update text on button to say "ON"
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

//Toggle between enabling and disabling right Height inspection.
void Lim::OnE2() 
{
	if( theapp->inspctn[2].enable==true ){CheckDlgButton(IDC_E2,1); theapp->inspctn[2].enable=false; m_enable2.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E2,0); theapp->inspctn[2].enable=true; m_enable2.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling middle Height inspection.
void Lim::OnE3() 
{
	if( theapp->inspctn[3].enable==true ){CheckDlgButton(IDC_E3,1); theapp->inspctn[3].enable=false; m_enable3.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E3,0); theapp->inspctn[3].enable=true; m_enable3.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling cocked cap inspection.
void Lim::OnE4() 
{
	if( theapp->inspctn[4].enable==true ){CheckDlgButton(IDC_E4,1);theapp->inspctn[4].enable=false;theapp->cam2diff=0;theapp->cam3diff=0; theapp->checkRearHeight=false; m_enable4.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E4,0);theapp->inspctn[4].enable=true;  theapp->checkRearHeight=true; m_enable4.Format("%s","ON");}

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling tamper band edge inspection.
void Lim::OnE5() 
{
	if( theapp->inspctn[5].enable==true ){CheckDlgButton(IDC_E5,1); theapp->inspctn[5].enable=false; m_enable5.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E5,0); theapp->inspctn[5].enable=true; m_enable5.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling tamper band front surface inspection.
void Lim::OnE6() 
{
	if( theapp->inspctn[6].enable==true ){CheckDlgButton(IDC_E6,1); theapp->inspctn[6].enable=false; m_enable6.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E6,0); theapp->inspctn[6].enable=true; m_enable6.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling tamper band rear (camera 2) surface inspection.
void Lim::OnE6b() 
{
	if( theapp->surfLR==true ){CheckDlgButton(IDC_E6b,1); theapp->surfLR=false; m_enable6b.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E6b,0); theapp->surfLR=true; m_enable6b.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling tamper band rear (cameras 3) surface inspection.
void Lim::OnE6c() 
{
if( theapp->surfRR==true ){CheckDlgButton(IDC_E6c,1); theapp->surfRR=false; m_enable6c.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E6c,0); theapp->surfRR=true; m_enable6c.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Toggle between enabling and disabling low fill inspection.
void Lim::OnE7() 
{
if( theapp->inspctn[7].enable==true ){CheckDlgButton(IDC_E7,1); theapp->inspctn[7].enable=false; m_enable7.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E7,0); theapp->inspctn[7].enable=true; m_enable7.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

//Toggles between large and small resolution (step size) of limits adjustments.
void Lim::OnFine() 
{
	if(fine==true)	//If step size was small
	{
		m_fine.Format("%s","fast"); 	//Display "fast" as button text.
		fine=false;						//Change flag indicating step size to large.
		CheckDlgButton(IDC_FINE,1);		//Show button as pressed.

		if(res==1 || res==10){res=10;}else{res=0.1;}
		res=10;	//Change step size to large
	}
	else
	{
		m_fine.Format("%s","slow");		//Display "slow" as button text.
		fine=true;						//Change flag indicating step size to large.
		//NOT USED as is overridden by line below
		if(res==1 || res==10){res=1;}else{res=0.01;}
		res=1;	//Change step size to small
		CheckDlgButton(IDC_FINE,0);		//Show button as not pressed.
	}
	
	UpdateData(false);
	
}

//NOT USED
void Lim::OnE10() 
{
/*	if( theapp->inspctn[9].enable==true ){CheckDlgButton(IDC_E9,1); theapp->inspctn[9].enable=false; m_enable9.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E9,0); theapp->inspctn[9].enable=true; m_enable9.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
*/	
}

//Toggle between enabling and disabling cap color inspection.
void Lim::OnE8() 
{
	if( theapp->inspctn[8].enable==true )
	{
		CheckDlgButton(IDC_E8,1); 
		theapp->inspctn[8].enable=false; 
		m_enable8.Format("%s","OFF"); 
	}
	else
	{ 
		CheckDlgButton(IDC_E8,0); 
		theapp->inspctn[8].enable=true; 
		m_enable8.Format("%s","ON");
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);		
}




//NOT USED
void Lim::On10up() 
{
/*	if(Max==true)
	{
		theapp->inspctn[10].lmax+=res;
		//if(theapp->inspctn[8].lmax >=AbsoluteMax) theapp->inspctn[8].lmax=AbsoluteMax;
		m_10.Format("%3i",theapp->inspctn[10].lmax);
	}
	else
	{
		theapp->inspctn[10].lmin+=res;
		//if(theapp->inspctn[8].lmin >=AbsoluteMax) theapp->inspctn[8].lmin=AbsoluteMax;
		m_10.Format("%3i",theapp->inspctn[10].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	*/
}

//NOT USED
void Lim::On10dn() 
{
/*	if(Max==true)
	{
		theapp->inspctn[10].lmax-=res;
		//if(theapp->inspctn[8].lmax >=AbsoluteMax) theapp->inspctn[8].lmax=AbsoluteMax;
		if(theapp->inspctn[10].lmax <=0) theapp->inspctn[10].lmax=0;
		m_10.Format("%3i",theapp->inspctn[10].lmax);
	}
	else
	{
		theapp->inspctn[10].lmin-=res;
		//if(theapp->inspctn[8].lmin >=AbsoluteMax) theapp->inspctn[8].lmin=AbsoluteMax;
		if(theapp->inspctn[10].lmin <=0) theapp->inspctn[10].lmin=0;
		m_10.Format("%3i",theapp->inspctn[10].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	*/
}

//User Limits, NOT USED
void Lim::OnE9() 
{
	if( theapp->inspctn[9].enable==true ){CheckDlgButton(IDC_E9,1); theapp->inspctn[9].enable=false;theapp->inspctn[10].enable=false; m_enable9.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E9,0); theapp->inspctn[9].enable=true;theapp->inspctn[10].enable=true; m_enable9.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);	
	
}

//User Limits, NOT USED
void Lim::OnE9b() 
{
	if( theapp->userLR==true ){CheckDlgButton(IDC_E9b,1); theapp->userLR=false; m_enable9b.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E9b,0); theapp->userLR=true; m_enable9b.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//User Limits, NOT USED
void Lim::OnE9c() 
{
	if( theapp->userRR==true ){CheckDlgButton(IDC_E9c,1); theapp->userRR=false; m_enable9c.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E9c,0); theapp->userRR=true; m_enable9c.Format("%s","ON");}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//User Limits, NOT USED
void Lim::On9up() 
{
	if(Max==true)
	{
		theapp->inspctn[10].lmax=theapp->inspctn[9].lmax+=res;

		//if(theapp->inspctn[9].lmax >=1000) theapp->inspctn[9].lmax=1000;
		m_9.Format("%3i", theapp->inspctn[9].lmax);
	}
	else
	{
		theapp->inspctn[10].lmin=theapp->inspctn[9].lmin+=res;
		if(theapp->inspctn[9].lmin <=0) theapp->inspctn[9].lmin=0;
		m_9.Format("%3i",theapp->inspctn[9].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Lim::On9dn() 
{
	if(Max==true)
	{
		theapp->inspctn[10].lmax=theapp->inspctn[9].lmax-=res;
		if(theapp->inspctn[9].lmax <=0) theapp->inspctn[9].lmax=0;
		m_9.Format("%3i", theapp->inspctn[9].lmax);
	}
	else
	{
		theapp->inspctn[10].lmin=theapp->inspctn[9].lmin-=res;
		if(theapp->inspctn[9].lmax <=0) theapp->inspctn[9].lmax=0;
		m_9.Format("%3i",theapp->inspctn[9].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
	
}

//Increase the Max/Min limit for the cocked cap inspection.
void Lim::OnhtUp() 
{
/*	if(theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff >0  && theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff<30)
	{
	theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff+=1;
	}
		SetTimer(1,3000,NULL);
		UpdateData(false);
		UpdateDisplay();
*/
	if(Max==true)
	{
		theapp->inspctn[4].lmax+=res;
		if(theapp->inspctn[4].lmax >=100) theapp->inspctn[4].lmax=100;
		m_4.Format("%3i",theapp->inspctn[4].lmax);
	}
	else
	{
		theapp->inspctn[4].lmin+=res;
		if(theapp->inspctn[4].lmin >=0) theapp->inspctn[4].lmin=0;
		m_4.Format("%3i",theapp->inspctn[4].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	UpdateDisplay();
	

}

//Decrease the Max/Min limit for the cocked cap inspection.
void Lim::OnhtDwn() 
{

/*	if(theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff >0  && theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff<30)
	{
	theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff-=1;
	}
*/
	if(Max==true)
	{
		theapp->inspctn[4].lmax-=res;
		if(theapp->inspctn[4].lmax <=0) theapp->inspctn[4].lmax=0;
		m_4.Format("%3i",theapp->inspctn[4].lmax);
	}
	else
	{
		theapp->inspctn[4].lmin-=res;
		if(theapp->inspctn[4].lmin <=0) theapp->inspctn[4].lmin=0;
		m_4.Format("%3i",theapp->inspctn[4].lmin);
	}
	SetTimer(1,3000,NULL);
	UpdateData(false);
	UpdateDisplay();

}

//Toggle between enabling and disabling tamper band rear (cameras 2, 3) surface inspection.
//Seems to be a duplicate of the other rear camera tamper band toggle.
//Possibly used as a work around to the bug where the group and individual
//rear camera tamper band inspection toggles desynchronize.
void Lim::OnCheck1() 
{
	if(	theapp->checkRearHeight == true)
	{
		theapp->checkRearHeight=false;
	//	CheckDlgButton(IDC_CHECK1,0);

	}

	else if(theapp->checkRearHeight==false)
	{
	
	theapp->checkRearHeight=true;
//	CheckDlgButton(IDC_CHECK1,1);
	
	}

		UpdateData(false);
		UpdateDisplay();	
	
}

//Toggles between enabling and disabling another kind of tamper band inspection.
//This inspection is performed in all 3 cameras.
void Lim::OnenableTB() 
{
	if( theapp->jobinfo[pframe->CurrentJobNum].do_sobel==true )
	{
		CheckDlgButton(IDC_enableTB,1); theapp->jobinfo[pframe->CurrentJobNum].do_sobel=false; m_enabletb.Format("%s","OFF");
		//theapp->jobinfo[pframe->CurrentJobNum].etb_cam1=false;
		//theapp->jobinfo[pframe->CurrentJobNum].etb_cam2=false;theapp->jobinfo[pframe->CurrentJobNum].etb_cam3=false;
	}

	else{ CheckDlgButton(IDC_enableTB,0); theapp->jobinfo[pframe->CurrentJobNum].do_sobel=true;m_enabletb.Format("%s","ON"); }
	SetTimer(1,3000,NULL);
	UpdateData(false);

//if( theapp->inspctn[1].enable==true ){CheckDlgButton(IDC_E1,1); theapp->inspctn[1].enable=false; m_enable1.Format("%s","OFF"); }else{ CheckDlgButton(IDC_E1,0); theapp->inspctn[1].enable=true; m_enable1.Format("%s","ON");}

}

//Pops up a dialog which will all the user to adjust limits for another type of tamper band inspection.
void Lim::OnAdjustingLimits() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].do_sobel==true )
	{
		EnhancedTB enhancedtb;
		enhancedtb.DoModal();
	}
	else
	{
	//	AfxMessageBox("Please Turn ON Enhanced TB inspection from Limits Tab",MB_OK);
		pframe->prompt_code=7;
		Prompt3 promtp3;
		promtp3.DoModal();
	}
	
}

//Automatically determine the upper and lower limits for the height inspections.
//Seems to set the values to +- 10 of the average values.
void Lim::OnAuto() 
{
	if(pframe->m_pxaxis->Result1 >0 && pframe->m_pxaxis->Result2 >0 && pframe->m_pxaxis->Result3>0 )
	{
		theapp->inspctn[1].lmax=pframe->m_pxaxis->Result1+10;
		theapp->inspctn[2].lmax=pframe->m_pxaxis->Result2+10;
		theapp->inspctn[1].lmin=pframe->m_pxaxis->Result1-10;
		theapp->inspctn[2].lmin=pframe->m_pxaxis->Result2-10;
		
		theapp->inspctn[3].lmax=pframe->m_pxaxis->Result3+10;
		theapp->inspctn[3].lmin=pframe->m_pxaxis->Result3-10;	
	}
}

//NOT USED
void Lim::OnEnableRearSurface() 
{
	
	
}

//Toggle between enabling and disabling tamper band rear (cameras 2, 3) surface inspection.
void Lim::OnRadio5() 
{
	if( theapp->check_Rband==true ){CheckDlgButton(IDC_RADIO5,1); theapp->check_Rband=false;m_enableRear.Format("%s","OFF");  }else{ CheckDlgButton(IDC_RADIO5,0); theapp->check_Rband=true;m_enableRear.Format("%s","ON"); }
	
	//Toggles the other two controls which individually enable camera2 or camera3 tamper band surface inspections.
	//Possible bug, where the "Radio5" control's state does not correspond to "E6b" and "E6c" controls' state.
	OnE6b();

	OnE6c();

	//UpdateDisplay();
	UpdateData(false);

	
}
