// Setup.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Setup.h"
#include "mainfrm.h"
#include "serial.h"
#include "xaxisview.h"
#include "capview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Setup dialog


Setup::Setup(CWnd* pParent /*=NULL*/)
//	: CDialog(Setup::IDD, pParent)
{
	//{{AFX_DATA_INIT(Setup)
	m_zvalue = _T("");
	m_fvalue = _T("");
	//}}AFX_DATA_INIT
}


void Setup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Setup)
//	DDX_Text(pDX, IDC_ZVALUE, m_zvalue);
//	DDX_Text(pDX, IDC_FVALUE, m_fvalue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Setup, CDialog)
	//{{AFX_MSG_MAP(Setup)
//	ON_BN_CLICKED(IDC_ZIN, OnZin)
//	ON_BN_CLICKED(IDC_ZOUT, OnZout)
	ON_WM_TIMER()
//	ON_BN_CLICKED(IDC_AOP, OnAop)
//	ON_BN_CLICKED(IDC_ACL, OnAcl)
//	ON_BN_CLICKED(IDC_FIN, OnFin)
//	ON_BN_CLICKED(IDC_FFAR, OnFfar)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Setup message handlers

BOOL Setup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psetup=this;
	theapp=(CCapApp*)AfxGetApp();
	Value=0;
	Function=0;
	pframe->OnLine=false;

	//ML- THIS IS COMMENTED IN ORIGINAL PROGRAM SO I WILL NOT ADD ANYTHING HERE
//	pframe->m_pserial->SendMessage(WM_QUE,0,290);//red light
	
	pframe->m_pview->SetTimer(2,500,NULL);

	UpdateData(false);
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Setup::OnZin() 
{
	Function=1;
	SetTimer(1,100,NULL);

	CalculateChars();
	
}

void Setup::CalculateChars()
{
	
	
	if(Function<10) Function=Function *10;
	CntrBits=0;
	if(theapp->NoSerialComm==false) 
	{
		pframe->m_pserial->ControlChars=Value+Function+CntrBits;
	}
	
	//UpdateData(false);
}



void Setup::OnZout() 
{
	Function=2;
	SetTimer(1,100,NULL);
	
	CalculateChars();
	
}


void Setup::OnOK() 
{
	// TODO: Add extra validation here
	//pframe->m_pxaxis->InvalidateRect(NULL,false);
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	CDialog::OnOK();
}


void Setup::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Function=0;
		if(Function<10) Function=Function *10;
		CntrBits=0;
		if(theapp->NoSerialComm==false) 
		{
						
			pframe->m_pserial->ControlChars=Value+Function+CntrBits;
		}
		SetTimer(2,700,NULL);
		SetTimer(3,250,NULL);
		
		
	}

	if(nIDEvent==2)
	{
		KillTimer(2);
		pframe->SendMessage(WM_TRIGGER,NULL,NULL);
	}

	if(nIDEvent==3)
	{
		KillTimer(3);
	//	m_zvalue.Format("%i",pframe->m_pserial->ZoomPos);
		//m_fvalue.Format("%i",pframe->m_pserial->FocusPos);
		
		UpdateData(false);
	}
	
	CDialog::OnTimer(nIDEvent);
}

void Setup::OnAop() 
{
	Function=3;
	SetTimer(1,200,NULL);

	CalculateChars();
	
}

void Setup::OnAcl() 
{
	Function=4;
	SetTimer(1,200,NULL);

	CalculateChars();
	
	
}

void Setup::OnFin() 
{
	Function=5;
	SetTimer(1,200,NULL);

	CalculateChars();
	
}

void Setup::OnFfar() 
{
	Function=6;
	SetTimer(1,200,NULL);

	CalculateChars();
	
}
/////////////////////////////////////////////////////////////////////////////
// SDelay dialog


SDelay::SDelay(CWnd* pParent /*=NULL*/)
	: CDialog(SDelay::IDD, pParent)
{
	//{{AFX_DATA_INIT(SDelay)
	m_sdelay = _T("");
	m_editcw = _T("");
	//}}AFX_DATA_INIT
}


void SDelay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SDelay)
	DDX_Text(pDX, IDC_SDELAY, m_sdelay);
	DDX_Text(pDX, IDC_EDITCW, m_editcw);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SDelay, CDialog)
	//{{AFX_MSG_MAP(SDelay)
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MORE3, OnMore3)
	ON_BN_CLICKED(IDC_LESS3, OnLess3)
	ON_BN_CLICKED(IDC_CWMORE, OnCwmore)
	ON_BN_CLICKED(IDC_CWLESS, OnCwless)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SDelay message handlers

//Increase delay time between the bottle triggering the photoeye and image acquisition.
void SDelay::OnMore() 
{
	//theapp->SavedDelay+=1;

	theapp->jobinfo[pframe->CurrentJobNum].snapDelay+=1;	//Update job info.
	m_sdelay.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].snapDelay);	//Update gui variable for snap delay.

	UpdatePLC();	//Send new snap delay to PLC.
	
	UpdateData(false);	//Update gui from variables.
	
}

//Decrease delay time between the bottle triggering the photo eye and image acquisition.
void SDelay::OnLess() 
{
	theapp->jobinfo[pframe->CurrentJobNum].snapDelay-=1;
	m_sdelay.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].snapDelay);

	UpdatePLC();
	UpdateData(false);
	
}

//Send new snap delay to PLC.
void SDelay::UpdatePLC()
{
	//Set PLC frame to update snap delay.
	int Value=theapp->jobinfo[pframe->CurrentJobNum].snapDelay;
	int Function=0;
	int CntrBits=0;
	int ControlChars;
		
//	Value=Value*1000;
	Function=7;
	Function=Function *10;
	CntrBits=0;

	ControlChars=Value+Function+CntrBits;

	if(theapp->NoSerialComm==false)  //If connected to PLC
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);	//Send command to PLC
	}
}

BOOL SDelay::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();			//Get the app object
	pframe->m_psdelay=this;							//Set main frames reference for this dlg 
	theapp=(CCapApp*)AfxGetApp();					//Get the main from object

	//pframe->SetTimer(8,100,NULL);//com
	m_editcw.Format("%3i",theapp->SavedDeadBand);		//Set gui cap with variable.
	m_sdelay.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].snapDelay);	//Set gui delay variable.
	UpdateData(false);	//Update gui.
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Does not appear to be used.
void SDelay::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0;
		Function=0;
		CntrBits=0;

		if(theapp->NoSerialComm==false) 
		{
			pframe->m_pserial->ControlChars=Value+Function+CntrBits;
		}
		
		
	}
	
	CDialog::OnTimer(nIDEvent);
}

void SDelay::OnOK() 
{
	// TODO: Add extra validation here
	pframe->SaveJob(pframe->CurrentJobNum);	//Save modified job info.
	CDialog::OnOK();
}

void SDelay::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

//Increase delay time between the bottle triggering the photo eye and image acquisition, Large steps.
void SDelay::OnMore3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].snapDelay+=20;
	m_sdelay.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].snapDelay);

	UpdatePLC();
	
	UpdateData(false);
	
}

//Decrease delay time between the bottle triggering the photo eye and image acquisition, Large steps.
void SDelay::OnLess3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].snapDelay-=20;
	m_sdelay.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].snapDelay);

	UpdatePLC();
	
	UpdateData(false);
	
}


//The deadband delay, "cap width", prevents one bottle from triggering the photoeye multiple times. Similar to debouncing.

//Increase the deadband delay.
void SDelay::OnCwmore() 
{
	theapp->SavedDeadBand+=2;						//Increase the value of the deadband variable stored in the app object.
	m_editcw.Format("%3i",theapp->SavedDeadBand);	//Update gui variable for deadband delay.

	if(theapp->NoSerialComm==false)					//If PLC is connected
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->SavedDeadBand,120);
		pframe->m_pserial->SendChar(theapp->SavedDeadBand,120);		//Send command to PLC to update the deadband.
	}
	
	UpdateData(false);
	
}

//Increase the deadband delay.
void SDelay::OnCwless() 
{
	
	theapp->SavedDeadBand-=2;
	m_editcw.Format("%3i",theapp->SavedDeadBand);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->SavedDeadBand,120);
		pframe->m_pserial->SendChar(theapp->SavedDeadBand,120);
	}
	
	UpdateData(false);
	
}
