#if !defined(AFX_VIEW_H__A0671511_92DF_4B58_8C09_C55B19855EDB__INCLUDED_)
#define AFX_VIEW_H__A0671511_92DF_4B58_8C09_C55B19855EDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// View.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// View dialog

class View : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;
public:

	CCapApp *theapp;
	CMainFrame* pframe;
	int res;
	bool toggle;
	View(CWnd* pParent = NULL);   // standard constructor
//BOOL Create(DWORD dwstyle, const RECT& rect, CWnd* pParentWnd, UINT nID);
// Dialog Data
	//{{AFX_DATA(View)
	enum { IDD = IDD_VIEW };
	CStatic	m_a9;		//Reference static label control which displays last eject filler number
	CStatic	m_a8;		//Reference static label control which displays current filler number
	CStatic	m_a6;		//Reference static label control which displays last eject capper number
	CStatic	m_a5;		//Reference static label control which displays current capper number
	CStatic	m_a4;		//Reference static label control which displays capper number label
	CButton	m_a3;		//Reference group box control which displays capper current and last reject label
	CButton	m_a2;		//Reference group box control which displays filler current and last reject label
	CStatic	m_a1;		//Reference static label control which displays filler number label
	CButton	m_tbsname;	//NOT USED, Reference to group box control which displays the tamper band surface label.
	CString	m_id1;		//Stores number of bottles scanned per minute.
	CString	m_id2;		//rejected
	CString	m_id3;		//Number of left height defects
	CString	m_id4;		//Number of right height defects
	CString	m_id5;		//Number of middle height defects
	CString	m_id6;		//NOT USED
	CString	m_id7;		//Number of tamper band edge defects
	CString	m_id8;		//Number of tamper band surface defects
	CString	m_id9;		//Number of low fill defects
	CString	m_id10;		//Number of cap color defects
	CString	m_id11;		//Number of missing caps
	CString	m_id12;		//Number of cocked caps
	CString	m_id13;		//Number of loose caps
	CString	m_id14;		//Number of user defects, NOT USED
	CString	m_t3;		//Stores average for left height
	CString	m_t4;		//Stores average for right height
	CString	m_t5;		//Stores average for middle height
	CString	m_t6;		//NOT USED
	CString	m_t7;		//Stores average for tamper band edge 
	CString	m_t8;		//Stores average for tamper band surface 
	CString	m_t9;		//Stores average for low fill 
	CString	m_t10;		//Stores average for color
	CString	m_sd1;		//Stores standard deviation for left height
	CString	m_sd2;		//Stores standard deviation for right height
	CString	m_sd3;		//Stores standard deviation for middle height
	CString	m_sd4;		//NOT USED
	CString	m_sd5;		//Stores standard deviation tamper band edge 
	CString	m_sd6;		//Stores standard deviation tamper band surface 
	CString	m_sd7;		//Stores standard deviation low fill 
	CString	m_sd8;		//Stores standard deviation color
	CString	m_sport;			//label switches between "Height Middle" and "Sports Band".
	CString	m_cappernumber;		//String for storing current capper number
	CString	m_fillernumber;		//String for storing current filler number
	CString	m_cappernumber2;	//String for storing last rejected capper number
	CString	m_fillernumber2;	//String for storing last rejected filler number
	CString	m_capwidth;			//String for holding the cap width.
	CString	m_imgRead1;			//Used for debugging camera dropped frames
	CString	m_imgRead2;			//Used for debugging camera dropped frames
	CString	m_imgRead3;			//Used for debugging camera dropped frames
	CString	m_finished1;		//Used for debugging camera dropped frames
	CString	m_finished2;		//Used for debugging camera dropped frames
	CString	m_finished3;		//Used for debugging camera dropped frames
	CString	m_FoilSensor;
	//}}AFX_DATA
	
	CTabCtrl m_tabmenu2;
	void UpdateDisplay();
	//void OnSelchangeTabmenu2(NMHDR* pNMHDR, LRESULT* pResult) ;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(View)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(View)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEW_H__A0671511_92DF_4B58_8C09_C55B19855EDB__INCLUDED_)
