//{{AFX_INCLUDES()
//}}AFX_INCLUDES
#if !defined(AFX_INSP_H__07A48C30_EFB2_422F_8E42_F0DA1F59B386__INCLUDED_)
#define AFX_INSP_H__07A48C30_EFB2_422F_8E42_F0DA1F59B386__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Insp.h : header file
//
#include "VisCore.h"
#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// Insp dialog

typedef struct  {
	int x; 
	int y; } SPoint;


class Insp : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:

	BYTE* im_insp1;
	BYTE* im_insp2;
	BYTE* im_insp3;

	BYTE* im_INSP1;
	BYTE* im_INSP2;
	BYTE* im_INSP3;

	BYTE* im_data1;			//Holds grayscale image data for camera 1.
	BYTE* im_data2;			//Holds grayscale image data for camera 2.
	BYTE* im_data3;			//Holds grayscale image data for camera 3.

	BYTE*  im_ptr_C1;
	BYTE*  im_ptr_C2;
	BYTE*  im_ptr_C3;

	BYTE* im_ptr_C1_1;
	BYTE* im_ptr_C2_2;
	BYTE* im_ptr_C3_3;


	bool manual;		//Not used
	bool GotLive;
	int res;			//Resolution used for changing granularity of controls in window.
	bool once;
	Insp(CWnd* pParent = NULL);   // standard constructor
	void OnInspect();
	void InitialXLocations();
	
// Dialog Data
	//{{AFX_DATA(Insp)
	enum { IDD = IDD_INSP };
	CButton	m_check1;
	CButton	m_lipsize;
	CButton	m_lipareataller;
	CButton	m_lipareasmaller;
	CButton	m_sqoffset;
	CButton	m_lipdn2;
	CButton	m_lipup2;
	CButton	m_liparea;
	CButton	m_lipup;
	CButton	m_lipright;
	CButton	m_lipleft;
	CButton	m_lipdn;
	CButton	m_tbupsp;
	CButton	m_tbsmallersp;
	CButton	m_tb2upsp;
	CButton	m_tb2mupsp;
	CButton	m_tb2mdnsp;
	CButton	m_tb2dnsp;
	CButton	m_fdnsp;
	CButton	m_fleftsp;
	CButton	m_lrightsp;
	CButton	m_lleftsp;
	CButton	m_fupsp;
	CButton	m_frightsp;
	CButton	m_nupsp;
	CButton	m_ndnsp;
	CButton	m_mupsp;
	CButton	m_mdnsp;
	CButton	m_tbdnsp;
	CButton	m_tblargersp;
	CButton	m_tbleftsp;
	CButton	m_tbrightsp;
	CButton	m_udnsp;
	CButton	m_ulargesp;
	CButton	m_uleftsp;
	CButton	m_urightsp;
	CButton	m_usmallsp;
	CButton	m_uupsp;

	//pullup inspection variables.
	CString	m_capmiddle;
	CString	m_capneck;
	CString	m_topleft;

	//front camera edge tamper band  variables
	CString	m_tbl;	//horizontal position
	CString	m_tbr;
	CString	m_fillh;	//Low fill search area horizontal position, does not appear to be used.
	CString	m_fillv;	//Low fill search area vertical position
	CString	m_edittb2s;
	CString	m_edittb2v;
	CString	m_edittb2h;

	CString	m_edituh;		//Color search box horizontal position, display variable.
	CString	m_editus;		//Color search box strength, does not appear to be used
	CString	m_edituv;		//Color search box vertical position, display variable.
	CString	m_editsize;		//Color search box size, display variable.
	CString	m_edittb2;
	CString	m_editb2v;
	CString	m_edittbseam;	//vertical position
	CString	m_tb2vert;
	//}}AFX_DATA

	CMainFrame* pframe;
	CCapApp* theapp;
	CBitmap bitmapl;
	CBitmap bitmapr;

	BYTE *imgBuf;

	CVisRGBAByteImage imageI;
	CVisRGBAByteImage imageI2;
	CVisRGBAByteImage imageI3;

	CVisRGBAByteImage image_C1;//
	CVisRGBAByteImage image_C2;
	CVisRGBAByteImage image_C3;
	CVisByteImage m_image;


	const CVisImageBase& Image() const
		{ return m_image; }
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Insp)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Insp)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnMup();
	afx_msg void OnMdn();
	virtual void OnOK();
	afx_msg void OnNup();
	afx_msg void OnNdn();
	afx_msg void OnLleft();
	afx_msg void OnLright();
	afx_msg void OnPaint();
	afx_msg void OnFine();
	afx_msg void OnTbup();
	afx_msg void OnTbdn();
	afx_msg void OnTbleft();
	afx_msg void OnTbright();
	afx_msg void OnFup();
	afx_msg void OnFdn();
	afx_msg void OnFleft();
	afx_msg void OnFright();
	afx_msg void OnTrigger();
	afx_msg void OnTrigger2();
	afx_msg void OnTrigger3();
	afx_msg void OnInspectPushed();
//	virtual void OnCancel();
	afx_msg void OnTech();
	afx_msg void OnTb2up();
	afx_msg void OnTb2dn();
	afx_msg void OnTb2left();
	afx_msg void OnTb2right();
	afx_msg void OnTb2mup();
	afx_msg void OnTb2mdn();
	afx_msg void OnUup();
	afx_msg void OnUdn();
	afx_msg void OnUleft();
	afx_msg void OnUright();
	afx_msg void OnUsup();
	afx_msg void OnUsdn();
	afx_msg void OnUsmall();
	afx_msg void OnUlarge();
	afx_msg void OnTbsmaller();
	afx_msg void OnTblarger();
	afx_msg void OnRadio2();
	afx_msg void OnRadio5();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	afx_msg void OnRadio6();
	afx_msg void OnRadio1x();
	afx_msg void OnRadio2x();
	afx_msg void OnRadio3x();
	afx_msg void OnRadio4x();
	afx_msg void OnRadio5x();
	afx_msg void OnManual();
	afx_msg void OnLipup();
	afx_msg void OnLipdn();
	afx_msg void OnLipleft();
	afx_msg void OnLipright();
	afx_msg void OnLipup2();
	afx_msg void OnLipdn2();
	afx_msg void OnLipareataller();
	afx_msg void OnLipareasmaller();
	afx_msg void OnRadio6x();
	afx_msg void OnSunny();
	afx_msg void OnSetupcolor();
	afx_msg void OnXtralight();
	afx_msg void OnUseleft();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSP_H__07A48C30_EFB2_422F_8E42_F0DA1F59B386__INCLUDED_)
