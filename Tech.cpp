// Tech.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Tech.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "insp.h"
#include "serial.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Tech dialog


Tech::Tech(CWnd* pParent /*=NULL*/)
	: CDialog(Tech::IDD, pParent)
{
	//{{AFX_DATA_INIT(Tech)
	m_fsizeh = _T("");
	m_fsizew = _T("");
	m_edittb = _T("");
	m_editlswin = _T("");
	m_editrswin = _T("");
	m_editwidth = _T("");
	m_editol = _T("");
	m_edityoff = _T("");
	m_target_ht_bar = _T("");
	m_target_height_bar2 = _T("");
	m_target_ht_bar3 = _T("");
	m_diff_var = _T("");
	//}}AFX_DATA_INIT
}


void Tech::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Tech)
	DDX_Control(pDX, IDC_RTB2RIGHT, m_rtb2right);
	DDX_Control(pDX, IDC_RTB2LEFT, m_rtb2left);
	DDX_Control(pDX, IDC_LTB2RIGHT, m_ltb2right);
	DDX_Control(pDX, IDC_LTB2LEFT, m_ltb2left);
	DDX_Text(pDX, IDC_EDITFILLSIZEH, m_fsizeh);
	DDX_Text(pDX, IDC_EDITFILLSIZEW, m_fsizew);
	DDX_Text(pDX, IDC_EDITTB, m_edittb);
	DDX_Text(pDX, IDC_EDITLSWIN, m_editlswin);
	DDX_Text(pDX, IDC_EDITRSWIN, m_editrswin);
	DDX_Text(pDX, IDC_EDITWIDTH, m_editwidth);
	DDX_Text(pDX, IDC_EDITOL, m_editol);
	DDX_Text(pDX, IDC_EDITYOFF, m_edityoff);
	DDX_Text(pDX, IDC_target_ht_bar, m_target_ht_bar);
	DDX_Text(pDX, IDC_target_ht_bar2, m_target_height_bar2);
	DDX_Text(pDX, IDC_target_ht_bar3, m_target_ht_bar3);
	DDX_Text(pDX, IDC_diff, m_diff_var);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Tech, CDialog)
	//{{AFX_MSG_MAP(Tech)
	ON_BN_CLICKED(IDC_WLARGER, OnWlarger)
	ON_BN_CLICKED(IDC_WSMALLER, OnWsmaller)
	ON_BN_CLICKED(IDC_HLARGER, OnHlarger)
	ON_BN_CLICKED(IDC_HSMALLER, OnHsmaller)
	ON_BN_CLICKED(IDC_TBLARGER, OnTblarger)
	ON_BN_CLICKED(IDC_TBSMALLER, OnTbsmaller)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_LTB2LEFT, OnLtb2left)
	ON_BN_CLICKED(IDC_LTB2RIGHT, OnLtb2right)
	ON_BN_CLICKED(IDC_RTB2RIGHT, OnRtb2right)
	ON_BN_CLICKED(IDC_RTB2LEFT, OnRtb2left)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_BN_CLICKED(IDC_CHECK2, OnCheck2)
	ON_BN_CLICKED(IDC_CHECK3, OnCheck3)
	ON_BN_CLICKED(IDC_WIDTHMORE, OnWidthmore)
	ON_BN_CLICKED(IDC_WIDTHLESS, OnWidthless)
	ON_BN_CLICKED(IDC_CHECK4, OnCheck4)
	ON_BN_CLICKED(IDC_ENABLEWIDTH, OnEnablewidth)
	ON_BN_CLICKED(IDC_CHECK5, OnCheck5)
	ON_BN_CLICKED(IDC_OLUP, OnOlup)
	ON_BN_CLICKED(IDC_OLDN, OnOldn)
	ON_BN_CLICKED(IDC_YOFFUP, OnYoffup)
	ON_BN_CLICKED(IDC_YOFFDN, OnYoffdn)
	ON_BN_CLICKED(IDC_BUTTONUP, OnButtonup)
	ON_BN_CLICKED(IDC_BUTTONDN, OnButtondn)
	ON_BN_CLICKED(IDC_BUTTONUP2, OnButtonup2)
	ON_BN_CLICKED(IDC_BUTTONUP3, OnButtonDown2)
	ON_BN_CLICKED(IDC_BUTTONUP4, OnButtonup3)
	ON_BN_CLICKED(IDC_BUTTONUP5, OnButtonDown3)
	ON_BN_CLICKED(IDC_CHECK6, OnCheck6)
	ON_BN_CLICKED(IDC_BUTTON10, OnIncrDiff)
	ON_BN_CLICKED(IDC_BUTTON12, OnDcrDiff)
	ON_BN_CLICKED(IDC_CHECK7, OnCheck7)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Tech message handlers

BOOL Tech::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_ptech=this;
	theapp=(CCapApp*)AfxGetApp();

	//Load GUI from saved variables.
	m_fsizew.Format("%3i",theapp->SavedFillW);
	m_fsizeh.Format("%3i",theapp->SavedFillH);

	m_edittb.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandDy);

	m_editwidth.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].capWidthLimit);

	m_editrswin.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].rSWin);
	m_editlswin.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].lSWin);

	m_editol.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].fillArea);
	m_edityoff.Format("%i",theapp->xtraYOffset);
	
	m_target_ht_bar.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar);
	m_target_height_bar2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2);
	m_target_ht_bar3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3);

	switch(theapp->jobinfo[pframe->CurrentJobNum].tBandType)
	{
	case 1:
		CheckDlgButton(IDC_RADIO1,1);
		CheckDlgButton(IDC_RADIO2,0);//CheckDlgButton(IDC_RADIO5,0);
		break;

	case 2:
		CheckDlgButton(IDC_RADIO1,0);
		CheckDlgButton(IDC_RADIO2,1);
//		CheckDlgButton(IDC_RADIO5,0);
		if(theapp->jobinfo[pframe->CurrentJobNum].dark==1)
		{
			CheckDlgButton(IDC_RADIO3,1);
			//CheckDlgButton(IDC_RADIO4,0);
		}
		else
		{
			CheckDlgButton(IDC_RADIO3,0);
			//CheckDlgButton(IDC_RADIO4,1);
		}
		break;
	
	case 3:
		CheckDlgButton(IDC_RADIO1,0);CheckDlgButton(IDC_RADIO2,0);CheckDlgButton(IDC_RADIO3,0);CheckDlgButton(IDC_RADIO4,0);

	}

	if(theapp->enableWidth==true)
	{
	
		CheckDlgButton(IDC_ENABLEWIDTH,1);
	}
	else
	{
		
		CheckDlgButton(IDC_ENABLEWIDTH,0);
	}

	//if(theapp->SavedKillSave==true){ m_enablepics.Format("%s","Pics are not saved");}else{m_enablepics.Format("%s","Pics are saved");}
	
	//if(theapp->ForceLightWJob==true){m_forcelights.Format("%s","Saved W/Job");}else{m_forcelights.Format("%s","Not Saved W/Job");}
	
	switch(theapp->jobinfo[pframe->CurrentJobNum].lip)
	{
	case 0:
		CheckDlgButton(IDC_CHECK1,1);
		CheckDlgButton(IDC_CHECK2,0);
		
	break;
	case 1:
		CheckDlgButton(IDC_CHECK1,0);
		CheckDlgButton(IDC_CHECK2,1);
	}

	switch(theapp->jobinfo[pframe->CurrentJobNum].sport)
	{
	case 0:
		CheckDlgButton(IDC_CHECK3,1);
		CheckDlgButton(IDC_CHECK4,0);
		CheckDlgButton(IDC_CHECK5,0);
		
	break;
	case 1:
		CheckDlgButton(IDC_CHECK3,0);
		CheckDlgButton(IDC_CHECK4,1);
		CheckDlgButton(IDC_CHECK5,0);

	break;
	case 2:
		CheckDlgButton(IDC_CHECK3,0);
		CheckDlgButton(IDC_CHECK4,0);
		CheckDlgButton(IDC_CHECK5,1);
	}

	if(theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke==true)
	 {
		
	//	theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke=false;
		CheckDlgButton(IDC_CHECK6,1);
	 }

   else
	  {
	  // theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke=true;
	   CheckDlgButton(IDC_CHECK6,0);
	   

	   }
	//Not used.
   m_diff_var.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap);
	
	UpdateDisplay();
	UpdateData(false);
	
	//Not used
	Value=0;
	Function=0;
	CntrBits=0;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//Increase width of low fill search box.
void Tech::OnWlarger() 
{
	theapp->SavedFillW+=1;	  //Increase app variable which holds low fill box width.
	m_fsizew.Format("%3i",theapp->SavedFillW);	//Update gui variable which holds low fill box width.
	UpdateData(false);	//Update gui from variables.
	InvalidateRect(NULL,false);	 //Probably not needed. VTK
	pframe->m_pinsp->InvalidateRect(NULL,false);	//Repaint the inspection gui so that it repaints the low fill box.
}

//Decrease width of low fill search box.
void Tech::OnWsmaller() 
{
	theapp->SavedFillW-=1;
	m_fsizew.Format("%3i",theapp->SavedFillW);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pinsp->InvalidateRect(NULL,false);
}

//Increase height of low fill search box.
void Tech::OnHlarger() 
{
	theapp->SavedFillH+=1;
	m_fsizeh.Format("%3i",theapp->SavedFillH);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pinsp->InvalidateRect(NULL,false);
}

//Decrease height of low fill search box.
void Tech::OnHsmaller() 
{
	theapp->SavedFillH-=1;
	m_fsizeh.Format("%3i",theapp->SavedFillH);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pinsp->InvalidateRect(NULL,false);
}

//Increases the size of the front tamperband edge search rectangle.
void Tech::OnTblarger() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandDy+=1;
	//if(theapp->SavedTBH=pframe->m_pxaxis->TBHeight >=100)
	m_edittb.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandDy);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pinsp->InvalidateRect(NULL,false);
	
}

//Decreases the size of the front tamperband edge search rectangle.
void Tech::OnTbsmaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandDy-=1;
	m_edittb.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandDy);
	UpdateData(false);
	InvalidateRect(NULL,false);
	pframe->m_pinsp->InvalidateRect(NULL,false);
	
}


//NOT USED
void Tech::OnRadio1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=1;
	//CheckDlgButton(IDC_RADIO1,1);CheckDlgButton(IDC_RADIO2,0);
	CheckDlgButton(IDC_RADIO3,0);CheckDlgButton(IDC_RADIO4,0);
	
}

//NOT USED
void Tech::OnRadio2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=2;
	CheckDlgButton(IDC_RADIO3,0);CheckDlgButton(IDC_RADIO4,0);
	//CheckDlgButton(IDC_RADIO1,1);CheckDlgButton(IDC_RADIO2,0);
	
}

//NOT USED
void Tech::OnRadio5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].tBandType=3;
	CheckDlgButton(IDC_RADIO3,0);CheckDlgButton(IDC_RADIO4,0);
}



//Appears to not be used.
void Tech::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0;
		Function=0;
		CntrBits=0;

		pframe=(CMainFrame*)AfxGetMainWnd();
		if(theapp->NoSerialComm==false) 
		{
						
			pframe->m_pserial->ControlChars=Value+Function+CntrBits;
		}
		
	}	
	CDialog::OnTimer(nIDEvent);
}


//Moves left camera2 cap edge search box.
void Tech::OnLtb2left() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lSWin-=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].lSWin<=0)	//Search box cannot be outside of camera image. 
		theapp->jobinfo[pframe->CurrentJobNum].lSWin=0;
	
	m_editlswin.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].lSWin);
	UpdateData(false);
	
	pframe->m_pinsp->InvalidateRect(NULL,false);
}

//Moves right camera2 cap edge search box.
void Tech::OnLtb2right() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lSWin+=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].lSWin>=158) theapp->jobinfo[pframe->CurrentJobNum].lSWin=158;
	
	m_editlswin.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].lSWin);
	UpdateData(false);
	pframe->m_pinsp->InvalidateRect(NULL,false);
}

//Moves right camera3 cap edge search box.
void Tech::OnRtb2right() 
{

	if(theapp->jobinfo[pframe->CurrentJobNum].rSWin>=158) theapp->jobinfo[pframe->CurrentJobNum].rSWin=158;

	theapp->jobinfo[pframe->CurrentJobNum].rSWin+=2;
	m_editrswin.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].rSWin);
	UpdateData(false);
	pframe->m_pinsp->InvalidateRect(NULL,false);

}

//Moves left camera3 cap edge search box.
void Tech::OnRtb2left() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rSWin-=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].rSWin<=0) theapp->jobinfo[pframe->CurrentJobNum].rSWin=0;


	m_editrswin.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].rSWin);
	UpdateData(false);
	pframe->m_pinsp->InvalidateRect(NULL,false);

}


void Tech::OnOK() 
{
	pframe->SaveJob(pframe->CurrentJobNum);				//Save changes made to job.
	pframe->m_pinsp->InvalidateRect(NULL,false);	//Repaint inspection configuration dialog.
	pframe->SetTimer(45,200,NULL);
	CDialog::OnOK();
}

//Select normal bottle lip style.
void Tech::OnCheck1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lip=0;
	CheckDlgButton(IDC_CHECK1,1);	//Check normal lip button
	CheckDlgButton(IDC_CHECK2,0);	//Uncheck no-lip button
	
	UpdateData(false);
	
}

//Select no lip bottle style.
void Tech::OnCheck2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lip=1;
	CheckDlgButton(IDC_CHECK1,0);
	CheckDlgButton(IDC_CHECK2,1);

	UpdateData(false);
	
}

//Normal bottle cap inspection selected.
void Tech::OnCheck3() 
{

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,530);
		pframe->m_pserial->SendChar(0,530);		//Send something to PLC
	}

	theapp->jobinfo[pframe->CurrentJobNum].sport=0; //Normal cap
	//Update check boxes which are used like radio buttons.	
	CheckDlgButton(IDC_CHECK3,1);
	CheckDlgButton(IDC_CHECK4,0);
	CheckDlgButton(IDC_CHECK5,0);
	
	UpdateDisplay();
	UpdateData(false);	
	
}

//sports bottle cap inspection selected.
void Tech::OnCheck4() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,530);
		pframe->m_pserial->SendChar(0,530);
	}

	theapp->jobinfo[pframe->CurrentJobNum].sport=1;
	CheckDlgButton(IDC_CHECK3,0);
	CheckDlgButton(IDC_CHECK4,1);
	CheckDlgButton(IDC_CHECK5,0);

	UpdateDisplay();
	UpdateData(false);
}

//Foil bottle cap inspection selected.
void Tech::OnCheck5() 
{
	

	theapp->jobinfo[pframe->CurrentJobNum].sport=2;
	CheckDlgButton(IDC_CHECK3,0);
	CheckDlgButton(IDC_CHECK4,0);
	CheckDlgButton(IDC_CHECK5,1);

	//theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor == 1
	UpdateDisplay();
	UpdateData(false);
}

//Used to find missing cap.

//Increase cap width limit.
void Tech::OnWidthmore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capWidthLimit+=1;
	m_editwidth.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].capWidthLimit);
	UpdateData(false);
	
}

//Decrease cap width limit.
void Tech::OnWidthless() 
{
	theapp->jobinfo[pframe->CurrentJobNum].capWidthLimit-=1;
	m_editwidth.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].capWidthLimit);
	UpdateData(false);
	
}

//Toggles enable disable of cap width limit inspection.
void Tech::OnEnablewidth() 
{
	if(theapp->enableWidth==true)	//If cap width inspection was enabled
	{
		theapp->enableWidth=false;	//disable it.
		CheckDlgButton(IDC_ENABLEWIDTH,0);	//Uncheck enable button.
	}
	else  //Opposite of above.
	{
		theapp->enableWidth=true;
		CheckDlgButton(IDC_ENABLEWIDTH,1);
	}
}



//Moves the extra light inspection area up.
void Tech::OnOlup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].fillArea-=2;
	m_editol.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].fillArea);
	UpdateData(false);

	pframe->m_pinsp->InvalidateRect(NULL,false);
	
}

//Moves the extra light inspection area down.
void Tech::OnOldn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].fillArea+=2;
	m_editol.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].fillArea);
	UpdateData(false);

pframe->m_pinsp->InvalidateRect(NULL,false);
	
}

//Move the rear cameras tamperband break search boxes up.
void Tech::OnYoffup() 
{
	if(theapp->xtraYOffset > - 30)
	{
	theapp->xtraYOffset-=1;
	}
	m_edityoff.Format("%i",theapp->xtraYOffset);
	UpdateData(false);
	
}

//Move the rear cameras tamperband break search boxes up.
void Tech::OnYoffdn() 
{
	if(theapp->xtraYOffset < 30)   //Limit guards
	{
	theapp->xtraYOffset+=1;
	}
	m_edityoff.Format("%i",theapp->xtraYOffset);
	UpdateData(false);
	
}

//Moves the camera1 top limit search bar up.
void Tech::OnButtonup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar-=1;
	m_target_ht_bar.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar);
	UpdateData(false);
}

//Moves the camera1 top limit search bar down.
void Tech::OnButtondn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar+=1;	
	m_target_ht_bar.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar);
	UpdateData(false);
}

//Moves the camera2 top limit search bar up.
void Tech::OnButtonup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2-=1;
	m_target_height_bar2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2);
	UpdateData(false);
}

//Moves the camera2 top limit search bar down.
void Tech::OnButtonDown2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2+=1;
	m_target_height_bar2.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam2);
	UpdateData(false);
}

//Moves the camera3 top limit search bar up.
void Tech::OnButtonup3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3-=1;
	m_target_ht_bar3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3);
	UpdateData(false);
}

//Moves the camera3 top limit search bar down.
void Tech::OnButtonDown3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3+=1;
	m_target_ht_bar3.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].bottleReferenceBar_cam3);
	UpdateData(false);
	
}


//NOT USED
void Tech::OnCheck6() 
{
 
   	if(theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke==true)
   	{		
		theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke=false;
		CheckDlgButton(IDC_CHECK6,0);
	}

   else
   {
	   theapp->jobinfo[pframe->CurrentJobNum].is_diet_coke=true;
	   CheckDlgButton(IDC_CHECK6,1);
   }
   	UpdateData(false);
}

//NOT USED
void Tech::OnIncrDiff() 
{
	theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap+=1;
	m_diff_var.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap);
	
			UpdateData(false);
	
}

//NOT USED
void Tech::OnDcrDiff() 
{
//if(theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap >=0)
//	{	
		theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap -=1;
		m_diff_var.Format("%i",theapp->jobinfo[pframe->CurrentJobNum].diff_for_no_cap);
//	}
	
			UpdateData(false);
}

void Tech::OnCheck7() 
{
	/*
	if(theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor==0) //If disabled
	{
		theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor=1; //ENABLE
		

	}
	else
	{
		theapp->jobinfo[pframe->CurrentJobNum].enable_foil_sensor=0;//ELSE DISABLE
	}
	UpdateDisplay();
	*/
}

void Tech::UpdateDisplay()
{

	//#####

	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_CHECK5);  ///For Pattern 1
	int ChkBox = m_ctlCheck->GetCheck();
	if( (ChkBox == BST_CHECKED) && (theapp->NoSerialComm==false))
	{
		pframe->m_pserial->SendChar(1,360);

	}
	else if( (ChkBox == BST_UNCHECKED) && (theapp->NoSerialComm==false))
	{

		pframe->m_pserial->SendChar(2,360);
	}

	pframe->SetTimer(45,100,NULL);
	//######


		UpdateData(false);
}
