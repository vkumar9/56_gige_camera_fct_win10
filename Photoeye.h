#if !defined(AFX_PHOTOEYE_H__6FF88DDD_697E_4C84_AAF4_8E157EEDF4DE__INCLUDED_)
#define AFX_PHOTOEYE_H__6FF88DDD_697E_4C84_AAF4_8E157EEDF4DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Photoeye.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Photoeye dialog

class Photoeye : public CDialog
{
friend class CCapApp;
friend class CMainFrame;
// Construction
public:
	Photoeye(CWnd* pParent = NULL);   // standard constructor

	CMainFrame* pframe;
	CCapApp* theapp;
	CString m_enable;	//String which displays current product flow orientation.

	long Value;		//NOT USED
	int Function;	//NOT USED
	int CntrBits;	//NOT USED
// Dialog Data
	//{{AFX_DATA(Photoeye)
	enum { IDD = IDD_PHOTOEYE };
	CString	m_title;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Photoeye)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Photoeye)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRadio1a();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PHOTOEYE_H__6FF88DDD_697E_4C84_AAF4_8E157EEDF4DE__INCLUDED_)
