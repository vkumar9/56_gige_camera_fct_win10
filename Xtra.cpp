// Xtra.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Xtra.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Xtra dialog


Xtra::Xtra(CWnd* pParent /*=NULL*/)
	: CDialog(Xtra::IDD, pParent)
{
	//{{AFX_DATA_INIT(Xtra)
	m_editthresh = _T("");
	m_editthresh2 = _T("");
	//}}AFX_DATA_INIT
}


void Xtra::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Xtra)
	DDX_Text(pDX, IDC_EDITTHRESH, m_editthresh);
	DDX_Text(pDX, IDC_EDITTHRESH2, m_editthresh2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Xtra, CDialog)
	//{{AFX_MSG_MAP(Xtra)
	ON_BN_CLICKED(IDC_U2THRESHLESS, OnU2threshless)
	ON_BN_CLICKED(IDC_U2THRESHMORE, OnU2threshmore)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_U2POSUP, OnU2posup)
	ON_BN_CLICKED(IDC_U2POSDN, OnU2posdn)
	ON_BN_CLICKED(IDC_U2THRESHLESS2, OnU2threshless2)
	ON_BN_CLICKED(IDC_U2THRESHMORE2, OnU2threshmore2)
	ON_BN_CLICKED(IDC_U2POSUP2, OnU2posup2)
	ON_BN_CLICKED(IDC_U2POSDN2, OnU2posdn2)
	ON_BN_CLICKED(IDC_SMALLER, OnSmaller)
	ON_BN_CLICKED(IDC_LARGER, OnLarger)
	ON_BN_CLICKED(IDC_SMALLER2, OnSmaller2)
	ON_BN_CLICKED(IDC_LARGER2, OnLarger2)
	ON_BN_CLICKED(IDC_U2POSUP3, OnU2posup3)
	ON_BN_CLICKED(IDC_U2POSDN3, OnU2posdn3)
	ON_BN_CLICKED(IDC_U2POSUP4, OnU2posup4)
	ON_BN_CLICKED(IDC_U2POSDN4, OnU2posdn4)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Xtra message handlers

BOOL Xtra::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_px=this;
	theapp=(CCapApp*)AfxGetApp();
	res=1;
	
	
	UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Xtra::UpdateDisplay()
{
	
	//m_editoffset.Format("%3i",pframe->m_pxaxis->U2Offset+40);
	m_editthresh.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1);
	m_editthresh2.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2);
	
	if(theapp->jobinfo[pframe->CurrentJobNum].userType==1)
	{
		CheckDlgButton(IDC_RADIO2,1);
		CheckDlgButton(IDC_RADIO1,0);
	}
	else
	{
		CheckDlgButton(IDC_RADIO1,1);
		CheckDlgButton(IDC_RADIO2,0);
	}
	UpdateData(false);
}


void Xtra::OnU2threshless() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1+=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1 >100) theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1=100;
	m_editthresh.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1);
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
	
}

void Xtra::OnU2threshmore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1-=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1 <0) theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1=0;	

	m_editthresh.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh1);
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) 
	{
		KillTimer(1);
	pframe->SaveJob(pframe->CurrentJobNum);
	}
	
	CDialog::OnTimer(nIDEvent);
}

void Xtra::OnU2posup() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2YOffset-=1;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnU2posdn() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2YOffset+=1;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Xtra::OnU2threshless2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2+=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2>100) theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2=100;
	m_editthresh2.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2);
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnU2threshmore2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2-=1;

	if(theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2<0) theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2=0;
	
	m_editthresh2.Format("%3i",100-theapp->jobinfo[pframe->CurrentJobNum].u2Thresh2);
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnU2posup2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2YOffset-=20;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnU2posdn2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2YOffset+=20;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnSmaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2XOffset+=2;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnLarger() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2XOffset-=2;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Xtra::OnSmaller2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR-=2;
	if(theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR <=0)theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR=0;
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnLarger2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR+=2;
		if(theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR >=200)theapp->jobinfo[pframe->CurrentJobNum].u2XOffsetR=200;

	SetTimer(1,3000,NULL);
	UpdateData(false);	
	
}

void Xtra::OnU2posup3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR-=1;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

void Xtra::OnU2posdn3() 
{
theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR+=1;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Xtra::OnU2posup4() 
{
theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR-=20;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Xtra::OnU2posdn4() 
{
theapp->jobinfo[pframe->CurrentJobNum].u2YOffsetR+=20;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);	
}

void Xtra::OnRadio1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].userType=0;
	CheckDlgButton(IDC_RADIO2,0); 
	CheckDlgButton(IDC_RADIO1,1);	
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
}

void Xtra::OnRadio2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].userType=1;
	CheckDlgButton(IDC_RADIO2,1); 
	CheckDlgButton(IDC_RADIO1,0);
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
}
