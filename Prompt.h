#if !defined(AFX_PROMPT_H__DC837C60_6C62_4CFF_B023_54AA0EDE2FFA__INCLUDED_)
#define AFX_PROMPT_H__DC837C60_6C62_4CFF_B023_54AA0EDE2FFA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Prompt.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Prompt dialog

class Prompt : public CDialog
{
// Construction
friend class CMainFrame;
public:
	void RePaint();
	int BoxColor;			//Variable used to toggle between gray and red color background for this dialog box.
	Prompt(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Prompt)
	enum { IDD = IDD_PROMPT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

CMainFrame* pframe;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Prompt)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Prompt)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnOff();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROMPT_H__DC837C60_6C62_4CFF_B023_54AA0EDE2FFA__INCLUDED_)
