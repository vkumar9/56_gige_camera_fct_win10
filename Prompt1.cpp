// Prompt1.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Prompt1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Prompt1 dialog


Prompt1::Prompt1(CWnd* pParent /*=NULL*/)
	: CDialog(Prompt1::IDD, pParent)
{
	//{{AFX_DATA_INIT(Prompt1)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Prompt1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Prompt1)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Prompt1, CDialog)
	//{{AFX_MSG_MAP(Prompt1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Prompt1 message handlers

void Prompt1::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
