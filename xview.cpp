// xview.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "xview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// xview

IMPLEMENT_DYNCREATE(xview, CView)

xview::xview()
{
}

xview::~xview()
{
}


BEGIN_MESSAGE_MAP(xview, CView)
	//{{AFX_MSG_MAP(xview)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// xview drawing

void xview::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// xview diagnostics

#ifdef _DEBUG
void xview::AssertValid() const
{
	CView::AssertValid();
}

void xview::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// xview message handlers
