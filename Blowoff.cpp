// Blowoff.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Blowoff.h"
#include "mainfrm.h"
#include "serial.h"
#include "capview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Blowoff dialog


Blowoff::Blowoff(CWnd* pParent /*=NULL*/)
	: CDialog(Blowoff::IDD, pParent)
{
	//{{AFX_DATA_INIT(Blowoff)
	m_dist = _T("");
	m_dur = _T("");
	m_slatpw = _T("");
	//}}AFX_DATA_INIT
}


void Blowoff::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Blowoff)
	DDX_Text(pDX, IDC_DIST, m_dist);
	DDX_Text(pDX, IDC_DUR, m_dur);
	DDX_Text(pDX, IDC_ENABLE, m_enable);
	DDX_Text(pDX, IDC_SLATPW, m_slatpw);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Blowoff, CDialog)
	//{{AFX_MSG_MAP(Blowoff)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_BN_CLICKED(IDC_M2, OnM2)
	ON_BN_CLICKED(IDC_L2, OnL2)
	ON_BN_CLICKED(IDC_MORE2, OnMore2)
	ON_BN_CLICKED(IDC_LESS2, OnLess2)
	ON_BN_CLICKED(IDC_ENABLE, OnEnable)
	ON_BN_CLICKED(IDC_PWMORE, OnPwmore)
	ON_BN_CLICKED(IDC_PWLESS, OnPwless)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Blowoff message handlers

BOOL Blowoff::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();		//Get reference to main window.
	pframe->m_pblowoff=this;					//Set main window's blowoff dlg reference to this window.
	theapp=(CCapApp*)AfxGetApp();				//Get reference to app object.

	//pframe->SetTimer(8,100,NULL);//com
	//pframe->OnLine=false;
	//pframe->m_pview->SetTimer(2,500,NULL);
	Value=0;
	Function=0;
	//Update variables used on gui.
	m_dist.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDist);
	m_dur.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDur);
	m_slatpw.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].slatPW);

	//if(theapp->SavedEjectDisable==true){CheckDlgButton(IDC_ENABLE,1); m_enable.Format("%s","Eject is disabled");}else{CheckDlgButton(IDC_ENABLE,0);m_enable.Format("%s","Eject is enabled");}
	UpdateData(false);	//Update gui from variables.

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Increase blowoff distance.
void Blowoff::OnMore() 
{
	Value=theapp->jobinfo[pframe->CurrentJobNum].encDist+=1;
	m_dist.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDist);

	//Value=Value*1000;
	Function=80;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
	
}

//Increase blowoff distance by a lot.
void Blowoff::OnM2() 
{
	Value=theapp->jobinfo[pframe->CurrentJobNum].encDist+=20;
	m_dist.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDist);

	//Value=Value*1000;
	Function=80;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
	
}

//decrease blowoff distance.
void Blowoff::OnLess() 
{
	Value=theapp->jobinfo[pframe->CurrentJobNum].encDist-=1;
	m_dist.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDist);

	if(Value<=0) Value=theapp->jobinfo[pframe->CurrentJobNum].encDist=0;

	//Value=Value*1000;
	Function=80;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
}

//decrease blowoff distance by a lot.
void Blowoff::OnL2() 
{
	Value=theapp->jobinfo[pframe->CurrentJobNum].encDist-=20;
	m_dist.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDist);

	if(Value<=0) Value=theapp->jobinfo[pframe->CurrentJobNum].encDist=0;
//	Value=Value*1000;
	Function=80;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
}

//Increase blowoff duration
void Blowoff::OnMore2() 
{
	Value=theapp->jobinfo[pframe->CurrentJobNum].encDur+=1;
	m_dur.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDur);

	//Value=Value*1000;
	Function=90;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
}

//decrease blowoff duration
void Blowoff::OnLess2() 
{
	Value=theapp->jobinfo[pframe->CurrentJobNum].encDur-=1;
	m_dur.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].encDur);
	
	//Value=Value*1000;
	Function=90;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
}

//Toggles between enabled or disabled ejector mechanism
void Blowoff::OnEnable() 
{
			//if ejector is disabled
	if( theapp->SavedEjectDisable==true )
	{
		CheckDlgButton(IDC_ENABLE,0);				//Update gui to show ejector is enabled.
		Enable(false);								//Send enable ejector command to PLC.
		theapp->SavedEjectDisable=false; 			//Save new state of ejector to app variable.
		m_enable.Format("%s","Eject is enabled");	//Update variable which stores ejector enb/dis string for ejector.
	}
	else  //opposite of above.
	{ 
		CheckDlgButton(IDC_ENABLE,1);
		Enable(true); 
		theapp->SavedEjectDisable=true;
		m_enable.Format("%s","Eject is disabled");
	}
	UpdateData(false);	//Update gui
	//pframe->m_pview->UpDateEject();
}

void Blowoff::OnOK() 
{
	pframe->Stop=false;	//Does not appear to be used.
	pframe->SaveJob(pframe->CurrentJobNum);	//Save changes made to job.
	
	CDialog::OnOK();
}

//Does not appear to be used.
void Blowoff::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0;
		Function=0;
		CntrBits=0;


		SetTimer(2,200,NULL);
		
	}

	if(nIDEvent==2)
	{
		KillTimer(2);
		pframe=(CMainFrame*)AfxGetMainWnd();
		pframe->Stop=false;
	}

	
	CDialog::OnTimer(nIDEvent);
}

//Function to which tells the PLC to either enable or disable the ejector.
//off: enable or disable the ejector.
void Blowoff::Enable(bool off)
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->Stop=true;

	if(off==true)	//If you want to turn off the ejector.
	{	
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,100);
			pframe->m_pserial->SendChar(0,100);
		}
	}
	else   //If you want to turn on the ejector.
	{
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,110);
			pframe->m_pserial->SendChar(0,110);
		}
		//pframe->m_pserial->QueMessage(0,110);
	}
	

}



//Increase pulse width modulation
void Blowoff::OnPwmore() 
{
	theapp->jobinfo[pframe->CurrentJobNum].slatPW+=1;
	m_slatpw.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].slatPW);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->jobinfo[pframe->CurrentJobNum].slatPW,280);
		pframe->m_pserial->SendChar(theapp->jobinfo[pframe->CurrentJobNum].slatPW,280);
	}

	UpdateData(false);
	
}

//Decrease pulse width modulation
void Blowoff::OnPwless() 
{
	theapp->jobinfo[pframe->CurrentJobNum].slatPW-=1;
	m_slatpw.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].slatPW);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->jobinfo[pframe->CurrentJobNum].slatPW,280);
		pframe->m_pserial->SendChar(theapp->jobinfo[pframe->CurrentJobNum].slatPW,280);
	}

	UpdateData(false);
	
}
