// Serial.cpp : implementation file
//
#include "stdafx.h"
#include "Cap.h"
#include "Serial.h"
#include "mainfrm.h"
#include "capview.h"
#include "Prompt3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Serial dialog
//56 program 
Serial::Serial(CWnd* pParent /*=NULL*/)
	: CDialog(Serial::IDD, pParent)
{
	//{{AFX_DATA_INIT(Serial)
	m_chars = _T("");
	m_chars2 = _T("");
	m_commcount = _T("");
	m_filler = _T("");
	m_motor = _T("");
	m_capper = _T("");
	m_editinchar = _T("");
	m_commcount2 = _T("");
	//}}AFX_DATA_INIT
}

void Serial::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Serial)
	DDX_Control(pDX, IDC_MSCOMM1, m_serial);
	DDX_Text(pDX, IDC_CHARS, m_chars);
	DDX_Text(pDX, IDC_CHARS2, m_chars2);
	DDX_Text(pDX, IDC_COMMCOUNT, m_commcount);
	DDX_Text(pDX, IDC_FILLER, m_filler);
	DDX_Text(pDX, IDC_MOTOR, m_motor);
	DDX_Text(pDX, IDC_CAPPER, m_capper);
	DDX_Text(pDX, IDC_EDITINCHAR, m_editinchar);
	DDX_Text(pDX, IDC_COMMCOUNT2, m_commcount2);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(Serial, CDialog)
	//{{AFX_MSG_MAP(Serial)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_QUE,QueMess)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Serial message handlers

BOOL Serial::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pserial=this;

	theapp=(CCapApp*)AfxGetApp();
	returnedInt=0;
	noCommCount=0;
	noCommCount2=0;
	didComOnce=false;
	m_serial.SetOutBufferSize(128);			//Set output buffer to 128 bytes.
	m_serial.SetInBufferSize(128);			//Set input buffer to 128 bytes.
	m_serial.SetCommPort(1); // 6: Office laptop ....1 is Field Machine
	m_serial.SetSettings("38400,n,8,1");	//Baud rate 38400, no parity checking, 8 data bits, 1 stop bit    //19200
	CommProblem=false;
	count=0;
	inQue=0;
	lockOut=false;
	inSendChar=0;
	currentFillerPosition=FillPos=currentCapperPosition=CapPos=0;
	SetTimer(3,4,NULL);						//Set timer to read from PLC every 4 milliseconds.
	
	try
	{
		m_serial.SetPortOpen(true);
		theapp->commProblem=false;
	}
	//Prompt the user with the message that the COM port is not working.
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
		//m_serial.SetPortOpen(false);
		CommProblem=true;
		theapp->commProblem=true;
	//	AfxMessageBox("The serial port could not be opened.");

		pframe->prompt_code=10;
		Prompt3 promtp3;
		promtp3.DoModal();
		
	}
	long commID=m_serial.GetCommID();	//NOT USED

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Read from serial port. Update motor position, user eject all bottles control, temperature, filler/capper heads
void Serial::GetChar(int FromPLC)
{	
	//noCommCount2+=1;
	m_serial.SetRThreshold(1);				//Serial will fire event for every 1 character received.
	
	buf2=m_serial.GetInput();				//Read input from received buffer.
	short event=m_serial.GetCommEvent();	//Get type of event generated.
	
	CharsFromPLC1=buf2.bstrVal;				//Convert from buffer to string.
	VariantClear(&buf2);
	
	returnedChar="";

	//Loop looks for + sign then parses the characters after the + sign.
	for(int i=1; i<=16; i++)
	{
		b=CharsFromPLC1.Left(32);		//Take the left most 32 characters.
		b=b.Right(16+i);				//Take the right most 16+i characters
		a=b.Right(16+i);
		b=b.Left(1);					//Take the leading character

		if(b=="+")						//Check if this character is a plus sign
		{	
			CharsFromPLC=a.Left(16);				//Get 16 characters after the + sign
			returnedChar=CharsFromPLC.Right(3);
			//CString tem=sentChar;
			i=16;									//don't repeat the loop again.

			int temp_motor_pos;

			temp_motor_pos=atoi(CharsFromPLC.Mid(5,3));		//parse out the current motors position.

			if(temp_motor_pos != 666)						//If motor position is not 666 then eject from 
															//user is not enabled and this the actual position.
			{

				MotorPos=atoi(CharsFromPLC.Mid(5,3));		//parse out the current motors position.
				theapp->rejectFromUser=false;				//Set flag indicating that eject signal from user was not received.
				pframe->is666received=false;				//Set flag indicating that eject signal from user was not received.
			}
			else if(temp_motor_pos ==666)					//If eject signal from user was received. 
			{
					if(pframe->OnLine==true)				//If ejector is on-line
					{
						pframe->IOReject=true;				//Set flag indicating that eject signal from user was received.
						theapp->rejectFromUser=true;		//Set flag indicating that eject signal from user was received.
					}
					pframe->is666received=true; //For consecutive Reject signal from External I/O (client input onPLC)

			}
				///Re wrote the logic below to not over ride MotorPos variable when 666 is received
				/*
					MotorPos=atoi(CharsFromPLC.Mid(5,3));
				if(MotorPos==666)
				{
					if(pframe->OnLine==true)
					{
						pframe->IOReject=true;
						theapp->rejectFromUser=true;
					}
				}
				else
				{theapp->rejectFromUser=false;}
				*/
				////
			
		}
	}

	//Check if beginning of frame is correct, 789 or 701 are just symbols indicating the beginning of a frame.
	if(returnedChar=="789" || returnedChar=="701")//sentChar)
	{
		//KillTimer(1);
		noCommCount2=noCommCount;
	
		ShowResults(true);
		//SendChar(0,0);
		//noCommCount2=0;
		noCommCount=0;		//Reset send/receive counter.
	}



	UpdateData(false);
}

//Sends data out from the serial port.
//data is the info to send out.
//function is the function code for the frame.
bool Serial::SendChar(int data, int function)
{
	data2 = data;
	function2 = function;

	bool result;			//NOT USED
	bool verified=false;
	bool verified2=false;	//NOT USED

	L1=function + 10000;//add check code
	itoa(L1,st1,10);
	f1=st1;
	f1=f1.Right(4);
	
	L2=data+10000000;
	itoa(L2,st2,10);
	d1=st2;
	d1=d1.Right(7);

	strcpy(st,d1);
	strcat(st,f1);
	ch=st;
	ch="%0000"+ch;
//	ch="%12345670123";

	sentChar=ch.Right(3);
	buf.vt=VT_BSTR;
	CharsToPLC=buf.bstrVal=ch.AllocSysString();

	m_serial.SetOutput(buf);
	
	VariantClear(&buf);
	ch.FreeExtra();

	noCommCount+=1;							//Increment send/receive counter
	m_chars2.Format("%s",CharsToPLC );		//Load send string to GUI variable for display
	UpdateData(false);

	return verified;
}

//Closes serial port. Called when program is shutting down
void Serial::ShutDown()
{
	m_serial.SetPortOpen(false);
	KillTimer(1);
	KillTimer(2);

}

//Closes serial port.
void Serial::OnClose()
{
	m_serial.SetPortOpen(false);
	KillTimer(1);
	KillTimer(2);
	CDialog::OnClose();
}
void Serial::OnOK() 
{
	// TODO: Add extra validation here
	pframe->SetTimer(43,100,NULL); //To Refresh the pic in XAxisView
	CDialog::OnOK();
}

//NOT USED
LRESULT Serial::QueMess(WPARAM data, LPARAM function)
{
	bool Done=false;
	
	Done=SendChar(data, function);
		
	return true;
}

void Serial::OnTimer(UINT nIDEvent) 
{
	//???
	if(nIDEvent==1)
	{
		
	}

	//Does not appear to be used.
	if(nIDEvent==2)
	{
		KillTimer(2);
		
		SendChar(0,0);
	}

	//Timer which reads in the output of the PLC
	if(nIDEvent==3)
	{
		GetChar(1);

		
		//ML- now similar to the 85
		////////////////////////////////
		
		//if(noCommCount>=102) 
		if(noCommCount>=50) //ML- 85System uses 50		//If 50 sends with no receive there is an error
		{
			comString=itoa(noCommCount,bufz,10);	//convert send/receive counter integer to string.
			pframe->m_pview->m_status="Comm Count " + comString;		//Show issue in main screen status box

			noCommCount=0;
			ShowResults(false);
			didComOnce=true;	//Set flag indicating a serial port problem.
		}

		////////////////////////////////

	}

	//Does not appear to be used.
	if(nIDEvent==4)
	{
		KillTimer(4);

		lockOut=false;
	}

	if(nIDEvent==5)
	{
		KillTimer(5);
		pframe->m_pview->m_status="Slat Eject OK!!!";	
		pframe->m_pview->UpdateDisplay();
		
	}

	CDialog::OnTimer(nIDEvent);
}

//Update temperature from PLC, motor position, current filler/capper heads used, user eject request, reset request.
//Updates the display with the last sent data, last received data and send/receive count.
void Serial::ShowResults(bool ok)
{
	if(ok==true)
	{
		//Get temperature.
		b=CharsFromPLC.Right(14);
		Temperature=atoi(b.Left(3));
		
		b=CharsFromPLC.Right(11);

		int temp_var;
		temp_var=atoi(b.Left(3));
		if(temp_var !=666)
		{
			MotorPos=atoi(b.Left(3));		//Get motor position.
		}
		
		
		b=CharsFromPLC.Right(8);
		currentFillerPosition=FillPos=atoi(b.Left(3));		//Get current filler head being used.

		//Validate filler head is within range.
		if(currentFillerPosition>=120)currentFillerPosition=120;
		if(currentFillerPosition<=0)currentFillerPosition=0;

		b=CharsFromPLC.Right(5);
		currentCapperPosition=CapPos=atoi(b.Left(2));		//Get current capper head being used.

		b=CharsFromPLC.Right(3);
		statusChar=atoi(b);									//Get status from PLC.

		//Validate capper head is within range.
		if(currentCapperPosition>=32)currentCapperPosition=32;
		if(currentCapperPosition<=0)currentCapperPosition=0;
		
		//Update display
		m_motor.Format("%i",MotorPos );
		m_filler.Format("%i",FillPos );
		m_capper.Format("%i",CapPos );

		if(temp_var==666)									//If eject signal from user was received. 
		{
			if(pframe->OnLine==true)						//If ejector is on-line
			{
				pframe->IOReject=true;						//Set flag indicating that eject signal from user was received.
				theapp->rejectFromUser=true;				//Set flag indicating that eject signal from user was received.
			}
		}
		else
		{
			theapp->rejectFromUser=false;					//Set flag indicating that eject signal from user was not received.
		}
		if(MotorPos==777) theapp->resetFromPLC=true;		//Not sure what this does.
		if(MotorPos!=777) theapp->resetFromPLC=false;
		if(statusChar==701){ theapp->slatBroken=true; SetTimer(5,30000,NULL);}
		if(statusChar!=701) theapp->slatBroken=false;

		if( theapp->slatBroken==true){pframe->m_pview->m_status="Eject Slat Broken!!!";	pframe->m_pview->UpdateDisplay();}
		
		if(theapp->tempSelect ==1)	//Temperature is obtained from the PLC
		{
			theapp->PLC_Temperature = Temperature;	//Save read temperature to variable.

			if(theapp->PLC_Temperature>=theapp->tempLimit)	//Check if temperature is with in normal range
			{
				pframe->m_pview->m_status="Cabinet Temp Exceeded!!!";
				pframe->m_pview->UpdateDisplay();
			}
		}
		//if(MotorPos==812) theapp->resetLight=true;
		//if(MotorPos!=812) theapp->resetLight=false;
	}

	CharsFromPLC=CharsFromPLC.Right(16);
	m_chars.Format("%s",CharsFromPLC );
	m_chars2.Format("%s",CharsToPLC );	
	m_commcount.Format("%i",noCommCount2);

	
		
	UpdateData(false);
	
}
