#if !defined(AFX_XTRA_H__20C6AFEA_7D6E_4656_994A_5D054D741574__INCLUDED_)
#define AFX_XTRA_H__20C6AFEA_7D6E_4656_994A_5D054D741574__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Xtra.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Xtra dialog

class Xtra : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:
	void UpdateDisplay();
	int res;
	Xtra(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Xtra)
	enum { IDD = IDD_XTRA };
	CString	m_editthresh;
	CString	m_editthresh2;
	//}}AFX_DATA
CMainFrame* pframe;
CCapApp* theapp;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Xtra)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Xtra)
	virtual BOOL OnInitDialog();
	afx_msg void OnU2threshless();
	afx_msg void OnU2threshmore();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnU2posup();
	afx_msg void OnU2posdn();
	afx_msg void OnU2threshless2();
	afx_msg void OnU2threshmore2();
	afx_msg void OnU2posup2();
	afx_msg void OnU2posdn2();
	afx_msg void OnSmaller();
	afx_msg void OnLarger();
	afx_msg void OnSmaller2();
	afx_msg void OnLarger2();
	afx_msg void OnU2posup3();
	afx_msg void OnU2posdn3();
	afx_msg void OnU2posup4();
	afx_msg void OnU2posdn4();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XTRA_H__20C6AFEA_7D6E_4656_994A_5D054D741574__INCLUDED_)
