// Band.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Band.h"

#include "mainfrm.h"
#include "xaxisview.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Band dialog


Band::Band(CWnd* pParent /*=NULL*/)
	: CDialog(Band::IDD, pParent)
{
	//{{AFX_DATA_INIT(Band)
	m_edittaller = _T("");
	m_editsen2 = _T("");
	m_edit2 = _T("");
	m_editband2size = _T("");
	m_choice = _T("");
	//}}AFX_DATA_INIT
}


void Band::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Band)
	DDX_Text(pDX, IDC_EDIT1, m_edittaller);
	DDX_Text(pDX, IDC_EDITSEN, m_editsen2);
	DDX_Text(pDX, IDC_EDIT2, m_edit2);
	DDX_Text(pDX, IDC_EDITBAND2SIZE, m_editband2size);
	DDX_Text(pDX, IDC_CHOICE, m_choice);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Band, CDialog)
	//{{AFX_MSG_MAP(Band)
	ON_BN_CLICKED(IDC_UP5, OnUp5)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_DN5, OnDn5)
	ON_BN_CLICKED(IDC_UP4, OnUp4)
	ON_BN_CLICKED(IDC_DN4, OnDn4)
	ON_BN_CLICKED(IDC_UP3, OnUp3)
	ON_BN_CLICKED(IDC_DN3, OnDn3)
	ON_BN_CLICKED(IDC_UP2, OnUp2)
	ON_BN_CLICKED(IDC_DN2, OnDn2)
	ON_BN_CLICKED(IDC_UP1, OnUp1)
	ON_BN_CLICKED(IDC_DN1, OnDn1)
	ON_BN_CLICKED(IDC_UPR5, OnUpr5)
	ON_BN_CLICKED(IDC_DNR5, OnDnr5)
	ON_BN_CLICKED(IDC_UPR4, OnUpr4)
	ON_BN_CLICKED(IDC_DNR4, OnDnr4)
	ON_BN_CLICKED(IDC_UPR3, OnUpr3)
	ON_BN_CLICKED(IDC_DNR3, OnDnr3)
	ON_BN_CLICKED(IDC_UPR2, OnUpr2)
	ON_BN_CLICKED(IDC_DNR2, OnDnr2)
	ON_BN_CLICKED(IDC_UPR1, OnUpr1)
	ON_BN_CLICKED(IDC_DNR1, OnDnr1)
	ON_BN_CLICKED(IDC_TALLER, OnTaller)
	ON_BN_CLICKED(IDC_SHORTER, OnShorter)
	ON_BN_CLICKED(IDC_UPALL, OnUpall)
	ON_BN_CLICKED(IDC_DNALL, OnDnall)
	ON_BN_CLICKED(IDC_LESSSEN, OnLesssen)
	ON_BN_CLICKED(IDC_MORESEN, OnMoresen)
	ON_BN_CLICKED(IDC_LEFTLEFT, OnLeftleft)
	ON_BN_CLICKED(IDC_LEFTRIGHT, OnLeftright)
	ON_BN_CLICKED(IDC_RIGHTLEFT, OnRightleft)
	ON_BN_CLICKED(IDC_RIGHTRIGHT, OnRightright)
	ON_BN_CLICKED(IDC_WIDER, OnWider)
	ON_BN_CLICKED(IDC_SHORT, OnShort)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Band message handlers

void Band::OnUp5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1)  //Updates job with new settings
	{
		KillTimer(1);
		pframe->SaveJob(pframe->CurrentJobNum);
	}
	
	CDialog::OnTimer(nIDEvent);
}

BOOL Band::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pband=this;
	theapp=(CCapApp*)AfxGetApp();
;
	
	refresh();

	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Band::OnDn5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[5]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUp4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDn4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[4]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUp3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDn3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[3]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUp2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDn2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[2]+=2;
	SetTimer(1,3000,NULL);
}

//Moves rear camera2 tamper band break search box 1 up.
void Band::OnUp1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1]-=2;
	SetTimer(1,3000,NULL);	//Updates selected job with new settings. Why Not call a function directly instead of a timer.
}

//Moves rear camera2 tamper band break search box 1 down.
void Band::OnDn1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].lBandOffset[1]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUpr5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDnr5() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[5]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUpr4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDnr4() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[4]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUpr3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDnr3() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[3]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUpr2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDnr2() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[2]+=2;
	SetTimer(1,3000,NULL);
}

void Band::OnUpr1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1]-=2;
	SetTimer(1,3000,NULL);
}

void Band::OnDnr1() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rBandOffset[1]+=2;
	SetTimer(1,3000,NULL);
}

//Increase the height of all rear (cameras 2, 3) tamper band search boxes.
void Band::OnTaller() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamDy+=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].rearCamDy>=50) theapp->jobinfo[pframe->CurrentJobNum].rearCamDy=50;
	m_edittaller.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamDy);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Decrease the height of all rear (cameras 2, 3) tamper band search boxes.
void Band::OnShorter() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamDy-=1;
	if(theapp->jobinfo[pframe->CurrentJobNum].rearCamDy<=0) theapp->jobinfo[pframe->CurrentJobNum].rearCamDy=0;
	m_edittaller.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamDy);
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves all rear (cameras 2, 3) tamper band search boxes up. 
void Band::OnUpall() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamY-=1;
	m_edit2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves all rear (cameras 2, 3) tamper band search boxes down. 
void Band::OnDnall() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rearCamY+=1;
	m_edit2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamY);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Values below or above the threshold (depending on selection) will be counted at
//Tamper band break pixels.

//Decrease the rear tamper band threshold.
void Band::OnLesssen() 
{
	
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 < 0 || theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 > 255) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2=0;
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2-=1;
	
	m_editsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2);
	refresh();
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Increase the rear tamper band threshold.
void Band::OnMoresen() 
{//tBandSurfThresh2
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 < 0 || theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2 > 255) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2=0;
	theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2+=1;
	
	if(theapp->jobinfo[pframe->CurrentJobNum].tBandType==5)
	{
		if(theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2>=100) theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2=100;
	}
	
	m_editsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2);
	refresh();
	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Moves all of the rear (camera 2) tamper band search boxes left
void Band::OnLeftleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].leftBandShift+=1;
	//Boxes cannot be more then 50 pixels from right edge of cap
	if(theapp->jobinfo[pframe->CurrentJobNum].leftBandShift>=50) theapp->jobinfo[pframe->CurrentJobNum].leftBandShift=50;
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves all of the rear (camera 2) tamper band search boxes right
void Band::OnLeftright() 
{

	theapp->jobinfo[pframe->CurrentJobNum].leftBandShift-=1;
	//Boxes cannot be moved beyond the right edge of the cap
	if(theapp->jobinfo[pframe->CurrentJobNum].leftBandShift<=0) theapp->jobinfo[pframe->CurrentJobNum].leftBandShift=0;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Moves all of the rear (camera 3) tamper band search boxes left
void Band::OnRightleft() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rightBandShift-=1;
	//Boxes cannot be moved beyond the left edge of the cap
	if(theapp->jobinfo[pframe->CurrentJobNum].rightBandShift<=0) theapp->jobinfo[pframe->CurrentJobNum].rightBandShift=0;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
}

//Moves all of the rear (camera 3) tamper band search boxes right
void Band::OnRightright() 
{
	theapp->jobinfo[pframe->CurrentJobNum].rightBandShift+=1;
	//Boxes cannot be more then 30 pixels from left edge of cap
	if(theapp->jobinfo[pframe->CurrentJobNum].rightBandShift>=30) theapp->jobinfo[pframe->CurrentJobNum].rightBandShift=30;
	
	SetTimer(1,3000,NULL);
	UpdateData(false);
	InvalidateRect(NULL,false);
	
}

//Increase the width of all rear (cameras 2, 3) tamper band search boxes.
void Band::OnWider() 
{
	theapp->jobinfo[pframe->CurrentJobNum].band2Size+=4;
	m_editband2size.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].band2Size);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Decrease the width of all rear (cameras 2, 3) tamper band search boxes.
void Band::OnShort() 
{
	theapp->jobinfo[pframe->CurrentJobNum].band2Size-=4;
	m_editband2size.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].band2Size);

	SetTimer(1,3000,NULL);
	UpdateData(false);
	
}

//Updates modify rear tab gui.
void Band::refresh()
{

	
	switch(theapp->jobinfo[pframe->CurrentJobNum].tBandType)
	{
		
		case 0:
			m_choice.Format("%s","");
		break;
		case 1:
			m_choice.Format("%s","edge");
		break;

		case 2:
			if(theapp->jobinfo[pframe->CurrentJobNum].dark==1)
			{
				m_choice.Format("%s","find dark");
			}
			else
			{
				m_choice.Format("%s","find light");
			}
		break;

		case 3:
			
		break;
	
		case 4:
			m_choice.Format("%s","integrity");
		break;
	
		case 5:
			m_choice.Format("%s","color");
		break;

		
	}

	m_editband2size.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].band2Size);
	m_edittaller.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamDy);
	m_editsen2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].tBandSurfThresh2);
	m_edit2.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].rearCamY);
	
	UpdateData(false);
}
