// Job.cpp : implementation file
//

#include "stdafx.h"
//#include "afxwin.h"

#include "cap.h"
#include "Job.h"
#include "mainfrm.h"
#include "xaxisview.h"
#include "capview.h"
#include "serial.h"
#include "insp.h"
#include "modify.h"
#include "lim.h"
#include "saveas.h"
#include "camera.h"
#include "Prompt3.h"
#include "view.h"
#include <atomic>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Job dialog


Job::Job(CWnd* pParent /*=NULL*/)
	: CDialog(Job::IDD, pParent)
{
	//{{AFX_DATA_INIT(Job)
	m_renamejob = _T("");
	//}}AFX_DATA_INIT
}


void Job::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Job)
	DDX_Control(pDX, IDC_RENAME, m_rename);
	DDX_Control(pDX, IDC_CHANGENAME, m_changename);
	DDX_Control(pDX, IDC_JOBS, m_joblist);
	DDX_Text(pDX, IDC_RENAME, m_renamejob);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Job, CDialog)
	//{{AFX_MSG_MAP(Job)
	ON_LBN_SELCHANGE(IDC_JOBS, OnSelchangeJobs)
	ON_BN_CLICKED(IDC_CHANGENAME, OnChangename)
	ON_BN_CLICKED(IDC_A, OnA)
	ON_BN_CLICKED(IDC_B, OnB)
	ON_BN_CLICKED(IDC_C, OnC)
	ON_BN_CLICKED(IDC_D, OnD)
	ON_BN_CLICKED(IDC_E, OnE)
	ON_BN_CLICKED(IDC_F, OnF)
	ON_BN_CLICKED(IDC_H, OnH)
	ON_BN_CLICKED(IDC_G, OnG)
	ON_BN_CLICKED(IDC_I, OnI)
	ON_BN_CLICKED(IDC_J, OnJ)
	ON_BN_CLICKED(IDC_K, OnK)
	ON_BN_CLICKED(IDC_L, OnL)
	ON_BN_CLICKED(IDC_M, OnM)
	ON_BN_CLICKED(IDC_N, OnN)
	ON_BN_CLICKED(IDC_O, OnO)
	ON_BN_CLICKED(IDC_P, OnP)
	ON_BN_CLICKED(IDC_Q, OnQ)
	ON_BN_CLICKED(IDC_R, OnR)
	ON_BN_CLICKED(IDC_S, OnS)
	ON_BN_CLICKED(IDC_T, OnT)
	ON_BN_CLICKED(IDC_U, OnU)
	ON_BN_CLICKED(IDC_V, OnV)
	ON_BN_CLICKED(IDC_W, OnW)
	ON_BN_CLICKED(IDC_X, OnX)
	ON_BN_CLICKED(IDC_Y, OnY)
	ON_BN_CLICKED(IDC_Z, OnZ)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_DASH, OnDash)
	ON_BN_CLICKED(IDC_J0, OnJ0)
	ON_BN_CLICKED(IDC_J1, OnJ1)
	ON_BN_CLICKED(IDC_J2, OnJ2)
	ON_BN_CLICKED(IDC_J3, OnJ3)
	ON_BN_CLICKED(IDC_J4, OnJ4)
	ON_BN_CLICKED(IDC_J5, OnJ5)
	ON_BN_CLICKED(IDC_J6, OnJ6)
	ON_BN_CLICKED(IDC_J7, OnJ7)
	ON_BN_CLICKED(IDC_J8, OnJ8)
	ON_BN_CLICKED(IDC_J9, OnJ9)
	ON_WM_TIMER()
	ON_COMMAND(ID_SAVEAS, OnSaveas)
	ON_BN_CLICKED(IDC_BUTTON1, OnDeleteJob)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Job message handlers

BOOL Job::OnInitDialog() 
{
	CDialog::OnInitDialog();
	pframe=(CMainFrame*) AfxGetMainWnd();			//Get pointer to main frame window.
	pframe->m_pjob=this;							//Set Main Frame's jobs dlg pointer this dlg.
	LoadJobs();										//Populate list box with saved jobs.
	m_joblist.SetCaretIndex(0);						//Set selection to first job in list.
	NameChanged=false;
	nIndex=0;										//Set index of currently selected job.
	original=JobOver=0;								//Indices of the original and new duplicate job reset.
	DoOver=false;									//Selected job is not a duplicate.
	SavAs=false;									//Duplicate dialog box has not been opened yet.
	UpdateData(true);

	//strcpy(newjobname,"saved_copy");
//	pframe->m_pserial->QueMessage(0,410);//red lite

	//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
//	pframe->m_pserial->SendMessage(WM_QUE,0,290);

	pframe->OnLine=false;
	pframe->m_pview->SetTimer(2,500,NULL);//toggle red square
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,410);
		pframe->m_pserial->SendChar(0,410);
	}
	
	CMenu* pMenu = GetMenu();
	pMenu->EnableMenuItem(ID_SAVEAS, pframe->SaveCopyDisabled ? MF_DISABLED | MF_GRAYED : 0);
	DrawMenuBar();

	GetDlgItem(IDC_BUTTON1)->EnableWindow(pframe->SaveCopyDisabled ? FALSE : TRUE);
		
	//pframe->UpdateOutput(4, true);
	//pframe->UpdateOutput(5, false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Populates Jobs list box.
void Job::LoadJobs()
{
	theapp=(CCapApp*)AfxGetApp();					//Get pointer to the app object.

	m_joblist.InsertString(0,"Job List");						//Set first entry to be the title "Job List"
	for(int i=1; i<=theapp->SavedTotalJobs; i++)				//for all jobs
	{
		if(theapp->jobinfo[i].jobname=="")						//if jobname is not set
		{
			theapp->jobinfo[i].jobname="xxx";//+itoa();			//change name to "xxx"
		}
		m_joblist.InsertString(i,theapp->jobinfo[i].jobname);	//update the list box with job's name.
	}

	UpdateData(false);
}

//Handler for selection of a new job in the jobs list box.
void Job::OnSelchangeJobs() 
{
	//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
//	pframe->m_pserial->SendMessage(WM_QUE,0,290);
	pframe->OnLine=false;							//Set flag indicating ejector is off-line.
	pframe->m_pview->SetTimer(2,500,NULL);//toggle red square
	
	DoOver=false;
	original=nIndex=m_joblist.GetCaretIndex();		//Get index of newly selected job. If the duplicate dialog box is opened now "original" will be the index of the job to duplicate.
	int n = m_joblist.GetTextLen( nIndex );			//NOT USED, Get the length of the newly selected job's name.

//	m_joblist.GetText( nIndex, str.GetBuffer(n) );
	m_joblist.GetText( nIndex, str );				//Get name string of newly selected job and put it in str.

	str.Remove(' ');								//Removes any white spaces from str.
	m_renamejob.Format("%s",str);					//Updates the variable holding the renameJob's text box string.

	m_joblist.GetText( original, Value );			//Get name string of newly selected job and put it in Value.
	Value.Remove(' ');								//Removes any white spaces from Value.

	UpdateData(false);	
}

//Updates system with the selected job settings.
void Job::OnOK() 
{
	//This if-else block is not used since nIndex is overwritten in the next statement.
	if(SavAs==false) {nIndex=m_joblist.GetCaretIndex();}else{nIndex=original;}

	nIndex=m_joblist.GetCaretIndex();		//Get index of currently selected job.
	if(nIndex != LB_ERR)   //LB_ERR implies NO selection was made
	{
		if(theapp->NoSerialComm==false) 
		{
			pframe->m_pserial->SendChar(0,410); //If some job selection was made, send system Offline
		}
	}


	theapp=(CCapApp*)AfxGetApp();
	pframe=(CMainFrame*)AfxGetMainWnd();


	//commenting for now
//	pframe->SaveJob(pframe->CurrentJobNum);//save old job..
				//Check if current selection is different from previously loaded job. If so..save the previous job before loading new job
//	pframe->CleanUp(); // empty function
	pframe->CurrentJobNum=nIndex;				//Set index of newly selected job.

	pframe->GetJob(pframe->CurrentJobNum);		//Read new job from registry.

	//Set current job and status in the main frame to the new job name.
	pframe->m_pview->m_currentjob=pframe->m_pview->m_status=theapp->jobinfo[pframe->CurrentJobNum].jobname;
	
	//If the job was previously taught reload its info and saved lip patterns.	
	if(theapp->jobinfo[nIndex].taught==true || SavAs==true){ SavAs=false;pframe->SendMessage(WM_RELOAD,nIndex); }
	else  //If job has not been taught show message box saying the job has not been taught.
	{
	//	AfxMessageBox("This Job has not been Taught");
	
		pframe->prompt_code=16;
			Prompt3 promtp3;
			promtp3.DoModal();	
	}

	pframe->SetTimer(31,1000,NULL);//Used to send new job info to PLC

	pframe->SetTimer(37,300,NULL);	//Sends ring light duration information to the PLC.

	//Should not set timer 3 times!
	pframe->SetTimer(42,1100,NULL); //Timer to reset the total bottle count ..For Sunny D .
	pframe->SetTimer(42,1250,NULL);
	pframe->SetTimer(42,1400,NULL);

	//pframe->SendMessage(WM_SETLIGHTLEVEL,theapp->jobinfo[nIndex].lightLevel,1);

	//Update camera gain levels.
	if(!theapp->noCams)
		pframe->m_pcamera->SetLightLevel(theapp->jobinfo[pframe->CurrentJobNum].lightLevel1, theapp->jobinfo[pframe->CurrentJobNum].lightLevel2, theapp->jobinfo[pframe->CurrentJobNum].lightLevel3);
	
	
	if(DoOver==true )//Update camera gain levels.
		pframe->CurrentJobNum=JobOver;	//Set index of newly selected job.

	pframe->m_pxaxis->InvalidateRect(NULL,false);	//Updates xaxis gui.
	theapp->DidTriggerOnce=false;
	pframe->JobLoaded=true;

	//Update tabs for newly loaded job.
	pframe->m_pmod->UpdateDisplay();
	pframe->m_plimit->UpdateDisplay();
	pframe->m_pview->UpdateDisplay();
	pframe->m_pview->UpdateMinor();
	pframe->m_pview2->UpdateDisplay();

	//Tells PLC to use or not use the ring light.
	if(theapp->NoSerialComm==false) 
	{
						
		if(theapp->jobinfo[pframe->CurrentJobNum].useRing==false)
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,260);
			pframe->m_pserial->SendChar(0,260);
		//	CheckDlgButton(IDC_RING,1);
		}
		else
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,270);
			pframe->m_pserial->SendChar(0,270);
	//	CheckDlgButton(IDC_RING,0);
		}

	}

	//Updates the cap width parameter.
	pframe->m_pxaxis->learnedCapWidth=theapp->jobinfo[pframe->CurrentJobNum].taught_cap_width;  //
//	if(theapp->jobinfo[nIndex].taught==true ) {pframe->m_pxaxis->OnLoadPattern(pframe->CurrentJobNum);} // Added by Vivek 0m 08/28/13 ..North East Hotfill in Boston
	/////
	CDialog::OnOK();
}



void Job::OnDatabase() 
{
	
	OnCancel();
}

//NOT USED
void Job::DoEnables()
{

}

//Handler for changing the name of the currently selected job in the jobs list box.
void Job::OnChangename() 
{
	nIndex=m_joblist.GetCaretIndex();
	if(nIndex > 0)
	{

	if(DoOver==false)													//If not duplicating job
		nIndex=m_joblist.GetCaretIndex();								//Get index of job selected in list box.

	//	Get Index Of Currently Selected Job
	//	Get Rename Textbox string and assign to` jobname2 char array string
	m_rename.GetWindowTextA(jname);
	//	Convert char array string to CString
	//	Delete current job name from list box
	m_joblist.DeleteString(nIndex);

	//	Insert new job name into list box
	m_joblist.InsertString(nIndex,jname);

	//	Clear Rename textbox
	m_renamejob.Format("%s","");

	LONG res;
//	res=RegDeleteTree(HKEY_CURRENT_USER,"Software\\Silgan Closures\\Job Array\\Job050");
//	DelRegTree(HKEY_CURRENT_USER,"Software\\Silgan Closures\\Job Array\\Job050");
	
	//Set current job and status in the main frame to the new job name.
	pframe->m_pview->m_currentjob=pframe->m_pview->m_status=theapp->jobinfo[nIndex].jobname=jname;
	//save job name to registry

	char inspnum[10];
	char folderName[255];	//save job name to registry

	sprintf(folderName, "JobArray\\Job%03d", nIndex);
	theapp->WriteProfileString(folderName, "jobname", theapp->jobinfo[nIndex].jobname);

	//LoadJobs();										//Reload jobs
	m_joblist.SetCaretIndex(nIndex);				//Update the list box to show the renamed job as the currently selected job.
	NameChanged=true;
	UpdateData(false); 
	}
	//Sleep(1000);
	//OnOK();
}

//Responds to duplicate job dialog box. Used for duplicating job.
void Job::OnTimer(UINT nIDEvent) 
{
	/*
	if (nIDEvent==1) 
	{
		KillTimer(1);
		nIndex=JobOver;
		DoOver=true;
 		int n = m_joblist.GetTextLen( original );

		m_joblist.GetText( original, str.GetBuffer(n) );
		m_joblist.GetText( original, Value );
		str.TrimRight("  ");
		
		
		strcpy(newjobname,Value);
		//m_renamejob.Format("%s",str);

		//load the original first
		pframe->GetJob(original);

		//m_redit.GetLine(0,jobname);
		m_joblist.DeleteString(nIndex);
		m_joblist.InsertString(nIndex,newjobname);
		m_renamejob.Format("%s",newjobname);
		NameChanged=true;						 //not being used anywhere

		pframe->m_pview->m_currentjob=pframe->m_pview->m_status=theapp->jobinfo[pframe->CurrentJobNum].jobname=CString(str);

		m_joblist.SetCurSel(nIndex);

		CopyFile(original, nIndex); //very imp function

		UpdateData(false);
	}		
	*/

	//VK Re doing above code
	if (nIDEvent==1) 
	{
		KillTimer(1);
		if(( source_job_index>0) && (source_job_index<=50) && (dest_job_index >0) && (dest_job_index<=50))
		{
			CString job_name;
			
			m_joblist.GetText(source_job_index,job_name);	

			m_joblist.DeleteString(dest_job_index);
			m_joblist.InsertString((dest_job_index),job_name);//index is 0 based in the list box //InsertString() doesn't look like its 0 indexed

			pframe->m_pview->m_currentjob=pframe->m_pview->m_status=theapp->jobinfo[dest_job_index].jobname=job_name;
			m_joblist.SetCurSel(source_job_index); 

//			theapp->DeleteRegKey(50);
			CopyFile(source_job_index, dest_job_index); //very imp function

			theapp->jobinfo[dest_job_index].taught=true;
			pframe->SaveJob(dest_job_index);

		}
		else
		{AfxMessageBox(" Select a valid source and destination job from the list");}
	
	
		UpdateData(false);
	}	



	CDialog::OnTimer(nIDEvent);
}

//Copies job parameters from original to duplicate.
void Job::CopyFile(int Old, int New)
{

//strcpy(newjobname,theapp->jobinfo[Old].jobname);
	//strcat(newjobname,"_copy");
	
//	theapp->jobinfo[New].jobname=CString(newjobname);

	char buffer[20];
	CString a=" copy ";
	CString Space=" ";
	CString OldJobNum=itoa(Old,buffer,10);
	
	CString currentpath = "c:\\silgandata\\pat\\";

	CString OldFile=OldJobNum+"\\"+OldJobNum+".bmp";
	CString OldLPattern=currentpath+OldJobNum+"\\Left.bmp";
	CString OldRPattern=currentpath+OldJobNum+"\\Right.bmp";

	CString NewJobNum=itoa(New,buffer,10);
	CString NewFile=NewJobNum+"\\"+NewJobNum+".bmp";
	CString NewLPattern=currentpath+NewJobNum+"\\Left.bmp";
	CString NewRPattern=currentpath+NewJobNum+"\\Right.bmp";



	//Not very clean..need to revisit & re do better
	CString c=a+currentpath+OldFile +Space+currentpath+NewFile;
	system(c);
	CString d=a+OldLPattern+Space+NewLPattern;
	system(d);
	CString e=a+OldRPattern+Space+NewRPattern;
	system(e);

//	system("xcopy OldFile NewFile /E ");
//	system("xcopy  OldLPattern  NewLPattern  /E ");
//	system("xcopy OldRPattern NewRPattern /E ");
	
 
	theapp->jobinfo[New].lightLevel1=theapp->jobinfo[Old].lightLevel1;
	theapp->jobinfo[New].lightLevel2=theapp->jobinfo[Old].lightLevel2;
	theapp->jobinfo[New].lightLevel3=theapp->jobinfo[Old].lightLevel3;

	
	theapp->jobinfo[New].is_diet_coke=theapp->jobinfo[Old].is_diet_coke;

	theapp->jobinfo[New].vertBarX=theapp->jobinfo[Old].vertBarX;

	theapp->jobinfo[New].midBarY=theapp->jobinfo[Old].midBarY;
	theapp->jobinfo[New].neckBarY=theapp->jobinfo[Old].neckBarY;
	
	theapp->jobinfo[New].lDist=theapp->jobinfo[Old].lDist;

	theapp->jobinfo[New].tBandX=theapp->jobinfo[Old].tBandX;

	theapp->jobinfo[New].tBandY=theapp->jobinfo[Old].tBandY;

	theapp->jobinfo[New].tBandDx=theapp->jobinfo[Old].tBandDx;

	theapp->jobinfo[New].tBandDy=theapp->jobinfo[Old].tBandDy;

	theapp->jobinfo[New].tBandThresh=theapp->jobinfo[Old].tBandThresh;

	theapp->jobinfo[New].tBandSurfThresh=theapp->jobinfo[Old].tBandSurfThresh;

	theapp->jobinfo[New].tBandSurfThresh2=theapp->jobinfo[Old].tBandSurfThresh2;

	theapp->jobinfo[New].tBand2X=theapp->jobinfo[Old].tBand2X;
	theapp->jobinfo[New].tBand2Y=theapp->jobinfo[Old].tBand2Y;
	theapp->jobinfo[New].tBand2Dx=theapp->jobinfo[Old].tBand2Dx;

	theapp->jobinfo[New].rearCamDy=theapp->jobinfo[Old].rearCamDy;
	
	theapp->jobinfo[New].rearCamY=theapp->jobinfo[Old].rearCamY;
	theapp->jobinfo[New].lSWin=theapp->jobinfo[Old].lSWin;

	theapp->jobinfo[New].is_NeckBarY_offset_negative=theapp->jobinfo[Old].is_NeckBarY_offset_negative;
 
	theapp->jobinfo[New].neckBarY_offset=theapp->jobinfo[Old].neckBarY_offset;



	///////

	theapp->jobinfo[New].taught=theapp->jobinfo[Old].taught; //Not 

	theapp->jobinfo[New].taught_cap_width=theapp->jobinfo[Old].taught_cap_width;

	theapp->jobinfo[New].Rear_ht_Diff=theapp->jobinfo[Old].Rear_ht_Diff;

	theapp->jobinfo[New].enable_foil_sensor=theapp->jobinfo[Old].enable_foil_sensor;

	theapp->jobinfo[New].cam1_ul=theapp->jobinfo[Old].cam1_ul;
	theapp->jobinfo[New].cam1_ll=theapp->jobinfo[Old].cam1_ll;

	theapp->jobinfo[New].cam2_ul=theapp->jobinfo[Old].cam2_ul;
	theapp->jobinfo[New].cam2_ll=theapp->jobinfo[Old].cam2_ll;

	theapp->jobinfo[New].cam3_ul=theapp->jobinfo[Old].cam3_ul;			
	theapp->jobinfo[New].cam3_ll=theapp->jobinfo[Old].cam3_ll;

	theapp->jobinfo[New].do_sobel=theapp->jobinfo[Old].do_sobel;
	theapp->jobinfo[New].etb_cam1=theapp->jobinfo[Old].etb_cam1;
	theapp->jobinfo[New].etb_cam2=theapp->jobinfo[Old].etb_cam2;
	theapp->jobinfo[New].etb_cam3=theapp->jobinfo[Old].etb_cam3;

	theapp->jobinfo[New].bottleReferenceBar=theapp->jobinfo[Old].bottleReferenceBar;
	theapp->jobinfo[New].bottleReferenceBar_cam2=theapp->jobinfo[Old].bottleReferenceBar_cam2;
	theapp->jobinfo[New].bottleReferenceBar_cam3=theapp->jobinfo[Old].bottleReferenceBar_cam3;



	theapp->jobinfo[New].rSWin=theapp->jobinfo[Old].rSWin;
	theapp->jobinfo[New].is_fillX_neg=theapp->jobinfo[Old].is_fillX_neg;
	theapp->jobinfo[New].fillX=theapp->jobinfo[Old].fillX;
	theapp->jobinfo[New].fillY=theapp->jobinfo[Old].fillY;
	
		//
	theapp->jobinfo[New].userStrength=theapp->jobinfo[Old].userStrength;
	theapp->jobinfo[New].userX=theapp->jobinfo[Old].userX;
	theapp->jobinfo[New].userY=theapp->jobinfo[Old].userY;

	theapp->jobinfo[New].userDx=theapp->jobinfo[Old].userDx;
	theapp->jobinfo[New].active=theapp->jobinfo[Old].active;
	theapp->jobinfo[New].u2Pos=theapp->jobinfo[Old].u2Pos;

	///

	theapp->jobinfo[New].u2Thresh2=theapp->jobinfo[Old].u2Thresh2;
	theapp->jobinfo[New].u2Thresh1=theapp->jobinfo[Old].u2Thresh1;
	theapp->jobinfo[New].u2XOffset=theapp->jobinfo[Old].u2XOffset;
	theapp->jobinfo[New].u2YOffset=theapp->jobinfo[Old].u2YOffset;

	theapp->jobinfo[New].u2XOffsetR=theapp->jobinfo[Old].u2XOffsetR;
	theapp->jobinfo[New].u2YOffsetR=theapp->jobinfo[Old].u2YOffsetR;

	theapp->jobinfo[New].profile=theapp->jobinfo[Old].profile;
	theapp->jobinfo[New].tBandType=theapp->jobinfo[Old].tBandType;
	theapp->jobinfo[New].sensitivity=theapp->jobinfo[Old].sensitivity;

	theapp->jobinfo[New].enable=theapp->jobinfo[Old].enable;
	theapp->jobinfo[New].dark=theapp->jobinfo[Old].dark;

	theapp->jobinfo[New].motPos=theapp->jobinfo[Old].motPos;
	theapp->jobinfo[New].lip=theapp->jobinfo[Old].lip;
	theapp->jobinfo[New].lipx=theapp->jobinfo[Old].lipx;
	theapp->jobinfo[New].lipy=theapp->jobinfo[Old].lipy;
	theapp->jobinfo[New].lipYOffset=theapp->jobinfo[Old].lipYOffset;
	theapp->jobinfo[New].lipSize=theapp->jobinfo[Old].lipSize;

	theapp->jobinfo[New].rBandOffset[1]=theapp->jobinfo[Old].rBandOffset[1];
	theapp->jobinfo[New].lBandOffset[1]=theapp->jobinfo[Old].lBandOffset[1];
	theapp->jobinfo[New].rBandOffset[2]=theapp->jobinfo[Old].rBandOffset[2];
	theapp->jobinfo[New].lBandOffset[2]=theapp->jobinfo[Old].lBandOffset[2];
	theapp->jobinfo[New].rBandOffset[3]=theapp->jobinfo[Old].rBandOffset[3];
	theapp->jobinfo[New].lBandOffset[3]=theapp->jobinfo[Old].lBandOffset[3];
	theapp->jobinfo[New].lBandOffset[4]=theapp->jobinfo[Old].lBandOffset[4];
	theapp->jobinfo[New].rBandOffset[4]=theapp->jobinfo[Old].rBandOffset[4];
	theapp->jobinfo[New].rBandOffset[5]=theapp->jobinfo[Old].rBandOffset[5];
	theapp->jobinfo[New].lBandOffset[5]=theapp->jobinfo[Old].lBandOffset[5];

	theapp->jobinfo[New].rearSurfThresh=theapp->jobinfo[Old].rearSurfThresh;
	theapp->jobinfo[New].capWidthLimit=theapp->jobinfo[Old].capWidthLimit;
	theapp->jobinfo[New].colorTargR=theapp->jobinfo[Old].colorTargR;
	theapp->jobinfo[New].colorTargG=theapp->jobinfo[Old].colorTargG;
	theapp->jobinfo[New].colorTargB=theapp->jobinfo[Old].colorTargB;

	theapp->jobinfo[New].rightBandShift=theapp->jobinfo[Old].rightBandShift;
	theapp->jobinfo[New].leftBandShift=theapp->jobinfo[Old].leftBandShift;
	theapp->jobinfo[New].band2Size=theapp->jobinfo[Old].band2Size;
	theapp->jobinfo[New].bandColor=theapp->jobinfo[Old].bandColor;
	theapp->jobinfo[New].sport=theapp->jobinfo[Old].sport;

	theapp->jobinfo[New].userType=theapp->jobinfo[Old].userType;
	theapp->jobinfo[New].neckOffset=theapp->jobinfo[Old].neckOffset;
	theapp->jobinfo[New].ringLight=theapp->jobinfo[Old].ringLight;
	theapp->jobinfo[New].backLight=theapp->jobinfo[Old].backLight;
	theapp->jobinfo[New].sportPos=theapp->jobinfo[Old].sportPos;

	theapp->jobinfo[New].sportSen=theapp->jobinfo[Old].sportSen;
	theapp->jobinfo[New].xtraLight=theapp->jobinfo[Old].xtraLight;
	theapp->jobinfo[New].redCutOff=theapp->jobinfo[Old].redCutOff;
	theapp->jobinfo[New].fillArea=theapp->jobinfo[Old].fillArea;
	theapp->jobinfo[New].snapDelay=theapp->jobinfo[Old].snapDelay;

	theapp->jobinfo[New].slatPW=theapp->jobinfo[Old].slatPW;
	theapp->jobinfo[New].encDur=theapp->jobinfo[Old].encDur;
	theapp->jobinfo[New].encDist=theapp->jobinfo[Old].encDist;
	theapp->jobinfo[New].diff_for_no_cap=theapp->jobinfo[Old].diff_for_no_cap;
	theapp->jobinfo[New].limit_no_cap=theapp->jobinfo[Old].limit_no_cap;


	///
	theapp->jobinfo[New].useLeft=theapp->jobinfo[Old].useLeft;
	theapp->jobinfo[New].useRing=theapp->jobinfo[Old].useRing;
	theapp->jobinfo[New].ScaleFactor=theapp->jobinfo[Old].ScaleFactor;

	theapp->jobinfo[New].totinsp=theapp->jobinfo[Old].totinsp;

	char inspnum[10];
	char folderName[255];
	

	sprintf(folderName, "JobArray\\Job%03d", New);
	std::string abc(folderName);

	for (int curInspctn=1; curInspctn<=theapp->jobinfo[New].totinsp; curInspctn++)//theapp->jobinfo[jobnum].totinsp
	{		
		sprintf(inspnum, "\\Insp%03d", curInspctn);
	//	strcat(folderName,inspnum);

		abc=abc+inspnum;

		theapp->WriteProfileString(abc.c_str(), "name", theapp->inspctn[curInspctn].name);
		theapp->WriteProfileInt(abc.c_str(), "lmin", theapp->inspctn[curInspctn].lmin);
		theapp->WriteProfileInt(abc.c_str(), "lmax", theapp->inspctn[curInspctn].lmax);
		theapp->WriteProfileInt(abc.c_str(), "enable", theapp->inspctn[curInspctn].enable);

		int n=abc.length();
		abc=abc.substr(0,n-8);
	}	

	pframe->SaveJob(Old);
}

// Letter button handlers
void Job::OnA() 
{
	UpdateGUIString(Value += "A");
}

void Job::OnB() 
{
	UpdateGUIString(Value += "B");
}

void Job::OnC() 
{
	UpdateGUIString(Value += "C");
}

void Job::OnD() 
{
	UpdateGUIString(Value += "D");	
}

void Job::OnE() 
{
	UpdateGUIString(Value += "E");
}

void Job::OnF() 
{
	UpdateGUIString(Value += "F");	
}

void Job::OnG() 
{
	UpdateGUIString(Value += "G");
}

void Job::OnH() 
{
	UpdateGUIString(Value += "H");
}

void Job::OnI() 
{
	UpdateGUIString(Value += "I");
}

void Job::OnJ() 
{
	UpdateGUIString(Value += "J");
}

void Job::OnK() 
{
	UpdateGUIString(Value += "K");
}

void Job::OnL() 
{
	UpdateGUIString(Value += "L");
}

void Job::OnM() 
{
	UpdateGUIString(Value += "M");
}

void Job::OnN() 
{
	UpdateGUIString(Value += "N");
}

void Job::OnO() 
{
	UpdateGUIString(Value += "O");
}

void Job::OnP() 
{
	UpdateGUIString(Value += "P");
}

void Job::OnQ() 
{
	UpdateGUIString(Value += "Q");
}

void Job::OnR() 
{
	UpdateGUIString(Value += "R");
}

void Job::OnS() 
{
	UpdateGUIString(Value += "S");
}

void Job::OnT() 
{
	UpdateGUIString(Value += "T");
}

void Job::OnU() 
{
	UpdateGUIString(Value += "U");
}

void Job::OnV() 
{
	UpdateGUIString(Value += "V");
}

void Job::OnW() 
{
	UpdateGUIString(Value += "W");
}

void Job::OnX() 
{
	UpdateGUIString(Value += "X");
}

void Job::OnY() 
{
	UpdateGUIString(Value += "Y");
}

void Job::OnZ() 
{
	UpdateGUIString(Value += "Z");
}

void Job::OnDel() 
{
	UpdateGUIString(Value = Value.Left(Value.GetLength() - 1));	
}

void Job::OnDash() 
{
	UpdateGUIString(Value += "-");
}

void Job::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

// Number button handlers
void Job::OnJ0() 
{
	UpdateGUIString(Value += "0");
}

void Job::OnJ1() 
{
	UpdateGUIString(Value += "1");
}

void Job::OnJ2() 
{
	UpdateGUIString(Value += "2");
}

void Job::OnJ3() 
{
	UpdateGUIString(Value += "3");
}

void Job::OnJ4() 
{
	UpdateGUIString(Value += "4");
}

void Job::OnJ5() 
{
	UpdateGUIString(Value += "5");
}

void Job::OnJ6() 
{
	UpdateGUIString(Value += "6");	
}

void Job::OnJ7() 
{
	UpdateGUIString(Value += "7");	
}

void Job::OnJ8() 
{
	UpdateGUIString(Value += "8");	
}

void Job::OnJ9() 
{
	UpdateGUIString(Value += "9");
}

void Job::UpdateGUIString(CString c)
{
	m_renamejob.Format("%s", Value);	//Update variable holding job name
	UpdateData(false);	//Update dialog gui data
}

//Opens a dialog box for duplicating jobs.
void Job::OnSaveas() 
{
	source_job_index=m_joblist.GetCaretIndex();
	SaveAs saveas;
	saveas.DoModal();	
}

//Handler for deletion of a job in the jobs list box.
void Job::OnDeleteJob() 
{
	CString jobname2;
	char folderName[255];	//Stores registry folder name and path for deleted job. Used to remove job from registry

	pframe->CurrentJobNum=nIndex=m_joblist.GetCaretIndex();

	if(nIndex!=0)		//First position is used to display the title string, "Job List" so it should not be modified.
	{
		m_joblist.DeleteString(nIndex);							//Remove string at currently selected index in list box.
		m_joblist.InsertString(nIndex,"xxx");					//Replace with "xxx"	
		theapp->jobinfo[nIndex].jobname = "xxx";

		m_rename.SetWindowText("");								//Clear rename edit box which displays the currently selected jobs name.

		CString jobname;
		jobname.Format(_T("Job%03d"), nIndex);

		theapp->DeleteKey(nIndex);
		if(nIndex == pframe->CurrentJobNum)							//If deleted job is selected in the app
		{
			pframe->CurrentJobNum = 0;
			m_joblist.SetCaretIndex(0);
			//pframe->GetJob(nIndex);								//Load job from registry
			pframe->m_pview->m_currentjob="xxx";							//Update main frame's view variables for current job.
			pframe->m_pview->m_status="Job xxx reloaded";					//Update the status text in the main frame to indicate that the selected job has been deleted.
		//	pframe->m_psetuptab->UpdateDisplay();
		}
	}
	else	//First position is used to display the title string "Job List" so it should not be modified
	{	
		//AfxMessageBox("Select a Job first");
			pframe->prompt_code=15;
			Prompt3 promtp3;
			promtp3.DoModal();	
	}	
}
