#if !defined(AFX_ENHANCEDTB_H__124DA8F1_0FD2_45AD_95B9_60CA14E53561__INCLUDED_)
#define AFX_ENHANCEDTB_H__124DA8F1_0FD2_45AD_95B9_60CA14E53561__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EnhancedTB.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// EnhancedTB dialog

class EnhancedTB : public CDialog
{
friend class Lim;
friend class CMainFrame;
friend class CCapApp;
friend class XAxisView;
friend class Modify;
// Construction
public:
	void UpdateDisplay();
	EnhancedTB(CWnd* pParent = NULL);   // standard constructor

	CMainFrame* pframe;
	CCapApp *theapp;
	XAxisView* m_pxaxis;

		int step_size;

// Dialog Data
	//{{AFX_DATA(EnhancedTB)
	enum { IDD = IDD_EnhancedTB };
	CButton	m_button13;
	CButton	m_check8;
	CButton	m_check7;
	CButton	m_check66;
	CString	m_edit1;
	CString	m_edit2;
	CString	m_edit5;
	CString	m_edit6;
	CString	m_edit7;
	CString	m_edit8;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EnhancedTB)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(EnhancedTB)
	virtual void OnOK();
	afx_msg void OnCam1LLincre();
	afx_msg void OnCam1ULincre();
	afx_msg void OnCam2LLincre();
	afx_msg void OnCam3LLincre();
	afx_msg void OnCam2ULincre();
	afx_msg void OnCam3ULincre();
	afx_msg void OnCam1LLdcr();
	afx_msg void OnCam2LLdcr();
	afx_msg void OnCam3LLdcr();
	afx_msg void OnCam1ULdcr();
	afx_msg void OnCam2ULdcr();
	afx_msg void OnCam3ULdcr();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheck6();
	afx_msg void OnCheck7();
	afx_msg void OnCheck8();
	afx_msg void OnCancelMode();
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg void OnClose();
	afx_msg void OnRadio1();
	afx_msg void OnButton13();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENHANCEDTB_H__124DA8F1_0FD2_45AD_95B9_60CA14E53561__INCLUDED_)
