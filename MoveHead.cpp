// MoveHead.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "MoveHead.h"
#include "Serial.h"
#include "mainfrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MoveHead dialog


MoveHead::MoveHead(CWnd* pParent /*=NULL*/)
	: CDialog(MoveHead::IDD, pParent)
{
	//{{AFX_DATA_INIT(MoveHead)
	m_mpos = _T("");
	m_mpos2 = _T("");
	//}}AFX_DATA_INIT
}


void MoveHead::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MoveHead)
	DDX_Text(pDX, IDC_EDITPOS, m_mpos);
	DDX_Text(pDX, IDC_EDITPOS2, m_mpos2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MoveHead, CDialog)
	//{{AFX_MSG_MAP(MoveHead)
	ON_BN_CLICKED(IDC_START, OnStart)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MOTORUP, OnMotorup)
	ON_BN_CLICKED(IDC_MOTORDN, OnMotordn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MoveHead message handlers

BOOL MoveHead::OnInitDialog() 
{
	CDialog::OnInitDialog();
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pmovehead=this;
	theapp=(CCapApp*)AfxGetApp();
	pframe->MotPos=theapp->jobinfo[pframe->CurrentJobNum].motPos;	//Load saved motor position for selected job.

	if(theapp->NoSerialComm==false) 
	{
		MotorPos=pframe->m_pserial->MotorPos;	//Get current motor position.
	if(MotorPos<=0) MotorPos=pframe->m_pserial->MotorPos=0;
	}
	//pframe->SetTimer(8,100,NULL);//com
	m_mpos.Format("%i",MotorPos);				//Load current motor position into display variable
	m_mpos2.Format("%i",pframe->MotPos);		//Load saved motor position into display variable
	UpdateData(false);
	
	//Not used
	Value=0;
	Function=0;
	CntrBits=0;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Move motor to saved position
void MoveHead::OnStart() 
{
	count=0;	//Reset attempt count for trying to reach the saved motor position.
	SetTimer(1,1500,NULL);	//Set timer which will call functions which will try to reach the saved motor position.
	
	//Below a first attempt is made to reach the saved motor position.
	if(theapp->NoSerialComm==false) 
	{					
		MotorPos=pframe->m_pserial->MotorPos;	//Get current motor position
	}

	if(MotorPos==0)		//If current motor position is 0
		OnHighSlow();	//Move up slowly
	
	if(MotorPos > pframe->MotPos)//If current motor position is greater then the saved motor position
		OnHighSlow();	//Move up slowly

	if(MotorPos < pframe->MotPos) //If current motor position is less then the saved motor position
		OnLowSlow();	//Move down slowly

	if(MotorPos == pframe->MotPos) //If current motor position is equal to the saved motor position
		KillTimer(1);//Stop moving.

	m_mpos.Format("%i",MotorPos);			//Update current motor position
	m_mpos2.Format("%i",pframe->MotPos);	//Update target motor position
	UpdateData(false);	
}

//Used to to move the motor from its current position to the position saved for the job.
void MoveHead::CheckMotorPos()
{
	if(theapp->NoSerialComm==false) 
	{				
		MotorPos=pframe->m_pserial->MotorPos;	//Get current motor position from PLC
	}
	if(MotorPos==0)	//If current motor position is 0.
	{
		OnHighSlow();	//Move up slowly
	}
	else
	{
		if(MotorPos > pframe->MotPos) //If current motor position is above saved motor position
		{
			if(MotorPos-10 > pframe->MotPos) 	//If current motor position is far from saved motor position.
			{
				OnHigh();	//Move up 
			}
			else
			{
				OnHighSlow();	//Move up slowly
			}
		}

		if(MotorPos < pframe->MotPos)	//If current motor position is below saved motor position
		{
			if(MotorPos+10 < pframe->MotPos) //If current motor position is far from saved motor position.
			{
				OnLow();	//Move down
			}
			else
			{
				OnLowSlow();	//Move down slowly
			}
		}
	}

	if(MotorPos  == pframe->MotPos)//If current motor position equal to saved motor position
		KillTimer(1);	//Stop trying to reach saved motor position.

	//If 60 attempts were made to move the motor to the saved position and we are still not successful then stop trying.
	if(count>=60) KillTimer(1);
	count+=1;	//increment move to saved motor position attempt count.

	m_mpos.Format("%i",MotorPos );			//Update current motor position.
	m_mpos2.Format("%i",pframe->MotPos);	//Update saved motor position.
	UpdateData(false);

}

//Stop motor after start motor has been pressed, does not affect motion due to other button presses.
void MoveHead::OnStop() 
{
	KillTimer(1);
	
}

//Move motor up.
void MoveHead::OnHigh() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,240);
		pframe->m_pserial->SendChar(0,240);
	}
	SetTimer(3,200,NULL);

	UpdateData(false);
	
}

//Move motor down
void MoveHead::OnLow() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,250);
		pframe->m_pserial->SendChar(0,250);
	}
	SetTimer(3,200,NULL);

	UpdateData(false);
	
}

//Move motor up slowly
void MoveHead::OnHighSlow() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,130);
		pframe->m_pserial->SendChar(0,130);
	}
	SetTimer(3,200,NULL);//going up

	UpdateData(false);
	
}

//Move motor down slowly
void MoveHead::OnLowSlow() 
{
	if(theapp->NoSerialComm==false) 
	{
						
		//pframe->m_pserial->SendMessage(WM_QUE,0,140);
		pframe->m_pserial->SendChar(0,140);
	}
	SetTimer(3,200,NULL);

	UpdateData(false);
	
}

void MoveHead::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)	//Used when motor needs to be moved from current position to saved position for the job.
	{
		CheckMotorPos();
	}

	if(nIDEvent==3) //Used in combination with timer ID 5 to update the current motor position display after the motor has been moved.
	{
		KillTimer(3);
		SetTimer(5,300,NULL);
		
	}
	if(nIDEvent==5)//Used in combination with timer ID 3 to update the current motor position display after the motor has been moved.
	{
		KillTimer(5);
		if(theapp->NoSerialComm==false) 
		{
			MotorPos=pframe->m_pserial->MotorPos;	//Get current motor position.
		}
		if(MotorPos==0) SetTimer(5,5,NULL);
		m_mpos.Format("%i",MotorPos);	//Update gui variable for motor position.
		pframe->SendMessage(WM_TRIGGER,NULL,NULL);	//Trigger cameras?
		UpdateData(false);	//Update gui
	}
	CDialog::OnTimer(nIDEvent);
}

void MoveHead::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

//Move motor up, handler for up button on gui.
void MoveHead::OnMotorup() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,130);
		pframe->m_pserial->SendChar(0,130);		//Send command to PLC to raise the motor.
	}

	SetTimer(3,200,NULL);	//Set timer to update the display with the motor position after the motion is done.

	UpdateData(false);		//Update gui
}

//Move motor down, handler for down button on gui.
void MoveHead::OnMotordn() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,140);
		pframe->m_pserial->SendChar(0,140);		//Send command to PLC to lower the motor.
	}

	SetTimer(3,200,NULL);	//Set timer to update the display with the motor position after the motion is done.

	UpdateData(false);	
}
