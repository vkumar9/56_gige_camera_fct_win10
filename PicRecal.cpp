// PicRecal.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "PicRecal.h"
#include "MainFrm.h"
#include "XAxisView.h"
#include "Serial.h"
#include "Prompt3.h"

#include <iostream>
#include <sstream>


using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// PicRecal dialog


PicRecal::PicRecal(CWnd* pParent /*=NULL*/)
	: CDialog(PicRecal::IDD, pParent)
{
	//{{AFX_DATA_INIT(PicRecal)
	m_edit1 = _T("");
	m_countpics = _T("");
	//}}AFX_DATA_INIT
}


void PicRecal::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PicRecal)
	DDX_Control(pDX, IDC_LIST2, m_list_box);
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_count_pics, m_countpics);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PicRecal, CDialog)
	//{{AFX_MSG_MAP(PicRecal)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON1, OnReset)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
//	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	ON_BN_CLICKED(IDC_SaveToDisk, OnSaveToDisk)
	ON_LBN_SELCHANGE(IDC_LIST2, OnSelchangeList2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PicRecal message handlers

BOOL PicRecal::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_picrecall=this;
	theapp=(CCapApp*)AfxGetApp();

	pframe->m_pxaxis->showostore=1;

	//Allocate memory for saving 3 camera images per defective bottle, used when save to disk is pressed.
	//Initializes values to 0.
	//Memory is fixed.
	imgBuf_def=(BYTE*)GlobalAlloc(GMEM_FIXED |GMEM_ZEROINIT,(theapp->cam1Width/2)*(theapp->cam1Height/2)*4 );
	imgBuf_def2=(BYTE*)GlobalAlloc(GMEM_FIXED |GMEM_ZEROINIT,(theapp->cam2Width/2)*(theapp->cam2Height/2)*4 );
	imgBuf_def3=(BYTE*)GlobalAlloc(GMEM_FIXED |GMEM_ZEROINIT,(theapp->cam3Width/2)*(theapp->cam3Height/2)*4 );

	//Initialize listbox which holds time stamps of defective bottle detection.
	//From here
	char abc1[100];		//Does not appear to be used.
  
	//For last 500 defective bottles.
	for(int i=499;i>=0;i--)
	{
		stringstream ss (stringstream::in | stringstream::out);

		if(pframe->m_pxaxis->tim_stamp[i][0] !=0 )
		{
		
			/*
			ss << pframe->m_pxaxis->tim_stamp[i][1]; // Hour
			ss<<":";
			ss <<pframe->m_pxaxis->tim_stamp[i][2];  // Min
			ss<<":";
			ss << pframe->m_pxaxis->tim_stamp[i][3];  // Sec
			ss<<":";
			ss << pframe->m_pxaxis->tim_stamp[i][4];  // MSec
			*/
			ss.fill('0');		//pad with zeros
			ss.width(2);		//2 characters wide 
			ss << pframe->m_pxaxis->tim_stamp[i][1]; // Hour
			ss<<" : ";
			ss.fill('0');ss.width(2);
			ss <<pframe->m_pxaxis->tim_stamp[i][2];  // Min
			ss<<" : ";
			ss.fill('0');ss.width(2);
			ss << pframe->m_pxaxis->tim_stamp[i][3];  // Sec
			ss<<" : ";
			ss.fill('0');ss.width(3);
			ss << pframe->m_pxaxis->tim_stamp[i][4];  // MSec
	
			
			CString abc(ss.str().c_str()); //Get text and convert from C string to C++ string

			//	m_pcombo->AddString(abc);
			m_list_box.AddString(abc);	//add string to list box.
			
		}
		//To here
	}

	count=0;			//Does not appear to be used.	
	reasnofailur=0;

	//Keeping track of the last memory address retained by these pointers
	arr_strng_addrs[0]=pframe->m_pxaxis->imgBuf3RGB_cam1; //Keeping track of the last memory address retained by these pointers
	arr_strng_addrs[1]=pframe->m_pxaxis->imgBuf3RGB_cam2;
	arr_strng_addrs[2]=pframe->m_pxaxis->imgBuf3RGB_cam3;

//	m_pcombo->SetCurSel(0); // Used to Initialise the Dialog to displaying the 1st Picture

	m_list_box.SetCurSel(0);		//Select last image which was in error.
//	OnSelchangeList1();
	InvalidateRect(NULL,false);		//Repaint dialog box.

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PicRecal::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	CRect rect;
	std::string str1;	//Holds string which contains all of the failure reasons for the selected failed bottle.
    
  	if(pframe->CurrentJobNum >=1)	//Check if proper item is selected in jobs dialog.
	{

		count++;		//Does not appear to be used.

		//Not used, overridden by following line.
		int nIndex = m_list_box.GetCurSel();	//Get index of currently selected defect time stamp, 

		nIndex=pframe->m_pxaxis->imge_no-nIndex-1; //this is the revised INdex Number

		//int nCount = m_ListBox.GetCount();

		if ((nIndex != CB_ERR) &&(nIndex >= 0) ) // Make sure a defect time stamp is selected.
		{
			//Compute and set index of currently selected defective image.
			pframe->m_pxaxis->imgBuf3RGB_cam1 = 
				                  //Base of image array  +   Index of selected defective bottle     multiplied by
				pframe->m_pxaxis->  arr1[0]              +  (nIndex                                 *
				// size of image                               multiplied by 4, 4 color channels per pixel: RGB and transparency
				((theapp->cam1Width/2)*(theapp->cam1Height/2)  *4));

			pframe->m_pxaxis->imgBuf3RGB_cam2=pframe->m_pxaxis->arr1[1]+(nIndex*((theapp->cam2Width/2)*(theapp->cam2Height/2)*4));
				
			pframe->m_pxaxis->imgBuf3RGB_cam3=pframe->m_pxaxis->arr1[2]+(nIndex*((theapp->cam3Width/2)*(theapp->cam3Height/2)*4));
			//Constructing 3 RGBAlpha images
											        //width              , height    
			CVisRGBAByteImage im_FileBW3_cam1(theapp->cam1Width/2,theapp->cam1Height/2,
											//number of bands,    , pixel data
											  1,                    -1, pframe->m_pxaxis->imgBuf3RGB_cam1);
			CVisRGBAByteImage im_FileBW3_cam2(theapp->cam2Width/2,theapp->cam2Height/2, 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam2);
			CVisRGBAByteImage im_FileBW3_cam3(theapp->cam3Width/2,theapp->cam3Height/2, 1, -1, pframe->m_pxaxis->imgBuf3RGB_cam3);


		//	reasnofailur=*(pframe->m_pxaxis->imgBuf3RGB_cam1);

			//!!!!!!inputting new format of reading and displaying all inspections it failed on
			int count=*(pframe->m_pxaxis->imgBuf3RGB_cam1+2);

			for(int i=1;i<=count;i++)	//For each type of inspection failure
			{
				//Get value pointed to by imgBuf3RGB_cam1+4 bytes + 2*i bytes. This is the index of inspection type which failed. 
				//Every two bytes stores another inspection type.
				reasnofailur=*(pframe->m_pxaxis->imgBuf3RGB_cam1 + 4 +(2*i));
				str1=str1+UpdateDisplay()+"\r\n";	//Write to edit box	
			}
			
			//!!!!!

			//Display cam1 image
			CVisImageBase& refimage1=im_FileBW3_cam1;
			dc.SetWindowOrg(-60,0);////(0,-10);
			assert(refimage1.IsValid());
			refimage1.DisplayInHdc(dc); 


			//Display cam2 image
			dc.SetWindowOrg(0,-260);
			CVisImageBase& refimage2=im_FileBW3_cam2;
			assert(refimage2.IsValid());
			refimage2.DisplayInHdc(dc); 


			//Display cam3 image
			dc.SetWindowOrg(-320,-260);
			CVisImageBase& refimage3=im_FileBW3_cam3;
			assert(refimage3.IsValid());
			refimage3.DisplayInHdc(dc); 

		}

		if(count == 500){count =0;}
	}
	
	CString abc(str1.c_str());
	m_edit1.Format("%s",abc);
	UpdateData(false);
}


//Updates the listbox which shows reasons for failure.
std::string PicRecal::UpdateDisplay()
{
	m_countpics.Format("%2i",count);

	switch(reasnofailur)
	{
		case 1:m_edit1.Format("Left Height");
				temp_str= "Left Height";
				break;

		case 2:m_edit1.Format("Right Height");
				temp_str= "Right Height";
				break;
		
		case 3:m_edit1.Format("Mid Height");
				temp_str= "Mid Height";
				break;

		case 4:m_edit1.Format("Cocked");
				temp_str= "Cocked";
				break;

		case 5:m_edit1.Format("Band Edges");
				temp_str= "Band Edges";
				break;

		case 6:m_edit1.Format("Band Surface");
				temp_str= "Band Surface";
				break;

		case 7:m_edit1.Format("Low Fill Level");
				temp_str= "Low Fill Level";
				break;

		case 8:m_edit1.Format("Color");
				temp_str= "Color";
				break;

		case 9:m_edit1.Format("User 9");
				temp_str= "User 9";
				break;
		case 10:m_edit1.Format("User 10");
				temp_str= "User 10";
				break;
		case 11:m_edit1.Format("NO CAP");
				temp_str= "NO CAP";
				break;
		
		case 12:m_edit1.Format("MISSING FOIL");
				temp_str= "MISSING FOIL";
				break;

		default:m_edit1.Format("----");
				break;
	}

		UpdateData(false);
		return temp_str;
}

//Clears defective bottles log.
void PicRecal::OnReset() 
{
	int result=AfxMessageBox("Do you want to Reset the Pictures?", MB_YESNO);
	if(IDYES!=result) return;
	else
	{
		if(m_list_box.GetCurSel() != CB_ERR)  //To Reset, 1st check if user selected something from the Drop Down
		{
		
	    		pframe->m_pxaxis->imgBuf3RGB_cam1=pframe->m_pxaxis->arr1[0];//storing the Default address in an array
				pframe->m_pxaxis->imgBuf3RGB_cam2=pframe->m_pxaxis->arr1[1];
				pframe->m_pxaxis->imgBuf3RGB_cam3=pframe->m_pxaxis->arr1[2];

			if((GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam1) == NULL) &&  
				(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam2) == NULL) && 
				(GlobalFree(pframe->m_pxaxis->imgBuf3RGB_cam3) == NULL)) //Free Up the memory already occupied
			{
				//Re allocate and Reinitialize the Pointers
				pframe->m_pxaxis->imgBuf3RGB_cam1      =   (BYTE*)GlobalAlloc(GMEM_FIXED| GMEM_ZEROINIT,(theapp->cam1Width/2)*(theapp->cam1Height/2)*4 * 500);
				pframe->m_pxaxis->imgBuf3RGB_cam2      =   (BYTE*)GlobalAlloc(GPTR,(theapp->cam2Width/2)*(theapp->cam2Height/2)*4 * 500); 
				pframe->m_pxaxis->imgBuf3RGB_cam3      =   (BYTE*)GlobalAlloc(GPTR,(theapp->cam3Width/2)*(theapp->cam3Height/2)*4 * 500); 
				
				pframe->m_pxaxis->arr1[0]=pframe->m_pxaxis->imgBuf3RGB_cam1; //storing the Default address in an array
				pframe->m_pxaxis->arr1[1]=pframe->m_pxaxis->imgBuf3RGB_cam2;
				pframe->m_pxaxis->arr1[2]=pframe->m_pxaxis->imgBuf3RGB_cam3;

				arr_strng_addrs[0]=pframe->m_pxaxis->imgBuf3RGB_cam1;
				arr_strng_addrs[1]=pframe->m_pxaxis->imgBuf3RGB_cam2;
				arr_strng_addrs[2]=pframe->m_pxaxis->imgBuf3RGB_cam3;

				//Clear time stamps array.
				for(int i=0;i<500;i++)
				{
					for(int j=0;j<=3;j++)
					{
						pframe->m_pxaxis->tim_stamp[i][j]=0;
					}
				}

				//Clear timestamp list box.
				for (int k = m_list_box.GetCount()-1; k >= 0; k--)
				{
				  m_list_box.DeleteString( k );
				}


				m_list_box.SetCurSel(-1);
				pframe->m_pxaxis->imge_no=0;
			}
			
		}
		else{
		//	AfxMessageBox(" Need to Select Something from the Drop Down Before you can Reset ",MB_YESNO|MB_ICONSTOP   );

		//	Prompt1 prompt1;
		//	prompt1.DoModal();

			pframe->prompt_code=12;
			Prompt3 prompt3;
			prompt3.DoModal();



			 

		
		}

			InvalidateRect(NULL,false);
	}
}

//NOT USED
void PicRecal::OnShowPics() 
{
///Invalidate(true);	
	
}

void PicRecal::OnOK() 
{
	// TODO: Add extra validation here
//	pframe->SetTimer(44,1000,NULL);//Timer for sending 330 to the PLC for bottle off issue
//	pframe->SetTimer(44,1500,NULL);

	OnClose();
	CDialog::OnOK();
}

void PicRecal::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if(theapp->NoSerialComm==false) 
	{
						
		//pframe->m_pserial->SendMessage(WM_QUE,0,330);
		//pframe->m_pserial->SendMessage(WM_QUE,0,330);
		
	//	pframe->m_pserial->SendChar(0,330);
	//	pframe->m_pserial->SendChar(0,330);
	}
	
}

void PicRecal::OnClose() 
{
	count=0;
	pframe->m_pxaxis->imgBuf3RGB_cam1=arr_strng_addrs[0];   //Putting the stored address back
	pframe->m_pxaxis->imgBuf3RGB_cam2=arr_strng_addrs[1];
	pframe->m_pxaxis->imgBuf3RGB_cam3=arr_strng_addrs[2];

	pframe->m_pxaxis->showostore=0;
	
	pframe->SetTimer(44,2000,NULL);//Timer for refreshing the view in Main window

	OnDestroy();

	CDialog::OnClose();
}

//NOT UESED
void PicRecal::OnSelection()
{
	

//	Invalidate(true);	


}


//NOT USED
void PicRecal::OnSelchangeList1() 
{
	//Invalidate(true);	
	InvalidateRect(NULL,false);
	
}

//Saves pictures of defective bottles to disk
void PicRecal::OnSaveToDisk() 
{

	
		int nCount = m_list_box.GetCount(); //how many pics are in the list box
		bool successfull=false;
		char* pFileName = "c:\\silgandata\\DefectivePics\\";//drive
		char currentpath[100]={0}; //initi and Reset
	


	for(int i=0;i<nCount;i++)
	{
	//		currentpath[100]={0};
			imgBuf_def=(pframe->m_pxaxis->arr1[0]+(i*((theapp->cam1Width/2)*(theapp->cam1Height/2)*4)));
			imgBuf_def2=(pframe->m_pxaxis->arr1[1]+(i*((theapp->cam2Width/2)*(theapp->cam2Height/2)*4)));
			imgBuf_def3=(pframe->m_pxaxis->arr1[2]+(i*((theapp->cam3Width/2)*(theapp->cam3Height/2)*4)));
			
			//Create path and file name
			strcpy(currentpath,pFileName);
			///
			stringstream ss (stringstream::in | stringstream::out);

			//Append time stamp to file name.
			ss << pframe->m_pxaxis->tim_stamp[i][1];
			ss<<"_";
			ss << pframe->m_pxaxis->tim_stamp[i][2];
			ss<<"_";
			ss << pframe->m_pxaxis->tim_stamp[i][3];
			ss<<"_";
			ss << pframe->m_pxaxis->tim_stamp[i][4];
		
			
			CString path(ss.str().c_str());
			CString cam1_path,cam2_path,cam3_path; //temporary local variables

			//Create file name for each camera.
			cam1_path=path+"_Cam1.bmp";
			cam2_path=path+"_Cam2.bmp";
			cam3_path=path+"_Cam3.bmp";
		
		
			///
			strcat(currentpath,cam1_path);//

		//Save Cam1 image.	
		CVisRGBAByteImage image(theapp->cam1Width/2,theapp->cam1Height/2,1,-1,imgBuf_def);
		imageS=image;
	
		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;

			filedescriptorT.filename = currentpath;

			imageS.WriteFile(filedescriptorT);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
		// Warn the user.
		//	AfxMessageBox("The file could Not be saved. ");
			
			pframe->prompt_code=11;
			Prompt3 promtp3;
			promtp3.DoModal();


			
		}

	
		sprintf(currentpath,"");
	

		//Save Cam2 image.	
		CVisRGBAByteImage image2(theapp->cam2Width/2,theapp->cam2Height/2,1,-1,imgBuf_def2);
		imageS2=image2;
	
		strcpy(currentpath,pFileName);
		strcat(currentpath,cam2_path);//

		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;

			filedescriptorT.filename = currentpath;

			imageS2.WriteFile(filedescriptorT);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
		// Warn the user.
		//	AfxMessageBox("The file could Not be saved. ");
			
			pframe->prompt_code=11;
			Prompt3 promtp3;
			promtp3.DoModal();
			
		}
        

		sprintf(currentpath,"");

		//Save Cam3 image.
		CVisRGBAByteImage image3(theapp->cam3Width/2,theapp->cam3Height/2,1,-1,imgBuf_def3);
		imageS3=image3;

		strcpy(currentpath,pFileName);
		strcat(currentpath,cam3_path);//

		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;

			filedescriptorT.filename = currentpath;

			imageS3.WriteFile(filedescriptorT);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Warn the user.
		//	AfxMessageBox("The file could Not be saved. ");
			
			pframe->prompt_code=11;
			Prompt3 promtp3;
			promtp3.DoModal();


			
		}	
		sprintf(currentpath,""); //Reset currentpath
        

		successfull=true;
	}

	if(successfull==true)
	{
		pframe->prompt_code=28;
		Prompt3 promtp3;
		promtp3.DoModal();


	}
}

//Response to a different defective bottle time stamp being selected.
void PicRecal::OnSelchangeList2() 
{
	InvalidateRect(NULL,false);
	
}
