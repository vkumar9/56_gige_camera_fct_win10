// Photoeye.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Photoeye.h"
#include "mainfrm.h"
#include "serial.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Photoeye dialog


Photoeye::Photoeye(CWnd* pParent /*=NULL*/)
	: CDialog(Photoeye::IDD, pParent)
{
	//{{AFX_DATA_INIT(Photoeye)
	m_title = _T("");
	//}}AFX_DATA_INIT
}


void Photoeye::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Photoeye)
		DDX_Text(pDX, IDC_RADIO1A, m_enable);
	DDX_Text(pDX, IDC_Title, m_title);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Photoeye, CDialog)
	//{{AFX_MSG_MAP(Photoeye)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RADIO1A, OnRadio1a)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Photoeye message handlers

BOOL Photoeye::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pphoto=this;
	theapp=(CCapApp*)AfxGetApp();

	CFont l_font;

	//Create font of "top view" label.
	l_font.CreateFont(	16,					//int nHeight
						0,					//int nWidth,
						0,					//int nEscapement
						0,					//int nOrientation 
						FW_BOLD,			//int nWeight, FW_NORMAL
						FALSE,				//BYTE bItalic,
						FALSE,				//BYTE bUnderline,
						FALSE,				//BYTE cStrikeOut
						0,					//BYTE nCharSet
						OUT_DEFAULT_PRECIS,	//BYTE nOutPrecision, 
						CLIP_DEFAULT_PRECIS,//BYTE nClipPrecision,   
						DEFAULT_QUALITY,	//BYTE nQuality,
						DEFAULT_PITCH | FF_ROMAN, //BYTE nPitchAndFamily,
						"Arial"				//LPCTSTR lpszFacename
					);

	CDC *DC = GetDC();
	DC->SelectObject(&l_font);

	//Set font for text: Top View
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(160, "Arial Bold");
	CStatic * m_Label;
	m_Label = (CStatic *)GetDlgItem(IDC_Title);
	m_Label->SetFont(m_Font1);
//	m_version.SetFont(m_Font1);

	//Set static text to say "TOP VIEW"
	m_title.Format("%s", "TOP VIEW"); //Specify the Version number here
	

	//Update toggle button on gui to reflect saved product flow orientation.
	if(theapp->SavedPhotoeye==false)
	{
		CheckDlgButton(IDC_RADIO1A,1);
		m_enable.Format("%s","Bottle flow from the right");
	}
	else
	{
		CheckDlgButton(IDC_RADIO1A,0);
		m_enable.Format("%s","Bottle flow from the left");
	}
	UpdateData(false);	//Update gui.
	

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Does not appear to be used.
void Photoeye::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0;
		Function=0;
		CntrBits=0;

		pframe=(CMainFrame*)AfxGetMainWnd();
		if(theapp->NoSerialComm==false) 
		{
						
			pframe->m_pserial->ControlChars=Value+Function+CntrBits;
		}
		
		
	}	
	
	CDialog::OnTimer(nIDEvent);
}

//Toggles the orientation of product direction of travel with respect to the camera tunnel.
//There are two photoeyes on the camera tunnel, the direction of travel of the product specifies 
//which of the two photoeyes should be used.
void Photoeye::OnRadio1a() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();	//Get reference to main window.
	if(theapp->SavedPhotoeye==true)			//If product flows from the left 
	{
		theapp->SavedPhotoeye=false;		//Set app variable to indicate that product is set to
											//from from the right.
		CheckDlgButton(IDC_RADIO1A,1); 		//Update toggle button to indicate changed state.
		m_enable.Format("%s","Bottle flow is from the right");	//Update variable for toggle button text.
		
		if(theapp->NoSerialComm==false) 		//If PLC is connected.
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,220);
			pframe->m_pserial->SendChar(0,220);		//Send new flow orientation to PLC
		}
	
	}
	else	//Opposite of above.
	{
		theapp->SavedPhotoeye=true;
		CheckDlgButton(IDC_RADIO1A,0);
		m_enable.Format("%s","Bottle flow is from the left");

		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,230);
			pframe->m_pserial->SendChar(0,230);
		}
	
	}
	UpdateData(false);	
	
	
}

void Photoeye::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

