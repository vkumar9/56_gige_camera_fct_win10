#if !defined(AFX_VIEW2_H__2F9F23E9_AF23_44C6_9A31_5AFE52F32051__INCLUDED_)
#define AFX_VIEW2_H__2F9F23E9_AF23_44C6_9A31_5AFE52F32051__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//View2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// View2 form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
friend class CMainFrame;

class View2 : public CFormView
{
protected:
	View2();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(View2)

// Form Data
public:
	//{{AFX_DATA(View2)
	enum { IDD = IDD_VIEW2 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(View2)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~View2();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(View2)
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEW2_H__2F9F23E9_AF23_44C6_9A31_5AFE52F32051__INCLUDED_)
