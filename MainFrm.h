// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(AFX_MAINFRM_H__C35428C6_3B04_4A9D_8867_7C6AF0902675__INCLUDED_)
#define AFX_MAINFRM_H__C35428C6_3B04_4A9D_8867_7C6AF0902675__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "GoIO_DLL_interface.h"
#include <atomic>

enum CameraInfoMode
{
   REQUESTED_FPS,
   PROCESSED_FPS,
   DISPLAYED_FPS,
   MODEL_AND_SERIAL,
};

enum ImageInfoMode
{
   TIMESTAMP,
   CURSOR,
};

//
// The main window's status bar supports fewer modes
// than the title bar so we need to impose some limits.
//
#define NUM_CAMERAINFO_MODES 3
#define NUM_IMAGEINFO_MODES 2

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)

	friend class TBColor;
	friend class CCamera;
	friend class CWhiteBalance;
	friend class Serial;
	friend class CCapView;
	friend class CCapApp;
	friend class Job;
	friend class Setup;
	friend class CXAxisView;
	friend class CYAxisView;
	friend class Pics;
	friend class Insp;
	friend class Blowoff;
	friend class SDelay;
	friend class Filler;
	friend class FillerData;
	friend class FSetup;
	friend class Data;
	friend class View;	//statistics dialog
	friend class Prompt;
	friend class Security;

	friend class Light;
	friend class Modify;
	friend class Motor;
	friend class Tech;
	friend class Lim;
	friend class BottleDown;
	friend class SaveAs;
	friend class MoveHead;
	
	friend class Photoeye;
	friend class DigIO;
	friend class Database;
	friend class Status;
	friend class CCamera;
	friend class Band;
	friend class Xtra;
	friend class C3WaySplitterFrame;
	friend class Advanced;
	friend class PicRecal;
	friend class Usb;

	friend class FillerGraph;
	friend class SelectingFiller;
	friend class EnhancedTB;
	friend class Prompt3;


public:

	

	//////////////////////
	//ML- TO KEEP TRACK OF CAMERA PERFORMANCE		
	std::atomic<int> countC0;
	std::atomic<int> countC1;
	std::atomic<int> countC2;
	std::atomic<int> countC3;

	bool	gotPicC1;
	bool	gotPicC2;
	bool	gotPicC3;

	//////////////////////

	CMainFrame();
	void UpdateOutput(int Address, bool Status);
	int StoredStatus;								//NOT USED
	LARGE_INTEGER  timeStart;								//NOT USED
	LARGE_INTEGER timeEnd;								//NOT USED
	LARGE_INTEGER Frequency;								//NOT USED
	bool out0;								//NOT USED
	bool out1;								//NOT USED
	bool out2;								//NOT USED
	bool out3;								//NOT USED
	bool out4;								//NOT USED
	bool out5;								//NOT USED
	bool out6;								//NOT USED
	bool out7;								//NOT USED

	int prompt_code; //This is going to hold codes for the prompt messages.


   UINT_PTR m_hTimer;

   void enterFullScreenMode();
   void exitFullScreenMode();
// Attributes
public:
	FillerData* m_pfillerdata;
	FSetup*		m_pfsetup2;
	TBColor*	m_ptbcolor;
    Setup*		m_psetup;
	Job*		m_pjob;
	CCapView*	m_pview;
	//CCapView2* m_pview2;
	CXAxisView* m_pxaxis;
	CYAxisView* m_pyaxis;
	CMainFrame* m_pform;
	Serial*		m_pserial;
	SDelay*		m_psdelay;
	
	CCapApp*	theapp;
	Pics*		m_ppics;
	Status*		m_pstatus;
	Filler*		m_pfiller;

	Insp*		m_pinsp;
	Blowoff*	m_pblowoff;

	Data*		m_pdata;
	View*		m_pview2;
	CWnd*		pParent;
	Prompt*		m_pprompt;

	Security*	m_psec;
	
	Light*		m_plight;
	Modify*		m_pmod;
	Motor*		m_pmotor;
	Tech*		m_ptech;
	Lim*		m_plimit;
	BottleDown* m_pbotdn;
	SaveAs*		m_psaveas;
	MoveHead*	m_pmovehead;
	
	CCamera*	m_pcamera;
	CWhiteBalance* m_pwhtbal;
	Usb*		m_pusb;
	
	Photoeye*	m_pphoto;
	DigIO*		m_pdigio;
	Database*	m_pdatabase;
	Band*		m_pband;
	Xtra*		m_px;
	C3WaySplitterFrame* m_psplit;
	Advanced*	m_advanced;
	PicRecal*	m_picrecall;
	FillerGraph* m_pfillergraph;
	SelectingFiller* m_pselectingfiller;

	EnhancedTB* m_penhancedtb;


public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	void Compare_temp();
//	void OSSleep(unsigned long msToSleep);
//	bool GetAvailableDeviceName(char *deviceName, gtype_int32 nameLength, gtype_int32 *pVendorId, gtype_int32 *pProductId);
//	void Initialise_temp_probe_main();
//	void onUsb();
	float FoilWindow(BYTE* im_srcIn);
	void UpdatePLC();
	int timesThru;				//Counter used to sequentially send scan statistics to user PLC.	
	bool dataReady;				//Flag indicating that new data is available for user PLC, used to send data to user PLC
	int oldTotal;				//Variable used to track the total number of bottles which where scanned the last time the user PLC was updated.
								//This variable is used for checking if new information needs to be sent to user PLC. If the oldtotal is
								//less than TotalNet then send new data to user PLC.

	void ResetTotals2();
	int TotalJob;				//Total number of bottles scanned since the job was selected.
	int RejectsJob;				//Total number of bottles rejected since the job was selected.
	void PrintJobData();
	int count6;					//NOT USED
	int Rejects24;				//Total number of bottles ejected in the last 24 hours.
	int Total24;				//Total number of bottles ran in the last 24 hours.
	CFile DataFile;				//File used to save statistics every 24 hours.
	bool testData;

	//Used to save scan statistics to file, once every 24 hours and when new job is selected in the jobs dialog box.
	CString m_r1;
	CString m_r2;
	CString m_r3;
	CString m_r4;
	CString m_r5;
	CString m_r6;
	CString m_r7;
	CString m_r8;
	CString m_r9;
	CString m_r10;
	CString m_r11;
	CString m_r12;


	bool CopyDone;				//Flag indicating that scan data is being backed up.
	void Update24HourData();
	bool is666received;			//Flag indicating that eject all bottles signal from user was received.

	int fillerArray[140];		//Array holding the number of rejected bottles for each filler head.
	int capperArray[40];		//Array holding the number of rejected bottles for each capper head.
	void RunHelp();
	bool togglej;				//NOT USED
	bool reloading;				//Flag indicating that a job is being reloaded.
	int gotNext;				//NOT USED
	bool JobLoaded;				//Flag indicating if a job has been loaded. Set in jobs dialog.
	void SaveJob(int jobNum);
	bool MakeCamList();
	void GetJob(int JobNum);
	void DeleteSubKey(int JobNum);
	void SetTrigger();
	void CleanUp();	

	CComboBox CameraListCombo;		//NOT USED
	CComboBox* m_pcameralist;		//NOT USED
	CComboBox FormatListCombo;		//NOT USED
	CComboBox ModeListCombo;		//NOT USED
	CComboBox FrateListCombo;		//NOT USED


	void GenerateFormatList(BOOL bRedisp);
	void GenerateModeList(BOOL bRedisp);
	void GenerateFrateList(BOOL bRedisp);
	bool DeleteCamList();
	int *m_pCrntMode;
	int *m_pCrntFrate;
	int *m_pTrigger;
	int CurrentLip;
	int CurrentU2Thresh2;

	CString ProfileCode;
	
	int CurrentJoba5;
	int CurrentJoba4;
	int savedtotal;
	int conreject;
	int ConRejectLimit;
	int IORejectLimit;
	bool IODebounce;
	bool IODone;
	int IORejects;
	bool IOReject;
	void PhotoEyeEnable(bool enabled);
	bool PicsOpen;
	bool ToggleCheckStatus;
	int LockUp;
	int Condition2;
	int Condition1;
	int NewTriggerCount;
	int OldTriggerCount;
	int TriggerCount;
	void CheckStatus();
	bool OnLine;
	void UpdatePLCData();
	bool SJob;
	bool SSecretMenu;
	bool FreezeReset;
	int MotPos;
	float TBsd;
	bool STech;
	bool Stop;
	bool JobNoGood;
	bool Freeze;
	bool SModify;
	bool SEject;
	bool SLimits;
	bool SSetup;
	bool Safe;
	bool SaveCopyDisabled;
	bool WaitNext;
	void ResetTotals();
	float EjectRate;
	float BottleRate;
	int OldBottleCount;
	bool InspOpen;
	bool WasSetup;
	bool CountEnable;
	bool Faulted;
	void GotFault();
	bool debounce;
	int DigitalData;
	int Rejects;
	int Total;
	void Init();
	std::atomic<int> CurrentJobNum;
	int CurrentJob;

	int CurrentJoba;
	int CurrentJobb;
	int CurrentJobc;
	int CurrentJobd;
	int CurrentJobe;
	int CurrentJobf;
	int CurrentJobg;
	int CurrentJobh;
	int CurrentJobi;
	int CurrentJobj;
	int CurrentJobk;
	int CurrentJobl;
	int CurrentJobm;
	int CurrentJobn;
	int CurrentJobo;
	int CurrentJobp;
	int CurrentJobq;
	int CurrentJobr;
	int CurrentJobs;
	int CurrentJobt;
	int CurrentJobu;
	int CurrentJobv;
	int CurrentJobu2;
	int CurrentJobv2;
	int CurrentJobw;
	int CurrentJobx;
	int CurrentJoby;
	int CurrentJobz;
	int CurrentJoba1;
int CurrentJoba2;
int CurrentJoba3;
	int CurrentJobb1;
	int CurrentJobc1;
	int CurrentJobd1;
	int CurrentJobe1;
	int CurrentJobf1;
	int CurrentJobg1;
	int CurrentJobh1;
	int CurrentJobi1;
	int CurrentJobUx;
	int CurrentJobUy;
int CurrentU2Pos;
	int CurrentU2Thresh;

	bool EjectEnable;

	afx_msg LONG Inspect(UINT, LONG);
	afx_msg LONG Trigger(UINT, LONG);
	afx_msg LONG NewJob(UINT, LONG);
	afx_msg LONG ReLoad(UINT mp, LONG);
	afx_msg LONG SetLightLevel(UINT level, LONG);

	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CSize   m_sizeMax;
// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSerial();
	afx_msg void OnJobchange();
	afx_msg void OnClose();
	afx_msg void OnSetup2();
	afx_msg void OnPics();
	afx_msg void OnEject();
	afx_msg void OnSetup3();
	afx_msg void OnSecurity();
	afx_msg void OnLight();
	afx_msg void OnMotor();
	afx_msg void OnBottledown();
	afx_msg void OnPhotoeye();
	afx_msg void OnDigio();
	afx_msg void OnData();
	afx_msg void OnSetup4();
	afx_msg void OnCamcal();
	afx_msg void OnLogout();
	afx_msg void OnTeach();
	afx_msg void OnAligncams();
	afx_msg void OnFiller();
	afx_msg void OnFillerData();
	afx_msg void OnCameras();
	afx_msg void OnAdvanced();
	afx_msg void OnUpdateFcData(CCmdUI* pCmdUI);
	afx_msg void OnRecallDefective();
	afx_msg void OnAdjustshutter();
	afx_msg void OnAdjustWhtBal();
//	afx_msg void OnUsb();
	afx_msg void OnTestEject();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__C35428C6_3B04_4A9D_8867_7C6AF0902675__INCLUDED_)
