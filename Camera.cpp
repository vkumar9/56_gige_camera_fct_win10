// Camera.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <FlyCapture2.h>

#include "status.h"
#include "Camera.h"
#include "MainFrm.h"
#include "xaxisview.h"
#include "capview.h"
#include "Prompt3.h"
#include "serial.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FlyCapture2;

LARGE_INTEGER  timeStartc, timeEndc, Frequencyc;
/////////////////////////////////////////////////////////////////////////////
// CCamera dialog

#define _DEFAULT_WINDOW_X 1024					//Default width of window.
#define _DEFAULT_WINDOW_Y 768					//Default height of window.
// Grab this many images, then quit.
//
#define IMAGES 5				//NOT USE

// Set the grab timeout to this many milliseconds.
//
#define TIMEOUT 5000							//NOT USED

// Software trigger the camera instead of using an external hardware trigger
//
#define SOFTWARE_TRIGGER_CAMERA					//NOT USED

// By default, use the PGR-specific SOFT_ASYNC_TRIGGER register 0x102C to
// generate the software trigger. Comment this out to use the DCAM 1.31 
// SOFTWARE_TRIGGER register 0x62C as the software trigger (note: this requires
// a DCAM 1.31-compliant camera that implements this functionality).
//
#define USE_SOFT_ASYNC_TRIGGER					//NOT USED

#define _MAX_BGR_BUFFER_SIZE  1024 * 768 * 4	//Maximum image size RGB and transparency

#define ALLOW_CAMERA_SELECT true				//NOT USED
// The minimum size of the BGR image buffer.
//
#define _MIN_BGR_BUFFER_SIZE  640 * 480 * 4		//Minimum image size RGB and transparency
#define _MIN_BGR_COLS  640						//Minimum image size RGB and transparency
#define _MIN_BGR_ROWS  480						//Minimum image size RGB and transparency
// Register defines
// 
#define INITIALIZE         0x000				//NOT USED
#define TRIGGER_INQ        0x530				//NOT USED
#define CAMERA_POWER       0x610				//NOT USED
#define SOFTWARE_TRIGGER   0x62C				//NOT USED
#define SOFT_ASYNC_TRIGGER 0x102C				//NOT USED

// Buffers per camera.
//
#define _BUFFERS 40					//NOT USED, number of buffers
int g_uiCameras = 3;					//Number of cameras used.
//
// Maximum cameras on the bus. 
// (the maximum devices allowed on a 1394 bus is 64).
//
#define _MAX_IMAGE_SIZE 1600 * 1200 * 4			//NOT USED, Maximum image size RGB and transparency
#define _MAX_CAMERAS 3							//Maximum number of cameras used.


CCamera::CCamera(CWnd* pParent /*=NULL*/)
	: CDialog(CCamera::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCamera)
	m_pos1 = _T("");
	m_pos2 = _T("");
	m_cam1shutter = _T("");
	m_cam2shutter = _T("");
	m_cam3shutter = _T("");
	m_pos3 = _T("");
	m_bandwidth1 = _T("");
	m_bandwidth2 = _T("");
	m_whitebalance1Red = _T("");
	m_whitebalance2Red = _T("");
	m_whitebalance3Red = _T("");
	m_whitebalance1Blue = _T("");
	m_whitebalance2Blue = _T("");
	m_whitebalance3Blue = _T("");
	//}}AFX_DATA_INIT

	whitebalincrement = 10;
	ipaddresses[0].octets[0] = ipaddresses[1].octets[0] = ipaddresses[2].octets[0] = 192;
	ipaddresses[0].octets[1] = ipaddresses[1].octets[1] = ipaddresses[2].octets[1] = 168;

	ipaddresses[0].octets[2] = 2;
	ipaddresses[1].octets[2] = 3;
	ipaddresses[2].octets[2] = 4;

	ipaddresses[0].octets[3] = ipaddresses[1].octets[3] = ipaddresses[2].octets[3] = 1;
	grabThread1 = NULL;
	grabThread2 = NULL;
	grabThread3 = NULL;

	for (int i = 0; i < EXPECTED_CAMS; i++)
	{
		readNext[i] = 0;
		writeNext[i] = 1;
	}
	signal1 = CreateSemaphore(NULL, 0, 1, "SIGNAL1");
	signal2 = CreateSemaphore(NULL, 0, 1, "SIGNAL2");
	signal3 = CreateSemaphore(NULL, 0, 1, "SIGNAL3");
	dispSignal[0] = CreateSemaphore(NULL, 1, 1, "DISPSIGNAL1");
	dispSignal[1] = CreateSemaphore(NULL, 1, 1, "DISPSIGNAL2");
	dispSignal[2] = CreateSemaphore(NULL, 1, 1, "DISPSIGNAL3");

	filestream.open("c:\\out\\log.txt");
	if (filestream.is_open())
		filestream.write("\n", 1);
	
}


void CCamera::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCamera)
	DDX_Text(pDX, IDC_POS1, m_pos1);
	DDX_Text(pDX, IDC_POS2, m_pos2);
	DDX_Text(pDX, IDC_CAM1SHUTTER, m_cam1shutter);
	DDX_Text(pDX, IDC_CAM2SHUTTER, m_cam2shutter);
	DDX_Text(pDX, IDC_CAM3SHUTTER, m_cam3shutter);
	DDX_Text(pDX, IDC_Pos3, m_pos3);
	DDX_Text(pDX, IDC_BANDWIDTH1, m_bandwidth1);
	DDX_Text(pDX, IDC_BANDWIDTH2, m_bandwidth2);;
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCamera, CDialog)
	//{{AFX_MSG_MAP(CCamera)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_MOVEUP1, OnMoveup1)
	ON_BN_CLICKED(IDC_MOVEDN1, OnMovedn1)
	ON_BN_CLICKED(IDC_MOVEUP2, OnMoveup2)
	ON_BN_CLICKED(IDC_TRIGGER, OnTrigger)
	ON_BN_CLICKED(IDC_DOWN23, OnDown23)
	ON_BN_CLICKED(IDC_CAM1F, OnCam1ExpF)
	ON_BN_CLICKED(IDC_CAM1S, OnCam1ExpS)
	ON_BN_CLICKED(IDC_CAM2F, OnCam2ExpF)
	ON_BN_CLICKED(IDC_CAM2S, OnCam2ExpS)
	ON_BN_CLICKED(IDC_CAM3F, OnCam3ExpF)
	ON_BN_CLICKED(IDC_CAM3S, OnCam3ExpS)
	ON_BN_CLICKED(IDC_MOVEUP3, OnMoveup3)
	ON_BN_CLICKED(IDC_DOWN24, OnDown3)
	ON_BN_CLICKED(IDC_BANDWIDTHUP1, OnBandwidthup1)
	ON_BN_CLICKED(IDC_BANDWIDTHDW1, OnBandwidthdw1)
	ON_BN_CLICKED(IDC_BANDWIDTHUP2, OnBandwidthup2)
	ON_BN_CLICKED(IDC_BANDWIDTHDW2, OnBandwidthdw2)
	ON_BN_CLICKED(IDOK, OnClose)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCamera message handlers

BOOL CCamera::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pcamera=this;
	theapp=(CCapApp*)AfxGetApp();

	LockOut2=false;
	LockOut=false;
	//ModifyCams();
	frameRate=20.0;
	
	shutterSpeed[0]= theapp->shutterSpeed;
	gain[0]=10.0;
	shutterSpeed[1]= theapp->shutterSpeed2;
	gain[1]=17.0;
	shutterSpeed[2]= theapp->shutterSpeed3;
	gain[2]=17.0;

	cam1Height = theapp->cam1Height;
	cam1Width = theapp->cam1Width;
	cam1Top = theapp->cam1Top;
	cam1Left = theapp->cam1Left;

	cam2Height = theapp->cam2Height;
	cam2Width = theapp->cam2Width;
	cam2Top = theapp->cam2Top;
	cam2Left = theapp->cam2Left;

	cam3Height = theapp->cam3Height;
	cam3Width = theapp->cam3Width;
	cam3Top = theapp->cam3Top;
	cam3Left = theapp->cam3Left;

	m_pos3.Format("%3i",theapp->cam3Top);
	m_pos2.Format("%3i",theapp->cam2Top);
	m_pos1.Format("%3i",theapp->cam1Top);
	cam_e[0] = &cam_e1;
	cam_e[1] = &cam_e2;
	cam_e[2] = &cam_e3;
	m_loopmode = FREE_RUNNING;	//Set camera acquisition mode.

	m_cam1shutter.Format("%2.2f",theapp->shutterSpeed);
	m_cam2shutter.Format("%2.2f",theapp->shutterSpeed2);
	m_cam3shutter.Format("%2.2f",theapp->shutterSpeed3);
	m_bandwidth1.Format("%2.0f",theapp->bandwidth1);
	m_bandwidth2.Format("%2.0f",theapp->bandwidth2);
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//NOT USED
void CCamera::initBitmapStruct( int iCols, int iRows )
{
   BITMAPINFOHEADER* pheader = &m_bitmapInfo.bmiHeader;
   
   //
   // Initialize permanent data in the bitmapinfo header.
   //
   pheader->biSize          = sizeof( BITMAPINFOHEADER );
   pheader->biPlanes        = 1;
   pheader->biCompression   = BI_RGB;
   pheader->biXPelsPerMeter = 100;
   pheader->biYPelsPerMeter = 100;
   pheader->biClrUsed       = 0;
   pheader->biClrImportant  = 0;
   
   //
   // Set a default window size.
   // 
   pheader->biWidth    = iCols;
   pheader->biHeight   = -iRows;
   pheader->biBitCount = 32;
   
   m_bitmapInfo.bmiHeader.biSizeImage = 
      pheader->biWidth * pheader->biHeight * ( pheader->biBitCount / 8 );
}

//Call back function from point gray camera driver. Called when state of firewire bus changes, such as camera being disconnected,
//bus reset, image grabbed, register read/written.
//void CCamera::busCallback( void* pparam, int iMessage, unsigned long ulParam )
void CCamera::busRemovalCallback( void* pparam, int serial)
{ 
    CCamera* pDoc = (CCamera*)pparam;
	if(pDoc->LockOut==false)
	{
		pDoc->LockOut=true;
		pDoc->theapp->lostCam+=1;
		pDoc->StopCams();
	}
}

void CCamera::busCallback( void* pparam, int serial)
{
   CCamera* pDoc = (CCamera*)pparam;
   
   const unsigned long ulSerialNumber = serial;
   
   char buf[10];
   CString c;
   CString csMsg;
	   
	pDoc->SetTimer(1,1,NULL);
	pDoc->SetTimer(2,10000,NULL);//10 seconds

	c =itoa(ulSerialNumber,buf,10);
	pDoc->pframe->m_pview->m_status=c;
}

void CCamera::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1)  //Used in combination with timers 3 and 4 to restart cameras.
	{
		KillTimer(1);
		if(LockOut2==false)
		{
			//InitializeCameras();
			ReStartCams();
		}
		else
		{
			SetTimer(1,1000,NULL);
			SetTimer(4,500,NULL);
		}
	}
	
	if (nIDEvent==2)   //Used to restart cameras when there is bus error.
	{
		KillTimer(2);
 		LockOut=false;
	}

	if (nIDEvent==3)  //Used in combination with timers 1 and 4 to restart cameras.
	{
		KillTimer(3);

		LockOut2=true;
		StopCams();
		

		SetTimer(1,1000,NULL);
	}

	if (nIDEvent==4)  //Used in combination with timers 1 and 3 to restart cameras.
	{
		KillTimer(4);
 		LockOut2=false;
	}

	if (nIDEvent==5)  //Used to save and display new bandwidth.
	{KillTimer(5); pframe->SaveJob(pframe->CurrentJobNum);}
	if ((nIDEvent == 6))
	{
		if (!updateCamsInProgress)
		{
			updateCamsInProgress = true;
			StopCams();
			Sleep(1000);
			updateCamsInProgress = false;
			ReStartCams();
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CCamera::OnClose() 
{
	pframe->SSecretMenu = false;
	ShowWindow(SW_HIDE);
}

//Stop camera threads, stop cameras, clear image buffers, clear image pumping mechanism buffers.
void CCamera::StopCams()
{
	m_bContinueGrabThread   = false;	//Stop image grab threads.
	Sleep(100);
}

FlyCapture2::Error CCamera::SetImageSettings(int index)
{
	GigEImageSettings		imgSettings;
	if (index == 0)
	{
		imgSettings.offsetX = cam1Left.load();
		imgSettings.offsetY = cam1Top.load();
		imgSettings.height = cam1Height.load();
		imgSettings.width = cam1Width.load();
	}
	else if (index == 1)
	{
		imgSettings.offsetX = cam2Left.load();
		imgSettings.offsetY = cam2Top.load();
		imgSettings.height = cam2Height.load();
		imgSettings.width = cam2Width.load();
	}
	else if (index == 2)
	{
		imgSettings.offsetX = cam3Left.load();
		imgSettings.offsetY = cam3Top.load();
		imgSettings.height = cam3Height.load();
		imgSettings.width = cam3Width.load();
	}
	CString str;
	str.Format("Camera %d Set left %d, top %d width %d height %d", index, imgSettings.offsetX, imgSettings.offsetY, imgSettings.width, imgSettings.height);

	imgSettings.pixelFormat = PIXEL_FORMAT_RAW8; //PIXEL_FORMAT_MONO8//PIXEL_FORMAT_422YUV8//PIXEL_FORMAT_RAW8
	FlyCapture2::Error error = cam_e[index]->SetGigEImageSettings(&imgSettings);
	return error;
}
 
//Initializes the cameras and memory for camera image output, starts the image grab threads and starts the cameras.
int CCamera::StartCams()
{
	bool bContinueGrabThread=true;
	int camOK[3]={0, 0, 0};

	unsigned long cam3SerialNumber=theapp->CamSer3;		//Load serial number for camera 1.	//8100333;//test 3
	unsigned long cam2SerialNumber=theapp->CamSer2;		//Load serial number for camera 2.	//8210086;//test 1
	unsigned long cam1SerialNumber=theapp->CamSer1;		//Load serial number for camera 3.	//8100339;//test 4

	CameraInfo		camInfo[128];
    unsigned int	numCamInfo = 128;
	unsigned int	numCameras;
	GigEImageSettingsInfo	imgSettingsInfo;
	GigEImageSettings		imgSettings;
	busMgr = new BusManager();


	FlyCapture2::Error error;
   
    unsigned int puiSize;	//Holds number of cameras connected bus.
	puiSize=3;
	
	Sleep(600);

	error = FlyCapture2::BusManager::DiscoverGigECameras(camInfo, &numCamInfo);
	if(error != PGRERROR_OK) {for(int i=0; i<EXPECTED_CAMS; i++) camOK[i]=1; error.PrintErrorTrace();}
	
    error =	busMgr->GetNumOfCameras(&numCameras);
	if(error != PGRERROR_OK) {if(numCameras == EXPECTED_CAMS)for(int i=0; i<EXPECTED_CAMS; i++) camOK[i]=2; error.PrintErrorTrace();}

	if(error == PGRERROR_OK)
	{
		if(numCameras == 0)
		{
			camOK[0] = camOK[1] = camOK[2] = 2;
			theapp->noCams=true;
		}
		else
		{
			camOK[0] = camOK[1] = camOK[2] = -2;
		}
	 
		handles[0] = CreateEvent(0, true, false, "Event 1");
		handles[1] = CreateEvent(0, true, false, "Event 2");
		handles[2] = CreateEvent(0, true, false, "Event 3");

		m_bRestart = true;
		m_bContinueGrabThread = true;
		int index = 0;
		int index2 = 1;
		int index3 = 2;

		grabThread1 = new std::thread(threadGrabImage, this);
		SetThreadDescription((HANDLE)grabThread1->native_handle(), L"Grab Thread 1");
		SetThreadPriority((HANDLE)grabThread1->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread1 = new std::thread(threadInspect1, this);
		SetThreadDescription((HANDLE)inspectThread1->native_handle(), L"Inspect Thread 1");
		SetThreadPriority((HANDLE)inspectThread1->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		Status status;	status.DoModal();
		cam++;

		grabThread2 = new std::thread(threadGrabImage2, this);
		SetThreadDescription((HANDLE)grabThread2->native_handle(), L"Grab Thread 2");
		SetThreadPriority((void*)grabThread2->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread2 = new std::thread(threadInspect2,this);
		SetThreadDescription((HANDLE)inspectThread2->native_handle(), L"Inspect Thread 2");
		SetThreadPriority((HANDLE)inspectThread2->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		status;	status.DoModal();
		cam++;
		
		grabThread3 = new std::thread(threadGrabImage3, this);
		SetThreadDescription((HANDLE)grabThread3->native_handle(), L"Grab Thread 3");
		SetThreadPriority((HANDLE)grabThread3->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		inspectThread3 = new std::thread(threadInspect3, this);
		SetThreadDescription((HANDLE)inspectThread3->native_handle(), L"Inspect Thread 3");
		SetThreadPriority((HANDLE)inspectThread3->native_handle(), THREAD_PRIORITY_HIGHEST);//Set camera 1 thread to highest priority.
		status;	status.DoModal();
		cam++;

   }

   return TRUE;
   
}

FlyCapture2::Error CCamera::CameraSetup(int index, FlyCapture2::BusManager* busMgr, int& camOK)
{
	FlyCapture2::Error error;
	PGRGuid guid;
	InterfaceType interfaceType;
	const unsigned int k_camInitialize = INITIALIZE;
	const unsigned int k_regVal = 0x80000000;

	cam = index + 1;

	error =	busMgr->GetCameraFromIPAddress(ipaddresses[index], &guid);
	if(error != PGRERROR_OK) {camOK=3;  error.PrintErrorTrace();}
	
	//getting type of camera's interface
	error = busMgr->GetInterfaceTypeFromGuid(&guid, &interfaceType);
	if(error != PGRERROR_OK) {camOK=4; error.PrintErrorTrace();}

	if(interfaceType == INTERFACE_GIGE)
	{
		error =	cam_e[index]->Connect(&guid);
		if(error != PGRERROR_OK)	{camOK=5; error.PrintErrorTrace();}
		error  = cam_e[index]->WriteRegister( k_camInitialize, k_regVal );
		if(error != PGRERROR_OK)	{camOK=5; error.PrintErrorTrace();}
			
		//Set register 954 to 1 to kill camera's heartbeat
		cam_e[index]->WriteGVCPRegister(0x954, 0x1, false);
	
		CameraInfo camInfo;
		error		=	cam_e[index]->GetCameraInfo(&camInfo);
		if(error != PGRERROR_OK)	{camOK=6; error.PrintErrorTrace();}

		unsigned int numStreamChannels = 0;
		error		=	cam_e[index]->GetNumStreamChannels(&numStreamChannels);
		if(error != PGRERROR_OK)  	{camOK=7; error.PrintErrorTrace();}

		for (unsigned int i=0; i < numStreamChannels; i++)
		{
			GigEStreamChannel streamChannel;
			error	=	cam_e[index]->GetGigEStreamChannelInfo(i, &streamChannel);
				
			if(error != PGRERROR_OK){camOK=8; error.PrintErrorTrace();}
		}
						
		imageSettings.lock();
		error = SetCameraProperty(cam_e[index], GAIN, gain[index]);
		if(error != PGRERROR_OK)	{camOK=9; error.PrintErrorTrace(); AfxMessageBox("Failed to Set GAIN");}

		error = SetCameraProperty(cam_e[index], SHUTTER, shutterSpeed[index]);
		if (error != PGRERROR_OK)  	{camOK=10; error.PrintErrorTrace(); AfxMessageBox("Failed to Set SHUTTERSPEED");}
		imageSettings.unlock();

		error = SetCameraProperty(cam_e[index], BRIGHTNESS, 1.0);
		if (error != PGRERROR_OK)  	{camOK=11; error.PrintErrorTrace(); AfxMessageBox("Failed to Set BRIGHTNESS");}

		error = SetCameraProperty(cam_e[index], HUE, 1.5);
		if(error != PGRERROR_OK)	{camOK=12; error.PrintErrorTrace(); AfxMessageBox("Failed to Set HUE");}

		error = SetCameraProperty(cam_e[index], SATURATION, 100);
		if(error != PGRERROR_OK)	{camOK=13; error.PrintErrorTrace(); AfxMessageBox("Failed to Set SATURATION");}
		
		error = SetCameraPropertyNoAbs(cam_e[index], SHARPNESS, 500);
		if(error != PGRERROR_OK)	{camOK=14; error.PrintErrorTrace(); AfxMessageBox("Failed to Set SHARPNESS");}

		error = SetCameraProperty(cam_e[index], AUTO_EXPOSURE, 1.0);
		if(error != PGRERROR_OK)	{camOK=15; error.PrintErrorTrace(); AfxMessageBox("Failed to Set AUTO_EXPOSURE");}

		if (index == 0)
		{
			error = SetCameraProperty(cam_e[index], WHITE_BALANCE, theapp->whitebalance1Red, theapp->whitebalance1Blue);
		}
		else if (index == 1)
		{
			error = SetCameraProperty(cam_e[index], WHITE_BALANCE, theapp->whitebalance2Red, theapp->whitebalance2Blue);
		}
		else if (index == 2)
		{
			error = SetCameraProperty(cam_e[index], WHITE_BALANCE, theapp->whitebalance3Red, theapp->whitebalance3Blue);
		}


		//int wbr, wbb;
		//error = GetCameraProperty(cam_e[index], WHITE_BALANCE, wbr, wbb);
		//if (index == 0)
		//{
		//	theapp->whitebalance1Red.store(wbr);
		//	theapp->whitebalance1Blue.store(wbb);
		//}
		//else if (index == 1)
		//{
		//	theapp->whitebalance2Red.store(wbr);
		//	theapp->whitebalance2Blue.store(wbb);
		//}
		//else if (index == 2)
		//{
		//	theapp->whitebalance3Red.store(wbr);
		//	theapp->whitebalance3Blue.store(wbb);
		//}
		if(error != PGRERROR_OK)	{camOK=16; error.PrintErrorTrace(); AfxMessageBox("Failed to Set WHITE_BALANCE");}
	}
	return error;
}


FlyCapture2::Error CCamera::SetTriggerMode(FlyCapture2::GigECamera* cam, bool value = true)
{
	TriggerMode triggerMode;
	FlyCapture2::Error error = cam->GetTriggerMode(&triggerMode);
    			
	if(error!=PGRERROR_OK)	
	{error.PrintErrorTrace(); return error;}

	// Set camera to trigger mode
//	triggerMode.onOff		=	false;
	triggerMode.onOff		= value;
	triggerMode.polarity = 1; // rising edge
	triggerMode.source = 2; //2-External, 7 Internal

	error = cam->SetTriggerMode(&triggerMode);

	if(error!=PGRERROR_OK)
	{error.PrintErrorTrace();	AfxMessageBox("Failed to SetTriggerMode()"); return error;}
	return error;
}

FlyCapture2::Error CCamera::doContinousGrab(FlyCapture2::GigECamera* cam, bool &gotPic, std::atomic<int>& count, bool cameraDone[], int& bufferSize, int index)
{
	FlyCapture2::Error error;
	Image rawImage;  
	int camOK = 0;
	FC2Config config;
	HANDLE* signal;
	while (m_bRestart.load())
	{
			imageSettings.lock();

			m_iProcessedBufferSize[index] = 0;
			memset(&m_imageProcessed[index][0], 0x0, sizeof(m_imageProcessed[index][0]));
			memset(&m_imageProcessed[index][1], 0x0, sizeof(m_imageProcessed[index][1]));

			if(index == 0)
				m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed1;
			else if(index == 1)
				m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed2;
			else
				m_imageProcessed[index][0] = m_imageProcessed[index][1] = &m_imageProcessed3;


			initBitmapStruct(_DEFAULT_WINDOW_X, _DEFAULT_WINDOW_Y);
			resizeProcessedImage(_MIN_BGR_ROWS, _MIN_BGR_COLS, m_imageProcessed[index][0], m_iProcessedBufferSize[index]);
			resizeProcessedImage(_MIN_BGR_ROWS, _MIN_BGR_COLS, m_imageProcessed[index][1], m_iProcessedBufferSize[index]);

			SetImageSettings(index);
			Mode giGeImgMode = MODE_0; //MODE_0:1288x694 //MODE_1:644x482
			error = cam->SetGigEImagingMode(giGeImgMode);

			if (error != PGRERROR_OK) { camOK = 19; error.PrintErrorTrace(); /*AfxMessageBox("Failed %d", camOK);*/ }

			//*******************************************************************
			error = SetCameraProperty(cam_e[index], GAIN, gain[index]);
			if (error != PGRERROR_OK) { camOK = 9; error.PrintErrorTrace(); AfxMessageBox("Failed to Set GAIN"); }

			error = SetCameraProperty(cam_e[index], SHUTTER, shutterSpeed[index]);
			if (error != PGRERROR_OK) { camOK = 10; error.PrintErrorTrace(); AfxMessageBox("Failed to Set SHUTTERSPEED"); }

			TriggerMode triggerMode;
			error = cam->GetTriggerMode(&triggerMode);

			if (error != PGRERROR_OK)
			{
				error.PrintErrorTrace(); return error;
			}

			SetTriggerMode(cam, true);
			if (error != PGRERROR_OK) { error.PrintErrorTrace();	AfxMessageBox("Failed to SetTriggerMode()"); return error; }


			if(index == 0)
			{ 
				SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance1Red.load(), theapp->whitebalance1Blue.load());
				signal = &signal1;
			}
			else if (index == 1)
			{
				SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance2Red.load(), theapp->whitebalance2Blue.load());
				signal = &signal2;
			}
			else if (index == 2)
			{
				SetCameraProperty(cam, WHITE_BALANCE, theapp->whitebalance3Red.load(), theapp->whitebalance3Blue.load());
				signal = &signal3;
			}


			error = cam->GetConfiguration(&config);
			if (error != PGRERROR_OK) { error.PrintErrorTrace(); return error; }

			// Set the grab timeout to INFINITE
			config.grabTimeout = -1;

			// Set the camera configuration
			error = cam->SetConfiguration(&config);
			if (error != PGRERROR_OK) { error.PrintErrorTrace(); AfxMessageBox("Failed to SetConfiguration()"); return error; }

			// Set the bus notification callback so the user-level app can know when
			// the camera is unplugged, etc.
			CallbackHandle handle;
			error = busMgr->RegisterCallback((BusEventCallback)busRemovalCallback, BusCallbackType::REMOVAL, 0, &handle);
			if (error != PGRERROR_OK) AfxMessageBox("failed to create bus callback");

			imageSettings.unlock();
			error = cam->StartCapture();
	

			if (camOK != 0) { theapp->noCams = true; }
			else { theapp->noCams = false;  }

			SetEvent(handles[index]);
		while (m_bContinueGrabThread.load())
		{
//			WaitForMultipleObjects(3, handles, true, INFINITE);
			//////////////////////////////		
			// Grab the raw image.
			//
			error = cam->RetrieveBuffer(&rawImage);
			unsigned int i = rawImage.GetDataSize();
//			ResetEvent(handles[index]);
			if (!m_bContinueGrabThread.load())
			{
				break;
			}

			switch (error.GetType())
			{
			case PGRERROR_ISOCH_NOT_STARTED:
				//
				// This is an expected error case if we are changing modes or 
				// frame rates in another thread (ie, PGRFlycaptureGUI.dll).  Wait
				// a while and then try again.
				//
				TRACE(
					"flycaptureGrabImage2() returned an error (\"%s\") "
					"(expected case.)\n",
					error.GetDescription());

				//AfxMessageBox( "Grab2 camera 1 returned an error" );
				Sleep(50);
				continue;
				break;
			}

			// Do post processing on the image.
			gotPic = true;
			count.store(count+1);

			if (count > 180 || count < 0)
			{
				count = 1;
			}


			//set it to false
			cameraDone[count] = false;
			error = rawImage.Convert(PIXEL_FORMAT_BGRU, m_imageProcessed[index][writeNext[index]]);
			i = m_imageProcessed[index][writeNext[index]]->GetDataSize();
			if (error != PGRERROR_OK)
			{
				if (error == PGRERROR_FAILED)
				{
					error = rawImage.Convert(PIXEL_FORMAT_BGRU, m_imageProcessed[index][writeNext[index]]);
					if (error != PGRERROR_OK) {
						gotPic = false;
						continue;
					}
				}			//AfxMessageBox("failed to convert image camera 1");
				//OutputDebugString(imProc->GetData());
			}
			///////////////////////////////
			//ML- TO KEEP TRACK OF CAMERA PERFORMANCE
			if (count.load() > 180 || count.load() < 0)
			{
				count.store(1);
			}
			else if (count.load() == 180)
			{
				cameraDone[0] = false;
				cameraDone[1] = false;
			}
			cameraDone[count.load() + 1] = false;

			//set it to false
			cameraDone[count.load()] = false;

			writeNext[index] = writeNext[index] ? 0 : 1;
			readNext[index] = readNext[index] ? 0 : 1;
			ReleaseSemaphore(*signal, 1, 0);

	//		SetEvent(handles[index]);
		}//while( m_bContinueGrabThread.load() )

		cam->StopCapture();


	}//while( m_bRestart )
	return error;
}

void CCamera::doInspect1()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal1, INFINITE);
		theapp->pframeSub->m_pxaxis->Display(NULL);
	}
}

void CCamera::doInspect2()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal2, INFINITE);
		theapp->pframeSub->m_pxaxis->Display2(NULL);
	}
}
void CCamera::doInspect3()
{
	while (m_bContinueGrabThread.load())
	{
		WaitForSingleObject(signal3, INFINITE);
		theapp->pframeSub->m_pxaxis->Display3(NULL);
	}
}



//Function which pumps images from camera 1 into the PC.
UINT CCamera::doGrabLoop()
{
	int camOK = 0;
	FlyCapture2::Error error;
	for (int i = 1; i< 6; i++)
	{
		initTry = i;
		error = CameraSetup(0, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
			error = SetImageSettings(0);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[0]);
	doContinousGrab(cam_e[0], theapp->pframeSub->gotPicC1, theapp->pframeSub->countC1, theapp->pframeSub->m_pxaxis->cameraDone1, m_iProcessedBufferSize[0], 0);
   return 0;
}

//Function which pumps images from camera 2 into the PC.
UINT CCamera::doGrabLoop2()
{
	int camOK = 0;
	FlyCapture2::Error error;
	for (int i = 1; i< 6; i++)
	{
		initTry = i;
		error = CameraSetup(1, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
			error = SetImageSettings(1);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}

	SetTriggerMode(cam_e[1]);
	doContinousGrab(cam_e[1], theapp->pframeSub->gotPicC2, theapp->pframeSub->countC2, theapp->pframeSub->m_pxaxis->cameraDone2, m_iProcessedBufferSize[1], 1);

   return 0;
}

//Function which pumps images from camera 3 into the PC.
UINT CCamera::doGrabLoop3()
{

	int camOK = 0;
	FlyCapture2::Error error;
	for (int i = 1; i< 6; i++)
	{
		initTry = i;
		error = CameraSetup(2, busMgr, camOK);
		if (error == PGRERROR_OK)
		{
			error = SetImageSettings(2);
			if (error == PGRERROR_OK)
			{
				break;
			}
		}
	}
	SetTriggerMode(cam_e[2]);
	doContinousGrab(cam_e[2], theapp->pframeSub->gotPicC3, theapp->pframeSub->countC3, theapp->pframeSub->m_pxaxis->cameraDone3, m_iProcessedBufferSize[2], 2);

   return 0;
}

//Starts camera 1 grab image loop.
UINT CCamera::threadGrabImage( void* pparam )
{

   CCamera* pNewCamera= (CCamera*) pparam;					//Get reference to this dialog box
	CMainFrame*   	pframe=(CMainFrame*)AfxGetMainWnd();	//Get reference to main window

   UINT uiRetval = pNewCamera->doGrabLoop(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;

	   pframe->prompt_code=22;
		Prompt3 promtp3;
		promtp3.DoModal();
   }
   return uiRetval;
}

UINT CCamera::threadInspect1(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect1();
	return 0;
}

UINT CCamera::threadInspect2(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect2();
	return 0;
}

UINT CCamera::threadInspect3(void* pparam)
{
	CCamera* pNewCamera = (CCamera*)pparam;
	pNewCamera->doInspect3();
	return 0;
}

//Starts camera 2 grab image loop.
UINT CCamera::threadGrabImage2( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;
	CMainFrame*   	pframe=(CMainFrame*)AfxGetMainWnd();

   UINT uiRetval = pNewCamera->doGrabLoop2(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;

	   pframe->prompt_code=22;
			Prompt3 promtp3;
			promtp3.DoModal();
   }
   return uiRetval;
}

//Starts camera 3 grab image loop.
UINT CCamera::threadGrabImage3( void* pparam )
{
   CCamera* pNewCamera= (CCamera*) pparam;
   	CMainFrame*   	pframe=(CMainFrame*)AfxGetMainWnd();

   UINT uiRetval = pNewCamera->doGrabLoop3(); 
   
   if( uiRetval != 0 )
   {
      CString csMessage;

	  pframe->prompt_code=22;
			Prompt3 promtp3;
			promtp3.DoModal();
      
   }
   return uiRetval;
}

//Increase the camera 1 processed image buffer size.
void CCamera::resizeProcessedImage( int rows, int cols, FlyCapture2::Image* im,  int& bufferSize)
{
	//No padding was used in old version... assuming stride == cols
 	
	if( rows * cols * 4 < _MIN_BGR_BUFFER_SIZE )			//New size cannot be smaller than minimum
	{
		cols = min(_MIN_BGR_COLS, cols);
		rows = min(_MIN_BGR_ROWS, rows);
	}
   
    
   if( rows * cols > bufferSize )		//New size cannot be smaller than previous size.
   {
	   im->SetDimensions(rows, cols, cols, PixelFormat::PIXEL_FORMAT_BGRU, im->GetBayerTileFormat()); 
	  bufferSize = rows * cols * 4;								//Save the new buffer size.
   }   
}

//Updates the camera with modified parameters.
void CCamera::ModifyCams()
{
	unsigned long     ulSerialNumber = 0;
      INT_PTR		    ipDialogStatus;

	  unsigned int      uiCamerasOnBus = 64;
	 int m_iCameraBusIndex = -1; 


	m_bContinueGrabThread   = false;				//Stop image grab threads.
	Sleep(100);
	m_bContinueGrabThread = true;				//Stop image grab threads.

}

//Increase the gain of camera 1.
void CCamera::OnGmore() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	if(gain[0] <=23.75)
		{	gain[0]+=0.25;}
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel1=gain[0];
	error = SetCameraProperty(cam_e[0], GAIN, gain[0]);
	imageSettings.unlock();
	UpdateData(false);
	
}

//Decrease the gain of camera 1.
void CCamera::OnGless() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	if(gain[0] >= 0.25)
		{	gain[0]-=0.25; }
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel1=gain[0];
	error = SetCameraProperty(cam_e[0], GAIN, gain[0]);
	imageSettings.unlock();
	UpdateData(false);
	
}


//Increase the gain of camera 2.
void CCamera::OnGmore2() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	if(gain[1]<=23.75)
		{	gain[1]+=0.25;}
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel2=gain[1];
	error = SetCameraProperty(cam_e[1], GAIN, gain[1]);
	imageSettings.unlock();
	UpdateData(false);
	
}

//Decrease the gain of camera 2.
void CCamera::OnGless2() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	if(gain[1]>=0.25)
		{	gain[1]-=0.25;}
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel2=gain[1];
	error = SetCameraProperty(cam_e[1], GAIN, gain[1]);
	imageSettings.unlock();
	UpdateData(false);
	
}
//Increase the gain of camera 3.
void CCamera::OnGmore3() 
{
	FlyCapture2::Error error;
	imageSettings.lock();
	if(gain[2]<=23.75)
		{	gain[2]+=0.25; }

	theapp->jobinfo[pframe->CurrentJobNum].lightLevel3=gain[2];
	error = SetCameraProperty(cam_e[2], GAIN, gain[2]);
	imageSettings.unlock();
	UpdateData(false);
}

//Decrease the gain of camera 3.
void CCamera::OnGless3() 
{
	FlyCapture2::Error error;
	imageSettings.lock();
	if(gain[2] >=0.25)
		{	gain[2]-=0.25; }
	theapp->jobinfo[pframe->CurrentJobNum].lightLevel3=gain[2];
	error = SetCameraProperty(cam_e[2], GAIN, gain[2]);
	imageSettings.unlock();
	UpdateData(false);
}

//Clears all camera related recourses. Called when program closes.
BOOL CCamera::DestroyWindow() 
{
	//Clear image buffers.
	for (int i = 0; i < EXPECTED_CAMS; i++)
	{
		m_imageProcessed[i][0]->SetData(0, m_imageProcessed[i][0]->GetDataSize());
		m_imageProcessed[i][1]->SetData(0, m_imageProcessed[i][1]->GetDataSize());
	}
	delete busMgr;

	delete grabThread1;
	delete grabThread2;
	delete grabThread3;
	delete inspectThread1;
	delete inspectThread2;
	delete inspectThread3;
	filestream.flush();
	filestream.close();

	return CDialog::DestroyWindow();
}

//Sets the light level for all three cameras
void CCamera::SetLightLevel(double cam1Gain, double cam2Gain, double cam3Gain)
{
	FlyCapture2::Error   error;
	imageSettings.lock();

	gain[0]=cam1Gain;
	gain[1]=cam2Gain;
	gain[2]=cam3Gain;

	for(int i=0; i<EXPECTED_CAMS; i++)
	{
		error = SetCameraProperty(cam_e[i], GAIN, gain[i]);
	}
	imageSettings.unlock();

}

//Initializes the cameras and memory for camera image output.
int CCamera::ReStartCams()
{
	m_bContinueGrabThread = true;
   
   return TRUE;
}

//Move camera 1 area of interest up
void CCamera::OnMoveup1() 
{	
	theapp->cam1Top-=8;
	if(theapp->cam1Top<=84)theapp->cam1Top=84;
	
	cam1Top = theapp->cam1Top;

	SetTimer(3, 1000, NULL);
	m_pos1.Format("%3i",theapp->cam1Top);		//Update GUI variable with new position

	UpdateData(false);
}

//Move camera 1 area of interest down
void CCamera::OnMovedn1() 
{
	theapp->cam1Top+=8;
	if(theapp->cam1Top>=180)theapp->cam1Top=180;
	cam1Top = theapp->cam1Top;
	char str[50];
	//memset(str, '\0', 50);
	//sprintf(str, "CCamera val: %d, CApp vall: %d\n", cam1Top.load(), theapp->cam1Top);
	//filestream.write(str, strlen(str));


	SetTimer(3, 1000, NULL);
	m_pos1.Format("%3i",theapp->cam1Top);

	UpdateData(false);
}

//Move camera 2 area of interest up
void CCamera::OnMoveup2() 
{	
	theapp->cam2Top-=8;
	if(theapp->cam2Top<=350)theapp->cam2Top=350;
	cam2Top = theapp->cam2Top;

	SetTimer(3, 1000, NULL);
	m_pos2.Format("%3i",theapp->cam2Top);

	UpdateData(false);
}

//Manual triggering of the camera.
void CCamera::OnTrigger() 
{
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);	
}

//Move camera 2 area of interest down
void CCamera::OnDown23() 
{
	theapp->cam2Top+=8;

	if(theapp->cam2Top>=450)theapp->cam2Top=450;
	cam2Top = theapp->cam2Top;

	SetTimer(3, 1000, NULL);
	m_pos2.Format("%3i",theapp->cam2Top);

	UpdateData(false);
}

FlyCapture2::Error CCamera::SetCameraPropertyAuto(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, bool autoOn)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present = true;
	pProp.onOff = true;
	pProp.autoManualMode = autoOn;
	pProp.type = type;
	error = cam->SetProperty(&pProp, false);
	return error;

}
FlyCapture2::Error CCamera::SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present			=	true;
	pProp.absControl		=	true;
	pProp.onePush			=	false;
	pProp.onOff				=	true;
	pProp.autoManualMode	=	false;
	pProp.type				=	type;
	pProp.absValue			=	value;
	error					=	cam->SetProperty(&pProp,false); 
	return error;
}


FlyCapture2::Error CCamera::SetCameraPropertyNoAbs(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present			=	true;
	pProp.absControl		=	false;
	pProp.onePush			=	false;
	pProp.onOff				=	true;
	pProp.autoManualMode	=	false;
	pProp.type				=	type;
	pProp.absValue			=	value;
	error					=	cam->SetProperty(&pProp,false); 
	return error;
}

FlyCapture2::Error CCamera::SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, unsigned int valueA, unsigned int valueB)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.present			=	1;
	pProp.absControl		=	false;
	pProp.onePush			=	false;
	pProp.onOff				=	true;
	pProp.autoManualMode	=	false;
	pProp.type				=	type;
	pProp.valueA			=	valueA;
	pProp.valueB			=	valueB;
	error					=	cam->SetProperty(&pProp,false); 
	return error;
}

FlyCapture2::Error CCamera::GetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, int &valueA, int &valueB)
{
	FlyCapture2::Error   error;
	Property pProp;
	pProp.type = type;
	pProp.onePush = false;
	pProp.onOff = true;
	error = cam->GetProperty(&pProp);
	valueA = pProp.valueA;
	valueB = pProp.valueB;
	return error;
}

//Increase camera 1 shutter time.
void CCamera::OnCam1ExpF() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	theapp->shutterSpeed+=0.10;
	error = SetCameraProperty(cam_e[0], SHUTTER, theapp->shutterSpeed);
	m_cam1shutter.Format("%2.2f",theapp->shutterSpeed);
	shutterSpeed[0] = theapp->shutterSpeed;
	imageSettings.unlock();
	UpdateData(false);
}

//Decrease camera 1 shutter time.
void CCamera::OnCam1ExpS() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	theapp->shutterSpeed-=0.10;
	error = SetCameraProperty(cam_e[0], SHUTTER, theapp->shutterSpeed);
	m_cam1shutter.Format("%2.2f",theapp->shutterSpeed);
	shutterSpeed[0] = theapp->shutterSpeed;
	imageSettings.unlock();
	UpdateData(false);
}

//Increase camera 2 shutter time.
void CCamera::OnCam2ExpF() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	theapp->shutterSpeed2+=0.10;
	error = SetCameraProperty(cam_e[1], SHUTTER, theapp->shutterSpeed2);
	m_cam2shutter.Format("%2.2f",theapp->shutterSpeed2);
	shutterSpeed[1] = theapp->shutterSpeed2;
	imageSettings.unlock();
	UpdateData(false);
}

//Decrease camera 2 shutter time.
void CCamera::OnCam2ExpS() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	theapp->shutterSpeed2-=0.10;
	error = SetCameraProperty(cam_e[1], SHUTTER, theapp->shutterSpeed2);
	m_cam2shutter.Format("%2.2f",theapp->shutterSpeed2);
	shutterSpeed[1] = theapp->shutterSpeed2;
	imageSettings.unlock();
	UpdateData(false);
}

//Increase camera 3 shutter time.
void CCamera::OnCam3ExpF() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	theapp->shutterSpeed3+=0.10;
	error = SetCameraProperty(cam_e[2], SHUTTER, theapp->shutterSpeed3);
	m_cam3shutter.Format("%2.2f",theapp->shutterSpeed3);
	shutterSpeed[2] = theapp->shutterSpeed3;
	imageSettings.unlock();
	UpdateData(false);
}

//Decrease camera 3 shutter time.
void CCamera::OnCam3ExpS() 
{
	FlyCapture2::Error   error;
	imageSettings.lock();
	theapp->shutterSpeed3-=0.10;
	error = SetCameraProperty(cam_e[2], SHUTTER, theapp->shutterSpeed3);
	m_cam3shutter.Format("%2.2f",theapp->shutterSpeed3);
	shutterSpeed[2] = theapp->shutterSpeed3;
	imageSettings.unlock();
	UpdateData(false);
}

//Move camera 3 area of interest up
void CCamera::OnMoveup3() 
{
	theapp->cam3Top-=8;
	if(theapp->cam3Top<=350)theapp->cam3Top=350;

	cam3Top = theapp->cam3Top;

	SetTimer(3, 1000, NULL);
	m_pos3.Format("%3i",theapp->cam3Top);
	UpdateData(false);
}

//Move camera 3 area of interest down
void CCamera::OnDown3() 
{
	theapp->cam3Top+=8;
	
	if(theapp->cam3Top>=450)theapp->cam3Top=450;

	cam3Top = theapp->cam3Top;

	SetTimer(3,1000,NULL);
	m_pos3.Format("%3i",theapp->cam3Top);

	UpdateData(false);
}

//Increase the bandwidth for bus 1. Bandwidth is a percentage value between 1 and 100.
void CCamera::OnBandwidthup1() 
{
	theapp->bandwidth1+=2;

	if(theapp->bandwidth1>=99)
	theapp->bandwidth1=99;

	UpdateValues();		
}

//Decrease the bandwidth for bus 1.
void CCamera::OnBandwidthdw1() 
{
	theapp->bandwidth1-=2;

	if(theapp->bandwidth1<=30)
	theapp->bandwidth1=30;

	UpdateValues();				
}

//Increase the bandwidth for bus 2.
void CCamera::OnBandwidthup2() 
{
	theapp->bandwidth2+=2;

	if(theapp->bandwidth2>=99)
	theapp->bandwidth2=99;

	UpdateValues();				
}

//Decrease the bandwidth for bus 2.
void CCamera::OnBandwidthdw2() 
{
	theapp->bandwidth2-=2;

	if(theapp->bandwidth2<=30)
	theapp->bandwidth2=30;

	UpdateValues();		
}

//Save and display new bandwidth.
void CCamera::UpdateValues()
{
	SetTimer(5,300,NULL);
	UpdateDisplay();
}

//Display new bandwidth.
void CCamera::UpdateDisplay()
{
	m_bandwidth1.Format("%.0f",theapp->bandwidth1);
	m_bandwidth2.Format("%.0f",theapp->bandwidth2);

	UpdateData(false);
}


CWhiteBalance::CWhiteBalance(CWnd* pParent /*=NULL*/)
	: CDialog(CWhiteBalance::IDD, pParent)
{
	SelectedCamera = 1;
	LinkedCameras = 0;
	whtbalinc = 1;
	m_sCam = _T("Camera 1");
	m_sLinkCam1 = _T("C2");
	m_sLinkCam2 = _T("C3");
	m_whitebalanceRedText = _T("");
	m_whitebalanceBlueText = _T("");
	//{{AFX_DATA_INIT(CCamera)
	//}}AFX_DATA_INIT
}


BEGIN_MESSAGE_MAP(CWhiteBalance, CDialog)
	//{{AFX_MSG_MAP(CCamera)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_SELCAM1, Cam1RadioSelect)
	ON_BN_CLICKED(IDC_SELCAM2, Cam2RadioSelect)
	ON_BN_CLICKED(IDC_SELCAM3, Cam3RadioSelect)
	ON_BN_CLICKED(IDC_LINKCAM1CHKBOX, OnLinkCam1)
	ON_BN_CLICKED(IDC_LINKCAM2CHKBOX, OnLinkCam2)
	ON_BN_CLICKED(IDC_WBREDUP, OnRedUp)
	ON_BN_CLICKED(IDC_WBREDDOWN, OnRedDown)
	ON_BN_CLICKED(IDC_WBBLUEUP, OnBlueUp)
	ON_BN_CLICKED(IDC_WBBLUEDOWN, OnBlueDown)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdateLinked)
	ON_BN_CLICKED(IDAUTOWB, OnAutoWB)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER4, OnReleasedcaptureRedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER10, OnReleasedcaptureBlueSlider)
	ON_BN_CLICKED(IDOK, OnClose)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCamera message handlers

BOOL CWhiteBalance::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	theapp=(CCapApp*)AfxGetApp();

	Cam1RadioSelect();
	ClearLinks();
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Clears all camera related recourses. Called when program closes.
BOOL CWhiteBalance::DestroyWindow() 
{	
	return CDialog::DestroyWindow();
}

void CWhiteBalance::SetAutoOff(int index)
{
	int wbr, wbb;

	pframe->m_pcamera->SetCameraPropertyAuto(pframe->m_pcamera->cam_e[index], WHITE_BALANCE, false);
	Sleep(100);
	pframe->m_pcamera->GetCameraProperty(pframe->m_pcamera->cam_e[index], WHITE_BALANCE, wbr, wbb);
	UpdateWBParameters(wbr, wbb);

}

void CWhiteBalance::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==6)  //
	{
		if (IsCamera1Selected())
			SetAutoOff(0);
		if (IsCamera2Selected())
			SetAutoOff(1);
		if (IsCamera3Selected())
			SetAutoOff(2);

		pframe->m_pcamera->m_bContinueGrabThread = true;
		Sleep(100);
		pframe->SendMessage(WM_TRIGGER, NULL, NULL);
		if (m_whitebalanceAuto)
		{
			m_auto.SetCheck(0);
			m_whitebalanceAuto = false;
		}
		KillTimer(6);
	}

	CDialog::OnTimer(nIDEvent);
}

void CWhiteBalance::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWhiteBalance)
	DDX_Text(pDX, IDC_CAMERALABEL, m_sCam);
	DDX_Text(pDX, IDC_CAMLINK1, m_sLinkCam1);
	DDX_Text(pDX, IDC_CAMLINK2, m_sLinkCam2);
	DDX_Text(pDX, IDC_WBRED, m_whitebalanceRedText);
	DDX_Text(pDX, IDC_WBBLUE, m_whitebalanceBlueText);
	DDX_Control(pDX, IDC_SLIDER4, m_redSlider);
	DDX_Control(pDX, IDC_SLIDER10, m_blueSlider);
	DDX_Control(pDX, IDAUTOWB, m_auto);
	//}}AFX_DATA_MAP
}

void CWhiteBalance::OnClose() 
{
	pframe->SSecretMenu = false;
	ShowWindow(SW_HIDE);
	//CDialog::OnClose();
}

//	Select Camera 1
void CWhiteBalance::Cam1RadioSelect() 
{
	SelectedCamera = Cam1Selected;
	CheckDlgButton(IDC_SELCAM1,1);
	CheckDlgButton(IDC_SELCAM2,0);
	CheckDlgButton(IDC_SELCAM3,0);
	m_sCam = _T("Camera 1");
	m_sLinkCam1 = _T("C2");
	m_sLinkCam2 = _T("C3");
	ClearLinks();
	LoadCamera1Values();
	UpdateData(false);
}

//	Select Camera 2
void CWhiteBalance::Cam2RadioSelect() 
{
	SelectedCamera = Cam2Selected;
	CheckDlgButton(IDC_SELCAM1,0);
	CheckDlgButton(IDC_SELCAM2,1);
	CheckDlgButton(IDC_SELCAM3,0);
	m_sCam = _T("Camera 2");
	m_sLinkCam1 = _T("C1");
	m_sLinkCam2 = _T("C3");
	ClearLinks();
	LoadCamera2Values();
	UpdateData(false);
}

//	Select Camera 3
void CWhiteBalance::Cam3RadioSelect() 
{
	SelectedCamera = Cam3Selected;
	CheckDlgButton(IDC_SELCAM1,0);
	CheckDlgButton(IDC_SELCAM2,0);
	CheckDlgButton(IDC_SELCAM3,1);
	m_sCam = _T("Camera 3");
	m_sLinkCam1 = _T("C1");
	m_sLinkCam2 = _T("C2");
	ClearLinks();
	LoadCamera3Values();
	UpdateData(false);
}

void CWhiteBalance::OnLinkCam1()
{
	bool isC1 = m_sLinkCam1.Compare(_T("C1")) == 0;

	if(isC1)
	{
		if ((LinkedCameras & Cam1Selected) == Cam1Selected)
		{
			LinkedCameras -= Cam1Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam1Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 1);
		}
	}
	else
	{
		if ((LinkedCameras & Cam2Selected) == Cam2Selected)
		{
			LinkedCameras -= Cam2Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam2Selected;
			CheckDlgButton(IDC_LINKCAM1CHKBOX, 1);
		}
	}
	
	UpdateData(false);
}

void CWhiteBalance::OnLinkCam2()
{
	bool isC2 = m_sLinkCam2.Compare(_T("C2")) == 0;

	if(isC2)
	{
		if ((LinkedCameras & Cam2Selected) == Cam2Selected)
		{
			LinkedCameras -= Cam2Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam2Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 1);
		}
	}
	else
	{
		if ((LinkedCameras & Cam3Selected) == Cam3Selected)
		{
			LinkedCameras -= Cam3Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 0);
		}
		else
		{
			LinkedCameras += Cam3Selected;
			CheckDlgButton(IDC_LINKCAM2CHKBOX, 1);
		}
	}
	
	UpdateData(false);
}

void CWhiteBalance::ClearLinks()
{
	LinkedCameras = NoCamSelected;
	CheckDlgButton(IDC_LINKCAM1CHKBOX,0);
	CheckDlgButton(IDC_LINKCAM2CHKBOX,0);
}

void CWhiteBalance::LoadCamera1Values()
{
	m_whitebalanceRed = theapp->whitebalance1Red.load();
	m_whitebalanceBlue = theapp->whitebalance1Blue.load();
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera1.
	m_redSlider.SetRange(0,1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera1.
	m_blueSlider.SetRange(0,1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i",m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i",m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::LoadCamera2Values()
{
	m_whitebalanceRed = theapp->whitebalance2Red;
	m_whitebalanceBlue = theapp->whitebalance2Blue;
	m_auto.SetCheck(m_whitebalanceAuto);

	//Initialize slider for camera2.
	m_redSlider.SetRange(0,1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera2.
	m_blueSlider.SetRange(0,1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i",m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i",m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::LoadCamera3Values()
{
	m_whitebalanceRed = theapp->whitebalance3Red;
	m_whitebalanceBlue = theapp->whitebalance3Blue;
	m_auto.SetCheck(m_whitebalanceAuto);
	
	//Initialize slider for camera3.
	m_redSlider.SetRange(0,1023);
	m_redSlider.SetTicFreq(32);
	m_redSlider.SetPos(m_whitebalanceRed);

	//Initialize slider for camera3.
	m_blueSlider.SetRange(0,1023);
	m_blueSlider.SetTicFreq(32);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i",m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i",m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::OnReleasedcaptureRedSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (m_whitebalanceAuto)
	{
		m_redSlider.SetPos(m_whitebalanceRed);
			return;
	}
	m_whitebalanceRed = m_redSlider.GetPos();		//Get thumb position.
	UpdateWhiteBalanceRed(m_whitebalanceRed);
	UpdateData(false);
}

void CWhiteBalance::OnReleasedcaptureBlueSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (m_whitebalanceAuto)
	{
		m_blueSlider.SetPos(m_whitebalanceBlue);
		return;
	}
	m_whitebalanceBlue = m_blueSlider.GetPos();		//Get thumb position.
	UpdateWhiteBalanceBlue(m_whitebalanceBlue);
	UpdateData(false);
}

void CWhiteBalance::OnRedUp() 
{
	if (m_whitebalanceAuto)
	{
		m_redSlider.SetPos(m_whitebalanceRed);
		return;
	}
	if(m_whitebalanceRed < 1023)
	{
		m_whitebalanceRed += whtbalinc;
		UpdateWhiteBalanceRed(m_whitebalanceRed);
		UpdateData(false);
	}
}

void CWhiteBalance::OnRedDown() 
{
	if (m_whitebalanceAuto)
	{
		m_redSlider.SetPos(m_whitebalanceRed);
		return;
	}
	if(m_whitebalanceRed > 0)
	{
		m_whitebalanceRed -= whtbalinc;
		UpdateWhiteBalanceRed(m_whitebalanceRed);
		UpdateData(false);
	}
}

void CWhiteBalance::OnBlueUp() 
{
	if (m_whitebalanceAuto)
	{
		m_blueSlider.SetPos(m_whitebalanceBlue);
		return;
	}
	if(m_whitebalanceBlue < 1023)
	{
		m_whitebalanceBlue += whtbalinc;
		UpdateWhiteBalanceBlue(m_whitebalanceBlue);
		UpdateData(false);
	}
}

void CWhiteBalance::OnBlueDown() 
{
	if (m_whitebalanceAuto)
	{
		m_blueSlider.SetPos(m_whitebalanceBlue);
		return;
	}
	if(m_whitebalanceBlue > 0)
	{
		m_whitebalanceBlue -= whtbalinc;
		UpdateWhiteBalanceBlue(m_whitebalanceBlue);
		UpdateData(false);
	}
}

bool CWhiteBalance::IsCamera1Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam1Selected) == Cam1Selected)
	{
		return true;
	}
	return false;
}

bool CWhiteBalance::IsCamera2Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam2Selected) == Cam2Selected)
	{
		return true;
	}
	return false;
}

bool CWhiteBalance::IsCamera3Selected()
{
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam3Selected) == Cam3Selected)
	{
		return true;
	}
	return false;
}

void CWhiteBalance::OnAutoWB()
{
	int wbred, wbblue;
	
	if (IsCamera1Selected())
		DoAutoWB(0);
	if (IsCamera2Selected())
		DoAutoWB(1);
	if (IsCamera3Selected())
		DoAutoWB(2);

	SetTimer(6, 4000, NULL); //Allow Cam to Auto correct Wb for 4 sec,then set cam to Ext trigger & Update UI

}

void CWhiteBalance::DoAutoWB(int cameraToUpdate)
{
	m_whitebalanceAuto = m_auto.GetCheck();

	if (m_whitebalanceAuto)
	{
		pframe->m_pcamera->m_bContinueGrabThread = false;
		pframe->m_pserial->SendChar(0, 600);
		Sleep(100);
		pframe->m_pcamera->SetTriggerMode(pframe->m_pcamera->cam_e[cameraToUpdate], false);
		pframe->m_pcamera->SetCameraPropertyAuto(pframe->m_pcamera->cam_e[cameraToUpdate], WHITE_BALANCE, true);
	}
}


void CWhiteBalance::UpdateWhiteBalanceRed(int whitebalanceRed)
{
	FlyCapture2::Error   error;

	pframe->m_pcamera->m_bContinueGrabThread = false;
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam1Selected) == Cam1Selected)
	{
		theapp->whitebalance1Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[0], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam2Selected) == Cam2Selected)
	{
		theapp->whitebalance2Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[1], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam3Selected) == Cam3Selected)
	{
		theapp->whitebalance3Red = whitebalanceRed;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[2], WHITE_BALANCE, whitebalanceRed, m_whitebalanceBlue);
	}

	m_whitebalanceRed = whitebalanceRed;
	m_whitebalanceRedText.Format("%4i", whitebalanceRed);
	m_redSlider.SetPos(whitebalanceRed);
	Sleep(100);
	pframe->m_pcamera->m_bContinueGrabThread = true;

	pframe->SendMessage(WM_TRIGGER, NULL, NULL);
}

void CWhiteBalance::UpdateWhiteBalanceBlue(int whitebalanceBlue)
{
	FlyCapture2::Error   error;

	pframe->m_pcamera->m_bContinueGrabThread = false;
	int camerasToUpdate = SelectedCamera | LinkedCameras;
	if ((camerasToUpdate & Cam1Selected) == Cam1Selected)
	{
		theapp->whitebalance1Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[0], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}
	if ((camerasToUpdate & Cam2Selected) == Cam2Selected)
	{
		theapp->whitebalance2Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[1], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}

	if ((camerasToUpdate & Cam3Selected) == Cam3Selected)
	{
		theapp->whitebalance3Blue = whitebalanceBlue;
		pframe->m_pcamera->SetCameraProperty(pframe->m_pcamera->cam_e[2], WHITE_BALANCE, m_whitebalanceRed, whitebalanceBlue);
	}

	m_whitebalanceBlue = whitebalanceBlue;
	m_whitebalanceBlueText.Format("%4i", whitebalanceBlue);
	m_blueSlider.SetPos(whitebalanceBlue);
	Sleep(100);
	pframe->m_pcamera->m_bContinueGrabThread = true;
	pframe->SendMessage(WM_TRIGGER, NULL, NULL);
}



void CWhiteBalance::OnUpdateLinked()
{
	UpdateWhiteBalanceBlue(m_whitebalanceBlue);
	UpdateWhiteBalanceRed(m_whitebalanceRed);
	UpdateData(false);
}

void CWhiteBalance::UpdateWBParameters(int wbred, int wbblue)
{
	m_whitebalanceRed = wbred;
	m_whitebalanceBlue = wbblue;

	m_redSlider.SetPos(m_whitebalanceRed);
	m_blueSlider.SetPos(m_whitebalanceBlue);

	m_whitebalanceRedText.Format("%4i", m_whitebalanceRed);
	m_whitebalanceBlueText.Format("%4i", m_whitebalanceBlue);

	InvalidateRect(NULL, false);
	UpdateData(false);
}