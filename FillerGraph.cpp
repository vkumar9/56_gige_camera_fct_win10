// FillerGraph.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "FillerGraph.h"
#include "MainFrm.h"
#include "XAxisView.h"
#include "Modify.h"
#include "SelectingFiller.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FillerGraph dialog


FillerGraph::FillerGraph(CWnd* pParent /*=NULL*/)
	: CDialog(FillerGraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(FillerGraph)
	m_resultavg = _T("");
	m_selected_filler_avg = _T("");
	//}}AFX_DATA_INIT
}


void FillerGraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FillerGraph)
	DDX_Text(pDX, IDC_RESULT, m_resultavg);
	DDX_Text(pDX, IDC_selected_filler_avg, m_selected_filler_avg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FillerGraph, CDialog)
	//{{AFX_MSG_MAP(FillerGraph)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK1, OnScalefactor10)
	ON_BN_CLICKED(IDC_BUTTON1, OnSelectFillerValve)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FillerGraph message handlers

void FillerGraph::OnOK() 
{

	CDialog::OnOK();
}

void FillerGraph::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CBrush Red, Blue, RedLine, Colorw;

	COLORREF greencolor	=	RGB(0,255,0);
	COLORREF redcolor	=	RGB(255,0,0);
	COLORREF bluecolor	=	RGB(0,0,150);
	COLORREF lbluecolor	=	RGB(168,218,242);
	COLORREF graycolor	=	RGB(190,190,190);
	COLORREF blackcolor	=	RGB(50,50,50);
	COLORREF gray		=	RGB(100,100,100);
	COLORREF wcolor		=	RGB(255,255,255);
	COLORREF Orange		=   RGB(255, 128, 64);

	Colorw.CreateSolidBrush(wcolor);

	Red.CreateSolidBrush(RGB(255,0,0)); 
	Blue.CreateSolidBrush(RGB(0,0,255));

	CPen YellowPen		(PS_DASH,		1, RGB(255,255,0));
	CPen LGrayPenDash	(PS_DASH,		1, RGB(200,200,200));
	CPen LGrayPenSolid	(PS_SOLID,		1, RGB(200,200,200));
	CPen BlackPenSolid1	(PS_SOLID,		1, RGB(0,0,0));
	CPen OrangeSolid2	(PS_SOLID,		2, RGB(255, 128, 64));	

	CPen BluePen(PS_SOLID, 3, RGB(0,0,250));
	CPen LRedPen(PS_SOLID, 1, RGB(230,30,80));
	CPen FRedPen(PS_SOLID, 3, RGB(255,0,0));
	CPen RedDash(PS_DASH, 1, RGB(255,0,0));
	CPen BlackPen(PS_SOLID, 2, RGB(0,0,0));
	CPen GreenPen(PS_SOLID, 1, RGB(0,255,0));
//	CPen YellowPen(PS_DASH, 1, RGB(255,255,0));
	CPen WhitePen(PS_SOLID, 2, RGB(255,255,255));
	CPen AquaPen(PS_SOLID, 4, RGB(0,255,255));

	CFont l_font;

	l_font.CreateFont(	12,					//int nHeight
						0,					//int nWidth,
						0,					//int nEscapement
						0,					//int nOrientation 
						FW_BOLD,			//int nWeight, FW_NORMAL
						FALSE,				//BYTE bItalic,
						FALSE,				//BYTE bUnderline,
						FALSE,				//BYTE cStrikeOut
						0,					//BYTE nCharSet
						OUT_DEFAULT_PRECIS,	//BYTE nOutPrecision, 
						CLIP_DEFAULT_PRECIS,//BYTE nClipPrecision,   
						DEFAULT_QUALITY,	//BYTE nQuality,
						DEFAULT_PITCH | FF_ROMAN, //BYTE nPitchAndFamily,
						"Arial"				//LPCTSTR lpszFacename
					);

	CFont* l_old_font = dc.SelectObject(&l_font); 







	dc.Draw3dRect(64, 10,600,240,blackcolor,blackcolor);
	dc.SelectObject(&YellowPen);


	//horizontal grids
	for(int j=1; j<6; j+=1) 
	{dc.MoveTo(64,250- (j*40));	dc.LineTo(664,250-(j*40));}

		dc.SetBkMode(TRANSPARENT  ); 
		dc.TextOut(50,200,"10"); //reducin 10 from each for text to line up with the Horizontal grid
		dc.TextOut(50,160,"20");
		dc.TextOut(50,120,"30");
		dc.TextOut(50,80,"40");
		dc.TextOut(50,40,"50");
		dc.TextOut(33,5,"mm");
		dc.TextOut(53,5,"60");

		
		dc.SelectObject(&BlackPenSolid1);
		dc.SelectObject(&OrangeSolid2);

		if(! theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor==true)
		{
			for(int z=0;z<600;z++)
			{
				//dc.MoveTo(125+z,250);dc.LineTo(125+z,250-(pframe->m_pxaxis->fill_level[z]*4));
				dc.SetPixel(65+z,250-(pframe->m_pxaxis->fill_level[z]*4),Orange);
				if(z>=2){dc.MoveTo(65+z-1,250-(pframe->m_pxaxis->fill_level[z-1]*4));dc.LineTo(65+z,250-(pframe->m_pxaxis->fill_level[z]*4));}
			
			}

			if(filler_valve_selection >0)
			{
				for(int z=filler_valve_selection-1;z<600;z+=120)
				{
					dc.SelectObject(&BlackPenSolid1);
					dc.SetPixel(65+z,250-(pframe->m_pxaxis->fill_level[z]*4),blackcolor);
					dc.Arc(65+z-2,250-(pframe->m_pxaxis->fill_level[z]*4)-2,65+z+2,250-(pframe->m_pxaxis->fill_level[z]*4)+2,65+z+2,250-(pframe->m_pxaxis->fill_level[z]*4),65+z+2,250-(pframe->m_pxaxis->fill_level[z]*4));

					
				
				}
			
				
				//Compare Avg of the selected Filler Valve to the Total Avg of other valves
				sum_tot_filler=0;count_tot_fillr=0;
				sum_slctd_filler=0;count_slctd_filler=0;
				for(int z2=0;z2<600;z2++)
				{
					
					if((z2 != (filler_valve_selection-1)) && (z2 !=(filler_valve_selection-1+120)) && (z2 !=(filler_valve_selection-1+240)) && (z2 !=(filler_valve_selection-1+360)) && (z2 !=(filler_valve_selection-1+ 480)))
					{
						
						sum_tot_filler+=pframe->m_pxaxis->fill_level[z2];
						if(pframe->m_pxaxis->fill_level[z2] != 0 ){count_tot_fillr++;}
						
						
					}

					else	
					{
						sum_slctd_filler+=pframe->m_pxaxis->fill_level[z2];
						if(pframe->m_pxaxis->fill_level[z2] != 0 ){count_slctd_filler++;}
							
					}
					
				}
				sum_tot_filler=sum_tot_filler/count_tot_fillr; //Avg of all the other filler valves

				sum_slctd_filler=sum_slctd_filler/count_slctd_filler;//Avg of the Selected Filler Valve

				m_resultavg.Format("The Avg Value of the other Filler Valves is %.2f",sum_tot_filler);
				m_selected_filler_avg.Format("The Avg Value of Filler Valve %d is %.2f",filler_valve_selection,sum_slctd_filler);


				
			}
		}

		//////vertical Markers on X Axis
		dc.SelectObject(&BlackPenSolid1);
		dc.TextOut(60,252,"0");
		dc.TextOut(162,252,"100");  dc.MoveTo(164,247);dc.LineTo(164,251);
		dc.TextOut(262,252,"200");	dc.MoveTo(264,247);dc.LineTo(264,251);
		dc.TextOut(362,252,"300");  dc.MoveTo(364,247);dc.LineTo(364,251);
		dc.TextOut(462,252,"400");  dc.MoveTo(464,247);dc.LineTo(464,251);
		dc.TextOut(562,252,"500");  dc.MoveTo(564,247);dc.LineTo(564,251);
		dc.TextOut(662,252,"600");  dc.MoveTo(664,247);dc.LineTo(664,251);
		dc.TextOut(662,260,"Bottles");

		if(theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor==true)
		{
			dc.SelectObject(&BlackPenSolid1);
			dc.TextOut(162,252,"1000");  dc.MoveTo(164,248);dc.LineTo(164,251);
			dc.TextOut(262,252,"2000");  dc.MoveTo(264,248);dc.LineTo(264,251);
			dc.TextOut(362,252,"3000");  dc.MoveTo(364,248);dc.LineTo(364,251);
			dc.TextOut(462,252,"4000");  dc.MoveTo(464,248);dc.LineTo(464,251);
			dc.TextOut(562,252,"5000");  dc.MoveTo(564,248);dc.LineTo(564,251);
			dc.TextOut(662,252,"6000");  dc.MoveTo(664,248);dc.LineTo(664,251);
			dc.TextOut(662,260,"Bottles");

			count1=0;for(int a=0;a<600;a++){fill_graph_aftr_scale[a]=0.0;} //initialising this local array

			for(int z=0;z<6000;z+=10)
			{
				sum=0;
				for(int z1=z;z1<z+10;z1++)
				{sum+=pframe->m_pxaxis->fill_level_scale10[z1]; }
				sum=sum/10;
				fill_graph_aftr_scale[count1]=sum; 
				count1++;
				sum=0;
								
								
				
			} 

			dc.SelectObject(&GreenPen);
			for(int p=0;p<600;p++)
			{
				dc.SetPixel(65+p,250-(fill_graph_aftr_scale[p]*4),Orange);
				if(p>=2){dc.MoveTo(65+p-1,250-(fill_graph_aftr_scale[p-1]*4));dc.LineTo(65+p,250-(fill_graph_aftr_scale[p]*4));}
			
			}

			dc.SelectObject(&BlackPenSolid1);
			dc.TextOut(462,280,"Scaled by factor of 10");
			
			
		}
		UpdateData(false);

}

BOOL FillerGraph::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pfillergraph=this;
	theapp=(CCapApp*)AfxGetApp();

	theapp->ReadRegistery(pframe->CurrentJobNum); // ReadRegistry to see if ScaleFactor is Set

	pframe->m_pxaxis->graph_open=1;
	UpdateDisplay();
	
	return TRUE;  
	            
}

void FillerGraph::OnDestroy() 
{
	pframe->m_pxaxis->graph_open=0;	
	CDialog::OnDestroy();
	
}

void FillerGraph::OnScalefactor10() 
{
	if(theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor==true)
	{
		CheckDlgButton(IDC_CHECK1,0);
		theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor=false;
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor==false)
	{
		CheckDlgButton(IDC_CHECK1,1);
		theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor=true;
	}
	pframe->SaveJob(pframe->CurrentJobNum);

	UpdateData(false);
	UpdateDisplay();
	InvalidateRect(false);
	
}

void FillerGraph::UpdateDisplay()
{
	if(theapp->jobinfo[pframe->CurrentJobNum].ScaleFactor==true)
	{
	CheckDlgButton(IDC_CHECK1,1); 
	}
	else{	CheckDlgButton(IDC_CHECK1,0); }

	
}

void FillerGraph::OnSelectFillerValve() 
{
	SelectingFiller selectingfiller;
	selectingfiller.DoModal();
}
