#if !defined(AFX_SAVEAS_H__3AEEFA43_0325_40F2_8EC2_F3E6408E7CC5__INCLUDED_)
#define AFX_SAVEAS_H__3AEEFA43_0325_40F2_8EC2_F3E6408E7CC5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveAs.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SaveAs dialog

class SaveAs : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp; 
public:
	int OldJob;
	int CurrentJob;
	SaveAs(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SaveAs)
	enum { IDD = IDD_SAVEAS };
	CListBox	m_listjobs;
	CString	m_jobcopy;
	//}}AFX_DATA
	CCapApp *theapp;
	CMainFrame* pframe;
	void LoadJobs();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SaveAs)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SaveAs)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListjobs();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVEAS_H__3AEEFA43_0325_40F2_8EC2_F3E6408E7CC5__INCLUDED_)
