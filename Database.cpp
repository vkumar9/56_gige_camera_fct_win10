// Database.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Database.h"
#include "mainfrm.h"

#include "job.h"
#include "Prompt3.h"

#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

char* pfilecurrent = "c:\\silgandata\\pat\\";//drive
char* pfileload = "c:\\silgandata\\load\\";//drive
char* pfilesave = "c:\\silgandata\\save\\";//drive
/////////////////////////////////////////////////////////////////////////////
// Database dialog


Database::Database(CWnd* pParent /*=NULL*/)
	: CDialog(Database::IDD, pParent)
{
	//{{AFX_DATA_INIT(Database)
	//}}AFX_DATA_INIT
}


void Database::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Database)
	DDX_Control(pDX, IDC_LISTSAVE, m_savelist);
	DDX_Control(pDX, IDC_LISTLOAD, m_loadlist);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Database, CDialog)
	//{{AFX_MSG_MAP(Database)
	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDC_LOAD2, OnLoad2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Database message handlers

BOOL Database::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pdatabase=this;
	theapp=(CCapApp*)AfxGetApp();

	D1=theapp->SavedD1Name;
	ShowData();
	//D2="No DataBase";
	m_savelist.InsertString(0,D1);
	m_loadlist.InsertString(0,D2);
	//ShowData();
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Database::ShowData()
{
	CString C1, N1;
	char File[60];
	char File2[60];

	char pbuf[1];
	int Num=0;
	LoadOK=true;
	
	strcpy(File,pfileload);
	strcat(File,"DateFile");
	
	strcat(File,".txt");

	strcpy(File2,pfileload);
	strcat(File2,"DateFile2");
	
	strcat(File2,".txt");

	//DataFile.Close();

	//int filenum=open( File, ios::nocreate, filebuf::sh_read );// filebuf::openprot
	//if(filenum==-1) {D2="No DataBase" ; LoadOK=false; }
	//close(filenum);

	if(LoadOK==true) 
	{
		DataFile.Open(File, CFile::modeRead);
	
		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];

		int toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		D2=N1;
		N1="";
	}
}

void Database::MovePic(int index)
{
	int skip=false;
	char SourceFile[60];
	char indexchar[4];
	itoa(index,indexchar,10);

	strcpy(SourceFile,pfilecurrent);
	strcat(SourceFile,indexchar);
	strcat(SourceFile,"\\");
	strcat(SourceFile,indexchar);
	strcat(SourceFile,".bmp");

	char DestinationFile[60];

	strcpy(DestinationFile,pfilesave);
	strcat(DestinationFile,indexchar);
	strcat(DestinationFile,".bmp");

	CVisByteImage image;
	try
	{
		image.ReadFile(SourceFile);
		imageMem.Allocate(image.Rect());
		image.CopyPixelsTo(imageMem);

	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		skip=true;
		//AfxMessageBox("The Source file specified could not be opened. No saved Database?");
	}
	
	//CVisByteImage image(640,480,1,-1,im_sr);
	//imageS=image;
	if(skip==false)
	{
		try
		{
			SVisFileDescriptor filedescriptorT;
			ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
			filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
			filedescriptorT.jpeg_quality = 0;

			filedescriptorT.filename = DestinationFile;

			imageMem.WriteFile(filedescriptorT);
		}
		catch (...)  // "..." is bad - we could leak an exception object.
		{
			// Warn the user.
		//	AfxMessageBox("The file could not be saved. ");
			pframe->prompt_code=8;
			Prompt3 promtp3;
			promtp3.DoModal();

			
		}
	}

}

void Database::LoadPic(int index)
{
	char SourceFile[60];
	char indexchar[4];
	itoa(index,indexchar,10);

	strcpy(SourceFile,pfileload);
	
	strcat(SourceFile,indexchar);
	strcat(SourceFile,".bmp");

	char DestinationFile[60];

	strcpy(DestinationFile,pfilecurrent);
	strcat(DestinationFile,indexchar);
	strcat(DestinationFile,"\\");
	strcat(DestinationFile,indexchar);
	strcat(DestinationFile,".bmp");

	CVisByteImage image;
	try
	{
		image.ReadFile(SourceFile);
		imageMem.Allocate(image.Rect());
		image.CopyPixelsTo(imageMem);

	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
	//	AfxMessageBox("The Source file specified could not be opened. No saved Database?");

			pframe->prompt_code=19;
			Prompt3 promtp3;
			promtp3.DoModal();
	}
	
	//CVisByteImage image(640,480,1,-1,im_sr);
	//imageS=image;
	
	try
	{
		SVisFileDescriptor filedescriptorT;
		ZeroMemory(&filedescriptorT, sizeof(SVisFileDescriptor));
		filedescriptorT.has_alpha_channel = CVisImageBase::IsAlphaWritten();
		filedescriptorT.jpeg_quality = 0;

		filedescriptorT.filename = DestinationFile;

		imageMem.WriteFile(filedescriptorT);
	}
	catch (...)  // "..." is bad - we could leak an exception object.
	{
		// Warn the user.
	//	AfxMessageBox("The file could not be saved. ");

			pframe->prompt_code=11;
			Prompt3 promtp3;
			promtp3.DoModal();

		
	}

}

void Database::OnAccept() 
{
	
	CTime thetime = CTime::GetCurrentTime();
	CString S1;
	
	int mth=thetime.GetMonth();
	int day=thetime.GetDay();
	int yr=thetime.GetYear();
	
	itoa(mth,month,10);
	itoa(day,dayofweek,10);
	itoa(yr,year,10);
	strcpy(cp,pfilesave);
		
	strcpy(currentpath2,"Db");
	//strcat(currentpath2,index);
	strcat(currentpath2,"_");
	strcat(currentpath2,month);
	strcat(currentpath2,"_");
	strcat(currentpath2,dayofweek);
	strcat(currentpath2,"_");
	strcat(currentpath2,year);

	strcat(cp,"DateFile");
	strcat(cp,".txt");

	D1=theapp->SavedD1Name=currentpath2;
		
	DataFile.Open(cp, CFile::modeWrite | CFile::modeCreate);
	DataFile.Write(D1,D1.GetLength());
	DataFile.Write("~",1);
	DataFile.Close();


	strcpy(cp2,pfilesave);
	strcat(cp2,"DateFile2");
	strcat(cp2,".txt");
	DataFile2.Open(cp2, CFile::modeWrite | CFile::modeCreate);
	DataFile2.Close();

	for(int r=1; r<=10; r++)
	{
		itoa(r,index,10);

		strcpy(cp,pfilesave);
		
		strcpy(currentpath2,"Db");
		strcat(currentpath2,index);
		strcat(cp,currentpath2);
		strcat(cp,".txt");
		
		DataFile.Open(cp, CFile::modeWrite | CFile::modeCreate);
		m_savelist.ResetContent();
		//CString S1;
		//S1.Format("%3.2f",theapp->SavedJ1Name);

		DataFile.Write(theapp->SavedJ1Name,theapp->SavedJ1Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ2Name,theapp->SavedJ2Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ3Name,theapp->SavedJ3Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ4Name,theapp->SavedJ4Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ5Name,theapp->SavedJ5Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ6Name,theapp->SavedJ6Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ7Name,theapp->SavedJ7Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ8Name,theapp->SavedJ8Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ9Name,theapp->SavedJ9Name.GetLength());
		DataFile.Write("~",1);
		DataFile.Write(theapp->SavedJ10Name,theapp->SavedJ10Name.GetLength());
		DataFile.Write("~",1);

		switch(r)
		{
		case 1:

		
		
		break;

	case 2:
		
			
		break;

	case 3:
		
		
		break;

case 4:
		
			
		break;

case 5:
		
		
		break;

case 6:
		
		
		break;

case 7:
		
		
		break;

case 8:
		
		
		break;

case 9:
		
		
		break;

case 10:
		
		
		break;
		}//switch
		
		DataFile.Close();
		
		switch(r)
		{
			case 1:
				MovePic(1);
			break;

			case 2:
				MovePic(2);
			break;

			case 3:
				MovePic(3);
			break;

			case 4:
				MovePic(4);
			break;

			case 5:
				MovePic(5);
			break;

			case 6:
				MovePic(6);
			break;

			case 7:
				MovePic(7);
			break;

			case 8:
				MovePic(8);
			break;

			case 9:
				MovePic(9);
			break;

			case 10:
				MovePic(10);
			break;
		}
	}

	m_savelist.InsertString(0,D1);
	theapp->SavedD1Name=D1;
	UpdateData(false);		
}

void Database::OnLoad() 
{
	
	CString C1, N1, Name;
	Name="Name";
	char File[60];
	char pbuf[1];
	int Num=0;
	bool fileOK=true;
	
	for(int r=1; r<=10; r++)
	{

		itoa(r,index,10);
		strcpy(File,pfileload);
		strcat(File,"DB");
		strcat(File,index);
	//strcat(File,CurrentSpot);
		strcat(File,".txt");

//		int filenum=open( File, ios::nocreate , filebuf::openprot );
//		if(filenum==-1) {AfxMessageBox("Can not find database file!") ; fileOK=false; }
//		close(filenum);

		if(fileOK==true) 
		{
			DataFile.Open(File, CFile::modeRead);
		//searchto=nIndex*130;

//************search to correct job****************************
		//DataFile.Seek( 1, CFile::begin );

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];

		int toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ1Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ2Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ3Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ4Name=N1;
		N1="";
		
		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ5Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ6Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ7Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ8Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ9Name=N1;
		N1="";

		DataFile.Read(pbuf, 1 );
		C1=pbuf[0];
		toomany=0;
		while(C1!="~" || toomany>=50)
		{
			N1+=pbuf[0];
			DataFile.Read(pbuf, 1 );
			toomany+=1;
			C1=pbuf[0];
		}
		theapp->SavedJ10Name=N1;
		N1="";
		
		
		switch(r)
		{
		case 1:

		
			break;

		case 2:

		
			break;

	case 3:

		
			break;

	case 4:

		
			break;

	case 5:

			
			break;

	case 6:

			
			break;

	case 7:

		
			break;

	case 8:

		
			break;

	case 9:

		
			break;

	case 10:

			

DataFile.Read(pbuf, 1 );
			C1=pbuf[0];
			toomany=0;
			while(C1!="~" || toomany>=50)
			{
				N1+=pbuf[0];
				DataFile.Read(pbuf, 1 );
				toomany+=1;
				C1=pbuf[0];
			}
			theapp->SavedJ10Profile=atoi(N1);
			N1="";
			break;

		}

		DataFile.Close();
		}
		if(LoadOK==true)
		{
			switch(r)
			{
				case 1:
					LoadPic(1);
				break;

				case 2:
					LoadPic(2);
				break;

				case 3:
					LoadPic(3);
				break;

				case 4:
					LoadPic(4);
				break;

				case 5:
					LoadPic(5);
				break;

				case 6:
					LoadPic(6);
				break;

				case 7:
					LoadPic(7);
				break;

				case 8:
					LoadPic(8);
				break;

				case 9:
					LoadPic(9);
				break;

				case 10:
					LoadPic(10);
				break;
			}
		}
	}

	
}

void Database::OnOK() 
{
	pframe->m_pjob->LoadJobs();
	
	CDialog::OnOK();
}


void Database::OnLoad2() 
{
//	int result=AfxMessageBox("Do you want to write over the existing job", MB_YESNO);
//	if(IDYES!=result) return;	
}
