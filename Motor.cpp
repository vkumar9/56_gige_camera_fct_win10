// Motor.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Motor.h"
#include "Mainfrm.h"
#include "serial.h"
#include "xaxisview.h"
#include "capview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Motor dialog


Motor::Motor(CWnd* pParent /*=NULL*/)
	: CDialog(Motor::IDD, pParent)
{
	//{{AFX_DATA_INIT(Motor)
	m_mpot = _T("");
	//}}AFX_DATA_INIT
}


void Motor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Motor)
	DDX_Text(pDX, IDC_MPOT, m_mpot);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Motor, CDialog)
	//{{AFX_MSG_MAP(Motor)
	ON_BN_CLICKED(IDC_HIGH, OnHigh)
	ON_BN_CLICKED(IDC_LOWER, OnLower)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_HIGH2, OnHigh2)
	ON_BN_CLICKED(IDC_LOWER2, OnLower2)
	ON_BN_CLICKED(IDC_TRIGGER, OnTrigger)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Motor message handlers

BOOL Motor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();				//Get pointer to main window
	pframe->m_pmotor=this;								//Set main window's pointer for this dlgBx.
	theapp=(CCapApp*)AfxGetApp();						//Get pointer to app.
	pframe->OnLine=false;								//Set online flag to false.

	pframe->m_pview->SetTimer(2,500,NULL);			//Turn on blinking red box in main window view.

	//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
	/*
	//pframe->SetTimer(8,100,NULL);//com
	//pframe->m_pserial->SendMessage(WM_QUE,0,400);//red light
	//pframe->UpdateOutput(5, true);
	//pframe->UpdateOutput(4, false);
	//pframe->UpdateOutput(7, false);
	*/
	
	Value=0;//NOT USED
	Function=0;//NOT USED
	CntrBits=0;//NOT USED
	MotorPos=0;
	if(theapp->NoSerialComm==false) 		//If connected to PLC
	{				
		MotorPos=pframe->m_pserial->MotorPos;	//Get current motor position.
	}
		
	m_mpot.Format("%i",MotorPos);	//Display current motor position.
	UpdateData(false);
	//m_fvalue.Format("%i",pframe->m_pserial->FocusPos);ZoomPos
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Rise camera enclosure.
void Motor::OnHigh() 
{
	if(theapp->NoSerialComm==false) 
	{
						
		//pframe->m_pserial->SendMessage(WM_QUE,0,130);
		pframe->m_pserial->SendChar(0,130);
	}

	SetTimer(4,1000,NULL);		//Use timer to get the new motor position after the motion has been completed.
	//SetTimer(3,200,NULL);
//m_mpot.Format("%i",pframe->m_pserial->FocusPos);
//UpdateData(false);
	
}

//Lower camera enclosure.
void Motor::OnLower() 
{
	if(theapp->NoSerialComm==false) 
	{
						
		//pframe->m_pserial->SendMessage(WM_QUE,0,140);
		pframe->m_pserial->SendChar(0,140);			//Send move motor signal to PLC.
	}
	//pframe->m_pserial->ControlChars=Value+Function+CntrBits;
	SetTimer(4,1000,NULL);	//Use timer to get the new motor position after the motion has been completed.
//m_mpot.Format("%i",pframe->m_pserial->FocusPos);
//UpdateData(false);
	
}

void Motor::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent==1) //Not used?
	{
		KillTimer(1);
 		
		UpdateData(false);
	}
	
	if (nIDEvent==2) //Not used?
	{
		KillTimer(2);
 	
		UpdateData(false);
	}

	if(nIDEvent==3)//Not used?
	{
		KillTimer(3);

		Value=0;
		Function=0;
		CntrBits=0;
		if(theapp->NoSerialComm==false) 
		{
						
		pframe->m_pserial->ControlChars=Value+Function+CntrBits;
		}
		SetTimer(4,500,NULL);
		pframe->m_pxaxis->InvalidateRect(NULL,false);
		
		
	}

	if(nIDEvent==4)//Get motor position from serial port, and update the GUI with new value.
	{
		KillTimer(4);
		if(theapp->NoSerialComm==false) 
		{
						
			pframe->m_pserial->GetChar(0);			//Query PLC for motor position.

			MotorPos=pframe->m_pserial->MotorPos;	//Get motor position from main frame serial port object.
		}

		//if(MotorPos==0) SetTimer(4,5,NULL);
		m_mpot.Format("%i",MotorPos);				//Update dlgBx motor position display.
		UpdateData(false);							//Load updated data to user interface.
		
		OnTrigger();								//?Some kind of debug operations?
		SetTimer(5,1500,NULL);
	}
	if(nIDEvent==5)//Not used?
	{
		KillTimer(5);
		OnButton1();
	}

		
	CDialog::OnTimer(nIDEvent);
}

//Rise camera enclosure, fast.
void Motor::OnHigh2() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,240);
		pframe->m_pserial->SendChar(0,240);			//Send move motor signal to PLC.
	}

	SetTimer(4,1000,NULL);		//Use timer to get the new motor position after the motion has been completed.

	//UpdateData(false);
}

//Lower camera enclosure, slow.
void Motor::OnLower2() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,250);
		pframe->m_pserial->SendChar(0,250);			//Send move motor signal to PLC.
	}

	SetTimer(4,1000,NULL);	//Use timer to get the new motor position after the motion has been completed.
	//UpdateData(false);
}

//Update motor position in main frame and save to job.
void Motor::OnOK() 
{
	if(theapp->NoSerialComm==false) 
	{
						

	theapp->jobinfo[pframe->CurrentJobNum].motPos=pframe->MotPos=MotorPos=pframe->m_pserial->MotorPos;
	}
	pframe->SaveJob(pframe->CurrentJobNum);		//Save changes to the current job.

	CDialog::OnOK();
}

//NOT USED
void Motor::OnTrigger() 
{
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);	
}

//Get motor position from main frame and update the gui with the new value.
void Motor::OnButton1() 
{
	if(theapp->NoSerialComm==false) 
	{
							
		MotorPos=pframe->m_pserial->MotorPos;
	}
		//if(MotorPos==0) SetTimer(4,5,NULL);
		m_mpot.Format("%i",MotorPos);
	UpdateData(false);
}
