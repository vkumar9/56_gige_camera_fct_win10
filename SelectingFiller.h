#if !defined(AFX_SELECTINGFILLER_H__E807FF48_39BB_46D6_93D1_B500AB01BA3B__INCLUDED_)
#define AFX_SELECTINGFILLER_H__E807FF48_39BB_46D6_93D1_B500AB01BA3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectingFiller.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SelectingFiller dialog

/////////////////////////////////////////////////////////////////////////////
// SelectingFiller dialog

class SelectingFiller : public CDialog
{
// Construction
friend class FillerGraph;
friend class CMainFrame;
friend class CCapApp;
public:
	void UpdateDisplay();
	SelectingFiller(CWnd* pParent = NULL);   // standard constructor

	FillerGraph *m_pfillergraph;

	CMainFrame* pframe;
	CCapApp *theapp;

// Dialog Data
	//{{AFX_DATA(SelectingFiller)
	enum { IDD = IDD_SelectFiller };
	CString	m_edit1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SelectingFiller)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SelectingFiller)
	virtual void OnOK();
	afx_msg void OnIncreby1();
	afx_msg void OnDecrby1();
	afx_msg void OnIncreby5();
	afx_msg void OnDecrby5();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTINGFILLER_H__E807FF48_39BB_46D6_93D1_B500AB01BA3B__INCLUDED_)
