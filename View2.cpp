// View2.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "View2.h"
#include "CapView.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// View2

IMPLEMENT_DYNCREATE(View2, CFormView)

View2::View2()
	: CFormView(View2::IDD)
{
	//{{AFX_DATA_INIT(View2)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

View2::~View2()
{
}

void View2::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(View2)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(View2, CFormView)
	//{{AFX_MSG_MAP(View2)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// View2 diagnostics

#ifdef _DEBUG
void View2::AssertValid() const
{
	CFormView::AssertValid();
}

void View2::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// View2 message handlers

void View2::OnButton1() 
{
	// TODO: Add your control notification handler code here
	
}
