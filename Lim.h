#if !defined(AFX_LIM_H__BECFB756_F84B_424A_8A61_4C5B16B1F18B__INCLUDED_)
#define AFX_LIM_H__BECFB756_F84B_424A_8A61_4C5B16B1F18B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Lim.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Lim dialog

class Lim : public CDialog
{
// Construction
friend class CCapApp;
friend class CMainFrame;
public:
	bool fine;			//Flag indicating if large (false) or small (true) steps are used to adjust the inspection limits.
	int AbsoluteMax;	//Maximum height inspections limit.
	CString m_fine;		//Fine adjustments button text string, toggles between "slow" and "fast".
	double res;			//Resolution (step size) of adjustments
	bool Max;			//Flag indicating if the maximum (true) limits are being adjusted or the minimum (false) limits are being adjusted.
	void UpdateDisplay();
	Lim(CWnd* pParent = NULL);   // standard constructor
	CCapApp *theapp;
	CMainFrame* pframe;

	CString m_enable1;		//left height inspection on/off text
	CString m_enable2;		//right height inspection on/off text
	CString m_enable3;		//middle height inspection on/off text
	CString m_enable4;		//cocked cap inspection on/off text
	CString m_enable5;		//band edges inspection on/off text
	CString m_enable6;		//band surface (camera1) inspection on/off text

	CString m_enable6b;		//band rear surface (camera2) inspection on/off text
	CString m_enable6c;		//band rear surface (camera3) on/off text
	CString m_enable7;		//low fill inspection on/off text
	CString m_enable8;		//cap color inspection on/off text

	CString m_enable9;		//user inspection a on/off text
	CString m_enable9b;		//user inspection b on/off text
	CString m_enable9c;		//user inspection c on/off text
	CString m_enable10;		//NOT USED
	CString m_enable11;		//NOT USED
	CString m_enabletb;		//Enhanced tamper band inspection button "ON"/"OFF" string.
	CString m_enableRear;	//band surface (cameras 2, 3) inspection on/off text
// Dialog Data
	//{{AFX_DATA(Lim)
	enum { IDD = IDD_LIM };
	CButton	m_10up;			//NOT USED
	CButton	m_10dn;			//NOT USED
	CStatic	m_name4;		//NOT USED
	CButton	m_6upc;			//NOT USED
	CButton	m_6dnc;			//NOT USED
	CEdit	m_6c;			//NOT USED
	CButton	m_1up;			//NOT USED
	CButton	m_1dn;			//NOT USED

	CString	m_1;			//Stores left height inspection min/max value.
	CString	m_2;			//Stores right height inspection min/max value.
	CString	m_5;			//Stores tamper band edges inspection min/max value.
	CString	m_4;			//Stores cocked cap inspection min/max value.
	CString	m_3;			//Stores middle height inspection min/max value.
	CString	m_6;			//Stores tamper band surface inspection min/max value.
	CString	m_7;			//Stores low fill inspection min/max value.
	CString	m_8;			//Stores cap color inspection min/max value.
	CString	m_9;			//Stores user inspection min/max value.
	CString	m_10;
	
	//When the minimum or maximum buttons are pressed the text above the inspection limit adjustments changes.
	//The below strings are used to perform this switching i.e. toggling between displaying "Min" and "Max".	
	CString	m_m1;
	CString	m_m2;
	CString	m_m3;
	CString	m_m4;
	CString	m_m5;
	CString	m_m6;
	CString	m_m7;
	CString	m_m8;
	CString	m_m9;
	CString	m_m10;


	//The below strings are used to display the name of the inspection type such as: height left or low fill. 
	//These are located above each set of limit adjustment controls.
	CString	m_l1;
	CString	m_l2;
	CString	m_l3;
	CString	m_l4;
	CString	m_l5;
	CString	m_l6;
	CString	m_l7;
	CString	m_l8;
	CString	m_l9;
	CString	m_l10;

	//NOT USED, may have held maximum values for the limits for user display, e.g. enter a value between 0 and 255.
	CString	m_1max;
	CString	m_2max;
	CString	m_3max;
	CString	m_4max;
	CString	m_5max;
	CString	m_6max;
	CString	m_7max;
	CString	m_8max;
	CString	m_1min;
	CString	m_2min;
	CString	m_3min;
	CString	m_4min;
	CString	m_5min;
	CString	m_6min;
	CString	m_7min;
	CString	m_8min;
	CString	m_9max;
	CString	m_9min;
	CString	m_10max;
	CString	m_10min;


	CString	m_edit1;	//NOT USED.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Lim)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Lim)
	virtual BOOL OnInitDialog();
	afx_msg void On1up();
	afx_msg void On1dn();
	afx_msg void On2up();
	afx_msg void On2dn();
	afx_msg void On3up();
	afx_msg void On3dn();
	afx_msg void On4up();
	afx_msg void On4dn();
	afx_msg void On5up();
	afx_msg void On5dn();
	afx_msg void On6up();
	afx_msg void On6dn();
	afx_msg void On7up();
	afx_msg void On7dn();
	afx_msg void On8up();
	afx_msg void On8dn();
	afx_msg void OnMax();
	afx_msg void OnMin();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnE1();
	afx_msg void OnE2();
	afx_msg void OnE3();
	afx_msg void OnE4();
	afx_msg void OnE5();
	afx_msg void OnE6();
	afx_msg void OnE7();
	afx_msg void OnFine();
	afx_msg void OnE10();
	afx_msg void On10up();
	afx_msg void On10dn();
	afx_msg void OnE9();
	afx_msg void On9up();
	afx_msg void On9dn();
	afx_msg void OnE8();
	afx_msg void OnE6b();
	afx_msg void OnE6c();
	afx_msg void OnE9b();
	afx_msg void OnE9c();
	afx_msg void OnhtUp();
	afx_msg void OnhtDwn();
	afx_msg void OnCheck1();
	afx_msg void OnenableTB();
	afx_msg void OnAdjustingLimits();
	afx_msg void OnAuto();
	afx_msg void OnEnableRearSurface();
	afx_msg void OnRadio5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIM_H__BECFB756_F84B_424A_8A61_4C5B16B1F18B__INCLUDED_)
