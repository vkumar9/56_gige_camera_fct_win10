#if !defined(AFX_PICRECAL_H__338551FC_743D_42EA_8846_D4E685C12F15__INCLUDED_)
#define AFX_PICRECAL_H__338551FC_743D_42EA_8846_D4E685C12F15__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PicRecal.h : header file
//
#include "VisCore.h"

//#include "TransparentListBox.h"


/////////////////////////////////////////////////////////////////////////////
// PicRecal dialog

class PicRecal : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
friend class XAxisView;
public:
	std::string UpdateDisplay();
	PicRecal(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(PicRecal)
	enum { IDD = IDD_Picture };
	CListBox	m_list_box;			//list box holding failed bottle time stamps.
	CString	m_edit1;				//failed inspection reason.
	CString	m_countpics;			//DOES NOT APPEAR TO BE USED, Number of defective pictures,
	//}}AFX_DATA

	CMainFrame* pframe;
	CCapApp *theapp;
	XAxisView* m_pxaxis;

	int count;				//Does not appear to be used.
	int reasnofailur;		//Index of reason for failure.

//	BYTE *im_dg3s;
	
//	CComboBox *m_pcombo;

	CVisRGBAByteImage imageS,imageS2,imageS3;
	
	BYTE *imgBuf3RGB;			//Does not appear to be used.
	BYTE *imgBuf3RGB_cam2;		//Does not appear to be used.
	BYTE *imgBuf3RGB_cam3;		//Does not appear to be used.
	
	//Memory for saving 3 camera images per defective bottle, used when save to disk is pressed.
	BYTE *imgBuf_def;
	BYTE *imgBuf_def2;
	BYTE *imgBuf_def3;

	BYTE* arr_strng_addrs[3];
	std::string temp_str;		//Hold reason for failure, but does not appear to be used.

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PicRecal)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PicRecal)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnReset();
	afx_msg void OnShowPics();
	virtual void OnOK();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSelection();
	afx_msg void OnSelchangeList1();
	afx_msg void OnSaveToDisk();
	afx_msg void OnSelchangeList2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICRECAL_H__1682219F_E7B5_498E_B44C_6C250C57A6BF__INCLUDED_)
