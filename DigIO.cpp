// DigIO.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "DigIO.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DigIO dialog


DigIO::DigIO(CWnd* pParent /*=NULL*/)
	: CDialog(DigIO::IDD, pParent)
{
	//{{AFX_DATA_INIT(DigIO)
	m_editiorejects = _T("");
	m_editconrejects = _T("");
	//}}AFX_DATA_INIT
}


void DigIO::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DigIO)
	DDX_Text(pDX, IDC_EDITIOREJECTS, m_editiorejects);
	DDX_Text(pDX, IDC_EDITIOREJECTS2, m_editconrejects);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DigIO, CDialog)
	//{{AFX_MSG_MAP(DigIO)
	ON_BN_CLICKED(IDC_BMORE, OnBmore)
	ON_BN_CLICKED(IDC_BLESS, OnBless)
	ON_BN_CLICKED(IDC_BMORE2, OnBmore2)
	ON_BN_CLICKED(IDC_BLESS2, OnBless2)
	ON_BN_CLICKED(IDC_FORCEEJECT, OnForceeject)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DigIO message handlers

BOOL DigIO::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pdigio=this;
	theapp=(CCapApp*)AfxGetApp();

	m_editiorejects.Format("%3i",theapp->SavedIORejects);
	m_editconrejects.Format("%3i",theapp->SavedConRejects);



	if(theapp->SavedForcedEjects==true)
	{
		CheckDlgButton(IDC_FORCEEJECT,1); 	
	}
	else
	{
		CheckDlgButton(IDC_FORCEEJECT,0);	
	}
	UpdateData(false);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DigIO::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}


//Customer Input Ejector Handlers
//One of the PLC inputs is designated for the customer to manually reject bottles.
//There are two ways in which this can be done. 
//1.) The customer can specify the number of bottles to be rejected after the input is set to high.
//2.) The customer can keep the input high for as long as they want the bottles to be rejected.

//Increases the number of bottles to be rejected after the PLC input is set to high.
void DigIO::OnBmore() 
{
	pframe->IORejectLimit=theapp->SavedIORejects+=1;
	if(theapp->SavedIORejects>=200)theapp->SavedIORejects=200; 	//Maximum number of bottles which can be rejected is 200.
	if(theapp->SavedIORejects<=0)theapp->SavedIORejects=0;		//Number of bottles to be rejected cannot be negative.
	m_editiorejects.Format("%3i",theapp->SavedIORejects);
	UpdateData(false);
	
}

//Decreases the number of bottles to be rejected after the PLC input is set to high.
void DigIO::OnBless() 
{
	pframe->IORejectLimit=theapp->SavedIORejects-=1;
	if(theapp->SavedIORejects>=200)theapp->SavedIORejects=200;
	if(theapp->SavedIORejects<=0)theapp->SavedIORejects=0;
	m_editiorejects.Format("%3i",theapp->SavedIORejects);
	UpdateData(false);
	
	
}
// Customer Output Consecutive Ejector Handlers
//One of the PLC outputs is designated for the customer to take action after 
//a certain number of bottles has been consecutively rejected.

//Increases the number of consecutive bottle rejects before PLC output is set to high
void DigIO::OnBmore2() 
{
	pframe->ConRejectLimit=theapp->SavedConRejects+=1;
	if(theapp->SavedConRejects>=200)theapp->SavedConRejects=200;
	if(theapp->SavedConRejects<=0)theapp->SavedConRejects=0;
	m_editconrejects.Format("%3i",theapp->SavedConRejects);
	UpdateData(false);
	
}

//decreases the number of consecutive bottle rejects before PLC output is set to high
void DigIO::OnBless2() 
{
	pframe->ConRejectLimit=theapp->SavedConRejects-=1;
	if(theapp->SavedConRejects>=200)theapp->SavedConRejects=200;
	if(theapp->SavedConRejects<=0)theapp->SavedConRejects=0;
	m_editconrejects.Format("%3i",theapp->SavedConRejects);
	UpdateData(false);
	
}

//Toggles between bottles being rejected as long as the PLC input remains high,
//and a specific number of bottles being rejected after the input is set to high.
void DigIO::OnForceeject() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	if(theapp->SavedForcedEjects==true)
	{
		theapp->SavedForcedEjects=false;
		CheckDlgButton(IDC_FORCEEJECT,0); 
	
		
	}
	else
	{
		theapp->SavedForcedEjects=true;
		CheckDlgButton(IDC_FORCEEJECT,1);
		theapp->SavedIORejects=0;
		
	}
	UpdateData(false);	
	
}
