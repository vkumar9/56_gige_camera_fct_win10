// YAxisView.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "YAxisView.h"
#include "XAxisView.h"
#include "View.h"
#include "Modify.h"
#include "Lim.h"
#include "xtra.h"
#include "band.h"
#include "Prompt3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//HIFCGRAB GrabID1;
/////////////////////////////////////////////////////////////////////////////
// CYAxisView

IMPLEMENT_DYNCREATE(CYAxisView, CView)

CYAxisView::CYAxisView()
{
}

CYAxisView::~CYAxisView()
{
}


BEGIN_MESSAGE_MAP(CYAxisView, CView)
	//{{AFX_MSG_MAP(CYAxisView)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CYAxisView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pyaxis=this;
	theapp=(CCapApp*)AfxGetApp();
	toggle=true;	//NOT USED

	//imgBuf0 = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );
	//im_result = (BYTE*)GlobalAlloc(GMEM_FIXED,640 * 480 );

	//CVisByteImage image(640,480,1,-1,imgBuf0);
	//imageBW=image;

	
		RECT rect;
	 rect.left=0;
	 rect.right=640;
	 rect.bottom=240;
	 rect.top=0;
	//toggle=true;
	//res=1;

	//Create tab menu
	m_tabmenu.Create(TCS_TABS | TCS_FIXEDWIDTH | WS_CHILD | WS_VISIBLE, rect, this, 0x1006); //IDC_TAB1

//	CTabCtrl* pmenu=m_tabmenu2;

	//Insert the same item into all tabs.
	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;			//Text will be set.
	TabCtrlItem.pszText = "Statistics";
	m_tabmenu.InsertItem( 0, &TabCtrlItem );
	TabCtrlItem.pszText = "Modify";
	m_tabmenu.InsertItem( 1, &TabCtrlItem );
	TabCtrlItem.pszText = "Modify Rear";
	m_tabmenu.InsertItem( 2, &TabCtrlItem );
	TabCtrlItem.pszText = "Limits";
	m_tabmenu.InsertItem( 3, &TabCtrlItem );
	TabCtrlItem.pszText = "User";
	m_tabmenu.InsertItem( 4, &TabCtrlItem );
	
	
	CWnd* pParent=GetParent(); 
	m_pstats=new View(); 				//Create statistics dialog box and show it as the selected tab.

	m_pstats->Create(IDD_VIEW,this);
	m_pstats->ShowWindow(SW_SHOW);
	pframe->m_pview2->UpdateDisplay();

	m_pmod=new Modify(); 				//Create modify dialog box/tab. 
	m_pmod->Create(IDD_MODIFY,this);

	m_plimit=new Lim();  				//Create limits dialog box/tab.
	m_plimit->Create(IDD_LIM,this);

	m_puser=new Xtra();  				//Create user dialog box/tab. 
	m_puser->Create(IDD_XTRA,this);

	m_pband=new Band();   				//Create rear band dialog box/tab. 
	m_pband->Create(IDD_REARBAND,this);
	
}

/////////////////////////////////////////////////////////////////////////////
// CYAxisView drawing

void CYAxisView::OnDraw(CDC* pDC)
{
	
	CDocument* pDoc = GetDocument();
	CClientDC ColorRect(this);
	CString m_1,m_2,m_3,m_4;
	CBrush Colorw, Colorr;
	//GBr.CreateSolidBrush(RGB(200,200,200));
	COLORREF Gray, Red,sqrred,sqrwhite;
	CPen GrayPen(PS_SOLID, 1, RGB(200,200,200));
	CPen BluePen(PS_SOLID, 1, RGB(0,0,255));

	CPen LGreenPen(PS_SOLID, 1, RGB(150,255,150));
	CRect a;
	
	COLORREF bluecolor = RGB(0,0,255);
	
	sqrred=RGB(200,0,0);
	sqrwhite=RGB(255,255,255);
	a.SetRect(60,0,600,90);
	Colorr.CreateSolidBrush(sqrred);
	Colorw.CreateSolidBrush(sqrwhite);
	
	//CVisImageBase& refimage=imageBW;// = pDoc->Image();
	//refimage.SetRect(0,40,640,480);
	//assert(refimage.IsValid());
	//refimage.DisplayInHdc(*pDC); 
	
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(0,0,0));
	//pDC->SelectObject(&GBr);
	//ColorRect.Rectangle(a);
	//Gray=RGB(200,200,200);
	//Red=RGB(255,0,0);
	//pDC->Draw3dRect(320,2,20,4,bluecolor,bluecolor);//top
}

/////////////////////////////////////////////////////////////////////////////
// CYAxisView diagnostics

#ifdef _DEBUG
void CYAxisView::AssertValid() const
{
	CView::AssertValid();
}

void CYAxisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CYAxisView message handlers





void CYAxisView::OnDestroy() 
{
	CView::OnDestroy();
	m_pstats->DestroyWindow();
	m_pmod->DestroyWindow();
	m_plimit->DestroyWindow();
	m_puser->DestroyWindow();
	m_pband->DestroyWindow();


	delete (m_pstats);
	delete (m_pmod);
	delete(m_plimit);
	delete(m_puser);
    delete (m_pband);	
}

BOOL CYAxisView::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	int w=m_tabmenu.GetCurSel();	//Get currently selected tab.

//	AfxMessageBox("HELLo");
	
		switch(w)
		{
		case 0:	//Statistics Tab

		m_pband->ShowWindow(SW_HIDE);
		m_pmod->ShowWindow(SW_HIDE);
		m_plimit->ShowWindow(SW_HIDE);
		m_puser->ShowWindow(SW_HIDE);
		m_pstats->ShowWindow(SW_SHOW);
		pframe->m_pview2->UpdateDisplay();		//Update statistics tab.
		pframe->m_pxaxis->Modify=false;
		pframe->m_pxaxis->InvalidateRect(NULL,false);

		UpdateData(false);
		
		break;

		case 1:	//Modify Tab

			//If user has security access to modify tab. Show modify tab.
			if(pframe->SModify==true)
			{
				pframe->m_pxaxis->Modify=true;

				m_pband->ShowWindow(SW_HIDE);
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_puser->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_SHOW);

				pframe->m_pmod->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else  //Otherwise reselect statistics tab and show dialog box asking user to login.
			{
				m_tabmenu.SetCurSel(0);

				pframe->prompt_code=1;
				Prompt3 promtp3;
				promtp3.DoModal();

				
			}

		UpdateData(false);
		break;

		case 3:	//Limits Tab
		
			//If user has security access to limits tab. Show limits tab.
			if(pframe->SLimits==true)
			{
				//pframe->m_pxaxis->Limits=true;

				m_pband->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				m_pstats->ShowWindow(SW_HIDE);
				m_puser->ShowWindow(SW_HIDE);

				m_plimit->ShowWindow(SW_SHOW);
				pframe->m_pxaxis->Modify=false;
				pframe->m_plimit->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else  //Otherwise reselect statistics tab and show dialog box asking user to login.
			{
				m_tabmenu.SetCurSel(0);

				pframe->prompt_code=1;
				Prompt3 promtp3;
				promtp3.DoModal();

				
			}

		UpdateData(false);
		break;

		case 4:	//User Tab
		
			if(pframe->SModify==true)
			{
				theapp=(CCapApp*)AfxGetApp();
				//pframe->m_pxaxis->Limits=true;
				if(theapp->inspctn[9].enable==true)
				{

					m_pmod->ShowWindow(SW_HIDE);
					m_pstats->ShowWindow(SW_HIDE);
					m_pband->ShowWindow(SW_HIDE);
					m_plimit->ShowWindow(SW_HIDE);
					m_puser->ShowWindow(SW_SHOW);

					pframe->m_pxaxis->Modify=false;
					pframe->m_px->UpdateDisplay();
					pframe->m_pxaxis->InvalidateRect(NULL,false);
				}
				else
				{
					//AfxMessageBox("User not enabled from Limits menu!",MB_OK);
				
					pframe->prompt_code=2;
					Prompt3 promtp3;
					promtp3.DoModal();

					m_tabmenu.SetCurSel(0);
					pframe->m_pxaxis->InvalidateRect(NULL,false);
				}

			}
			else
			{
				m_tabmenu.SetCurSel(0);
			//	AfxMessageBox("Password Protected");
				pframe->prompt_code=1;
				Prompt3 promtp3;
				promtp3.DoModal();

				//m_pstats->ShowWindow(SW_SHOW);
				
			}

		UpdateData(false);
		break;

		case 2:	//Rear Modify Tab
		
			//If user has security access to modify tab. Show rear modify tab.
			if(pframe->SModify==true)
			{
				//pframe->m_pxaxis->Modify=true;

				m_pband->ShowWindow(SW_SHOW);
				m_pstats->ShowWindow(SW_HIDE);
				m_plimit->ShowWindow(SW_HIDE);
				m_puser->ShowWindow(SW_HIDE);
				m_pmod->ShowWindow(SW_HIDE);
				pframe->m_pband->refresh();
				//pframe->m_pband->UpdateDisplay();
				pframe->m_pxaxis->InvalidateRect(NULL,false);
			}
			else  //Otherwise reselect statistics tab and show dialog box asking user to login.
			{
				m_tabmenu.SetCurSel(0);

			
				
			//	AfxMessageBox("Password Protected",MB_YESNO);
				pframe->prompt_code=1;
				Prompt3 promtp3;
				promtp3.DoModal();

				//m_pstats->ShowWindow(SW_SHOW);
			}

			UpdateData(false);
			break;
		}	
	return CView::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}
