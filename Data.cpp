// Data.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "Data.h"
#include "mainfrm.h"
#include "xaxisview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Data dialog


Data::Data(CWnd* pParent /*=NULL*/)
	: CDialog(Data::IDD, pParent)
{
	//{{AFX_DATA_INIT(Data)
	m_r1 = _T("");
	m_r10 = _T("");
	m_r2 = _T("");
	m_r3 = _T("");
	m_r4 = _T("");
	m_r5 = _T("");
	m_r6 = _T("");
	m_r7 = _T("");
	m_r8 = _T("");
	m_r9 = _T("");
	m_r11 = _T("");
	m_24hrtime = _T("");
	m_foil_count = _T("");
	//}}AFX_DATA_INIT
}


void Data::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Data)
	DDX_Text(pDX, IDC_R1, m_r1);
	DDX_Text(pDX, IDC_R10, m_r10);
	DDX_Text(pDX, IDC_R2, m_r2);
	DDX_Text(pDX, IDC_R3, m_r3);
	DDX_Text(pDX, IDC_R4, m_r4);
	DDX_Text(pDX, IDC_R5, m_r5);
	DDX_Text(pDX, IDC_R6, m_r6);
	DDX_Text(pDX, IDC_R7, m_r7);
	DDX_Text(pDX, IDC_R8, m_r8);
	DDX_Text(pDX, IDC_R9, m_r9);
	DDX_Text(pDX, IDC_R11, m_r11);
	DDX_Text(pDX, IDC_24HRTIME, m_24hrtime);
	DDX_Text(pDX, IDC_foil_count, m_foil_count);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Data, CDialog)
	//{{AFX_MSG_MAP(Data)
	ON_BN_CLICKED(IDPRINT, OnPrint)
	ON_BN_CLICKED(IDC_BUTTONEARLY, OnButtonearly)
	ON_BN_CLICKED(IDC_BUTTONLATE, OnButtonlate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Data message handlers

BOOL Data::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pdata=this;
	theapp=(CCapApp*)AfxGetApp();

	int totalUser=pframe->m_pxaxis->E9Cnt + pframe->m_pxaxis->E10Cnt;	//Not used

	m_24hrtime.Format("%2i",theapp->hourClock);				//Set hour of day to save data.	

	m_r1.Format("%3i",pframe->m_pxaxis->NoCapCnt);				//Set missing caps text
	m_r2.Format(" %3i",pframe->m_pxaxis->CockedCnt);		//Set cocked caps text
	m_r3.Format(" %3i",pframe->m_pxaxis->LooseCnt);			//Set loose cap text
	
	m_r4.Format(" %3i",totalUser);	//Not used
	m_r5.Format(" %3i",pframe->m_pxaxis->E5Cnt);			//Set tamper band edge count text
	m_r6.Format(" %3i",pframe->m_pxaxis->E6Cnt);			//Set tamper band surface count text
	m_r7.Format(" %3i",pframe->m_pxaxis->E7Cnt);			//Set low fill count text
	m_r8.Format(" %3i",pframe->m_pxaxis->E8Cnt);			//Set wrong color count text

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)//If job was taught as bottles with sports caps
	{
		m_r11.Format(" %3i",pframe->m_pxaxis->E3Cnt); //Display number of missing sport caps.
	}
	else //otherwise
	{
		m_r11.Format(" %3i",0);	//show 0.
	}

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==2) //When Foil Sensor is enabled
	{
		m_foil_count.Format(" %3i",pframe->m_pxaxis->E3Cnt);
	}
	else{m_foil_count.Format(" %3i",0);}
	
	m_r9.Format("%7i",pframe->Total);		//Set total number of bottles scanned text
	m_r10.Format(" %7i",pframe->Rejects);	//Set total number of bottles rejected text

	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//Does not appear to be used.
void Data::CreateNewFile()
{
	
	//d4.Format("%.3i",time/date);
	//DataFile.Write(d4,d4.GetLength());

}

//Save data at an earlier hour of the day.
void Data::OnPrint() 
{
	char buf[10];
	int filenum=theapp->SavedDataFile+=1;		//Increment file number.
	CTime thetime = CTime::GetCurrentTime();	//Get time to append to file name.

//char* CurrentName=theapp->jobinfo[pframe->CurrentJobNum].jobname;
	char* CurrentName="capdat"; //File name base.

	int mth=thetime.GetMonth();
	int day=thetime.GetDay();

	char month[2];
	char dayofweek[2];
	itoa(mth,month,10);			//Convert month number to month string
	itoa(day,dayofweek,10);		//Convert day number to day string
	
	//Generate file name string.
	CString x =itoa(filenum,buf,10);
	CString d = CurrentName;
	//CString d=theapp->jobinfo[pframe->CurrentJobNum].jobname;

	char* pFileName = "c:\\silgandata\\";//drive	
	char currentpath[60];
	strcpy(currentpath,pFileName);	//Start with file path.
	strcat(currentpath,d);			//Concatenate file name
	strcat(currentpath,month);		//Concatenate month
	strcat(currentpath,dayofweek);	//Concatenate day
	strcat(currentpath,x);			//Concatenate file number
	strcat(currentpath,".txt");		//Concatenate file extension ".txt"

	DataFile.Open(currentpath, CFile::modeWrite | CFile::modeCreate);	//Open file for writing. If it already exist overwrite it.

	int totalUser=pframe->m_pxaxis->E9Cnt + pframe->m_pxaxis->E10Cnt;	//Not used

	//Load saved values into Data dialog box member variables.
	m_r1.Format("%3i",pframe->m_pxaxis->NoCapCnt);
	m_r2.Format(" %3i",pframe->m_pxaxis->CockedCnt);

	m_r3.Format(" %3i",pframe->m_pxaxis->LooseCnt);	


	m_r4.Format(" %3i",totalUser);
	m_r5.Format(" %3i",pframe->m_pxaxis->E5Cnt);
	m_r6.Format(" %3i",pframe->m_pxaxis->E6Cnt);
	m_r7.Format(" %3i",pframe->m_pxaxis->E7Cnt);
	m_r8.Format(" %3i",pframe->m_pxaxis->E8Cnt);
	m_r9.Format(" %8i",pframe->Total);
	m_r10.Format(" %5i",pframe->Rejects);

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==2) //When Foil Sensor is enabled
	{
		m_foil_count.Format(" %3i",pframe->m_pxaxis->E3Cnt);
	}

	CString name=theapp->jobinfo[pframe->CurrentJobNum].jobname;

	//Write text file with member variables.

	//DataFile.Write(name,6);
	DataFile.Write(name,name.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);
	
	
	
	DataFile.Write("NoCap:",6);
	DataFile.Write(m_r1,m_r1.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);



	DataFile.Write("Cocked:",6);
	DataFile.Write(m_r2,m_r2.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("Loose:",6);
	DataFile.Write(m_r3,m_r3.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("User:",4);
	DataFile.Write(m_r4,m_r4.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("TBSurface:",10);
	DataFile.Write(m_r5,m_r5.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("TBEdges:",8);
	DataFile.Write(m_r6,m_r6.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("NoFill:",7);
	DataFile.Write(m_r7,m_r7.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("Color:",5);
	DataFile.Write(m_r8,m_r8.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("Total:",6);
	DataFile.Write(m_r9,m_r9.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	DataFile.Write("Rejects:",8);
	DataFile.Write(m_r10,m_r10.GetLength());
	DataFile.Write(", ",1);
	DataFile.Write("\n",1);

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==2) //When Foil Sensor is enabled
	{
		DataFile.Write("Missing Foil:",11);
		DataFile.Write(m_foil_count,m_foil_count.GetLength());
		DataFile.Write(", ",1);
		DataFile.Write("\n",1);
	}

	DataFile.Close();
	
}

/////////////////////////////////////////////////////////////////////////////
// FillerData dialog


FillerData::FillerData(CWnd* pParent /*=NULL*/)
	: CDialog(FillerData::IDD, pParent)
{
	//{{AFX_DATA_INIT(FillerData)
	m_1 = _T("");
	m_10 = _T("");
	m_11 = _T("");
	m_12 = _T("");
	m_13 = _T("");
	m_14 = _T("");
	m_15 = _T("");
	m_16 = _T("");
	m_17 = _T("");
	m_18 = _T("");
	m_19 = _T("");
	m_2 = _T("");
	m_22 = _T("");
	m_20 = _T("");
	m_21 = _T("");
	m_24 = _T("");
	m_23 = _T("");
	m_25 = _T("");
	m_3 = _T("");
	m_4 = _T("");
	m_5 = _T("");
	m_6 = _T("");
	m_7 = _T("");
	m_9 = _T("");
	m_8 = _T("");
	m_26 = _T("");
	m_27 = _T("");
	m_28 = _T("");
	m_29 = _T("");
	m_30 = _T("");
	m_31 = _T("");
	m_32 = _T("");
	m_33 = _T("");
	m_34 = _T("");
	m_35 = _T("");
	m_36 = _T("");
	m_38 = _T("");
	m_37 = _T("");
	m_39 = _T("");
	m_40 = _T("");
	m_41 = _T("");
	m_42 = _T("");
	m_43 = _T("");
	m_44 = _T("");
	m_45 = _T("");
	m_46 = _T("");
	m_47 = _T("");
	m_48 = _T("");
	m_49 = _T("");
	m_50 = _T("");
	m_51 = _T("");
	m_52 = _T("");
	m_53 = _T("");
	m_54 = _T("");
	m_55 = _T("");
	m_56 = _T("");
	m_57 = _T("");
	m_58 = _T("");
	m_59 = _T("");
	m_60 = _T("");
	m_61 = _T("");
	m_62 = _T("");
	m_63 = _T("");
	m_64 = _T("");
	m_65 = _T("");
	m_66 = _T("");
	m_67 = _T("");
	m_68 = _T("");
	m_69 = _T("");
	m_70 = _T("");
	m_71 = _T("");
	m_72 = _T("");
	m_73 = _T("");
	m_74 = _T("");
	m_c1 = _T("");
	m_c10 = _T("");
	m_c11 = _T("");
	m_c12 = _T("");
	m_c13 = _T("");
	m_c14 = _T("");
	m_c15 = _T("");
	m_c16 = _T("");
	m_c17 = _T("");
	m_c18 = _T("");
	m_c19 = _T("");
	m_c2 = _T("");
	m_c20 = _T("");
	m_c22 = _T("");
	m_c23 = _T("");
	m_c24 = _T("");
	m_c3 = _T("");
	m_c4 = _T("");
	m_c5 = _T("");
	m_c6 = _T("");
	m_c7 = _T("");
	m_c8 = _T("");
	m_c9 = _T("");
	m_c21 = _T("");
	//}}AFX_DATA_INIT
}


void FillerData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FillerData)
	DDX_Text(pDX, IDC_1, m_1);
	DDX_Text(pDX, IDC_10, m_10);
	DDX_Text(pDX, IDC_11, m_11);
	DDX_Text(pDX, IDC_12, m_12);
	DDX_Text(pDX, IDC_13, m_13);
	DDX_Text(pDX, IDC_14, m_14);
	DDX_Text(pDX, IDC_15, m_15);
	DDX_Text(pDX, IDC_16, m_16);
	DDX_Text(pDX, IDC_17, m_17);
	DDX_Text(pDX, IDC_18, m_18);
	DDX_Text(pDX, IDC_19, m_19);
	DDX_Text(pDX, IDC_2, m_2);
	DDX_Text(pDX, IDC_22, m_22);
	DDX_Text(pDX, IDC_20, m_20);
	DDX_Text(pDX, IDC_21, m_21);
	DDX_Text(pDX, IDC_24, m_24);
	DDX_Text(pDX, IDC_23, m_23);
	DDX_Text(pDX, IDC_25, m_25);
	DDX_Text(pDX, IDC_3, m_3);
	DDX_Text(pDX, IDC_4, m_4);
	DDX_Text(pDX, IDC_5, m_5);
	DDX_Text(pDX, IDC_6, m_6);
	DDX_Text(pDX, IDC_7, m_7);
	DDX_Text(pDX, IDC_9, m_9);
	DDX_Text(pDX, IDC_8, m_8);
	DDX_Text(pDX, IDC_26, m_26);
	DDX_Text(pDX, IDC_27, m_27);
	DDX_Text(pDX, IDC_28, m_28);
	DDX_Text(pDX, IDC_29, m_29);
	DDX_Text(pDX, IDC_30, m_30);
	DDX_Text(pDX, IDC_31, m_31);
	DDX_Text(pDX, IDC_32, m_32);
	DDX_Text(pDX, IDC_33, m_33);
	DDX_Text(pDX, IDC_34, m_34);
	DDX_Text(pDX, IDC_35, m_35);
	DDX_Text(pDX, IDC_36, m_36);
	DDX_Text(pDX, IDC_38, m_38);
	DDX_Text(pDX, IDC_37, m_37);
	DDX_Text(pDX, IDC_39, m_39);
	DDX_Text(pDX, IDC_40, m_40);
	DDX_Text(pDX, IDC_41, m_41);
	DDX_Text(pDX, IDC_42, m_42);
	DDX_Text(pDX, IDC_43, m_43);
	DDX_Text(pDX, IDC_44, m_44);
	DDX_Text(pDX, IDC_45, m_45);
	DDX_Text(pDX, IDC_46, m_46);
	DDX_Text(pDX, IDC_47, m_47);
	DDX_Text(pDX, IDC_48, m_48);
	DDX_Text(pDX, IDC_49, m_49);
	DDX_Text(pDX, IDC_50, m_50);
	DDX_Text(pDX, IDC_51, m_51);
	DDX_Text(pDX, IDC_52, m_52);
	DDX_Text(pDX, IDC_53, m_53);
	DDX_Text(pDX, IDC_54, m_54);
	DDX_Text(pDX, IDC_55, m_55);
	DDX_Text(pDX, IDC_56, m_56);
	DDX_Text(pDX, IDC_57, m_57);
	DDX_Text(pDX, IDC_58, m_58);
	DDX_Text(pDX, IDC_59, m_59);
	DDX_Text(pDX, IDC_60, m_60);
	DDX_Text(pDX, IDC_61, m_61);
	DDX_Text(pDX, IDC_62, m_62);
	DDX_Text(pDX, IDC_63, m_63);
	DDX_Text(pDX, IDC_64, m_64);
	DDX_Text(pDX, IDC_65, m_65);
	DDX_Text(pDX, IDC_66, m_66);
	DDX_Text(pDX, IDC_67, m_67);
	DDX_Text(pDX, IDC_68, m_68);
	DDX_Text(pDX, IDC_69, m_69);
	DDX_Text(pDX, IDC_70, m_70);
	DDX_Text(pDX, IDC_71, m_71);
	DDX_Text(pDX, IDC_72, m_72);
	DDX_Text(pDX, IDC_73, m_73);
	DDX_Text(pDX, IDC_74, m_74);
	DDX_Text(pDX, IDC_75, m_75);
	DDX_Text(pDX, IDC_76, m_76);
	DDX_Text(pDX, IDC_77, m_77);
	DDX_Text(pDX, IDC_78, m_78);
	DDX_Text(pDX, IDC_79, m_79);
	DDX_Text(pDX, IDC_80, m_80);
	DDX_Text(pDX, IDC_81, m_81);
	DDX_Text(pDX, IDC_82, m_82);
	DDX_Text(pDX, IDC_83, m_83);
	DDX_Text(pDX, IDC_84, m_84);
	DDX_Text(pDX, IDC_85, m_85);
	DDX_Text(pDX, IDC_86, m_86);
	DDX_Text(pDX, IDC_87, m_87);
	DDX_Text(pDX, IDC_88, m_88);
	DDX_Text(pDX, IDC_89, m_89);
	DDX_Text(pDX, IDC_90, m_90);
	DDX_Text(pDX, IDC_91, m_91);
	DDX_Text(pDX, IDC_92, m_92);
	DDX_Text(pDX, IDC_93, m_93);
	DDX_Text(pDX, IDC_94, m_94);
	DDX_Text(pDX, IDC_95, m_95);
	DDX_Text(pDX, IDC_96, m_96);
	DDX_Text(pDX, IDC_97, m_97);
	DDX_Text(pDX, IDC_98, m_98);
	DDX_Text(pDX, IDC_99, m_99);
	DDX_Text(pDX, IDC_100, m_100);
	DDX_Text(pDX, IDC_101, m_101);
	DDX_Text(pDX, IDC_102, m_102);
	DDX_Text(pDX, IDC_103, m_103);
	DDX_Text(pDX, IDC_104, m_104);
	DDX_Text(pDX, IDC_105, m_105);
	DDX_Text(pDX, IDC_106, m_106);
	DDX_Text(pDX, IDC_107, m_107);
	DDX_Text(pDX, IDC_108, m_108);
	DDX_Text(pDX, IDC_109, m_109);
	DDX_Text(pDX, IDC_110, m_110);
	DDX_Text(pDX, IDC_111, m_111);
	DDX_Text(pDX, IDC_112, m_112);
	DDX_Text(pDX, IDC_113, m_113);
	DDX_Text(pDX, IDC_114, m_114);
	DDX_Text(pDX, IDC_115, m_115);
	DDX_Text(pDX, IDC_116, m_116);
	DDX_Text(pDX, IDC_117, m_117);
	DDX_Text(pDX, IDC_118, m_118);
	DDX_Text(pDX, IDC_119, m_119);
	DDX_Text(pDX, IDC_120, m_120);

	DDX_Text(pDX, IDC_CC1, m_c1);
	DDX_Text(pDX, IDC_CC10, m_c10);
	DDX_Text(pDX, IDC_CC11, m_c11);
	DDX_Text(pDX, IDC_CC12, m_c12);
	DDX_Text(pDX, IDC_CC13, m_c13);
	DDX_Text(pDX, IDC_CC14, m_c14);
	DDX_Text(pDX, IDC_CC15, m_c15);
	DDX_Text(pDX, IDC_CC16, m_c16);
	DDX_Text(pDX, IDC_CC17, m_c17);
	DDX_Text(pDX, IDC_CC18, m_c18);
	DDX_Text(pDX, IDC_CC19, m_c19);
	DDX_Text(pDX, IDC_CC2, m_c2);
	DDX_Text(pDX, IDC_CC20, m_c20);
	DDX_Text(pDX, IDC_CC22, m_c22);
	DDX_Text(pDX, IDC_CC23, m_c23);
	DDX_Text(pDX, IDC_CC24, m_c24);
DDX_Text(pDX, IDC_CC25, m_c25);
	DDX_Text(pDX, IDC_CC26, m_c26);
	DDX_Text(pDX, IDC_CC27, m_c27);
	DDX_Text(pDX, IDC_CC28, m_c28);
	DDX_Text(pDX, IDC_CC29, m_c29);
DDX_Text(pDX, IDC_CC30, m_c30);
	DDX_Text(pDX, IDC_CC31, m_c31);
	DDX_Text(pDX, IDC_CC32, m_c32);
	DDX_Text(pDX, IDC_CC3, m_c3);
	DDX_Text(pDX, IDC_CC4, m_c4);
	DDX_Text(pDX, IDC_CC5, m_c5);
	DDX_Text(pDX, IDC_CC6, m_c6);
	DDX_Text(pDX, IDC_CC7, m_c7);
	DDX_Text(pDX, IDC_CC8, m_c8);
	DDX_Text(pDX, IDC_CC9, m_c9);
	DDX_Text(pDX, IDC_CC21, m_c21);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FillerData, CDialog)
	//{{AFX_MSG_MAP(FillerData)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FillerData message handlers

void FillerData::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL FillerData::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pfillerdata=this;
	theapp=(CCapApp*)AfxGetApp();
	

m_1.Format("%3i", pframe->fillerArray[1]);
m_2.Format("%3i", pframe->fillerArray[2]);
m_3.Format("%3i", pframe->fillerArray[3]);
m_4.Format("%3i", pframe->fillerArray[4]);
m_5.Format("%3i", pframe->fillerArray[5]);
m_6.Format("%3i", pframe->fillerArray[6]);
m_7.Format("%3i", pframe->fillerArray[7]);
m_8.Format("%3i", pframe->fillerArray[8]);
m_9.Format("%3i", pframe->fillerArray[9]);
m_10.Format("%3i", pframe->fillerArray[10]);

m_11.Format("%3i", pframe->fillerArray[11]);
m_12.Format("%3i", pframe->fillerArray[12]);
m_13.Format("%3i", pframe->fillerArray[13]);
m_14.Format("%3i", pframe->fillerArray[14]);
m_15.Format("%3i", pframe->fillerArray[15]);
m_16.Format("%3i", pframe->fillerArray[16]);
m_17.Format("%3i", pframe->fillerArray[17]);
m_18.Format("%3i", pframe->fillerArray[18]);
m_19.Format("%3i", pframe->fillerArray[19]);
m_20.Format("%3i", pframe->fillerArray[20]);

m_21.Format("%3i", pframe->fillerArray[21]);
m_22.Format("%3i", pframe->fillerArray[22]);
m_23.Format("%3i", pframe->fillerArray[23]);
m_24.Format("%3i", pframe->fillerArray[24]);
m_25.Format("%3i", pframe->fillerArray[25]);
m_26.Format("%3i", pframe->fillerArray[26]);
m_27.Format("%3i", pframe->fillerArray[27]);
m_28.Format("%3i", pframe->fillerArray[28]);
m_29.Format("%3i", pframe->fillerArray[29]);
m_30.Format("%3i", pframe->fillerArray[30]);

m_31.Format("%3i", pframe->fillerArray[31]);
m_32.Format("%3i", pframe->fillerArray[32]);
m_33.Format("%3i", pframe->fillerArray[33]);
m_34.Format("%3i", pframe->fillerArray[34]);
m_35.Format("%3i", pframe->fillerArray[35]);
m_36.Format("%3i", pframe->fillerArray[36]);
m_37.Format("%3i", pframe->fillerArray[37]);
m_38.Format("%3i", pframe->fillerArray[38]);
m_39.Format("%3i", pframe->fillerArray[39]);

m_40.Format("%3i", pframe->fillerArray[40]);
m_41.Format("%3i", pframe->fillerArray[41]);
m_42.Format("%3i", pframe->fillerArray[42]);
m_43.Format("%3i", pframe->fillerArray[43]);
m_44.Format("%3i", pframe->fillerArray[44]);
m_45.Format("%3i", pframe->fillerArray[45]);
m_46.Format("%3i", pframe->fillerArray[46]);
m_47.Format("%3i", pframe->fillerArray[47]);
m_48.Format("%3i", pframe->fillerArray[48]);
m_49.Format("%3i", pframe->fillerArray[49]);

m_50.Format("%3i", pframe->fillerArray[50]);
m_51.Format("%3i", pframe->fillerArray[51]);
m_52.Format("%3i", pframe->fillerArray[52]);
m_53.Format("%3i", pframe->fillerArray[53]);
m_54.Format("%3i", pframe->fillerArray[54]);
m_55.Format("%3i", pframe->fillerArray[55]);
m_56.Format("%3i", pframe->fillerArray[56]);
m_57.Format("%3i", pframe->fillerArray[57]);
m_58.Format("%3i", pframe->fillerArray[58]);
m_59.Format("%3i", pframe->fillerArray[59]);

m_60.Format("%3i", pframe->fillerArray[60]);
m_61.Format("%3i", pframe->fillerArray[61]);
m_62.Format("%3i", pframe->fillerArray[62]);
m_63.Format("%3i", pframe->fillerArray[63]);
m_64.Format("%3i", pframe->fillerArray[64]);
m_65.Format("%3i", pframe->fillerArray[65]);
m_66.Format("%3i", pframe->fillerArray[66]);
m_67.Format("%3i", pframe->fillerArray[67]);
m_68.Format("%3i", pframe->fillerArray[68]);
m_69.Format("%3i", pframe->fillerArray[69]);

m_70.Format("%3i", pframe->fillerArray[70]);
m_71.Format("%3i", pframe->fillerArray[71]);
m_72.Format("%3i", pframe->fillerArray[72]);
m_73.Format("%3i", pframe->fillerArray[73]);
m_74.Format("%3i", pframe->fillerArray[74]);
m_75.Format("%3i", pframe->fillerArray[75]);
m_76.Format("%3i", pframe->fillerArray[76]);
m_77.Format("%3i", pframe->fillerArray[77]);
m_78.Format("%3i", pframe->fillerArray[78]);
m_79.Format("%3i", pframe->fillerArray[79]);

m_80.Format("%3i", pframe->fillerArray[80]);
m_81.Format("%3i", pframe->fillerArray[81]);
m_82.Format("%3i", pframe->fillerArray[82]);
m_83.Format("%3i", pframe->fillerArray[83]);
m_84.Format("%3i", pframe->fillerArray[84]);
m_85.Format("%3i", pframe->fillerArray[85]);
m_86.Format("%3i", pframe->fillerArray[86]);
m_87.Format("%3i", pframe->fillerArray[87]);
m_88.Format("%3i", pframe->fillerArray[88]);
m_89.Format("%3i", pframe->fillerArray[89]);

m_90.Format("%3i", pframe->fillerArray[90]);
m_91.Format("%3i", pframe->fillerArray[91]);
m_92.Format("%3i", pframe->fillerArray[92]);
m_93.Format("%3i", pframe->fillerArray[93]);
m_94.Format("%3i", pframe->fillerArray[94]);
m_95.Format("%3i", pframe->fillerArray[95]);
m_96.Format("%3i", pframe->fillerArray[96]);
m_97.Format("%3i", pframe->fillerArray[97]);
m_98.Format("%3i", pframe->fillerArray[98]);
m_99.Format("%3i", pframe->fillerArray[99]);

m_100.Format("%3i", pframe->fillerArray[100]);
m_101.Format("%3i", pframe->fillerArray[101]);
m_102.Format("%3i", pframe->fillerArray[102]);
m_103.Format("%3i", pframe->fillerArray[103]);
m_104.Format("%3i", pframe->fillerArray[104]);
m_105.Format("%3i", pframe->fillerArray[105]);
m_106.Format("%3i", pframe->fillerArray[106]);
m_107.Format("%3i", pframe->fillerArray[107]);
m_108.Format("%3i", pframe->fillerArray[108]);
m_109.Format("%3i", pframe->fillerArray[109]);

m_110.Format("%3i", pframe->fillerArray[110]);
m_111.Format("%3i", pframe->fillerArray[111]);
m_112.Format("%3i", pframe->fillerArray[112]);
m_113.Format("%3i", pframe->fillerArray[113]);
m_114.Format("%3i", pframe->fillerArray[114]);
m_115.Format("%3i", pframe->fillerArray[115]);
m_116.Format("%3i", pframe->fillerArray[116]);
m_117.Format("%3i", pframe->fillerArray[117]);
m_118.Format("%3i", pframe->fillerArray[118]);
m_119.Format("%3i", pframe->fillerArray[119]);

m_120.Format("%3i", pframe->fillerArray[120]);


m_c1.Format("%3i", pframe->capperArray[1]);
m_c2.Format("%3i", pframe->capperArray[2]);
m_c3.Format("%3i", pframe->capperArray[3]);
m_c4.Format("%3i", pframe->capperArray[4]);
m_c5.Format("%3i", pframe->capperArray[5]);
m_c6.Format("%3i", pframe->capperArray[6]);
m_c7.Format("%3i", pframe->capperArray[7]);
m_c8.Format("%3i", pframe->capperArray[8]);
m_c9.Format("%3i", pframe->capperArray[9]);
m_c10.Format("%3i", pframe->capperArray[10]);

m_c11.Format("%3i", pframe->capperArray[11]);
m_c12.Format("%3i", pframe->capperArray[12]);
m_c13.Format("%3i", pframe->capperArray[13]);
m_c14.Format("%3i", pframe->capperArray[14]);
m_c15.Format("%3i", pframe->capperArray[15]);
m_c16.Format("%3i", pframe->capperArray[16]);
m_c17.Format("%3i", pframe->capperArray[17]);
m_c18.Format("%3i", pframe->capperArray[18]);
m_c19.Format("%3i", pframe->capperArray[19]);
m_c20.Format("%3i", pframe->capperArray[20]);
m_c21.Format("%3i", pframe->capperArray[21]);
m_c22.Format("%3i", pframe->capperArray[22]);
m_c23.Format("%3i", pframe->capperArray[23]);
m_c24.Format("%3i", pframe->capperArray[24]);

m_c25.Format("%3i", pframe->capperArray[25]);
m_c26.Format("%3i", pframe->capperArray[26]);
m_c27.Format("%3i", pframe->capperArray[27]);
m_c28.Format("%3i", pframe->capperArray[28]);
m_c29.Format("%3i", pframe->capperArray[29]);
m_c30.Format("%3i", pframe->capperArray[30]);
m_c31.Format("%3i", pframe->capperArray[31]);
m_c32.Format("%3i", pframe->capperArray[32]);

		UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void Data::OnButtonearly() 
{
	theapp->hourClock-=1;
	if(theapp->hourClock<=1) theapp->hourClock=1;
	m_24hrtime.Format("%2i",theapp->hourClock);
	UpdateData(false);
	
}

void Data::OnButtonlate() 
{
	theapp->hourClock+=1;
	if(theapp->hourClock>=24) theapp->hourClock=24;
	m_24hrtime.Format("%2i",theapp->hourClock);
	UpdateData(false);
	
	
}

void Data::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
