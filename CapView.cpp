// CapView.cpp : implementation of the CCapView class
//

#include "stdafx.h"
#include "Cap.h"
#include "mainfrm.h"
#include "CapDoc.h"
#include "CapView.h"
#include "xaxisview.h"
#include "View.h"
#include "serial.h"
#include "camera.h"
#include "prompt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCapView

IMPLEMENT_DYNCREATE(CCapView, CFormView)

BEGIN_MESSAGE_MAP(CCapView, CFormView)
	//{{AFX_MSG_MAP(CCapView)
	ON_BN_CLICKED(IDC_TRIG1, OnTrig1)
	ON_BN_CLICKED(IDC_TRIG2, OnTrig2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RTOT, OnRtot)
	ON_BN_CLICKED(IDC_FREEZE, OnFreeze)
	ON_BN_CLICKED(IDC_SHOWJOB, OnShowjob)
	ON_BN_CLICKED(IDC_REJECTALL, OnRejectall)
	ON_BN_CLICKED(IDC_ONLINE, OnOnline)
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_BN_CLICKED(IDC_CONT, OnCont)
	ON_BN_CLICKED(IDC_FREEZE2, OnFreeze2)
	ON_BN_CLICKED(IDC_FBUTTON, OnFbutton)
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_BN_CLICKED(IDC_TESTEJECT, OnTesteject)
	ON_BN_CLICKED(IDC_FMORE, OnFmore)
	ON_BN_CLICKED(IDC_FLESS, OnFless)
	ON_BN_CLICKED(IDC_BUTTON1, OnC2Up)
	ON_BN_CLICKED(IDC_BUTTON5, OnC2Dwn)
	ON_BN_CLICKED(IDC_BUTTON9, OnC3Up)
	ON_BN_CLICKED(IDC_BUTTON11, OnC3Dwn)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_EN_SETFOCUS(IDC_EDIT2, OnSetfocusEdit2)
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT7, OnChangeEdit7)
	ON_EN_CHANGE(IDC_EDIT8, OnChangeEdit8)
	ON_BN_CLICKED(IDC_BUTTON3, OnResetTotCnt)
	ON_BN_CLICKED(IDC_CHECK6, OnTempPlc)
	ON_BN_CLICKED(IDC_CHECK9, OnTempUSB)
	ON_BN_CLICKED(IDC_CHECK7, OnLoadMultiple)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_INSPDELAYUP, OnInspdelayup)
	ON_BN_CLICKED(IDC_INSPDELAYDW, OnInspdelaydw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCapView construction/destruction

CCapView::CCapView()
	: CFormView(CCapView::IDD)
{
	//{{AFX_DATA_INIT(CCapView)
	m_total = _T("");
	m_rejects = _T("");
	m_status = _T("");
	m_result1 = _T("");
	m_result2 = _T("");
	m_result3 = _T("");
	m_result4 = _T("");
	m_result5 = _T("");
	m_result6 = _T("");
	m_result7 = _T("");
	m_result8 = _T("");
	m_result9 = _T("");
	m_result10 = _T("");
	m_result11 = _T("");
	m_limhl = _T("");
	m_limhm = _T("");
	m_limhr = _T("");
	m_limdiff = _T("");
	m_limfill = _T("");
	m_limtb = _T("");
	m_limu = _T("");
	m_limdiff2 = _T("");
	m_limfill2 = _T("");
	m_limhl2 = _T("");
	m_limhm2 = _T("");
	m_limhr2 = _T("");
	m_limtb2 = _T("");
	m_limu2 = _T("");
	m_limtb3 = _T("");
	m_limtb4 = _T("");
	m_limux = _T("");
	m_limux2 = _T("");
	m_bwcount = _T("");
	m_colorcount = _T("");
	m_timedtrigger = _T("");
	m_time = _T("");
	m_time1 = _T("");
	m_time2 = _T("");
	m_time3 = _T("");
	m_time4 = _T("");
	m_name3 = _T("");
	m_h1 = _T("");
	m_h2 = _T("");
	m_h3 = _T("");
	m_h4 = _T("");
	m_h5 = _T("");
	m_h6 = _T("");
	m_h7 = _T("");
	m_h8 = _T("");
	m_h9 = _T("");
	m_l1 = _T("");
	m_l2 = _T("");
	m_l3 = _T("");
	m_l4 = _T("");
	m_l5 = _T("");
	m_l6 = _T("");
	m_l7 = _T("");
	m_l8 = _T("");
	m_l9 = _T("");
	m_insp1 = _T("");
	m_insp2 = _T("");
	m_insp3 = _T("");
	m_insp4 = _T("");
	m_insp5 = _T("");
	m_insp6 = _T("");
	m_insp7 = _T("");
	m_insp8 = _T("");
	m_insp9 = _T("");
	m_timedfiller = _T("");
	m_edit1 = _T("");
	m_edit5 = _T("");
	m_editcam1 = _T("");
	m_editcam2 = _T("");
	m_editcam3 = _T("");
	m_inspdelay = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CCapView::~CCapView()
{
}

void CCapView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCapView)
	DDX_Control(pDX, IDC_EDIT8, m_camser3);
	DDX_Control(pDX, IDC_EDIT7, m_camser2);
	DDX_Control(pDX, IDC_EDIT2, m_camser1);
	DDX_Control(pDX, IDC_L9, m_ul9);
	DDX_Control(pDX, IDC_H9, m_uh9);
	DDX_Control(pDX, IDC_RESULT10, m_u2e);
	DDX_Control(pDX, IDC_RESULT9, m_u2d);
	DDX_Text(pDX, IDC_TOTAL, m_total);
	DDX_Text(pDX, IDC_REJECTS, m_rejects);
	DDX_Text(pDX, IDC_STATUS, m_status);
	DDX_Text(pDX, IDC_RESULT1, m_result1);
	DDX_Text(pDX, IDC_RESULT2, m_result2);
	DDX_Text(pDX, IDC_RESULT3, m_result3);
	DDX_Text(pDX, IDC_RESULT4, m_result4);
	DDX_Text(pDX, IDC_RESULT5, m_result5);
	DDX_Text(pDX, IDC_RESULT6, m_result6);
	DDX_Text(pDX, IDC_RESULT7, m_result7);
	DDX_Text(pDX, IDC_RESULT8, m_result8);
	DDX_Text(pDX, IDC_RESULT9, m_result9);
	DDX_Text(pDX, IDC_RESULT10, m_result10);
	DDX_Text(pDX, IDC_Result11, m_result11);
	DDX_Control(pDX, IDC_RESULT8, m_result8s);
	DDX_Text(pDX, IDC_BWCOUNT, m_bwcount);
	DDX_Text(pDX, IDC_COLORCOUNT, m_colorcount);
	DDX_Text(pDX, IDC_TIMEDTRIGGER, m_timedtrigger);
	DDX_Text(pDX, IDC_TIME, m_time);
	DDX_Text(pDX, IDC_TIME1, m_time1);
	DDX_Text(pDX, IDC_TIME2, m_time2);
	DDX_Text(pDX, IDC_TIME3, m_time3);
	DDX_Text(pDX, IDC_TIME4, m_time4);
	DDX_Text(pDX, IDC_H1, m_h1);
	DDX_Text(pDX, IDC_H2, m_h2);
	DDX_Text(pDX, IDC_H3, m_h3);
	DDX_Text(pDX, IDC_H4, m_h4);
	DDX_Text(pDX, IDC_H5, m_h5);
	DDX_Text(pDX, IDC_H6, m_h6);
	DDX_Text(pDX, IDC_H7, m_h7);
	DDX_Text(pDX, IDC_H8, m_h8);
	DDX_Text(pDX, IDC_H9, m_h9);
	DDX_Text(pDX, IDC_L1, m_l1);
	DDX_Text(pDX, IDC_L2, m_l2);
	DDX_Text(pDX, IDC_L3, m_l3);
	DDX_Text(pDX, IDC_L4, m_l4);
	DDX_Text(pDX, IDC_L5, m_l5);
	DDX_Text(pDX, IDC_L6, m_l6);
	DDX_Text(pDX, IDC_L7, m_l7);
	DDX_Text(pDX, IDC_L8, m_l8);
	DDX_Text(pDX, IDC_L9, m_l9);
	DDX_Text(pDX, IDC_INSP1, m_insp1);
	DDX_Text(pDX, IDC_INSP2, m_insp2);
	DDX_Text(pDX, IDC_INSP3, m_insp3);
	DDX_Text(pDX, IDC_INSP4, m_insp4);
	DDX_Text(pDX, IDC_INSP5, m_insp5);
	DDX_Text(pDX, IDC_INSP6, m_insp6);
	DDX_Text(pDX, IDC_INSP7, m_insp7);
	DDX_Text(pDX, IDC_INSP8, m_insp8);
	DDX_Text(pDX, IDC_INSP9, m_insp9);
	DDX_Text(pDX, IDC_TIMEDFILLER, m_timedfiller);
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_EDIT5, m_edit5);
	DDX_Text(pDX, IDC_EDIT2, m_editcam1);
	DDX_Text(pDX, IDC_EDIT7, m_editcam2);
	DDX_Text(pDX, IDC_EDIT8, m_editcam3);
	DDX_Text(pDX, IDC_INSPDELAY, m_inspdelay);
	//}}AFX_DATA_MAP
}

BOOL CCapView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCapView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pview=this;
	theapp=(CCapApp*)AfxGetApp();
	toggle=true;
	m_status="Enter a job";
	EjectAll=false;
	ToggleColor=false;

	/////////////////////////////////////
	//ML- TO SHOW INSP DELAY TIME
	m_inspdelay.Format("%2i",theapp->inspDelay);
	/////////////////////////////////////

	SetTimer(2,500,NULL);	//Toggle ejector off-line background from gray to red.
	m_timedtrigger.Format("%i",theapp->timedTrigger);
	m_timedfiller.Format("%i",theapp->fillerPulse);
	SetTimer(3,800,NULL);

	if(theapp->tempSelect == 1) // for PLC Temp (1 is the default value for this variable)
	{

	CheckDlgButton(IDC_CHECK6,1);
	CheckDlgButton(IDC_CHECK9,0);
	}
	else if (theapp->tempSelect==2)
	{
	CheckDlgButton(IDC_CHECK6,0);
	CheckDlgButton(IDC_CHECK9,1);
	}
	//if(EjectAll==false){CheckDlgButton(IDC_EJECT2,0); m_eject2.Format("%s","Eject Auto");}else{CheckDlgButton(IDC_EJECT2,1); m_eject2.Format("%s","Eject All");}
	
	UpdateData(false);


}

/////////////////////////////////////////////////////////////////////////////
// CCapView diagnostics

#ifdef _DEBUG
void CCapView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCapView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCapDoc* CCapView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCapDoc)));
	return (CCapDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCapView message handlers

//Used to make color backgrounds for the ejector on-line/off-line buttons. 1->Red, 2->Green, 3->Gray
void CCapView::OnlineBox(int BoxColor)
{
	int x=132;
	int y=429;
	int x2=x+10;
	int y2=y+65;
	CRect a, b, c, d;
	//CRgn s;
	COLORREF sqrred, sqrgreen, sqrgray;
	CBrush Colorr, Colorg, Colorb;
	CClientDC ColorRect(this);

	//aa.SetRect(83,90,125,125);
	
	a.SetRect(x,y,x2,y2);
	b.SetRect(x+44,y,x2+44,y2);

	c.SetRect(x,y,x2+44,y+3);
	d.SetRect(x,y2-3,x2+44,y2);
	
	sqrred=RGB(200,0,0);
	sqrgreen=RGB(0,200,0);
	sqrgray=RGB(200,200,200);
	Colorr.CreateSolidBrush(sqrred);
	Colorg.CreateSolidBrush(sqrgreen);
	Colorb.CreateSolidBrush(sqrgray);

	switch (BoxColor)
	{
		case 1:  //Create red rectangles
		
			ColorRect.SelectObject(&Colorr);	
			ColorRect.FillRect(a,&Colorr);
			ColorRect.FillRect(b,&Colorr);
			ColorRect.FillRect(c,&Colorr);
			ColorRect.FillRect(d,&Colorr);
			break;
		
		case 2:  //Create green rectangles
		
			ColorRect.SelectObject(&Colorg);	
			ColorRect.FillRect(a,&Colorg);
			ColorRect.FillRect(b,&Colorg);
			ColorRect.FillRect(c,&Colorg);
			ColorRect.FillRect(d,&Colorg);
			break;
		
		case 3:  //Create gray rectangles
		
			ColorRect.SelectObject(&Colorb);	
			ColorRect.FillRect(a,&Colorb);
			ColorRect.FillRect(b,&Colorb);
			ColorRect.FillRect(c,&Colorb);
			ColorRect.FillRect(d,&Colorb);
			break;

	}
	UpdateData(false);
}


//Update all the pass/fail color rectangles, inspection result values, total scans, number rejected, upper limits, lower limits, inspections names.
void CCapView::UpdateDisplay()
{
	//Generate pass/fail color squares.

	//first square coordinates.
	int x=160;
	int y=124;
	int x2=x+20;
	int y2=y+20;

	//pass/fail color squares.
	CRect aa, a, b, c, d, e, f, g, h, i,j,k,l;
	//CRgn s;

	//Colors and brushes
	COLORREF sqrred, sqrgreen, sqrgray;
	CBrush Colorr, //Red rectangle 
		   Colorg, //Green rectangle 
		   Colorb;	//Gray rectangle 
	CClientDC ColorRect(this);

	//aa.SetRect(83,90,125,125);
	
	//Position rectangles
	a.SetRect(130,78,170,103);
	b.SetRect(x,y+30,x2,y2+30);
	c.SetRect(x,y+61,x2,y2+61);
	d.SetRect(x,y+92,x2,y2+92);

	e.SetRect(x,y+123,x+10,y2+123);
	l.SetRect(x+13,y+123,x+23,y2+123);

	f.SetRect(x,y+154,x2,y2+154);
	g.SetRect(x,y+185,x2,y2+185);
	h.SetRect(x,y+216,x2,y2+216);
	i.SetRect(x,y+247,x2,y2+247);
	j.SetRect(x,y+278,x2,y2+278);
	k.SetRect(x+10,y+278,x2,y2+278);

//s.CreateRectRgn(60,58,126,74);

	sqrred=RGB(200,0,0);
	sqrgreen=RGB(0,200,0);
	sqrgray=RGB(200,200,200);-
	Colorr.CreateSolidBrush(sqrred);
	Colorg.CreateSolidBrush(sqrgreen);
	Colorb.CreateSolidBrush(sqrgray);

	//If no bottles have been scanned reset all the fields.
	if(pframe->Total<=0 )
	{
		m_total.Format(" %.5i",0);
		//m_time.Format(" %.5f",0);
		if (pframe->InspOpen==false) //If the configure inspection dialog box is not opened.  Set all the result values to 0.
		{
			m_result1.Format("%3.2f",0);
			m_result2.Format("%3.2f",0);
			m_result3.Format("%3.2f",0);
			m_result4.Format("%3.2f",0);
			m_result11.Format("%3.2f",0);
			m_result5.Format("%3.2f",0);
			m_result6.Format("%3.2f",0);
			m_result7.Format("%3.2f",0);
			m_result8.Format("%3.2f",0);
			m_result9.Format("%3.2i",0);
			m_result10.Format("%3.2i",0);
		}
		else //Otherwise load saved results
		{
			m_result1.Format("%3.2f",pframe->m_pxaxis->Result1);
			m_result2.Format("%3.2f",pframe->m_pxaxis->Result2);
			m_result3.Format("%3.2f",pframe->m_pxaxis->Result3);
			if(theapp->checkRearHeight ==true)
			{	m_result4.Format("%3.2f",theapp->cam2diff);
				m_result11.Format("%3.2f",theapp->cam3diff);
			}
			m_result5.Format("%3.2f",pframe->m_pxaxis->Result5);
			m_result6.Format("%3.2f",pframe->m_pxaxis->Result6);
			m_result7.Format("%3.2f",pframe->m_pxaxis->Result7);
			m_result8.Format("%3.2f",pframe->m_pxaxis->Result8);
			m_result9.Format("%3.2i",pframe->m_pxaxis->Result9);
			m_result10.Format("%3.2i",pframe->m_pxaxis->Result10);
		}
		m_rejects.Format(" %.5i",0);
		//m_jnum.Format(" %1i",pframe->CurrentJobNum);
		ColorRect.SelectObject(&Colorb);
		
		//ColorRect.FrameRgn(&s,&Colorb,10,10);
		ColorRect.FillRect(a,&Colorb);
		ColorRect.FillRect(b,&Colorb);
		ColorRect.FillRect(c,&Colorb);
		ColorRect.FillRect(d,&Colorb);

		ColorRect.FillRect(e,&Colorb);
		ColorRect.FillRect(l,&Colorb);

		ColorRect.FillRect(f,&Colorb);
		ColorRect.FillRect(g,&Colorb);
		ColorRect.FillRect(h,&Colorb);
		ColorRect.FillRect(i,&Colorb);
		ColorRect.FillRect(j,&Colorb);
		ColorRect.FillRect(k,&Colorb);
	}
	else   //If bottles have been scanned the fields based on the saved data and last inspection fields.
	{
		ColorRect.Rectangle(a);
		if((theapp->inspctn[1].enable==false) && 
		   (theapp->inspctn[2].enable==false) && 
		   (theapp->inspctn[3].enable==false) && 
		   (theapp->inspctn[4].enable==false) &&
		   (theapp->inspctn[5].enable==false) && 
		   (theapp->inspctn[6].enable==false) &&
		   (theapp->inspctn[7].enable==false) &&
		   (theapp->inspctn[8].enable==false) && 
		   (theapp->inspctn[9].enable==false))
		{
				ColorRect.SelectObject(&Colorb);
				ColorRect.FillRect(a,&Colorb);	//Color the overall pass/fail rectangle gray.
		}
		else  //If some inspections are enabled.
		{
			//If non of the turned on inspections failed
			if(pframe->m_pxaxis->BadCap == false)
			{
				ColorRect.SelectObject(&Colorg);
				//ColorRect.FrameRgn(&s,&Colorg,10,10);
				ColorRect.FillRect(a,&Colorg);	//Set the overall pass/fail rectangle to green. 
				//m_status="Good Bag Seal";
			}
			else	//If some of the turned on inspections failed
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(a,&Colorr);	//Set the overall pass/fail rectangle to red. 
				//ColorRect.FrameRgn(&s,&Colorr,10,10);
				//m_status="Bad Bag Seal";
			}
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 1
		ColorRect.Rectangle(b);
		if(theapp->inspctn[1].enable==true)	//If inspection 1 is enabled.
		{
			if(pframe->m_pxaxis->inspResultOK[1] == true)	//If this inspection passed.
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(b,&Colorg);				//Color the corresponding rectangle green.
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(b,&Colorr);				//Color the corresponding rectangle red.
				//m_status="Bad Bag Seal";
			}
		}
		else
		{	
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(b,&Colorb);					//Color the corresponding rectangle gray.
			//m_result4.Format("%2d",0);
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 2
		ColorRect.Rectangle(c);

		if(theapp->inspctn[2].enable==true)
		{
			if(pframe->m_pxaxis->inspResultOK[2] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(c,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(c,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(c,&Colorb);
		
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 3
		ColorRect.Rectangle(d);
		if(theapp->inspctn[3].enable==true)
		{
			if(pframe->m_pxaxis->inspResultOK[3] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(d,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(d,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(d,&Colorb);
		
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 4,	cocked cap camera 2
		ColorRect.Rectangle(e);
		if(theapp->inspctn[4].enable==true)
		{
			if(theapp->checkRearHeight == true)
			{
				if(theapp->cam2diff <= theapp->inspctn[4].lmax) //heapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff) //	if(pframe->m_pxaxis->cam2cocked == false)
				{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(e,&Colorg);
				}//m_status="Good Bag Seal";
			
				else
				{
				
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(e,&Colorr);
				//m_status="Bad Bag Seal";
				}

				m_result4.Format("%2d",theapp->cam2diff);
    
			}
		}
		
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(e,&Colorb);
			m_result4.Format("%2d",0);
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 4, cocked cap camera 3
		ColorRect.Rectangle(l);
		if(theapp->inspctn[4].enable==true)
		{
			if(theapp->checkRearHeight == true)
			{
				if(theapp->cam3diff <= theapp->inspctn[4].lmax) //heapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff) //	if(pframe->m_pxaxis->cam2cocked == false)
				{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(l,&Colorg);
				}//m_status="Good Bag Seal";
			
				else
				{
				
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(l,&Colorr);
				//m_status="Bad Bag Seal";
				}

				m_result11.Format("%2d",theapp->cam3diff);
    
			}
		}
			else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(l,&Colorb);
			m_result11.Format("%2d",0);
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 5
		ColorRect.Rectangle(f);
		if(theapp->inspctn[5].enable==true)
		{
		
			if(pframe->m_pxaxis->inspResultOK[5] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(f,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(f,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(f,&Colorb);
		}
		//if(theapp->Single==false)
		//{
	
		
		
		//--------------------------------------------------------------------------------------------------
		//For inspection 6		
		ColorRect.Rectangle(g);
		if(theapp->inspctn[6].enable==true)
		{
			if(pframe->m_pxaxis->inspResultOK[6] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(g,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(g,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}

		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(g,&Colorb);
		
		
		}
		
		//--------------------------------------------------------------------------------------------------
		//For inspection 7
		ColorRect.Rectangle(h);
		if(theapp->inspctn[7].enable==true)
		{
		
			if(pframe->m_pxaxis->inspResultOK[7] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(h,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(h,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
				ColorRect.SelectObject(&Colorb);
				ColorRect.FillRect(h,&Colorb);
		
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 8
		ColorRect.Rectangle(i);
		if(theapp->inspctn[8].enable==true)
		{

			if(pframe->m_pxaxis->inspResultOK[8] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(i,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(i,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(i,&Colorb);
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 9
		ColorRect.Rectangle(j);

		if(theapp->inspctn[9].enable==true)
		{

			if(pframe->m_pxaxis->inspResultOK[9] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(j,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(j,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(j,&Colorb);
		}

		//--------------------------------------------------------------------------------------------------
		//For inspection 9
		ColorRect.Rectangle(k);
		if(theapp->inspctn[9].enable==true)
		{

			if(pframe->m_pxaxis->inspResultOK[10] == true)
			{
				ColorRect.SelectObject(&Colorg);
				ColorRect.FillRect(k,&Colorg);
				//m_status="Good Bag Seal";
			}
			else
			{
				ColorRect.SelectObject(&Colorr);
				ColorRect.FillRect(k,&Colorr);
				//m_status="Bad Bag Seal";
			}
		}
		else
		{
			ColorRect.SelectObject(&Colorb);
			ColorRect.FillRect(k,&Colorb);
		}
    
		//m_jnum.Format("%1i",pframe->CurrentJobNum);
		m_total.Format("%5i",pframe->Total);	//Update the gui variable holding the total number of bottles scanned.
		
		//Update all the inspection results.
		m_result1.Format("%3.2f",pframe->m_pxaxis->Result1);
		m_result2.Format("%3.2f",pframe->m_pxaxis->Result2);
		m_result3.Format("%3.2f",pframe->m_pxaxis->Result3);
	//	m_result4.Format("%3.2f",pframe->m_pxaxis->Result4);
		m_result5.Format("%3.2f",pframe->m_pxaxis->Result5);
		m_result6.Format("%3.2f",pframe->m_pxaxis->Result6);
		m_result7.Format("%3.2f",pframe->m_pxaxis->Result7);
		m_result8.Format("%3.2f",pframe->m_pxaxis->Result8);
		m_result9.Format("%3.2i",pframe->m_pxaxis->Result9);
		m_result10.Format("%3.2i",pframe->m_pxaxis->Result10);

		//Update inspection lower limits.
		m_l1.Format("%3i",theapp->inspctn[1].lmin);
		m_l2.Format("%3i",theapp->inspctn[2].lmin);
		m_l3.Format("%3i",theapp->inspctn[3].lmin);
		m_l4.Format("%3i",theapp->inspctn[4].lmin);
		//m_l4.Format("%3i",0);
		m_l5.Format("%3i",theapp->inspctn[5].lmin);
		m_l6.Format("%3i",theapp->inspctn[6].lmin);
		m_l7.Format("%3i",theapp->inspctn[7].lmin);
		m_l8.Format("%3i",theapp->inspctn[8].lmin);
		m_l9.Format("%3i",theapp->inspctn[9].lmin);

	
		//Update inspection upper limits.
		m_h1.Format("%3i",theapp->inspctn[1].lmax);
		m_h2.Format("%3i",theapp->inspctn[2].lmax);
		m_h3.Format("%3i",theapp->inspctn[3].lmax);
		//m_h4.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff);
		m_h4.Format("%3i",theapp->inspctn[4].lmax);

		m_h5.Format("%3i",theapp->inspctn[5].lmax);
		m_h6.Format("%3i",theapp->inspctn[6].lmax);
		m_h7.Format("%3i",theapp->inspctn[7].lmax);
		m_h8.Format("%3i",theapp->inspctn[8].lmax);
		m_h9.Format("%3i",theapp->inspctn[9].lmax);

		m_rejects.Format(" %5i",pframe->Rejects);	//Update the gui variable holding the total number of bottles rejected.
	}

	//User inspection, NOT USED
	if(theapp->inspctn[9].enable==true)
	{
	//	m_u2a.ShowWindow(SW_SHOW);
		m_uh9.ShowWindow(SW_SHOW);
		m_ul9.ShowWindow(SW_SHOW);
		m_u2d.ShowWindow(SW_SHOW);
		m_u2e.ShowWindow(SW_SHOW);
	}
	else
	{
		ColorRect.SelectObject(&Colorb);
		ColorRect.FillRect(k,&Colorb); 
		ColorRect.FillRect(j,&Colorb); 
		//m_u2a.ShowWindow(SW_HIDE);
		m_uh9.ShowWindow(SW_HIDE);
		m_ul9.ShowWindow(SW_HIDE);
		m_u2d.ShowWindow(SW_HIDE);
		m_u2e.ShowWindow(SW_HIDE);
	}
	//if(theapp->SavedEjectDisable==true){CheckDlgButton(IDC_EJECT2,1); m_eject2.Format("%s","Eject is disabled");}else{CheckDlgButton(IDC_EJECT2,0);m_eject2.Format("%s","Eject is enabled");}
	m_bwcount.Format("%.3i",pframe->m_pxaxis->BWCount);	//NOT USED
	//m_colorcount.Format("%.3i",pframe->m_pxaxis->colorCount);


	//Update the cocked cap offsets.
	m_edit1.Format("%3i",theapp->cam2_ht_offset);
	m_edit5.Format("%3i",theapp->cam3_ht_offset);

	//Update displayed camera serial numbers
	if(theapp->CamSer1 >0 && theapp->CamSer2 >0 && theapp->CamSer3 >0)
	{
		m_editcam1.Format("%i",theapp->CamSer1);
		m_editcam2.Format("%i",theapp->CamSer2);
		m_editcam3.Format("%i",theapp->CamSer3);
	}

	/*
	m_time1.Format(" %.5f",pframe->m_pxaxis->spanElapsed4);
	m_time2.Format(" %.5f",pframe->m_pxaxis->spanElapsed4   );
	m_time3.Format(" %3i",pframe->m_pxaxis->tot_bottle_cnt);
*/

	//Update performance info
	m_time4.Format(" %.2f",pframe->m_pxaxis->spanElapsed0);				//XaxisView.Inspect()
	m_time1.Format(" %.2f",pframe->m_pxaxis->timeMicroSecondC1I0);		//XAxisView.Display()
	m_time2.Format(" %.2f",pframe->m_pxaxis->timeMicroSecondC2I0);		//XAxisView.Display2()
	m_time3.Format(" %2f", pframe->m_pxaxis->timeMicroSecondC3I0);		//XAxisView.Display3()

	//If all bottles are being ejected show the corresponding message in the status box.
	if(EjectAll==true)  m_status="Eject All is ON!!!";
	UpdateData(false);
}


//Trigger the camera a single time. If no camera is connected load the next image from the hard drive.
void CCapView::OnTrig1() 
{
	KillTimer(1);	//Stop timed trigger.
	toggle=true;	//Set flag to indicate that timed trigger is off.
	pframe->SendMessage(WM_TRIGGER,NULL,NULL);	//Sent message to trigger.
	
	//CCapDoc* pDoc = (CCapDoc*)GetDocument();
//	pDoc->CamRestart();
}


//NOT USED
void CCapView::OnTrig2() 
{
	//if(toggle==true){ toggle=false; SetTimer(1,500,NULL);}else{toggle=true; KillTimer(1);}
	
}

void CCapView::OnTimer(UINT nIDEvent) 
{
	//Timed camera trigger.
	if (nIDEvent==1) 
	{
		pframe->SendMessage(WM_TRIGGER,NULL,NULL);
		CCapDoc* pDoc = (CCapDoc*)GetDocument();
//		pDoc->CamRestart();
	}
	//Toggle ejector off-line background from gray to red.
	if (nIDEvent==2) 
	{
		
		if(ToggleColor==true){ OnlineBox(1); ToggleColor=false;}else{ ToggleColor=true; OnlineBox(3);}
	}

	if (nIDEvent==3) 
	{
		KillTimer(3);
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,790);
			pframe->m_pserial->SendChar(0,790);
		}
	} 
	//NOT USED?
	if (nIDEvent==4) 
	{
		KillTimer(4);
		//pframe->m_pserial->QueMessage(0,510);
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,510);
			pframe->m_pserial->SendChar(0,510);
		}
		SetTimer(5,100,NULL);
		
	}
	//NOT USED?
	if (nIDEvent==5) 
	{
		KillTimer(5);
		
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,790);
			pframe->m_pserial->SendChar(0,790);
		}
	} 
	//Send ejector on-line signal to PLC
	if (nIDEvent==6) 
	{
		KillTimer(6);
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,400);
			pframe->m_pserial->SendChar(0,400);
		}
	}
	//Send ejector off-line signal to PLC
	if (nIDEvent==7) 
	{
		KillTimer(7);
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,410);
			pframe->m_pserial->SendChar(0,410);
		}
	}

	//NOT USED?
	if (nIDEvent==8) 
	{
		KillTimer(8);  //Patch fix for the bottle behind issue
		//This timer never gets called.
		
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,330);
			pframe->m_pserial->SendChar(0,330);
		}
	}

	CFormView::OnTimer(nIDEvent);
}

//NOT USED
void CCapView::OnRfault() 
{

}

//Resets inspection data such as total run, total reject and other statistics. 
void CCapView::OnRtot() 
{
	
	//pframe->ResetTotals();
	int result=AfxMessageBox("Do you want to reset the totals?", MB_YESNO);
	if (IDYES == result) {
		pframe->ResetTotals();
		pframe->m_pserial->SendChar(0, 590);
	}
}

//Only updates the display when a new defective bottle is found.
void CCapView::OnFreeze() 
{
	pframe->Freeze=true;
	pframe->FreezeReset=true;
	
}

//Update the display for each trigger.
void CCapView::OnFreeze2() 
{
	pframe->Freeze=false;	
}

//Updates the display to show the total number of inspections performed and the total number of rejected bottles.
void CCapView::UpdateDisplay2()
{
	m_total.Format("%5i",pframe->Total);
	//m_time.Format("%.5f",pframe->m_pxaxis->spanElapsed);
	m_rejects.Format(" %5i",pframe->Rejects);
	
	UpdateData(false);
}

//Show the name of the job in the status edit box.
void CCapView::OnShowjob() 
{
	char buf[20];
	CString LNum=CString(itoa(pframe->LockUp,buf,10));
	m_status=m_currentjob + "     " +LNum;
	
	UpdateDisplay();
}

//NOT USED
void CCapView::UpDateEject()
{
	//if(theapp->SavedEjectDisable==true){CheckDlgButton(IDC_EJECT2,1); m_eject2.Format("%s","Eject is Disabled");}else{CheckDlgButton(IDC_EJECT2,0);m_eject2.Format("%s","Eject is Enabled");}
	UpdateData(false);
}

//Pop up a dialog box asking if all bottles should be rejected.
void CCapView::OnRejectall() 
{	
	Prompt prompt;
	prompt.DoModal();	
}

//Turn the ejector on.
void CCapView::OnOnline() 
{
	//Send ejector online signal to PLC.
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,400);
		pframe->m_pserial->SendChar(0,400);
	}

	//Resend ejector online signal to PLC
	SetTimer(6,20,NULL);
	SetTimer(6,40,NULL);
	SetTimer(6,60,NULL);
	SetTimer(6,80,NULL);
	SetTimer(6,100,NULL);
	
	pframe->OnLine=true;	//Set ejector online flag.
	KillTimer(2);			//Stop the ejector button background from blinking between gray and red.
	//pframe->KillTimer(8);
	OnlineBox(2);			//Set ejector buttons backgrounds to green indicating that the ejector is turned on.

	

//	pframe->UpdateOutput(7, true);
	//pframe->UpdateOutput(4, true);
	//pframe->UpdateOutput(5, false);
	
}

//Turn the ejector off.
void CCapView::OnOffline() 
{
	//Send ejector offline signal to PLC.
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,410);
		pframe->m_pserial->SendChar(0,410);
	}
	//Resend ejector offline signal to PLC
	SetTimer(7,20,NULL);
	SetTimer(7,40,NULL);
	SetTimer(7,60,NULL);
	SetTimer(7,80,NULL);
	SetTimer(7,100,NULL);
	
	pframe->OnLine=false;		//Set ejector offline flag.
	SetTimer(2,500,NULL);		//Set the ejector button background to blinking between gray and red.
	SetTimer(3,50,NULL);		//send good signal to send the fingers back up

		

	
}

//Toggle between timed trigger being on and off.
void CCapView::OnCont() 
{
//if(toggle==true){ toggle=false; SetTimer(1,250,NULL);}else{toggle=true; KillTimer(1);}

	if(toggle==true)
	{ 
		SetTimer(1,theapp->timedTrigger,NULL);
		toggle=false;
		//pframe->m_pserial->SendChar(480);
		//SetTimer(1,65,NULL);
	}
	else
	{
		toggle=true;
		//pframe->m_pserial->SendChar(490);
		KillTimer(1);	//Stop timer for timed trigger.
	}//SetTimer(1,75,NULL);KillTimer(1);	
	
}

//NOT USED
void CCapView::newImageSize()
{
  
   
}

//Updates the min and max inspection limit labels and inspection name labels. Called after the user modifies the inspection limits.
void CCapView::UpdateMinor() 
{
	//Update min labels.
	m_l1.Format("%3i",theapp->inspctn[1].lmin);
	m_l2.Format("%3i",theapp->inspctn[2].lmin);
	m_l3.Format("%3i",theapp->inspctn[3].lmin);
	//m_l4.Format("%3i",0);
	m_l4.Format("%3i",theapp->inspctn[4].lmin);

	m_l5.Format("%3i",theapp->inspctn[5].lmin);
	m_l6.Format("%3i",theapp->inspctn[6].lmin);
	m_l7.Format("%3i",theapp->inspctn[7].lmin);
	m_l8.Format("%3i",theapp->inspctn[8].lmin);
	m_l9.Format("%3i",theapp->inspctn[9].lmin);
	
	//Update max labels.
	m_h1.Format("%3i",theapp->inspctn[1].lmax);
	m_h2.Format("%3i",theapp->inspctn[2].lmax);
	m_h3.Format("%3i",theapp->inspctn[3].lmax);
	//m_h4.Format("%3i",theapp->jobinfo[pframe->CurrentJobNum].Rear_ht_Diff);
	m_h4.Format("%3i",theapp->inspctn[4].lmax);
	m_h5.Format("%3i",theapp->inspctn[5].lmax);
	m_h6.Format("%3i",theapp->inspctn[6].lmax);
	m_h7.Format("%3i",theapp->inspctn[7].lmax);
	m_h8.Format("%3i",theapp->inspctn[8].lmax);
	m_h9.Format("%3i",theapp->inspctn[9].lmax);
	
	//Update names of inspections being performed.
	m_insp1.Format("%s",theapp->inspctn[1].name);
	m_insp2.Format("%s",theapp->inspctn[2].name);

	m_insp4.Format("%s",theapp->inspctn[4].name);
	m_insp5.Format("%s",theapp->inspctn[5].name);
	m_insp6.Format("%s",theapp->inspctn[6].name);
	m_insp7.Format("%s",theapp->inspctn[7].name);
	m_insp8.Format("%s",theapp->inspctn[8].name);
	m_insp9.Format("%s",theapp->inspctn[9].name);

	if(theapp->jobinfo[pframe->CurrentJobNum].sport==0)
	{
			//m_name3.Format("%s","Height Middle");
		m_insp3.Format("%s",theapp->inspctn[3].name);
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].sport==1)
	{
			//m_name3.Format("%s","Sport Band");
		m_insp3.Format("%s","Sport Band");
	}
	else if(theapp->jobinfo[pframe->CurrentJobNum].sport==2)
	{
			//m_name3.Format("%s","Sport Band");
		m_insp3.Format("%s","Foil");
	}
		
	UpdateData(false);
}

//NOT USED
void CCapView::OnFbutton() 
{
	if(theapp->noCams==false){theapp->noCams=true;}else{theapp->noCams=false;}	
}

//Increase the number of milliseconds between timed triggers
void CCapView::OnMore() 
{
	theapp->timedTrigger+=10;
	m_timedtrigger.Format("%i",theapp->timedTrigger);

	SetTimer(1,theapp->timedTrigger,NULL);
	UpdateData(false);
	
}

//Decrease the number of milliseconds between timed triggers
void CCapView::OnLess() 
{
	theapp->timedTrigger-=10;
	m_timedtrigger.Format("%i",theapp->timedTrigger);

	SetTimer(1,theapp->timedTrigger,NULL);
	UpdateData(false);
}

//Manually trigger the ejector.
void CCapView::OnTesteject() 
{
	if(theapp->NoSerialComm==false) 
	{
		
		pframe->m_pserial->SendChar(0,500);
	}

	//-ML JUST TO TEST.. THIS IS WEIRD
	//THIS TWO LINES MADE THE CUP GO ONE BEHIND...
	//ARE THE TIMERS BEING KILLED?

	//SetTimer(4,1000,NULL);
	//SetTimer(8,1800,NULL);

	//Timers 4 and 8 only send 510 and 790
	//since they are commented, let's send them here
	//510 is for number of Filler Valves 

	if(theapp->NoSerialComm==false) 
	{

		pframe->m_pserial->SendChar(0,790);
	}
}


void CCapView::OnFmore() 
{
	theapp->fillerPulse+=10;
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->fillerPulse,820);
		pframe->m_pserial->SendChar(theapp->fillerPulse,820);
	}

	m_timedfiller.Format("%i",theapp->fillerPulse);
	UpdateData(false);
}

void CCapView::OnFless() 
{
	theapp->fillerPulse-=10;
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->fillerPulse,820);
		pframe->m_pserial->SendChar(theapp->fillerPulse,820);
	}

	m_timedfiller.Format("%i",theapp->fillerPulse);
	UpdateData(false);
	
}

//Increase offset for camera 2 cocked cap inspection
void CCapView::OnC2Up() 
{
	theapp->cam2_ht_offset+=1;
	
	UpdateData(false);
	UpdateDisplay();
}

//Decrease offset for camera 2 cocked cap inspection
void CCapView::OnC2Dwn() 
{
	
	theapp->cam2_ht_offset--;
	if(theapp->cam2_ht_offset <0 )	//Offset cannot be negative.
	{theapp->cam2_ht_offset =0 ;}
	UpdateData(false);
	UpdateDisplay();
}

//Increase offset for camera 3 cocked cap inspection
void CCapView::OnC3Up() 
{
	theapp->cam3_ht_offset++;
	UpdateData(false);
	UpdateDisplay();
	
}

//Decrease offset for camera 3 cocked cap inspection
void CCapView::OnC3Dwn() 
{
	theapp->cam3_ht_offset--;
	if(theapp->cam3_ht_offset <0 )	//Offset cannot be negative.
	{theapp->cam3_ht_offset =0 ;}
	UpdateData(false);
	UpdateDisplay();
	
}

//Saves the serial numbers entered for the cameras.
void CCapView::OnButton2() 
{
int len;
	
	CString tempString1;									//Holds camera 1 serial number
	len=m_camser1.LineLength(m_camser1.LineIndex(0));		//Get length of camera 1 serial number.
	m_camser1.GetLine(0,tempString1.GetBuffer(len),len);	//Get camera 1 serial number.
	theapp->CamSer1=atoi(tempString1);						//Convert serial number from string to int then save.
	m_editcam1.Format("%i",theapp->CamSer1);				//Update GUI variable to the entered serial number.

	CString tempString2;
	len=m_camser2.LineLength(m_camser2.LineIndex(0));
	m_camser2.GetLine(0,tempString2.GetBuffer(len),len);
	theapp->CamSer2=atoi(tempString2);
	m_editcam2.Format("%i",theapp->CamSer2);

	CString tempString3;
	len=m_camser3.LineLength(m_camser3.LineIndex(0));
	m_camser3.GetLine(0,tempString3.GetBuffer(len),len);
	theapp->CamSer3=atoi(tempString3);
	m_editcam3.Format("%i",theapp->CamSer3);
	
	pframe->SaveJob(pframe->CurrentJobNum);					//Save entered serial numbers.
	UpdateData(false);	
}

//NOT USED
void CCapView::OnSetfocusEdit2() 
{
	// TODO: Add your control notification handler code here
	
}

//Called when text is entered into the camera 1 serial number edit box. 
void CCapView::OnChangeEdit2() 
{
	m_camser1.GetWindowText(m_editcam1);	//Get serial number from GUI into the associated string variable
	UpdateData(false);						//Load data to GUI

}

//Called when text is entered into the camera 2 serial number edit box. 
void CCapView::OnChangeEdit7() 
{
	m_camser2.GetWindowText(m_editcam2);
		UpdateData(false);
}

//Called when text is entered into the camera 3 serial number edit box. 
void CCapView::OnChangeEdit8() 
{
	m_camser3.GetWindowText(m_editcam3);
	UpdateData(false);
	
}

//NOT USED
void CCapView::OnResetTotCnt() 
{
	pframe->m_pxaxis->tot_bottle_cnt=0;	
}

//Use PLC based temperature sensor.
void CCapView::OnTempPlc() 
{
	theapp->tempSelect = 1; // for PLC Temp (1 is the default value for this variable)
	CheckDlgButton(IDC_CHECK6,1);
	CheckDlgButton(IDC_CHECK9,0);


	pframe->SaveJob(pframe->CurrentJobNum);
	
}

//Use USB based temperature sensor.
void CCapView::OnTempUSB() 
{
	theapp->tempSelect = 2; // for USB Temp
	CheckDlgButton(IDC_CHECK6,0);
	CheckDlgButton(IDC_CHECK9,1);

	pframe->SaveJob(pframe->CurrentJobNum);
	
}

//Toggles between loading multiple pictures from the hard drive or regular operation (cameras or single picture from hard drive).
void CCapView::OnLoadMultiple() 
{

	if(pframe->m_pxaxis->load_1_pic ==true)
	{
		pframe->m_pxaxis->load_1_pic=false;
//		pframe->m_pxaxis->Load50Pics();
		pframe->m_pxaxis->count_pic=1;
		CheckDlgButton(IDC_CHECK7,1);


	}
	else
	{
		CheckDlgButton(IDC_CHECK7,0);
		pframe->m_pxaxis->load_1_pic=true;

	}

	
}

//Toggles between continually loading picture from the hard drive or displaying the last loaded picture. 
void CCapView::OnRadio1() 
{
		if(pframe->m_pxaxis->freeze_current_pic == false)
		{

				pframe->m_pxaxis->freeze_current_pic =true;
				CheckDlgButton(IDC_RADIO1,1);
		}
		else
		{
				pframe->m_pxaxis->freeze_current_pic=false;
				CheckDlgButton(IDC_RADIO1,0);
		}
		UpdateData(false);
}

//Increase delay from the camera 1 XAxisView.Display() function to the XAxisView.Inspec() function.
void CCapView::OnInspdelayup() 
{
	theapp->inspDelay+=1;
	m_inspdelay.Format("%2i",theapp->inspDelay);
	
	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);		
}

//Decrease delay from the camera 1 XAxisView.Display() function to the XAxisView.Inspec() function.
void CCapView::OnInspdelaydw() 
{
	theapp->inspDelay-=1;
	m_inspdelay.Format("%2i",theapp->inspDelay);
	
	pframe->SaveJob(pframe->CurrentJobNum);
	UpdateData(false);			
}
