// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Cap.h"
#include "CapDoc.h"
#include "Camera.h"

#include "serial.h"
#include "MainFrm.h"
#include "job.h"
#include "xaxisview.h"
#include "setup.h"
#include "insp.h"
#include "capview.h"
#include "yaxisview.h"
#include "pics.h"

#include "blowoff.h"

#include "Data.h"
#include "view.h"
#include "advanced.h"
#include "security.h"

#include "light.h"
#include "motor.h"
#include "modify.h"
#include "bottledown.h"
#include "movehead.h"
#include "photoeye.h"
#include "digio.h"
#include "xtra.h"
#include "Filler.h"
#include "splitter.h"

#include "PicRecal.h"
#include "fillergraph.h"
#include "EnhancedTB.h"
#include "Usb.h"
#include "Prompt3.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_NUM_MEASUREMENTS 100
//#define GOIO_MAX_SIZE_DEVICE_NAME 100

//char *deviceDesc[8] = {"?", "?", "Go! Temp", "Go! Link", "Go! Motion", "?", "?", "Mini GC"};


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_COMMAND(ID_SERIAL, OnSerial)
	ON_COMMAND(ID_JOBCHANGE, OnJobchange)
	ON_WM_CLOSE()
	ON_COMMAND(ID_SETUP2, OnSetup2)
	ON_COMMAND(ID_EJECT, OnEject)
	ON_COMMAND(ID_SECURITY, OnSecurity)
	ON_COMMAND(ID_LIGHT, OnLight)
	ON_COMMAND(ID_MOTOR, OnMotor)
	ON_COMMAND(ID_BOTTLEDOWN, OnBottledown)
	ON_COMMAND(ID_PHOTOEYE, OnPhotoeye)
	ON_COMMAND(ID_DIGIO, OnDigio)
	ON_COMMAND(ID_DATA, OnData)
	ON_COMMAND(ID_SETUP4, OnSetup4)
	ON_COMMAND(ID_LOGOUT, OnLogout)
	ON_COMMAND(IDD_TEACH, OnTeach)
	ON_COMMAND(ID_ALIGNCAMS, OnAligncams)
	ON_COMMAND(ID_FILLER, OnFiller)
	ON_COMMAND(ID_FC_DATA, OnFillerData)
	ON_COMMAND(IDC_CAMERAS, OnCameras)
	ON_COMMAND(ID_ADVANCED, OnAdvanced)
	ON_UPDATE_COMMAND_UI(ID_FC_DATA, OnUpdateFcData)
	ON_COMMAND(ID_RecallDefective, OnRecallDefective)
	ON_COMMAND(ID_ADJUSTEXPTIME, OnAdjustshutter)
	ON_COMMAND(ID_ADJUSTWHTBAL, OnAdjustWhtBal)
//	ON_COMMAND(ID_USB, OnUsb)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_NEWJOB,NewJob)
	ON_MESSAGE(WM_INSPECT,Inspect)
	ON_COMMAND(ID_HELP, RunHelp)
	ON_MESSAGE(WM_TRIGGER,Trigger)
	ON_MESSAGE(WM_RELOAD,ReLoad)
	ON_MESSAGE(WM_SETLIGHTLEVEL,SetLightLevel)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction


CMainFrame::CMainFrame()
{

	HDC hDC = ::GetDC(NULL);
	int XSpan = GetDeviceCaps(hDC, HORZRES);
	int YSpan = GetDeviceCaps(hDC, VERTRES);
	::ReleaseDC(NULL, hDC);
	CRect r;
	r.SetRect(0, 0, XSpan, YSpan);
	//    r.SetRect(0, 0, 856, 742); 
     //pWnd = AfxGetMainWnd();   
    Create(NULL, 
    "Cap Inspection System",
    WS_VISIBLE, //| WS_OVERLAPPEDWINDOW,
    r, 
    this,
    MAKEINTRESOURCE(IDR_MAINFRAME));// MAKEINTRESOURCE(IDR_MAINFRAME)

	m_pform=NULL;
	CurrentJobNum=0;
	SetTimer(3,150,NULL);//calls Init()
	CopyDone=true;
	Total24=0;
	Rejects24=0;
}

void CMainFrame::Init()
{	
	theapp=(CCapApp*)AfxGetApp();
	theapp->pframeSub=this;
	
	pParent=GetParent(); 

	//Initialize model-windows
	if(theapp->NoSerialComm==false)
	{
		m_pserial=new Serial(); 						//serial class object
		m_pserial->Create(IDD_SERIAL,pParent);
		m_pserial->ControlChars=0;
	}
	m_pcamera=new CCamera();					//camera object
	m_pcamera->Create(IDD_CAMERA, pParent);
	
	m_pwhtbal=new CWhiteBalance();
	m_pwhtbal->Create(IDD_WHTBAL, pParent);

	m_pusb=new Usb();
	m_pusb->Create(IDD_USB, pParent);


	//Initialize variables
	if(theapp->noCams==false)
		m_pcamera->StartCams();

	//Initialize variables.
	TotalJob				=0;
	oldTotal				=0;
	RejectsJob				=0;
	count6					=0;
	theapp->fillerEject		=false;
	theapp->resetFromPLC	=false;
	IOReject				=false;
	IORejects				=0;
	IODone					=true;
	IODebounce				=false;
	IORejectLimit			=theapp->SavedIORejects;
	ConRejectLimit			=theapp->SavedConRejects;
	conreject				=0;
	PicsOpen				=false;
	Faulted					=false;
	Total					=0;
	Rejects					=0;
	EjectRate				=0;
	Freeze					=false;
	CountEnable				=false;
	debounce				=false;
	WasSetup				=false;
	InspOpen				=false;
	JobNoGood				=false;
	Safe					=false;
	OnLine					=false;
	LockUp					=0;
	Condition1				=0;
	Condition2				=0;
	reloading				=false;
	BottleRate				=0;
	OldBottleCount			=0;
	is666received			=false; //this is for the consecutive Reject from external input(client)
	TriggerCount=OldTriggerCount=NewTriggerCount=0;
	gotNext					=0;
	SaveCopyDisabled = false;

	//Initialize security
	if(theapp->SecOff==true)
	{
		theapp->SavedSetup=SSetup=true;
		theapp->SavedLimits=SLimits=true;
		theapp->SavedEject=SEject=true;
		theapp->SavedModify=SModify=true;
		theapp->SavedJob=SJob=true;
	}
	else
	{
		theapp->SavedSetup=SSetup=false;
		theapp->SavedLimits=SLimits=false;
		theapp->SavedEject=SEject=false;
		theapp->SavedModify=SModify=false;
		theapp->SavedJob=SJob=false;
	}

	SSecretMenu=false;
	ToggleCheckStatus		=true;
	Stop					=false;
	
	WaitNext				=false;
		
	StoredStatus			=255;
	if(theapp->NoSerialComm==false)
	{
		SetTimer(11,10000,NULL);	//Backup scanning statistics data every 24 hours. Also used to calculate BottleRate.	//timer for bottles/min and freeze status
		SetTimer(12,900,NULL);		//Get motor position from PLC	//get motor position from PLC

		SetTimer(26,2000,NULL);	//NOT USED	//turn on amber lite
	//	SetTimer(27,10000,NULL);//send heartbeat and check temperature every 10 seconds
	
	}
	PhotoEyeEnable(true); //Make sure Photo eye is enabled when application launches

//	m_pCrntFormat			 = NULL; // commented on 5/6/2014 
	m_pCrntMode				= NULL;
	m_pCrntFrate			 = NULL;
	m_pTrigger				 = NULL;
	SetTimer(23,1000,NULL);//snap picture

	GetJob(0);
	ResetTotals2();

	if(theapp->NoSerialComm==false) 
	{
						
		//turn off foil sensor
		//m_pserial->SendMessage(WM_QUE,0,530);
		m_pserial->SendChar(0,530);
		
		//Conveyor speed offset.
		m_pserial->SendChar(theapp->convRatio,290);	//Update gui variable to show offset
	
	}

	SetTimer(40,200,NULL);//Make Stack Light go RED
	
	
/////////////////////
	//ML- TO KEEP TRACK OF CAMERA PERFORMANCE
	countC0 = countC1 = countC2 = countC3  = 0;

	gotPicC1 = false;
	gotPicC2 = false;
	gotPicC3 = false;

/*	for(i=0; i<190; i++)
	{
		m_pxaxis->cameraDone1[i] = false;
		m_pxaxis->cameraDone2[i] = false;
		m_pxaxis->cameraDone3[i] = false;
	}*/

/////////////////////

}

CMainFrame::~CMainFrame()
{
	delete(m_pcamera);
	if(theapp->NoSerialComm==false) 
	{
		delete (m_pserial);
	}
	delete(m_pusb);
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

/*
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
*/
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnTimer(UINT nIDEvent) 
{
	//DOES NOT APPEAR TO BE USED, Make necessary updates to client PLC
	if (nIDEvent==1) 
	{	
		//External (client) PLC reset
		if(theapp->resetFromPLC==true){ResetTotals2(); theapp->resetFromPLC=false;}
		
		//If new number of bottles (TotalNet) is greater than last total update total and send data to PLC.
		if(theapp->TotalNet> oldTotal)
		{
			SetTimer(30,100,NULL);
			dataReady=true;
			oldTotal=theapp->TotalNet;
		}
	
	}

	//DOES NOT APPEAR TO BE USED.
	if (nIDEvent==2) 
	{
		Faulted=true;
		KillTimer(8);
		m_pview->m_status="Comm to PLC Lost";
		GotFault();
 	
	}
	
	//Timer set in the Constructor of Main Frame, calls init()
	if (nIDEvent==3)  //Timer called from the COnstructor of Main Frame
	{
		KillTimer(3);
 		Init(); 
	}	

	//NOT USED, Set in USB (temperature sensor) dialog box.
	if (nIDEvent==4) 
	{
		KillTimer(4);
// 		m_pxaxis->OnSnap();
	}

	//NOT USED
	if (nIDEvent==5) 
	{
		KillTimer(5);
 		UpdateOutput(0,false);//SetTimer(10,100,NULL);
		m_pxaxis->InvalidateRect(NULL,false);
	}
	
	//NOT USED
	if (nIDEvent==7) 
	{
		KillTimer(7);
 		
	}
	
	//NOT USED	
	if (nIDEvent==8) 
	{
		if(theapp->NoSerialComm==false) 
		{
						
		m_pserial->GetChar(1);
		}
	}

	//NOT USED
	if (nIDEvent==9) 
	{
		KillTimer(9);
	
	}

	//NOT USED
	if (nIDEvent==10) 
	{
		KillTimer(10);
 		Stop=true;
	
	}

	//Timer used to backup scanning statistics data every 24 hours. Also used to calculate BottleRate
	//Set in Init()
	if (nIDEvent==11) 
	{
		//look every 10 secs
		BottleRate=float(Total-OldBottleCount)*6;
		OldBottleCount=Total;
 		Update24HourData();
	}

	//Updates motor position.
	//Set in the Init() function.
	if (nIDEvent==12) 
	{
		if(theapp->NoSerialComm==false) 
		{
						
		MotPos=m_pserial->MotorPos;
		}
 		KillTimer(12);
	}

	//Timer for security logout. Set in Security dialog box in OnOk()
	if (nIDEvent==13) 
	{
		KillTimer(13);
	//	SetTimer(24,600000,NULL);
		SetTimer(24,2700000,NULL);  //Increasing the Duration from 10 mins to 45 mins
	}

	//NOT USED
	if (nIDEvent==14) 
	{
		debounce=false;
 		KillTimer(14);
	}

	//NOT USED
	if (nIDEvent==15) 
	{
		
 		KillTimer(15);
	}

	//NOT USED
	if (nIDEvent==21) 
	{
		int Value=0;
		int Function=0;
		int CntrBits=0;
		if(theapp->NoSerialComm==false) 
		{
						
			m_pserial->ControlChars=Value+Function+CntrBits;
		}
		
 		KillTimer(21);
	}

	//NOT USED
	if (nIDEvent==22) 
	{
		UpdateOutput(6, false);//consecutive rejects
 		KillTimer(22);
	}

	//Causes 1 trigger, capturing an image for all connected cameras when the program turns on.
	//Called from Init()
	if (nIDEvent==23) 
	{
		SendMessage(WM_TRIGGER,NULL,NULL);
 		KillTimer(23);
	}

	//Timeout for security logout. Set in OnLogout().
	if (nIDEvent==24) 
	{
		KillTimer(24);
		theapp->SavedSetup=false; SSetup=false; 
		theapp->SavedLimits=false; SLimits=false; 
		theapp->SavedEject=false;SEject=false; 
		theapp->SavedModify=false; SModify=false;
		theapp->SavedJob=false; SJob=false;
		Safe=false;
 	
	}

	//NOT USED
	if (nIDEvent==25) 
	{
		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,0,110);
			m_pserial->SendChar(0,110);
		}

 		KillTimer(25);
	}

	//NOT USED
	if (nIDEvent==26) 
	{
		//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING		
//		m_pserial->SendMessage(WM_QUE,0,290);

 		KillTimer(26);
	}

	//NOT USED
	if (nIDEvent==27) 
	{
		
		if(theapp->NoSerialComm==false) 
		{
						
			if(m_pserial->currentFillerPosition>=theapp->tempLimit)
			{
				//m_pserial->SendMessage(WM_QUE,0,470);
				m_pserial->SendChar(0,470);
				m_pview->m_status="Over Temperature Limit!!!";
			}


			//m_pserial->SendMessage(WM_QUE,0,50);
			m_pserial->SendChar(0,50);  //50 is for the heartbeat
		}
	}

	//Timer used to update user PLC with scan statistics. Set in UpdatePLC().
	if (nIDEvent==30) 
	{
		UpdatePLCData();
		if(dataReady==false) KillTimer(30);
		//
		//SetTimer(28,4000,NULL);
	}

	//Set from jobs dialog, used to send new job info to PLC.
	if (nIDEvent==31) 
	{
		UpdatePLC();
		KillTimer(31);
		//

	}

	//NOT USED
	if (nIDEvent==32) 
	{
		KillTimer(32);

		if(theapp->jobinfo[CurrentJobNum].xtraLight==0)
		{
			//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING		
		//	m_pserial->SendMessage(WM_QUE,0,270);
		
		}
		else
		{
			//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
		//	m_pserial->SendMessage(WM_QUE,0,260);
		
		}

	}

	//Timer used to communicate with motor
	//Called from OnMotor() and OnSetup2()
	if (nIDEvent==33) 
	{
		KillTimer(33);

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,0,410);
			m_pserial->SendChar(0,410);
		}
		
	}

	//Sets ejector slat parameters. Set in UpdatePLC().
	if (nIDEvent==34) 
	{
		KillTimer(34);

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,theapp->jobinfo[CurrentJobNum].slatPW,280);
			m_pserial->SendChar(theapp->jobinfo[CurrentJobNum].slatPW,280);
		}
		
	}

	//Sends info to PLC about how long the ejector should remain extended. Set in UpdatePLC().
	if (nIDEvent==35) 
	{
		KillTimer(35);

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,theapp->jobinfo[CurrentJobNum].encDur,90);
			m_pserial->SendChar(theapp->jobinfo[CurrentJobNum].encDur,90);
		}
		
	}

	//Sends info to PLC about how far the ejector is from the photo eye. Set in UpdatePLC().
	if (nIDEvent==36) 
	{
		KillTimer(36);

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,theapp->jobinfo[CurrentJobNum].encDist,80);
			m_pserial->SendChar(theapp->jobinfo[CurrentJobNum].encDist,80);
		}
		
	}

	//Sends info to PLC about the duration of the ring light. Set in UpdatePLC() and onOK of jobs dialog box.
	if (nIDEvent==37) //this timer is for pushing new values in the PLC upon loading a new job
	{
		KillTimer(37);

			if(theapp->jobinfo[CurrentJobNum].useRing==false)
			{
				if(theapp->NoSerialComm==false) 
				{
					//m_pserial->SendMessage(WM_QUE,0,260);
					m_pserial->SendChar(0,260);
				}

			}
			else
			{
				if(theapp->NoSerialComm==false) 
				{
						
					//m_pserial->SendMessage(WM_QUE,0,270);
					//m_pserial->SendMessage(WM_QUE,theapp->jobinfo[CurrentJobNum].ringLight,370);

					m_pserial->SendChar(0,270);
					m_pserial->SendChar(theapp->jobinfo[CurrentJobNum].ringLight,370);

				}

			}

	
		
	}

	//NOT USED	
	if (nIDEvent==38) //
	{
		KillTimer(38);

		if(theapp->NoSerialComm==false) 
		{
		
		//	m_pserial->SendChar(0,330);
		}
		
	}

		//Add 340 in another timer
	if (nIDEvent==39) //
	{
		KillTimer(39);

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,0,340);
			m_pserial->SendChar(0,340);
		}
		
	}

	//Turn on stack light to green.
	//Called from Init() function.	
	if (nIDEvent==40) //
	{
		KillTimer(40);

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,0,410);
			m_pserial->SendChar(0,410);
		}
		
	}

	//Security time out, resets permissions to access different dialogs and tabs. Set in OnLogout().
	if (nIDEvent==41) //
	{
		KillTimer(41);
		m_pyaxis->m_tabmenu.SetCurFocus(0);
		theapp->SavedSetup=false; SSetup=false; 
		theapp->SavedLimits=false; SLimits=false; 
		theapp->SavedEject=false;SEject=false; 
		theapp->SavedModify=false; SModify=false;
		theapp->SavedJob=false; SJob=false;
		Safe=false;
		
	}

	//Timer to reset the total bottle count ..For Sunny D .
	if (nIDEvent==42) //
	{
		KillTimer(42);

		if(theapp->NoSerialComm==false) 
		{
			//THis is for sunnyD> To Reset the counters of total bottle count							
			//m_pserial->SendMessage(WM_QUE,0,590); 
			m_pserial->SendChar(0,590); 
		}
	}

	//This timer refreshs the pic being displayed in XAxisView
	if (nIDEvent==43) 
	{
		m_pxaxis->InvalidateRect(NULL,false);  //This timer will refresh the pic being displayed in XAxisView

		KillTimer(43);
	}

	//Timer for refreshing the X-Axis pictures. Set in Lights dialog box.
	if (nIDEvent==44) 
	{
		m_pview->UpdateDisplay();

		KillTimer(44);
	}

	if (nIDEvent==45) 
	{
		m_pview->UpdateMinor();


		if(theapp->jobinfo[CurrentJobNum].sport==2) //If foil sensor is enabled
		{
			m_pserial->SendChar(1,360);
		}
		else
		{
			m_pserial->SendChar(2,360);
		}

		KillTimer(45);
	}

	

	CMDIFrameWnd::OnTimer(nIDEvent);
}

//Open Serial Dialog Box
void CMainFrame::OnSerial() 
{
	if(theapp->NoSerialComm==false) 
	{
		m_pserial->ShowWindow(SW_SHOW); 
		m_pserial->UpdateData(false);
	}
	else
	{
		AfxMessageBox("No serial communication");
	}
}

//NOT USED
int CALLBACK GrabCallback(PVOID pContext, double SampleTime, BYTE * pBuffer, long lBufferSize )
{
	//Sleep(5);
	CMainFrame* pframe=(CMainFrame*)AfxGetMainWnd();
	CCapApp* theapp=(CCapApp*)AfxGetApp();
	//pframe->SetTimer(15,5,NULL);
		
	if(lBufferSize==921600)
	{
		//pframe->m_pxaxis->DisplayColor(pBuffer);
	}
	else if(lBufferSize==614400)
	{
		pframe->m_pxaxis->Display(pBuffer);
		//pframe->SendMessage(WM_INSPECT,NULL,NULL);
		pframe->Inspect(0, 0);
		//pframe->Total+=1;
	}

	
	return 0;
}

//Manual trigger of the camera called from various dialog boxes.
LONG CMainFrame::Trigger(UINT, LONG)//manual button
{
	if(theapp->noCams==true) //If cameras are not connected load pictures from hard drive.
	{
		if(m_pxaxis->load_1_pic ==true)
		{
			m_pxaxis->LoadImage();
			m_pxaxis->Inspect(m_pxaxis->im_cam1Color, m_pxaxis->im_cam2Color, m_pxaxis->im_cam3Color, m_pxaxis->im_data,m_pxaxis->im_data2,m_pxaxis->im_data3,m_pxaxis->sobel_cam2,m_pxaxis->sobel_cam3, 
				m_pxaxis->im_red,m_pxaxis->im_red2, m_pxaxis->im_red3, m_pxaxis->im_hue, m_pxaxis->im_hue2, m_pxaxis->im_hue3);
		}
		else if(m_pxaxis->load_1_pic == false)
		{
			if(m_pxaxis->freeze_current_pic == false)
			{
				m_pxaxis->Load50Pics();
			}

	    	m_pxaxis->Inspect(m_pxaxis->im_cam1Color, m_pxaxis->im_cam2Color, m_pxaxis->im_cam3Color, 
				m_pxaxis->im_data,m_pxaxis->im_data2,m_pxaxis->im_data3,m_pxaxis->sobel_cam2,m_pxaxis->sobel_cam3,
				m_pxaxis->im_red,m_pxaxis->im_red2, m_pxaxis->im_red3, m_pxaxis->im_hue, m_pxaxis->im_hue2, m_pxaxis->im_hue3);
		}

		//Always executes this code.
		if(PicsOpen==false)
		{
			if(Freeze==false)
			{
				m_pxaxis->InvalidateRect(NULL,false);
			}
			else
			{
				if(FreezeReset==true) m_pxaxis->InvalidateRect(NULL,false);
				if(m_pxaxis->BadCap==true) { m_pxaxis->InvalidateRect(NULL,false); FreezeReset=false;}else{ m_pview->UpdateDisplay2(); }
			}
		}
	}
	else //If cameras are connected trigger the cameras to get the next set of images.
	{
		if(theapp->NoSerialComm==false)
		{
			//m_pserial->SendMessage(WM_QUE,0,20);
			m_pserial->SendChar(0,20);
		}
		else
		{
			for (int i = 0; i < 3; i++)
				m_pcamera->cam_e[i]->FireSoftwareTrigger();
		}
	}

	//Always executes this code.
	if(m_pxaxis->LearnDone==true)
	{
		theapp->DidTriggerOnce=true;
	}

	return true;
}

//NOT USED
LONG CMainFrame::NewJob(UINT, LONG)
{
	CountEnable=false;
	//WasSetup=false;
	//m_pview->m_status="Now Teach Job";
	ResetTotals();
	return true;
}

//NOT USED
LONG CMainFrame::SetLightLevel(UINT level, LONG level2)
{
	int Value=level;
	int Function=0;
	int CntrBits=0;
	int ControlChars;
	
		//cam0=m_pBrdMod->GetCam(0);
		//cam0=(CICamera*)theapp->m_pCam;
		//cam0->SetAcqParam(P_STROBE_DURATION,level*theapp->lightGain);

		if(level2==1)
		{
			Value=Value*1000;
			Function=22;
			Function=Function *10;
			CntrBits=0;

			ControlChars=Value+Function+CntrBits;
			
			if(theapp->NoSerialComm==false) 
			{
				//m_pserial->SendMessage(WM_QUE,Value,Function);
				m_pserial->SendChar(Value,Function);
			}
			//SetTimer(21,50,NULL);
		}
		else
		{
			Value=Value*1000;
			Function=22;
			Function=Function *10;
			CntrBits=0;

			ControlChars=Value+Function+CntrBits;

			if(theapp->NoSerialComm==false) 
			{
				//m_pserial->SendMessage(WM_QUE,Value,Function);
				m_pserial->SendChar(Value,Function);
			}
			//SetTimer(21,50,NULL);
		}
//
	return true;
}

//Called when jobs dialog box is closed. Loads X axis with the saved job settings and patterns. Needs further explanation.
LONG CMainFrame::ReLoad(UINT mp, LONG)
{
	m_pxaxis->OnLoad(CurrentJobNum); // load the BW image saved during teaching

	if(JobNoGood==false)
	{
		reloading=true;
		
		SinglePoint CapLSide=m_pxaxis->FindCapSide(m_pxaxis->im_data,theapp->jobinfo[CurrentJobNum].midBarY*2,true);
		SinglePoint CapRSide=m_pxaxis->FindCapSide(m_pxaxis->im_data,theapp->jobinfo[CurrentJobNum].midBarY*2,false);

		theapp->jobinfo[CurrentJobNum].vertBarX=CapLSide.x+theapp->jobinfo[CurrentJobNum].lDist;
		if(theapp->jobinfo[CurrentJobNum].lDist >=350) theapp->jobinfo[CurrentJobNum].lDist=350; //150
		if(theapp->jobinfo[CurrentJobNum].lDist <=0) theapp->jobinfo[CurrentJobNum].lDist=0;
					
		theapp->jobinfo[CurrentJobNum].rDist=theapp->jobinfo[CurrentJobNum].lDist;
		//
		//if (theapp->jobinfo[CurrentJobNum].tBandType == 2 && theapp->jobinfo[CurrentJobNum].dark == 2) //red
		//{
		//	m_pxaxis->GetRedChannel(m_pxaxis->im_cam1Color, theapp->cam1Width, theapp->cam1Height, m_pxaxis->im_red);
		//	m_pxaxis->GetRedChannel(m_pxaxis->im_cam2Color, theapp->cam2Width, theapp->cam2Height, m_pxaxis->im_red2);
		//	m_pxaxis->GetRedChannel(m_pxaxis->im_cam3Color, theapp->cam3Width, theapp->cam3Height, m_pxaxis->im_red3);
		//}
		//if (theapp->jobinfo[CurrentJobNum].tBandType == 2 && theapp->jobinfo[CurrentJobNum].dark == 3) //hue
		//{
		//	m_pxaxis->GetHueChannel(m_pxaxis->im_cam1Color, theapp->cam1Width, theapp->cam1Height, m_pxaxis->im_hue);
		//	m_pxaxis->GetHueChannel(m_pxaxis->im_cam2Color, theapp->cam2Width, theapp->cam2Height, m_pxaxis->im_hue2);
		//	m_pxaxis->GetHueChannel(m_pxaxis->im_cam3Color, theapp->cam3Width, theapp->cam3Height, m_pxaxis->im_hue3);
		//}
		m_pxaxis->Inspect(m_pxaxis->im_dataC, m_pxaxis->im_dataC2, m_pxaxis->im_dataC3, m_pxaxis->im_data,m_pxaxis->im_data2,m_pxaxis->im_data3,
		m_pxaxis->sobel_cam2,m_pxaxis->sobel_cam3, m_pxaxis->im_red, m_pxaxis->im_red2, m_pxaxis->im_red3, m_pxaxis->im_hue, m_pxaxis->im_hue2, m_pxaxis->im_hue3);
	
		m_pxaxis->OriginalTBYOffset=(m_pxaxis->CapLTop.y+m_pxaxis->CapRTop.y)/2;

		m_pxaxis->LipOffset=m_pxaxis->FindNeckLip(m_pxaxis->im_data, m_pxaxis->NeckLSide, true);
//		if(theapp->jobinfo[CurrentJobNum].lip==1) m_pxaxis->LipOffset.y=m_pxaxis->NeckLSide.y-5;
		m_pxaxis->LipDiffl=m_pxaxis->NeckLSide.y-m_pxaxis->LipOffset.y;

		m_pxaxis->LipOffset=m_pxaxis->FindNeckLip(m_pxaxis->im_data, m_pxaxis->NeckRSide, false);
//		if(theapp->jobinfo[CurrentJobNum].lip==1) m_pxaxis->LipOffset.y=m_pxaxis->NeckLSide.y-5;
		m_pxaxis->LipDiffr=m_pxaxis->NeckRSide.y-m_pxaxis->LipOffset.y;

		m_pxaxis->TBOffset.x=theapp->jobinfo[CurrentJobNum].tBandX-CapLSide.x;
		if(m_pxaxis->TBOffset.x>=50)m_pxaxis->TBOffset.x=50;
		m_pxaxis->TBOffset.y=(theapp->jobinfo[CurrentJobNum].tBandY*2 )-m_pxaxis->OriginalTBYOffset;

	
		switch(theapp->jobinfo[CurrentJobNum].lip)
		{
			case 0:
					m_pxaxis->LearnPattern(m_pxaxis->im_data, true); //From the full sized BW image loaded, re initialise the lip pointers with the right pattern.
					m_pxaxis->LearnPattern(m_pxaxis->im_data, false);
					break;
		
			case 1:
					m_pxaxis->LearnPatternUser(m_pxaxis->im_data, true);
					m_pxaxis->LearnPatternUser(m_pxaxis->im_data, false);
					break;
			
			case 2:
					m_pxaxis->LearnPattern(m_pxaxis->im_data, true);
					m_pxaxis->LearnPattern(m_pxaxis->im_data, false);
				
		
		}

		float b=( ((CapRSide.x-CapLSide.x)+( (CapRSide.x-CapLSide.x) ) *0.70)-120+(m_pxaxis->TBl)*2 );//45
		m_pxaxis->Band2Size=((float(CapRSide.x-CapLSide.x))-b-10)/-0.929;
		m_pxaxis->ColorTarget = RGB(theapp->jobinfo[CurrentJobNum].colorTargR,theapp->jobinfo[CurrentJobNum].colorTargG,theapp->jobinfo[CurrentJobNum].colorTargB);

		//m_pxaxis->reteachColor=true;
		theapp->teachFrontBand=true;
		ResetTotals();
		m_pview->m_status="Job "+m_pview->m_currentjob+ " has been Reloaded";	
		WasSetup=true;
		InspOpen=false;
		CountEnable=true;
		m_pmod->UpdateDisplay();
		m_px->UpdateDisplay();
		if(theapp->Motor==true)
		{
			MoveHead movehead;
			movehead.DoModal();
		}
		PhotoEyeEnable(true);
		reloading=false;

		PrintJobData();
	}
	JobNoGood=false;


	return true;
}

//Resets the data collected for the inspections such as total runs, total rejects and statistics. Called from Z axis upon user request.
void CMainFrame::ResetTotals()
{
/////////////////////
	//ML- TO KEEP TRACK OF CAMERA PERFORMANCE
	countC0 = countC1 = countC2 = countC3  = 1;

	gotPicC1 = false;
	gotPicC2 = false;
	gotPicC3 = false;

	for(int i=0; i<190; i++)
	{
		m_pxaxis->cameraDone1[i] = false;
		m_pxaxis->cameraDone2[i] = false;
		m_pxaxis->cameraDone3[i] = false;
	}

/////////////////////

	Total=0;		//Reset total number of scans
	Rejects=0;		//Reset total number rejected.
	
	//Clear filler head reject counts.
	for(int i=0; i<=120; i++)
	{
		fillerArray[i]=0;
		capperArray[theapp->capperCount]+=1;
	}

	//Clear capper head reject counts.
	for(int j=0; j<=32; j++)
	{
		capperArray[j]=0;
			
	}

	//Clear count of each type of inspection failed.
	m_pxaxis->E1Cnt=0;
	m_pxaxis->E2Cnt=0;
	m_pxaxis->E3Cnt=0;
	m_pxaxis->E4Cnt=0;
	m_pxaxis->E5Cnt=0;
	m_pxaxis->CockedCnt=0;
	m_pxaxis->LooseCnt=0;
	m_pxaxis->E6Cnt=0;
	m_pxaxis->E7Cnt=0;
	m_pxaxis->E8Cnt=0;
	m_pxaxis->E9Cnt=0;
	m_pxaxis->E10Cnt=0;
	m_pxaxis->NoCapCnt=0;
	m_pxaxis->Result1=0;
	m_pxaxis->Result2=0;
	m_pxaxis->Result3=0;
	m_pxaxis->Result4=0;
	m_pxaxis->Result5=0;
	m_pxaxis->Result6=0;
	m_pxaxis->Result7=0;
	m_pxaxis->Result8=0;
	m_pxaxis->Result9=0;

	m_pxaxis->Missing_Foil_Count=0; //Counter for storing missing foil container
	EjectRate=0;
	BottleRate=0;
	OldBottleCount=0;
	m_pxaxis->StartLockOut=true;
	m_pxaxis->TimesThru=0;
	m_pview->UpdateDisplay();
	
}

//Resets totals when an external (client) PLC gives a signal.
void CMainFrame::ResetTotals2()
{

	theapp->TotalNet=0;
	m_pxaxis->E1CntNet=0;
	m_pxaxis->E2CntNet=0;
	m_pxaxis->E3CntNet=0;
	m_pxaxis->E4CntNet=0;

	m_pxaxis->E7CntNet=0;
	oldTotal=0;
	timesThru=0;
}

//Updates the number of triggers activated, updates and controls the filler/capper ejection. 
//Controls bad cap ejection.
//Outputs user signal when the specified number of consecutive bad bottles occur.
//Increments the totals and rejects counters. 
//Sends a signal to the PLC specifying if a bottle passed or failed.
//Updates the left hand pan (Z axis).
//Called from XAxisView Inspect 
LONG CMainFrame::Inspect(UINT, LONG)//from photoeye
{
	TriggerCount+=1;	//Increment number of bottles which triggered the photoeye.
	if(TriggerCount>=32764)		// if int over flow reset counter.
		TriggerCount=0;

//----------------------------------------------------------------------------------------------------------
	//Update number of filler and capper heads.
	if(theapp->NoSerialComm==false) 
	{
						
		theapp->fillerCount=m_pserial->currentFillerPosition;
		theapp->capperCount=m_pserial->currentCapperPosition;
	}
	
	//Used to start capper ejection from head number 1.
	if(theapp->capperCount==1)theapp->waitForOne=true;

	//If current filler head bottles need to be ejected and the filler/capper settings are not being modified.
	if(theapp->fillEject[theapp->fillerCount] != 0 && theapp->fillDataLoaded==true)
	{
		m_pxaxis->BadCap=true;
		//sample=true;
		//sampleWasLast=true;
		theapp->fillEject[theapp->fillerCount]-=1;
		if(theapp->fillEject[theapp->fillerCount]<=0) theapp->fillEject[theapp->fillerCount]=0;
			
	}

	//If current capper head bottles need to be ejected and the filler/capper settings are not being modified and capper head count started from 1.
	if(theapp->capEject[theapp->capperCount]!=0 && theapp->fillDataLoaded==true && theapp->waitForOne==true)
	{
		m_pxaxis->BadCap=true;	//Eject bottle.
//		sample=true;
//		sampleWasLast=true;
		theapp->capEject[theapp->capperCount]-=1;	//Decrement the number of bottles that need to be ejected for the current filler head.
		if(theapp->capEject[theapp->capperCount]<=0) theapp->capEject[theapp->capperCount]=0;
	}
//----------------------------------------------------------------------------------------------------------
	//If the application is not in the process of closing.
	if(Stop==false)
	{
		//If a configured job has been selected and configure inspection dialog box is not open.
		if(WasSetup==true && InspOpen==false)
		{
			//Increment totals
			Total+=1;
			theapp->TotalNet+=1;
			TotalJob+=1;
			Total24+=1;
			//SetTimer(1,1000,NULL);

//			SetTimer(27,10000,NULL);//send heartbeat
	
			KillTimer(30);		//Stop sending data to user's external central PLC.

//----------------------------------------------------------------------------------------------------------
			//If eject all bottles on external PLC signal is not set.
			if(theapp->SavedForcedEjects==false) theapp->rejectFromUser=false;
			
			//If no bottles are to be ejected after external PLC signal.
			if(theapp->SavedIORejects==false)IOReject=false;
			
			//If bad cap flag has been set or eject from user has been set.
			if(m_pxaxis->BadCap==true || theapp->rejectFromUser==true || IOReject==true)
			{
				//If ejector is on
				if(OnLine==true)
				{
						//Update last ejected filler and capper head numbers.
						theapp->fillerCount2=theapp->fillerCount;
						theapp->capperCount2=theapp->capperCount;
						
						//If bottle for current filler head should be ejected.
						if(theapp->fillerEject==true)fillerArray[theapp->fillerCount]+=1;	//Increment the number of bottles ejected for this filler head.
						//If bottle for current capper head should be ejected.
						if(theapp->fillerEject==false)capperArray[theapp->capperCount]+=1;	//Increment the number of bottles ejected for this capper head.
					
					//If user PLC has signaled to reject all bottles update status display to this state.
					if(theapp->rejectFromUser==true)m_pview->m_status="Reject From Master";
					
					//Update number of bottles rejected variables.
					Rejects+=1;
					Rejects24+=1;
					RejectsJob+=1;
					
					//Check if rejects are consecutive.
					if(Total-savedtotal==1) 
					{
						conreject+=1;	//If so increment consecutive rejects counter.
					}
					else
					{
						conreject=0;	//Otherwise reset counter.
					}
					savedtotal=Total; //Updated saved total to continue checking for consecutive rejects.

					//If set number of consecutive bottles have been rejected
					if(conreject>=ConRejectLimit && ConRejectLimit>0)
					{
						conreject=0;	//reset consecutive reject count. 
						
						//Send signal to PLC to set output signal for user. 
						//The output signal means that x number of consecutive bottles have been rejected.
						if(theapp->NoSerialComm==false)
						{
							//m_pserial->SendMessage(WM_QUE,0,620);
							m_pserial->SendChar(0,620);
						}

						//count6+=1;
					}
					else  //Otherwise send signal to PLC to notify that a bottle has been rejected.
					{
						if(theapp->NoSerialComm==false) 
						{
							//m_pserial->SendMessage(WM_QUE,0,420);
							m_pserial->SendChar(0,420);
						}
						//count6+=1;
					}
				}
			}
			else if(OnLine==true) //Otherwise if online send signal to PLC that all inspections passed. Possibly turns on pass stack light.
			{
			
					if(theapp->NoSerialComm==false)
					{
						//good cap
						//m_pserial->SendMessage(WM_QUE,0,790);
						m_pserial->SendChar(0,790);
					}
			}

			m_pview2->UpdateDisplay();
		}
		else
		{
			
			if(InspOpen==false)//If selected job is not taught prompt user.
			{
				//m_pxaxis->OnManual();
				m_pview->m_status="You must teach job first";
				m_pview->UpdateData(false);
			}
		}

		//Update eject rate.
		if(Total!=0)
			EjectRate=(float(Rejects)/float(Total))*100;
	}

	//This if statement block does not appear to be used.
	if(InspOpen==true  )
	{
	//	m_pxaxis->OnManual();
		if(WaitNext==true)
		{
			gotNext+=1;
			if(gotNext==2)
			{
				gotNext=0;
				WaitNext=false; 
				m_pinsp->InvalidateRect(NULL,false);
				PhotoEyeEnable(false);
			}
		}
		int stoppoint=true;
	}

	//If user PLC has requested reject
	if(theapp->rejectFromUser==true)
	{
		IOReject=false;		//Set flag indicating that user has requested all bottles to be rejected
		m_pview->m_status="External Reject Activated";	//Update status on main display to stat that user has requested that all bottles be rejected.
	}

	//If user PLC has requested reject and a fixed number of bottles are to be rejected.
	if(IOReject==true && theapp->SavedForcedEjects==false)
	{
		//UpdateOutput(2,true);
		//SetTimer(9,20,NULL);
		m_pview->m_status="External Reject Activated";		//Update status on main display to stat that user has requested that all bottles be rejected.
		m_pview->UpdateDisplay();		//Update the the left hand pan (Z-axis).
		IORejects+=1;					//Increment counter of number of bottles rejected based on user PLC's request.
		if(IORejects>=theapp->SavedIORejects)	//If the required number of bottles has been rejected.
		{
			IORejects=0;		//Reset bottles rejected counter.
			IODone=true;		//Set flag indicating that the requested number of bottles based on user's PLC's request has been rejected.
			IOReject=false;		//Reset flag indicating that user PLC has requested to reject bottles.
			//theapp->rejLockOut=false;
		}
	}
	else //Otherwise reset bottles rejected counter.
	{
		IORejects=0;
	}

	return true;
}

//NOT USED
void CMainFrame::UpdateOutput(int Address, bool Status)
{
	int current=0;
	int Change=0;

	bool skip=false;
	if(Status==true)
	{
		if(Address==0 && out0==true) skip=true;
		if(Address==1 && out1==true) skip=true;
		if(Address==2 && out2==true) skip=true;
		if(Address==3 && out3==true) skip=true;
		if(Address==4 && out4==true) skip=true;
		if(Address==5 && out5==true) skip=true;
		if(Address==6 && out6==true) skip=true;
		if(Address==7 && out7==true) skip=true;
		//if(Address==8 && printon==true) skip=true;
	}
	else
	{
		if(Address==0 && out0==false) skip=true;
		if(Address==1 && out1==false) skip=true;
		if(Address==2 && out2==false) skip=true;
		if(Address==3 && out3==false) skip=true;
		if(Address==4 && out4==false) skip=true;
		if(Address==5 && out5==false) skip=true;
		if(Address==6 && out6==false) skip=true;
		if(Address==7 && out7==false) skip=true;
	}

	if(skip==false)
	{
		if(Address==0){current=254;if(Status==true){ out0=true; }else{out0=false;}}//trigger
		if(Address==1){current=253;if(Status==true){ out1=true; }else{out1=false;}}//lights
		if(Address==2){current=251;if(Status==true){ out2=true; }else{out2=false;}}
		if(Address==3){current=247;if(Status==true){ out3=true; }else{out3=false;}}
		if(Address==4){current=239;if(Status==true){ out4=true; }else{out4=false;}}//red light
		if(Address==5){current=223;if(Status==true){ out5=true; }else{out5=false;}}//green lite
		if(Address==6){current=191;if(Status==true){ out6=true; }else{out6=false;}}//consecutive rejects
		if(Address==7){current=127;if(Status==true){ out7=true; }else{out7=false;}}//motor height
		
		if(Status==true) Change=current + StoredStatus+1;
			
		if(Status==false) Change= StoredStatus-current -1;
		int Change2=Change & 255;
		
		//theapp->m_pMod0->OutportVal(Change2,0);
		StoredStatus=Change2;
	}
}


void CMainFrame::OnClose() 
{
	//Prompt user before closing program	
	int result=AfxMessageBox("Do you want to close the application?", MB_YESNO);
	if(IDYES!=result) return;
	if(theapp->NoSerialComm==false)
	{

		m_pserial->SendChar(0,410);
	
	
	}

	PhotoEyeEnable(false); //Disable Photoeye when closing app
	
	//Close serial port
	if(theapp->NoSerialComm==false) 
	{				
		if(theapp->commProblem==false) m_pserial->ShutDown();
	}

	m_pcamera->m_bRestart = false;
	m_pcamera->m_bContinueGrabThread = false;		//Stop grab threads.

	
	Stop=true;	//Set flag indicating that the program is closing.
	
	SaveJob(CurrentJobNum);		//Save current settings.

	CMDIFrameWnd::OnClose();
	std::terminate();
}

//Open configure inspection dialog box.
void CMainFrame::OnSetup2() 
{
	if(is666received==false)	//If eject all bottles is not currently active.
	{
		if(SSetup==true)	//If user has required security level.
		{
			if(theapp->NoSerialComm==false) 
			{
				//m_pserial->SendMessage(WM_QUE,0,410);
				m_pserial->SendChar(0,410);
			}

			//Should not set the same timer 3 times.
			SetTimer(33,20,NULL);
			SetTimer(33,40,NULL);
			SetTimer(33,60,NULL);

			Insp insp;
			insp.DoModal();
		}
		else
		{
			Security sec;
			sec.DoModal();

			if(SSetup==true)
			{
		
				if(theapp->NoSerialComm==false) 
				{
					//m_pserial->SendMessage(WM_QUE,0,410);
					m_pserial->SendChar(0,410);
				}

				SetTimer(33,20,NULL);
				SetTimer(33,40,NULL);
				SetTimer(33,60,NULL);

				Insp insp;
				insp.DoModal();
			}
		}
	}
	else
	{

			prompt_code=27;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
}

//NOT USED
void CMainFrame::GotFault()
{
	Faulted=true;
	if(theapp->NoSerialComm==false) 
	{
		//m_pserial->SendMessage(WM_QUE,0,410);//red light
		m_pserial->SendChar(0,410);
	}

	UpdateOutput(5, true);
	UpdateOutput(4, false);
	UpdateOutput(7, false);
	m_pview->UpdateDisplay();

}

//NOT USED
void CMainFrame::OnPics() 
{
	Pics pics;
	pics.DoModal();
	
}

//Opens ejector configuration dialog box.
void CMainFrame::OnEject() 
{
	if(SEject==true) //Check if user has proper permission levels
	{
		Blowoff blowoff;
		blowoff.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SEject==true)
		{
			Blowoff blowoff;
			blowoff.DoModal();
		}
	}
}

//NOT USED
void CMainFrame::OnSetup3() 
{
	m_pcamera->ShowWindow(SW_SHOW); 
	m_pcamera->UpdateData(false);
	//m_pcamera->ModifyCams();
	
}

//Open security dialog box.
void CMainFrame::OnSecurity() 
{
	Security sec;
	sec.DoModal();	
}

//Open lights configuration dialog box
void CMainFrame::OnLight() 
{
	if(SSetup==true) //Check if user has proper access level.
	{
		Light light;
		light.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SSetup==true)
		{
			Light light;
			light.DoModal();
		}
	}
}

//Open camera tunnel height adjustment (motor)
void CMainFrame::OnMotor() 
{
	//Idea is that when 666received=true, external I/O Reject is oN and lock out 
	//Motor Height dialog
	if(is666received==false)	//If eject all bottles is not currently active.
	{
	
		if(SSetup==true)	//If user has proper security access.
		{
			if(theapp->NoSerialComm==false) 
			{
				//m_pserial->SendMessage(WM_QUE,0,410);
				m_pserial->SendChar(0,410);
			}
			//Should not set the same timer ID 3 times.
			SetTimer(33,20,NULL);
			SetTimer(33,40,NULL);
			SetTimer(33,60,NULL);
			Motor motor;
			motor.DoModal();
			
		}
		else
		{
			Security sec;
			sec.DoModal();

			if(SSetup==true)
			{
				if(theapp->NoSerialComm==false) 
				{
					//m_pserial->SendMessage(WM_QUE,0,410);
					m_pserial->SendChar(0,410);
				}
				SetTimer(33,20,NULL);
				SetTimer(33,40,NULL);
				SetTimer(33,60,NULL);
				Motor motor;
				motor.DoModal();
			}
		}	
	}
	else
	{
			prompt_code=27;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
}

//Open bottle down dialog box.
void CMainFrame::OnBottledown() 
{
	if(SModify==true)	//Check security level
	{
		
		BottleDown botdn;
		botdn.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();

		if(SModify==true)
		{
		
			BottleDown botdn;
			botdn.DoModal();
		}
	}	
	

}

//Opens the jobs selection dialog box.
void CMainFrame::OnJobchange() 
{	

	if(is666received==false)//If eject all bottles is not currently active.
	{
		if(SJob==true) //If user has proper security level
		{
			
			Job job;
			job.DoModal();
		}
		else
		{
			Security sec;
			sec.DoModal();

			if(SJob==true)
			{
			
				Job job;
				job.DoModal();
			}
		}	
	}
	else
	{

			prompt_code=27;
			Prompt3 promtp3;
			promtp3.DoModal();

	}
}

//NOT USED
void CMainFrame::CheckStatus()
{
	if(ToggleCheckStatus==true) 
	{
		if(theapp->NoSerialComm==false) 
		{
						
			Condition1=m_pserial->StatusByte;
		}
		OldTriggerCount=TriggerCount;
		ToggleCheckStatus=false;
	}
	else
	{

		if(theapp->NoSerialComm==false) 
		{
						
			Condition2=m_pserial->StatusByte;
		}
		ToggleCheckStatus=true;
		NewTriggerCount=TriggerCount;
		if(Condition1 != Condition2)
		{
			if(OldTriggerCount==NewTriggerCount)
			{
				m_pxaxis->ReEstablishCameras();
				//AfxMessageBox("in lock");
				LockUp+=1;
			}
		}
	}
}

//Open photoeye/product flow configuration dialog box.
void CMainFrame::OnPhotoeye() 
{
	
	
	if(SSetup==true)
	{
		Photoeye photo;
		photo.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SSetup==true)
		{
			Photoeye photo;
			photo.DoModal();
		}
	}
}

BOOL CMainFrame::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CMDIFrameWnd::DestroyWindow();
}

//Enables/Disables photo eye triggering. Used in configure (inspection) dialog box to turn off the photo eye while the job is being setup.
void CMainFrame::PhotoEyeEnable(bool enabled)
{
	if(theapp->NoSerialComm==false) 
	{
	
		if(enabled==true)
		{
			//m_pserial->SendMessage(WM_QUE,0,300);
			m_pserial->SendChar(0,300);
			
		}
		else
		{
			//m_pserial->SendMessage(WM_QUE,0,310);
			m_pserial->SendChar(0,310);
		}
	}
}

//Open customer digital IO configuration dialog box.
void CMainFrame::OnDigio() 
{
	if(SSetup==true)
	{
		DigIO digio;
		digio.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SSetup==true)
		{
			DigIO digio;
			digio.DoModal();
		}
	}
}

//NOT USED
void CMainFrame::OnData() 
{
	Data data;
	data.DoModal();
	
}

//Calls the apps ReadRegistry function. Reads the specified job from the registry. Used in the job dialog box.
void CMainFrame::GetJob(int JobNum)
{
	theapp->ReadRegistery(JobNum);	
}

void CMainFrame::DeleteSubKey(int JobNum)
{
	theapp->DeleteKey(JobNum);	
}


//Save current job settings and some system settings such as password, camera serial numbers and settings of the inspection configuration.
// Called from various dialog boxes.
void CMainFrame::SaveJob(int jobNum)
{
	theapp=(CCapApp*)AfxGetApp();
	theapp->jobMutex.lock();
	// Write each record in a separate Registry folder
	char inspnum[10];
	char folderName[255];
	int curInsp=1;
	
	sprintf(folderName, "JobArray\\Job%03d", jobNum);
	theapp->WriteProfileString(folderName, "jobname", theapp->jobinfo[jobNum].jobname);
	
	theapp->WriteProfileInt(folderName,"taught",theapp->jobinfo[jobNum].taught);
	theapp->WriteProfileInt(folderName,"lightLevel1",theapp->jobinfo[jobNum].lightLevel1);
	theapp->WriteProfileInt(folderName,"lightLevel2",theapp->jobinfo[jobNum].lightLevel2);
	theapp->WriteProfileInt(folderName,"lightLevel3",theapp->jobinfo[jobNum].lightLevel3);

	theapp->WriteProfileInt(folderName, "vertBarX", theapp->jobinfo[jobNum].vertBarX);
	theapp->WriteProfileInt(folderName, "midBarY", theapp->jobinfo[jobNum].midBarY);
	theapp->WriteProfileInt(folderName, "neckBarY", theapp->jobinfo[jobNum].neckBarY);

	theapp->WriteProfileInt(folderName, "is_NeckBarY_offset_negative", theapp->jobinfo[jobNum].is_NeckBarY_offset_negative);

	if( theapp->jobinfo[jobNum].is_NeckBarY_offset_negative == 1){theapp->jobinfo[jobNum].neckBarY_offset=theapp->jobinfo[jobNum].neckBarY_offset*(-1);}
	theapp->WriteProfileInt(folderName, "neckBarY_offset", theapp->jobinfo[jobNum].neckBarY_offset);

		
	theapp->WriteProfileInt(folderName, "cam1_ll",theapp->jobinfo[jobNum].cam1_ll);
	theapp->WriteProfileInt(folderName, "cam1_ul",theapp->jobinfo[jobNum].cam1_ul);

	theapp->WriteProfileInt(folderName, "cam2_ll",theapp->jobinfo[jobNum].cam2_ll);
	theapp->WriteProfileInt(folderName, "cam2_ul",theapp->jobinfo[jobNum].cam2_ul);

	theapp->WriteProfileInt(folderName, "cam3_ll",theapp->jobinfo[jobNum].cam3_ll);
	theapp->WriteProfileInt(folderName, "cam3_ul",theapp->jobinfo[jobNum].cam3_ul);

	theapp->WriteProfileInt(folderName, "do_sobel",theapp->jobinfo[jobNum].do_sobel);

	theapp->WriteProfileInt(folderName, "etb_cam1",theapp->jobinfo[jobNum].etb_cam1);
	theapp->WriteProfileInt(folderName, "etb_cam2",theapp->jobinfo[jobNum].etb_cam2);
	theapp->WriteProfileInt(folderName, "etb_cam3",theapp->jobinfo[jobNum].etb_cam3);

	theapp->WriteProfileInt(folderName, "is_diet_coke",theapp->jobinfo[jobNum].is_diet_coke);

	theapp->WriteProfileInt(folderName, "limit_no_cap",theapp->jobinfo[jobNum].limit_no_cap);
	
	theapp->WriteProfileInt(folderName, "lDist", theapp->jobinfo[jobNum].lDist);

	theapp->WriteProfileInt(folderName, "taught_cap_width",m_pxaxis->learnedCapWidth);
			
	theapp->WriteProfileInt(folderName, "tBandX", theapp->jobinfo[jobNum].tBandX);
	theapp->WriteProfileInt(folderName, "tBandY", theapp->jobinfo[jobNum].tBandY);
	theapp->WriteProfileInt(folderName, "tBandDx", theapp->jobinfo[jobNum].tBandDx);
	theapp->WriteProfileInt(folderName, "tBandDy", theapp->jobinfo[jobNum].tBandDy);
	theapp->WriteProfileInt(folderName, "tBandThresh", theapp->jobinfo[jobNum].tBandThresh);
	theapp->WriteProfileInt(folderName, "tBandSurfThresh", theapp->jobinfo[jobNum].tBandSurfThresh);
	theapp->WriteProfileInt(folderName, "tBandSurfThresh2", theapp->jobinfo[jobNum].tBandSurfThresh2);

	theapp->WriteProfileInt(folderName, "tBand2X", theapp->jobinfo[jobNum].tBand2X);
	theapp->WriteProfileInt(folderName, "tBand2Y", theapp->jobinfo[jobNum].tBand2Y);
	theapp->WriteProfileInt(folderName, "tBand2Dx", theapp->jobinfo[jobNum].tBand2Dx);
	theapp->WriteProfileInt(folderName, "lSWin", theapp->jobinfo[jobNum].lSWin);
	theapp->WriteProfileInt(folderName, "rSWin", theapp->jobinfo[jobNum].rSWin);

	theapp->WriteProfileInt(folderName, "rearCamDy", theapp->jobinfo[jobNum].rearCamDy);
	theapp->WriteProfileInt(folderName, "rearCamY", theapp->jobinfo[jobNum].rearCamY);
	
	theapp->WriteProfileInt(folderName, "is_fillX_neg", theapp->jobinfo[jobNum].is_fillX_neg);
//    if(theapp->jobinfo[jobNum].fillX <0) {theapp->jobinfo[jobNum].fillX= theapp->jobinfo[jobNum].fillX *(-1);}
	theapp->WriteProfileInt(folderName, "fillX", theapp->jobinfo[jobNum].fillX);
	theapp->WriteProfileInt(folderName, "fillY", theapp->jobinfo[jobNum].fillY);
			
	theapp->WriteProfileInt(folderName, "userStrength", theapp->jobinfo[jobNum].userStrength);
	theapp->WriteProfileInt(folderName, "userX", theapp->jobinfo[jobNum].userX);
	theapp->WriteProfileInt(folderName, "userY", theapp->jobinfo[jobNum].userY);
	theapp->WriteProfileInt(folderName, "userDx", theapp->jobinfo[jobNum].userDx);

	theapp->WriteProfileInt(folderName, "active", theapp->jobinfo[jobNum].active);
	theapp->WriteProfileInt(folderName, "u2Pos", theapp->jobinfo[jobNum].u2Pos);
	theapp->WriteProfileInt(folderName, "u2Thresh2", theapp->jobinfo[jobNum].u2Thresh2);
	theapp->WriteProfileInt(folderName, "u2Thresh1", theapp->jobinfo[jobNum].u2Thresh1);

	theapp->WriteProfileInt(folderName, "u2XOffset", theapp->jobinfo[jobNum].u2XOffset);
	theapp->WriteProfileInt(folderName, "u2YOffset", theapp->jobinfo[jobNum].u2YOffset);
	theapp->WriteProfileInt(folderName, "u2XOffsetR", theapp->jobinfo[jobNum].u2XOffsetR);
	theapp->WriteProfileInt(folderName, "u2YOffsetR", theapp->jobinfo[jobNum].u2YOffsetR);
	
	theapp->WriteProfileInt(folderName, "profile", theapp->jobinfo[jobNum].profile);
	
	theapp->WriteProfileInt(folderName, "tBandType", theapp->jobinfo[jobNum].tBandType);
	theapp->WriteProfileInt(folderName, "sensitivity", theapp->jobinfo[jobNum].sensitivity);
	theapp->WriteProfileInt(folderName, "enable", theapp->jobinfo[jobNum].enable);
	theapp->WriteProfileInt(folderName, "dark", theapp->jobinfo[jobNum].dark);
	theapp->WriteProfileInt(folderName, "motPos", theapp->jobinfo[jobNum].motPos);
	theapp->WriteProfileInt(folderName, "lip", theapp->jobinfo[jobNum].lip);
	theapp->WriteProfileInt(folderName, "lipx", theapp->jobinfo[jobNum].lipx);
	theapp->WriteProfileInt(folderName, "lipy", theapp->jobinfo[jobNum].lipy);
	theapp->WriteProfileInt(folderName, "lipyoffset", theapp->jobinfo[jobNum].lipYOffset);
	theapp->WriteProfileInt(folderName, "lipSize", theapp->jobinfo[jobNum].lipSize);
	theapp->WriteProfileInt(folderName, "rBandOffset1", theapp->jobinfo[jobNum].rBandOffset[1]);
	theapp->WriteProfileInt(folderName, "lBandOffset1", theapp->jobinfo[jobNum].lBandOffset[1]);
	theapp->WriteProfileInt(folderName, "rBandOffset2", theapp->jobinfo[jobNum].rBandOffset[2]);
	theapp->WriteProfileInt(folderName, "lBandOffset2", theapp->jobinfo[jobNum].lBandOffset[2]);
	theapp->WriteProfileInt(folderName, "rBandOffset3", theapp->jobinfo[jobNum].rBandOffset[3]);
	theapp->WriteProfileInt(folderName, "lBandOffset3", theapp->jobinfo[jobNum].lBandOffset[3]);
	theapp->WriteProfileInt(folderName, "rBandOffset4", theapp->jobinfo[jobNum].rBandOffset[4]);
	theapp->WriteProfileInt(folderName, "lBandOffset4", theapp->jobinfo[jobNum].lBandOffset[4]);
	theapp->WriteProfileInt(folderName, "rBandOffset5", theapp->jobinfo[jobNum].rBandOffset[5]);
	theapp->WriteProfileInt(folderName, "lBandOffset5", theapp->jobinfo[jobNum].lBandOffset[5]);

	theapp->WriteProfileInt(folderName, "enable_foil_sensor", theapp->jobinfo[jobNum].enable_foil_sensor);
	
	theapp->WriteProfileInt(folderName, "rearsurfthresh", theapp->jobinfo[jobNum].rearSurfThresh);
	theapp->WriteProfileInt(folderName, "capwidthlimit", theapp->jobinfo[jobNum].capWidthLimit);
	theapp->WriteProfileInt(folderName, "colortargr", theapp->jobinfo[jobNum].colorTargR);
	theapp->WriteProfileInt(folderName, "colortargg", theapp->jobinfo[jobNum].colorTargG);
	theapp->WriteProfileInt(folderName, "colortargb", theapp->jobinfo[jobNum].colorTargB);
	theapp->WriteProfileInt(folderName, "leftbandshift", theapp->jobinfo[jobNum].leftBandShift);
	theapp->WriteProfileInt(folderName, "rightbandshift", theapp->jobinfo[jobNum].rightBandShift);
	theapp->WriteProfileInt(folderName, "band2size", theapp->jobinfo[jobNum].band2Size);
	theapp->WriteProfileInt(folderName, "bandcolor", theapp->jobinfo[jobNum].bandColor);
	theapp->WriteProfileInt(folderName, "sport", theapp->jobinfo[jobNum].sport);
	theapp->WriteProfileInt(folderName, "usertype", theapp->jobinfo[jobNum].userType);
	theapp->WriteProfileInt(folderName, "neckoffset", theapp->jobinfo[jobNum].neckOffset);
	theapp->WriteProfileInt(folderName, "ringlight", theapp->jobinfo[jobNum].ringLight);
	theapp->WriteProfileInt(folderName, "backlight", theapp->jobinfo[jobNum].backLight);
	theapp->WriteProfileInt(folderName, "sportcapsen", theapp->jobinfo[jobNum].sportSen);
	theapp->WriteProfileInt(folderName, "sportcappos", theapp->jobinfo[jobNum].sportPos);
	theapp->WriteProfileInt(folderName, "xtralight", theapp->jobinfo[jobNum].xtraLight);
	theapp->WriteProfileInt(folderName, "redcutoff", theapp->jobinfo[jobNum].redCutOff);
	theapp->WriteProfileInt(folderName, "fillarea", theapp->jobinfo[jobNum].fillArea);
	theapp->WriteProfileInt(folderName, "snapdelay", theapp->jobinfo[jobNum].snapDelay);
	theapp->WriteProfileInt(folderName, "slatpw", theapp->jobinfo[jobNum].slatPW);
	theapp->WriteProfileInt(folderName, "encdist", theapp->jobinfo[jobNum].encDist);
	theapp->WriteProfileInt(folderName, "encdur", theapp->jobinfo[jobNum].encDur);
	////////////
	theapp->WriteProfileInt(folderName,"Rear_ht_Diff",theapp->jobinfo[jobNum].Rear_ht_Diff);
	////////////
	theapp->WriteProfileInt(folderName,"bottleReferenceBar",theapp->jobinfo[jobNum].bottleReferenceBar);

	theapp->WriteProfileInt(folderName,"bottleReferenceBar_cam2",theapp->jobinfo[jobNum].bottleReferenceBar_cam2);
	theapp->WriteProfileInt(folderName,"bottleReferenceBar_cam3",theapp->jobinfo[jobNum].bottleReferenceBar_cam3);
	
	theapp->WriteProfileInt(folderName, "useleft", theapp->jobinfo[jobNum].useLeft);
	theapp->WriteProfileInt(folderName, "usering", theapp->jobinfo[jobNum].useRing);
	
	theapp->jobinfo[jobNum].totinsp=10;

	std::string abc(folderName);

	for (int curInspctn=1; curInspctn<=theapp->jobinfo[jobNum].totinsp; curInspctn++)//theapp->jobinfo[jobnum].totinsp
	{		
		sprintf(inspnum, "\\Insp%03d", curInspctn);
	//	strcat(folderName,inspnum);

		abc=abc+inspnum;

		theapp->WriteProfileString(abc.c_str(), "name", theapp->inspctn[curInspctn].name);
		theapp->WriteProfileInt(abc.c_str(), "lmin", theapp->inspctn[curInspctn].lmin);
		theapp->WriteProfileInt(abc.c_str(), "lmax", theapp->inspctn[curInspctn].lmax);
		theapp->WriteProfileInt(abc.c_str(), "enable", theapp->inspctn[curInspctn].enable);

		int n=abc.length();
		abc=abc.substr(0,n-8);
	}
	
	if(theapp->inspctn[9].enable==true)theapp->inspctn[10].enable=true;
	if(theapp->inspctn[9].enable==false)theapp->inspctn[10].enable=false;

	if(jobNum!=0)
	{
		//theapp->SaveDataToReg();
		//theapp->WriteProfileInt("EncDist", "Current", theapp->SavedEncDist);
		//theapp->WriteProfileInt("EncDur", "Current", theapp->SavedEncDur);
		theapp->WriteProfileInt("Eject", "Current", theapp->SavedEjectDisable);
		theapp->WriteProfileInt("SavedDelay", "Current", theapp->SavedDelay);
		//WriteProfileInt("Thresh", "Current", SavedThresh);
		//WriteProfileInt("TB2Thresh", "Current", SavedTB2Thresh);
		theapp->WriteProfileInt("Photo", "Current", theapp->SavedPhotoeye);
		
		//WriteProfileInt("Setup", "Current", SavedSetup);
		//WriteProfileInt("Limits", "Current", SavedLimits);
		//WriteProfileInt("SEject", "Current", SavedEject);
		//WriteProfileInt("Modify", "Current", SavedModify);
		//WriteProfileInt("Job", "Current", SavedJob);

		theapp->WriteProfileInt("FillHeight", "Current", theapp->SavedFillH);
		theapp->WriteProfileInt("FillWidth", "Current", theapp->SavedFillW);
		//WriteProfileInt("TBH", "Current", SavedTBH);
		//WriteProfileInt("TB2H", "Current", SavedTB2H);
		//WriteProfileInt("Type", "Current", SavedInspType);
		//WriteProfileInt("black", "Current", SavedBlack);
	
		theapp->WriteProfileInt("EncSim", "Current", theapp->SavedEncSim);
		//WriteProfileInt("LWin", "Current", SavedLeftSWin);
	//	WriteProfileInt("RWin", "Current", SavedRightSWin);
		theapp->WriteProfileInt("TBl", "Current", theapp->SavedTBl);//*****************OK
		theapp->WriteProfileInt("Photo", "Current", theapp->SavedPhotoeye);
		theapp->WriteProfileInt("IORejects", "Current", theapp->SavedIORejects);
		theapp->WriteProfileInt("ConRejects", "Current", theapp->SavedConRejects);
		theapp->WriteProfileInt("KillSave", "Current", theapp->SavedKillSave);
		//theapp->WriteProfileInt("KillSD", "Current", theapp->SavedKillSD);
		//WriteProfileInt("Level", "Current", SavedLightLevel);
	}
	theapp->WriteProfileString("Pass", "Current", theapp->SavedPassWord);
	theapp->WriteProfileString("PassOP", "Current", theapp->SavedOPPassWord);
	theapp->WriteProfileString("PassMAN", "Current", theapp->SavedMANPassWord);
	theapp->WriteProfileString("PassSUP", "Current", theapp->SavedSUPPassWord);
	theapp->WriteProfileString("PassMASTER", "Current", theapp->SavedMaster);
	theapp->WriteProfileInt("forceejects", "Current",  theapp->SavedForcedEjects);
	theapp->WriteProfileInt("dboffset", "Current",  theapp->SavedDBOffset);
	theapp->WriteProfileInt("botdn", "Current", theapp->SavedBottleSensor);
	theapp->WriteProfileInt("deadband", "Current", theapp->SavedDeadBand);
	theapp->WriteProfileInt("fillerpulse", "Current", theapp->fillerPulse);
	
	theapp->WriteProfileInt("fillersize", "Current", theapp->fillerSize);
	theapp->WriteProfileInt("cappersize", "Current", theapp->capperSize);

	theapp->WriteProfileInt("camser1","Current", theapp->CamSer1);
	theapp->WriteProfileInt("camser2","Current", theapp->CamSer2);
	theapp->WriteProfileInt("camser3","Current", theapp->CamSer3);

	theapp->WriteProfileInt("tempSelect","Current", theapp->tempSelect);

	


	theapp->WriteProfileInt("cam2_ht_offset", "Current", theapp->cam2_ht_offset);
	theapp->WriteProfileInt("cam3_ht_offset", "Current", theapp->cam3_ht_offset);

	theapp->WriteProfileInt("pocketnum", "Current", theapp->convInches);


	theapp->WriteProfileInt("fillpocketoffset", "Current", theapp->fillPocketOffset);
	theapp->WriteProfileInt("cappocketoffset", "Current", theapp->capPocketOffset);	
	theapp->WriteProfileInt("cam1top", "Current", theapp->cam1Top);	
	theapp->WriteProfileInt("cam2top", "Current", theapp->cam2Top);	
	theapp->WriteProfileInt("cam3top", "Current", theapp->cam3Top);	
	theapp->WriteProfileInt("templimit", "Current", theapp->tempLimit);	
	theapp->WriteProfileInt("surfrr", "Current", theapp->surfRR);	
	theapp->WriteProfileInt("surflr", "Current", theapp->surfLR);	
	theapp->WriteProfileInt("userrr", "Current", theapp->userRR);	
	theapp->WriteProfileInt("userlr", "Current", theapp->userLR);	
	theapp->WriteProfileInt("check_Rband", "Current", theapp->check_Rband);	
	theapp->WriteProfileInt("convratio", "Current", theapp->convRatio);	
	theapp->WriteProfileInt("xtrayoffset", "Current", theapp->xtraYOffset);

	theapp->WriteProfileInt("checkRearHeight", "Current", theapp->checkRearHeight);

	char charBuf[4];	

	sprintf(charBuf,"%2.1f",theapp->shutterSpeed);
	theapp->WriteProfileString("shuttercam1", "Current", charBuf);

	char charBuf2[4];	

	sprintf(charBuf2,"%2.1f",theapp->shutterSpeed2);
	theapp->WriteProfileString("shuttercam2", "Current", charBuf2);

	char charBuf3[4];	

	sprintf(charBuf3,"%2.1f",theapp->shutterSpeed3);
	theapp->WriteProfileString("shuttercam3", "Current", charBuf3);


	//////////////////////////////
	//ML- 
	theapp->WriteProfileInt("inspdelay",		"Current", theapp->inspDelay);

	theapp->WriteProfileInt("bandwidth1",		"Current", theapp->bandwidth1);
	theapp->WriteProfileInt("bandwidth2",		"Current", theapp->bandwidth2);

	theapp->WriteProfileInt("whitebalance1Red",		"Current", theapp->whitebalance1Red);
	theapp->WriteProfileInt("whitebalance2Red",		"Current", theapp->whitebalance2Red);
	theapp->WriteProfileInt("whitebalance3Red",		"Current", theapp->whitebalance3Red);
	theapp->WriteProfileInt("whitebalance1Blue",	"Current", theapp->whitebalance1Blue);
	theapp->WriteProfileInt("whitebalance2Blue",	"Current", theapp->whitebalance2Blue);
	theapp->WriteProfileInt("whitebalance3Blue",	"Current", theapp->whitebalance3Blue);
	//////////////////////////////

	theapp->jobMutex.unlock();

}

//NOT USED
void CMainFrame::CleanUp()
{


}

//Open delay trigger dialog box.
void CMainFrame::OnSetup4() 
{

	if(SModify==true) //Check if user has required settings.
	{
		SDelay sdelay;
		sdelay.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SModify==true)
		{
			SDelay sdelay;
			sdelay.DoModal();
		}
	}
	
}

//NOT USED
void CMainFrame::OnCamcal() 
{

	if(SModify==true)
	{
		m_pcamera->ModifyCams();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SModify==true)
		{
			m_pcamera->ModifyCams();
		}
	}
}

//Logout of current security level.
void CMainFrame::OnLogout() 
{
	SetTimer(24,100,NULL);
	UpdateData(false);	

//	SetTimer(24,500,NULL);
	SetTimer(41,1000,NULL);
}

//Re-teach color main menu button.
void CMainFrame::OnTeach() 
{
	if(SJob==true)	//Check if user has access to jobs selection dialog box.
	{
		m_pxaxis->reteachColor=true;
	
	}
	else
	{
	//	AfxMessageBox("Please login with a minimum of the Label Operator level password!");
			prompt_code=14;
			Prompt3 prompt3;
			prompt3.DoModal();
		Security sec;
		sec.DoModal();
		if(SJob==true)
		{
			m_pxaxis->reteachColor=true;
	
		}
	}
	
}

//NOT USED
void CMainFrame::RunHelp()
{
	CString currentpath = "c:\\silgandata\\cap.pdf";
	
	CString c=currentpath;
	system(c);
}


//Show camera alignment overlay.
void CMainFrame::OnAligncams() 
{
	if(theapp->showAlign==false)
	{
		theapp->showAlign=true;
	}
	else
	{
		theapp->showAlign=false;
	}
	
}

//Open Filler/Capper Dialog Box
void CMainFrame::OnFiller() 
{
	if(SModify==true) //if user has security access to the modify tab. Create modeless dialog box 
	{
		//SetTimer(27,1,NULL);//filler on
		//SetTimer(28,500,NULL);//activity off

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,0,710);
			m_pserial->SendChar(0,710);
		}

		pParent=GetParent(); 
		m_pfiller=new Filler(); 
		m_pfiller->Create(IDD_FILLER,pParent);
		m_pfiller->ShowWindow(SW_SHOW);
	}
	else //Otherwise prompt the user with an error message and tell them they need to login with a higher access level.
	{
	//	AfxMessageBox("Please login with a minimum of the Filler Operator level password!");

			prompt_code=13;
			Prompt3 promtp3;
			promtp3.DoModal();

		Security sec;
		sec.DoModal();
		if(SModify==true)
		{
			if(theapp->NoSerialComm==false) 
			{
				//m_pserial->SendMessage(WM_QUE,0,710);
				m_pserial->SendChar(0,710);
			}

			pParent=GetParent(); 
			m_pfiller=new Filler(); 
			m_pfiller->Create(IDD_FILLER,pParent);
			m_pfiller->ShowWindow(SW_SHOW);
		}
	}
}

//NOT USED? Open Filler/Capper Data Dialog Box
void CMainFrame::OnFillerData() 
{
	
	if(SModify==true)
	{
			FillerData fillerdata;
			fillerdata.DoModal();
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SModify==true)
		{
			FillerData fillerdata;
			fillerdata.DoModal();
		}
	}
}

//Open Camera Settings Dialog Box
void CMainFrame::OnCameras() 
{	
	SSecretMenu=false;	//	Resets upon entry
	Security sec;
	sec.DoModal();

	if(SSecretMenu==true)
	{		
		if(SModify==true)
		{
			m_pcamera->ShowWindow(SW_SHOW); 
			m_pcamera->UpdateData(false);
		}
	}	
}

//Backs up scan statistics to hard drive. Used to back up statistics every 24 hours.
void CMainFrame::Update24HourData()
{
		
		CTime thetime = CTime::GetCurrentTime();	//Time used to append to file name.
		if( (thetime.GetHour()==theapp->hourClock && CopyDone==false) || testData==true)	//Check if it is time to save and copying is not in progress.
		{
			
			CopyDone=true;		//Set flag indicating that copying is in progress.
			//testData=false;
			//s="e:\\" + s + ".xls";
			char buf[10];
			//int filenum=theapp->SavedDataFile+=1;
			CTime thetime = CTime::GetCurrentTime();	//called twice?
			char* CurrentName="24Hrdata";

			CString s = thetime.Format( "%m%d%Y" );
			CString d = CurrentName;
			//CString d4;

			//Format file name.
			//Base file name
			char* pFileName = "c:\\silgandata\\";//drive	
			char currentpath[60];
			strcpy(currentpath,pFileName);
			strcat(currentpath,d);
			strcat(currentpath,s);
			
			strcat(currentpath,".txt");

			//Write data to file.
			DataFile.Open(currentpath, CFile::modeWrite | CFile::modeCreate);

			int allCap=m_pxaxis->E1Cnt24+m_pxaxis->E2Cnt24+m_pxaxis->E3Cnt24+m_pxaxis->E4Cnt24;
			int allUser=m_pxaxis->E9Cnt24+m_pxaxis->E10Cnt24;

			m_r1.Format("%3i",allCap);//CapHeight

			m_r5.Format(" %3i",m_pxaxis->E5Cnt24);
			m_r6.Format(" %3i",m_pxaxis->E6Cnt24);
			m_r7.Format(" %3i",m_pxaxis->E7Cnt24);
			m_r8.Format(" %3i",m_pxaxis->E8Cnt24);
			m_r9.Format(" %3i",allUser);
			//m_r10.Format(" %3i",m_pxaxis->E10Cnt24);
			
			m_r11.Format(" %8i",Total24);
			m_r12.Format(" %5i",Rejects24);

	DataFile.Write("AllCapDefect",12);
	DataFile.Write(m_r1,m_r1.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("BandEdge",8);
	DataFile.Write(m_r5,m_r5.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("BandSurface",11);
	DataFile.Write(m_r6,m_r6.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("Fill", 4);
	DataFile.Write(m_r7,m_r7.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("CapColor",8);
	DataFile.Write(m_r8,m_r8.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("User",4);
	DataFile.Write(m_r9,m_r9.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	//DataFile.Write("User2",5);
	//DataFile.Write(m_r10,m_r10.GetLength());
	//DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	
	DataFile.Write("Total",5);
	DataFile.Write(m_r11,m_r11.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("Rejects",7);
	DataFile.Write(m_r12,m_r12.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);
			
			DataFile.Close();

			//Reset 24 hour statistic counters.
			m_pxaxis->E1Cnt24=0;
			m_pxaxis->E2Cnt24=0;
			m_pxaxis->E3Cnt24=0;
			m_pxaxis->E4Cnt24=0;
			m_pxaxis->E5Cnt24=0;
			m_pxaxis->E6Cnt24=0;
			m_pxaxis->E7Cnt24=0;
			m_pxaxis->E8Cnt24=0;
			m_pxaxis->E9Cnt24=0;
			m_pxaxis->E10Cnt24=0;
			Total24=0;
			Rejects24=0;

			//strcpy(currentpath2,s);
		}

		if(thetime.GetHour()!=theapp->hourClock)
		{
			CopyDone=false;		//Clear flag indicating that  data is being copied to file.
		}
	
}

//Open advanced settings dialog box.
void CMainFrame::OnAdvanced() 
{

	if(SModify==true)
	{
		Advanced advanced;
		advanced.DoModal();	
	}
	else
	{
		Security sec;
		sec.DoModal();
		if(SModify==true)
		{
			Advanced advanced;
			advanced.DoModal();
		}
	}
}

//Backs up scan statistics to hard drive. Called when a new job is selected. This saves the statistics of the previous job and resets the counters.
void CMainFrame::PrintJobData()
{

char buf[10];
			int filenum=theapp->SavedDataFile+=1;		//Increment file number.
			CString n =itoa(filenum,buf,10);
			
			//Get time to append to file name.
			CTime thetime = CTime::GetCurrentTime();
			char* CurrentName="Jobdata";

			CString s = thetime.Format( "%m%d%Y" );
			
			CString d = CurrentName;
			//CString d4;

			//Create file name.
			char* pFileName = "c:\\silgandata\\";//drive	
			char currentpath[60];
			strcpy(currentpath,pFileName);
			strcat(currentpath,d);
			strcat(currentpath,s);
			strcat(currentpath,n);
			strcat(currentpath,".txt");

			DataFile.Open(currentpath, CFile::modeWrite | CFile::modeCreate);

			//int allCap=m_pxaxis->E1Cnt24+m_pxaxis->E2Cnt24+m_pxaxis->E3Cnt24+m_pxaxis->E4Cnt24;
	int allUser=m_pxaxis->E9Cntx+m_pxaxis->E10Cntx;

	//Format values for saving to file. 
	m_r1.Format("%3i",m_pxaxis->E1Cntx);//missing
	m_r2.Format("%3i",m_pxaxis->E2Cntx);//cocked
	m_r3.Format("%3i",m_pxaxis->E3Cntx);//loose
	m_r4.Format("%3i",m_pxaxis->E4Cntx);//sport
	m_r5.Format(" %3i",m_pxaxis->E5Cntx);
	m_r6.Format(" %3i",m_pxaxis->E6Cntx);
	m_r7.Format(" %3i",m_pxaxis->E7Cntx);
	m_r8.Format(" %3i",m_pxaxis->E8Cntx);
	m_r9.Format(" %3i",allUser);
			//m_r10.Format(" %3i",m_pxaxis->E10Cnt24);
			
	m_r11.Format(" %8i",TotalJob);
	m_r12.Format(" %5i",RejectsJob);

	//Save number of each type of inspection failed.
	DataFile.Write("missing",7);
	DataFile.Write(m_r1,m_r1.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("cocked",6);
	DataFile.Write(m_r2,m_r2.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("loose",5);
	DataFile.Write(m_r3,m_r3.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);
if(theapp->jobinfo[CurrentJobNum].sport==1)
{
	DataFile.Write("Sport",5);
	DataFile.Write(m_r4,m_r4.GetLength());
	DataFile.Write(", ",1);
}
else
{

}
	//DataFile.Write("\n",1);

	DataFile.Write("BandEdge",8);
	DataFile.Write(m_r5,m_r5.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("BandSurface",11);
	DataFile.Write(m_r6,m_r6.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("Fill", 4);
	DataFile.Write(m_r7,m_r7.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("CapColor",8);
	DataFile.Write(m_r8,m_r8.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("User",4);
	DataFile.Write(m_r9,m_r9.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	//DataFile.Write("User2",5);
	//DataFile.Write(m_r10,m_r10.GetLength());
	//DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	
	//Save the total number of scans and total rejected.		
	DataFile.Write("Total",5);
	DataFile.Write(m_r11,m_r11.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("Rejects",7);
	DataFile.Write(m_r12,m_r12.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);
			
			DataFile.Close();

			m_pxaxis->E1Cntx=0;
			m_pxaxis->E2Cntx=0;
			m_pxaxis->E3Cntx=0;
			m_pxaxis->E4Cntx=0;
			m_pxaxis->E5Cntx=0;
			m_pxaxis->E6Cntx=0;
			m_pxaxis->E7Cntx=0;
			m_pxaxis->E8Cntx=0;
			m_pxaxis->E9Cntx=0;
			m_pxaxis->E10Cntx=0;
			TotalJob=0;
			RejectsJob=0;

}

//Bugs in this function m_pserial->SendMessage(Value,Function); should be changed to m_pserial->SendChar(Value, Function);
//Used with central (client) PLC, sends gathered statistics data external (client) PLC.
void CMainFrame::UpdatePLCData()
{
	int Value=0;
	int Function=0;
	int EjectRate2=0;

	timesThru+=1;
	if(timesThru>=12) timesThru=1;
	
	switch(timesThru)
	{
	case 1:
		Value=Total;
		Function=650;//gross total    *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
				m_pserial->SendChar(Value,Function);
		}
	
		break;

	case 2:
		Value=theapp->TotalNet;
		Function=660;//net bottle count    *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
			
				m_pserial->SendChar(Value,Function);
		}
		break;

	case 3:
		Value=m_pxaxis->E5Cntx;
		Function=710;//TB Edge    *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
				m_pserial->SendChar(Value,Function);
		}
		break;

	case 4:
		Value=m_pxaxis->E6Cntx;
		Function=720;//TB Surface     *
		
		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		

			m_pserial->SendChar(Value,Function);
		}
		break;

	case 5:
		Value=m_pxaxis->E7CntNet;
		Function=670;//Low Fill      *

		if(theapp->NoSerialComm==false) 
		{		
			//m_pserial->SendMessage(WM_QUE,value,function);
		
			m_pserial->SendChar(Value,Function);
		}

		break;

	case 6:
		Value=m_pxaxis->E8Cntx;
		Function=730;//cap Color     *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
			m_pserial->SendChar(Value,Function);
		}
		break;	
	case 7:
		Value=m_pxaxis->E2CntNet + m_pxaxis->E3CntNet;
		Function=690;//cocked      *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
			m_pserial->SendChar(Value,Function);
		}
		break;
		
	case 8:
		
		if(theapp->jobinfo[CurrentJobNum].sport==1)
		{
			Value=m_pxaxis->E4CntNet;
		}
		else
		{
			Value=0;
		}
		
		Function=700;//sports     *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
			m_pserial->SendChar(Value,Function);
		}

		break;	

	case 9:
		Value=theapp->statusInt;//status 1 -online  2-offline  3-poweroff  4-bad cam  5-bad light 6-over temp
		Function=750;//status

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
			m_pserial->SendChar(Value,Function);
										
		}
		break;
		
	case 10:
		Value=m_pxaxis->E1CntNet;
		Function=680;//no cap     *

		if(theapp->NoSerialComm==false) 
		{
			//m_pserial->SendMessage(WM_QUE,value,function);
		
			m_pserial->SendChar(Value,Function);							
		}

		break;

	case 11:

	dataReady=false;
		
		CTime thetime = CTime::GetCurrentTime();
		if( (thetime.GetHour()==theapp->hourClock && CopyDone==false) || testData==true)
		{
			
			CopyDone=true;
			testData=false;
			//s="e:\\" + s + ".xls";
			char buf[10];
			//int filenum=theapp->SavedDataFile+=1;
			CTime thetime = CTime::GetCurrentTime();
			char* CurrentName="24Hrdata";

			CString s = thetime.Format( "%m%d%Y" );
			CString d = CurrentName;
			//CString d4;

			char* pFileName = "c:\\silgandata\\";//drive	
			char currentpath[60];
			strcpy(currentpath,pFileName);
			strcat(currentpath,d);
			strcat(currentpath,s);
			
			strcat(currentpath,".txt");

			DataFile.Open(currentpath, CFile::modeWrite | CFile::modeCreate);

			int allUser=m_pxaxis->E9Cnt24+m_pxaxis->E10Cnt24;

			m_r1.Format("%3i",m_pxaxis->E1Cnt24);//CapHeight

			m_r2.Format(" %3i",m_pxaxis->E2Cnt24);
			m_r3.Format(" %3i",m_pxaxis->E3Cnt24);	
			m_r4.Format(" %3i",m_pxaxis->E4Cnt24);
			m_r5.Format(" %3i",m_pxaxis->E5Cnt24);
			m_r6.Format(" %3i",m_pxaxis->E6Cnt24);
			m_r7.Format(" %3i",m_pxaxis->E7Cnt24);
			m_r8.Format(" %3i",m_pxaxis->E8Cnt24);
			m_r9.Format(" %8i",allUser);
			
			m_r11.Format(" %3i",Total24);
		
			m_r12.Format(" %5i",Rejects24);

	//Save number of each type of inspection failed.
	DataFile.Write("missing",7);
	DataFile.Write(m_r1,m_r1.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("cocked",6);
	DataFile.Write(m_r2,m_r2.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("loose",5);
	DataFile.Write(m_r3,m_r3.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);
if(theapp->jobinfo[CurrentJobNum].sport==1)
{
	DataFile.Write("Sport",5);
	DataFile.Write(m_r4,m_r4.GetLength());
	DataFile.Write(", ",1);
}
else
{

}
	//DataFile.Write("\n",1);

	DataFile.Write("BandEdge",8);
	DataFile.Write(m_r5,m_r5.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("BandSurface",11);
	DataFile.Write(m_r6,m_r6.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("Fill", 4);
	DataFile.Write(m_r7,m_r7.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("CapColor",8);
	DataFile.Write(m_r8,m_r8.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("User",4);
	DataFile.Write(m_r9,m_r9.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	//DataFile.Write("User2",5);
	//DataFile.Write(m_r10,m_r10.GetLength());
	//DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	
	DataFile.Write("Total",5);
	DataFile.Write(m_r11,m_r11.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);

	DataFile.Write("Rejects",7);
	DataFile.Write(m_r12,m_r12.GetLength());
	DataFile.Write(", ",1);
	//DataFile.Write("\n",1);
			
			DataFile.Close();

			m_pxaxis->E1Cnt24=0;
			m_pxaxis->E2Cnt24=0;
			m_pxaxis->E3Cnt24=0;
			m_pxaxis->E4Cnt24=0;
			m_pxaxis->E5Cnt24=0;
			m_pxaxis->E6Cnt24=0;
			m_pxaxis->E7Cnt24=0;
			m_pxaxis->E8Cnt24=0;
			m_pxaxis->E9Cnt24=0;
			m_pxaxis->E10Cnt24=0;
			Total24=0;
			Rejects24=0;

			//strcpy(currentpath2,s);
		}

		if(thetime.GetHour()!=theapp->hourClock){CopyDone=false;}
		break;
	}
}

//Updates the PLC when a new job is selected. Called through timer set by the jobs dialog box when a new job is selected.
void CMainFrame::UpdatePLC()
{
	if(theapp->NoSerialComm==false) 
	{
						

		if(theapp->jobinfo[CurrentJobNum].sport==2)  //If foil sensor is enabled
		{			
			//m_pserial->SendChar(0,520);
			//m_pserial->SendChar(1,360);
			m_pserial->SendChar(0,520);
		}
		else
		{
			
			//m_pserial->SendChar(0,530);
		  	//m_pserial->SendChar(2,360);
			m_pserial->SendChar(0,530);
		}

		if(theapp->jobinfo[CurrentJobNum].xtraLight==true)	//If extra light is on
		{
	
			if(theapp->NoSerialComm==false) 
			{

				//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
				m_pserial->SendMessage(WM_QUE,0,430); 
			}

		}
		else//Opposite of above.
		{
			
			if(theapp->NoSerialComm==false) 
			{
				//ML- THIS IS COMMENTED BY THE ORIGINAL SOFTWARE... SO I WONT ADD ANYTHING
				m_pserial->SendMessage(WM_QUE,0,440);
			}
			
		}

		SetTimer(32,50,NULL);	//NOT USED
		SetTimer(34,100,NULL);	//slat parameters
		SetTimer(30,300,NULL);//periodically updates PLC with scan parameters.//slat timing
		SetTimer(35,500,NULL);	//duration of ejector	//duration
		SetTimer(36,700,NULL);	//distance of ejector to photoeye.	//distance
		SetTimer(37,900,NULL);//ring light duration	//ring light duration
	}
}

//NOT USED
void CMainFrame::OnUpdateFcData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}
	
//Open the recall pictures dialog box.
void CMainFrame::OnRecallDefective() 
{
	PicRecal picrecall;
	picrecall.DoModal();
	
}

//Open Camera Settings Dialog Box
void CMainFrame::OnAdjustshutter() 
{	
	SSecretMenu=false;	//	Resets upon entry
	Security sec;
	sec.DoModal();

	if(SSecretMenu==true)
	{	
		m_pcamera->ShowWindow(SW_SHOW);
	}	
}

//Open White Balance Dialog Box
void CMainFrame::OnAdjustWhtBal() 
{	
	SSecretMenu=false;	//	Resets upon entry
	Security sec;
	sec.DoModal();

	if(SSecretMenu==true)
	{	
		m_pwhtbal->ShowWindow(SW_SHOW);
	}	
}


/*
void CMainFrame::OnUsb() 
{

	
	m_pusb->ShowWindow(SW_SHOW);
	
}
*/

//NOT USED
void CMainFrame::Compare_temp()
{
		//m_pview->m_status="Cabinet Temp Exceeded!!!";
		//m_pview->UpdateDisplay();

}
