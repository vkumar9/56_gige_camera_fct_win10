//{{AFX_INCLUDES()
#include "mscomm.h"
//}}AFX_INCLUDES
#if !defined(AFX_SERIAL_H__F5F83A22_CAB3_41D2_908A_29AA2FA09B2F__INCLUDED_)
#define AFX_SERIAL_H__F5F83A22_CAB3_41D2_908A_29AA2FA09B2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Serial.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Serial dialog
typedef struct  {
	int data; 
	int function; } mA;

class Serial : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp; 
public:
	int statusChar;				//Used to check slat status.
	int Temperature;			//Temperature read from the PLC
	bool lockOut;				//NOT USED

	afx_msg LRESULT QueMess(WPARAM data, LPARAM function);
	int function2;				//Used for sending out from the serial port, this is the frame function code.
	int data2;					//Used for sending out data from the serial port.
	int inSendChar;				//NOT USED

	int currentCapperPosition;	//Number of the currently operating capper head. Seems to be used by filler capper dialog box.
	int currentFillerPosition;	//Number of the currently operating filler head. Seems to be used by filler capper dialog box.
	int fillerPos;				//Number of the currently operating filler head. Seems to be used by filler capper dialog box.
	int datax;	//NOT USED
	int functionx;	//NOT USED
	mA messArray[20];	//NOT USED
	int len1;	//NOT USED
	int len2;	//NOT USED
	int len3;	//NOT USED
	int len4;	//NOT USED
	void ShowResults(bool ok);
	char bufz[10];				//Used for converting the send/receive counter when a problem is detected.
	CString comString;			//When the send/receive counter is to large, indicating an error, this string is used to hold the number of sends with no receive.
	bool didComOnce;			//Flag indicating that a serial port error caused by to many sends with no receives.
	int count;	//NOT USED
	int count2;	//NOT USED
	void ShutDown();
	int noCommCount;			//Used to track send/receive count. increment for send, decrement for receive, should be 0.
	int noCommCount2;			//Used to track send/receive count, duplicate of above.
	int sentInt;	//NOT USED
	int returnedInt;	//NOT USED
	int MotorPos;				//Position of camera tunnel motor.
	int StatusByte;
	bool CommProblem;			//Flag indicating that there is a problem connecting to the PLC through the serial port 
	int CapPos;					//Number of the currently operating capper head. Used for displaying info in this dialog box.. Seems to be used by Xaxisview.
	int FillPos;				//Number of the currently operating filler head. Used for displaying info in this dialog box.
	int inQue;				//NOT USED
	long ControlChars;		//NOT USED
	long ControlChars2;		//NOT USED
	Serial(CWnd* pParent = NULL);   // standard constructor
	void GetChar(int FromPLC);
	void GetChar2(int FromPLC);					//NOT USED
	bool SendChar(int data, int function);		//NOT USED
	bool SendChar2(int data, int function);
	void QueMessage(int data, int function);	//NOT USED
	CString CharsToPLC;			//String used to display the string sent to the PLC.
	CString CharsFromPLC;		//Parsed string from received buffer.
	CString CharsFromPLC1;		//String from the received buffer
	CString ch;					//Assembler, combined output of st[] and some number.
	CString ch2;	//NOT USED
	CString a;					//Variable used for parsing received data.
	CString CharBuf1;	//NOT USED
	CString CharBuf2;	//NOT USED
	CString CharBuf3;	//NOT USED
	CString CharBuf4;	//NOT USED
	CString CharBuf5;	//NOT USED
	CString returnedChar;		//Parsed piece of string from received buffer.
	CString sentChar;			//Assembler, part of ch which is sent out to PLC.
	CString b;					//Variable used for parsing received data.
	CString f1;					//Assembler for sending data out to PLC. Converted from char[] to CString
	CString d1;					//Assembler for sending data out to PLC. Converted from char[] to CString
	char st[20];				//Assembler, combined output from f1 and d1.
	char st1[20];				//Assembler for sending data out to PLC. Converted from long to char[]
	char st2[20];				//Assembler for sending data out to PLC. Converted from long to char[]
	VARIANT buf;				//Data is written out of this buffer to the serial port.
	VARIANT buf2;				//Data from the serial port is read into this buffer.
	long L1;					//Assembler for sending data out to PLC.
	long L2;					//Assembler for sending data out to PLC.
// Dialog Data
	//{{AFX_DATA(Serial)
	enum { IDD = IDD_SERIAL };
	CMSComm	m_serial;			//Reference to the activeX control
	CString	m_chars;			//GUI variable used to display characters received from the PLC.
	CString	m_chars2;			//GUI variable used to display characters sent to the PLC.
	CString	m_commcount;		//GUI variable used to display the send/receive count.
	CString	m_endchar;		//NOT USED
	CString	m_filler;			//GUI variable used to display current filler head being used.
	CString	m_motor;			//GUI variable used to display current motor position.
	CString	m_capper;			//GUI variable used to display current capper head being used.
	CString	m_editinchar;	//NOT USED
	CString	m_commcount2;		//GUI variable used to display the send/receive count, duplicate of m_commcount.
	//}}AFX_DATA

	CMainFrame* pframe;
	CCapApp* theapp;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Serial)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Serial)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIAL_H__F5F83A22_CAB3_41D2_908A_29AA2FA09B2F__INCLUDED_)
