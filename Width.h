#if !defined(AFX_WIDTH_H__FBF0597D_64D4_4AAC_A45C_09DF99DA4CE5__INCLUDED_)
#define AFX_WIDTH_H__FBF0597D_64D4_4AAC_A45C_09DF99DA4CE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Width.h : header file
//
friend class CCapApp;
friend class CMainFrame;
/////////////////////////////////////////////////////////////////////////////
// CWidth dialog

class CWidth : public CDialog
{
// Construction
public:
	CWidth(CWnd* pParent = NULL);   // standard constructor
	CCapApp *theapp;
	CMainFrame* pframe;
// Dialog Data
	//{{AFX_DATA(CWidth)
	enum { IDD = IDD_WIDTH };
	CString	m_sdelay;
	//}}AFX_DATA
	long Value;
	int Function;
	int CntrBits;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWidth)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWidth)
	afx_msg void OnMore();
	afx_msg void OnLess();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WIDTH_H__FBF0597D_64D4_4AAC_A45C_09DF99DA4CE5__INCLUDED_)
