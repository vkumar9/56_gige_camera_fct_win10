// Security.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Security.h"
#include "mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Security dialog


Security::Security(CWnd* pParent /*=NULL*/)
	: CDialog(Security::IDD, pParent)
{
	//{{AFX_DATA_INIT(Security)
	m_editpass = _T("");
	m_manpass = _T("");
	m_oppass = _T("");
	m_suppass = _T("");
	//}}AFX_DATA_INIT
}


void Security::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Security)
	DDX_Control(pDX, IDC_EDITPASS, m_editpass2);
	DDX_Text(pDX, IDC_EDITPASS, m_editpass);
	DDX_Text(pDX, IDC_MANPASS, m_manpass);
	DDX_Text(pDX, IDC_OPPASS, m_oppass);
	DDX_Text(pDX, IDC_SUPPASS, m_suppass);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Security, CDialog)
	//{{AFX_MSG_MAP(Security)
	ON_BN_CLICKED(IDC_B1, OnB1)
	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDC_B2, OnB2)
	ON_BN_CLICKED(IDC_B3, OnB3)
	ON_BN_CLICKED(IDC_B4, OnB4)
	ON_BN_CLICKED(IDC_B5, OnB5)
	ON_BN_CLICKED(IDC_B6, OnB6)
	ON_BN_CLICKED(IDC_B7, OnB7)
	ON_BN_CLICKED(IDC_B8, OnB8)
	ON_BN_CLICKED(IDC_B9, OnB9)
	ON_BN_CLICKED(IDC_RSETUP, OnRsetup)
	ON_BN_CLICKED(IDC_RLIMITS, OnRlimits)
	ON_BN_CLICKED(IDC_REJECT, OnReject)
	ON_BN_CLICKED(IDC_RMODIFY, OnRmodify)
	ON_BN_CLICKED(IDC_B0, OnB0)
	ON_BN_CLICKED(IDC_TECH, OnTech)
	ON_BN_CLICKED(IDC_RJOB, OnRjob)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHANGEOP, OnChangeop)
	ON_BN_CLICKED(IDC_CHANGESUP, OnChangesup)
	ON_BN_CLICKED(IDC_CHANGEMAN, OnChangeman)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Security message handlers

BOOL Security::OnInitDialog() 
{
	CDialog::OnInitDialog();
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psec=this;
	theapp=(CCapApp*)AfxGetApp();

	bypassTimer=false;
	PassWord="";
	PassWordX="";
	UpdateData(false);
	Safe=false;
	Safe1=false;
	Safe2=false;
	//if( theapp->SavedSetup==true ){CheckDlgButton(IDC_RSETUP,0); pframe->SSetup=true; }else{ CheckDlgButton(IDC_RSETUP,1); pframe->SSetup=false; }
	//if( theapp->SavedLimits==true ){CheckDlgButton(IDC_RLIMITS,0); pframe->SLimits=true; }else{ CheckDlgButton(IDC_RLIMITS,1); pframe->SLimits=false; }
	//if( theapp->SavedEject==true ){CheckDlgButton(IDC_REJECT,0); pframe->SEject=true; }else{ CheckDlgButton(IDC_REJECT,1); pframe->SEject=false; }
	//if( theapp->SavedModify==true ){CheckDlgButton(IDC_RMODIFY,0); pframe->SModify=true; }else{ CheckDlgButton(IDC_RMODIFY,1); pframe->SModify=false; }
	//if( theapp->SavedTech==true ){CheckDlgButton(IDC_TECH,0); pframe->STech=true; }else{ CheckDlgButton(IDC_TECH,1); pframe->STech=false; }
	//if( theapp->SavedJob==true ){CheckDlgButton(IDC_RJOB,0); pframe->SJob=true; }else{ CheckDlgButton(IDC_RJOB,1); pframe->SJob=false; }
	GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_OPPASS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MANPASS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHANGEMAN)->ShowWindow(SW_HIDE);
	CheckDlgButton(IDC_RMODIFY,1);
		CheckDlgButton(IDC_RJOB,1);
		CheckDlgButton(IDC_TECH,1);
		CheckDlgButton(IDC_REJECT,1);
		CheckDlgButton(IDC_RSETUP,1);
		CheckDlgButton(IDC_RLIMITS,1);
	CheckDlgButton(IDC_RLIMITS2,1);
	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Security::OnB0() 
{
	PassWord=PassWord+"0";					//Append number to end of password.
	m_editpass12.Format("%s",PassWord);		//Append number to end of change password.
	PassWordX=PassWordX+"*";				//Append "*" to end of text displaying number of characters.	
	m_editpass.Format("%s",PassWordX);		//Update display password string with additional "*".	
	UpdateData(false);
	
}

//Handler for 1 button press
void Security::OnB1() 
{
	PassWord=PassWord+"1";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
}

void Security::OnAccept() 
{

//	theapp=(CCapApp*)AfxGetApp();
	//pframe=(CMainFrame*)AfxGetMainWnd();
	
	if(PassWord==_T("60515") || PassWord==_T("114031") )	//Manufacturer password. //Changing from 60137
	{

		Safe=true;//NOT USED
		Safe1=true;//NOT USED
		Safe2=true;//NOT USED
		pframe->SaveCopyDisabled = false;

		//Show change/current passwords controls group box	
		GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_OPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_MANPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEMAN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);		//Hide enter button until new password is entered.

		CheckDlgButton(IDC_RMODIFY,0);		//Show supervisor abilities: modify, limits, setup, eject
		CheckDlgButton(IDC_RJOB,0);			//Show operator abilities: Job
		CheckDlgButton(IDC_TECH,0);			//Show Master abilities: Tech Access
		CheckDlgButton(IDC_REJECT,0);
		CheckDlgButton(IDC_RSETUP,0);
		CheckDlgButton(IDC_RLIMITS,0);
		CheckDlgButton(IDC_RLIMITS2,0);		//NOT USED

		//Set flags indicating which options are available for the user.
		theapp->SavedJob=pframe->SJob=true; 
		theapp->SavedLimits=pframe->SLimits=true;
		theapp->SavedModify=pframe->SModify=true;
		theapp->SavedSetup=pframe->SSetup=true; 
		theapp->SavedEject=pframe->SEject=true;
		theapp->SavedTech=pframe->STech=true;

		//Display passwords
		m_oppass.Format("%s",theapp->SavedOPPassWord);
		m_suppass.Format("%s",theapp->SavedSUPPassWord);
		m_manpass.Format("%s",theapp->SavedMANPassWord);
		m_editpass.Format("%s","Accepted");					//Display "Accepted" text in password edit box.

		if(PassWord==_T("114031"))
		{
			bypassTimer=true;
		}
	}	
	else if(PassWord==_T(theapp->SavedMANPassWord) )	//Master password.
	{
		CheckDlgButton(IDC_RJOB,0);			//Show operator abilities: Job
		CheckDlgButton(IDC_RMODIFY,0);		//Show supervisor abilities: modify, limits, setup, eject
		CheckDlgButton(IDC_RLIMITS,0);
		CheckDlgButton(IDC_REJECT,0);
		CheckDlgButton(IDC_RSETUP,0);
		CheckDlgButton(IDC_TECH,0);			//Show Master abilities: Tech Access
		CheckDlgButton(IDC_RLIMITS2,0);		//NOT USED
		pframe->SaveCopyDisabled = false;

		//Set flags indicating which options are available for the user.
		theapp->SavedJob=pframe->SJob=true;
		theapp->SavedLimits=pframe->SLimits=true;
		theapp->SavedModify=pframe->SModify=true;
		theapp->SavedSetup=pframe->SSetup=true; 
		theapp->SavedEject=pframe->SEject=true;
		theapp->SavedTech=pframe->STech=true;
		m_editpass.Format("%s","Accepted");


		//Show change/current passwords controls group box
		GetDlgItem(IDC_CHANGERECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CURRENTRECT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_OPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SUPPASS)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGEOP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHANGESUP)->ShowWindow(SW_SHOW);

		m_oppass.Format("%s",theapp->SavedOPPassWord);
		m_suppass.Format("%s",theapp->SavedSUPPassWord);
		//UpdateData(false);
	}
	else if(PassWord==_T(theapp->SavedSUPPassWord) )	//Supervisor password.
	{
		CheckDlgButton(IDC_RJOB,0);			//Show operator abilities: Job
		CheckDlgButton(IDC_RMODIFY,0);		//Show supervisor abilities: modify, limits, setup, eject
		CheckDlgButton(IDC_RLIMITS,0);
		CheckDlgButton(IDC_REJECT,0);
		CheckDlgButton(IDC_RSETUP,0);
		CheckDlgButton(IDC_RLIMITS2,0);		//NOT USED
		pframe->SaveCopyDisabled = false;
		
		//Set flags indicating which options are available for the user.
		theapp->SavedJob=pframe->SJob=true;
		theapp->SavedLimits=pframe->SLimits=true;
		theapp->SavedModify=pframe->SModify=true;
		theapp->SavedSetup=pframe->SSetup=true; 
		theapp->SavedEject=pframe->SEject=true;

		m_editpass.Format("%s","Accepted");
	}
	else if(PassWord==_T(theapp->SavedOPPassWord) )		//Operator password.
	{
		CheckDlgButton(IDC_RJOB,0);			//Show operator abilities: Job
		CheckDlgButton(IDC_RLIMITS2,0);		//NOT USED
		
		//Set flags indicating which options are available for the user.
		theapp->SavedJob=pframe->SJob=true;
		pframe->SaveCopyDisabled = true;
		
		m_editpass.Format("%s","Accepted");
	}
	else if(PassWord==_T("60085") )		//Secret menu password.
	{
		pframe->SSecretMenu=true;
	}
	else  //Entered password not accepted.
	{
		Safe=false;		//NOT USED
		Safe1=false;	//NOT USED
		Safe2=false;	//NOT USED
		m_editpass.Format("%s","Rejected");
		Beep(100,100);
	}
	
	m_editpass12="";	//Reset password text
	PassWord="";		//Reset password text
	PassWordX="";		//Reset password "*" text
	UpdateData(false);

}

//Handler for 2 button press
void Security::OnB2() 
{
	PassWord=PassWord+"2";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 3 button press
void Security::OnB3() 
{	
	PassWord=PassWord+"3";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 4 button press
void Security::OnB4() 
{
	PassWord=PassWord+"4";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 5 button press
void Security::OnB5() 
{
	PassWord=PassWord+"5";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 6 button press
void Security::OnB6() 
{
	PassWord=PassWord+"6";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 7 button press
void Security::OnB7() 
{
	PassWord=PassWord+"7";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 8 button press
void Security::OnB8() 
{
	PassWord=PassWord+"8";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//Handler for 9 button press
void Security::OnB9() 
{
	PassWord=PassWord+"9";
	m_editpass12.Format("%s",PassWord);
	PassWordX=PassWordX+"*";
	m_editpass.Format("%s",PassWordX);
	UpdateData(false);
	
}

//NOT USED
void Security::OnRsetup() 
{
		
}

//NOT USED
void Security::OnRlimits() 
{
	
}

//NOT USED
void Security::OnReject() 
{
	
}

//NOT USED
void Security::OnRmodify() 
{
	
}

//NOT USED
void Security::OnTech() 
{
	
}

//NOT USED
void Security::OnRjob() 
{
	
}

//NOT USED
void Security::OnTimer(UINT nIDEvent) 
{
		if(nIDEvent==1)
	{
		KillTimer(1);
	
	}
	
	CDialog::OnTimer(nIDEvent);
}

void Security::OnOK() 
{
	if(bypassTimer==false) pframe->SetTimer(13,600000,NULL);	//Reset security permissions after ten minutes	
	CDialog::OnOK();
}

//Change operator password 
void Security::OnChangeop() 
{
		//Last character must be a number
		if( (m_editpass12.Right(1)=="0")||
			(m_editpass12.Right(1)=="1")|| 
			(m_editpass12.Right(1)=="2")|| 
			(m_editpass12.Right(1)=="3")|| 
			(m_editpass12.Right(1)=="4")|| 
			(m_editpass12.Right(1)=="5")|| 
			(m_editpass12.Right(1)=="6")|| 
			(m_editpass12.Right(1)=="7")|| 
			(m_editpass12.Right(1)=="8")|| 
			(m_editpass12.Right(1)=="9") ) 
		{
			theapp->SavedOPPassWord=PassWord=m_editpass12;		//Save the new operator password.
			m_editpass.Format("%s","New password accepted!");	//Display message to use saying the new password has been accepted.
			
		}
		else	// If last charcter is not a number reject password.
		{
			m_editpass.Format("%s","Enter a valid password!");
			Beep(1000,100);
		}

		m_oppass.Format("%s",PassWord);		//Update displayed operator password.
		m_editpass12="";					//Reset change password
		PassWord="";						//Reset password
		PassWordX="";						//Reset password "*"
		UpdateData(false);					//Update gui.
}

//Change supervisor password
void Security::OnChangesup() 
{
	if( (m_editpass12.Right(1)=="0")||
			(m_editpass12.Right(1)=="1")|| 
			(m_editpass12.Right(1)=="2")|| 
			(m_editpass12.Right(1)=="3")|| 
			(m_editpass12.Right(1)=="4")|| 
			(m_editpass12.Right(1)=="5")|| 
			(m_editpass12.Right(1)=="6")|| 
			(m_editpass12.Right(1)=="7")|| 
			(m_editpass12.Right(1)=="8")|| 
			(m_editpass12.Right(1)=="9") ) 
		{
			theapp->SavedSUPPassWord=PassWord=m_editpass12;
			m_editpass.Format("%s","New password accepted!");
			
		}
		else
		{
			m_editpass.Format("%s","Enter a valid password!");
			Beep(1000,100);
		}
		m_suppass.Format("%s",PassWord);
		m_editpass12="";
		PassWord="";
		PassWordX="";
		UpdateData(false);
}

//Change supervisor manager
void Security::OnChangeman() 
{
	if( (m_editpass12.Right(1)=="0")||
			(m_editpass12.Right(1)=="1")|| 
			(m_editpass12.Right(1)=="2")|| 
			(m_editpass12.Right(1)=="3")|| 
			(m_editpass12.Right(1)=="4")|| 
			(m_editpass12.Right(1)=="5")|| 
			(m_editpass12.Right(1)=="6")|| 
			(m_editpass12.Right(1)=="7")|| 
			(m_editpass12.Right(1)=="8")|| 
			(m_editpass12.Right(1)=="9") ) 
		{
			theapp->SavedMANPassWord=PassWord=m_editpass12;
			m_editpass.Format("%s","New password accepted!");
		
		}
		else
		{
			m_editpass.Format("%s","Enter a valid password!");
			Beep(1000,100);
		}
	m_manpass.Format("%s",PassWord);
		m_editpass12="";
		PassWord="";
		PassWordX="";
		UpdateData(false);
	
}

