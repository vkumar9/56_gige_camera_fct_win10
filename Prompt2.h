#if !defined(AFX_PROMPT2_H__ED774FC9_3D30_48EA_A537_00A2478F93A9__INCLUDED_)
#define AFX_PROMPT2_H__ED774FC9_3D30_48EA_A537_00A2478F93A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Prompt2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Prompt2 dialog

class Prompt2 : public CDialog
{
// Construction
public:
	Prompt2(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Prompt2)
	enum { IDD = IDD_DIALOG2 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Prompt2)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Prompt2)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROMPT2_H__ED774FC9_3D30_48EA_A537_00A2478F93A9__INCLUDED_)
