// Advanced.cpp : implementation file
//

#include "stdafx.h"
#include "cap.h"
#include "Advanced.h"
#include "mainfrm.h"
#include "serial.h"



#include <stdio.h>
#include <string.h>
#include <memory.h>
//#include <windows.h>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define MAX_NUM_MEASUREMENTS 100
//#define GOIO_MAX_SIZE_DEVICE_NAME 100


/////////////////////////////////////////////////////////////////////////////
// Advanced dialog


Advanced::Advanced(CWnd* pParent /*=NULL*/)
	: CDialog(Advanced::IDD, pParent)
{
	//{{AFX_DATA_INIT(Advanced)
	m_editheight = _T("");
	m_edittemp = _T("");
	m_editcurrent = _T("");
	m_editconv = _T("");
	//}}AFX_DATA_INIT
}


void Advanced::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Advanced)
	DDX_Text(pDX, IDC_EDITHEIGHT, m_editheight);
	DDX_Text(pDX, IDC_EDITTEMP, m_edittemp);
	DDX_Text(pDX, IDC_EDITTEMP2, m_editcurrent);
	DDX_Text(pDX, IDC_ENCSIM, m_enable);
	DDX_Text(pDX, IDC_EDITCONV, m_editconv);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Advanced, CDialog)
	//{{AFX_MSG_MAP(Advanced)
	ON_BN_CLICKED(IDC_ENCSIM, OnEncsim)
	ON_BN_CLICKED(IDC_PLCPOS, OnPlcpos)
	ON_BN_CLICKED(IDC_MORE, OnMore)
	ON_BN_CLICKED(IDC_LESS, OnLess)
	ON_BN_CLICKED(IDC_MORE2, OnMore2)
	ON_BN_CLICKED(IDC_LESS2, OnLess2)
	ON_BN_CLICKED(IDC_THIGHER, OnThigher)
	ON_BN_CLICKED(IDC_TLOWER, OnTlower)
	ON_BN_CLICKED(IDC_OHIGHER, OnOhigher)
	ON_BN_CLICKED(IDC_OLOWER, OnOlower)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, OnOk)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Advanced message handlers

//Toggles between using a timer and the conveyor encoder output. 
void Advanced::OnEncsim() 
{

	pframe=(CMainFrame*)AfxGetMainWnd();
	if(theapp->SavedEncSim==true)	//If encoder is used
	{
		theapp->SavedEncSim=false;	//update variable to use timer.
		CheckDlgButton(IDC_ENCSIM,1);  //update gui toggle button.
		m_enable.Format("%s","A timer is used in place of the encoder");	//Update text on gui toggle button.
	
	
		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,190);
			pframe->m_pserial->SendChar(0,190);		//Send into to PLC to not use encoder for synchronization.
		}
		
	}
	else //Opposite of above.
	{
		theapp->SavedEncSim=true;
		CheckDlgButton(IDC_ENCSIM,0);
		m_enable.Format("%s","A Normal Encoder is Used");

		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,0,200);
			pframe->m_pserial->SendChar(0,200);
		}
		
	}
	UpdateData(false);	
	
}

BOOL Advanced::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_advanced=this;
	theapp=(CCapApp*)AfxGetApp();

//	pframe->KillTimer(43); //Kill the Read Temp Timer in Main Frame so it can be accessd from this dialog
	
	if(theapp->SavedEncSim==false)
	{
		CheckDlgButton(IDC_ENCSIM,1); 
		m_enable.Format("%s","A timer is used in place of the encoder");
	}
	else
	{
		CheckDlgButton(IDC_ENCSIM,0);
		m_enable.Format("%s","A Normal Encoder is Used");
	}

	


	motorHeight=200;
	m_editheight.Format("%3i",motorHeight);
	m_edittemp.Format("%3i",theapp->tempLimit);

	//current temp
	if(theapp->tempSelect==2)
	{
		localTemp = theapp->USB_Temperature;
	}
	else if(theapp->tempSelect==1)
	{
		localTemp = theapp->PLC_Temperature;
	}

    m_editcurrent.Format("%i",localTemp);
    SetTimer(2,2000,NULL);//Timer that updates current temp

	
	m_editconv.Format("%3i",theapp->convRatio);

//	SetTimer(2,900,NULL);//Starts the temp probe
	UpdateData(false);	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}




//--------------------------------------------------------
//These controls are used to adjust coordinate system for the motor which moves the 
//camera enclosure up and down.
//When the motor shaft is extended all the way out and the camera enclosure is at
//its top position the system is considered to be at position 0. When the motor is 
//retracted all the way the motor is considered to be at about 400.
//There are two ways in which the motor coordinate system can be adjusted:
//1.) Extend the motor all the way out and then set the position to 0.
//2.) Eyeball the current motor position with respect and enter the value.




//Program the PLC with the new top height limit. This will set the current motor shaft position to show as 0.
void Advanced::OnPlcpos() 
{
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,0,210);
		pframe->m_pserial->SendChar(0,210);
	}
	
}

//Increase the height number for the current camera enclosure motor position. Normal increment.
void Advanced::OnMore() 
{
	motorHeight+=1;
	m_editheight.Format("%3i",motorHeight);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,motorHeight,160);
		pframe->m_pserial->SendChar(motorHeight,160);
	}
	UpdateData(false);
}

//Decrease the height number for the current camera enclosure motor position. Normal increment.
void Advanced::OnLess() 
{
	motorHeight-=1;
	m_editheight.Format("%3i",motorHeight);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,motorHeight,160);
		pframe->m_pserial->SendChar(motorHeight,160);
	}
	UpdateData(false);
	
}

//Increase the height number for the current camera enclosure motor position. Large increment.
void Advanced::OnMore2() 
{
	motorHeight+=10;
	m_editheight.Format("%3i",motorHeight);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,motorHeight,160);
		pframe->m_pserial->SendChar(motorHeight,160);
	}

	UpdateData(false);
	
}

//Decrease the height number for the current camera enclosure motor position. Large increment.
void Advanced::OnLess2() 
{
	motorHeight-=10;
	m_editheight.Format("%3i",motorHeight);
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,motorHeight,160);
		pframe->m_pserial->SendChar(motorHeight,160);
	}
	UpdateData(false);
	
}


//--------------------------------------------------------

//Increase control cabinet temperature limit.
void Advanced::OnThigher() 
{
	theapp->tempLimit+=1;
	m_edittemp.Format("%3i",theapp->tempLimit);

	UpdateData(false);
	
}

//Decrease control cabinet temperature limit.
void Advanced::OnTlower() 
{
	theapp->tempLimit-=1;
	m_edittemp.Format("%3i",theapp->tempLimit);

	UpdateData(false);
	
}

//Increase the conveyor speed offset. Used to correct triggering issues when conveyor speed is increased or decreased.
void Advanced::OnOhigher() 
{
	//offset limit is 30.
	if(theapp->convRatio < 30) 	{theapp->convRatio+=1;}
	m_editconv.Format("%3i",theapp->convRatio);

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->convRatio,290);
		pframe->m_pserial->SendChar(theapp->convRatio,290);	//Update gui variable to show offset
	}
	UpdateData(false);	//Update gui.
	
}

//Decrease the conveyor speed offset. Used to correct triggering issues when conveyor speed is increased or decreased.
void Advanced::OnOlower() 
{
	if(theapp->convRatio >=1){	theapp->convRatio-=1; }
	m_editconv.Format("%3i",theapp->convRatio);
	
	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,theapp->convRatio,290);
		pframe->m_pserial->SendChar(theapp->convRatio,290);
	}

	UpdateData(false);
	
}

//Used to reset the timing windows for the PLC triggering.
void Advanced::OnButton2() 
{
	SetTimer(1,200,NULL);
	if(theapp->NoSerialComm==false) 
	{
		pframe->m_pserial->SendChar(0, 330);
	}

	UpdateData(false);
	
}

void Advanced::OnTimer(UINT nIDEvent) 
{
		if (nIDEvent==1) 
		{
		
			if(theapp->NoSerialComm==false) 
			{
				
//				pframe->m_pserial->SendChar(0, 330);
			}

			KillTimer(1);

		}

         if (nIDEvent==2)  //Updates current control cabinet temperature
         {
                
       

					if(theapp->tempSelect==2)	//Get temperature from USB sensor.
					{
					localTemp = theapp->USB_Temperature;
					}
					else if(theapp->tempSelect==1)	//Get temperature from PLC sensor.
					{
					localTemp = theapp->PLC_Temperature;
					}
         
                  //  localTemp = theapp->Temperature1;
                    m_editcurrent.Format("%i",localTemp);
         
                
                    UpdateData(false);         
         }


	
	CDialog::OnTimer(nIDEvent);
}


void Advanced::OnOk() 
{
	
	KillTimer(2);		//Stop checking temperature.

	pframe->SaveJob(pframe->CurrentJobNum);		//Save changes made to current job.

	CDialog::OnOK();

}

void Advanced::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnClose();
}
