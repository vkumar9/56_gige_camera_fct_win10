// splitter.cpp : implementation file
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "xview.h"
#include "splitter.h"
#include "xaxisview.h"
#include "yaxisview.h"
#include "mainfrm.h"

//#include "SideView.h"
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// C3WaySplitterFrame - just like CSplitterFrame except the input view is
//   the first pane, and there are two output views

//                             |  Text View (CTextView)
//    INPUT VIEW (CInputView)  |------------------------
//                             |  Color View (CColorView)

IMPLEMENT_DYNCREATE(C3WaySplitterFrame, CMDIChildWnd)

C3WaySplitterFrame::C3WaySplitterFrame()
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_psplit=this;
	
}

C3WaySplitterFrame::~C3WaySplitterFrame()
{
}

BOOL C3WaySplitterFrame::PreCreateWindow( CREATESTRUCT& cs )
{
	cs.cy=605;//535
	cs.cx=840;
	//cs.style &= ~WS_MAXIMIZEBOX; 
	cs.style = WS_DLGFRAME | WS_CHILD; 
	return CMDIChildWnd::PreCreateWindow(cs);
}


BOOL C3WaySplitterFrame::OnCreateClient(LPCREATESTRUCT lpcs,
	 CCreateContext* pContext)
{
	
	
	// create a splitter with 1 row, 2 columns
	m_wndSplitter.CreateStatic(this, 1, 2);
		// add the first splitter pane - the default view in column 0
	m_wndSplitter.CreateView(0, 0, pContext->m_pNewViewClass, CSize(184, 300), pContext);
		// add the second splitter pane - which is a nested splitter with 2 rows
	m_wndSplitter2.CreateStatic(
			&m_wndSplitter,     // our parent window is the first splitter
			2, 1,               // the new splitter is 2 rows, 1 column
			WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter.IdFromRowCol(0, 1));

				
				// now create the two views inside the nested splitter

	//m_wndSplitter3.CreateStatic(&m_wndSplitter2, 2, 1,WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter2.IdFromRowCol(0, 0));
	m_wndSplitter2.CreateView(1, 0, RUNTIME_CLASS(CYAxisView), CSize(600, 100), pContext);	

	m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CXAxisView), CSize(512, 375), pContext);	
	//m_wndSplitter3.CreateView(0, 1, RUNTIME_CLASS(SideView), CSize(88, 370), pContext);
	
	return TRUE;
}

BEGIN_MESSAGE_MAP(C3WaySplitterFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(C3WaySplitterFrame)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//

IMPLEMENT_DYNAMIC(CViewExSplitWnd, CSplitterWnd)

CViewExSplitWnd::CViewExSplitWnd()
{
}

CViewExSplitWnd::~CViewExSplitWnd()
{
}

CWnd* CViewExSplitWnd::GetActivePane(int* pRow, int* pCol)
{
	ASSERT_VALID(this);

	// attempt to use active view of frame window
	CWnd* pView = NULL;
	CFrameWnd* pFrameWnd = GetParentFrame();
	ASSERT_VALID(pFrameWnd);
	pView = pFrameWnd->GetActiveView();

	// failing that, use the current focus
	if (pView == NULL)
		pView = GetFocus();

	return pView;
}
