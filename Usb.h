#if !defined(AFX_USB_H__E5313B2B_3137_4E7F_99D4_715C72596699__INCLUDED_)
#define AFX_USB_H__E5313B2B_3137_4E7F_99D4_715C72596699__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Usb.h : header file
//
//#include "GoIO_DLL_interface.h"


/////////////////////////////////////////////////////////////////////////////
// Usb dialog

class Usb : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:
	Usb(CWnd* pParent = NULL);   // standard constructor
	
	char buf[10];

	CString m_probe;
	CString MessageToDisplay;
	int LensTrigger4;

	CCapApp *theapp;
	CMainFrame* pframe;

	int LensTrigger3;
	int LensTrigger2;
	int LensTrigger;

	DWORD								ActualBytesRead;
	DWORD								BytesRead;
	DWORD								cbBytesRead;
	bool								MyDeviceDetected; 
	CString								MyDevicePathName;
	DWORD								NumberOfBytesRead;
	char								OutputReport[256];
	DWORD								dwError;
	char								FeatureReport[256];
	unsigned char						InputReport[256];
	ULONG								Required;
	CString								ValueToDisplay;
	ULONG								Length;

	
	void OnHide(); 

// Dialog Data
	//{{AFX_DATA(Usb)
	enum { IDD = IDD_USB };
	CEdit	m_VendorID;
	CEdit	m_ProductID;
	CListBox	m_BytesReceived;
	CListBox	m_ResultsList;
	CButton	m_Once;
	CString	m_ResultsString;
	CString	m_strBytesReceived;
	CString	m_ProductIDString;
	CString	m_VendorIDString;
	CString	m_probe1;
	CString	m_probe2;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Usb)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Usb)
	virtual BOOL OnInitDialog();
	afx_msg void OnOnce();
	afx_msg void OnChangeResults();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void On_cmdFindMyDevice();
	afx_msg void OnChange_txtVendorID();
	afx_msg void OnChange_txtProductID();
	afx_msg void OnRb2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


	BOOL DeviceNameMatch(LPARAM lParam);
	bool FindTheHID();
	LRESULT Main_OnDeviceChange(WPARAM wParam, LPARAM lParam);
	void CloseHandles();
	void DisplayCurrentTime();
	void DisplayData(CString cstrDataToDisplay);
	void DisplayFeatureReport();
	void DisplayInputReport();
	void DisplayLastError(CString Operation);
	void DisplayReceivedData(USHORT seqNum, unsigned char ReceivedByte);
	void GetDeviceCapabilities();
	void PrepareForOverlappedTransfer();
	void ScrollToBottomOfListBox(USHORT idx);
	void ReadAndWriteToDevice();
	void ReadFeatureReport();
	void ReadInputReport();
	void RegisterForDeviceNotifications();
	void WriteFeatureReport();
	void WriteOutputReport();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USB_H__E5313B2B_3137_4E7F_99D4_715C72596699__INCLUDED_)
