// BottleDown.cpp : implementation file
//

#include "stdafx.h"
#include "Cap.h"
#include "BottleDown.h"
#include "Mainfrm.h"
#include "serial.h"
#include "capview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BottleDown dialog


BottleDown::BottleDown(CWnd* pParent /*=NULL*/)
	: CDialog(BottleDown::IDD, pParent)
{
	//{{AFX_DATA_INIT(BottleDown)
	m_dbedit = _T("");
	//}}AFX_DATA_INIT
}


void BottleDown::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BottleDown)
		DDX_Text(pDX, IDC_BOTTLEDOWN, m_enable);
	DDX_Text(pDX, IDC_DBEDIT, m_dbedit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BottleDown, CDialog)
	//{{AFX_MSG_MAP(BottleDown)
	ON_BN_CLICKED(IDC_BOTTLEDOWN, OnBotdn)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_DBMORE, OnDbmore)
	ON_BN_CLICKED(IDC_DBLESS, OnDbless)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BottleDown message handlers

BOOL BottleDown::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	pframe=(CMainFrame*)AfxGetMainWnd();
	pframe->m_pbotdn=this;
	theapp=(CCapApp*)AfxGetApp();

	//Update gui to indicated bottle down sensor on/off based on saved application value.
	if(theapp->SavedBottleSensor==0)
	{
		CheckDlgButton(IDC_BOTTLEDOWN,0); 
		m_enable.Format("%s","Sensor is disabled");
		
	}
	else
	{
		CheckDlgButton(IDC_BOTTLEDOWN,1);
		m_enable.Format("%s","Sensor is enabled");
		
	}
	UpdateData(false);
	Value=0;
	Function=0;
	m_dbedit.Format("%3i",theapp->SavedDBOffset);	//Use saved value to update gui variable which holds the ejector encoder count delay.
	
	UpdateData(false);	//Update gui.
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Toggles whether bottle down sensor will be on or off.
void BottleDown::OnBotdn() 
{
	pframe=(CMainFrame*)AfxGetMainWnd();
	if(theapp->SavedBottleSensor==0)		//If bottle down sensor is enabled
	{
		CheckDlgButton(IDC_BOTTLEDOWN,1); 	//Update gui button to show that down sensor is disabled.
		m_enable.Format("%s","Sensor is enabled");	//Update gui text variable to say that bottle down sensor is disabled.
		theapp->SavedBottleSensor=1;	//Update app variable to indicate that bottle down sensor is disabled.
	
		Value=0;
		Function=170;

		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
			pframe->m_pserial->SendChar(Value,Function);
		}
	}
	else //Opposite of above.
	{
		CheckDlgButton(IDC_BOTTLEDOWN,0);
		m_enable.Format("%s","Sensor is disabled");
		theapp->SavedBottleSensor=0;

		Value=0;
		Function=180;

		if(theapp->NoSerialComm==false) 
		{
			//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
			pframe->m_pserial->SendChar(Value,Function);
		}
	}
	UpdateData(false);
	
}

//Does not appear to be used.
void BottleDown::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		KillTimer(1);

		Value=0;
		Function=0;
		CntrBits=0;

		if(theapp->NoSerialComm==false) 
		{
		pframe->m_pserial->ControlChars=Value+Function+CntrBits;
		}
		
	}
	
	CDialog::OnTimer(nIDEvent);
}

//Increase bottle down ejector encoder count.
void BottleDown::OnDbmore() 
{
	Value=theapp->SavedDBOffset+=2;	//increase value to be sent to the PLC

	//if(theapp->SavedDBOffset<=0)theapp->SavedDBOffset=0;
	m_dbedit.Format("%3i",theapp->SavedDBOffset);	//Update gui edit box with new encoder count.

	Value=theapp->SavedDBOffset;
	Function=610;

	//pframe->m_pserial->QueMessage(Value,Function);
	if(theapp->NoSerialComm==false)  //If serial port is connected.
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}

	UpdateData(false);
	
}

//decrease bottle down ejector encoder count.
void BottleDown::OnDbless() 
{
	Value=theapp->SavedDBOffset-=2;

	//if(theapp->SavedDBOffset<=0)theapp->SavedDBOffset=0;
	m_dbedit.Format("%3i",theapp->SavedDBOffset);

	Value=theapp->SavedDBOffset;
	Function=610;

	if(theapp->NoSerialComm==false) 
	{
		//pframe->m_pserial->SendMessage(WM_QUE,Value,Function);
		pframe->m_pserial->SendChar(Value,Function);
	}
	
	UpdateData(false);
	
}
