#if !defined(AFX_CAMERA_H__959BAD3F_CD37_460F_A2FE_6411CB179F87__INCLUDED_)
#define AFX_CAMERA_H__959BAD3F_CD37_460F_A2FE_6411CB179F87__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Camera.h : header file
//
#include <FlyCapture2.h>
#include "GigECamera.h"
const int EXPECTED_CAMS = 3;
#include <thread>
#include <ppl.h>
#include <fstream>

/////////////////////////////////////////////////////////////////////////////
// CCamera dialog


class CCamera : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:


	CMainFrame* pframe;
	CCapApp* theapp = NULL;
	void OnGmore();
	void OnGmore2();
	void OnGmore3();
	void OnGless();
	void OnGless2();
	void OnGless3();
	double gain[EXPECTED_CAMS];			//Gain for camera
	double shutterSpeed[EXPECTED_CAMS];	//Shutter speed for camera
	double frameRate;		//Frame rate for cameras 1, 2, 3
	void ModifyCams();

	std::ofstream filestream;
	std::atomic<int> m_bContinueGrabThread;		//Flag used for stopping running camera grab thread.
	std::atomic<int> m_bRestart;

	std::atomic<int> cam1Height;				//Variable for holding camera 1 height.
	std::atomic<int> cam1Width;				//Variable for holding camera 1 width.
	std::atomic<int> cam1Top;				//Variable for holding camera 1 offset from top edge.
	std::atomic<int> cam1Left;				//Variable for holding camera 1 offset from left edge.

	std::atomic<int> cam2Height;				//Variable for holding camera 2 height.
	std::atomic<int> cam2Width;				//Variable for holding camera 2 width.
	std::atomic<int> cam2Top;				//Variable for holding camera 2 offset from top edge.
	std::atomic<int> cam2Left;				//Variable for holding camera 2 offset from left edge

	std::atomic<int> cam3Height;				//Variable for holding camera 3 height.
	std::atomic<int> cam3Width;				//Variable for holding camera 3 width.
	std::atomic<int> cam3Top;				//Variable for holding camera 3 offset from top edge.
	std::atomic<int> cam3Left;				//Variable for holding camera 3 offset from left edge.


	int StartCams();
	CCamera(CWnd* pParent = NULL);   // standard constructor
// Processed image for displaying and saving
	FlyCapture2::Image   m_imageProcessed1;
	FlyCapture2::Image   m_imageProcessed2;
	FlyCapture2::Image   m_imageProcessed3;
	FlyCapture2::Image*   m_imageProcessed[EXPECTED_CAMS][2];
	std::atomic<unsigned char> readNext[EXPECTED_CAMS];
	std::atomic<unsigned char> writeNext[EXPECTED_CAMS];
	HANDLE signal1;
	HANDLE signal2;
	HANDLE signal3;
	HANDLE dispSignal[3];

	bool updateCamsInProgress = false;
	FlyCapture2::IPAddress ipaddresses[EXPECTED_CAMS];
	FlyCapture2::BusManager* busMgr;

	std::thread* grabThread1;
	std::thread* grabThread2;
	std::thread* grabThread3;
	std::thread* inspectThread1;
	std::thread* inspectThread2;
	std::thread* inspectThread3;
		
	std::mutex imageSettings;

	HANDLE handles[3];

double spanElapsed;
int whitebalincrement;
// Event indicating that the thread is about to exit.

	FlyCapture2::GigECamera cam_e1;	
	FlyCapture2::GigECamera cam_e2;
	FlyCapture2::GigECamera cam_e3;
	FlyCapture2::GigECamera* cam_e[EXPECTED_CAMS];

void StopCams();
// The image grab thread.
   static UINT threadGrabImage( void* pparam );
	static UINT threadGrabImage2( void* pparam );
static UINT threadGrabImage3( void* pparam );
static UINT threadInspect1(void* pparam);
static UINT threadInspect2(void* pparam);
static UINT threadInspect3(void* pparam);

// The object grab image loop.  Only executed from within the grab thread.
   UINT doGrabLoop();
	UINT doGrabLoop2();
	UINT doGrabLoop3();

void initBitmapStruct( int iCols, int iRows );
void resizeProcessedImage( int rows, int cols, FlyCapture2::Image* im,  int& bufferSize);
// Structure used to draw to the screen.
   BITMAPINFO        m_bitmapInfo;  
// Current size of the processed image buffer
   int m_iProcessedBufferSize[EXPECTED_CAMS];

//CameraGUIContext m_guicontext;
// Dialog Data
	//{{AFX_DATA(CCamera)
	enum { IDD = IDD_CAMERA };
	CString	m_pos1;				//GUI variable for displaying camera 1 offset from top edge.
	CString	m_pos2;				//GUI variable for displaying camera 2 offset from top edge.
	CString	m_cam1shutter;		//GUI variable for displaying camera 1 shutter speed.
	CString	m_cam2shutter;		//GUI variable for displaying camera 2 shutter speed.
	CString	m_cam3shutter;		//GUI variable for displaying camera 3 shutter speed.
	CString	m_pos3;				//GUI variable for displaying camera 3 offset from top edge.
	CString	m_bandwidth1;		//GUI variable for displaying bus 1 bandwidth.
	CString	m_bandwidth2;		//GUI variable for displaying bus 2 bandwidth.
	CString	m_whitebalance1Red;	//GUI variable for displaying camera 1 whitebalance.
	CString	m_whitebalance2Red;	//GUI variable for displaying camera 2 whitebalance.
	CString	m_whitebalance3Red;	//GUI variable for displaying camera 3 whitebalance.
	CString	m_whitebalance1Blue;//GUI variable for displaying camera 1 whitebalance.
	CString	m_whitebalance2Blue;//GUI variable for displaying camera 2 whitebalance.
	CString	m_whitebalance3Blue;//GUI variable for displaying camera 3 whitebalance.

	CSliderCtrl	m_redSlider;		//Slider control interface for red component.
	CSliderCtrl	m_blueSlider;		//Slider control interface for blue component.
	
	//}}AFX_DATA
	//static void busCallback( void* pparam, int iMessage, unsigned long ulParam );
	static void busCallback( void* pparam, int serial);
	static void busRemovalCallback( void* pparam, int serial);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCamera)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

	enum LoopMode
   {
      // None, the grab thread doesn't exist. The camera is stopped.
      NONE,
      // Lock latest.  The normal flycap mode.
      FREE_RUNNING,
      // Record mode.  Lock next.  Every image is displayed.
      RECORD,
      // Storing images for .avi saving.
      RECORD_STORING,
      // Selected to record streaming images, waiting for user to begin.
      RECORD_PRE_STREAMING,
      // Record streaming images.  Lock next.
      RECORD_STREAMING,
   };

	LoopMode m_loopmode;		//NOT USED? Selected image acquisition mode.
protected:
	FlyCapture2::Error CameraSetup(int index, FlyCapture2::BusManager* busMgr, int& camOK);
	FlyCapture2::Error doContinousGrab(FlyCapture2::GigECamera* cam, bool &gotPic, std::atomic<int>& count, bool cameraDone[], int& bufferSize, int index);

public:
	FlyCapture2::Error SetTriggerMode(FlyCapture2::GigECamera* cam, bool value);
	FlyCapture2::Error SetCameraPropertyAuto(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, bool autoOn);
	FlyCapture2::Error SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value);
	FlyCapture2::Error SetCameraPropertyNoAbs(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, float value);
	FlyCapture2::Error SetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, unsigned int valueA, unsigned int valueB);
	FlyCapture2::Error GetCameraProperty(FlyCapture2::GigECamera* cam, FlyCapture2::PropertyType type, int &valueA, int &valueB);
	FlyCapture2::Error SetImageSettings(int index);
	void CCamera::doInspect1();
	void CCamera::doInspect2();
	void CCamera::doInspect3();

	void UpdateDisplay();
	void UpdateValues();
	bool LockOut;				//NOT USED
	bool LockOut2;				//Used when reinitializing cameras
	int ReStartCams();
	int initTry;				//Used when looping through camera initialization attempts
	int cam;					//Used in the connecting to camera status dialog box to show which camera is being connected to.
	bool gotThree;				//NOT USED
	bool gotTwo;				//NOT USED
	bool gotOne;				//NOT USED
	bool lockQuery;				//NOT USED
	void SetLightLevel(double cam1Gain, double cam2Gain, double cam3Gain);
	// Generated message map functions
	//{{AFX_MSG(CCamera)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnMoveup1();
	afx_msg void OnMovedn1();
	afx_msg void OnMoveup2();
	afx_msg void OnTrigger();
	afx_msg void OnDown23();
	afx_msg void OnCam1ExpF();
	afx_msg void OnCam1ExpS();
	afx_msg void OnCam2ExpF();
	afx_msg void OnCam2ExpS();
	afx_msg void OnCam3ExpF();
	afx_msg void OnCam3ExpS();
	afx_msg void OnMoveup3();
	afx_msg void OnDown3();
	afx_msg void OnBandwidthup1();
	afx_msg void OnBandwidthdw1();
	afx_msg void OnBandwidthup2();
	afx_msg void OnBandwidthdw2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


class CWhiteBalance : public CDialog
{
// Construction
public:

	CMainFrame* pframe;
	CCapApp* theapp;
	
	CWhiteBalance(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CCamera)
	enum { IDD = IDD_WHTBAL };
	enum { NoCamSelected = 0, Cam1Selected = 1, Cam2Selected = 2, Cam3Selected = 4 };

	char SelectedCamera;
	char LinkedCameras;

	CSliderCtrl	m_redSlider;		//Slider control interface for red component.
	CSliderCtrl	m_blueSlider;		//Slider control interface for blue component.
	CButton m_auto;					//Button for Auto WB

	int   iPolarity;
	int   iSource;
	int   iRawValue;
	int   iMode;
	FlyCapture2::Error error;
	bool mOnePush,mOnOff,mAuto;
	
	bool m_whitebalanceAuto = false;
	int m_whitebalanceRed;	//GUI variable for displaying camera 1 whitebalance.
	int m_whitebalanceBlue;	//GUI variable for displaying camera 2 whitebalance.
	CString m_whitebalanceRedText;
	CString m_whitebalanceBlueText;
	CString m_sCam;
	CString m_sLinkCam1;
	CString m_sLinkCam2;
	int whtbalinc;
	//}}AFX_DATA

	void DoAutoWB(int cameraToUpdate);
	void SetAutoOff(int index);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCamera)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
//protected:
public:
	// Generated message map functions
	//{{AFX_MSG(CCamera)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void Cam1RadioSelect();
	afx_msg void Cam2RadioSelect();
	afx_msg void Cam3RadioSelect();
	afx_msg void ClearLinks();
	afx_msg void LoadCamera1Values();
	afx_msg void LoadCamera2Values();
	afx_msg void LoadCamera3Values();
	afx_msg void RefreshWB();
	afx_msg void OnBlueDown();
	afx_msg void OnBlueUp();
	afx_msg void OnRedDown();
	afx_msg void OnRedUp();
	afx_msg void OnLinkCam1();
	afx_msg void OnUpdateLinked();
	afx_msg void OnAutoWB();
	afx_msg void OnLinkCam2();
	afx_msg void UpdateWBParameters(int wbred, int wbblue);
	afx_msg void UpdateWhiteBalanceRed(int whitebalanceRed);
	afx_msg void UpdateWhiteBalanceBlue(int whitebalanceBlue);
	afx_msg void OnReleasedcaptureBlueSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureRedSlider(NMHDR* pNMHDR, LRESULT* pResult);

	bool IsCamera1Selected();
	bool IsCamera2Selected();
	bool IsCamera3Selected();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAMERA_H__959BAD3F_CD37_460F_A2FE_6411CB179F87__INCLUDED_)
