#if !defined(AFX_FILLER_H__194DC651_A1DF_4A38_A5D8_9A8D8F556F97__INCLUDED_)
#define AFX_FILLER_H__194DC651_A1DF_4A38_A5D8_9A8D8F556F97__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Filler.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Filler dialog

class Filler : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp; 
friend class FSetup;
public:
	bool nineteenTwentyfour;	//NOT USED
	bool thirteenEighteen;		//NOT USED
	bool sevenTwelve;			//NOT USED
	bool oneSix;				//NOT USED
	bool allFill;			//Flag indicating that bottles from all filler heads should be ejected.
	bool allCap;			//Flag indicating that bottles from all capper heads should be ejected.
	bool lockOut;			//NOT USED
	
	int z;
	bool shiftOne;			//NOT USED
	int oldPos;				//NOT USED
	void UpdateDisplay();
	int Function;			//For PLC communication
	int Value;				//For PLC communication
	Filler(CWnd* pParent = NULL);   // standard constructor
	
	FSetup* m_pfsetup;		//Reference to filler/capper setup dialog box.

	CWnd* pParent2;			//Reference to parent window. Needed because this is a modeless dialog box.
// Dialog Data
	//{{AFX_DATA(Filler)
	enum { IDD = IDD_FILLER };
	CButton	m_filldnc;		//NOT USED
	CButton	m_filldn2c;		//NOT USED
	CButton	m_fillup2c;		//NOT USED
	CButton	m_fillupc;		//NOT USED
	CString	m_position;
	CString	m_duration;
	CString	m_cappos;		//For displaying the index of the capper head from which bottles are to be ejected.
	CString	m_capnum;		//For displaying the number of bottles to be ejected from the selected capper head.
	CString	m_filpos;		//For displaying the index of the filler head from which bottles are to be ejected.
	CString	m_filnum;		//For displaying the number of bottles to be ejected from the selected filler head.
	CString	m_s10;			//Capper (c) and filler (s) labels for bottles used to display which head was used for which bottle.
	CString	m_s2;
	CString	m_s3;
	CString	m_s4;
	CString	m_s5;
	CString	m_s6;
	CString	m_s7;
	CString	m_s8;
	CString	m_s9;
	CString	m_c1;
	CString	m_c10;
	CString	m_c2;
	CString	m_c3;
	CString	m_c4;
	CString	m_c5;
	CString	m_c6;
	CString	m_c7;
	CString	m_c8;
	CString	m_c9;
	CString	m_s1;
	CString	m_c11;
	CString	m_c12;
	CString	m_c13;
	CString	m_c14;
	CString	m_c15;
	CString	m_c16;
	CString	m_c17;
	CString	m_c18;
	CString	m_c19;
	CString	m_c20;
	CString	m_c21;
	CString	m_c22;
	CString	m_c23;
	CString	m_c24;
	CString	m_c25;
	CString	m_c26;
	CString	m_c27;
	CString	m_c28;
	CString	m_c29;
	CString	m_c30;
	CString	m_c31;
	CString	m_c32;
	CString	m_c33;
	CString	m_s11;
	CString	m_s12;
	CString	m_s13;
	CString	m_s14;
	CString	m_s15;
	CString	m_s16;
	CString	m_s17;
	CString	m_s18;
	CString	m_s19;
	CString	m_s20;
	CString	m_s21;
	CString	m_s22;
	CString	m_s24;
	CString	m_s23;
	CString	m_s25;
	CString	m_s26;
	CString	m_s27;
	CString	m_s28;
	CString	m_s29;
	CString	m_s30;
	CString	m_s31;
	CString	m_s32;
	CString	m_s33;
	//}}AFX_DATA

	CCapApp *theapp;
	CMainFrame* pframe;
CPoint bottles[200];	//Array which stores the numbers of the filler (x) and capper (y) heads used for a bottle and which bottles are to be ejected.
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Filler)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Filler)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSetup();
	afx_msg void OnCapup();
	afx_msg void OnCapdn();
	afx_msg void OnCapup2();
	afx_msg void OnCapdn2();
	afx_msg void OnFilup();
	afx_msg void OnFildn();
	afx_msg void OnFilup2();
	afx_msg void OnFildn2();
	afx_msg void OnAllheads();
	afx_msg void OnStart();
	afx_msg void OnClear();
	afx_msg void OnCapupfast();
	afx_msg void OnCapdnfast();
	afx_msg void OnFilupfast();
	afx_msg void OnFildnfast();
	afx_msg void OnAllheads2();
	afx_msg void On1thru6();
	afx_msg void On7thru12();
	afx_msg void On13thru18();
	afx_msg void On19thru24();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// FSetup dialog

class FSetup : public CDialog
{
// Construction
public:
	FSetup(CWnd* pParent = NULL);   // standard constructor

	CCapApp *theapp;
	CMainFrame* pframe;

	int Value;			//For PLC communication
	int Function;		//For PLC communication
CString m_ejenable;
// Dialog Data
	//{{AFX_DATA(FSetup)
	enum { IDD = IDD_FILLERSET };
	CString	m_pocketnum;			//GUI variable which holds the user specified conveyor length.
	CString	m_cappersize;			//GUI variable which holds the number of heads on the capper.
	CString	m_fillersize;			//GUI variable which holds the number of heads on the filler
	CString	m_editcapoffset;		//GUI variable which holds the capper head offset.
	CString	m_editfilloffset;		//GUI variable which holds the filler head offset.
	CString	m_pocketquant;			//GUI variable which holds the number of bottles present on the conveyor between the filler/capper and camera tunnel.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FSetup)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FSetup)
	virtual BOOL OnInitDialog();
	afx_msg void OnBup();
	afx_msg void OnBdn();
	afx_msg void OnBejposlonger();
	afx_msg void OnBejposshorter();
	afx_msg void OnBdndur();
	afx_msg void OnBupdur();
	afx_msg void OnBdn2();
	afx_msg void OnBup2();
	afx_msg void OnEjenable();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBmore();
	afx_msg void OnBless();
	virtual void OnOK();
	afx_msg void OnFillnummore();
	afx_msg void OnFillnumless();
	afx_msg void OnCappermore();
	afx_msg void OnCapperless();
	afx_msg void OnFoffsetmore();
	afx_msg void OnFoffsetless();
	afx_msg void OnCapoffsetmore();
	afx_msg void OnCapoffsetless();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLER_H__194DC651_A1DF_4A38_A5D8_9A8D8F556F97__INCLUDED_)
