#if !defined(AFX_SECURITY_H__EBA93489_7E32_461D_9F81_B9A1E6D80778__INCLUDED_)
#define AFX_SECURITY_H__EBA93489_7E32_461D_9F81_B9A1E6D80778__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Security.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Security dialog

class Security : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp;
public:
	bool bypassTimer;		//Flag indicating if security privileges given for the entered password will time out.
	bool Safe;				//NOT USED
	bool Safe1;				//NOT USED
	bool Safe2;				//NOT USED
	CString PassWord;		//Variable for storing entered password.
	CString PassWordX;		//Variable for storing "*" corresponding to the number of characters entered into the password.
	Security(CWnd* pParent = NULL);   // standard constructor
	CString	m_editpass12;	//Variable for storing new password when password change is requested.
// Dialog Data
	//{{AFX_DATA(Security)
	enum { IDD = IDD_SECURITY };
	CEdit	m_editpass2;	//NOT USED
	CString	m_editpass;		//Variable for displaying entered password text in the password text box. Show * equal to the number of characters entered.
	CString	m_manpass;		//Variable for displaying saved manager password
	CString	m_oppass;		//Variable for displaying saved operator password
	CString	m_suppass;		//Variable for displaying saved supervisor password
	CString	m_master;		//NOT USED
	//}}AFX_DATA

	CMainFrame* pframe;
	CCapApp *theapp;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Security)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Security)
	virtual BOOL OnInitDialog();
	afx_msg void OnB1();
	afx_msg void OnAccept();
	afx_msg void OnB2();
	afx_msg void OnB3();
	afx_msg void OnB4();
	afx_msg void OnB5();
	afx_msg void OnB6();
	afx_msg void OnB7();
	afx_msg void OnB8();
	afx_msg void OnB9();
	afx_msg void OnRsetup();
	afx_msg void OnRlimits();
	afx_msg void OnReject();
	afx_msg void OnRmodify();
	afx_msg void OnB0();
	afx_msg void OnTech();
	afx_msg void OnRjob();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnChangeop();
	afx_msg void OnChangesup();
	afx_msg void OnChangeman();
	afx_msg void OnChangemaster();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SECURITY_H__EBA93489_7E32_461D_9F81_B9A1E6D80778__INCLUDED_)
