#if !defined(AFX_JOB_H__AA5A9589_E81E_493B_9F9C_991DE651A4B4__INCLUDED_)
#define AFX_JOB_H__AA5A9589_E81E_493B_9F9C_991DE651A4B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Job.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// Job dialog

class Job : public CDialog
{
// Construction
friend class CMainFrame;
friend class CCapApp; 
friend class KBoard; 
public:
	int source_job_index;
	int dest_job_index;

public:
	void DoEnables();	//Not used
	bool SavAs;					//Flag indicating that the duplicate dialog box was opened.
	int original;				//Used when a job is duplicated. Stores the index of the job from which the duplicate was made.
	bool DoOver;				//Flag indicating that the currently selected job is a duplicate.
	int JobOver;				//Index of newly duplicated job.
	CString Value;				//Variable used to store characters and number entered to the rename edit box.
	bool NameChanged;			//NOT USED
	void LoadJobs();
	Job(CWnd* pParent = NULL);   // standard constructor
	char newjobname[40];		//Used to store duplicate jobs name.
	void SaveOld();		//Not used
	void CopyFile(int Old,int New);
	int nIndex;					//Index of currently selected job.
	CString jname;				//NOT USED
	CMainFrame* pframe;
	CCapApp *theapp;
	KBoard* pkboard;			//NOT USED

// Dialog Data
	//{{AFX_DATA(Job)
	enum { IDD = IDD_JOB };
	CEdit	m_rename;			//Control used to interface with the rename edit box.
	CButton	m_changename;		//NOT USED
	CListBox	m_joblist;		//Control used to interface with the jobs list box.
	CString	m_renamejob;		//Variable used to store characters and number entered to the rename edit box.
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Job)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateGUIString(CString c);

	//CString jobname;
	CString str;				//Stores a jobs name after a new job is selected. Used to load the rename box with the newly selected job's name.
	
	//NOT USED
	CString Job1;
	CString Job2;
	CString Job3;
	CString Job4;
	CString Job5;
	CString Job6;
	CString Job7;
	CString Job8;
	CString Job9;
	CString Job10;
	CString Job11;
	CString Job12;
	CString Job13;
	CString Job14;
	CString Job15;
	CString Job16;
	CString Job17;
	CString Job18;
	CString Job19;
	CString Job20;
	
	// Generated message map functions
	//{{AFX_MSG(Job)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeJobs();
	afx_msg void OnChangename();
	afx_msg void OnA();
	afx_msg void OnB();
	afx_msg void OnC();
	afx_msg void OnD();
	afx_msg void OnE();
	afx_msg void OnF();
	afx_msg void OnH();
	afx_msg void OnG();
	afx_msg void OnI();
	afx_msg void OnJ();
	afx_msg void OnK();
	afx_msg void OnL();
	afx_msg void OnM();
	afx_msg void OnN();
	afx_msg void OnO();
	afx_msg void OnP();
	afx_msg void OnQ();
	afx_msg void OnR();
	afx_msg void OnS();
	afx_msg void OnT();
	afx_msg void OnU();
	afx_msg void OnV();
	afx_msg void OnW();
	afx_msg void OnX();
	afx_msg void OnY();
	afx_msg void OnZ();
	afx_msg void OnDel();
	afx_msg void OnDash();
	virtual void OnCancel();
	afx_msg void OnJ0();
	afx_msg void OnJ1();
	afx_msg void OnJ2();
	afx_msg void OnJ3();
	afx_msg void OnJ4();
	afx_msg void OnJ5();
	afx_msg void OnJ6();
	afx_msg void OnJ7();
	afx_msg void OnJ8();
	afx_msg void OnJ9();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDatabase();
	afx_msg void OnSaveas();
	afx_msg void OnDeleteJob();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JOB_H__AA5A9589_E81E_493B_9F9C_991DE651A4B4__INCLUDED_)
