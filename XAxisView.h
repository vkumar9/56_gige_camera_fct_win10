#if !defined(AFX_CXAXISVIEW_H__FABF46A7_94FC_44B1_A39A_F7F5753FF25B__INCLUDED_)
#define AFX_CXAXISVIEW_H__FABF46A7_94FC_44B1_A39A_F7F5753FF25B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XAxisView.h : header file
//
#include "VisCore.h"
#include <FlyCapture2.h>
//#include "pcrcam.h"

/////////////////////////////////////////////////////////////////////////////
// CXAxisView view

typedef struct {
	float accum;
	float percent;} lipStat;

typedef struct  {
	int x; 
	int y; 
	int dx; 
	int dy;} HVLINE;

	typedef struct  {
	int x; 
	int y; 
	int x1; 
	int y1;
	int x2; 
	int y2;} SIXPOINT;

typedef struct  {
	int x; 
	int y; } SinglePoint;

typedef struct  {
	float x; 
	float y; } SinglePointF;

typedef struct  {
	int x; 
	int y; 
	int x1; 
	int y1; } TwoPoint;

class CXAxisView : public CView
{
friend class CCapApp;
friend class CMainFrame;
public:

	///////////////////////////////////////////
	bool first;

	bool	cameraDone1[200]; 
	bool	cameraDone2[200]; 
	bool	cameraDone3[200]; 

	double spanElapsed0;		//Used to hold time in seconds of how long XAxisView.Inspect() takes.
	LARGE_INTEGER  timeStart0,	//Used to hold time at which XAxisView.Inspect() starts.
				   timeEnd0,	//Used to hold time at which XAxisView.Inspect() ends.
				   Frequency0;	//Used to convert processor cycles to seconds time measurements.

    __int64 nDiff0;				//Temporary variable used to hold difference between the end and start time of XAxisView.Inspect().
	__int64 nDiff1;				//Temporary variable used to hold difference between the end and start time of XAxisView.Inspect().
	__int64 nDiff2;				//Temporary variable used to hold difference between the end and start time of XAxisView.Inspect(). 
	__int64 nDiff3;				//Temporary variable used to hold difference between the end and start time of XAxisView.Inspect().				

	double timeMicroSecondC1I0;		//Used to hold time in seconds of how long XAxisView.Display() takes.
	LARGE_INTEGER m_lnC1StartI0,	//Used to hold time at which XAxisView.Display() starts.
				  m_lnC1EndI0,		//Used to hold time at which XAxisView.Display() ends.	
				  m_lnC1FreqI0;		//Used to convert processor cycles to seconds time measurements.

	double timeMicroSecondC2I0;		//Used to hold time in seconds of how long XAxisView.Display2() takes.
	LARGE_INTEGER m_lnC2StartI0,	//Used to hold time at which XAxisView.Display2() starts.
				  m_lnC2EndI0,		//Used to hold time at which XAxisView.Display2() ends.	
				  m_lnC2FreqI0;		//Used to convert processor cycles to seconds time measurements.

	double timeMicroSecondC3I0;		//Used to hold time in seconds of how long XAxisView.Display3() takes.	
	LARGE_INTEGER m_lnC3StartI0,	//Used to hold time at which XAxisView.Display3() starts.
				  m_lnC3EndI0,		//Used to hold time at which XAxisView.Display3() ends.	
				  m_lnC3FreqI0;		//Used to convert processor cycles to seconds time measurements.


	///////////////////////////////////////////
protected:
	CXAxisView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CXAxisView)

	BYTE* FlipImage1(BYTE* Image);
	BYTE* FlipImage2(BYTE* Image);
	BYTE* FlipImage3(BYTE* Image);

	SinglePoint FindNeckLipPatUser2(BYTE* im_srcIn,int x, int y, bool left);
	SinglePoint CheckU2Band(BYTE *im_srcHV, int x,int y,int x1,int y1, int TBx, int TBy);
	bool CheckForBlack(BYTE *im_srcIn, int filly);
	SinglePoint FindCapSide(BYTE* im_srcIn, int MidCap, bool left);
	SinglePoint FindCapTop(BYTE* im_srcIn, int SideDist, bool left);
	SinglePoint FindNeckSide(BYTE* im_srcIn, int NeckPos, int CapPos,bool left);
	SinglePoint FindNeckLip(BYTE* im_srcIn, SinglePoint NeckSide, bool left);
	SinglePoint FindNeckLipPat(BYTE* im_srcIn, SinglePoint NeckSide, bool left);
	SinglePoint FindNeckLipPatNoLip(BYTE* im_srcIn, SinglePoint NeckSide, bool left);
	SinglePoint FindNeckLipPatUser(BYTE* im_srcIn, int x, int y, bool left);
    SinglePoint FindCapTopSport(BYTE* im_srcIn, SinglePoint SideDist, bool left);
	float CheckSportBand(BYTE* im_srcIn, SinglePoint CapLTop, SinglePoint CapRTop);
	int ldist;					//x coordinate of left vertical cap line.
	int rdist;					//x coordinate of right vertical cap line.
	int midBand;
	int sgSize;
	int capWidth;
	int learnedCapWidth;		//Cap width taught during the inspection configuration.
	int capWidthDiff;			//Difference between learned cap width and actual cap width.
	int targetR;				//Cap color taught R value.	
	int targetG;				//Cap color taught G value.
	int targetB;				//Cap color taught B value.
	int resR;
	int resG;
	int resB;

	CFile ImageFile;
	int	count_pic;				//Number of pictures to load from the hard drive. Used in Load50Pics() 
	bool load_1_pic;			//Flag indicating that multiple pictures will be loaded from the hard drive.
	bool freeze_current_pic;	//Flag used when multiple pictures are loaded from the hard drive. If set to true it will keep the current picture displayed.

	BYTE* LearnPatternNoLip(BYTE *im_srcIn, bool left);
	BYTE* LearnPatternUser(BYTE *im_srcIn, bool left);
	
	void OnReadC(char* pFileName);
	void PreInsp();
	BYTE* GetCam1(BYTE* im_cam1);
	BYTE* GetCam2(BYTE* im_cam2);
	BYTE* GetCam3(BYTE* im_cam3);
	BYTE *im_bw;				//Temporary image array for converting camera 1 image to grayscale.
	BYTE *im_bw2;				//Temporary image array for converting camera 2 image to grayscale.
	BYTE *im_bw3;				//Temporary image array for converting camera 3 image to grayscale.
	BYTE *im_srcam1;			//NOT USED
	BYTE *im_srcam2;			//NOT USED
	BYTE *im_srcam3;			//NOT USED
	BYTE* im_cam1;				//Grayscale image of camera 1.

	BYTE* im_cam2;;				//Grayscale image of camera 2.
	BYTE* im_cam3;;				//Grayscale image of camera 3.
	BYTE* NoCamConvert(BYTE* Image);
	unsigned char status;

	void OnSaveBW(BYTE *im_sr, int who);

	bool OnSaveC(BYTE *im_sr, int who);

	int GetHue(int r, int g, int b);
	//BYTE *imgBufBad;

	int CheckForBumpSport2(BYTE *im_srcHV3, int x,int x2, int limit,int left);
	SinglePoint CheckBand2(BYTE *im_srcIn, BYTE *im_srcC, BYTE *im_srcInSob, BYTE *im_srcR, BYTE *im_srcInHue, int TBYDiff, int XWidth, int type, int color, bool Left);
	SinglePoint CheckBand(BYTE *im_srcHV, BYTE *im_srcC, BYTE *im_red, BYTE *im_hue, int x, int y, int x1, int y1, int TBx, int TBy, int type, int color);
		TwoPoint TB2Rect;
// Structure used to draw to the screen.
   BITMAPINFO        m_bitmapInfo; 
 // Copy of the image structure grabbed from the SDK.
    BYTE*   m_imageRaw;

   // Processed image for displaying and saving
    BYTE*   m_imageProcessed[3];
	int imageSize[3];
	void AllocateAndCopyImage(int index);

	
	void initBitmapStruct( int iCols, int iRows );
	void DrawImages();
	BYTE* Reduce(BYTE* Image);
	BYTE* Reduce2(BYTE* Image);
	BYTE* Reduce3(BYTE* Image);
	BYTE* Reduce2x(BYTE* Image);
	BYTE* Reduce3x(BYTE* Image);

	BYTE* CtoBW(BYTE* Image);
	BYTE* CtoBW2(BYTE* Image);
	BYTE* CtoBW3(BYTE* Image);
	BYTE* im_reduce;		//Temporary variable used spatially subsample images for camera1, used in Reduce()
	BYTE* im_reduce1;		//Camera 1 image spatially subsampled
	BYTE* im_reduce2;		//Camera 1 image spatially subsampled
	BYTE* im_reduce3;		//Camera 1 image spatially subsampled

private:
	//TwoPoint GetBestDist(int distance[10]);
	//int GetBestGap(int st2[60]);
	//int GetBestGapTop(int st2[30]);
	lipStat statSaveLeft[70];
	lipStat statSaveRight[70];
	void TeachBandFront(BYTE *im_srcHV, int x,int y,int x1,int y1, int TBx, int TBy );

	int currentSegValueFront[30];
	int taughtSegValueFront[30];
	int currentSegValueRight[30];
	int taughtSegValueRight[30];
	int currentSegValueLeft[30];
	int taughtSegValueLeft[30];
	
	float CapW;
	float CapLHeight;
	float CapRHeight;
	
	float LHArray[12];			//Array holding the last 10 left heights. If left height inspection is enabled.
	int LHInc;					//Index of left height last 10 array.

	float RHArray[12];			//Array holding the last 10 right heights. If right height inspection is enabled.
	int RHInc;					//Index of right height last 10 array.

	float MHArray[12];			//Array holding the last 10 middle heights. If middle height inspection is enabled.
	int MHInc;					//Index of middle height last 10 array.

	float DArray[12];			//Array holding the last 10 cocked cap inspection values. If cocked cap inspection is enabled.
	int DInc;					//Index of cocked cap values last 10 array.

	float TBArray[12];			//Array holding the last 10 Camera 1 surface tamper band inspection values. If Camera 1 surface tamper band inspection inspection is enabled.
	int TBInc;					//Index of Camera 1 surface tamper band inspection values last 10 array.

	float TB2Array[12];			//Array holding the last 10 Camera 2 and 3 surface tamper band inspection values. If Camera 2 and 3 surface tamper band inspection inspection are enabled.
	int TB2Inc;					//Index of Camera 2 and 3 surface tamper band inspection values last 10 array.

	float FArray[12];			//Array holding the last 10 low fill inspection values. If low fill inspection is enabled.
	int FInc;					//Index of low fill inspection values last 10 array.

	float UArray[12];			//Array holding the last 10 cap color inspection values. If cap color inspection is enabled.
	int UInc;					//Index of cap color inspection values last 10 array.

	SinglePoint lBand[30];
	SinglePoint rBand[30];
	

// Attributes
public:

	bool inspResultOK[12];	//Array holding results of inspections. 1 = left height, 2 = right height, 3 = middle height, 4 = cocked cap, 5 = Band edges, 6 = Band surface, 7 = Low fill, 8 = cap color, 9 and 10 user inspection.
	bool fault1;				//Flag indicating if inspection 1 failed
	bool fault2;				//Flag indicating if inspection 1 failed
	bool fault3;				//Flag indicating if inspection 1 failed
	bool fault4;				//Flag indicating if inspection 1 failed
	bool fault5;				//Flag indicating if inspection 1 failed
	bool fault6;				//Flag indicating if inspection 1 failed
	bool fault7;				//Flag indicating if inspection 1 failed
	bool fault8;				//Flag indicating if inspection 1 failed
	bool fault9;				//Flag indicating if inspection 1 failed
	bool fault10;				//Flag indicating if inspection 1 failed
	bool skipNext;				//NOT USED
	int tot_bottle_cnt;

////////

	BYTE* grayscale_cam1;
	BYTE* grayscale_cam2;
	BYTE* grayscale_cam3;



	BYTE* sobel_cam1;			//NOT USED
	BYTE* sobel_cam1_reduced;			//NOT USED
	BYTE* sobel_cam2;			//Sobel resulted image (edge highlighted image) for camera 2.
	BYTE* sobel_cam3;			//Sobel resulted image (edge highlighted image) for camera 3.

	BYTE* im_red;
	BYTE* im_red2;
	BYTE* im_red3;
	BYTE* im_hue;
	BYTE* im_hue2;
	BYTE* im_hue3;

	SinglePoint rearBandLwSobel[2000];
	SinglePoint rearBandRwSobel[2000];
	SinglePoint FrontBandWSobel[2000];
	int inspn_failed[20];					//Array holding type of inspection failed.
	int count_inspn_failed;					//Index of failed inspection
	
	bool foil_missing;

/////////////

	int E1Cnt;			//Number of left height inspections failed, used for displaying on screen in statistics tab.
	int E1Cntx;			//Number of left height inspections failed, used for saving data to hard drive.
	int E1Cnt24;		//Number of left height inspections failed, used for saving 24 hours data.

	int E2Cnt;			//Number of right height inspections failed, used for displaying on screen in statistics tab.
	int E2Cntx;			//Number of right height inspections failed, used for saving data to hard drive.
	int E2Cnt24;		//Number of right height inspections failed, used for saving 24 hours data.

	int E3Cnt;			//Number of middle height inspections or sports caps failed, used for displaying on screen in statistics tab.
	int E3Cntx;			//Number of middle height inspections failed, used for saving data to hard drive.
	int E3Cnt24;		//Number of middle height inspections failed, used for saving 24 hours data.
	
	int E4Cnt;			//Number of sports cap inspections failed, used for displaying on screen in statistics tab.
	int E4Cntx;			//Number of sports cap inspections failed, used for saving data to hard drive.
	int E4Cnt24;		//Number of sports cap inspections failed, used for saving 24 hours data.

	int Missing_Foil_Count;//Number of bottles with missing foil

	int E5Cnt;			//Number of tamper band edge inspections failed, used for displaying on screen in statistics tab.
	int E5Cntx;			//Number of tamper band edge inspections failed, used for saving data to hard drive.
	int E5Cnt24;		//Number of tamper band edge inspections failed, used for saving 24 hours data.

	int E6Cnt;			//Number of tamper band  surface inspections failed, used for displaying on screen in statistics tab.	
	int E6Cntx;			//Number of tamper band  surface inspections failed, used for saving data to hard drive.
	int E6Cnt24;		//Number of tamper band  surface inspections failed, used for saving 24 hours data.

	int E7Cnt;			//Number of low fill inspections failed, used for displaying on screen in statistics tab.	
	int E7Cntx;			//Number of low fill inspections failed, used for saving data to hard drive.
	int E7Cnt24;		//Number of low fill inspections failed, used for saving 24 hours data.	

	int E8Cnt;			//Number of color inspections failed, used for displaying on screen in statistics tab.	
	int E8Cntx;			//Number of color inspections failed, used for saving data to hard drive.
	int E8Cnt24;		//Number of color inspections failed, used for saving 24 hours data.	

	int E9Cnt;			//Number of user inspections failed, used for displaying on screen in statistics tab.	
	int E9Cntx;			//Number of user inspections failed, used for saving data to hard drive.
	int E9Cnt24;		//Number of user inspections failed, used for saving 24 hours data.	

	int E10Cnt;			//Number of user inspections failed, used for displaying on screen in statistics tab.
	int E10Cntx;		//Number of user inspections failed, used for saving data to hard drive.
	int E10Cnt24;		//Number of user inspections failed, used for saving 24 hours data.	

	int segSize;
	void updateStats(int q, bool left);
	void TeachBandRear(BYTE *im_srcIn, BYTE *im_srcC, int TBYDiff, int XWidth, bool Left);
	void clearLipStats(int q);
	void writeOutGrayArrayFile(const char * filename, const int width,const int height,const unsigned char * bmpGrayArray);
	void Display(BYTE *image);
	void Display2(BYTE *image);
	void Display3(BYTE *image);

	double spanElapsed;
	double spanElapsed1;
	double spanElapsed2;
	double spanElapsed3;
	double spanElapsed4;
	
	////////////
	SinglePoint RearL;			//Camera2 x distance of cap from right edge.
	SinglePoint RearR;			//Camera3 x distance of cap from left edge.

	SinglePoint Cam2_neckpts;
	SinglePoint Cam3_neckpts;

	SinglePointF CaptopRearL;	//Coordinates of top most point of cap as found on rear left camera (cam2).
	SinglePointF CaptopRearR;	//Coordinates of top most point of cap as found on rear left camera (cam3).

	SinglePoint cam2min;SinglePoint cam2max;SinglePoint cam3min;SinglePoint cam3max;
	bool cam2cocked;				//Temp flag variable used in dist2top_cam2() used to indicate that cap is cocked as seen by camera 2.
	bool cam3cocked;				//Temp flag variable used in dist2top_cam3() used to indicate that cap is cocked as seen by camera 3.


	///////////////
	
	SinglePoint TB2Offset;
	SinglePointF MidBottom;			//y coordinate of max(lip left, lip right)
	SinglePointF MidTop;			//Coordinates of mid top cap point.
	SinglePoint CapLSide;			//Coordinates of left cap point.
	SinglePoint CapRSide;			//Coordinates of right cap point.
	SinglePoint CapLTop;			//Coordinates of left top cap point.
	SinglePoint CapRTop;			//Coordinates of right top cap point.
	SinglePoint RTopBar;
	SinglePoint LTopBar;
	SinglePoint NeckLSide;			//Coordinates of left neck point.
	SinglePoint NeckRSide;			//Coordinates of right neck point.
	SinglePoint NeckLLip;			//Coordinates of left lip point.
	SinglePoint NeckRLip;			//Coordinates of right lip point.
	SinglePoint TBOffset;			//Delta between the cam1 tamper band edge search box coordinates and the cap edge.

	SinglePoint RTBand;
	SinglePoint LipOffset;			//?Vertical offset between the horizontal neck line and the actual lip.
	SinglePoint NeckBar;
	SinglePoint NeckBar1024;
	SinglePoint MidBar;
	SinglePoint MidBar1024;			//Coordinates of the mid cap bar at 1024 pixel image. (Not scaled).
	SinglePoint RightBand;			//Holds count of how many points in the cam3 surface tamper band are beyond the threshold.
	SinglePoint LeftBand;			//Holds count of how many points in the cam2 surface tamper band are beyond the threshold.
	SinglePoint FrontBand;			//Holds count of how many points in the cam1 surface tamper band are beyond the threshold.
	SinglePoint FrontU2Band;
	SinglePoint RightU2Band;
	SinglePoint LeftU2Band;
	int Targ1b;
	int Targ1a;
	int Lim1Max;
	int Lim1Min;
	bool InspOK;
	int lx;
	int ly;
	int ldx;
	int ldy;
	int rx;
	int ry;
	int rdx;
	int rdy;	

	// Get the image.
	const CVisImageBase& Image() const
		{ return m_image; }

	CVisImageBase& Image()
		{ return m_image; }
// Operations
	CCapApp *theapp;
	CMainFrame* pframe;
	BYTE *imgBuf0;
	BYTE *imgBuf1;
	BYTE *imgBuf2;
	BYTE *imgBuf3;
	BYTE *im_result;
	BYTE *im_resultc;

	void OnManual();

	int CRate;
	int ERate;
	int RHCnt;
	int LHCnt;
	int MHCnt;
	int FCnt;
	int DCnt;
	int UCnt;
	int U2Cnt;
	int TBCnt;
	int TB2Cnt;
	int CockedCnt;		//Number of cocked cap inspections failed, used for displaying on screen in statistics tab.

	

	float RHt;			//Average of the last 10 right height inspections.
	float LHt;			//Average of the last 10 left height inspections.
	float MHt;			//Average of the last 10 middle height inspections.
	float LHtf;			//Total of the last 10 left height inspections.
	float TBtf;			//Total of the last 10 Camera 1 surface tamper band inspections
	float TB2tf;		//Total of the last 10 Camera 2 and 3 surface tamper band inspections.
	float Dtf;			//Total of the last 10 cocked cap inspections.
	float Ft;			//Average of the last 10 low fill inspections.
	float Ftf;			//Total of the last 10 low fill inspections.
	float Dt;			//Average of the last 10 cocked cap inspections.
	float Ut;			//Average of the last 10 cap color inspections.
	float Utf;			//Total of the last 10 cap color inspections
	float U2t;
	float TBt;			//Average of the last 10 Camera 1 surface tamper band inspections
	float TB2t;			//Average of the last 10 Camera 2 and 3 surface tamper band inspections.

	BYTE* im_cam1Color;		//Camera 1 image in color flipped vertically.
	BYTE* im_flip1;			//Temporary image array for flipping camera 1 image.
	BYTE* im_cam2Color;		//Camera 2 image in color flipped vertically.
	BYTE* im_flip2;			//Temporary image array for flipping camera 2 image.
	BYTE* im_cam3Color;		//Camera 3 image in color flipped vertically.
	BYTE* im_flip3;			//Temporary image array for flipping camera 3 image.
	BYTE* im_sr;
	BYTE* im_src0;
	//BYTE* im_src;
	BYTE* im_srcIn;
	BYTE* im_srcIn2;
	BYTE* im_srcIn3;
	//BYTE* im_src2;
	BYTE* im_data;				//pointer to camera1 image grayscale.
	BYTE* im_datax;
	BYTE* im_data2;				//pointer to camera2 image grayscale.
	BYTE* im_data3;				//pointer to camera3 image grayscale.
	BYTE* im_dataC;				//pointer to camera1 image in color.
	BYTE* im_dataCR;
	BYTE* im_dataCL;
	BYTE* im_dataC2;			//pointer to camera2 image in color.
	BYTE* im_dataC3;			//pointer to camera3 image in color.
	BYTE* im_pat;
	BYTE* im_patl;				//Taught left lip pattern used for display
	BYTE* im_patr;				//Taught right lip pattern used for display
	BYTE* im_match;
	BYTE* im_matchl;			//Taught left lip pattern
	BYTE* im_matchr;			//Taught right lip pattern
	BYTE* im_match2;
	BYTE* im_reduceX;
	int Xtra;

	void OnReadLast();
	void OnReadLast2();
	void OnReadLast3();
	void OnReadLast4();
	void Inspect(BYTE* im_srcColor, BYTE *im_srcInC2, BYTE *im_srcInC3, BYTE* im_src, BYTE* im_src2, BYTE* im_src3,BYTE*,BYTE*, 
		BYTE *im_srcInRed, BYTE *im_srcInRed2, BYTE *im_srcInRed3, BYTE* im_srcInHue, BYTE *im_srcInHue2, BYTE *im_srcInHue3);
	void LoadImage();
	double GetMax(int numvals, double a, double b, double c, double d);
	//double GetMin(int numvals, double a, double b, double c, double d, double e, double f);
	double GetMin(int numvals, double a, double b, double c, double d);
	BYTE* GetOneThird(BYTE* im_sr, int cam);
	int CheckFill(BYTE *im_sr, int x, int y);
	void OnSave(BYTE *im_sr);
	void SavePattern(BYTE *im_sr);
	//void OnSnap();

	void LoadCam();
	TwoPoint GetTop(BYTE *im_srcHV, int x, int y);
    
	float FindCapMid(BYTE* im_srcIn, float MidTop);
	BYTE* LearnPattern(BYTE *im_srcIn, bool left);
	TwoPoint CapTopM;
	TwoPoint TBand;
	TwoPoint MTBand;
	int MTBLength;
	TwoPoint TBand2;
	TwoPoint U2;
	TwoPoint RTBand2;
	void OnRead(char* pFileName);
	void OnLoad(int Job);

public:

	void Foil_Sensor(BYTE* image, int startx,int endx,int starty,int endy,bool&);
	void BinarizeFillLevelArea(BYTE* Image);
	void CalcSobel_Reduced(BYTE* image);
	void GetHueChannel(BYTE *colorImage, int width, int height, BYTE* dest);
	void GetRedChannel(BYTE *colorImage, int width, int height, BYTE* dest);

	void Load50Pics(  );
	void OnLoadLipPattern(int);
	void SaveLipPattern(BYTE * im_sr,bool );
	void OnLoadPattern(int Job);
	void Check_Cap_presence(BYTE* , SinglePoint );
	void FindEndLight(int );
	void CheckTemp();
	BYTE* CalcSobel3(BYTE *Image, int camNo);
	BYTE* CalcSobel2(BYTE *Image, int camNo);
	BYTE* CalcSobel(BYTE *Image, int ,int,int,int);
	bool dist2top_cam3(BYTE *im_srcIn, int x, int y);
	bool dist2top_cam2(BYTE *im_srcIn, int x ,int y);
	SinglePointF FindCapTopRear(BYTE *im_srcIn, int SideDist , bool left);
	int newMidCap;
	int midYResult;
	int FoilWindow(BYTE* im_srcIn);
	int CheckFill2(BYTE *im_srcHV, int x,int y);
	int E7CntNet;	//Counter used for external client PLC.
	int E4CntNet;	//Counter used for external client PLC.
	int E3CntNet;	//Counter used for external client PLC.
	int E2CntNet;	//Counter used for external client PLC.
	int E1CntNet;	//Counter used for external client PLC.
	int posToShow;
	int currentCapWidth;		//Actual cap width of current cap.
	int cam1Total;
	int cam2Total;
	int cam3Total;
	int capPointCount;
	SinglePoint sam;
	SinglePoint saml;
	SinglePoint samr;
	int showPos;		//Used to set and get image index (cam1, 2 or 3 image) where a rectangle should be displayed, the rectangle indicates that the surface tamper band of that camera's image had the most values outside of bounds.
	int showPos1;		//NOT USED
	int showPos2;		//NOT USED
	int showPos3;		//NOT USED
	int bandPos;		//NOT USED
	bool teachFrontBand;
	bool initLip;
	int largestQR;
	int largestQ;
	int pic1;


	bool no_cap_detected;
	int pic_number;
	int no_cap_x;
	int no_cap_y;

	float TBYOffsetR;				//(OriginalTBYOffset-(CapLTop.y+CapRTop.y)/2)*scale factor.
									//Difference between the taught and computed cap left top and cap right top y coordinates.
									//Scaled for right camera.

	bool lockQuery;
	void ChooseBestPlacement();
	bool reteachColor;				//Flag indicating if a new cap color should be taught.
	bool GotImage3;
	bool GotImage2;
	bool GotImage1;
	int nScreenHeight;
	int nScreenWidth;
	int colorCount;
	int BWCount;
	bool gotColor;
	
	bool Once;
	SinglePoint RShoulder;
	SinglePoint LShoulder;
	SinglePoint FindShoulder(BYTE *im_srcIn, int TopLeft, int TopRight, SinglePoint CapSide, int NeckBar, bool Left);
	bool Skip;			//NOT USED
	bool Skip2;			//NOT USED
	bool Skip3;			//NOT USED
	bool Skip4;			//NOT USED
	bool Skip5;			//NOT USED
	int Result10;
	bool CapU2OK2;
	bool CapU2OK2Lt;
	bool CapTBOKLt;
	int U2Thresh2;
	SinglePoint CheckU2Band2(BYTE *im_srcIn, int TBYDiff, int XWidth, int offset, bool Left);
	TwoPoint CheckSportBand2(BYTE* im_srcIn, SinglePoint CapLTop, SinglePoint CapRTop);
	TwoPoint sportBand;
	SinglePoint capPoint[60];
	SinglePoint capPointR[60];
	SinglePoint capPointL[60];
	int capPointCountR;
	int capPointCountL;
	int CheckForBump(BYTE *im_srcHV, int x,int x2, int limit);
	int CheckForBump2(BYTE *im_srcHV3, int x,int x2, int limit, int left);
	int U2Offset;
	int U2Thresh;
	int U2Height;

	bool KillSD;
	bool KillSave;
	int badcount;
	bool RolledOver;				//Used in OnSaveBW() and OnSaveC(). Used for sequentially numbering files.
	bool GotOneBad;
//	bool BlackOK;

	void ReEstablishCameras();
	bool RejectAll;					//Flag indicating that all bottles are being ejected.
	int TBl;
	int OriginalTBYOffset;			//Taught average of cap left top and cap right top y coordinates.
	int TBYOffset;					//OriginalTBYOffset-(CapLTop.y+CapRTop.y)/2, difference between the taught and computed cap left top and cap right top y coordinates.
	int NoCapCnt;					//Number of missing cap inspections failed, used for displaying on screen in statistics tab.
	int LooseCnt;					//Number of loose cap inspections failed, used for displaying on screen in statistics tab.		
	bool TurnOff23;					//Flag indicating if camera 2 and 3 image drawing is turned off when adjusting the low fill box from the modify tab.
	bool StartLockOut;				//Flag used to check when to start computing averages and standard deviations for the inspections. 
	int TimesThru;					//Counter for tracking how many bottles have been scanned since either the user resetting the totals or the beginning of the programming being turned on.
	bool STech;
	int Black;

	float LHsd;			//Standard Deviation of the last 10 left height inspections.
	float TBsd;			//Standard Deviation of the last 10 Camera 1 surface tamper band inspections
	float TB2sd;		//Standard Deviation of the last 10 Camera 2 and 3 surface tamper band inspections.
	float Fsd;			//Standard Deviation of the last 10 low fill inspections.
	float Dsd;			//Standard Deviation of the last 10 cocked cap inspections.
	float Usd;			//Standard Deviation of the last 10 cap color inspections.
	
	//standard deviation for color
	COLORREF ColorTarget;			//cap color to search for.
	COLORREF ColorResult;			//cap color found.
	COLORREF DoUser(BYTE* im_srcIn, int x, int y, int width, int UserStrength);
	
	int UserStrength;	//Color search box, does not appear to be used.
	int b;
	float Band2Size;	//Cameras 2 and 3 tamper band search boxes width. This value is the sum of the width of the search boxes.
	
	
	bool ReloadOnce;
	int cam1dy;
	int cam1y;
	bool CapTB2OK;
	bool CapTB2OKLt;
	int XEnd;
	int XStart;
	int NeckOffsetX;	//X Delta between neck left side and cap left side.

	int LipDiffl;		//Coordinates of left lip bottom.
	int LipDiffr;		//Coordinates of left lip bottom.
	bool Modify;		//Flag indicating if modify tab is selected.
	void LearnBand(BYTE* im_srcIn);
	TwoPoint Fill;
	float HeightDiff;
	bool LearnDone;				//Flag indicating if inspection is being taught or performed.

	
	CVisByteImage imageBWx;
	CVisByteImage imageBW;			//Lip pattern image grayscale.
	CVisByteImage imageBW2;
	CVisByteImage imageBW3;
	CVisByteImage imagepat;				//Used in SavePattern() to save image of lip pattern to hard drive.
	CVisByteImage imageS;
	CVisRGBAByteImage imageFile;
	CVisRGBAByteImage imageFileL;
	CVisRGBAByteImage imageFileR;

	CVisByteImage	imageC1Sobel;


	CVisRGBAByteImage imageCS;			//Temporary variable used to save camera 1 to hard drive.
	CVisRGBAByteImage imageC;			//Camera 1 image displayed on screen.
	CVisRGBAByteImage imageC2;			//Camera 2 image displayed on screen.
	CVisRGBAByteImage imageC3;			//Camera 3 image displayed on screen.

	CVisRGBAByteImage imageFile1;
	CVisRGBAByteImage imageFile2;
	CVisRGBAByteImage imageFile3;
	bool write = false;
	int Rejects;
	int MidCap;
	
	bool ShowBars;
	bool toggle;
	float Result1;			//Left Cap Height, Distance between left neck lip and left top point of cap.
	float Result2;			//Right Cap Height, Distance between right neck lip and right top point of cap.
	float Result3;			//Middle Cap Height, Delta between the cap top y coordinate and lip y coordinate.
	float Result4;			//Cocked Cap, NOT USED
	float Result5;			//Holds count of how many points in the cam1 surface tamper band are beyond the threshold.
	float Result6;			//Holds count of how many points in the cam2 or cam3, which ever is greatest, surface tamper band are beyond the threshold.
	float Result7;			//Holds count of how many points in cam1 are beyond the low fill threshold.
	float Result8;			//Holds result of cap color RGB values comparison.
	int Result9;			//User inspection result, NOT USED

	int LHMin;
	int LHMax;
	int RHMin;
	int RHMax;
	int MHMin;
	int MHMax;
	int TBMin;
	int TBMax;
	int TB2Min;
	int TB2Max;
	int DMin;
	int DMax;
	int UMin;
	int UMax;
	int U2Min;
	int U2Max;

	int FillMin;
	int FillMax;
	int midband;
	int NeckHeight;
	float CapHMin;
	float CapHMax;
	void TrigReset();
	float CapWMin;
	float CapWMax;
	bool FillOK;
	float ScaleFactorY;
	bool CapTBOK;
	float CapMHeight;
	bool CapMHOK;
	bool CapUOK;
	bool CapU2OK;
	bool CapLHOK;
	bool CapRHOK;
	bool CapDHOK;
	bool CapFOK;
	bool CapMHOKLt;
	bool CapUOKLt;
	bool CapU2OKLt;
	bool CapLHOKLt;
	bool CapRHOKLt;
	bool CapDHOKLt;
	bool CapFOKLt;
	float CapDef;
	int CapTopR;
	int CapTopL;
	float ScaleFactor;
	float ScaleFactorM;
	HVLINE basel;
	HVLINE baser;
	HVLINE basepointr;
	HVLINE basepointl;

	HVLINE CapBottomM;
	
	int piccount;		//Used in OnSaveBW() and OnSaveC(). Used for sequentially numbering files.

	BYTE* Kernel(BYTE *im_srcHV,int x, int y, int dx, int dy);


	bool BackLight(BYTE *im_srcHV);

	bool BadCap;		//Flag indicating that one of the cap inspections failed.

	int GetBestAvg(int p1, int p2, int p3);


	/////////////for the picture Log
	//Contains pointer to byte array holding last defective images.
	BYTE *imgBuf3RGB_cam1;
	BYTE *imgBuf3RGB_cam2;
	BYTE *imgBuf3RGB_cam3;
	//BYTE *imgBuf3RGB_cam4;

	BYTE* arr1[3];			//array containing pointers to byte data for all defective images. arr1[0] is the pointer to all defective images
	int tim_stamp[1000][5]; //changed from [][4] to [][5] to accomodate for the Milliseconds
	int imge_no;			//Index of selected defective bottle.
	int typofault;			//Type of inspection failed.
	int showostore;			//Flag indicating if show defective pictures dialog box is currently displayed. 0 = not displayed, 1 = displayed
//////////

	
	/////for the fill level graph
	float fill_level[600];
	int count_fill;
	bool graph_open;


	float fill_level_scale10[6000];
	int count_fill_scale10;

	float filler_valves[120][6000];
	int filler_no;int fillr_no_value;

	////
SinglePoint rearBandL[2000];
SinglePoint rearBandR[2000];
SinglePoint frontBand[2000];


	int CapThresh;
	CString c;					//Used when in SavePattern() to save lip pattern image to hard drive. Used in OnLoad().
	CString d;
	CString Fault;				//Message displayed in status box.
	CString SavedLast;
	CString SavedLast1;
	CString SavedLast2;
	CString SavedLast3;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXAxisView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
CVisByteImage m_image;
	virtual ~CXAxisView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CXAxisView)
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CXAXISVIEW_H__FABF46A7_94FC_44B1_A39A_F7F5753FF25B__INCLUDED_)
